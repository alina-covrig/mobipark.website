$(document).ready(function() {


    $('#owl-main').owlCarousel({
        "items": 1,
        "dots": true,
        "autoplay": true,
        "loop": true,
        "autoplayTimeout": 5000,
        "smartSpeed": 550

    });


    var navbar = $('.navbar');
    var sticky = $('.hero').height();

    var stickNavbar = function(scroll){
        if (scroll >= sticky) {
            navbar.removeClass('navbar-hero').addClass('navbar-top');
        } else {
            navbar.addClass('navbar-hero').removeClass('navbar-top');
        }
    };

    var scroll = window.pageYOffset;
    $(window).scroll(function(){
        scroll = window.pageYOffset;
        stickNavbar(scroll);
    })

    $(document).ready(function(){
        stickNavbar(scroll);
    })



    $( ".subcateg" ).hover(
        function() {
            $('.dropdown-mm').css('display', 'block');
        },
        function() {
            $('.dropdown-mm').css('display', 'none');
        }
    );




    $('.carousel-item:first').addClass('active');

    $(".scroll-section").click(function() {
        $('html,body').animate({
                scrollTop: $(".second-section").offset().top},
            'slow');
    });


    $('#bloc1-gallery').owlCarousel({
        items: 5,
        margin:10,
        stagePadding: 10,
        dots: false,
        responsive:{
            0:{
                items:2.5
            },
            600:{
                items:3.5
            },
            1000:{
                items:5
            }
        }
    });


    $('.see-more').on('click', function(){
        var parent_div = $(this).parent().closest('.content-preview');
        var parent_gradient = $(this).parent().closest('.see-more-gradient');

        if ($(this).hasClass('see-less')) {
           $(this).text('Citeste mai mult');
            $(this).removeClass('see-less');
            parent_div.removeClass('content-full');
        } else {
            $(this).addClass('see-less');
           $('.see-less').text('Citeste mai putin');
            parent_div.addClass('content-full');
        }
    });



    // Accordion Action
    const accordionItem = document.querySelectorAll(".accordion-item");

    accordionItem.forEach((el) =>
        el.addEventListener("click", () => {
            if (el.classList.contains("active")) {
                el.classList.remove("active");
            } else {
                accordionItem.forEach((el2) => el2.classList.remove("active"));
                el.classList.add("active");
            }
        })
    );

  // acordeon apartment page
    $(document).ready(function(){
        $(".collapse.show").each(function(){
            $(this).prev(".card-header").find(".fa-w-14").addClass("fa-chevron-down").removeClass("fa-chevron-up");
        });
        $(".collapse").on('.show.bs.collapse', function(){
            $(this).prev(".card-header").find(".fa-w-14").removeClass("fa-chevron-up").addClass("fa-chevron-down");
        }).on('hide.bs.collapse', function(){
            $(this).prev(".card-header").find(".fa-w-14").removeClass("fa-chevron-down").addClass("fa-chevron-up");
        });
    });


    /*---- swhich apartment type ----*/
    $('#choose-apartment').on('change', function(e) {
        $('.tab-pane').removeClass('show active')
        $('#' + $(e.currentTarget).val()).addClass("show active");
    })


    /*--- galerie pagina apartament 1 -----*/
    $('.icon-gallery').on('click', function() {
        $.fancybox.open([
            {
                src: 'assets/360-images/spinner_ap_01/spinner-1.png',
                opts: {
                    thumb: 'assets/360-images/spinner_ap_01/spinner-1.png'
                }
            },
            {
                src: 'assets/360-images/spinner_ap_01/spinner-3.png',
                opts: {
                    thumb: 'assets/360-images/spinner_ap_01/spinner-3.png'
                }
            },
            {
                src: 'assets/360-images/spinner_ap_01/spinner-5.png',
                opts: {
                    thumb: 'assets/360-images/spinner_ap_01/spinner-5.png'
                }
            },
            {
                src: 'assets/360-images/spinner_ap_01/spinner-7.png',
                opts: {
                    thumb: 'assets/360-images/spinner_ap_01/spinner-7.png'
                }
            },
            {
                src: 'assets/360-images/spinner_ap_01/spinner-9.png',
                opts: {
                    thumb: 'assets/360-images/spinner_ap_01/spinner-9.png'
                }
            },
            {
                src: 'assets/360-images/spinner_ap_01/spinner-11.png',
                opts: {
                    thumb: 'assets/360-images/spinner_ap_01/spinner-11.png'
                }
            },
            {
                src: 'assets/360-images/spinner_ap_01/spinner-13.png',
                opts: {
                    thumb: 'assets/360-images/spinner_ap_01/spinner-13.png'
                }
            },
            {
                src: 'assets/360-images/spinner_ap_01/spinner-15.png',
                opts: {
                    thumb: 'assets/360-images/spinner_ap_01/spinner-15.png'
                }
            },
            {
                src: 'assets/360-images/spinner_ap_01/spinner-17.png',
                opts: {
                    thumb: 'assets/360-images/spinner_ap_01/spinner-17.png'
                }
            },
            {
                src: 'assets/360-images/spinner_ap_01/spinner-19.png',
                opts: {
                    thumb: 'assets/360-images/spinner_ap_01/spinner-19.png'
                }
            },
            {
                src: 'assets/360-images/spinner_ap_01/spinner-21.png',
                opts: {
                    thumb: 'assets/360-images/spinner_ap_01/spinner-21.png'
                }
            },
            {
                src: 'assets/360-images/spinner_ap_01/spinner-23.png',
                opts: {
                    thumb: 'assets/360-images/spinner_ap_01/spinner-23.png'
                }
            },
            {
                src: 'assets/360-images/spinner_ap_01/spinner-25.png',
                opts: {
                    thumb: 'assets/360-images/spinner_ap_01/spinner-25.png'
                }
            },
            {
                src: 'assets/360-images/spinner_ap_01/spinner-27.png',
                opts: {
                    thumb: 'assets/360-images/spinner_ap_01/spinner-27.png'
                }
            },
            {
                src: 'assets/360-images/spinner_ap_01/spinner-29.png',
                opts: {
                    thumb: 'assets/360-images/spinner_ap_01/spinner-29.png'
                }
            }
        ],
            {
            loop: true
        });
    });


});



