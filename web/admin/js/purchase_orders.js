$(document).ready(function() {
    let spinning_icon = '<i class="fa fa-spin fa-circle-o-notch"></i>';

    //$.fn.multipleSelect.defaults["selectAll"] = false;
    toastr.options['positionClass'] = "toast-top-right";

    // initialize multiple-select on manufacturer select field
    $('select#select-manufacturer').multipleSelect({
        filter: true,
        placeholder: "Marca"
    });

    // editable fields initialization
    $('.user-notes').editable();
    $('.product-selling_price').editable();
    $('.product-old_price').editable();
    $('.supplier-rate').editable();

    // select contents of inputs or textareas when clicked on
    $(".shadow").on("focus", function() {
        $(this).select();
    });

    /**
     * Get async products statistical data for each table row
     * in chunks of `asyncChunk` rows (to avoid `max stack exceeded` error)
     */
    /*let asyncRows = $('tr.table-row');
    let asyncChunk = 10;
    let asyncIndex = 1;
    let asyncTimeout = 4000;

    function startAsyncChunks() {
        for (let i = (asyncIndex - 1) * asyncChunk; i < asyncIndex * asyncChunk; i++) {
            if($(asyncRows[i]).length) {
                showAsyncSuppliersData($(asyncRows[i]));
            }
        }
        asyncIndex++;
        if((asyncIndex - 1) * asyncChunk < asyncRows.length) {
            setTimeout(function() {
                startAsyncChunks();
            }, asyncTimeout);
        }
    }
    if (asyncRows.length > 0) {
        startAsyncChunks();
    }*/

    $('tr.table-row').Lazy({
        asyncFetchLoader: function(element) {
            showAsyncSuppliersData(element);
        }
    });
    /**
     * Replace preorder add form with a loading spinner and explicative text
     * (setTimeout is present because the html replacement prevents form submission)
     */
    $('#generate-new-preorder').on('click', function() {
        let $this = $(this);
        setTimeout(function() {
            $this.parent('div')
                .css({'text-align': 'center'})
                .html(spinning_icon + ' <p>Se genereaza precomanda.<br>Va rugam sa asteptati calculul necesarurilor!</p>');
        }, 500);
    });


    /**
     * Take a product row and fetches all async statistical data
     *
     * @param $row - the product row
     * @param callback - function to pass to the suppliers data display method
     */
    function showAsyncSuppliersData($row, callback) {
        let id = $row.attr('data-pid');
        let url = '/statistics/supplies/fetchStatisticsData.json';
        let dataToSend = {};
        dataToSend['product_id'] = id;
        if(preorder_id !== null) {
            dataToSend['preorder_id'] = preorder_id;
        }
        if(supplier_ids !== '') {
            dataToSend['supplier_ids'] = supplier_ids;
        }
        let $container = $row.find('.suppliers');
        let str_html = '<div style="height:' + $container.height() + 'px;">' + spinning_icon + '</div>';
        $container.html(str_html);

        let $entries_container = $row.find('.entries table');
        $entries_container.html(spinning_icon);

        $row.find('.auto').html(spinning_icon);
        $row.find('._preorders span').html(spinning_icon);
        $row.find('._purchase_o span').html(spinning_icon);
        $row.find('._inbound_o span').html(spinning_icon);
        $row.find('._sold span').html(spinning_icon);
        $row.find('._ret span').html(spinning_icon);
        $row.find('.st-ult-stoc span').html(spinning_icon);
        $row.find('.st-date-to span').html(spinning_icon);
        $row.find('._pool_mbpde ._qty_av span').html(spinning_icon);
        $row.find('._pool_backorder ._qty_av span').html(spinning_icon);

        $row.find('.m-pns span').html(spinning_icon);

        $.ajax({
            type: "POST",
            url: url,
            data: dataToSend,
            dataType: "JSON",
            success: function(r) {
                $container.html('');
                $row.find('.pr-img').html('<img src="' + r.data.image + '" />');
                $row.find('._all .avg').html(r.data.average);
                $row.find('._all .auto').html(r.data.autonomy);
                $row.find('._30 .avg').html(r.data.average_30);
                $row.find('._30 .auto').html(r.data.autonomy_30);
                $row.find('._90 .avg').html(r.data.average_90);
                $row.find('._90 .auto').html(r.data.autonomy_90);

                // $row.find('._pool_gsmnet').find('._qty_av span').text(?); nu e ajax
                $row.find('._pool_backorder').find('._qty_av span').text(r.data.backorder_stock);
                $row.find('._pool_mbpde').find('._qty_av span').text(r.data.stock_mbpde);

                $row.find('._pool_gsmnet').find('._sold span').text(r.data.sold);
                $row.find('._pool_backorder').find('._sold span').text(r.data.sold_b);
                $row.find('._pool_mbpde').find('._sold span').text(r.data.sold_m);

                $row.find('._pool_gsmnet').find('._ret span').text(r.data.returns);
                $row.find('._pool_backorder').find('._ret span').text(r.data.returns_bk); //nu exista inca
                $row.find('._pool_mbpde').find('._ret span').text(r.data.returns_mbp);

                $row.find('._preorders span').text((r.data.total_other_pieces.preorders !== null) ? r.data.total_other_pieces.preorders : 0);
                $row.find('._purchase_o span').text((r.data.total_other_pieces.orders !== null) ? r.data.total_other_pieces.orders : 0);
                $row.find('._inbound_o span').text((r.data.total_iop_amount !== null) ? r.data.total_iop_amount : 0);


                if (r.data.manufacturer_part_numbers !== null) {
                    var pns = r.data.manufacturer_part_numbers;
                    if (~pns.indexOf(', ')){
                        var clipboard_text = pns.split(', ')[0];
                    } else {
                        var clipboard_text = pns;
                    }

                    $row.find('.m-pns span').attr('data-clipboard-text' ,clipboard_text);
                    $row.find('.m-pns strong').removeClass('hidden');
                    $row.find('.m-pns span').text(pns);
                } else {
                    $row.find('.m-pns-container').addClass('hidden');
                    $row.find('.m-pns strong').addClass('hidden');
                    $row.find('.m-pns span').html('');
                }


                if (r.data.st_date_to !== null) {
                    var currDate = new Date();
                    var dateAr = (r.data.st_date_to).split('-');
                    var strDateTo = dateAr[2] + '-' + dateAr[1] + '-' + dateAr[0];
                    var dateTo = new Date(Date.parse(strDateTo));
                    var timeDiff = Math.abs(currDate.getTime() - dateTo.getTime());
                    var dDiff = Math.ceil(timeDiff / (1000 * 3600 * 24));
                    $row.find('.st-date-to span').html(dDiff);
                    $row.find('.st-date-to').attr('data-original-title'
                        ,'Numar de zile de la data ultimei recalculari ale statisticilor produslui. <br>'
                        + 'Data ultimei recalculari: ' + r.data.st_date_to);
                } else {
                    $row.find('.st-date-to span').html('-');
                    $row.find('.st-date-to td').addClass('has-error');
                    $row.find('.st-date-to').attr('data-original-title'
                        ,'Recalcularea statisticilor produslui nu a avut loc.');
                }


                $row.find('.st-ult-stoc span').html((r.data.lstock !== null)
                    ? r.data.lstock
                    // ? $.datepicker.formatDate('dd.mm.y', new Date(r.data.lstock))
                    : '---');
                if (r.data.lstock !== null && r.data.lstock_diff) {
                    $row.find('.st-ult-stoc').addClass('bg-danger-imp')
                        .prop('title', 'Produsul a intrat in stoc acum mai mult de 6 luni')
                        .prop('data-toggle', 'tooltip')
                        .tooltip();
                }

                highlightStatisticsData($row);

                let passed_callback = null;
                if (typeof callback === 'function' && callback) {
                    passed_callback = callback;
                }

                suppliersDataTemplate($container, r.data.suppliers_data, passed_callback);
                entriesDataTemplate($row, r.data.entries_data);
            }
        });
    }

    /**
     * Apply CSS styles to certain fields depending on their values
     *
     * @param $row - the product row
     */
    let highlightStatisticsData = function($row) {
        let $qty_av_gsmnet = $row.find('._pool_gsmnet').find('._qty_av');
        if (parseInt($qty_av_gsmnet.text()) === 0) {$qty_av_gsmnet.addClass('danger') }

        let $ret_gsmnet = $row.find('._pool_gsmnet').find('._ret');
        if (parseInt($ret_gsmnet.text()) > 0) {$ret_gsmnet.removeClass('text-faded').addClass('danger') }

        let $qty_av_backorder = $row.find('._pool_backorder').find('._qty_av');
        if (parseInt($qty_av_backorder.text()) === 0) { $qty_av_backorder.addClass('text-faded') }

        let $qty_av_mbpde = $row.find('._pool_mbpde').find('._qty_av');
        if (parseInt($qty_av_mbpde.text()) === 0) { $qty_av_mbpde.addClass('text-faded') }


        let stats_lines = ['._all', '._90', '._30'];
        $.each(stats_lines, function(index, value) {
            let $element = $row.find(value);
            if (parseFloat($element.find('.avg').text()) === 0) {
                $element.find('.avg').addClass('danger');
            }
            if (parseFloat($element.find('.auto').text()) === 0) {
                $element.find('.auto').addClass('danger');
            }
            if (parseFloat($element.find('.nec').text()) <= 0) {
                $element.find('.nec').addClass('text-faded');
            }
        });
        
        let s_lines = ['._sold', '._ret',];
        $.each(s_lines, function(index, value) {
            let $itms = $row.find(value);

            $itms.each(function() {
                let $itm  = $(this);
                let $v = parseInt($itm.find('span').text());
                if ($v === 0) { $itm.addClass('text-faded') }
            });
        });


        let order_lines = ['._inbound_o', '._preorders', '._purchase_o'];
        $.each(order_lines, function(index, value) {
            let $e = $row.find(value);
            let $v = parseInt($e.find('span').text());
            if ($v === 0) { $e.addClass('text-faded') }
            if ($v > 0) { $e.removeClass('text-faded').addClass('info') }
        });
    };

    /**
     * Add a separator line in the suppliers data table
     *
     * @param $container
     */
    let appendSeparator = function($container) {
        $container.append('<tr class="supplier-row separator"><td colspan="10"></td></tr>');
    };

    /**
     * Convenience function that adds leading zeros to date/time elements
     * (when necessary)
     *
     * @param value             - date/time element
     * @returns string          - leading zero-ed date/time element
     */
    function addLeadingZero(value) {
        if(value < 10){
            return '0' + value;
        }
        return value;
    }

    /**
     * Convenience function that formats a js Date variable into
     * readable form -> dd-mm-yyyy hh:mm:ss
     *
     * @param date              - js Date object
     * @param show_time         - show time or not
     * @param date_separator    - character that separates date elements
     * @param time_separator    - character that separates time elements
     * @returns string          - formatted date as string
     */
    function formatDate(date, show_time = true, date_separator = '-', time_separator = ':') {
        let returned = addLeadingZero(date.getDate());
        returned += date_separator + addLeadingZero(date.getMonth() + 1);
        returned += date_separator + date.getFullYear();
        if(show_time) {
            returned += ' ';
            returned += addLeadingZero(date.getHours());
            returned += time_separator + addLeadingZero(date.getMinutes());
            returned += time_separator + addLeadingZero(date.getSeconds());
        }
        return returned;
    }

    /**
     * Function that generates HTML for the suppliers data in the table
     *
     * @param $container - the TBODY element for suppliers data
     * @param data - data received from AJAX call
     * @param callback - function to be executed after the html is appended
     */
    function suppliersDataTemplate($container, data, callback) {
        $container.html(data);
        if (typeof callback === 'function' && callback) {
            setTimeout(function() {
                // set a timeout to give time for the html to append and display
                callback();
            }, 500);
        }
    }

    /**
     * Function that generates HTML for the entries data in the table
     *
     * @param $row      - the TR element
     * @param data      - data received from AJAX call
     */
    function entriesDataTemplate($row, data) {
        let $container = $row.find('.entries table');
        let html  = '';
        $.each(data, function(index, value) {
            let entry_price_with_vat = value.EntryProduct.price * (1+(vat_percent/100));
            let entry_price_currency = (value.EntryProduct.euro_price !== 0) ? value.EntryProduct.euro_price : 0;
            html += '<tr class="entry-row">';
            html += '<td title="' + value.Supplier.name + '" style="width: 40px;">' + value.Supplier.abbreviation + '</td>';
            html += '<td title="Data intrare" style="width: 60px;">' + value[0].entry_data + '</td>';
            html += '<td title="Pret intrare" style="width: 55px;">' + parseFloat(value.EntryProduct.price).toFixed(2) + '</td>';
            html += '<td title="Pret intrare + TVA" style="width: 55px;" class="text-faded">' + parseFloat((Math.round(entry_price_with_vat * 100) / 100)).toFixed(2) + '</td>';
            html += '<td title="Bucati intrare" style="width: 35px;">' + value[0].total + '</td>';
            html += '<td title="Pret intrare valuta" style="width: 65px;">' + parseFloat((Math.round(entry_price_currency * 100) / 100)).toFixed(2);
            if (value.Currency.acronym !== null) {
                html += ' <i class="fa fa-' + value.Currency.acronym.toLowerCase() + '"></i>';
            }
            html += '</td>';
            html += '<td title="Bucati ramase" style="width: 35px;" ' + ((parseInt(value[0].remaining) === 0) ? 'class="text-faded"' : '') + '>' + value[0].remaining + '</td>';
            html += '</tr>';
        });
        $container.html(html);
    }

    /**
     * Triggers for adding a preorder product when pressing ENTER on price and quantity field
     */
    $('td.suppliers')
        .on('keyup', 'input[name="new_price"]', function(e) {
            if (e.keyCode === 13) {
                e.preventDefault();
                addBlankEntry($(this));
            }
        })
        .on('keyup', 'input[name="new_amount"]', function(e) {
            if (e.keyCode === 13) {
                e.preventDefault();
                addBlankEntry($(this));
            }
        });

    /**
     * Function that adds a preorder product for suppliers with no part numbers
     *
     * @param $element - the price or quantity field
     * @returns {boolean}
     */
    function addBlankEntry($element) {
        let $row = $element.parents('tr.table-row');
        let $trSupplier = $element.parents('tr.supplier-row');
        let $priceField = $trSupplier.find('input[name="new_price"]');
        let $amountField = $trSupplier.find('input[name="new_amount"]');
        let price = parseFloat($priceField.val());
        let amount = parseFloat($amountField.val());
        let supplier_id = $trSupplier.attr('data-supplier');
        let product_id = parseInt($row.data('pid'));
        if (isNaN(price) || isNaN(amount)) {
            toastr_putMessage('Pretul si cantitatea sunt obligatorii', 'error');
            return false;
        }

        let dataToSend = {
            'price': price,
            'amount': amount,
            'supplier_id': supplier_id,
            'product_id': product_id
        };
        if(preorder_id !== null) {
            dataToSend['preorder_id'] = preorder_id;
        }

        $.ajax({
            url: '/suppliers/purchase_orders/addBlankProduct.json',
            method: 'POST',
            data: dataToSend
        })
        .done(function (msg) {
            toastr_putMessage(msg.message, msg.status);
            showAsyncSuppliersData($row, function() {
                // focus on the first empty amount field
                let found = false;
                $row.find('td.suppliers table tbody tr.supplier-row input.preorder-amount').each(function(index, element) {
                    let $element = $(element);
                    if (!found && $element.val() === '') {
                        $element.focus();
                        found = true;
                    }
                });
            });
            updateTotals(parseInt(preorder_id), parseInt(supplier_id));
        });
    }

    /**
     * Insert or update a product in the current preorder
     */
    $("tr.table-row")
        .on("keyup", "input.preorder-amount", function(e) {
            if (e.keyCode === 13) {
                e.preventDefault();
                let $this = $(this);
                let preorder_value = $this.val();
                let preorder_amount = parseInt(preorder_value);
                if (isNaN(preorder_amount)) {
                    preorder_amount = 0;
                }
                let $trParent = $this.parents('tr.table-row');
                let $trSupplier = $this.parents('tr.supplier-row');
                let product_id = $trParent.attr('data-pid');
                let supplier_id = $trSupplier.attr('data-supplier');
                let price = parseFloat($trSupplier.find('td.so-price').text());
                let sp_number = $trSupplier.find('a.sp-number').text();
                let has_so_data = ($trSupplier.find('td.edit').attr('data-so-id') !== '');
                let has_pp_data = ($trSupplier.find('td.edit').attr('data-pp-id') !== '');
                let is_blank_entry = $this.hasClass('is-blank');

                if (isNaN(price) && has_so_data) {
                    price = parseFloat($trSupplier.find('input[name="new_price"]').val());
                }

                // if there is no supplier offer and no preorder product data, open the edit modal form
                if (!has_so_data && !has_pp_data) {
                    let trigger = $trSupplier.find('.trigger-edit-so');
                    let quantity = parseInt($trSupplier.find('input.preorder-amount').val());
                    price = parseFloat($trSupplier.find('input[name="new_price"]').val());
                    supplierOfferEditModal(trigger, function() {
                        $("#edit-so-modal").find('#price').val(price)
                        $("#edit-so-modal").find('#quantity').val(quantity);
                        $("#edit-so-modal").modal('show');
                    });
                    return false;
                }

                // auto-save quantity data
                if(preorder_page && !is_blank_entry) {
                    if (isNaN(price)) {
                        toastr_putMessage('Pretul si cantitatea sunt obligatorii', 'error');
                        return false;
                    }
                    let data_to_send = {
                        preorder_id: parseInt(preorder_id),
                        product_id: parseInt(product_id),
                        supplier_id: parseInt(supplier_id),
                        amount: preorder_amount,
                        supplier_pn: sp_number,
                        price: price
                    };
                    $.ajax({
                        url: '/suppliers/purchase_orders/addProduct.json',
                        method: 'POST',
                        data: data_to_send
                    })
                    .done(function (msg) {
                        toastr_putMessage(msg.message, msg.status);
                        updateTotals(parseInt(preorder_id), parseInt(supplier_id));
                        if (preorder_amount === 0) {
                            showAsyncSuppliersData($trParent);
                        }
                        focusOnNextInput_row($this, 'input.preorder-amount', 'next');
                    });
                }
                return false;
            }
        })
        .on('keydown', 'input.preorder-amount', function(e) {
            // arrow keys detection only works on 'keydown' event (not 'keypress')
            focusOnNextInput($(this), e, 'input.preorder-amount');
        })
        .on('blur', 'input.preorder-amount', function() {
            $(this).removeClass('focused');
        })
        .on('focus', 'input.preorder-amount', function() {
            $(this).addClass('focused');
        });

    /**
     * Function that reads the directional arrow keys and makes
     * the corresponding focus action
     *
     * @param $current - the current element (input)
     * @param event - event for keycode
     * @param searchClass - field class
     */
    let focusOnNextInput = function($current, event, searchClass) {
        if(event.keyCode === 38) {    // UP ARROW
            focusOnNextInput_row($current, searchClass, 'prev');
            event.preventDefault();
        } else if(event.keyCode === 40) {    // DOWN ARROW
            focusOnNextInput_row($current, searchClass, 'next');
            event.preventDefault();
        }
    };

    /**
     * Function that places the cursor (focus) on the same input for the next/previous row
     *
     * @param $current - current input field
     * @param searchClass - unique class for each field
     * @param type - 'next' | 'prev' for focus direction
     */
    let focusOnNextInput_row = function($current, searchClass, type) {
        let $trProduct = $current.parents('tr.table-row');
        let $trSupplier = $current.parents('tr.supplier-row');

        if(type === 'next') {
            // search for more fields on the same product row
            let $try = $trSupplier.nextAll('tr.supplier-row').not('.separator').first();
            if ($try.length) {
                $try.find(searchClass).focus();
            } else {
                // search for more fields on the previous product row
                let $try_row = $trProduct.next();
                if ($try_row.length) {
                    $try_row.find('td.suppliers table tbody').find('tr.supplier-row:first-child').find(searchClass).focus();
                }
            }
        } else if(type === 'prev') {
            // search for more fields on the same product row
            let $try = $trSupplier.prevAll('tr.supplier-row').not('.separator').first();
            if ($try.length) {
                $try.find(searchClass).focus();
            } else {
                // search for more fields on the previous product row
                let $try_row = $trProduct.prev();
                if ($try_row.length) {
                    $try_row.find('td.suppliers table tbody').find('tr.supplier-row:last-child').find(searchClass).focus();
                }
            }
        }
    };

    /**
     * Re-compute total product values for a supplier in a preorder
     *
     * @param preorder_id - the preorder
     * @param supplier_id - the supplier
     */
    let updateTotals = function(preorder_id, supplier_id) {
        let $target = $('.suppliers-preorder-data .supplier-totals-row[data-supplier="'+supplier_id+'"]');
        let $total = $target.find('strong.total');
        $total.html(spinning_icon);
        $.ajax({
            url: '/suppliers/purchase_orders/getPreorderSupplierTotal/'+preorder_id+'/'+supplier_id+'.json',
            method: 'POST'
        })
        .done(function (msg) {
            $total.html(parseFloat(msg.data));
        });
    };

    /**
     * Datepickers initialization
     */
    $("#fstock1").datetimepicker({
        timepicker: false,
        dayOfWeekStart: 1,
        closeOnDateSelect: true,
        lang: 'ro',
        format: 'Y/m/d',
        onShow: function (ct) {
            this.setOptions({
                maxDate: $('#fstock2').val() ? $('#fstock2').val() : false
            })
        },
        onSelectDate: function (ct, $i) {
            $('#fstock2').focus()
        }
    });

    $("#fstock2").datetimepicker({
        timepicker: false,
        dayOfWeekStart: 1,
        closeOnDateSelect: true,
        lang: 'ro',
        format: 'Y/m/d',
        onShow: function (ct) {
            this.setOptions({
                minDate: $('#fstock1').val() ? $('#fstock1').val() : false
            })
        }
    });

    /**
     * Trigger for showing preorder generation modal
     */
    $('a.btn-generate-preorder').on('click', function() {
        $('#preorder_generate').modal('show');
    });

    /**
     * Trigger for showing preorder selection modal
     */
    $('a.btn-choose-preorder').on('click', function() {
        $('#preorder_choose').modal('show');
    });

    /**
     * Trigger stuff when opening modal
     */
    $('#preorder_generate').on('show.bs.modal', function() {
        $('select#modal-1-suppliers').multipleSelect({
            filter: true
        });
    });

    /**
     * Change preorder from the select list
     */
    $('#use-preorder').on('click', function(e) {
        e.preventDefault();
        let preorder = $("#modal-1-preorders").val();
        if(preorder !== '') {
            window.document.location = '/suppliers/purchase_orders/acquisition/' + preorder;
        }
        return false;
    });

    /**
     * Trigger for showing preorder edit modal
     */
    $('a.btn-preorder-edit').on('click', function() {
        $('#preorder_edit').modal('show');
    });

    /**
     * Trigger stuff when opening modal
     */
    $('#preorder_edit').on('show.bs.modal', function() {
        $('select#modal-2-suppliers').multipleSelect({
            filter: true
        });
    });

    /**
     * Change product status
     */
    $(".status").on('click', 'a.change-status', function() {
        if(!confirm('Esti sigur ca vrei sa modifici starea produsului?')) {
            return false;
        }
    });

    /**
     * Function that displays availability icon
     *
     * @param av
     * @returns {string}
     */
    function iconAvailability(av) {
        let html = '';
        switch(av['id']) {
            case null:
                html += "<i class='fa fa-exclamation-triangle' style='color:salmon' title='Disponibilitate nesetata'></i>";
                break;
            case '1':
                html += "<i class='fa fa-rotate-270 fa-battery-full' style='color:"+av['hex_color']+"' title='"+av['name']+"'></i>";
                break;
            case '3':
                html += "<i class='fa fa-rotate-270 fa-battery-half' style='color:"+av['hex_color']+"' title='"+av['name']+"'></i>";
                break;
            case '4':
                html += "<i class='fa fa-rotate-270 fa-battery-empty' style='color:"+av['hex_color']+"' title='"+av['name']+"'></i>";
                break;
        }
        return html;
    }

    /**
     * Toggle SupplierOffer edit modal
     */
    $('td.suppliers').on('click', 'a.trigger-edit-so', function () {
        let trigger = $(this);
        supplierOfferEditModal(trigger);
    });

    /**
     * Fetch modal form
     * @param trigger
     * @param callback
     */
    function supplierOfferEditModal(trigger, callback) {
        let supplier_offer_id = trigger.parents('td').first().attr('data-so-id');
        let product_id = trigger.parents('tr.table-row').attr('data-pid');
        let supplier_id = trigger.parents('tr.supplier-row').attr('data-supplier');
        let part_number = trigger.parents('tr.supplier-row').find('a.sp-number').text();
        let modal = $('#edit-so-modal');
        let modal_content = modal.find('.modal-body');
        modal_content.html(spinning_icon);

        let url = '/suppliers/purchase_orders/getSupplierOfferEditForm.json';
        let dataToSend = {
            'preorder_id': preorder_id,
            'product_id': product_id,
            'supplier_id': supplier_id,
            'part_number': part_number
        };
        if(supplier_offer_id !== 'null') {
            dataToSend['supplier_offer_id'] = supplier_offer_id;
        }

        $.ajax({
            type: "POST",
            url: url,
            dataType: 'html',
            data: dataToSend,
            success: function(r) {
                modal_content.html(r);
                if(typeof callback === 'function') {
                    callback.call();
                }
            }
        });
    }

    /**
     * Add `popover` custom class
     */
    $('[data-toggle="popover-suppliers"]').popover({
        template: '<div class="popover popover-suppliers-list"><div class="arrow"></div><div class="popover-inner"><h3 class="popover-title"></h3><div class="popover-content"><p></p></div></div></div>'
    });

    /**
     * Trigger for opening Supplier part numbers administration modal
     */
    $(".link-supplier-part-number").on('click', function() {
        console.log('click');
        let $this = $(this);
        let $row = $this.parents('tr.table-row');
        let pid = $row.attr('data-pid');
        let name = $this.attr('data-name');
        console.log('before');
        openPartNumbersModal(pid, name);
        console.log('after');
        return false;
    });

    /**
     * Function that opens Supplier part numbers administration modal
     */
    function openPartNumbersModal(pid, name) {
        let $modal = $('#supplier-codes');
        let $modal_body = $modal.find('.modal-body');
        let dataToSend = {};
        if(preorder_id !== null) {
            dataToSend['preorder_id'] = preorder_id;
        }
        $modal_body.html('<i class="fa fa-spin fa-circle-o-notch"></i>');
        // ATTACH data-pid ATTRIBUTE TO MODAL
        $modal
            .attr('data-pid', pid)
            .modal('show');
        $.ajax({
            type: 'GET',
            url: '/suppliers/supplier_products/productPartNumbers/' + pid,
            data: dataToSend,
            success: function(r) {
                $modal_body.html(r);
                $('#modalSupplierCodes_title').html(name);
                $('.edit-sp-part-number').editable({
                    success: function(response, newValue) {
                        updatePartNumberChanges($modal);
                    }
                });
            }
        });
    }

    /**
     * Function that updates suppliers data in the table
     * and reopens the modal
     */
    function updatePartNumberChanges($modal) {
        let pid = $modal.attr('data-pid');
        let $row = $('tr[data-pid=' + pid + ']');
        let name = $modal.find('#modalSupplierCodes_title').html();
        showAsyncSuppliersData($row);
        openPartNumbersModal(pid, name);
    }

    /**
     * Supplier part numbers administration modal actions hijacking
     * ---------------------------------------------------------------------
     */
    // ADD PART NUMBER
    $("#supplier-codes").on('click', 'button.btn-add-pn', function(e) {
        e.preventDefault();
        let $modal = $('#supplier-codes');
        let $this = $(this);
        let $form = $this.parents('form');
        let action = $form.attr('action') + '.json';
        let data = $form.serializeArray();
        $.ajax({
            type: 'POST',
            url: action,
            data: data
        })
            .done(function(r) {
                toastr_putMessage(r.message, r.status, 2000);
                if(r.status === 'success') {
                    updatePartNumberChanges($modal);
                }
            });
        return false;
    });
    // DELETE PART NUMBER
    $("#supplier-codes").on('click', 'a.btn-trigger-delete', function(e) {
        e.preventDefault();
        let $anchor = $(this);
        let url = $anchor.attr('href') + '.json';
        let $modal = $('#supplier-codes');
        $.ajax({
            type: 'GET',
            url: url
        })
            .done(function(r) {
                toastr_putMessage(r.message, r.status, 2000);
                if(r.status === 'success') {
                    updatePartNumberChanges($modal);
                }
            });
        return false;
    });
    // DETACH data-pid ATTRIBUTE FROM MODAL
    $("#supplier-codes").on('hide.bs.modal', function() {
        $(this).removeAttr('data-pid');
    });
    /**-------------------------------------------------------------------*/

    /**
     * Change page from pages <select> element
     */
    $(".trigger_select_page").on("change", function() {
        let crt_page = parseInt($(this).data('crt-page'));
        let new_page = parseInt($(this).val());
        if(window.location.href.indexOf("page:") !== -1) {
            window.location.href = window.location.href.replace('page:' + crt_page, 'page:' + new_page);
        } else {
            if(window.location.href.indexOf("?") !== -1) {
                let url = window.location.href.split('?');
                window.location.href = url[0] + '/page:' + new_page + '?' + url[1];
            } else {
                window.location.href = window.location.href + '/page:' + new_page;
            }
        }
    });

    /**
     * Fields that may not have negative values
     */
    $(".min-zero").on("blur", function() {
        let $this = $(this);
        if(parseInt($this.val()) < 0) {
            $this.val(0);
        }
    });

    /**
     * Get popover html for product pieces in inbound orders or in unfinished preorders or in unfinished purchase orders
     */
    $(".table-row ._purchase_o,._preorders,._inbound_o").on("click", function() {
        let $this = $(this);
        if ($this.hasClass('_preorders')) {
            var detail_type = 'preorders'
        } else if ($this.hasClass('_purchase_o')) {
            var detail_type = 'orders'
        } else if ($this.hasClass('_inbound_o')) {
            var detail_type = 'reception'
        }
        if(preorder_id == null && detail_type !== 'reception') {return false};
        let $row = $this.parents(".table-row");
        let product_id = $row.data('pid');
        let url = '/suppliers/purchase_orders/getPopoverDetails/' + detail_type + '/' + product_id + '.json';
        $.ajax({
            type: 'GET',
            url: url,
            dataType: 'html',
            data: {
                preorder_id: preorder_id
            }
        })
        .done(function (r) {
            if (r.trim() !== '') {
                $this
                    .popover({
                        content: r,
                        placement: 'top',
                        html: true
                    })
                    .popover('show');
            }
        });
    });

    /**
     * Close all popovers when clicking outside of them
     */
    $('body').on('click', function (e) {
        let $target = $(e.target).parent('tr');
        if ($target.data('toggle') !== 'popover' && $target.parents('.popover.in').length === 0) {
            $('[data-toggle="popover"]').popover('destroy');
        }
    });

    /**
     * Trigger for product status change (checkbox)
     */
    $('.checkbox').on('click', 'input', function() {
        let $this = $(this);
        let $row = $this.parents(".table-row");
        let preorder_product_id = $row.data('ppnid');
        let url = '/suppliers/purchase_orders/updateToDeactivate/' + preorder_product_id + '.json';
        $.ajax({
            type: 'GET',
            url: url
        })
        .done(function (r) {
            toastr_putMessage(r.message, r.status, 3000);
        });
    });

    /**
     * Add modal data into preorder using ajax
     */
    $("#edit-so-modal").on('click', "#submit-edit-supplier-offer", function() {
        let $form = $("#edit-so-modal").find('form');
        let $price_field = $("#edit-so-modal").find("#price");
        let $quantity_field = $("#edit-so-modal").find("#quantity");

        if($price_field.val() === "" || $quantity_field.val() === "") {
            alert('Pretul si cantitatea sunt campuri obligatorii!');
            return false;
        }

        $.ajax({
            type: 'POST',
            url: $form.prop('action')+'.json',
            data: {
                'form_data': $form.serialize()
            }
        })
        .done(function (r) {
            $("#edit-so-modal").modal('hide');
            toastr_putMessage(r.message, r.status, 1000);
            let $row = $('tr.table-row[data-pid=' + r.data.product_id + ']');
            showAsyncSuppliersData($row);
            updateTotals(r.data.preorder_id, r.data.supplier);
        });
        return false;
    });

    /**
     * Triggered when the supplier rate has been changed
     * Used to recalculate row total with the new rate
     */
    $("a.supplier-rate").on("save", function(e, params) {
        let $this = $(this);
        let $total_field = $($this.parents('.row').get(0)).find('strong.total');
        let total = parseInt($total_field.attr('data-total-lei'));
        if(!isNaN(total)) {
            let new_total = (total / params.newValue).toFixed(2);
            $total_field.text(new_total);
        }
    });

    $('a.m-refresh-url').on('click', function(e) {
        e.preventDefault();
        let $row = $(this).parents('tr.table-row');
        showAsyncSuppliersData($row);
        return false;
    });
});

/**
 * Start the necessary update when entering the page
 *
 * @param preorder_id - the preorder
 */
function startNecessaryUpdate(preorder_id) {
    $.ajax({
        url: '/suppliers/purchase_orders/necessaryUpdate/'+preorder_id+'.json',
        method: 'GET'
    })
    .done(function (msg) {
        toastr_putMessage(msg.message, msg.status);
    });
}

/**
 * Show the table with all suppliers totals
 *
 * @param preorder_id - the preorder
 */
function showPreorderTotals(preorder_id) {
    $.ajax({
        url: '/suppliers/purchase_orders/getPreorderTotals/'+preorder_id+'.json',
        method: 'GET',
        dataType: 'html'
    })
    .done(function(msg) {
        $('.suppliers-preorder-data > .panel-body > .row').html(msg);
    });

}

//@copy to clipboard
var clipboard = new ClipboardJS('.x-clipboard');
clipboard.on('success', function(e) {
    toastr.success('<i>' + e.text + '</i>', 'Copiat in clipboard!');
    e.clearSelection();
});