Number.prototype.p = function() {
    return Math.round(this.valueOf() * 100) / 100;
};

let appendLeadingZeroes = function(n) {
    if (n <= 9) {
        return "0" + n;
    }
    return n;
};

let dateToString = function(date) {
    return date.getFullYear() + '-'
        + appendLeadingZeroes(date.getMonth() + 1) + '-'
        + appendLeadingZeroes(date.getDate());
};

$(function() {

    /* BGN enable side menu */
    $('#side-menu').metisMenu();
    /* END enable side menu */

    /* BGN  enable textarea replacement */
    if(typeof replace_txt !== 'undefined') {
        $(replace_txt).summernote({
            minHeight: 200
        });
    }
    /* END enable textarea replacement */

    $('.btn').tooltip();

    $(document).on('click touchstart', '.dataTable tbody tr', function() {
        myNewHref = document.location.href;

        if($(this).parent().data('location') != undefined) {
            myNewHref = $(this).parent().data('location');
        }

        if($(this).data('href_id') != undefined) {
            document.location.href = myNewHref + $(this).data('href_id');
            return;
        }

        document.location.href = myNewHref + $('td:first', $(this)).html();
    });

    var touchHandled = false;
    $(document).on('touchend click', '.toggle-pane-body', function (e) {
        e.stopImmediatePropagation();
        if (e.type == "touchend") {
            touchHandled = true;
            togglePanel($(this));
        } else if(e.type == "click" && !touchHandled) {
            togglePanel($(this));
        } else {
            touchHandled = false;
        }
    });

    function togglePanel(ele){
        $(ele).parent().next().slideToggle();
        $(ele).children('.fa')
            .toggleClass('fa-toggle-up')
            .toggleClass('fa-toggle-down');
        return false;
    }

    $('.add_datepicker').datepicker({
        changeMonth: true,
        changeYear: true,
        dateFormat: "yy-mm-dd"
    });

    $('.add_datetimepicker').datetimepicker({
        format: "Y-m-d H:i",
        lang: 'ro',
        step: 15
    });

    $('.add_datetimepickerdateonly').datetimepicker({
        format: "Y-m-d",
        lang: 'ro',
        step: 15,
        timepicker:false
    });

    if ($('.fancourier_options').length > 0) {
        $('#issue_awb').change(function(){
            if ($(this).prop('checked')){
                $('.fancourier_options').not('#issue_awb').prop('checked', false).prop('disabled', false);
                $('.to-enable').prop('disabled', false);
            } else {
                $('.fancourier_options').not('#issue_awb').prop('checked', false).prop('disabled', true);
            }
        })

        $('.fancourier_options').not('#issue_awb').on('change', function (e) {
            if ($('.if_issue_awb:checked').length > 3) {
                $(this).prop('checked', false);
                alert("Selectati maxim 3 optiuni");
            }
        });
    }

    if ($('.dhl_options').length > 0) {
        $('#issue_awb').change(function() {
            if ($(this).prop('checked')) {
                $('.dhl_options').not('#issue_awb').prop('checked', false).prop('disabled', false);
                $('.to-enable').prop('disabled', false);
            } else {
                $('.dhl_options').not('#issue_awb').prop('checked', false).prop('disabled', true);
            }
        })
    }

    if ($('.emag_courier_options').length > 0) {
        $('#issue_awb').change(function() {
            if ($(this).prop('checked')) {
                $('.emag_courier_options').not('#issue_awb').prop('checked', false).prop('disabled', false);
                $('.to-enable').prop('disabled', false);
            } else {
                $('.emag_courier_options').not('#issue_awb').prop('checked', false).prop('disabled', true);
            }
        })
    }

    /**
     * Change page from pages <select> element inside advanced pagination wrapper
     */
    $(".pagination-wrapper").on("change", ".trigger_select_page", function() {
        var crt_page = parseInt($(this).data('crt-page'));
        var new_page = parseInt($(this).val());
        if(window.location.href.indexOf("page:") !== -1) {
            window.location.href = window.location.href.replace('page:' + crt_page, 'page:' + new_page);
        } else {
            if(window.location.href.indexOf("?") !== -1) {
                var url = window.location.href.split('?');
                window.location.href = url[0] + '/page:' + new_page + '?' + url[1];
            } else {
                window.location.href = window.location.href + '/page:' + new_page;
            }
        }
    });

});

//Loads the correct sidebar on window load,
//collapses the sidebar on window resize.
// Sets the min-height of #page-wrapper to window size
$(function() {
    $(window).bind("load resize", function() {
        topOffset = 50;
        width = (this.window.innerWidth > 0) ? this.window.innerWidth : this.screen.width;
        if (width < 768) {
            $('div.navbar-collapse').addClass('collapse')
            topOffset = 100; // 2-row-menu
        } else {
            $('div.navbar-collapse').removeClass('collapse')
        }

        height = (this.window.innerHeight > 0) ? this.window.innerHeight : this.screen.height;
        height = height - topOffset;
        if (height < 1) height = 1;
        if (height > topOffset) {
            $("#page-wrapper").css("min-height", (height) + "px");
        }
    })
})

function NewWindow(mypage, myname, w, h, scroll)
{
    var win = null;
    LeftPosition = (screen.width) ? (screen.width-w)/2 : 0;
    TopPosition = (screen.height) ? (screen.height-h)/2 : 0;
    settings = "height="+h+",width="+w+",top="+TopPosition+",left='+LeftPosition+",scrollbars='+scroll+",resizable';
    win = window.open(mypage, myname, settings)
}

function strip_tags(input, allowed) {
    allowed = (((allowed || '') + '')
        .toLowerCase()
        .match(/<[a-z][a-z0-9]*>/g) || [])
        .join(''); // making sure the allowed arg is a string containing only tags in lowercase (<a><b><c>)
    var tags = /<\/?([a-z][a-z0-9]*)\b[^>]*>/gi,
        commentsAndPhpTags = /<!--[\s\S]*?-->|<\?(?:php)?[\s\S]*?\?>/gi;
    return input.replace(commentsAndPhpTags, '')
        .replace(tags, function($0, $1) {
            return allowed.indexOf('<' + $1.toLowerCase() + '>') > -1 ? $0 : '';
        });
}


/** *************** NIR section *************** **/

function setNIRValidationAlert(item, alert) {
    if (alert == '') {
        $('.alert-dismissable').each(function() {
            if ($(this).prop('id') != 'nir-validation-issues' && $(this).prop('id') != 'validation_in_progress_notif')
                $(this).closest('.row').remove();
        });
        $('#nir-validation-issues span').html('');
        if (!$('#nir-validation-issues').hasClass('hidden')) {
            $('#nir-validation-issues').addClass('hidden');
        }
    } else {
        alert = item + ' - ' + alert;
        var prev_alert = $('#nir-validation-issues span').html();
        $('#nir-validation-issues span').html(prev_alert + '<br/>' + alert);
        $('#nir-validation-issues').removeClass('hidden');
        window.scrollTo(0, 0);
    }
}

function showNIRvalidationInProgress(on) {
    return;
//	if (on == 'show') {
//		$('#validation_in_progress_notif').fadeIn(500);
//	} else {
//
//		if (on == 'force_hide') {
//			$('#validation_in_progress_notif').removeClass('ajax_in_progress');
//		}
//
//		if (!$('#validation_in_progress_notif').hasClass('ajax_in_progress')) {
//			$('#validation_in_progress_notif').fadeOut(500);
//		}
//	}
}

function validateNIRForm() {

    //showNIRvalidationInProgress('show');

    setNIRValidationAlert('','');

    var ret = true;
    $('.doubable .has-error').each(function() {
        $(this).removeClass('has-error');
    });

    var duplicate = 0;
    $('.product_id_ai:hidden').each(function() {
        if ($(this).val() != '') {
            var product_id = $(this).val();
            if ($('.product_id_ai_' + product_id).length > 1) {
                duplicate = $(this).closest('.product_additional_info').find('.product_pn_ai').val();
                $('.product_id_ai_' + product_id).each (function() {
                    $(this).closest('.product_additional_info').parent().addClass('has-error');
                });
                ret = true;
            } else {
                $('.product_id_ai_' + product_id).closest('.product_additional_info').parent().addClass('has-success');
            }
        }
    });

    if (duplicate) {
        setNIRValidationAlert(duplicate, 'produs care se repeta - se poate salva (se vor cumula cantitatile cu acelasi pret)');
    }

    $('.doubable .productId').each(function() {
        if(!$(this).val()) {
            $(this).closest('.doubable').find('.product-selector').removeClass('has-success').addClass('has-error');
            var item = $(this).closest('.doubable').find('.product_additional_info').find('.product_pn_ai').val();
            setNIRValidationAlert(item, 'trebuie selectat un produs');
            ret = false;
        }
    });

    var product_ids = [];
    $('.doubable .pan_id_ai').each(function() {
        if ($(this).val() != '')
            product_ids.push($(this).val());
    });
    if (product_ids.length) {
        $.ajax({
            async: false,
            type: "POST",
            url: '/catalog/products/checkInactiveProducts.json',
            data: {product_ids: product_ids},
            dataType: 'json',
        })
            .done(function(returnObj) {
                //if (returnObj.hasOwnPropery(inactive_products)) // @todo
                var c = 0;
                console.log(returnObj);
                for (var p in returnObj.inactive_products) {
                    setNIRValidationAlert(returnObj.inactive_products[p]['part_number'], 'este inactiv');
                    $('.doubable .pan_id_ai_' + p).closest('.product-selector').removeClass('has-success').addClass('has-error');
                    c++;
                }
                if (c > 0) {
                    ret = false;
                }
            })
            .fail(function() {
                showNIRvalidationInProgress('force_hide');
                alert('Nu s-a putut verifica daca exista produsele inactive');
                setNIRValidationAlert('', 'Nu s-a putut verifica daca exista produsele inactive');
                ret = false;
            });
    }

    $('.doubable .entry-product-price').each(function() {
        var n = $(this).val();
        var item = $(this).closest('.doubable').find('.product_additional_info').find('.product_pn_ai').val();
        if(isNaN(parseFloat(n)) || !isFinite(n) || parseFloat(n) <= 0) {
            $(this).parent().removeClass('has-success').addClass('has-error');
            setNIRValidationAlert(item, 'pret incorect');
            ret = false;
        } else {
            var str_n = n.toString();
            var str_n_arr = str_n.split('.');
            if (str_n_arr.length > 1 && str_n_arr[1].length > 2)  {
                $(this).parent().removeClass('has-success').addClass('has-error');
                setNIRValidationAlert(item, 'prea multe decimale in pret');
                ret = false;
            } else {
                $(this).parent().addClass('has-success').removeClass('has-error');
                $(this).val(parseFloat($(this).val()));
            }
        }
    });

    $('.doubable .entry-product-quantity:visible').each(function() {
        var n = $(this).val();
        if(isNaN(parseInt(n)) || !isFinite(n) || parseInt(n) == 0 || parseFloat(n) != parseInt(n)) {
            $(this).parent().removeClass('has-success').addClass('has-error');
            var item = $(this).closest('.doubable').find('.product_additional_info').find('.product_pn_ai').val();
            setNIRValidationAlert(item, 'cantitate incorecta');
            ret = false;
        } else {
            $(this).parent().addClass('has-success').removeClass('has-error');
        }
    });

    if (ret) {
        console.log('validated!');
        $('#validate-nir-form').data('validated', '1');
    } else {
        console.log('not validated!');
        $('#validate-nir-form').data('validated', '0');
    }
    //showNIRvalidationInProgress('hide');

    return ret;
}

function enableNIRTypeAhead() {
    $('.typeahead').each(function() {
        var thisElement = $(this);
        $(this).typeahead({
            ajax: '/catalog/products/searchAutocomplete.json',
            onSelect: function(item) {
                var this_product_box = thisElement.closest('.product-box');
                this_product_box.find('.productId').val(item.value);
                thisElement.attr('data-product-id', item.value);
                this_product_box.find('.product_additional_info').parent().removeClass('has-error');
                $.get(
                    '/catalog/products/searchAutocomplete/' + item.value + '.json',
                    function(returnObj) {

                        // remove previous classes, if any
                        if (this_product_box.find('.product_id_ai').val() != '') {
                            this_product_box.find('.product_id_ai').removeClass('product_id_ai_' + this_product_box.find('.product_id_ai').val());
                        }
                        if (this_product_box.find('.pan_id_ai').val() != '') {
                            this_product_box.find('.pan_id_ai').removeClass('pan_id_ai_' + this_product_box.find('.pan_id_ai').val());
                        }

                        this_product_box.find('.product_id_ai').val(returnObj.data.Product.id);
                        this_product_box.find('.product_id_ai').addClass('product_id_ai_' + returnObj.data.Product.id);

                        this_product_box.find('.pan_id_ai').val(returnObj.data.ProductAliasName.id);
                        this_product_box.find('.pan_id_ai').addClass('pan_id_ai_' + returnObj.data.ProductAliasName.id);

                        this_product_box.find('.product_pn_ai').val(returnObj.data.Product.part_number);
                    },
                    'json'
                );
            },
            hint: true,
            matcher: function (item) {
                return true;
            },
            highlighter: function (item) {
                CheckAgainst = this.query.split(' ').join('(.*)');
                var regex = new RegExp( '(' + CheckAgainst + ')', 'gi' );
//                console.log(regex);
                return item.replace( regex, "<strong>$1</strong>" );
            }
        });
    });
}

function calculateNIRRowTotal(row) {
    var price = $('.entry-product-price', row).val();
    var quantity = $('.entry-product-quantity', row).val();
    var total = price * quantity;
    total = (Math.round(100 * total)/100).toFixed(2); // cross-browser way (http://www.javascripter.net/faq/tofixed.htm)
    $('.entry-product-value', row).val(total);
    calculateNIRTotal();
}

function calculateNIRTotal() {
    var total = 0;
    $('.doubable .entry-product-value').each( function() {
        if ($(this).val() != '') {
            var rowVal = parseFloat($(this).val());
            total += rowVal;
        }
    });
    $('#NIRTotal').val(total.toFixed(2))
}


$('#EntryAddForm, #EntryEditForm').submit(function(e) {
    if ($('#validate-nir-form').data('validated') != '1') {
        alert('Va rugam validati campurile');
        e.preventDefault();
    } else if (validateNIRForm() === false) {
        e.preventDefault();
    }
});

$(document).on('keyup, keydown, keypress', '#EntryAddForm, #EntryEditForm', function(e) {
    if (e.keyCode == 13) {
        e.preventDefault();
    }
});

$(document).on('click touchstart', '#validate-nir-form', function(e) {
    e.preventDefault();
    validateNIRForm();
    return false;
});

$(document).on('keyup', '.entry-product-price, .entry-product-quantity', function() {
    var row = $(this).closest('.doubable');
    calculateNIRRowTotal(row);
});


/** *************** end NIR section *************** **/


//ucfirst like
function capitalizeFirstLetter(string) {
    return string.charAt(0).toUpperCase() + string.slice(1);
}

/*
 solutie generica pentru prevenirea submit-ului dublu:

 form-ul sa fie cu clasa .lock-form
 butonul de submit sa aiba si clasa 'btn-form-submit'
 butonul de submit sa aiba si o proprietate data-processing -> textul de pe buton
 dupa ce da click

 A se folosi cu grija, sa nu interfereze cu alte mecanici specializate de
 prevenire a problemei dublu-click.
 */

$(document).on('submit', '.lock-form', function(e){
    var button = $(this).find('.btn-form-submit');
    var text = button.attr('data-processing');

    if ($(document).find('div.has-error').length > 0) {
        $('html, body').animate({
            scrollTop: $('.has-error:first').offset().top - 70
        }, 1000);
    } else {
        button.prop("disabled", true);
        if (button.prop('nodeName') === 'BUTTON') {
            button.text(text);
        } else {
            button.prop('value', text);
        }
    }
});
// returns a number in the correct price format
function prettyPrice(price) {
    var newNumber = (Math.round(price*100)/100);
    newNumber = newNumber.toFixed(2);

    x = newNumber.split('.');
    x1 = x[0];
    x2 = x.length > 1 ? '.' + x[1] : '';
    var rgx = /(\d+)(\d{3})/;
    while (rgx.test(x1)) {
        x1 = x1.replace(rgx, '$1' + ',' + '$2');
    }
    return x1 + x2;
}

/** ******************* Toastr ******************** **/

var default_toastr_position = 'toast-bottom-right';
var default_toastr_timeout = '5000';
var default_toastr_hideMethod = 'fadeOut';
toastr.options = {
    "closeButton": false,
    "debug": false,
    "newestOnTop": true,
    "progressBar": true,
    "preventDuplicates": false,
    "onclick": null,
    "showDuration": "300",
    "hideDuration": "1000",
    "timeOut": default_toastr_timeout,
    "extendedTimeOut": "1000",
    "showEasing": "swing",
    "hideEasing": "linear",
    "showMethod": "fadeIn",
    "hideMethod": default_toastr_hideMethod,
    "positionClass": default_toastr_position
};

/**
 * putMessage -> Standard javascript message method
 * @param message -- message to be displayed
 * @param type -- type of message: info, success, warning, error. Default: info
 * @param timeOut -- if used, toast expires after [timeOut] ms.
 * @param position -- if used, toast is placed at [position] class.
 * @param dismissable
 */

function toastr_putMessage(message, type, timeOut, position, dismissable) {
    if (typeof timeOut !== 'undefined') {
        toastr.options['timeOut'] = timeOut;
    }
    if (typeof position !== 'undefined') {
        toastr.options['positionClass'] = position;
    }
    if (typeof dismissable !== 'undefined' && dismissable === false) {
        toastr.options['hideMethod'] = function() {
            return this;
        };
    }
    switch (type) {
        case 'success' : {
            toastr.success(message); break;
        }
        case 'info' : {
            toastr.info(message); break;
        }
        case 'warning' : {
            toastr.warning(message); break;
        }
        case 'error' : {
            toastr.error(message); break;
        }
        default : {
            toastr.info(message); break;
        }
    }
    // return to default options
    if (typeof timeOut !== 'undefined') {
        toastr.options['timeOut'] = default_toastr_timeout;
    }
    if (typeof position !== 'undefined') {
        toastr.options['positionClass'] = default_toastr_position;
    }
    if (typeof dismissable !== 'undefined' && dismissable === false) {
        toastr.options['hideMethod'] = default_toastr_hideMethod;
    }
}

/**
 *
 *  helper function for adding tags to lists
 * @param inclusion_el -> input to which selected tags will be added
 * @param exclusion_el -> input where we specify tags to be excluded
 */

function enableTagsTypeAhead(inclusion_el, exclusion_el ) {
    $('.typeahead').each(function() {
        var thisElement = $(this);
        $(this).typeahead({
            ajax: '/catalog/tags/searchAutocomplete.json',
            onSelect: function(item) {

                var list;
                if( exclusion_el !== null) {
                    if ($('input[name=tags_for]:checked').val() == 'inclusion') {
                        //list = $('#product_tag_ids');
                        list = inclusion_el;
                    } else {
                        //list = $('#excluded_product_tag_ids');
                        list = exclusion_el;
                    }
                } else {
                    list = inclusion_el;
                }

                //add tag to the list
                list.append($("<option></option>")
                    .attr("value", item.value)
                    .text(item.text)
                    .attr('selected', true));

                //reinitialize chosen multiple select
                $('.chosen-select').trigger('chosen:updated');

                //@tziganie: nu am gasit evenimentul din typeahead in care sa fac update cu ''
                setTimeout(function() {
                    $('input.typeahead').val('');
                }, 100);
            },
            hint: true,
            matcher: function (item) {
                return true;
            },
            highlighter: function (item) {
                CheckAgainst = this.query.split(' ').join('(.*)');
                var regex = new RegExp( '(' + CheckAgainst + ')', 'gi' );
                return item.replace( regex, "<strong>$1</strong>" );
            }
        });
    });
}

function removeDuplicates(str, separator) {
    str = str.split(' ').filter(function(item, i, allItems) {
        //keep separator in string without removing it
        if (item == separator) {
            return true;
        }
        return i==allItems.indexOf(item);
    }).join(' ');
    return str;
}

/** ******************* AJAX RMA changes ******************** **/
/** ************* (supplier, defect and entry) ************** **/

var changeRMAEntryOptions = function($row, data) {
    var $target = $row.find('select[name="EntryProductFlow[entry_id]"]');
    var target_html = '<option value="">- Selecteaza NIR -</option>';
    $.each(data, function(key, value) {
        target_html += '<option value="' + value['EntryProduct']['entry_id'] + '">';
        target_html += value['Entry']['invoice_number'] + ' | ';
        target_html += value['Entry']['created_at'] + '</option>';
    });
    $target.html(target_html);
};

var setVisualUpdate = function($object, status) {
    if (status == 'success') {
        $object.addClass('btn-success');
    } else if (status == 'error') {
        $object.addClass('btn-danger');
    }
};

var ajaxFailCatch_RMA = function(failMsg, msg) {
    var m ='Operatiune esuata:' + '\n' +
        'Tip eroare: ' + failMsg.statusText + '\n' + msg;
    if (!navigator.onLine) {
        toastr_putMessage('Conexiune la internet inexistenta sau instabila.', 'warning', 15000);
    }
    toastr_putMessage(m, 'error', 10000);
};

$(document).ready(function() {

    $('#productList')
        .on('click', '.btn-trigger-rma-change-supplier', function() {
            // ajax to change supplier
            var $this = $(this);
            var $form = $this.parents('form');
            var $row = $this.parents('tr');
            var $supplier = $form.find('select[name="EntryProductFlow[supplier_id]"]');
            var url = $form.prop('action') + '.json';
            var id = $form.find('input[name="EntryProductFlow[id]"]').val();
            var value = $supplier.val();
            var product_id = $row.find('input.product-id').data('pid');

            $.ajax({
                url: url,
                method: 'POST',
                data: {
                    'EntryProductFlow[id]': id,
                    'EntryProductFlow[supplier_id]': value,
                    'product_id': product_id
                }
            }).done(function (msg) {
                toastr_putMessage(msg.message, msg.status);
                if (msg.status == 'success') {
                    changeRMAEntryOptions($row, msg.data);
                }
                setVisualUpdate($this, msg.status);
            }).fail(function(fmsg) {
                ajaxFailCatch_RMA(fmsg, '');
            });
            return false;
        })
        .on('click', '.btn-trigger-rma-change-defect', function() {
            // ajax to change defect
            var $this = $(this);
            var $form = $this.parents('form');
            var $defect = $form.find('select[name="EntryProductFlow[defect_id]"]');
            var url = $form.prop('action') + '.json';
            var id = $form.find('input[name="EntryProductFlow[id]"]').val();
            var value = $defect.val();

            $.ajax({
                url: url,
                method: 'POST',
                data: {
                    'EntryProductFlow[id]': id,
                    'EntryProductFlow[defect_id]': value
                }
            }).done(function (msg) {
                toastr_putMessage(msg.message, msg.status);
                setVisualUpdate($this, msg.status);
            }).fail(function(fmsg) {
                ajaxFailCatch_RMA(fmsg, '');
            });
            return false;
        })
        .on('click', '.btn-trigger-rma-change-entry', function() {
            // ajax to change entry
            var $this = $(this);
            var $form = $this.parents('form');
            var $entry = $form.find('select[name="EntryProductFlow[entry_id]"]');
            var url = $form.prop('action') + '.json';
            var id = $form.find('input[name="EntryProductFlow[id]"]').val();
            var value = $entry.val();

            $.ajax({
                url: url,
                method: 'POST',
                data: {
                    'EntryProductFlow[id]': id,
                    'EntryProductFlow[entry_id]': value
                }
            }).done(function (msg) {
                toastr_putMessage(msg.message, msg.status);
                setVisualUpdate($this, msg.status);
            }).fail(function(fmsg) {
                ajaxFailCatch_RMA(fmsg, '');
            });
            return false;
        });
});