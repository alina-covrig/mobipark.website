$(function(){
    $('.SuppPnsPopoverBtn[rel="popover"]').popover({
        container: 'body',
        html: true,
        content: function () {
//                    $('.SuppPnsPopover').addClass('hide');
            var clone = $($(this).data('popover-content')).clone(true).removeClass('hide');
            return clone;
        }
    }).click(function(e) {
        e.preventDefault();
    });
});


$('.image-popover[rel=popover]').popover(
    initImagePopOver()
);

var screenWidth = $( window ).width();
$(window).resize(function() {
    screenWidth = $( window ).width();
});

function initImagePopOver() {


    $('.image-popover[rel=popover]').popover({
        // container: '.wrapper',
        html: true,
        trigger: 'hover',
        //placement: 'right',
        placement: function() {
            return (screenWidth < 820) ? 'bottom' : 'right';
        },
        template: '<div class="popover prod-img-popover"><div class="arrow">' +
        '</div><h3 class="popover-title"></h3><div class="popover-content"></div></div>',
        content: function () {
            return '<img src="' + $(this).attr('data-img') + '"/>';
        }
    });
}


$('.admin-notes').editable();
$('.note').editable();

$.fn.multipleSelect.defaults["selectAll"] = false;
$('.multi-select').multipleSelect({
    filter: true,
    container: 'parent'
});

/**
 * Marks competitors tasks as finished or unfinished
 */
function competitorsTaskValidate(element)
{
    element = $(element);

    var msg = "Esti sigur ca vrei sa marchezi task-ul ca " + ((element.prop('checked') == true) ? "finalizat" : "nefinalizat") + "?";
    var box = confirm(msg);

    if (box==true)
    {
        var orderId = parseInt(element.attr('data-inbound-order-id'));
        var supplier_id = parseInt(element.attr('data-supplier-id'));
        var is_completed = element.prop('checked');
        var task = element.attr('data-task');

        element.attr("disabled", true);

        $.ajax({
            url: '/suppliers/inbound_orders/checkCompetitorTask.json',
            method: 'POST',
            data: {
                supplier_id: supplier_id,
                inbound_order_id: orderId,
                task: task,
                is_completed: is_completed
            }
        })
            .done(function (msg) {
                toastr_putMessage(msg.message, msg.status)
                element.removeAttr("disabled");

                if (msg.status !== 'success')
                {
                    return true;
                }
                else
                {
                    return false;
                }
            })
            .fail(function(fmsg) {
                ajaxFailCatch(fmsg,'');
            });
    }
    else
    {
        if(element.prop('checked') == true)
        {
            element.prop('checked', false);
        }
        else
        {
            element.prop('checked', true);
        }
    }
}


$('#currency').on('change', function() {
    setCurrencyValues();
});

//TODO 1: vezi validarile astea la valori date comanda/ aplica-le si la index.ctp
function setCurrencyValues() {
    var currency = $('#currency').val();
    if(currency == '') { //fara valuta / leu
        $('#exchange-rate').find('input')
            .val('')
            .prop('disabled',true)
            .prop('required',false);
        $('#constants').find('input')
            .val('')
            .prop('disabled',true)
            .prop('required',false);
    } else { //cu valuta
        $('#exchange-rate')
            //.removeClass('hidden')
            .find('input')
            .prop('disabled',false)
            .prop('required',true);
        if(currency == 2) { //Dolar
            $('#constants').find('input')
                .prop('disabled',false)
                .find('input').prop('required',true);
        } else { // alta valuta
            $('#constants').find('input')
                .val('')
                .prop('disabled',true)
                .prop('required',false);
        }
    }
}


$(document).ready(function() {

    $("#page-wrapper").attr('style', 'padding-top: 0px;')

    // conditional fields for currency
    setCurrencyValues();

    $('#currency').on('change', setCurrencyValues);

    $(".shadow").on("focus", function() {
        $(this).select();
    });

    $('#notification_type').on('change', function () {
        var notification_type = $('#notification_type').val();

        if(notification_type == 1) {
            $('#notifications-editor').addClass('hidden');
        } else {
            $('#notifications-editor').removeClass('hidden');
        }
    });



    $('.barcode-scan')
        .on('dblclick', 'input', function() {
            $(this).val('');
        })
        .on('focus', 'input', function() {
            $(this).css({'background-color' : '#a7ebff'});
        })
        .on('blur', 'input', function() {
            var c ='#fff';
            if($(this).val() !== ''){c ='#f7e8ec';}
            $(this).css({'background-color' : c});
        });


    $('.search-input')
        .on('dblclick', 'input', function() {
            $(this).val('');
        })
        .on('focus', 'input', function() {
            $(this).css({'background-color' : '#FFFFEEE'});
        })
        .on('blur', 'input', function() {
            var c ='#fff';
            if($(this).val() !== ''){c ='#f7e8ec';}
            $(this).css({'background-color' : c});
        });


    //TODO: adauga la doc ready functia care s aprea pns- pt ptoduse daca modul e diferit de analiza preturi concurent
    //TODO: adauga la doc ready functia care sa scrie tiite-luirile p

    var currentValue = '';
    var send = false;
    /**
     * Edit in place for certain IOP fields
     */
    $('table#inbound-order-products')
        .on('focus', 'input.edit-in-place', function() {
            send = false;
            var $this = $(this);
            currentValue = $this.data('original-value');
            $this
                .removeClass('error success')
                .addClass('focused');
        })
        .on('keypress', 'input.edit-in-place', function(e) {
            var $this = $(this);
            //TODO: salvarile fieldurilor (inclusiv obseravtii de produs) sa ia in considerare si id-ul produslui (daca cumva se schimba asocierea)
            if(e.keyCode === 13) { // ENTER
                var $trParent = $this.parents('tr');
                var orderProductId = $trParent.attr('id');
                var associatedProductId = $trParent.find('input[name=product_id]').val();
                var fieldClass = $this.data('field');
                var newValue = $this.val();

                if(fieldClass=='lbl-print'){return false;}

                if(newValue != currentValue || currentValue !== '') {
                    var data = {product_id: associatedProductId, field: fieldClass, value: newValue, old_value: currentValue};
                    $.ajax({
                        url: '/suppliers/inbound_orders/iopSaveField/' + orderProductId + '.json',
                        method: 'POST',
                        data: data
                    })
                    //TODO: in calzul editarilor de pe mobil a cantitatilor livrate sa se faca increm,entarea daca exista o valoare si sa se retuneze ca succes mesaj corespunzator
                        .done(function(msg) {
                            if(msg.status == 'success') {
                                toastr_putMessage(msg.message, msg.status, 1000);
                                $this
                                    .data('original-value', newValue)
                                    .removeClass('error')
                                    .addClass('success');

                                getOrderStatistics();

                                if($.inArray(fieldClass, ['purchase_price', 'received_amount']) !== -1) {
                                    recalculateValues($trParent);
                                }

                                if($.inArray(fieldClass, ['selling_price', 'purchase_price', 'received_amount', 'missing_amount', 'de_amount']) !== -1) {
                                    resetAlerts($trParent);
                                }

                                if(fieldClass == 'received_amount')
                                {
                                    var order_amount = parseInt($trParent.find('input[name=order_amount]').val());
                                    var received_amount = parseInt(newValue); // $trParent.find('.received_amount input').val();
                                    resetOrderAmountStatusIndicator($trParent, order_amount, received_amount);
                                }

                            } else if(msg.status == 'info') {
                                if(msg.message == 'Association changed') {
                                    toastr_putMessage('Asocierea a fost schimbata. Se actualizeaza linia din lista', msg.status, 5000);
                                    var orderId = parseInt($('#order-id').val());
                                    $.ajax({
                                        url: '/suppliers/inbound_orders/view/' + orderId + '/' + orderProductId + '.json',
                                        dataType: 'JSON'
                                    })
                                        .done(function(msg) {
                                            if (msg.status === 'success') {
                                                if(msg.data) {
                                                    var iop = msg.data;
                                                    closeSearchResultsList()
                                                    //TODO updateOnAddingElementToOrder nu actualizeaza datele ce tin de cantitati si preturi ci pur si simplu le reseteaza
                                                    updateOnAddingElementToOrder($trParent, iop);
                                                    updateOnAssociationChange($trParent, iop);
                                                    recalculateValues($trParent);
                                                    resetAlerts($trParent);
                                                }
                                                getOrderStatistics();
                                            } else {
                                                toastr_putMessage('Actualizare date produs esuata. ' + '\n' + msg.message, msg.status);
                                            }
                                        })
                                        .fail(function(fmsg) {
                                            ajaxFailCatch(fmsg,'');
                                        });
                                } else if(msg.message == 'Association deleted') {
                                    toastr_putMessage('Asocierea a fost stearsa. Se actualizeaza linia din lista', msg.status, 5000);
                                    //TODO: pe viitor, actualizeaza si informatiile legate de cantitati si preturi
                                    // updateOnAddingElementToOrder($trParent, null);
                                    updateOnAssociationChange($trParent, null);
                                    recalculateValues($trParent);
                                    resetAlerts($trParent);
                                    getOrderStatistics();
                                } else {
                                    toastr_putMessage('Stare operatiune nederminata ' + '\n' + msg.message, msg.status);
                                }
                            } else {
                                toastr_putMessage('Salvare esuata.' + '\n' + msg.message, msg.status);
                            }
                        })
                        .fail(function(fmsg) {
                            ajaxFailCatch(fmsg,'');
                        });
                }
                // focus on the same input for the next row
                focusOnNextInput_row($trParent, fieldClass, 'next');
            }
        })
        .on('keydown', 'input.edit-in-place', function(e) {
            // arrow keys detection only works on 'keydown' event (not 'keypress')
            var $this = $(this);
            var $trParent = $this.parents('tr');
            var $elems = $trParent.find('input.edit-in-place');
            focusOnNextInput($this, $elems, e.keyCode);

            if(e.keyCode === 13) {
                send = true;
            }
        })
        .on('blur', 'input.edit-in-place', function() {
            var $this = $(this);
            var modifiedValue = $this.val();
            // ca sa rezolve diferentele cauzate re pretty_price intre 2 si 4 zecimale
            // if(!$this.hasClass('lbl-print') && !$this.hasClass('sp-p-data') && eval(modifiedValue) !== eval(currentValue) && !send) {
            if($this.hasClass('sp-p-data')) {
                if(modifiedValue != currentValue && !send) {
                    $this.addClass('error');
                    toastr_putMessage('Apasa ENTER pentru a salva date produs!', 'error');
                }
            } else {
                if(!$this.hasClass('lbl-print') && eval(modifiedValue) !== eval(currentValue) && !send) {
                    $this.addClass('error');
                    toastr_putMessage('Apasa ENTER pentru a salva!', 'error');
                }
            }
            $this.removeClass('focused');
        });

    /**
     * Function that places the cursor (focus) on the same input for the next/previous row
     *
     * @param $trParent     - the TR element (row)
     * @param fieldClass    - unique class for each field
     * @param type          - 'next' | 'prev' for focus direction
     */
    var focusOnNextInput_row = function($trParent, fieldClass, type) {
        if(type === 'next') {
            if ($trParent.next('tr').length) {
                $trParent.next().find('input.' + fieldClass).focus();
            } else {
                $trParent.parent('tbody').find('tr:first-child').find('input.' + fieldClass).focus();
            }
        } else if(type === 'prev') {
            if ($trParent.prev('tr').length) {
                $trParent.prev().find('input.' + fieldClass).focus();
            } else {
                $trParent.parent('tbody').find('tr:last-child').find('input.' + fieldClass).focus();
            }
        }
    };

    /**
     * Function that reads the directional arrow keys and makes
     * the corresponding focus action
     *
     * @param $current      - the current element (input)
     * @param $elements     - the set of inputs in the current row
     * @param keycode       - 'keydown' key code
     */
    var focusOnNextInput = function($current, $elements, keycode) {
        var $trParent = $current.parents('tr');
        var $fieldCopy = $current.clone();
        $fieldCopy.removeClass('edit-in-place focused mk-dif-prc no-order-amount');
        var fieldClass = $fieldCopy.prop('class');

        if(keycode === 37) {    // LEFT ARROW
            $elements.eq($elements.index($current) - 1).focus();
        } else if(keycode === 38) {    // UP ARROW
            focusOnNextInput_row($trParent, fieldClass, 'prev');
        } else if(keycode === 39) {    // RIGHT ARROW
            var crt_index = $elements.index($current);
            var next_index = crt_index + 1;
            if(crt_index === $elements.length - 1) {
                next_index = 0;
            }
            $elements.eq(next_index).focus();
        } else if(keycode === 40) {    // DOWN ARROW
            focusOnNextInput_row($trParent, fieldClass, 'next');
        }

    };

    function redrawStats($itm,ret_val) {
        var itm_val =$itm.html();
        itm_val +='';
        ret_val +='';
        if(itm_val == '&nbsp;'){itm_val ='0'};
        if(itm_val !== ret_val){
            if(ret_val == 0){
                $itm.parent().closest('div').removeClass('has-val');
            } else {
                $itm.parent().closest('div').addClass('has-val');
            }
            $itm.parent().closest('div').addClass('has-new-val');
            $itm.html(ret_val == 0 ? '&nbsp;' : ret_val);
            // $itm.effect('highlight', {}, 1000);
            $itm.effect("pulsate", "",2000);
        }
        // pulsate doar daca se modifica
    }

    function getOrderStatistics() {
        var orderId = parseInt($('#order-id').val());
        $.ajax({
            url: '/suppliers/inbound_orders/getStatistics/' + orderId + '.json',
            method: 'POST'
        })
            .done(function (msg) {
                if (msg.status !== 'success' && msg.data && msg.data.info) {
                    var str_val = '';
                    var $itm;

                    //TODO : fa si pt date de baza comanda cum ar fi factura, furnizor totaluri etc..., data livrare

                    str_val = msg.data.info.count_total_lines;
                    $itm =$(document).find('#count_total_lines');
                    redrawStats($itm,str_val);

                    str_val = msg.data.info.count_unassoc_prod;
                    $itm =$(document).find('#count_unassoc_prod');
                    redrawStats($itm,str_val);

                    str_val = msg.data.info.count_prod_without_pos;
                    $itm =$(document).find('#count_prod_without_pos');
                    redrawStats($itm,str_val);

                    //momentan nu este necesar
                    // str_val = msg.data.info.count_prod_no_selling_price;
                    // $itm =$(document).find('#count_prod_no_selling_price');
                    // redrawStats($itm,str_val);

                    str_val = msg.data.info.inbound_order['InboundOrder']['count_products_no_iop_pn'];
                    $itm =$(document).find('#count_products_no_iop_pn');
                    redrawStats($itm,str_val);

                    str_val = msg.data.info.inbound_order['InboundOrder']['count_not_unique_products'];
                    $itm =$(document).find('#count_not_unique_products');
                    redrawStats($itm,str_val);

                    str_val = msg.data.info.inbound_order['InboundOrder']['count_inactive_products'];
                    $itm =$(document).find('#count_inactive_products');
                    redrawStats($itm,str_val);

                    str_val = msg.data.info.inbound_order['InboundOrder']['count_partially_received_products'];
                    $itm =$(document).find('#count_partially_received_products');
                    redrawStats($itm,str_val);

                    str_val = msg.data.info.inbound_order['InboundOrder']['count_not_received_products'];
                    $itm =$(document).find('#count_not_received_products');
                    redrawStats($itm,str_val);

                    str_val = prettyPrice(msg.data.info.inbound_order['InboundOrder']['sum_total_real_purchase_price']);
                    $itm =$(document).find('#sum_total_real_purchase_price');

                    var required_entry_total = msg.data.info.inbound_order['InboundOrder']['required_entry_total'];
                    var sum_total_real_purchase_price = msg.data.info.inbound_order['InboundOrder']['sum_total_real_purchase_price'];
                    if(required_entry_total !== null){
                        var t = ' = Total calculat al comenzi (' +  sum_total_real_purchase_price +
                            ') minus total target nir (' +  required_entry_total +
                            ') in lei';
                        $itm.parent().closest('div').attr('data-original-title',t);
                        str_val = prettyPrice(sum_total_real_purchase_price-required_entry_total)
                    }
                    redrawStats($itm,str_val);





                    var currency_type = $(document).find('input[name=currency]').val();
                    var currency;
                    if(currency_type.toLowerCase() != 'ron'){
                        currency = currency_type.toLowerCase();
                    } else {
                        currency = currency_type;
                    }
                    str_val = currency;
                    $itm =$(document).find('.currency-symbol');
                    $itm.attr('class','currency-symbol fa fa-' . currency);
                    // redrawStats($itm,str_val);
                    //TODO: implementeaza notificare dac se schimba

                    str_val = (prettyPrice(msg.data.info.inbound_order['InboundOrder']['sum_total_purchase_price']));
                    $itm =$(document).find('#sum_total_purchase_price');
                    redrawStats($itm,str_val);

                    str_val = (prettyPrice(msg.data.info.inbound_order['InboundOrder']['invoice_total']));
                    $itm =$(document).find('#invoice_total');
                    redrawStats($itm,str_val);

                    setTasksState(msg.data.info.order_tasks, 1);
                } else {
                    toastr_putMessage('Actualizarea statisticilor a esuat. ' + '\n' + msg.message, msg.status, 10000);
                }
            })
            .fail(function(fmsg) {
                ajaxFailCatch(fmsg,'');
            });
    }


    //TODO: cand printezi elticheta si precompletezi valoare receptionata aceasta nu este scrisa corect in markup pt ca la blur pe input primesc mesaj ca nu a fost salvata


    /**
     * Recalculate alerts for the new quantity or price values
     * @param $trParent  - the TR element (row)
     * @param orderProductSellingPrice  -  the new order product selling price
     */
    function resetAlerts($trParent) {

        var data = [];
        data['orderProductSellingPrice'] = parseFloat($.trim($trParent.find('input.selling_price').val()));
        data['productSellingPrice'] = parseFloat($.trim($trParent.find('.associated_selling_price').text()));

        // data['exchange_rate'] = parseFloat($trParent.find('input[name=exchange_rate]').val());
        // data['entry_constant'] = parseFloat($trParent.find('input[name=entry_constant]').val());
        // data['acquisition_constant'] = parseFloat($trParent.find('input[name=acquisition_constant]').val());
        // data['vat'] = parseFloat($trParent.find('input[name=vat]').val());


        data['exchange_rate'] = parseFloat($('input[name=exchange_rate]').val());
        data['entry_constant'] = parseFloat($('input[name=entry_constant]').val());
        data['acquisition_constant'] = parseFloat($('input[name=acquisition_constant]').val());
        data['vat'] = parseFloat($('input[name=vat]').val());


        data['order_amount'] = parseInt($trParent.find('input[name=order_amount]').val());
        data['received_amount'] = parseInt($trParent.find('input.received_amount').val());
        data['de_amount'] = parseInt($trParent.find('input.de_amount').val());
        data['missing_amount'] = parseInt($trParent.find('input.missing_amount').val());

        data['purchase_price'] = parseFloat($trParent.find('input.purchase_price').val());
        data['diamond_selling_price'] = parseFloat($.trim($trParent.find('.diamond_price').text()));

        data['real_purchase_price'] = data['purchase_price'];
        data['real_purchase_price'] *= data['exchange_rate'];
        data['real_purchase_price'] *= (data['entry_constant'] !== 0) ? data['entry_constant'] : 1;

        data['acquisition_price'] = data['purchase_price'];
        data['acquisition_price'] *= data['exchange_rate'];
        data['acquisition_price'] *= (data['acquisition_constant'] !== 0) ? data['acquisition_constant'] : data['vat'];

        var priceToCompare = !isNaN(data['orderProductSellingPrice']) ? data['orderProductSellingPrice'] : data['productSellingPrice'];

        data['show_supplier_price_alert'] = 0;
        var competitor = $trParent.find('.competitor');

        for (var i = 0, len = competitor.length; i < len; i++) {
            var competitorPrice = parseFloat($.trim($(competitor[i]).find('.competitor-price').text()));
            if (competitorPrice < priceToCompare) {
                data['show_supplier_price_alert'] = 1;
                $(competitor[i]).addClass('marked-competitor');
            } else {
                $(competitor[i]).removeClass('marked-competitor');
            }
        }

        if (!isNaN(data['de_amount']) && !isNaN(data['received_amount'])) {
            $trParent.find('.alert-existent-de-amount').removeClass('hidden');
        } else {
            $trParent.find('.alert-existent-de-amount').addClass('hidden');
        }

        if (!isNaN(data['missing_amount']) && !isNaN(data['received_amount'])) {
            $trParent.find('.alert-missing-amount').removeClass('hidden');
        } else {
            $trParent.find('.alert-missing-amount').addClass('hidden');
        }

        if (!isNaN(data['real_purchase_price']) && data['real_purchase_price'] >= priceToCompare) {
            $trParent.find('.mk-nir-gr-p').removeClass('hidden');
        } else {
            $trParent.find('.mk-nir-gr-p').addClass('hidden');
        }

        if (!isNaN(data['real_purchase_price']) && !isNaN(data['diamond_selling_price']) && data['real_purchase_price'] >= data['diamond_selling_price']) {
            $trParent.find('.mk-nir-gr-diam').removeClass('hidden');
        } else {
            $trParent.find('.mk-nir-gr-diam').addClass('hidden');
        }

        if (!isNaN(data['acquisition_price']) && data['acquisition_price'] >= priceToCompare) {
            $trParent.find('.mk-saq-gr-p').removeClass('hidden');
        } else {
            $trParent.find('.mk-saq-gr-p').addClass('hidden');
        }

        if (!isNaN(data['acquisition_price']) && !isNaN(data['diamond_selling_price']) && data['acquisition_price'] >= data['diamond_selling_price']) {
            $trParent.find('.mk-aq-gr-diam').removeClass('hidden');
        } else {
            $trParent.find('.mk-aq-gr-diam').addClass('hidden');
        }

        if (!isNaN(data['acquisition_price']) && !isNaN(data['diamond_selling_price']) && (data['acquisition_price'] * 1.3) >= data['diamond_selling_price']) {
            $trParent.find('.mk-saq-gr-diam').removeClass('hidden');
        } else {
            $trParent.find('.mk-saq-gr-diam').addClass('hidden');
        }

        if (!isNaN(data['productSellingPrice']) && !isNaN(data['orderProductSellingPrice']) && data['productSellingPrice'] >= data['orderProductSellingPrice']) {
            $trParent.find('.mk-p-gr-np').removeClass('hidden');
        } else {
            $trParent.find('.mk-p-gr-np').addClass('hidden');
        }

        if (data['show_supplier_price_alert'] == 1) {
            $trParent.find('.alert-competition-smaller-selling-price').removeClass('hidden');
        } else {
            $trParent.find('.alert-competition-smaller-selling-price').addClass('hidden');
        }
    }

    /**
     * Recalculate all table values using the modified ones
     * @param $trParent     - the TR element (row)
     */
    function recalculateValues($trParent) {
        var data = [];
        data['purchase_price'] = parseFloat($trParent.find('input.purchase_price').val());
        if(isNaN(data['purchase_price'])) {
            data['purchase_price'] = 0;
        }
        data['received_amount'] = parseInt($trParent.find('input.received_amount').val());
        if(isNaN(data['received_amount'])) {
            data['received_amount'] = 0;
        }
        data['order_amount'] = parseInt($trParent.find('input[name=order_amount]').val());
        data['exchange_rate'] = parseFloat($('input[name=exchange_rate]').val());
        data['entry_constant'] = parseFloat($('input[name=entry_constant]').val());
        data['acquisition_constant'] = parseFloat($('input[name=acquisition_constant]').val());
        data['vat'] = parseFloat($('input[name=vat]').val());

        data['total_purchase_price'] = data['purchase_price'];
        data['total_purchase_price'] *= (data['received_amount'] !== 0) ? data['received_amount'] : data['order_amount'];

        data['real_purchase_price'] = data['purchase_price'];
        data['real_purchase_price'] *= data['exchange_rate'];
        data['real_purchase_price'] *= (data['entry_constant'] !== 0) ? data['entry_constant'] : 1;

        data['total_real_purchase_price'] = data['real_purchase_price'];
        data['total_real_purchase_price'] *= (data['received_amount'] !== 0) ? data['received_amount'] : data['order_amount'];

        data['acquisition_price'] = data['purchase_price'];
        data['acquisition_price'] *= data['exchange_rate'];
        data['acquisition_price'] *= (data['acquisition_constant'] !== 0) ? data['acquisition_constant'] : data['vat'];

        $trParent.find('p.total_purchase_price').html(round(data['total_purchase_price'],2)).effect('highlight', {}, 500);
        $trParent.find('p.real_purchase_price').html(round(data['real_purchase_price'],2)).effect('highlight', {}, 500);
        $trParent.find('p.total_real_purchase_price').html(round(data['total_real_purchase_price'],2)).effect('highlight', {}, 500);
        $trParent.find('p.acquisition_price').html(round(data['acquisition_price'],2)).effect('highlight', {}, 500);
    }

    /**
     * Convenience function to round decimals on a floating point number
     * @param value         - the number
     * @param decimals      - number of decimals
     * @returns {number}
     */
    function round(value, decimals) {
        return Number(Math.round(value + 'e' + decimals) + 'e-' + decimals);
    }


    $('.order_amount').mousedown(function(event) {
        if(event.which ==1 && $('#enable_order_amount_edit').prop('checked') == true){
            var $this = $(this);
            var val = $this.attr('value');
            var new_html = '<input type="text" class="edit-in-place order_amount" '
            + 'data-field="order_amount" data-original-value="' + val + '" value="' + val + '">'
            $this.html(new_html)
        }
    });

    /**
     * Confirm dialogs for important buttons
     */
    $('.gen-new-nir').on('click', function(e) {
        if(!confirm('Esti sigur(a) ca vrei sa continui?')) {
            return false;
        }
    });

    $('.entry-actions').on('click', 'a', function() {
        if(!confirm('Esti sigur(a) ca vrei sa continui?')) {
            return false;
        }
    });

    /**
     * Trigger for showing modal form for order data edit
     */
    $('a.btn-edit-order-data').on('click', function() {
        $('#edit_order').modal('show');
    });

    /**
     * Trigger for showing modal form for adding more order products via csv
     */
    $('a.btn-add-order-products').on('click', function() {
        $('#upload_order').modal('show');
    });

    /**
     * Trigger for showing modal notifications
     */
    $('a.btn-order-entry-actions').on('click', function() {
        $('#order_entry_actions').modal('show');
    });

    /**
     * Trigger for showing modal order notification
     */
    $('a.btn-order-notification').on('click', function() {
        $('#order_notification').modal('show');
    });

    /**
     * Trigger on showing modal order notification
     */
    $("#order_notification").on('show.bs.modal', function() {
//                $("#select-emails").attr('style', 'color: red;')
//                $(".ms-parent.multi-select").attr('style', 'width: 100%;')

        $('#order_notification').find('.ms-parent.multi-select').attr('style', 'width: 100%;')
    });

    /**
     * Trigger for toogle display of competitors data
     */
    $('#competitors-data-toggle').change(function(){
        if(this.checked) {
            $('.competitors').fadeIn('slow');
            $('.competitors_th').removeClass('hidden');
            $('.competitors').removeClass('hidden');
        } else {
            $('.competitors').fadeOut('slow');
            $('.competitors_th').addClass('hidden');
            $('.competitors').addClass('hidden');
        }
    });

    /**
     * Trigger for toogle display of product image
     */
    $('#images-toggle').change(function(){
        if(this.checked) {
            $('.image').fadeIn('slow');
            $('.image_th').removeClass('hidden');
            $('.image').removeClass('hidden');
        } else {
            $('.image').fadeOut('slow');
            $('.image_th').addClass('hidden');
            $('.image').addClass('hidden');
        }
    });

    /**
     * Trigger for toogle display of product search form
     */
    $('#search-toggle').change(function(){
        if(this.checked) {
            $('#search').fadeIn('slow');
            $('#search').removeClass('hidden');
        } else {
            $('#search').fadeOut('slow');
            $('#search').addClass('hidden');
        }
    });


    /**
     * Trigger for deleting an association
     */
    $('table#inbound-order-products')
        .on('click', 'a.delete-association', function() {
            // $('a.delete-association').on('click', function() {
            if (confirm('Esti sigur(a) ca vrei sa stergi asocierea?')) {
                var $this = $(this);
                var $trParent = $this.parents('tr');
                var orderProductId = $trParent.attr('id');
                var associatedProductId = $trParent.find('input[name=product_id]').val();
                $.ajax({
                    url: '/suppliers/inbound_orders/deleteAssociation.json',
                    method: 'POST',
                    data: {iop_id: orderProductId, p_id: associatedProductId}
                })
                // TODO: fa update la markup doar daca a resusit stergerea asocierii
                    .done(function (msg) {
                        toastr_putMessage(msg.message, msg.status, 3000);
                        // empty all related fields
                        //                        resetValues($trParent);
                        updateOnAssociationChange($trParent, null);
                        getOrderStatistics();
                    })
                    .fail(function(fmsg) {
                        ajaxFailCatch(fmsg,'');
                    })
                    ;
            }
    });

    function ajaxFailCatch(failMsg,msg) {
        var m ='Operatiune esuata:' + '\n' +
            'Tip eroare: ' + failMsg.statusText + '\n' + msg;
        if (!navigator.onLine) {
            toastr_putMessage('Conexiune la internet inexistenta sau instabila.', 'warning', 15000);
        }
        toastr_putMessage(m, 'error', 10000);
    }


    //TODO: AI RAMAS LA IMPELMENTAREA URL-urilor de produs
    /**
     * Trigger for product urls
     */
    $('table#inbound-order-products')
        .on('click', '.jsurl', function() {
            var orderId = parseInt($('#order-id').val());
            var supp_prod_raw_url = $('input[name=s_p_url]').val();
            var $this = $(this);
            var $trParent = $this.parents('tr');
            var orderProductId = parseInt($trParent.attr('id'));
            var associatedProductId = parseInt($trParent.find('input[name=product_id]').val());
            var associatedProductAliasId = parseInt($trParent.find('.pan-id').val());
            var suppProducPartNumber = $trParent.find('.supp_pn').val();
            var url = '';
            if (associatedProductId && $this.hasClass('p-url')) {
                // url = "<?=$this->H->storePath($product['Product']['ProductAliasName']['Category']['product_path']+ '/' + $product['Product']['ProductAliasName']['alias'])?>";
                url = 'https://www.gsmnet.ro/cauta/' + associatedProductAliasId;
            } else if (associatedProductId && $this.hasClass('edit-url')) {
                url = '/catalog/products/edit/' + associatedProductId;
            } else if (associatedProductId && $this.hasClass('stats-url')) {
                url = '/statistics/supplies/acquisition?ProductAliasName[id_or_code]=' + associatedProductAliasId + '&ProductAliasName[is_active][n][]=1&ProductAliasName[is_active][n][]=0';
            } else if (associatedProductId && $this.hasClass('m-flow-url')) {
                url = '/merchandise/entries/flow?product_id=' + associatedProductAliasId + '&nir_only=on';
            } else if (associatedProductId && $this.hasClass('m-entries-url')) {
                url = '/merchandise/entries/checkStock?product_id=' + associatedProductAliasId;

            } else if ($this.hasClass('p-log-url')) {
                url =  '/suppliers/inbound_orders/actionsHistory/' + orderId + '/' + orderProductId;

            } else if ($this.hasClass('p-pn-s-url')) {
                if(!supp_prod_raw_url){return false;}
                url = supp_prod_raw_url.replace('cod_produs', suppProducPartNumber);
                if(!url){return false;}
            }
            window.open(url);

        });

    // href="<?='/statistics/supplies/acquisition?ProductAliasName%5Bid_or_code%5D=' . $not_unique_spn_product?>">


    /**
     * Trigger for products association
     * (checks for association integrity and opens bs modal)
     */
    // make sure the association / re-association has the same parameters (iop <-> pid)
    $('table#inbound-order-products').on('click', 'a.associate-product', function() {
        var $this = $(this);
        var $trParent = $this.parents('tr');
        var orderProductId = $trParent.attr('id');
        var associatedProductId = $trParent.find('input[name=product_id]').val();
        var orderProductSuppCode =  $trParent.find('.supp_pn').val();
        var orderProductSuppName =  $trParent.find('.supp_p_name').val();
        var orderProductSuppSpec =  $trParent.find('.supp_p_specs').val();
        var orderProductAliasName =  $trParent.find('.pan-name').val();
        $.ajax({
            url: '/suppliers/inbound_orders/checkAssociation.json',
            method: 'POST',
            data: { iop_id: orderProductId, p_id: associatedProductId }
        })
            .done(function(msg) {
                if(msg.status !== 'success') {
                    toastr_putMessage('Verificarea validitatii asocierii a esuat. ' + '\n' + msg.message, msg.status);
                    return false;
                } else {
                    $('#associate_product').find('input[name=iop_id]').val(orderProductId);
                    $('#associate_product').find('#iop-sp-code').val(orderProductSuppCode);
                    $('#associate_product').find('#iop-sp-name').val(orderProductSuppName);
                    $('#associate_product').find('#iop-sp-spec').val(orderProductSuppSpec);
                    $('#associate_product').find('#iop-pan-name').val(orderProductAliasName);
                    $('#associate_product').modal('show');
                }
            })
            .fail(function(fmsg) {
                ajaxFailCatch(fmsg,'');
            });
        return false;
    });

    /**
     * Trigger for inline product search (pan_id, pan_name, part_number)
     * and shows a list of resulted products
     * (uses a timeout to ensure not every key press triggers a search)
     *
     * Second trigger (Enter key) overrides the timeout and calls
     * the search function immediately
     */
    var search_timeout = 0;
    var timer_timeout = 2500;
    $("#associate_product")
        .on('input', 'input[type=text]', function(e) {
            e.preventDefault();
            window.clearTimeout(search_timeout);
            initiateSearch();
            return false;
        })
        .on('keypress', function(e) {
            if (e.keyCode === 13) { // ENTER
                e.preventDefault();
                window.clearTimeout(search_timeout);
                performSearch();
            }
        });

    /**
     * Trigger that initiates product search with no parameters
     */
    $('#show_latest_200').on('click', function() {

        $('#pan_id').val('');
        $('#pan_name').val('');
        $('#part_number').val('');
        performSearch();
    });

    /**
     * Function that initiates search action wrapped in a timeout
     */
    var initiateSearch = function() {
        search_timeout = window.setTimeout(function() {
            performSearch();
        }, timer_timeout);
    };

    /**
     * Function that performs the search
     */
    var performSearch = function() {
        var orderId = parseInt($('#order-id').val());
        var $form = $('#associate_product');
        var $results = $form.find('.well');
        var pan_id = $form.find('#pan_id').val();
        var pan_name = $form.find('#pan_name').val();
        var part_number = $form.find('#part_number').val();
        var only_in_order = 1;
//                if(pan_id !== '' || pan_name !== '' || part_number !== '') {
        $results.html('<i class="fa fa-gear fa-spin"></i> Incarcare rezultate ...');
        $.ajax({
            url: '/suppliers/inbound_orders/searchAssociations/' + orderId + '.json',
            method: 'POST',
            data: {
                pan_id: pan_id,
                pan_name: pan_name,
                part_number: part_number,
                only_in_order: only_in_order
            }
        })
            .done(function (msg) {
                if (msg.status !== 'success') {
                    toastr_putMessage('Cautarea a esuat. ' + '\n' + msg.message, msg.status);
                    $results.html('');
                } else {
                    var results = msg.data;
                    if(results.length > 0) {
                        var list_data = '';
                        for (var i = 0; i < results.length; i++) {
                            list_data += templateSearchRow(results[i]);
                        }
                        $results.html(list_data);
                        initImagePopOver();
                    } else {
                        $results.html('Nu am gasit niciun produs');
                    }
                }
            })
            .fail(function(fmsg) {
                ajaxFailCatch(fmsg,'');
            });
//                }
    };

    /**
     * Function that generates HTML code for a result row
     * @param element           - object with row data
     * @returns {string}        - html
     */


    var templateSearchRow = function(element) {
        var html = '<div class="element">';
        html += '<div class="trigger" data-associated-product="' + element.p_id + '" data-active="' + element.active + '">';
        html += '<div class="inner">';
        html += '<div class="image"><a class="image-popover" rel="popover" data-img="' + element.image_large + '"' ;
        html += '  data-delay="{\'show\':\'1200\', \'hide\':\'5000\'}" >';
        html += '    <img class="small" src="' + element.image + '">';
        html += '</a></div>';
        html += '<div class="id"><span class="label label-default">' + element.pan_id + '</span></div>';
        html += '<div class="in-list">';
        html += (element.in_list)
            ? '<i class="fa fa-th-list" title="se ragaseste in comanda curenta"></i>'
            : '<i class="fa fa-th-list off" title="nu se ragaseste in comanda curenta"></i>';
        html += '</div>';
        html += '<div class="name">' + element.pan_name + '</div>';
        html += '<div class="part_number">' + element.part_nb + '</div>';
        html += '</div>';
        html += '</div>';
        html += '<div class="main">';
        html += (element.main)
            ? '<i class="fa fa-star" title="Este principal"></i>'
            : '<i class="fa fa-star off" title="Nu este principal"></i>';
        html += '</div>';
        html += '<div class="active">';
        html += (element.active)
            ? '<i class="fa fa-check-circle" title="Este activ"></i>'
            : '<i class="fa fa-check-circle off" title="Este inactiv"></i>';
        html += '</div>';
        html += '<div class="edit">';
        html += '<a href="/catalog/products/edit/' + element.p_id + '" title="Editeaza produs" ' +
            'class="btn btn-default btn-xs" target="_blank">';
        html += '<i class="fa fa-pencil-square"></i></a>';
        html += '</div>';
        html += '<div class="duplicate">';
        html += '<a href="/catalog/products/add/' + element.p_id + '" title="Duplica produs" ' +
            'class="btn btn-default btn-xs" target="_blank">';
        html += '<i class="fa fa-files-o"></i></a>';
        html += '</div>';
        html += '<div class="shop">';
        html += '<a href="https://www.gsmnet.ro/cauta/' + element.pan_id + '" title="Vezi produsul in shop" ' +
            'class="btn btn-default btn-xs" target="_blank">';
        html += '<i class="fa fa-desktop"></i></a>';
        html += '</div>';
        html += '<div class="statistics">';
        html += '<a href="/statistics/supplies/acquisition?ProductAliasName%5Bid_or_code%5D=' + element.pan_id + '&ProductAliasName[is_active][n][]=1&ProductAliasName[is_active][n][]=0" title="Vezi produsul in statistica" ' +
            'class="btn btn-default btn-xs" target="_blank">';
        html += '<i class="fa fa-pie-chart"></i></a>';
        html += '</div>';
        html += '</div>';
        return html;
    };

    /**
     * Trigger the association process when clicking on a result element
     */
    $("#associate_product .well").on('click', '.trigger', function() {
        var orderId = parseInt($('#order-id').val());
        var orderProductId = parseInt($('#associate_product').find('input[name=iop_id]').val());
        var associatedProductId = parseInt($(this).data('associated-product'));
        var $tr = $('tr[id="'+orderProductId+'"]');
        var oldAssociatedProductId = parseInt($tr.find('input[name=product_id]').val());
        if(isNaN(oldAssociatedProductId)) {
            oldAssociatedProductId = 0;
        }
        var active = $.parseJSON($(this).data('active'));
        var popupMessage = '';
        if(!active) {
            popupMessage += 'ATENTIE: Produsul selectat nu este activ.\n\r';
        }
        popupMessage += 'Esti sigur(a) ca vrei sa schimbi produsul asociat?';
        if(confirm(popupMessage)) {
            associateProducts(orderId, orderProductId, associatedProductId, oldAssociatedProductId);
        }
    });

    /**
     * The association process
     * (checks for association integrity)
     * @param iop_id        - InboundOrderProduct.id
     * @param p_id          - New Associated Product.id
     * @param old_p_id      - Existing Associated Product.id
     */
    var associateProducts = function(orderId, iop_id, p_id, old_p_id) {
        var data = {
            iop_id: iop_id,
            p_id: p_id,
            old_p_id: old_p_id
        };
        $.ajax({
            url: '/suppliers/inbound_orders/performAssociation.json',
            method: 'POST',
            data: data
        })
            .done(function(msgPerfAsso) {
                if (msgPerfAsso.status === 'success') {
                    $.ajax({
                        url: '/suppliers/inbound_orders/view/' + orderId + '/' + iop_id + '.json',
                        data: {
                            display: administration
                        },
                        dataType: 'JSON'
                    })
                        .done(function (msg) {
                            if (msg.status === 'success') {
                                $('#associate_product').modal('hide');
                                if (msg.data) {
                                    var iop = msg.data;
                                    var $tr = $('tr[id="' + iop_id + '"]');
//                                        resetValues($tr); $tr.find('.pan').html(templateNewAssociation(product));
                                    updateOnAssociationChange($tr, iop);
                                }
                                toastr_putMessage(msgPerfAsso.message, msgPerfAsso.status, 3000); // da, msgPerfAsso nu msg
                                getOrderStatistics();
                            } else {
                                toastr_putMessage('Actualizare date produs esuata. ' + '\n' + msgPerfAsso.message, msgPerfAsso.status, 3000);
                            }
                        })
                        .fail(function(fmsg) {
                            ajaxFailCatch(fmsg,'');
                        });
                } else {
                    toastr_putMessage('Asociere esuata. ' + '\n' + msgPerfAsso.message, msgPerfAsso.status, 3000);
                }
            })
            .fail(function(fmsg) {
                ajaxFailCatch(fmsg,'');
            })
        ;
    };

    /**
     * Function that updates html data after association change
     * @param $tr           - the TD element (row)
     * @param iop           - object with new product data
     * @todo:               adauga si element pt  stoc inainte de NIR
     */

    //TODO nu mai functioneaza aactualizarea datelor de cooronate pt ca am schimbat html-ul;
    var updateOnAssociationChange = function($tr, iop) {
        $tr.find('input[name=product_id]').val((iop !== null) ? iop['Product']['id'] : '');
        $tr.attr('data-product_alias_name', (iop == null) ? '' : iop['Product']['ProductAliasName']['name']);

        if (iop == null){
            $tr.find('.p-urls').addClass('hidden');
        }else{
            $tr.find('.p-urls').removeClass('hidden');
        }
        $tr.find('.p-url').attr('href', (iop == null) ? '' : 'https://www.gsmnet.ro/cauta/'
        + iop['Product']['ProductAliasName']['id']);
        $tr.find('.edit-url').attr('href', (iop == null) ? '' : '/catalog/products/edit/'
        + iop['Product']['id']);
        $tr.find('.stats-url').attr('href', (iop == null) ? ''
            : '/statistics/supplies/acquisition?ProductAliasName%5Bid_or_code%5D='
        + iop['Product']['ProductAliasName']['id'] + '&ProductAliasName[is_active][n][]=1&ProductAliasName[is_active][n][]=0');
        $tr.find('.m-flow-url').attr('href', (iop == null) ? '' : '/merchandise/entries/flow?product_id='
        + iop['Product']['ProductAliasName']['id'] + '&nir_only=on');
        $tr.find('.m-entries-url').attr('href', (iop == null) ? '' : '/merchandise/entries/checkStock?product_id='
        + iop['Product']['ProductAliasName']['id']);

        if (iop == null){
            $tr.find('.image-popover').addClass('hidden');
        }else{
            $tr.find('.image-popover').removeClass('hidden');
            if(iop['Product']['MainImage']['image']) {
                var $img_path = thumbs_path;
                var $img_name = '/products/' + iop['Product']['id'] + '/' + iop['Product']['MainImage']['image'];
                var $large_img = $img_path + '433x367' + $img_name;
                var $small_img = $img_path + '95x95' + $img_name;
                $tr.find('.image-popover').attr('data-img', $large_img);
                $tr.find('.image-popover').find('img').attr('src', $small_img);
                $tr.find('.image-popover').find('img').attr('alt', iop['Product']['ProductAliasName']['id']);

                // data-img="<?=!$p_has_image ? '' : $LargeImgUrl?>">
                //     <img src="<?=!$p_has_image ? '' : $SmallImgUrl?>"
                // alt="<?=!$p_has_image ? '' : $product['Product']['ProductAliasName']['id']?>">

            }else {
                $tr.find('.image-popover').attr('data-img', '');
                $tr.find('.image-popover').find('img').attr('src', '');
                $tr.find('.image-popover').find('img').attr('alt', '');
            }
        }

        if (iop == null){
            $tr.find('.pan-data').addClass('hidden');
            $tr.find('.first-association').removeClass('hidden');
        }else{
            $tr.find('.pan-data').removeClass('hidden');
            $tr.find('.first-association').addClass('hidden');
        }
        $tr.find('.pan-name').val((iop == null) ? '' : iop['Product']['ProductAliasName']['name']);
        $tr.find('.pan-id').val((iop == null) ? '' : iop['Product']['ProductAliasName']['id']);
        $tr.find('.pan-categ-name').val((iop == null) ? '' : iop['InboundOrdersProduct']['cat_name']);
        $tr.find('.p-part-number').val((iop == null) ? '' : iop['Product']['part_number']);
        $tr.find('.p-stock').html((iop == null) ? '' : iop['Product']['ProductAliasName']['stock']);
        $tr.find('.p-average-30').html((iop == null) ? '' : iop['InboundOrdersProduct']['average_30']);


        if (iop !== null) {
            if (iop['InboundOrdersProduct']['is_unique'] == 1) {
                $tr.find('.p-mk-not-unique').addClass('hidden');
            } else {
                $tr.find('.p-mk-not-unique').removeClass('hidden');
            }

            if (iop['Product']['ProductAliasName']['is_active'] == 1) {
                $tr.find('.p-mk-not-active').addClass('hidden');
            } else {
                $tr.find('.p-mk-not-active').removeClass('hidden');
            }

            if (iop['InboundOrdersProduct']['has_been_on_entries'] == 1) {
                $tr.find('.p-mk-firs-entry').addClass('hidden');
            } else {
                $tr.find('.p-mk-firs-entry').removeClass('hidden');
            }
        }

        var $pnote = $tr.find('.admin-notes');
        $pnote.attr('data-pk', (iop == null) ? '' : iop['Product']['id']);
        $pnote.attr('data-url', (iop == null) ? '' : '/catalog/products/updateAdminNotes/' + iop['Product']['id'] + '.json');
        $pnote.attr('data-action', (iop == null) ? '' : '/catalog/products/updateAdminNotes/' + iop['Product']['id'] + '.json');
        $pnote.attr('data-original-value', (iop == null) ? '' : iop['Product']['admin_notes']);
        $pnote.html((iop == null) ? '' : iop['Product']['admin_notes']);
        $pnote.editable();


        if (iop == null){
            $tr.find('.print').addClass('hidden');
            $tr.find('.wh-pos').html('');
        }else {
            $tr.find('.print').removeClass('hidden');
            if (iop['Product']['ProductPosition']['position'] === undefined) {
                $tr.find('.wh-pos').attr('title', 'Produs fara pozitie in magazie');
                $tr.find('.wh-pos').html('Fara <i class="fa fa-crosshairs"></i>');
            } else {
                $tr.find('.wh-pos').attr('title', 'Coordonata in magazie');
                $tr.find('.wh-pos').html(iop['Product']['ProductPosition']['position']);
            }
        }

        $tr.find('.associated_selling_price').html((iop == null) ? '' : iop['Product']['selling_price']);
        $tr.find('.competitors').html((iop == null) ? '' : 'Fa refresh la pagina pentru actualizarea datelor concurentilor');
    }

    //TODO: actualizeaza linia produsului daca la cautare se decteaza ca ascocierea produsul gasit nu este valida cu cea din sesiune curenta (SCENARIU: user X modifica produsul 1 in 2, userul Y cauta produsul 2 care nu exist in view-ul curent dar ca rezultat cautare este identificat)
    //TODO: Imediat dupa adaugare produs editul codului nu functioneaza
    //TODO: Imediat dupa reasociere nota admin a produslui nu este viziba daca nu exista, si nu este vizibila in editor daca exista si nu se salveaza

    //TODO: testeaza editarea unui camp care fost modificat. se actualizeaza valoare modificata de userul 1? testeaza si la adaugare,reasociere,dezasociere prospata
    /**
     * Function that updates html data after adding new product to order
     * @param $tr           - the TD element (row)
     */
    var updateOnAddingElementToOrder = function($tr, iop) {
        $tr.attr('id', (iop == null) ? '' : iop['InboundOrdersProduct']['id']);
        $tr.find('[name=order_amount]').val((iop == null) ? '' : parseInt(iop['InboundOrdersProduct']['order_amount']));
        $tr.find('[name=product_id]').val((iop == null) ? '' : parseInt(iop['Product']['id']));
        $tr.find('.supp_pn').val('');
        $tr.find('.supp_p_name').val('');
        $tr.find('.supp_p_specs').val('');
        $tr.find('.pan-name').val((iop == null) ? '' : iop['Product']['ProductAliasName']['name']);


        if (iop == null) {
            $tr.find('.p-mk-not-imported').addClass('hidden');
        } else {
            if (iop['InboundOrdersProduct']['imported'] == 1) {
                $tr.find('.p-mk-not-imported').addClass('hidden');
            } else {
                $tr.find('.p-mk-not-imported').removeClass('hidden');
            }
        }
        $tr.find('.supp_p_name').html('');
        $tr.find('.supp_p_specs').html('');

        // $tr.find('.pns-line').html('----'); //solutie temporara; codul de update pns trebuie sa se afle in functia updateOnAssociationChange

        // todo: imediat dupa adaugare trebuie sa restezi elemenete clase; Ex: cu sau fara coord, cu sau fara nir, cant liv mai mare, putina sau egala...
        // todo: imediat dupa adaugare nu functioneaza
        //         - butonul de sterge / de reasociere
        //         - editurile pe cantitati si preturi


        // todo: editorul de observatii produs
        var $pnote = $tr.find('.note');
        $pnote.attr('data-pk', (iop == null) ? '' : iop['InboundOrdersProduct']['id']);
        $pnote.attr('data-url', (iop == null) ? '' : '/suppliers/inbound_orders/updateOrdersProductNote/' + iop['InboundOrdersProduct']['id'] + '.json');
        $pnote.attr('data-action', (iop == null) ? '' : '/suppliers/inbound_orders/updateOrdersProductNote/' + iop['InboundOrdersProduct']['id'] + '.json');
        $pnote.attr('data-original-value', (iop == null) ? '' : iop['InboundOrdersProduct']['admin_notes']);
        $pnote.html((iop == null) ? '' : iop['InboundOrdersProduct']['note']);
        $pnote.editable();


        $tr.find('.p-log-url').attr('href', (iop == null) ? '' : '/suppliers/inbound_orders/actionsHistory/'
            + iop['InboundOrder']['id'] + '/' + iop['InboundOrdersProduct']['id']);

        //TODO: daca adauagi acelasi produs ce se intampla ?

        $tr.find('.order_amount').html('---');
        $tr.find('.received_amount').val('');
        $tr.find('.received_amount').attr('data-original-value','');
        $tr.find('.de_amount').val('');
        $tr.find('.de_amount').attr('data-original-value','');
        $tr.find('.missing_amount').val('');
        $tr.find('.missing_amount').attr('data-original-value','');

        $tr.find('.purchase_price').val('');
        $tr.find('.purchase_price').attr('data-original-value','');

        $tr.find('.selling_price').val('');
        $tr.find('.selling_price').attr('data-original-value','');

        // todo: Actualizeaza nr de etichete
        $tr.find('.labels').html(''); //todo actualizeaza html-ul
    };

    /**
     * Trigger for closing search modal form
     */
    $("#associate_product").on('hidden.bs.modal', function() {
        resetSearchForm();
    });

    /**
     * The modal closing process
     */
    var resetSearchForm = function() {
        $("#associate_product input[type=text]").val('');
        $("#associate_product .well").html('');
    };

    /**
     * Trigger for row inbound product search (pan_id, pan_name, part_number)
     * and shows a list of resulted products
     * (uses a timeout to ensure not every key press triggers a search)
     *
     * Second trigger (Enter key) overrides the timeout and calls
     * the search function immediately
     */
    $("#search")
        .on('input', 'input[type=text]', function(e) {
            e.preventDefault();
            window.clearTimeout(search_timeout);
            initiateRowSearch();
            return false;
        })
        .on('keypress', function(e) {
            if (e.keyCode === 13) { // ENTER
                e.preventDefault();
                window.clearTimeout(search_timeout);
                performRowSearch();
            }
        });

    /**
     * Trigger that initiates product search in rows with no parameters
     */
    $('#show_latest_200_in_row_search').on('click', function() {

        $('#search_pan_id').val('');
        $('#search_pan_name').val('');
        $('#search_part_number').val('');
        performRowSearch();
    });

    /**
     * Function that initiates product search in rows action wrapped in a timeout
     */
    var initiateRowSearch = function() {
        search_timeout = window.setTimeout(function() {
            performRowSearch();
        }, timer_timeout);
    };

    /**
     * Function that performs the product search in rows
     */
    var performRowSearch = function() {
        var orderId = parseInt($('#order-id').val());
        var $form = $('#search');
        if(isNaN(orderId)) {
            toastr_putMessage('Nu am putut obtine id-ul comenzii', 'error', 2000);
            return false;
        }
        var $results = $form.find('.well');
        var $wrapper_results = $results.find('.wrapper-results');
        var pan_id = $form.find('#search_pan_id').val();
        var pan_name = $form.find('#search_pan_name').val();
        var part_number = $form.find('#search_part_number').val();

        var only_in_order = 0;
        if($('#search_in').prop('checked') == true){only_in_order = 1;}

//                if(pan_id !== '' || pan_name !== '' || part_number !== '') {
        if($results.is(':hidden')) {
            $results.slideDown(300);
        }
        $wrapper_results.html('<i class="fa fa-gear fa-spin"></i> Incarcare rezultate ...');
        $.ajax({
            url: '/suppliers/inbound_orders/searchInboundProducts/' + orderId + '.json',
            method: 'POST',
            data: {
                pan_id: pan_id,
                pan_name: pan_name,
                part_number: part_number,
                only_in_order: only_in_order,
//                            is_main: is_main,
//                            is_active: is_active
            }
        })
            .done(function (msg) {
                if (msg.status !== '' && msg.status !== 'success') {
                    toastr_putMessage('Cautare esuata. ' + '\n' + msg.message, msg.status);
                    $wrapper_results.html('');
                } else {
                    var results = msg.data;
                    if(results.length > 0) {
                        var list_data = '';
                        for (var i = 0; i < results.length; i++) {
                            list_data += templateTopSearchRow(results[i]);
                        }
                        $wrapper_results.html(list_data);
                        initImagePopOver();
                    } else {
                        $wrapper_results.html('Nu am gasit niciun produs');
                    }
                }
            })
            .fail(function(fmsg) {
                ajaxFailCatch(fmsg,'');
            })
        ;
//                }
    };

    /**
     * Trigger to close the results list
     */
    $('#search .well').on('click', 'i.fa-times-circle', function() {
        closeSearchResultsList();
    });

    /**
     * Trigger to close the results list on ESCAPE key press
     */
    $(document).on('keyup', function(e) {
        if (e.keyCode === 27) { // ESCAPE
            e.preventDefault();
            var $target = $('#search .well');
            if($target.is(':visible')) {
                closeSearchResultsList();
            }
        }
    });

    /**
     * Function that closes the results list
     */
    var closeSearchResultsList = function() {
        $("#search .well").slideUp(150);
    };

    /**
     * Function that generates HTML code for a result row
     * @param element           - object with row data
     * @returns {string}        - html
     */
    var templateTopSearchRow = function(element) {
        var html = '<div class="element">';
        html += '<div class="trigger" data-iop="' + element.iop_id + '" data-in-list="' + element.in_list + '" data-product-id="' + element.p_id + '">';
        html += '<div class="inner">';
        html += '<div class="image"><a class="image-popover" rel="popover" data-img="' + element.image_large + '"' ;
        html += '  data-delay="{\'show\':\'1200\', \'hide\':\'5000\'}" >';
        html += '    <img class="small" src="' + element.image + '">';
        html += '</a></div>';
        html += '<div class="id"><span class="label label-default">' + element.pan_id + '</span></div>';
        html += '<div class="in-list">';
        html += (element.in_list)
            ? '<i class="fa fa-th-list" title="se ragaseste in comanda curenta"></i>'
            : '<i class="fa fa-th-list off" title="nu se ragaseste in comanda curenta"></i>';
        html += '</div>';
        html += '<div class="name">' + element.pan_name + '</div>';
        html += '<div class="qty">' + (($.parseJSON(element.qty_ordered) !== null) ? element.qty_ordered : '--') + '</div>';
        html += '<div class="qty">' + (($.parseJSON(element.qty_received) !== null) ? element.qty_received : '--') + '</div>';
        html += '<div class="part_number">' + element.part_nb + '</div>';
        html += '</div>';
        html += '</div>';
        html += '<div class="main">';
        html += (element.main)
            ? '<i class="fa fa-star" title="Este principal"></i>'
            : '<i class="fa fa-star off" title="Nu este principal"></i>';
        html += '</div>';
        html += '<div class="active">';
        html += (element.active)
            ? '<i class="fa fa-check-circle" title="Este activ"></i>'
            : '<i class="fa fa-check-circle off" title="Este inactiv"></i>';
        html += '</div>';
        html += '<div class="edit">';
        html += '<a href="/catalog/products/edit/' + element.p_id + '" title="Editeaza produs" ' +
            'class="btn btn-default btn-xs" target="_blank">';
        html += '<i class="fa fa-external-link"></i></a>';
        html += '</div>';
        html += '</div>';
        return html;
    };

    /**
     * Trigger row element focus when clicked
     */
    $("#search .well").on('click', '.trigger', function() {
        var orderId = parseInt($('#order-id').val());
        var $results = $(this).parents('.well');
        var IOP_id = parseInt($(this).data('iop'));
        var $tr = $('tr[id="' + IOP_id + '"]');
        var in_list = $.parseJSON($(this).data('in-list'));
        var product_id = $.parseJSON($(this).data('product-id'));
        if(!in_list) {
            if(confirm('Esti sigur(a) ca vrei sa adaugi acest produs in comanda?')) {
                addElementToOrder(product_id, orderId);
            }
        } else {
            $results.slideUp(300);
            scrollToElement($tr, function() {
                $tr.effect('highlight', {}, 1000);
                $tr.find('input.lbl-print').focus();
                $tr.find('input.lbl-print').effect("pulsate", "",3000);
                $('.found-product').removeClass('found-product'); //reset
                $tr.addClass('found-product');
            });
        }
    });

    /**
     * Add a new entry to an InboundOrder with an associated Product
     *
     * @param product_id    - Product.id
     * @param orderId      - InboundOrder.id
     */
    var addElementToOrder = function(p_id, orderId) {
        var data = {
            p_id: p_id,
            io_id: orderId
        };
        $.ajax({
            url: '/suppliers/inbound_orders/addInboundProduct.json',
            method: 'POST',
            data: data
        })
            .done(function(msg1) {
                if (msg1.status === 'success') {
                    var iop_id = msg1.data
                    $.ajax({
                        url: '/suppliers/inbound_orders/view/' + orderId + '/' + iop_id + '.json',
                        data: {
                            display: administration
                        },
                        dataType: 'JSON'
                    })
                        .done(function(msg) {
                            if (msg.status === 'success') {
                                $('#associate_product').modal('hide');
                                if(msg.data) {
                                    var iop = msg.data;

                                    var $table = $('#inbound-order-products')
                                    var $1st_tr = $table.find('tbody tr:first')
                                    var $cloned_tr = $1st_tr.clone();

                                    $table.find($('table tbody')).prepend($cloned_tr);

                                    closeSearchResultsList()
                                    updateOnAddingElementToOrder($cloned_tr, iop);
                                    updateOnAssociationChange($cloned_tr, iop);
                                    resetAlerts($cloned_tr);
                                }
                                getOrderStatistics();
                            } else {
                                toastr_putMessage('Actualizare date produs esuata. ' + '\n' + msg.message, msg.status);
                            }
                        })
                        .fail(function(fmsg) {
                            ajaxFailCatch(fmsg,'');
                        });
                } else {
                    toastr_putMessage('Adaugare esuata. ' + '\n' + msg1.message, msg1.status, 3000)
                }
            })
            .fail(function(fmsg) {
                ajaxFailCatch(fmsg,'');
            });
    };


    /**
     * Scroll animation with callback
     * @param $element          - the element to scroll to
     * @param custom_callback   - optional callback to execute after
     *                            scrolling is complete
     */
    var scrollToElement = function($element, custom_callback) {
        var $header = $('.fixed-header');
        $('html,body').animate({
            scrollTop: $element.offset().top - $header.height() - 50
        }, 500, function() {
            if(typeof custom_callback === 'function') {
                custom_callback();
            }
        });
    };

    //TODO : editul notei admin a produsul imediat dupa adaugare in lisat nu cointine datele precompletate la editul acesteia
    /**
     * Label printing AJAX
     */
    $('.lbl-print').on('keyup', function(e) {
        if (e.keyCode == 13) {
            e.preventDefault();
            var label_element = $(this);
            var print_value = label_element.val();
            print_initiate(label_element,print_value);
        }
    });

    function addSupplierProductPartNumber(supplierId, productId, part_number, callback) {
        if (part_number !== '') {
            $.ajax({
                url: '/suppliers/supplier_products/add.json',
                method: 'POST',
                data: {
                    'data[SupplierProduct][supplier_id]': supplierId,
                    'data[SupplierProduct][product_id]': productId,
                    'data[SupplierProduct][part_number]': part_number,
                }
            })
                .done(function(msg) {
                    if (typeof callback === 'function') {
                        callback(msg);
                    }
                })
                .fail(function(fmsg) {
                    if (typeof callback === 'function') {
                        callback(fmsg);
                    }
                });
        }
    }

    function perform_barcode_association_check($tr) {
        var $barcode_field =  $("#search_pn_ean");
        var barcode_val =  $barcode_field.attr('placeholder');
        var barcode_has_err =  $barcode_field.parent().closest('div').hasClass('has-error');
        //initiaza asocierea daca nu sunt comenzi de printare
        if (barcode_val !== '' && barcode_has_err) {
            if(confirm('Doresti sa asociezi codul {' + barcode_val + '} scanat anterior acestui produs ?')) {
                var sId =  $("#barcode-supplier-id").val();
                var pId = $tr.find('input[name=product_id]').val();
                addSupplierProductPartNumber(sId, pId, barcode_val, function(callbackResult) {
                    var r = callbackResult;
                    if (r.status === 'success') {
                        toastr_putMessage('Codul de bare {' + barcode_val + '} a fost asociat cu succes!',
                            r.status, 3000);
                        $barcode_field.parent().closest('div').removeClass('has-error'); //reset
                        $barcode_field.val(''); //reset
                        $barcode_field.blur();
                    } else if (r.status === 'warning') {
                        toastr_putMessage('Codul de bare {' + barcode_val + '} nu a fost asociat!'
                            + '\r\n' + r.message, r.status, 10000);
                    } else {
                        toastr_putMessage(r.message, r.status, 10000);
                    }
                });
            }
        }
    }

    function print_initiate(label_element, print_value){

        var $trParent = label_element.parents('tr');
        var orderProductId = $trParent.attr('id');

         perform_barcode_association_check($trParent);

        if(print_value !== '') {
            $.ajax({
                url: '/suppliers/inbound_orders/printLabel.json',
                method: 'POST',
                data: {
                    label: print_value,
                    iop_id: orderProductId
                }
            })
                .done(function (msg) {
                    if (msg.status !== 'success') {
                        toastr_putMessage('Printare esuata. ' + '\n' + msg.message, msg.status,3000);
                        label_element.addClass('error');
                    } else {

                        $($trParent).find('input[data-field=received_amount]').val(msg['data']['iop']['InboundOrdersProduct']['received_amount']);

                        label_element.val('');
                        label_element.removeClass('error');

                        $trParent.find('.head_labels_printed').empty();
                        $trParent.find('.labels_printed').empty();

                        if(msg['data']['iop'] && msg['data']['iop']['InboundOrdersProduct']['head_labels_printed'])
                            $trParent.find('.head_labels_printed').append("<i class='fa fa-lg fa-tag'></i>" + msg['data']['iop']['InboundOrdersProduct']['head_labels_printed']);

                        if(msg['data']['iop'] && msg['data']['iop']['InboundOrdersProduct']['labels_printed'])
                            $trParent.find('.labels_printed').append("<i class='fa fa-lg fa-tags'></i>" + msg['data']['iop']['InboundOrdersProduct']['labels_printed']);

                        var order_amount = parseInt($trParent.find('input[name=order_amount]').val());
                        var received_amount = parseInt(msg['data']['iop']['InboundOrdersProduct']['received_amount']); // $trParent.find('.received_amount input').val();

                        resetOrderAmountStatusIndicator($trParent, order_amount, received_amount);

                        if(typeof (msg['data']['labelData']) !== "undefined"){
                            zplLabelBuilder(msg['data']);
                        } else {
                            toastr_putMessage(msg.message, msg.status,100);
                        }
                    }
                })
                .fail(function(fmsg) {
                    ajaxFailCatch(fmsg,'');
                });
        }
    }

    function zplLabelBuilder(mData)
    {
        var unpicked_product_qty = mData['iop']['InboundOrdersProduct']['unpicked_product_qty'];
        var invoice_number = mData['iop']['InboundOrder']['invoice_number'];
        var product_id = mData['iop']['InboundOrdersProduct']['product_id'];
        var entry_id = mData['iop']['InboundOrder']['entry_id'];
        var alias_id = mData['iop']['Product']['ProductAliasName']['id'];
        var part_nr = mData['iop']['Product']['part_number'];
        var stock = mData['iop']['Product']['stock'];
        var stock_before_entry = mData['iop']['InboundOrdersProduct']['stock_before_entry'];
        var has_been_on_entries = mData['iop']['InboundOrdersProduct']['has_been_on_entries'];
        var average = mData['iop']['InboundOrdersProduct']['average_30'];
        var wr_position = mData['iop']['Product']['ProductPosition']['position'];
        if (typeof wr_position == 'undefined') { wr_position = '!Fara'; }
        var order_amount = mData['iop']['InboundOrdersProduct']['order_amount'];
        var received_amount = mData['iop']['InboundOrdersProduct']['received_amount'];

        var headLabelsCopies = parseInt(mData['labelData']['headLabelsCopies']);
        if(headLabelsCopies == 0) { headLabelsCopies = 1; }
        var headLabel = parseInt(mData['labelData']['headLabel']);
        var individualLabels = parseInt(mData['labelData']['individualLabels']);

        var resuplyVar = prettyPrice((average * 3) + 0.01);
        var xStock = stock;
        var highlightNo1 = 0; var highlightNo3 = 0;

        if(entry_id == null || stock_before_entry == null ) {
            if(parseInt(stock) < resuplyVar){highlightNo3 = 1;}
        } else {
            var xStock = stock_before_entry;
            if((parseInt(stock) - parseInt(received_amount)) < resuplyVar){highlightNo3 = 1;}
            if((parseInt(stock_before_entry) == 0 && parseInt(unpicked_product_qty)) > 0){highlightNo1 = 1;}
            if((parseInt(stock) - parseInt(received_amount)) < 0){highlightNo1 = 1;}
        }

        var entryStrZpl;
        var highlightNo1StrZpl = '';  var fieldReverseNo1StrZpl = '';
        var highlightNo3StrZpl = '';  var fieldReverseNo3StrZpl = '';
        //'^AHN,01,01^
        var invoiceFont ='^ADN,27,10';
        if(invoice_number.length < 9){invoiceFont ='^AHN,01,01'}

        if(entry_id !== null) {
            entryStrZpl = '^FO03,01^GB156,35,20^FS' + '\n' +
                '^FT03, 32' + invoiceFont + '^FR^FD' + invoice_number + '^FS' + '\n';
            if(highlightNo1 == 1) {
                highlightNo1StrZpl = '^FO02,35^GB301,33,20^FS' + '\n' + '^FO160,01^GB105,35,20^FS' + '\n';
                fieldReverseNo1StrZpl = '^FR';
            }
        } else {
            entryStrZpl = '^FT03, 32' + invoiceFont + '^FD' + invoice_number + '^FS'  + '\n';
        }

        if(highlightNo3 == 1) {
            highlightNo3StrZpl = '^FT02,160^GB301,60,50^FS' + '\n';
            fieldReverseNo3StrZpl = '^FR';
        }

        var wrPositionStrZpl = '^FT150';
        if(wr_position.length < 20) {
            wrPositionStrZpl = '^FT165';
        }
        wrPositionStrZpl = wrPositionStrZpl + ',153^A0N,65,50' + fieldReverseNo3StrZpl + '^FD' + wr_position + '^FS'  + '\n';

        var parNrStrZpl;
        if(part_nr.length < 20){ //setting the position and font type/size
            parNrStrZpl =  '^FT03,95^CFF,25,00'
        } else if(part_nr.length <28){
            parNrStrZpl =  '^FT03,96^CF0,33,27'
        } else if(part_nr.length <24){
            parNrStrZpl =  '^FT03,96^CF0,33,30'
        } else if(part_nr.length <31){
            parNrStrZpl =  '^FT03,96^A0,33,25'
        } else {
            parNrStrZpl =  '^FT03,95^CF0,33,21'
        }
        if(!has_been_on_entries){ //setting the field reverse and higlight for new product in entries
            parNrStrZpl += '^FR'
            parNrStrZpl = '^FT02,102^GB301,35,20^FS' + '\n' +  parNrStrZpl
        }
        parNrStrZpl += '^FD' + part_nr + '^FS'  + '\n'

        var tearOffPos = '~TA000';
        var defaultBackfeed = '~JSN';
        var lblTop = '^LT0';
        var mediaTracking  = '^MNW'; // W = non-continuous media web sensing
        var mediaType  = '^MTD'; // D = direct thermal media
        var lblOffest = '^LH10,10';
        var dotsPerMillimeter = '^JMA';
        var printRate = '^PR7,4';
        var printSpeed = '~SD10';
        var configurationUpdate = '^JUS'; // S = save current settings
        var printMode = '^MMT'; // T = Tear-off
        var printWidth = '^PW305'; // T = Tear-off
        var labelLength = '^LL0203'; // T = Tear-off

        var configStrZpl =   '^XA' + tearOffPos + defaultBackfeed + lblTop
            + mediaTracking + mediaType +  lblOffest + dotsPerMillimeter
            + printRate + printSpeed + configurationUpdate + '^CI0^XZ'  + '\n'
            + '^XA'  + '\n' + printMode + printWidth + labelLength + '\n';

        var Zpl = configStrZpl +
            entryStrZpl  +
            highlightNo1StrZpl +
            '^FT170,32^ADN,27,20' + fieldReverseNo1StrZpl + '^FD' + received_amount + '^FS'  + '\n' +
            '#IDENTITY#' +
            '^FT190,65^ADN,27,20,' + fieldReverseNo1StrZpl + '^FD' + xStock + '^FS'  + '\n' +
            ''  +
            '^FT03, 60^AAN,20,10' + fieldReverseNo1StrZpl + '^FDm^FS'  + '\n' +
            '^FT14, 60^A0N,28,28' + fieldReverseNo1StrZpl + '^FD' + prettyPrice(average) + '^FS'  + '\n' +
            ''  +
            '^FT94, 60^AAN,20,10' + fieldReverseNo1StrZpl + '^FDn^FS'  + '\n' +
            '^FT105, 60^A0N,28,28' + fieldReverseNo1StrZpl + '^FD' + resuplyVar + '^FS'  + '\n' +
            ''  +
            parNrStrZpl +
            highlightNo3StrZpl +
            wrPositionStrZpl +
            ''  +
            '^FT03,153^CFU01,01' + fieldReverseNo3StrZpl + '^FD' + alias_id + '^FS'  + '\n' +
            '^BY2,3,35^FT50,199^BCN,40,N,N,N,A'  + '\n' +
            '^FD#' + alias_id + '^FS'  + '\n' ;


        // backup: cod de bare lat vechi
        //'^BY3,3,35^FT10,199^BCN,40,N,N,N,A'  + '\n' +

        var copiesStrZpl;
        var tMsg;
        var c;
        var ident;
        var lZpl;
        if(headLabel == 1){
            c = '\n\r' + '^FX        ETICHETA PILOT / lot ' + '( ' + headLabelsCopies  + 'bucati)' + '\n\r';
            ident = '^FO267,04^GC30,30,B^FS'+ '\n' + '^FO274,11^A0N,28,28^FR^FDP^FS' + '\n'; //cerc pt marcarea etichetei pilot
            copiesStrZpl = '^PQ' + headLabelsCopies + ',0,1,Y';
            lZpl = Zpl.replace('#IDENTITY#', ident) + copiesStrZpl + c + '^XZ';
            tMsg ='Se printeaza eticheta(e) pilot in nr de ' + headLabelsCopies + '.';
            ZplPrintSimple(lZpl,1000,tMsg,3000);
        }

        if(individualLabels == 1){
            c = '\n\r' + '^FX        etichete inviduala / produs ' + '( ' + received_amount  + 'bucati)' + '\n\r';
            ident = '^FT255,25^A0N,28,28^FD . ^FS'; //numarul fiecarei bucati de produs
            copiesStrZpl = '^PQ' + received_amount + ',0,1,Y^';
            lZpl = Zpl.replace('#IDENTITY#', ident) + copiesStrZpl + c + '^XZ';
            tMsg ='Se printeaza etichete individuale in nr de ' + received_amount + '.';

            var triggerDelay;
            if(headLabel == 1){triggerDelay=3000;}else{triggerDelay=50;}
            setTimeout(function(){ZplPrintSimple(lZpl,1000,tMsg,3000)},triggerDelay);
        }
    }

    function ZplPrintSimple(ZplCommand,retryTimout,toastrMessage,toastrTimeout)
    {
        console.log(ZplCommand);
        var uri = 'http://labelary.com/viewer.html?units=1&height=1&width=1.5&zpl=' + ZplCommand;
        var encoded = encodeURI(uri.replace('#','%23.'));
        console.log('Vezi eticheta accesand acest link: ' + encoded);

        checkPrinterStatus( function (text){
        if (text == "Gata de printare")
        {
            toastr_putMessage(toastrMessage, '' ,toastrTimeout);
            sendData(ZplCommand);
        }
        else
        {
        }
    });
    }

    function resetOrderAmountStatusIndicator($trParent, order_amount, received_amount)
    {
        var order_amount_element = $trParent.find('.order_amount');
        var order_amount_icon = $trParent.find('.order_amount i');

        var qty_class = '';
        var qty_icon = '';
        var qty_title = 'Cantitate facturata';

        if(order_amount && received_amount)
        {
            if(order_amount == received_amount)
            {
                qty_class = 'received-all';
                qty_icon = 'fa-check';
                qty_title = 'Cantitate facturata';
            }
            else if(order_amount > received_amount)
            {
                qty_class = 'received-less';
                qty_icon = 'fa-chevron-right';
                qty_title = 'Cantitatea facturata este mai mare decat cea receptionata';
            }
            else if(order_amount < received_amount)
            {
                qty_class = 'received-more';
                qty_icon = 'fa-chevron-left';
                qty_title = 'Cantitatea facturata este mai mica decat cea receptionata';
            }
        }
        else if(!received_amount)
        {
            qty_class = 'not-received';
            qty_icon = 'fa-ellipsis-h';
            qty_title = 'Produs nereceptionat';
        }
        else
        {
            qty_class = 'no-order-amount';
            qty_title = 'Cantitate facturata necunoscuta. Produs adaugat ulterior in lista';
        }

        $(order_amount_element).attr('class', 'order_amount ' + qty_class);
        $(order_amount_element).attr('title', qty_title);

        $(order_amount_icon).attr('class', 'fa fa-lg ' + qty_icon + ' ' + qty_class);
    }

    /**
     * jQuery plugin definition for sticky scrolling an element
     */
    $.fn.setSticky = function() {
        var stickyOffset = this.offset().top;
        var $this = $(this);
        $(window).scroll(function() {
            var sticky = $this;
            var tbhead = $this.find('.results-table-content-thead');
            var scroll = $(window).scrollTop();
            if(scroll > stickyOffset) {
                sticky.addClass('fixed');
                tbhead.removeClass('hidden');
            } else {
                sticky.removeClass('fixed');
                tbhead.addClass('hidden');
            }
        });
    };
    $('.fixed-header').setSticky();



    $(".show-in-rows-alerts").hover(function(){
//                $(this).css("color", "black");
        $(this).css("font-weight", "bold");
        $(this).css('cursor','pointer');
        $(this).css('top','-2px');
    }, function(){
//                $(this).css("color", "inherit");
        $(this).css("font-weight", "inherit");
        $(this).css('cursor','auto');
        $(this).css('top','+2px');
    });

//            $('.p-mk-not-unique + :not(:hidden)').css('background-color', 'yellow');
    var alert_cls_itms;
    var i = 0;
    var alert_cls;
    $(".show-in-rows-alerts").click(function() {
//                $(this).effect( "pulsate", "fast");

        var $alert_cls = '';
        $alert_cls =  $(this).attr('data-alert-class');
        var $hidden = true;

        while ($hidden) {
            if (alert_cls !== $alert_cls) i = 0;
            alert_cls = $alert_cls;
            alert_cls_itms = $('.' + $alert_cls);

            if(alert_cls_itms.length == 0) {
                // $(this).css('color','red');
                $(this).effect("pulsate", "",1500);
                toastr_putMessage('Nu exista cazuri de acest gen', 'info', 1000);
                return false;
            }

            var $itm = $(alert_cls_itms[i]);
            $hidden = $itm.hasClass('hidden');
            if (!$hidden)  break;
            i++;
            if (i == alert_cls_itms.length) i = 0;
        }

        var $tr = $itm.closest('tr');
        scrollToElement($tr, function () {
            $tr.find('.' + $alert_cls).effect('highlight', {}, 1000);
            $tr.find('.' + $alert_cls).effect("pulsate", "",2000);
        });
        i++;
        alert_cls = $alert_cls;
        if (i == alert_cls_itms.length) i = 0;
        $(this).fadeIn('fast');

    });

    $(document).on('click','.inv-pr', function(){
        scrollToElementByAliasId($(this).text());
    });

    function scrollToElementByAliasId(aliasId){
        $q = $('.pan-id[value=' + aliasId + ']');
        var $qtr = $q.closest('tr');
        scrollToElement($qtr, function () {
            $qtr.effect('highlight', {}, 1000);
            $('.found-product').removeClass('found-product'); //reset
            $qtr.addClass('found-product');
        });
    }

    /**
     * Trigger to finish the notification task
     */
    $(document).on('click','.task-notification-btn-done', function(){
        var orderId = parseInt($(this).attr('data-inbound-order-id'));
        var task_id = parseInt($(this).attr('data-inbound-orders-task-id'));

        $.ajax({
            url: '/suppliers/inbound_orders/finishTask.json',
            method: 'POST',
            data: {
                task_id: task_id,
                inbound_order_id: orderId
            }
        })
            .done(function (msg) {
                if (msg.status !== '' && msg.status !== 'success') {
                    if (~msg.message.indexOf("Exista produse cu preturi de vanzare invalide")) {
                        toastr_putMessage(msg.message, 'error', 15000);
                        var flash_html = '<div class="row">' +
                            '<div class="col-md-10 col-md-offset-1">' +
                            '<div class="alert alert-danger alert-dismissable mt20">' +
                            '<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>' +
                            'Click pentru a vedea produsele: ' +
                            '<a class="inv-pr"> ' +  msg.data.join('</a>, <a class="inv-pr"> ') + '</a>' +
                            '</div>' +
                            '</div>' +
                            '</div>';
                        $('.fixed-header').prepend(flash_html);
                    } else {
                        toastr_putMessage(msg.message, msg.status);
                    }

                } else {
                    toastr_putMessage(msg.message, msg.status);


                    if(msg.data) {
                        setTasksState(msg.data.tasks,0);
                    }

                    if(typeof(msg.data) != "undefined" && msg.data !== null) {
                        generateNotificationTasks(msg.data);
                    }
                }


            })
            .fail(function(fmsg) {
                ajaxFailCatch(fmsg,'');
            });
    });

    function setTasksState(tasks, displayToasterOnStateChange)
    {
        var notifications_tasks_info_container =  $('#tasks_state_representation_container');
        // var notification_tasks_representation = $('#tasks_state_representation'); ?? schimbat de  pe <i> pe <a?
        var notification_tasks_representation =  $('#tasks_state_representation_container');
        // notification_tasks_representation.removeClass('btn-info');

        var current_tasks_state;
        if (notification_tasks_representation.hasClass('btn-default')) {
            current_tasks_state = 0;
            notification_tasks_representation.removeClass('btn-default');
        }

        if (notification_tasks_representation.hasClass('btn-success')) {
            notification_tasks_representation.removeClass('btn-success');
            current_tasks_state = 2;
        }

        if (notification_tasks_representation.hasClass('btn-info')) {
            notification_tasks_representation.removeClass('btn-info');
            current_tasks_state = 1;
        }


        var html = '';

        var tasks_state = 0; // no existing tasks
        var tasks_message = 'Nu exista task-uri';

        if(tasks && tasks.length > 0)
        {
            tasks_state = 1;
            var last_task = tasks[tasks.length - 1];
            var date_to_display, date_str;

            if(last_task.InboundOrdersTask.is_finished == 1)
            {
                date_to_display = new Date(last_task.InboundOrdersTask.updated_at);
                date_str = ((date_to_display.getDate() < 10) ? ("0" + date_to_display.getDate()) : date_to_display.getDate())
                    + "-"  + ((date_to_display.getMonth() < 10) ? ("0" + date_to_display.getMonth()) : date_to_display.getMonth())
                    + "-" + date_to_display.getFullYear() + " "
                    + ((date_to_display.getHours() < 10) ? ("0" + date_to_display.getHours()) : date_to_display.getHours())
                    + ":" + ((date_to_display.getMinutes() < 10) ? ("0" + date_to_display.getMinutes()) : date_to_display.getMinutes())
                    + ":" + ((date_to_display.getSeconds() < 10) ? ("0" + date_to_display.getSeconds()) : date_to_display.getSeconds());

                tasks_state = 2;
                tasks_message = 'Utilizatorul ' + last_task.Requestee.Person.fname + ' ' + last_task.Requestee.Person.lname +
                    ' a finalizat actualizarea preturilor de vanzare la data de ' + date_str + '.'
            }
            else
            {
                date_to_display = new Date(last_task.InboundOrdersTask.created_at);
                date_str = ((date_to_display.getDate() < 10) ? ("0" + date_to_display.getDate()) : date_to_display.getDate())
                    + "-"  + ((date_to_display.getMonth() < 10) ? ("0" + date_to_display.getMonth()) : date_to_display.getMonth())
                    + "-" + date_to_display.getFullYear() + " "
                    + ((date_to_display.getHours() < 10) ? ("0" + date_to_display.getHours()) : date_to_display.getHours())
                    + ":" + ((date_to_display.getMinutes() < 10) ? ("0" + date_to_display.getMinutes()) : date_to_display.getMinutes())
                    + ":" + ((date_to_display.getSeconds() < 10) ? ("0" + date_to_display.getSeconds()) : date_to_display.getSeconds());

                tasks_message = 'Utilizatorul ' + last_task.Requester.Person.fname + ' ' + last_task.Requester.Person.lname +
                    ' a solicitat utilizatorului ' + last_task.Requestee.Person.fname + ' ' + last_task.Requestee.Person.lname +
                    ' actualizarea preturilor de vanzare la data de ' + date_str + '.';
            }
        }

        notifications_tasks_info_container.attr('title', tasks_message);
        notifications_tasks_info_container.attr('data-original-title', tasks_message);

        if(tasks_state == 1)
            notification_tasks_representation.addClass('btn-info');
        else if(tasks_state == 2)
            notification_tasks_representation.addClass('btn-success');
        else
            notification_tasks_representation.addClass('btn-default');
        if(displayToasterOnStateChange == 1 && current_tasks_state !== tasks_state){
            toastr_putMessage(tasks_message, 'info', 10000);
        }
    }

    /**
     * Create HTML elements for the current notifications tasks
     * @param tasks          - current user tasks
     */
    var generateNotificationTasks = function(data) {

        var notifications_tasks_container =  $('#notification-tasks-container');
        $(notifications_tasks_container).empty();

        var html = '';

        if(data.tasks && data.tasks.length > 0)
        {
            $.each(data.tasks, function (key, value) {

                if (value.Requestee.Person.user_id == data.current_user_id && value.InboundOrdersTask.is_finished == false)
                {
                    html += '<div class="task-notification-container">';
                    html += '<div class="task-notification">';
                    html += '<i class="fa fa-circle-o-notch fa-spin task-notification-spinner"></i>';
                    html += '<p class="task-notification-text">Trebuie sa verificati preturile!</p>';
                    html += '<input type="submit" class="btn btn-xs btn-success task-notification-btn-done" value="Finalizare" ' +
                        'data-inbound-order-id="' + value.InboundOrdersTask.inbound_order_id + '" ' +
                        'data-inbound-orders-task-id="' + value.InboundOrdersTask.id + '" >';
                    html += '</div>';
                    html += '</div>';
                }
            });
        }

        $(notifications_tasks_container).html(html);
    };

    function deleteDocument() {
        $(".delete_file").click(function (event) {
            event.preventDefault();

            if (confirm('Esti sigur(a) ca vrei sa stergi fisierul?')) {

                var order_id = $('#order_with_files').attr('data-id');
                var file_id = $(this).attr('data-id');

                $.ajax({
                    url: '/suppliers/inbound_orders/deleteFile.json',
                    method: 'POST',
                    data: {order_id: order_id, file_id: file_id}
                })
                    .done(function (msg) {
                        toastr_putMessage(msg.message, msg.status, 3000);
                        if (msg.data && msg.data['html']) {
                            $("#documents_form_content").html(msg.data['html']);
                        }

                        // mark btn-order-documents to indicate if it has documents or not
                        if(typeof(msg.data) != "undefined" && msg.data !== null &&
                            typeof(msg.data['has_docs']) != "undefined" && msg.data['has_docs'] !== null ) {
                            if(msg.data['has_docs'] == true) {
                                $(".btn-order-documents").addClass('btn-success');
                            } else {
                                $(".btn-order-documents").removeClass('btn-success');
                            }
                        }

                        deleteDocument();
                    })
                    .fail(function(fmsg) {
                        ajaxFailCatch(fmsg,'');
                    });
            }
        });
    }
    deleteDocument();
    
    $(".btn-upload-order-documents").click(function (event) {
        event.preventDefault();
        $('form#order-documents-form').submit();
    });
    
    /**
     * Trigger for showing modal notifications
     */
    $('a.btn-concurents').on('click', function() {
        $('#inbound_order_concurents').modal('show');
    });

    /**
     * Trigger for showing modal settings
     */
    $('a.btn-settings').on('click', function() {
        $('#view_settings').modal('show');
    });

    /**
     * Trigger for showing modal order documents
     */
    $('a.btn-order-documents').on('click', function() {
        $('#order_documents').modal('show');
    });

    $("#search_pn_ean").on('keypress', function(e) {
        if (e.keyCode === 13) { // ENTER
            e.preventDefault();
            performBarcodeSearch($(this));
        }
    });

    var lastSearch = '';
    var lastProductTr = null;
    var performBarcodeSearch = function($field) {
        var orderId = parseInt($('#order-id').val());
        if(isNaN(orderId)) {
            toastr_putMessage('Nu am putut obtine id-ul comenzii', 'error', 2000);
            return false;
        }


        // intercept print instructions
        var value = $field.val();
        var labels = value.match(/\b(p|((dp|pp)([1-9][0-9]{0,2})?)|ok)\b/i);
        if (labels !== null) {
            if (lastSearch === '') {
                toastr_putMessage('Comanda de printare primita, insa nu ati cautat / scanat nimic inainte.');
                return false;
            }
            var label = labels[0];
            print_initiate(lastProductTr.find('input.lbl-print'), label);

            resetWithPlaceholder($field, lastSearch + ' > ' + value);
            return false;
        }

        // search
        var barcode_val = $field.val();
        $.ajax({
            url: '/suppliers/inbound_orders/searchInboundProductsByBarcode/' + orderId + '.json',
            method: 'POST',
            data: {
                barcode_val: barcode_val
            }
        })
        .done(function (msg) {
            if (msg.status !== '' && msg.status !== 'success') {
                toastr_putMessage('Cautare esuata. ' + '\n' + msg.message, msg.status);
                resetWithPlaceholder($field, barcode_val);
                $field.parent().closest('div').addClass('has-error'); //reset
                $('.found-product').removeClass('found-product'); //reset
                // $('#search_pan_name').focus() //set focus on search by neame
            } else {
                //TODO: trateaza si cazurile cu mai multe rezultate
                //TODO: afiseaza mesaj 'A fost identificat dupa /ean/ bcd/ cod' ???
                var $tr = $('tr[id="' + msg.data.id + '"]');
                if (lastSearch === barcode_val) {
                    print_initiate($tr.find('input.lbl-print'), 'dp1');
                    resetWithPlaceholder($field, barcode_val);
                    return true;
                }
                lastSearch = barcode_val;
                lastProductTr = $tr;
                scrollToElement($tr, function() {
                    $tr.effect('highlight', {}, 1000);
                    // $tr.find('input.lbl-print').focus(); // nu se doreste focus pe acest elemnt ci pe caseta de scanare
                    $tr.find('input.lbl-print').effect("pulsate", "",3000);
                    $('.found-product').removeClass('found-product'); //reset
                    $tr.addClass('found-product');
                });
                // print_initiate($tr.find('input.lbl-print'),'dp'); //nu se doreste initierea printarii
                resetWithPlaceholder($field, barcode_val);
                $field.parent().closest('div').removeClass('has-success'); //reset
                $field.parent().closest('div').removeClass('has-error'); //reset
                $field.parent().closest('div').addClass('has-success'); //reset
            }
        })
        .fail(function(fmsg) {
            ajaxFailCatch(fmsg,'');
        });
    };

    var resetWithPlaceholder = function($input, placeholder_value) {
        $input.attr('placeholder', placeholder_value);
        $input.val('');
        $input.focus();
    };


    $('.product_ean')
        .on('keypress', 'input', function(e) {
            if(e.keyCode === 13) { // ENTER
                var orderId = parseInt($('#order-id').val());
                var $tr = $(this).parents('tr');
                var product_id = $tr.find('input[name="product_id"]').val();
                updateProductEAN(orderId, product_id, $(this).val());
                return false;
            }
        })
        .on('click', 'button', function() {
            var orderId = parseInt($('#order-id').val());
            var $input = $(this).parents('.input-group').find('input');
            var $tr = $(this).parents('tr');
            var product_id = $tr.find('input[name="product_id"]').val();
            updateProductEAN(orderId, product_id, $input.val());
            return false;
        });

    var updateProductEAN = function(orderId, productId, ean_value) {
        if (ean_value !== '') {
            $.ajax({
                url: '/suppliers/inbound_orders/updateProductEAN/' + orderId + '.json',
                method: 'POST',
                data: {
                    product_id: productId,
                    ean: ean_value
                }
            })
            .done(function(msg) {
                toastr_putMessage(msg.message, msg.status, 3000);
            })
            .fail(function(fmsg) {
                ajaxFailCatch(fmsg,'');
            });
        }
    }

    /**
     * Trigger for products association
     * (checks for association integrity and opens bs modal)
     */
    // make sure the association / re-association has the same parameters (iop <-> pid)
    $('table#inbound-order-products').on('click', 'a.associate-barcodes', function() {
        var $this = $(this);
        var $trParent = $this.parents('tr');
        var associatedProductId = $trParent.find('input[name=product_id]').val();
        var $modal = $('#associate_product_barcode');
        $modal.find('input[name=product_id]').val(associatedProductId);
        $modal.find('input[name=ean_barcode]').val('');
        $modal.find('input[name=barcode]').val('');
        $modal.find('input[name=partnumber]').val('');
        $modal.modal('show');
    });

    /**
     * Trigger that initiates association of product EAN
     */
    $('#add_ean_barcode').on('click', function() {
        var $modal = $('#associate_product_barcode');
        var pn = $modal.find('input[name=ean_barcode]').val();
        var sId = $('#ean-supplier-id').val();
        perform_barcode_association(sId,pn);
    });

    /**
     * Trigger that initiates association of product BARCODE
     */
    $('#add_barcode').on('click', function() {
        var $modal = $('#associate_product_barcode');
        var pn = $modal.find('input[name=barcode]').val();
        var sId = $('#barcode-supplier-id').val();
        perform_barcode_association(sId,pn);
    });

    /**
     * Trigger that initiates association of product PART NUMBER
     */
    $('#add_partnumber').on('click', function() {
        var $modal = $('#associate_product_barcode');
        var pn = $modal.find('input[name=partnumber]').val();
        var sId = $('#partnumber-supplier-id').val();
        perform_barcode_association(sId,pn);
    });

    function perform_barcode_association(sId,pn) {
        var $modal = $('#associate_product_barcode');
        var pId = $modal.find('input[name=product_id]').val();

        addSupplierProductPartNumber(sId, pId, pn, function(callbackResult) {
            var r = callbackResult;
            toastr_putMessage(r.message, r.status, 10000);
        });
    }

    //@copy to clipboard
    var clipboard = new ClipboardJS('.x-clipboard');
    clipboard.on('success', function(e) {
        toastr.success('<i>' + e.text + '</i>', 'Copiat in clipboard!');
        e.clearSelection();
    });

    // ToasterJs alert if there is no budget invoice selected
    if (inbound_order_budget_invoice_id === null) {
        toastr_putMessage(
            'Nu ati selectat factura de furnizor pentru aceasta comanda',
            'warning'
        );
    }
});