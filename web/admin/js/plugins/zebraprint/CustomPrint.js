var available_printers = null;
var selected_category = null;
var default_printer = null;
var selected_printer = null;
var format_start = "^XA^LL200^FO80,50^A0N36,36^FD";
var format_end = "^FS^XZ";
var default_mode = true;

function setup_web_print()
{
    console.log('setup_web_print');
	$('#printer_select').on('change', onPrinterSelected);
    console.log('.on(change');
	showLoading("Se incarca datele imprimantei...");
	default_mode = true;
	selected_printer = null;
	available_printers = null;
	selected_category = null;
	default_printer = null;
	
	BrowserPrint.getDefaultDevice('printer', function(printer)
	{
		default_printer = printer
		if((printer != null) && (printer.connection != undefined))
		{
			selected_printer = printer;
			var printer_details = $('#printer_details');
			var selected_printer_div = $('#selected_printer');

			// selected_printer_div.text("Default: " + printer.name);
			selected_printer_div.text(printer.name);
			hideLoading();
			printer_details.show();
			$('#print_form').show();
		}
		BrowserPrint.getLocalDevices(function(printers)
			{
				available_printers = printers;
				var sel = document.getElementById("printers");
				var printers_available = false;
				sel.innerHTML = "";
				if (printers != undefined)
				{
					for(var i = 0; i < printers.length; i++)
					{
						if (printers[i].connection == 'usb')
						{
							var opt = document.createElement("option");
							opt.innerHTML = printers[i].connection + ": " + printers[i].uid;
							opt.value = printers[i].uid;
							sel.appendChild(opt);
							printers_available = true;
						}
					}
				}
				
				if(!printers_available)
				{
					showErrorMessage("Nici o imprimanta Zebra gasita!");
					hideLoading();
					$('#print_form').hide();
					return;
				}
				else if(selected_printer == null)
				{
					default_mode = false;
					changePrinter();
					$('#print_form').show();
					hideLoading();
				}
			}, undefined, 'printer');
	}, 
	function(error_response)
	{
		showBrowserPrintNotFound();
	});
};
function showBrowserPrintNotFound()
{
	// showErrorMessage("An error occured while attempting to connect to your Zebra Printer. You may not have Zebra Browser Print installed, or it may not be running. Install Zebra Browser Print, or start the Zebra Browser Print Service, and try again.");
	showErrorMessage("Eroare la conectare cu imprimanta. </br> Verifica daca aplicatia 'Zebra Browser Print' este pornita. </br> Instaleaza sau porneste aplicatia si reincearca.");

};

function sendDataTest()
{
	showLoading("Se printeaza...");
	checkPrinterStatus( function (text){
		if (text == "Gata de printare")
		{
			// selected_printer.send(format_start + $('#entered_name').val() + format_end, printComplete, printerError);
			selected_printer.send($('#entered_name').val(), printComplete, printerError);
		}
		else
		{
			printerError(text);
		}
	});
};


function sendData(zpl)
{
    showLoading("Se printeaza...");
    checkPrinterStatus( function (text){
        if (text == "Gata de printare")
        {
            // selected_printer.send(format_start + $('#entered_name').val() + format_end, printComplete, printerError);
            selected_printer.send(zpl, printComplete, printerError);
        }
        else
        {
            printerError(text);
        }
    });
};


function checkPrinterStatus(finishedFunction)
{
	selected_printer.sendThenRead("~HQES", 
				function(text){
						var that = this;
						var statuses = new Array();
						var ok = false;
						var is_error = text.charAt(70);
						var media = text.charAt(88);
						var head = text.charAt(87);
						var pause = text.charAt(84);
						// check each flag that prevents printing
						if (is_error == '0')
						{
							ok = true;
							statuses.push("Gata de printare");
						}
						if (media == '1')
							statuses.push("Fara hartie");
						if (media == '2')
							statuses.push("Fara ribon");
						if (media == '4')
							statuses.push("Capac deschis");
						if (media == '8')
							statuses.push("Problema la taiere");
						if (head == '1')
							statuses.push("Cap printare supraincalzit");
						if (head == '2')
							statuses.push("Motor supraincalzit");
						if (head == '4')
							statuses.push("Problema cap printare");
						if (head == '8')
							statuses.push("Cap printare incorect");
						if (pause == '1')
							statuses.push("Printer Paused");
						if ((!ok) && (statuses.Count == 0))
							statuses.push("Error: Unknown Error");
						finishedFunction(statuses.join());
			}, printerError);
};

function hidePrintForm()
{
	$('#print_form').hide();
};
function showPrintForm()
{
	$('#print_form').show();
};
function showLoading(text)
{
	$('#loading_message').text(text);
	$('#printer_data_loading').show();
	hidePrintForm();
	// $('#printer_details').hide();
	$('#printer_select').hide();
};
function printComplete()
{
	hideLoading();
    // // alert ("Printare finalizata");
    // toastr_putMessage('Printare finalizata', 'succes', 1000);
}
function hideLoading()
{
	$('#printer_data_loading').hide();
	if(default_mode == true)
	{
		showPrintForm();
		$('#printer_details').show();
	}
	else
	{
		$('#printer_select').show();
		showPrintForm();
	}
};
function changePrinter()
{
	default_mode = false;
	selected_printer = null;
	// $('#printer_details').hide();
	if(available_printers == null)
	{
		showLoading("Caut imprimanta...");
		$('#print_form').hide();
		setTimeout(changePrinter, 200);
		return;
	}
	$('#printer_select').show();
	onPrinterSelected();
	
}
function onPrinterSelected()
{
	console.log('onPrinterSelected');
	selected_printer = available_printers[$('#printers')[0].selectedIndex];
}
function showErrorMessage(text)
{
	// $('#main').hide();
	$('#error_btn').show();
	$('.btn-settings').addClass('btn-danger');
    $('.btn-settings').effect("pulsate", "",5000);
	// $('#error_message').html(text);
    toastr_putMessage(text, 'error', 10000);
}
function printerError(text)
{
	showErrorMessage("Eroare la printare. Reincearca." + text);
}
function trySetupAgain()
{
    console.log('trySetupAgain');
	// $('#main').show();
	$('#error_btn').hide();
	setup_web_print();
	//hideLoading();
}

$('#printers').on('click', function() {
    console.log('#printers on click');
    $('#printer_select').hide();
    $('#change-printer_btn').show();
})

$('#change-printer_btn').on('click', function() {
    console.log('#change-printer_btn on Click');
    $('#change-printer_btn').hide();
})


