$(function() {
    // Resize
    function resize(){
        $('.resize-height').height(window.innerHeight - 50);
        $('.resize-width').width(window.innerWidth - 250);
        //if(window.innerWidth<=1150){$('.resize-width').css('overflow','auto');}

        }
    $( window ).resize(function() {resize();});
    resize();

    //Add Sections
    $("#newsletter-builder-area-center-frame-buttons-add").hover(
        function() {
            $("#newsletter-builder-area-center-frame-buttons-dropdown").fadeIn(200);
        }, function() {
            $("#newsletter-builder-area-center-frame-buttons-dropdown").fadeOut(200);
        }
    );

    $("#newsletter-builder-area-center-frame-buttons-dropdown").hover(
        function() {
            $(".newsletter-builder-area-center-frame-buttons-content").fadeIn(200);
        }, function() {
            $(".newsletter-builder-area-center-frame-buttons-content").fadeOut(200);
        }
    );


    $("#add-header").hover(function() {
        $(".newsletter-builder-area-center-frame-buttons-content-tab[data-type='header']").show()
        $(".newsletter-builder-area-center-frame-buttons-content-tab[data-type='content']").hide()
        $(".newsletter-builder-area-center-frame-buttons-content-tab[data-type='footer']").hide()
      });

    $("#add-content").hover(function() {
        $(".newsletter-builder-area-center-frame-buttons-content-tab[data-type='header']").hide()
        $(".newsletter-builder-area-center-frame-buttons-content-tab[data-type='content']").show()
        $(".newsletter-builder-area-center-frame-buttons-content-tab[data-type='footer']").hide()
      });

    $("#add-footer").hover(function() {
        $(".newsletter-builder-area-center-frame-buttons-content-tab[data-type='header']").hide()
        $(".newsletter-builder-area-center-frame-buttons-content-tab[data-type='content']").hide()
        $(".newsletter-builder-area-center-frame-buttons-content-tab[data-type='footer']").show()
      });



     $(".newsletter-builder-area-center-frame-buttons-content-tab").hover(
      function() {
        $(this).append('<div class="newsletter-builder-area-center-frame-buttons-content-tab-add"><i class="fa fa-plus"></i>&nbsp;Insert</div>');
        $('.newsletter-builder-area-center-frame-buttons-content-tab-add').click(function() {

        $("#newsletter-builder-area-center-frame-content").append($("#newsletter-preloaded-rows .sim-row[data-id='"+$(this).parent().attr("data-id")+"']").clone());
        hover_edit();
        perform_delete();
        $("#newsletter-builder-area-center-frame-buttons-dropdown").fadeOut(200);
            })
      }, function() {
        $(this).children(".newsletter-builder-area-center-frame-buttons-content-tab-add").remove();
      }
    );


    //Edit
    function hover_edit(){


    $(".sim-row-edit").hover(
      function() {
        $(this).append('<div class="sim-row-edit-hover"><i class="fa fa-pencil" style="line-height:30px;"></i></div>');
        $(".sim-row-edit-hover").click(function(e) {e.preventDefault()})
        $(".sim-row-edit-hover i").click(function(e) {
        e.preventDefault();
        big_parent = $(this).parent().parent();

        //edit image
        if(big_parent.attr("data-type")=='image'){
            upload_image();
        $("#sim-edit-image .image").val(big_parent.children('img').attr("src"));
        $("#sim-edit-image").fadeIn(500);
        $("#sim-edit-image .sim-edit-box").slideDown(500);

        $("#sim-edit-image .sim-edit-box-buttons-save").click(function() {
          $(this).parent().parent().parent().fadeOut(500)
          $(this).parent().parent().slideUp(500)

          big_parent.children('img').attr("src",$("#sim-edit-image .image").val());

           });

        }

        //edit link
        if(big_parent.attr("data-type")=='link'){

        $("#sim-edit-link .title").val(big_parent.text());
        $("#sim-edit-link .url").val(big_parent.attr("href"));
        $("#sim-edit-link").fadeIn(500);
        $("#sim-edit-link .sim-edit-box").slideDown(500);

        $("#sim-edit-link .sim-edit-box-buttons-save").click(function() {
          $(this).parent().parent().parent().fadeOut(500)
          $(this).parent().parent().slideUp(500)

            big_parent.text($("#sim-edit-link .title").val());
            big_parent.attr("href",$("#sim-edit-link .url").val()).attr('title', $("#sim-edit-link .url"));

            });

        }

        //edit title

        if(big_parent.attr("data-type")=='title'){

        $("#sim-edit-title .title").val(big_parent.text());
        $("#sim-edit-title").fadeIn(500);
        $("#sim-edit-title .sim-edit-box").slideDown(500);

        $("#sim-edit-title .sim-edit-box-buttons-save").click(function() {
          $(this).parent().parent().parent().fadeOut(500)
          $(this).parent().parent().slideUp(500)

            big_parent.text($("#sim-edit-title .title").val());

            });

        }

        //edit text
        if(big_parent.attr("data-type")=='text'){

            $('.sim-row-edit-hover', big_parent).remove();

        $("#sim-edit-text .text").val(big_parent.html());
        $("#sim-edit-text").fadeIn(500);
        $("#sim-edit-text .sim-edit-box").slideDown(500);

        $("#sim-edit-text .sim-edit-box-buttons-save").click(function() {
          $(this).parent().parent().parent().fadeOut(500)
          $(this).parent().parent().slideUp(500)
            copied_text = $("#sim-edit-text .text").val();
            //copied_text = copied_text.replace(/(?:\r\n|\r|\n)/g, '<br />');
            big_parent.html(copied_text);
            });
        }

        //edit icon
        if(big_parent.attr("data-type")=='icon'){


        $("#sim-edit-icon").fadeIn(500);
        $("#sim-edit-icon .sim-edit-box").slideDown(500);

        $("#sim-edit-icon i").click(function() {
          $(this).parent().parent().parent().parent().fadeOut(500)
          $(this).parent().parent().parent().slideUp(500)

            big_parent.children('i').attr('class',$(this).attr('class'));

            });

        }

        if(big_parent.attr("data-type")=='image-link'){
            upload_image_link();
            $("#sim-edit-image-link .image").val(big_parent.children('img').attr("src"));
            $("#sim-edit-image-link .url").val(big_parent.attr("href"));
            $("#sim-edit-image-link .title").val(big_parent.children('img').attr("alt"));
            $("#sim-edit-image-link").fadeIn(500);
            $("#sim-edit-image-link .sim-edit-box").slideDown(500);

            $("#sim-edit-image-link .sim-edit-box-buttons-save").click(function() {
                $(this).parent().parent().parent().fadeOut(500)
                $(this).parent().parent().slideUp(500)

                big_parent.children('img').attr("src",$("#sim-edit-image-link .image").val()).attr("alt",$("#sim-edit-image-link .title").val());
                big_parent.attr('href', $("#sim-edit-image-link .url").val());

            });

        }

        if(big_parent.attr("data-type")=='youtube-link'){
            var menuId = "#sim-edit-youtube";
            $(menuId).fadeIn(500);
            $(menuId + " .sim-edit-box").slideDown(500);

            $(menuId + " .sim-edit-box-buttons-save").click(function() {
                $(this).parent().parent().parent().fadeOut(500);
                $(this).parent().parent().slideUp(500);

                var input = ($("#yt-link").val());
                var videoId = input.split('?v=')[1];
                $.ajax({
                    url: ' /loyalty/newsletters/getYoutubeThumbnail.json?vidId=' + videoId,
                    type : 'get',
                    dataType : 'JSON',
                    success: function(r){
                        var thumbnail = r.data['thumb'];
                        var title = r.data['title'];
                        big_parent.css("background-image", 'url(' + thumbnail + ')').attr("title", title);
                    },
                    error: function(r){
                        console.log(r);
                        alert(r.message);
                    }
                });
                big_parent.attr('href', input);
            });
        }

        });
      }, function() {
        $(this).children(".sim-row-edit-hover").remove();
      }
    );

        $('.add_product').mouseenter(function() {
            $(this).append('<div class="product-edit-hover"><i class="fa fa-pencil" style="line-height:30px;"></i></div>');
            $(this).css('background-color', 'rgba(63,141,191,0.2)');

        })
            .mouseleave(function() {
                $(this).children(".product-edit-hover").remove();
                $(this).css('background-color', '#ffffff');
            });
    }
    hover_edit();


    //close edit
    $(".sim-edit-box-buttons-cancel").click(function() {
      $(this).parent().parent().parent().fadeOut(500);
       $(this).parent().parent().slideUp(500);
    });



    //Drag & Drop
    $("#newsletter-builder-area-center-frame-content").sortable({
      revert: true
    });


    $(".sim-row").draggable({
          connectToSortable: "#newsletter-builder-area-center-frame-content",
          //helper: "clone",
          revert: "invalid",
          handle: ".sim-row-move"
    });



    //Delete
    function add_delete(){
        $(".sim-row").append('<div class="sim-row-delete"><i class="fa fa-times" ></i></div>');

        }
    add_delete();


    function perform_delete(){
    $(".sim-row-delete").click(function() {
      $(this).parent().remove();
    });
    }
    perform_delete();


    //Download
    $("#newsletter-builder-sidebar-buttons-abutton").click(function(){

        if ($('#newsletter-name-to-copy').val() == "") {
            $('#newsletter-name-to-copy').focus();
            return false;
        }

        $("#newsletter-preloaded-export").html($("#newsletter-builder-area-center-frame-content").html());
        /*$("#newsletter-preloaded-export .sim-row-delete").remove();
        $("#newsletter-preloaded-export .sim-row").removeClass("ui-draggable");
        $("#newsletter-preloaded-export .sim-row-edit").removeAttr("data-type");
        $("#newsletter-preloaded-export .sim-row-edit").removeClass("sim-row-edit");*/

        export_content = $("#newsletter-preloaded-export").html();

        $("#export-textarea").val(export_content);
        $('#newsletter-name').val($('#newsletter-name-to-copy').val());
        $( "#export-form" ).submit();
        $("#export-textarea").val(' ');

    });
    function edit_product() {
        var parent;
            $(document).on('click', '.product-edit-hover', function(){
                $("#sim-edit-product").fadeIn(500);
                $("#sim-edit-product .sim-edit-box").slideDown(500);
                parent = $(this).parent();
            })

        $("#sim-edit-product .sim-edit-box-buttons-save").click(function() {
            $(this).parent().parent().parent().fadeOut(500)
            $(this).parent().parent().slideUp(500)

            var value = $(this).closest('.sim-edit-box').find('.sim-edit-box-content-field-input').val();
            var cols = parseInt($(this).closest('.sim-edit-box').find('.sim-edit-box-column-select').val());
            var size ='';
            switch(cols){
                case 3 :{
                    size = '140x140';
                    break;
                }
                case 2 :{
                    size = '160x160';
                    break;
                }
                case 1 :{
                    size='354x300';
                    break;
                }
                default:{
                    size='213x215'
                }
            }

            $.ajax({
                url: '/catalog/product_alias_names/getProductDataForNewsletter',
                type: 'post',
                dataType: 'json',
                data: {
                    panid: value,
                    size: size,
                    cols: cols
                },
                success: function(r) {
                    if (r.success == 1) {
                        parent.find('.sim-row-content3-center-tab-text').text(r.name).attr('href', r.path);
                        parent.find('.cart-link').attr('href', r.path);
                        if (r.discount == "") {
                            r.discount = '&nbsp;';
                            parent.find('.discount').css("background-color","#fff");
                        } else {
                            parent.find('.discount').css("background-color","#2e85c6");
                            parent.find('.discount').html(r.discount);
                        }

                        parent.find('.price').text(r.price).parent().attr('href', r.path);
                        if (r.old_price == "") {
                            r.old_price = '&nbsp;';
                            parent.find('.old_price').parent().css("text-decoration","none");
                            parent.find('.old_price').html(r.old_price);
                        } else {
                            parent.find('.old_price').parent().css("text-decoration","line-through");
                            parent.find('.old_price').html(r.old_price);
                            parent.find('.old_price').parent().attr('href', r.path);
                        }
                        parent.find('.sim-row-content3-center-tab-image img').attr('src', r.image).attr('alt', r.name);
                        parent.find('.sim-row-content3-center-tab-image').attr('href', r.path);
                    } else {
                        alert('Produs inexistent');
                    }
                }

            })
        });
    }
    edit_product();

    function upload_image() {
        $('#upload_image_form').submit(function(e){
            var formData = new FormData($('#upload_image_form')[0]);
            form = $(this);
            $.ajax({
                url: '/loyalty/newsletters/upload',  //Server script to process data
                type: 'POST',
                dataType: 'json',
                // Form data
                data: formData,
                /*xhr: function() {  // Custom XMLHttpRequest
                    var myXhr = $.ajaxSettings.xhr();
                    if(myXhr.upload){ // Check if upload property exists
                        myXhr.upload.addEventListener('progress',progressHandlingFunction, false); // For handling the progress of the upload
                    }
                    return myXhr;
                },*/
                //Ajax events
                // beforeSend: beforeSendHandler,
                success: function(r){
                    if (r.success == 1) {
                        form.parent().parent().fadeOut(500)
                        form.parent().slideUp(500)

                        big_parent.children('img').attr("src", r.path);
                    } else {
                        alert("A intervenit o eroare la upload-ul imaginii");
                    }
                },
                error: function (r){
                    alert("A intervenit o eroare la upload-ul imaginii 2");
                },
                //Options to tell jQuery not to process data or worry about content-type.
                cache: false,
                contentType: false,
                processData: false
            });
            return false;
        })

    }

    function upload_image_link() {
        $('#upload_image_link_form').submit(function(e){
            var formData = new FormData($('#upload_image_link_form')[0]);
            form = $(this);
            $.ajax({
                url: '/loyalty/newsletters/upload',  //Server script to process data
                type: 'POST',
                dataType: 'json',
                // Form data
                data: formData,
                /*xhr: function() {  // Custom XMLHttpRequest
                 var myXhr = $.ajaxSettings.xhr();
                 if(myXhr.upload){ // Check if upload property exists
                 myXhr.upload.addEventListener('progress',progressHandlingFunction, false); // For handling the progress of the upload
                 }
                 return myXhr;
                 },*/
                //Ajax events
                // beforeSend: beforeSendHandler,
                success: function(r){
                    if (r.success == 1) {
                        form.parent().parent().fadeOut(500)
                        form.parent().slideUp(500)

                        big_parent.children('img').attr("src", r.path);
                    } else {
                        alert("A intervenit o eroare la upload-ul imaginii");
                    }
                },
                error: function (r){
                    alert("A intervenit o eroare la upload-ul imaginii 2");
                },
                //Options to tell jQuery not to process data or worry about content-type.
                cache: false,
                contentType: false,
                processData: false
            });
            return false;
        })

    }

});

/*
function progressHandlingFunction(e){
    if(e.lengthComputable){
        $('.progress').attr({value:e.loaded,max:e.total});
    }
}*/

