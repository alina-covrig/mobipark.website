<?php
/**
 * This file is loaded automatically by the app/webroot/index.php file after core.php
 *
 * This file should load/create any application wide configuration settings, such as
 * Caching, Logging, loading additional configuration files.
 *
 * You should also use this file to include any files that provide global functions/constants
 * that your application uses.
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link      http://cakephp.org CakePHP(tm) Project
 * @package   app.Config
 * @since     CakePHP(tm) v 0.10.8.2117
 * @license   http://www.opensource.org/licenses/mit-license.php MIT License
 */
//  define('REGEX_PASSWORD_COMPLEXITY', "/^(?:(?=.*[a-z])(?:(?=.*[A-Z])(?=.*[\d\W])|(?=.*\W)(?=.*\d))|(?=.*\W)(?=.*[A-Z])(?=.*\d)).{6,}$/");
 define('REGEX_PHONE', "/((\(\+4\)|\+4))?[-\s]?\(?((01|02|03|04|05|06|07)\d{2})?\)?[-\s]?\d{3}[-\s]?\d{3}$/");
 define('REGEX_REGISTRATION_ROMANIA', "/(J|j|F|f)[0-9]{1,2}\/[0-9]+\/(19|20)([0-9]{2})$/");
 define('REGEX_IBAN', "/[a-zA-Z]{2}[0-9]{2}[a-zA-Z0-9]{20}$/");
 define('REGEX_FULL_USERNAME', "/^[a-zA-Z\-\s]+$/");

// Setup a 'default' cache configuration for use in the application.
Cache::config('default', array('engine' => 'File'));

/**
 * 
 */
 
App::build(
    [
        'Plugin' => [
            ROOT . DS . APP_DIR . DS . 'Plugins'
        ]
    ]
);
CakePlugin::loadAll();
// CakePdf
CakePlugin::load('CakePdf');
Configure::write(
    'CakePdf', [
        'engine' =>'CakePdf.WkHtmlToPdf',
        'encoding' => 'UTF-8'
    ]
);

CakePlugin::load('DebugKit');
CakePlugin::load('ClearCache');

/**
 * Composer - I used a `vendor` folder inside the `Vendor` folder because of the existing non composer folders
 */
App::import('Vendor', array('file' => 'autoload'));
//require_once (APP . 'Vendor' . DS. 'autoload.php');

/**
 * You can attach event listeners to the request lifecycle as Dispatcher Filter. By default CakePHP bundles two filters:
 *
 * - AssetDispatcher filter will serve your asset files (css, images, js, etc) from your themes and plugins
 * - CacheDispatcher filter will read the Cache.check configure variable and try to serve cached content generated from controllers
 *
 * Feel free to remove or add filters as you see fit for your application. A few examples:
 *
 * Configure::write('Dispatcher.filters', array(
 *		'MyCacheFilter', //  will use MyCacheFilter class from the Routing/Filter package in your app.
 *		'MyCacheFilter' => array('prefix' => 'my_cache_'), //  will use MyCacheFilter class from the Routing/Filter package in your app with settings array.
 *		'MyPlugin.MyFilter', // will use MyFilter class from the Routing/Filter package in MyPlugin plugin.
 *		array('callable' => $aFunction, 'on' => 'before', 'priority' => 9), // A valid PHP callback type to be called on beforeDispatch
 *		array('callable' => $anotherMethod, 'on' => 'after'), // A valid PHP callback type to be called on afterDispatch
 *
 * ));
 */
Configure::write(
    'Dispatcher.filters', array(
        'AssetDispatcher',
        'CacheDispatcher'
    )
);

/**
 * Configures default file logging options
 */
App::uses('CakeLog', 'Log');
CakeLog::config(
    'debug', array(
    'engine' => 'File',
    'types' => array('notice', 'info', 'debug'),
    'file' => 'debug',
    )
);
CakeLog::config(
    'error', array(
    'engine' => 'File',
    'types' => array('warning', 'error', 'critical', 'alert', 'emergency'),
    'file' => 'error',
    )
);
include_once('common.php');
