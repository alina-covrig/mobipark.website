<?php
/**
 * This is core configuration file.
 *
 * Use it to configure core behavior of Cake.
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link      http://cakephp.org CakePHP(tm) Project
 * @package   app.Config
 * @since     CakePHP(tm) v 0.2.9
 * @license   http://www.opensource.org/licenses/mit-license.php MIT License
 */

/**
 * CakePHP Debug Level:
 *
 * Production Mode:
 * 	0: No error messages, errors, or warnings shown. Flash messages redirect.
 *
 * Development Mode:
 * 	1: Errors and warnings shown, model caches refreshed, flash messages halted.
 * 	2: As in 1, but also with full debug messages and SQL output.
 *
 * In production mode, flash messages redirect after a time interval.
 * In development mode, you need to click the flash message to continue.
 */

    Configure::write('debug', 2);
//    if ($_SERVER['REMOTE_ADDR'] == '5.2.225.206') {
//        Configure::write('debug', 2);
//    }

/**
 * Configure the Error handler used to handle errors for your application. By default
 * ErrorHandler::handleError() is used. It will display errors using Debugger, when debug > 0
 * and log errors with CakeLog when debug = 0.
 *
 * Options:
 *
 * - `handler` - callback - The callback to handle errors. You can set this to any callable type,
 *   including anonymous functions.
 *   Make sure you add App::uses('MyHandler', 'Error'); when using a custom handler class
 * - `level` - integer - The level of errors you are interested in capturing.
 * - `trace` - boolean - Include stack traces for errors in log files.
 *
 * @see ErrorHandler for more information on error handling and configuration.
 */
    Configure::write(
        'Error', array(
        'handler' => 'ErrorHandler::handleError',
        'level' => E_ALL & ~E_DEPRECATED,
        'trace' => true
        )
    );

/**
 * Configure the Exception handler used for uncaught exceptions. By default,
 * ErrorHandler::handleException() is used. It will display a HTML page for the exception, and
 * while debug > 0, framework errors like Missing Controller will be displayed. When debug = 0,
 * framework errors will be coerced into generic HTTP errors.
 *
 * Options:
 *
 * - `handler` - callback - The callback to handle exceptions. You can set this to any callback type,
 *   including anonymous functions.
 *   Make sure you add App::uses('MyHandler', 'Error'); when using a custom handler class
 * - `renderer` - string - The class responsible for rendering uncaught exceptions. If you choose a custom class you
 *   should place the file for that class in app/Lib/Error. This class needs to implement a render method.
 * - `log` - boolean - Should Exceptions be logged?
 * - `skipLog` - array - list of exceptions to skip for logging. Exceptions that
 *   extend one of the listed exceptions will also be skipped for logging.
 *   Example: `'skipLog' => array('NotFoundException', 'UnauthorizedException')`
 *
 * @see ErrorHandler for more information on exception handling and configuration.
 */
    Configure::write(
        'Exception', array(
        'handler' => 'ErrorHandler::handleException',
        'renderer' => 'ExceptionRenderer',
        'log' => true
        )
    );

/**
 * Application wide charset encoding
 */
    Configure::write('App.encoding', 'UTF-8');

/**
 * To configure CakePHP *not* to use mod_rewrite and to
 * use CakePHP pretty URLs, remove these .htaccess
 * files:
 *
 * /.htaccess
 * /app/.htaccess
 * /app/webroot/.htaccess
 *
 * And uncomment the App.baseUrl below. But keep in mind
 * that plugin assets such as images, CSS and JavaScript files
 * will not work without URL rewriting!
 * To work around this issue you should either symlink or copy
 * the plugin assets into you app's webroot directory. This is
 * recommended even when you are using mod_rewrite. Handling static
 * assets through the Dispatcher is incredibly inefficient and
 * included primarily as a development convenience - and
 * thus not recommended for production applications.
 */
    //Configure::write('App.baseUrl', env('SCRIPT_NAME'));

/**
 * To configure CakePHP to use a particular domain URL
 * for any URL generation inside the application, set the following
 * configuration variable to the http(s) address to your domain. This
 * will override the automatic detection of full base URL and can be
 * useful when generating links from the CLI (e.g. sending emails)
 */
    //Configure::write('App.fullBaseUrl', 'http://example.com');

/**
 * Web path to the public images directory under webroot.
 * If not set defaults to 'img/'
 */
    //Configure::write('App.imageBaseUrl', 'img/');

/**
 * Web path to the CSS files directory under webroot.
 * If not set defaults to 'css/'
 */
    //Configure::write('App.cssBaseUrl', 'css/');

/**
 * Web path to the js files directory under webroot.
 * If not set defaults to 'js/'
 */
    //Configure::write('App.jsBaseUrl', 'js/');

/**
 * 
 */
    //Configure::write('Routing.prefixes', array('admin'));

/**
 * 
 */
if (isset($_GET['time'])) {
    Configure::write('Cache.disable', true);
}
    //Configure::write('Cache.disable', true);

/**
 * 
 */
    Configure::write('Cache.check', true);

/**
 * Enable cache view prefixes.
 *
 * If set it will be prepended to the cache name for view file caching. This is
 * helpful if you deploy the same application via multiple subdomains and languages,
 * for instance. Each version can then have its own view cache namespace.
 * Note: The final cache file name will then be `prefix_cachefilename`.
 */
    Configure::write('Cache.viewPrefix', 'frontend');

/**
 * 
 */
	Configure::write('Session', array(
		'defaults' => 'cache',
        'cookie' => '_bossid'
	));

/**
 * A random string used in security hashing methods.
 */
    Configure::write('Security.salt', 'hfuwresth7w34t0&TY$)fy7e)*)$7t7t7e78r780t34-y7t-');

/**
 * A random numeric string (digits only) used to encrypt/decrypt strings.
 */
    Configure::write('Security.cipherSeed', '763859760547659476095470874509');

/**
 * Apply timestamps with the last modified time to static assets (js, css, images).
 * Will append a query string parameter containing the time the file was modified. This is
 * useful for invalidating browser caches.
 *
 * Set to `true` to apply timestamps when debug > 0. Set to 'force' to always enable
 * timestamping regardless of debug value.
 */
    //Configure::write('Asset.timestamp', true);

/**
 * Compress CSS output by removing comments, whitespace, repeating tags, etc.
 * This requires a/var/cache directory to be writable by the web server for caching.
 * and /vendors/csspp/csspp.php
 *
 * To use, prefix the CSS link URL with '/ccss/' instead of '/css/' or use HtmlHelper::css().
 */
    //Configure::write('Asset.filter.css', 'css.php');

/**
 * Plug in your own custom JavaScript compressor by dropping a script in your webroot to handle the
 * output, and setting the config below to the name of the script.
 *
 * To use, prefix your JavaScript link URLs with '/cjs/' instead of '/js/' or use JsHelper::link().
 */
    //Configure::write('Asset.filter.js', 'custom_javascript_output_filter.php');

/**
 * The class name and database used in CakePHP's
 * access control lists.
 */
    Configure::write('Acl.classname', 'DbAcl');
    Configure::write('Acl.database', 'default');

/**
 * Uncomment this line and correct your server timezone to fix
 * any date & time related errors.
 */
    date_default_timezone_set('Europe/Bucharest');
/**
 * `Config.timezone` is available in which you can set users' timezone string.
 * If a method of CakeTime class is called with $timezone parameter as null and `Config.timezone` is set,
 * then the value of `Config.timezone` will be used. This feature allows you to set users' timezone just
 * once instead of passing it each time in function calls.
 */
    //Configure::write('Config.timezone', 'Europe/Athens');

/**
 * Cache Engine Configuration
 * Default settings provided below
 *
 * File storage engine.
 *
 * 	 Cache::config('default', array(
 *		'engine' => 'File', //[required]
 *		'duration' => 3600, //[optional]
 *		'probability' => 100, //[optional]
 * 		'path' => CACHE, //[optional] use system tmp directory - remember to use absolute path
 * 		'prefix' => 'cake_', //[optional]  prefix every cache file with this string
 * 		'lock' => false, //[optional]  use file locking
 * 		'serialize' => true, //[optional]
 * 		'mask' => 0664, //[optional]
 *	));
 *
 * APC (http://pecl.php.net/package/APC)
 *
 * 	 Cache::config('default', array(
 *		'engine' => 'Apc', //[required]
 *		'duration' => 3600, //[optional]
 *		'probability' => 100, //[optional]
 * 		'prefix' => Inflector::slug(APP_DIR) . '_', //[optional]  prefix every cache file with this string
 *	));
 *
 * Xcache (http://xcache.lighttpd.net/)
 *
 * 	 Cache::config('default', array(
 *		'engine' => 'Xcache', //[required]
 *		'duration' => 3600, //[optional]
 *		'probability' => 100, //[optional]
 *		'prefix' => Inflector::slug(APP_DIR) . '_', //[optional] prefix every cache file with this string
 *		'user' => 'user', //user from xcache.admin.user settings
 *		'password' => 'password', //plaintext password (xcache.admin.pass)
 *	));
 *
 * Memcached (http://www.danga.com/memcached/)
 *
 * Uses the memcached extension. See http://php.net/memcached
 *
 * 	 Cache::config('default', array(
 *		'engine' => 'Memcached', //[required]
 *		'duration' => 3600, //[optional]
 *		'probability' => 100, //[optional]
 * 		'prefix' => Inflector::slug(APP_DIR) . '_', //[optional]  prefix every cache file with this string
 * 		'servers' => array(
 * 			'127.0.0.1:11211' // localhost, default port 11211
 * 		), //[optional]
 * 		'persistent' => 'my_connection', // [optional] The name of the persistent connection.
 * 		'compress' => false, // [optional] compress data in Memcached (slower, but uses less memory)
 *	));
 *
 *  Wincache (http://php.net/wincache)
 *
 * 	 Cache::config('default', array(
 *		'engine' => 'Wincache', //[required]
 *		'duration' => 3600, //[optional]
 *		'probability' => 100, //[optional]
 *		'prefix' => Inflector::slug(APP_DIR) . '_', //[optional]  prefix every cache file with this string
 *	));
 */

/**
 * Configure the cache handlers that CakePHP will use for internal
 * metadata like class maps, and model schema.
 *
 * By default File is used, but for improved performance you should use APC.
 *
 * Note: 'default' and other application caches should be configured in app/Config/bootstrap.php.
 *       Please check the comments in bootstrap.php for more info on the cache engines available
 *       and their settings.
 */
$engine = DEF_CACHE;

// In development mode, caches should expire quickly.
$duration = '+999 days';
/*if (Configure::read('debug') > 0) {
    $duration = '+10 seconds';
}*/

// Prefix each application on the same server with a different string, to avoid Memcache and APC conflicts.
$prefix = 'frontend_';

/**
 * Configure the cache used for general framework caching. Path information,
 * object listings, and translation cache files are stored with this configuration.
 */
Cache::config(
    '_cake_core_', [
        'engine' => $engine,
        'prefix' => $prefix . 'cake_core_',
        'path' => CACHE . 'persistent' . DS,
        'duration' => $duration
    ]
);

/**
 * Configure the cache for model and datasource caches. This cache configuration
 * is used to store schema descriptions, and table listings in connections.
 */
Cache::config(
    '_cake_model_', [
        'engine' => $engine,
        'prefix' => $prefix . 'cake_model_',
        'path' => CACHE . 'models' . DS,
        'duration' => $duration
    ]
);

/**
 * default cache
 */
Cache::config(
    'default', [
        'engine' => $engine,
        'prefix' => $prefix . 'default_',
        'path' => CACHE . 'default' . DS,
        'duration' => '+30 minutes'
    ]
);

Cache::config(
    'volatile', [
        'engine' => $engine,
        'prefix' => $prefix . 'volatile_',
        'path' => CACHE . 'cart' . DS,
        'duration' => '+2 minutes'
    ]
);

Cache::config(
    'shopping_carts', [
        'engine' => $engine,
        'prefix' => $prefix . 'shopping_cart_',
        'path' => CACHE . 'cart' . DS,
        'duration' => '+1 day'
    ]
);

Cache::config(
    'total_invoices', [
        'engine' => $engine,
        'prefix' => $prefix . 'total_invoices_',
        'path' => CACHE . 'total_invoices' . DS,
        'duration' => '+30 minutes'
    ]
);

/**
 * 1h cache
 */
Cache::config(
    'roller', [
        'engine' => $engine,
        'prefix' => $prefix . 'roller_',
        'path' => CACHE . 'roller' . DS,
        'duration' => '+1 hours'
    ]
);

Cache::config(
    'configurations', [
        'engine' => $engine,
        'prefix' => $prefix . 'configurations_',
        'path' => CACHE . 'configurations' . DS,
        'duration' => '+1 year'
    ]
);

Cache::config(
    'request_login', [
        'engine' => $engine,
        'prefix' => $prefix . 'request_login_',
        'path' => CACHE . 'request_login' . DS,
        'duration' => '+10 minutes'
    ]
);

Cache::config(
    'category_tree_menu', [
        'engine' => $engine,
        'prefix' => $prefix . 'category_tree_menu_',
        'path' => CACHE . 'category_tree_menu' . DS,
        'duration' => '+1 year'
    ]
);

Cache::config(
    'announcements', [
        'engine' => $engine,
        'prefix' => $prefix . 'announcements_',
        'path' => CACHE . 'announcements' . DS,
        'duration' => '+30 minutes'
    ]
);

Cache::config(
    'products', [
        'engine' => $engine,
        'prefix' => $prefix . 'products_',
        'path' => CACHE . 'products' . DS,
        'duration' => '+2 hours'
    ]
);

Cache::config(
    'home_products', [
        'engine' => $engine,
        'prefix' => $prefix . 'home_products_',
        'path' => CACHE . 'home_products' . DS,
        'duration' => '+1 day'
    ]
);

Cache::config(
    'router', [
        'engine' => $engine, // Or Xcache, or Apc
        'path' => CACHE . 'routes' . DS,
        'prefix' => $prefix . 'routes_',
        'duration' => '+1 hours'
    ]
);

Cache::config(
    'popups', [
        'engine' => $engine,
        'prefix' => $prefix . 'popups_',
        'path' => CACHE . 'popups' . DS,
        'duration' => '+30 minutes'
    ]
);

Cache::config(
    'campaigns', [
        'engine' => $engine,
        'prefix' => $prefix . 'campaigns_',
        'path' => CACHE. 'campaigns'. DS,
        'duration' => '+1 hours'
    ]
);