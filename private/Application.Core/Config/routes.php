<?php
/**
 * Routes configuration
 *
 * In this file, you set up routes to your controllers and their actions.
 * Routes are very important mechanism that allows you to freely connect
 * different URLs to chosen controllers and their actions (functions).
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link      http://cakephp.org CakePHP(tm) Project
 * @package   app.Config
 * @since     CakePHP(tm) v 0.2.9
 * @license   http://www.opensource.org/licenses/mit-license.php MIT License
 */

    Router::parseExtensions('json', 'xml');
    
    /**
     * Here, we are connecting '/' (base path) to controller called 'Pages',
     * its action called 'display', and we pass a param to select the view file
     * to use (in this case, /app/View/Pages/home.ctp)...
     */

    /**
     * General routes
     */
    Router::connect('/', ['controller' => 'home', 'action' => 'index']);
    Router::connect('/blocuri', ['controller' => 'pages', 'action' => 'buildings']);
    Router::connect('/bloc-1', ['controller' => 'pages', 'action' => 'building1']);
    Router::connect('/galerie', ['controller' => 'pages', 'action' => 'gallery']);
    Router::connect('/tipuri_apartament', ['controller' => 'pages', 'action' => 'apartment_types']);

    Router::connect('/studio1', ['controller' => 'pages', 'action' => 'apartment1']);
    Router::connect('/studio2', ['controller' => 'pages', 'action' => 'apartment2']);
    Router::connect('/apartament-2camere-tip03', ['controller' => 'pages', 'action' => 'apartment3']);
    Router::connect('/apartament-2camere-tip04', ['controller' => 'pages', 'action' => 'apartment4']);
    Router::connect('/apartament-2camere-small-tip04', ['controller' => 'pages', 'action' => 'apartment5']);
    Router::connect('/apartament-2camere-tip05', ['controller' => 'pages', 'action' => 'apartment6']);
    Router::connect('/apartament-3camere-tip01', ['controller' => 'pages', 'action' => 'apartment7']);
    Router::connect('/apartament-3camere-tip06', ['controller' => 'pages', 'action' => 'apartment8']);
    Router::connect('/apartament-3camere-tip07', ['controller' => 'pages', 'action' => 'apartment9']);
    Router::connect('/apartament-4camere-tip08', ['controller' => 'pages', 'action' => 'apartment10']);
    Router::connect('/duplex1', ['controller' => 'pages', 'action' => 'apartment11']);
    Router::connect('/duplex2', ['controller' => 'pages', 'action' => 'apartment12']);



    require CAKE . 'Config/routes.php';


