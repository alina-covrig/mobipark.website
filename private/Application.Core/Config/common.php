<?php
/**
* The alias function
* 
* Calls inflector slug with - as a second parmaeter
* Calls strtolower
* 
* @param string $who the string to return the alias
* 
* @return string
*/
function alias($who) {
    $who = str_replace('&', ' and ', $who);
    $who = str_replace('@', ' at ', $who);
    $who = str_replace('+', ' plus ', $who);
    $who = str_replace(' si ', ' ', $who);
    $who = utf8_encode($who);
    return Inflector::slug (strtolower ($who), '-');
}

/**
* The extract_strings function
* 
* This function takes an associative array and filles the $vector
* array with every string value the associative has
* This is a recursive function
* 
* @param array $array The array to scan for strings
* @param array $vector The built vector of strings
* 
* @return void
*/
function extract_strings($array, &$vector = array()) {
   
    if(is_array($array)) {
        foreach($array as $value){
            if(!is_array($value)) {
                $vector[] = $value;
            } else {
                extract_strings($value, $vector);
            }
        }
    } else {
        if(!is_array($array)) {
            $vector[] = $array;
        }
    }
    if(is_array($vector)) {
        $vector = array_unique($vector);
    }
}

/**
* The price function
* @return float
*/
function price ($price)
{
    $price = round($price, 2);
    return number_format ($price, 2, '.', '');
}

/**
 * @param $price    = pretul asupra caruia se aplica discountul
 * @param $dv       = discount value = suma fixa de redus din pret
 * @param $dp       = discount percent = procentul cu care e redus pretul, dupa
 *                    ce i s-a scazut dv-ul
 * @return float
 */
function p ($price, $dv, $dp)
{
    return price(($price - $dv) * (1 - ($dp/100)));
}

function label($string) {
    return array('label' => $string);
}

/**
 * @param $cif
 * @return bool
 */
function checkCIF($cif=''){
    $cif = trim(strtolower($cif),'ro');

    if (!is_numeric($cif)) {
        return false;
    }
    if (strlen($cif)>10 ) {
        return false;
    }

    $control_key = substr($cif, -1);
    $cif = substr($cif, 0, -1);
    while (strlen($cif)!=9){
        $cif = '0'.$cif;
    }

    $sum =
        $cif[0] * 7 + $cif[1] * 5 + $cif[2] * 3
        + $cif[3] * 2+ $cif[4] * 1 + $cif[5] * 7
        + $cif[6] * 5 + $cif[7] * 3 + $cif[8] * 2;
    $sum = $sum*10;
    $rest = fmod($sum, 11);
    if ($rest==10 ) {
        $rest=0;
    }
    if ($rest == $control_key) {
        return true;
    } else {
        return false;
    }
}

function getUrlFromATag($string) {
    $doc = new DOMDocument();
    $doc->loadHTML($string);
    $anchors = $doc->getElementsByTagName('a');
    foreach($anchors as $node) {
        if ($node->hasAttributes()) {
            foreach($node->attributes as $a) {
                if ($a->name == "href" && !is_null($a->value)) {
                    return str_replace("index/", "",$a->value);
                }
            }
        }
    }
}

/******************************************************************************/
/****                            Validare CNP                              ****/
/******************************************************************************/
/**
 * Validate CNP ( valid for 1800-2099 )
 *
 * @param string $p_cnp
 * @return boolean
 */
function validCNP($p_cnp) {
    // CNP must have 13 characters
    if(strlen($p_cnp) != 13) {
        return false;
    }
    $cnp = str_split($p_cnp);
    unset($p_cnp);
    $hashTable = array( 2 , 7 , 9 , 1 , 4 , 6 , 3 , 5 , 8 , 2 , 7 , 9 );
    $hashResult = 0;
    // All characters must be numeric
    for($i=0 ; $i<13 ; $i++) {
        if(!is_numeric($cnp[$i])) {
            return false;
        }
        $cnp[$i] = (int)$cnp[$i];
        if($i < 12) {
            $hashResult += (int)$cnp[$i] * (int)$hashTable[$i];
        }
    }
    unset($hashTable, $i);
    $hashResult = $hashResult % 11;
    if($hashResult == 10) {
        $hashResult = 1;
    }
    // Check Year
    $year = ($cnp[1] * 10) + $cnp[2];
    switch( $cnp[0] ) {
        case 1  : case 2 : { $year += 1900; } break; // cetateni romani nascuti intre 1 ian 1900 si 31 dec 1999
        case 3  : case 4 : { $year += 1800; } break; // cetateni romani nascuti intre 1 ian 1800 si 31 dec 1899
        case 5  : case 6 : { $year += 2000; } break; // cetateni romani nascuti intre 1 ian 2000 si 31 dec 2099
        case 7  : case 8 : case 9 : {                // rezidenti si Cetateni Straini
        $year += 2000;
        if($year > (int)date('Y')-14) {
            $year -= 100;
        }
    } break;
        default : {
            return false;
        } break;
    }
    return ($year > 1800 && $year < 2099 && $cnp[12] == $hashResult);
}

function generateRandomString($length = 10) {
    $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
    $charactersLength = strlen($characters);
    $randomString = '';
    for ($i = 0; $i < $length; $i++) {
        $randomString .= $characters[rand(0, $charactersLength - 1)];
    }
    return $randomString;
}

/**
 * Function that returns 'selected' for 'class' use in a menu element
 *    depending on action/actions
 *
 * @param array $action
 * @param string $request_action
 * @return string
 */
function setActiveLink($action, $request_action) {
    return (in_array($request_action, $action)) ? "selected" : "";
}

/**
 * Function that return the last modified timestamp for a jss/css group
 *
 * @param $type - possible values ('component', 'gears', 'html5forie')
 * @return integer
 */

function getLastFileTimestamp($vars_type) {
    $js_mtime_script = Cache::read('js_mtime_script_$' . $vars_type, 'volatile');

    if (!$js_mtime_script) {
        $group_config_vars =
            require($_SERVER['DOCUMENT_ROOT'] . '/min/groupsConfig.php');

        $js_mtime_script = [];

        foreach ($group_config_vars[$vars_type] as $js_script) {
            $js_mtime_script[] = filemtime($js_script);
        }

        Cache::write('js_mtime_script_$' . $vars_type, $js_mtime_script, 'volatile');
    }

    return max($js_mtime_script);
}

/**
 * isDoFollow - returns rel=nofollow for links with more than
 * one (characteristic + manufacturer)
 *
 * ex: f:material=piele-sintetica;tip-husa=book-cover -> nofollow
 * ex: f:material=piele-sintetica; -> dofollow
 * ex: f:material=piele-sintetica;p=apple -> dofollow
 * ex: f:material=piele-sintetica;p=apple,samsung -> nofollow
 *
 * @param $link
 * @return string
 */
function isDoFollow($link) {
    // if there are no filters defined, return dofollow
    if (stripos($link, '/f:') === false) {
        return '';
    }

    list($url, $filter_string) = explode('/f:', $link);

    //find all characteristics and manufacturers
    preg_match_all(
        '/([a-zA-Z0-9\-\,]+=([a-zA-Z0-9\-\,]+))+;?/i',
        $filter_string,
        $matches
    );

    $i = 0;

    foreach($matches[1] as $match) {
        list($name, $value) = explode('=', $match);

        // if there are multiple values for a match, return nofollow
        if (stripos($value, ',') !== false) {
            return 'rel="nofollow"';
        }

        // count characteristics that are not manufacturers;
        if ($name != 'p') {
            $i++;
        }

        // s = is for stock availability
        if ($name == 'pret' || $name == 's') {
            return 'rel="nofollow"';
        }

        // if there are more than one characteristic, return nofollow
        if ($i > 1) {
            return 'rel="nofollow"';
        }
    }

    // return dofollow for everything else
    return '';
}

/**
 * utility function used to determine if block of text only contains
 *  "non-content" content (empty <p> or <br> tags)
 *
 *
 * @param $text content to check
 * @return bool true if there's anything besides empty tags
 */

function notEmptyContent($text) {
    $strippedText = strip_tags($text, '<iframe>');
    return (!empty($strippedText)) ? true: false;
}

function computeDiscountPercent($total, $discount) {
    return price(($discount * 100) / $total);
}

/**
 * @param $string String to be truncated
 * @param int $length how many characters until truncation
 * @param string $sufix post-truncation string
 * @return string
 *
 *  Convenience function for elegantly truncating strings.
 */

function stringLimit($string, $length = 50, $sufix = "...")
{
    if (strlen($string) <= $length)
        return $string;

    return (strlen($fragment = substr($string, 0, $length + 1 - strlen($sufix)))
        < strlen($string) + 1 )
        ? preg_replace('/\s*\S*$/', '', $fragment) . $sufix
        : $string;
}

/**
 * @param $currency -- uppercase currency symbol
 * @return string beautiful Uppercase+lowercase currency symbol
 *
 *  Convenience function used for display purposes.
 */

function prettyCurrency($currency) {
    return ucfirst(strtolower($currency));
}


/**
 * @param void
 * @return string a-zA-Z0-9
 *
 *  Convenience function used for generate a string of letters and numbers
 */

function lettersAndNumbers() {
    return implode('', range('a', 'z'))
        . implode('', range('A', 'Z'))
        . implode('', range(0, 9));
}


/**
 * @param string
 * @return string encoded
 *
 *  Convenience function similar with javascript encodeURIComponent
 */

function encodeURIComponent($str) {
    $revert = [
        '%21'=>'!',
        '%2A'=>'*',
        '%27'=>"'",
        '%28'=>'(',
        '%29'=>')',
    ];
    return strtr(rawurlencode(strtr(rawurlencode($str), $revert)), $revert);
}

function searchCampaignVisual($campaign, $element, $type)
{
    $found = null;
    $found_type = null;
    $campaign_assets_dir = Configure::read('STORE_URL') . 'campaigns/'
        . $campaign['Campaign']['id'] . '/assets/';
    if (!empty($campaign['CampaignVisual'])) {
        foreach ($campaign['CampaignVisual'] as $visual) {
            if ($visual['element'] == $element && $visual['type'] == $type) {
                if (!empty($visual['image'])) {
                    $found = str_replace(' ', '%20', $visual['image']);
                    $found_type = 'image';
                } elseif (!empty($visual['color'])) {
                    $found = $visual['color'];
                    $found_type = 'color';
                } elseif (!empty($visual['value'])) {
                    $found = $visual['value'];
                    $found_type = 'value';
                }
                break;
            }
        }
        if (!is_null($found) && $found_type == 'image') {
            $found = $campaign_assets_dir . $found;
        }
    }
    return $found;
}
function transformDateRomaniaMonths($date, $separator = '-')
{
	$str_to_time = strtotime($date);

	$Y = date('Y', $str_to_time);
	$M = date('m', $str_to_time);
	$M = romaniaMonths($M);
	$D = date('j', $str_to_time);

	return $D . $separator . $M . $separator . $Y;
}
function romaniaMonths($month_number)
{
	switch ($month_number) {
		case '01':
			return 'Ianuarie';
		case '02':
			return 'Februarie';
		case '03':
			return 'Martie';
		case '04':
			return 'Aprilie';
		case '05':
			return 'Mai';
		case '06':
			return 'Iunie';
		case '07':
			return 'Iulie';
		case '08':
			return 'August';
		case '09':
			return 'Septembrie';
		case '10':
			return 'Octombrie';
		case '11':
			return 'Noiembrie';
		case '12':
			return 'Decembrie';
	}
}

function sanitizeFilename($filename)
{
    $str = strip_tags($filename);
    $str = preg_replace('/[\r\n\t ]+/', ' ', $str);
    $str = preg_replace('/[\"\*\/\:\<\>\?\'\|]+/', ' ', $str);
    $str = strtolower($str);
    $str = html_entity_decode( $str, ENT_QUOTES, "utf-8" );
    $str = htmlentities($str, ENT_QUOTES, "utf-8");
    $str = preg_replace("/(&)([a-z])([a-z]+;)/i", '$2', $str);
    $str = str_replace(' ', '-', $str);
    $str = rawurlencode($str);
    $str = str_replace('%', '-', $str);
    return $str;
}