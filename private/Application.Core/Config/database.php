<?php
class DATABASE_CONFIG
{
    public $default = array(
        'datasource' => 'Database/Mysql',
        'persistent' => false,
        'host' => 'localhost',
        'login' => 'root',
        'password' => '',
        'database' => 'mobipark1',
        'prefix' => '',
        'encoding' => 'utf8',
    );
}
