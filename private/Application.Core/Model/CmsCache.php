<?php
/**
 * CmsCache.php
 *
 * Inside this Model any model
 * data manipulation methods will be placed
 *
 * PHP version 5.4
 *
 * @category  Frontend
 * @package   Merchandise
 * @author    Daniel Ionascu <daniel.ionascu88@gmail.com>
 * @copyright 2016 Evercloud
 * @license   http://www.php.net/license/3_01.txt  PHP License 3.01
 * @version   SVN: $Id$
 * @link      /CmsCache
 * @since     1.0
 */

App::uses('AppModel', 'Model');


/**
 * CmsCache Model Class
 *
 * Inside this Model any application-wide
 * data manipulation methods will be placed
 *
 * @category  Frontend
 * @package   Merchandise
 * @author    Daniel Ionascu <daniel.ionascu88@gmail.com>
 * @copyright 2016 Evercloud
 * @license   http://www.php.net/license/3_01.txt  PHP License 3.01
 * @version   Release: 1.0
 * @link      /CmsCache
 * @since     Class available since Release 1.0
 */
class CmsCache extends AppModel
{
    public $useTable = 'cms__caches';

    public function write($key, $value, $config = 'default')
    {
        $newKey = explode('$', $key);
        $newKey = $newKey[0];

        $cache = $this->find(
            'first', [
                'conditions' => [
                    'key LIKE ' => '%' . $newKey . '%'
                ]
            ]
        );

        if (isset($cache['CmsCache']['is_active'])
            && $cache['CmsCache']['is_active']) {
            Cache::write($key, $value, $config);
        }
    }
}
