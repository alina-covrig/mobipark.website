<?php
/**
* Banner model file
*
* This is the Banner model
*
* PHP version 5.4
*
* @category  Frontend
* @package   Generic
* @author    Daniel Ionascu <daniel.ionascu88@gmail.com>
* @copyright 2014 Evercloud
* @license   http://www.php.net/license/3_01.txt  PHP License 3.01
* @version   SVN: $Id$
* @link      /Generic/models/Banner
* @since     1.0

App::uses('AppModel', 'Model');

/**
* Banner Model Class
*
* @category  Frontend
* @package   Generic
* @author    Daniel Ionascu <daniel.ionascu88@gmail.com>
* @copyright 2014 Evercloud
* @license   http://www.php.net/license/3_01.txt  PHP License 3.01
* @version   Release: 1.0
* @link      /Generic/models/Banner
* @since     Class available since Release 1.0
*/
class Banner extends AppModel
{
    
    /**
    * The table prefix used by the Person model
    * 
    * @var 
    */
    public $tablePrefix = 'cms__';

    /**
    * Display field
    *
    * @var string
    */
    public $displayField = 'fname';

}
