<?php
/**
* AppModel
*
* Inside this Model any application-wide
* data manipulation methods will be placed
*
* PHP version 5.4
*
* @category  Frontend
* @package   Core
* @author    Lucian Vasile <lucian.vasile@live.com>
* @copyright 2014 Evercloud
* @license   http://www.php.net/license/3_01.txt  PHP License 3.01
* @version   SVN: $Id$
* @link      /models/app-model
* @since     1.0
*/

App::uses('Model', 'Model');


/**
* AppModel Class
*
* Inside this Model any application-wide
* data manipulation methods will be placed
*
* @category  Frontend
* @package   Core
* @author    Lucian Vasile <lucian.vasile@live.com>
* @copyright 2014 Evercloud
* @license   http://www.php.net/license/3_01.txt  PHP License 3.01
* @version   Release: 1.0
* @link      /models/app-model
* @since     Class available since Release 1.0
* @method    findById($id)
* @method    contain($array)
*/
class AppModel extends Model
{
    /**
    * The unbindValidation method
    *
    * Unbinds validation rules and optionally sets the
    * remaining rules to required.
    *
    * @param string $type    remove or keep validation
    * @param array  $fields  The fields
    * @param bool   $require Whether to set 'required'=>true on
    * remaining fields after unbind
    *
    * @return null
    * @access public
    */
    function unbindValidation($type, $fields, $require=false)
    { 
        if ($type === 'remove') { 
            $this->validate = array_diff_key(
                $this->validate, 
                array_flip($fields)
            ); 
        } else { 
            if ($type === 'keep') { 
                $this->validate = array_intersect_key(
                    $this->validate, 
                    array_flip($fields)
                ); 
            } 
        } 
     
        if ($require === true) { 
            foreach ($this->validate as $field=>$rules) { 
                if (is_array($rules)) { 
                    $rule = key($rules); 
                 
                    $this->validate[$field][$rule]['required'] = true; 
                } else { 
                    $ruleName = (ctype_alpha($rules)) ? $rules : 'required'; 
                 
                    $this->validate[$field] = array(
                        $ruleName => array(
                            'rule'=>$rules,
                            'required'=>true
                        )
                    ); 
                } 
            } 
        } 
    }

    public function getErrorsList() {
        $messages = [];
        foreach ($this->validationErrors as $x) {
            foreach ($x as $message) {
                $messages[] = $message;
            }
        }
        return $messages;
    }

    /**
     * The getLastQuery method
     * @param boolean $full_log - requests entire query log
     *
     * @return mixed
     */
    public function getLastQuery($full_log = false)
    {
        $dbo = $this->getDatasource();
        $logs = $dbo->getLog();
        $last_log = end($logs['log']);
        if ($full_log) {
            $last_log = $logs['log'];
            return array_column($last_log, 'query');
        }
        return $last_log['query'];
    }

    /**
     * The Log method
     *
     * This method, available across all app
     * is intended to log every user action.
     * Each time a modification occurs the log method must
     * be called in order t preserve a history of all
     * actions taken
     *
     * The level param is intended to set an importance level
     * for the log line. It has values from 0 to 5 where
     * 0 is a fatal error and 5 is a simple log.
     * The values are as follows:
     * 0 - Fatal error
     * 1 - I/O error
     * 2 - Warning
     * 3 - Notice
     * 4 - Info
     * 5 - Log
     *
     * @param string  $message to be logged
     * @param integer $user_id the user id that made the action
     * @param string  $model   name of the model affected
     * @param string  $link    link to the page where the action was made
     * @param integer $row_id  the row id from the model
     * @param integer $level   0 to 5
     * @param string  $data    data that caused the error (form)
     *
     * @return void
     */
    public function log(
        $message,
        $user_id = null,
        $model = '',
        $link = '',
        $row_id = null,
        $level = 5,
        $data = ''
    ) {
        App::uses('Log', 'Model');
        $logModel = new Log();
        $log = array(
            'message' => $message,
            'user_id' => $user_id,
            'model' => $model,
            'row_id' => $row_id,
            'link' => $link,
            'level' => $level,
            'data' => $data,
        );
        $logModel->save($log);
    }

    /**
     * The prepareMatchAgainst method
     *
     * @param  $field_name
     * @param  $str
     * @return string
     */
    public function prepareMatchAgainst($field_name, $str)
    {
        if (stripos($str, '@') !== false) {
            return $field_name . ' LIKE "%' . $str . '%"';
        }

        if (stripos($str, '.') !== false) {
            return $field_name . ' LIKE "%' . $str . '%"';
        }

        $str = str_replace('+', ' ', $str);
        $str = trim(preg_replace('/[^ \w]+/', '', $str));
        $parts = explode(' ', $str);
        $str_match_against = 'MATCH(' . $field_name . ') AGAINST ("';

        foreach ($parts as $part) {
            $part=trim($part);
            if (!empty($part)) {
                $str_match_against .= '+' . $part . '* ';
            }
        }

        $str_match_against = trim($str_match_against);
        $str_match_against .= '" IN BOOLEAN MODE)';

        return $str_match_against;
    }

    /**
     * The prepareMatchAgainstScore method
     *
     * @param  $field_name
     * @param  $str
     * @return string
     */
    public function prepareMatchAgainstScore($field_name, $str)
    {
        $str = str_replace('+', ' ', $str);
        $str = trim(preg_replace('/[^ \w]+/', '', $str));
        $parts = explode(' ', $str);

        $exact_match = '';
        $partial_match = '';

        foreach ($parts as $part) {
            $part=trim($part);
            if (!empty($part)) {
                $exact_match .= '+' . $part . ' ';
                $partial_match .= '+' . $part . '* ';
            }
        }

        $score = 'MATCH(' . $field_name . ') AGAINST ("' . $exact_match
            . '" IN BOOLEAN MODE) * 0.7 + MATCH(' . $field_name . ') AGAINST ("'
            . $partial_match . '" IN BOOLEAN MODE) * 0.3';

        return $score;
    }
}
