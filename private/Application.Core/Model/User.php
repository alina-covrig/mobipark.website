<?php
/**
* User.php
*
* Inside this Model any module-wide
* data manipulation methods will be placed
*
* PHP version 5.4
*
* @category  Frontend
* @package   Generic
* @author    Daniel Ionascu <daniel.ionascu88@gmail.com>
* @copyright 2014 Evercloud
* @license   http://www.php.net/license/3_01.txt  PHP License 3.01
* @version   SVN: $Id$
* @link      /modules/Customers/models/user
* @since     1.0
*/

App::uses('AppModel', 'Model');

/**
* User.php
*
* Inside this Model any module-wide
* data manipulation methods will be placed
*
* PHP version 5.4
*
* @category  Frontend
* @package   Generic
* @author    Daniel Ionascu <daniel.ionascu88@gmail.com>
* @copyright 2014 Evercloud
* @license   http://www.php.net/license/3_01.txt  PHP License 3.01
* @version   Release: 1.0
* @link      /users/
* @since     1.0
*/
class User extends AppModel
{
    public $tablePrefix = 'customers__';
    public $actsAs = array('Containable');
    public $displayField = 'email';
    
    public $hasOne = array('Person');


    /**
    * Validation rules
    *
    * @var array
    */
    public $validate = array(
        'email' => array(
            'email' => array(
                'rule' => array('email'),
                'required' => true,
                'message' => 'Va rugam sa introduceti un email'
            ),
            'unique' => array(
                'rule' => 'isUnique',
                'required' => true,
                'message' => 'Acest email este deja inregistrat'
            ),
        ),
        'password' => array(
            'notBlank' => array(
                'rule' => array('notBlank'),
                'message' => 'Completati parola',
                'required' => true,
                'on' => 'create'
            ),
            'complexity' => array(
                'rule' => array('minLength', '6'),
                'message' => 'Parola trebuie sa aiba minim 6 caractere',
                'required' => true,
            )
        ),
        'phone' => array(
            'notBlank' => array(
                'rule' => array('notBlank'),
                'message' => 'Completati telefonul',
                'required' => true,
            ),
            'is_phone' => array(
                'rule' => REGEX_PHONE,
                'message' => 'Telefonul nu este valid',
            ),
        ),
    );
}
