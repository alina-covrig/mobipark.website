<?php
/**
* Person.php
*
* Inside this Model any module-wide
* data manipulation methods will be placed
*
* PHP version 5.4
*
* @category  Frontend
* @package   Generic
* @author    Daniel Ionascu <daniel.ionascu88@gmail.com>
* @copyright 2014 Evercloud
* @license   http://www.php.net/license/3_01.txt  PHP License 3.01
* @version   SVN: $Id$
* @link      /modules/Customers/models/person
* @since     1.0
*/

/**
* Person.php
*
* Inside this Model any module-wide
* data manipulation methods will be placed
*
* PHP version 5.4
*
* @category  Frontend
* @package   Generic
* @author    Daniel Ionascu <daniel.ionascu88@gmail.com>
* @copyright 2014 Evercloud
* @license   http://www.php.net/license/3_01.txt  PHP License 3.01
* @version   Release: 1.0
* @link      /modules/Customers/models/person
* @since     1.0
*/

App::uses('AppModel', 'Model');


/**
* Person Model Class
*
* Inside this Model any application-wide
* data manipulation methods will be placed
*
* @category  Frontend
* @package   Customers
* @author    Daniel Ionascu <daniel.ionascu88@gmail.com>
* @copyright 2014 Evercloud
* @license   http://www.php.net/license/3_01.txt  PHP License 3.01
* @version   Release: 1.0
* @link      /persons
* @since     Class available since Release 1.0
*/
class Person extends AppModel
{
    
    /**
    * The table prefix used by the Person model
    * 
    * @var 
    */
    public $tablePrefix = 'customers__';

    /**
    * Display field
    *
    * @var string
    */
    public $displayField = 'fname';

    /**
    * Validation rules
    *
    * @var array
    */
    public $validate = array(
        'fname'  => array(
            'notBlank' => array(
                'rule'    => 'notBlank',
                'required'=> true,
                'message' => 'Prenumele este obligatoriu',
            ),
            'is_name' => array(
                'rule' => REGEX_FULL_USERNAME,
                'message' => 'Prenumele trebuie sa contina doar litere, fara diacritice'
            ),
        ),
        'lname'  => array(
            'notBlank' => array(
                'rule'    => 'notBlank',
                'required'=> true,
                'message' => 'Numele este obligatoriu',
            ),
            'is_name' => array(
                'rule' => REGEX_FULL_USERNAME,
                'message' => 'Numele trebuie sa contina doar litere, fara diacritice'
            ),
        )
    );
}
