<?php
/**
* Configuration.php
*
* This file contains the Configuration model
*
* PHP version 5.4
*
* @category  Frontend
* @package   Generic
* @author    Lucian Vasile <lucian.vasile@live.com>
* @copyright 2014 Evercloud
* @license   http://www.php.net/license/3_01.txt  PHP License 3.01
* @version   SVN: $Id$
* @link      /modules/models/configuration
* @since     1.0
*/

App::uses('AppModel', 'Model');


/**
* Configuration Class
*
* Inside this Configuration any application-wide
* data manipulation methods will be placed
*
* @category  Frontend
* @package   Generic
* @author    Lucian Vasile <lucian.vasile@live.com>
* @copyright 2014 Evercloud
* @license   http://www.php.net/license/3_01.txt  PHP License 3.01
* @version   Release: 1.0
* @link      /modules/models/configuration
* @since     Class available since Release 1.0
*/
class Configuration extends AppModel
{
    public $tablePrefix = '';
}
