<?php

App::uses('AppController', 'Controller');

/**
 * Class UsersController
 *
 * @property UserCompany $UserCompany
 * @property UserPayment $UserPayment
 * @property City $City
 * @property Notice $Notice
 * @property NoticeResponse $NoticeResponse
 */
class UsersController extends AppController
{

    /**
     * This controller does not use a model
     *
     * @var array
     */
    public $uses = array(
        'User',
        'UserCompany',
        'UserPayment',
        'City',
        'Street',
        'County',
        'Notice',
        'Page',
        'NoticeResponse'
    );

    public $components = array(
        'Paginator',
        'Session',
        'RequestHandler' => array(
            'viewClassMap' => array('pdf' => 'CakePdf.Pdf'),
        ),
    );

    /**
     * The beforeFilter callback method
     *
     * @return void
     */
    public function beforeFilter()
    {
        parent::beforeFilter();
        $this->set('meta_index', 'noindex, nofollow');
    }

    public function authentication()
    {
        if ($this->request->is('post')) {
            $this->login();
        }
    }

    /**
     * The logout method
     *
     * This method deletes the login Cookies
     *
     * @return redirect
     */
    public function logout()
    {
        $this->Cookie->delete(sha1('User'));
        $this->Cookie->delete(sha1('Person'));
        $this->redirect(['controller' => 'home', 'action' => 'index']);
    }

    /**
     * The register method
     *
     * This method register an user
     *
     * @return redirect
     */
    public function register()
    {
        if ($this->request->is('post')) {
            $this->User->set($this->request->data);
            $errors = $this->User->invalidFields();
            if (empty($errors)) {
                $email = $this->request->data['User']['email'];
                $password = $this->request->data['User']['password'];

                $this->request->data['User']['password']
                    = sha1($this->request->data['User']['password']);
                $this->request->data['User']['is_email_confirmed'] = 0;
                $this->request->data['User']['hash'] = sha1($email . $password);
                $hash = $this->request->data['User']['hash'];

                $trans = $this->User->getDataSource();
                $trans->begin();

                if ($this->User->saveAssociated($this->request->data)) {
                    $email_vars = [
                        'email' => $email,
                        'user_id' => $this->User->id,
                        'user_name' => $this->request->data['Person']['fname']
                            . ' ' . $this->request->data['Person']['lname'],
                        'hash' => $hash
                    ];

                    if (!$this->_sendConfirmationEmail($email_vars)) {
                        $trans->rollback();
                        return $this->respond(
                            __('Eroare la trimiterea e-mailului de confirmare'),
                            'error'
                        );
                    }

                    $trans->commit();
                    $this->Session->write('User.register_email', $email);
                    $this->redirect([
                        'controller' => 'users', 'action' => 'confirmation'
                    ]);
                } else {
                    $this->respond(
                        __('Utilizatorul nu a putut fi creat. Incearca din nou!'),
                        'error',
                        Router::url(['controller' => 'users', 'action' => 'register'])
                    );
                }
            }
        }
    }

    public function confirmation()
    {
        $this->set('user_email', $this->Session->read('User.register_email'));
    }

    /**
     * The activate method
     *
     * @param string $hash The hash
     *
     * @return redirect
     */
    public function activate($hash = null)
    {
        $this->layout = false;
        $this->autoRender = false;

        if (is_null($hash)) {
            return $this->respond(
                __('Cod incorect.'),
                'error',
                Router::url(['controller' => 'home', 'action' => 'index'])
            );
        }

        $user = $this->User->find(
            'first',
            [
                'conditions' => [
                    'hash' => $hash
                ],
                'contain' => [
                    'Person'
                ]
            ]
        );
        if (empty ($user)) {
            return $this->respond(
                __('Cod incorect.'),
                'error',
                Router::url(['controller' => 'home', 'action' => 'index'])
            );
        }
        $this->User->id = $user['User']['id'];
        $this->User->saveField('is_email_confirmed', 1);

        $forEmail = [];
        $forEmail['email'] = $user['User']['email'];
        $forEmail['userid'] = $user['User']['id'];
        $forEmail['name'] = $user['Person']['fname']
            . ' ' . $user['Person']['lname'];

        $this->_sendRegistrationDataEmail($forEmail);

        // auto-login
        if ($this->Session->check('backlink')) {
            // ca sa ma duca in '/contul-meu'
            $this->Session->delete('backlink');
        }
        $this->login($user['User']['hash'], true, $user['User']['id']);
    }

    /**
     * The autoLogin method
     *
     * This method logins the user imediately after registration
     *
     * @param string $email    the email
     * @param string $password the unencrypted password
     *
     * @return boolean
     */
    public function autoLogin($email = null, $password = null)
    {
        $this->layout      = false;
        //         $this-> autoRender = false;
        if (is_null($email) || is_null($password)) {
            return false;
        }
        $this->Session->write('backlink', '/');
        $this->set('e', $email);
        $this->set('p', $password);
        $this->render('/Users/automated_login');
    }

    /**
     * The _sendConfirmationEmail method
     *
     * @param string $for_email Email variables
     *
     * @return boolean
     */
    private function _sendConfirmationEmail($for_email = null)
    {
        $this->layout      = false;
        $this->autoRender  = false;

        $email = $for_email['email'];
        $hash = $for_email['hash'];

        if (is_null($email) || is_null($hash)) {
            return false;
        }

        $baseUrl = Configure::read('WEBSITE_URL');
        $storeName = Configure::read('WEBSITE_NAME');

        $subject = __("Te rugam sa iti confirmi contul pe")." {$baseUrl}";

        $viewVars = array(
            'baseUrl' => $baseUrl,
            'storeName' => $storeName,
            'userId' =>  $for_email['user_id'],
            'code' => $hash
        );

        App::uses('Email', 'Network/Email');
        $em = new CakeEmail('default');

        $em->from([
            Configure::read('SMTP_FROM_EMAIL')
            => Configure::read('SMTP_FROM_NAME')
        ]);
        $em->to($email);
        $em->subject($subject);
        $em->template('user_registration_activation');
        $em->viewVars($viewVars);
        try {
            if ($em->send()) {
                return true;
            } else {
                return false;
            }
        } catch (Exception $e) {
            return false;
        }
    }

    /**
     * The _sendRegistrationDataEmail method
     *
     * @param array $forEmail The email
     *
     * @return boolean
     */
    private function _sendRegistrationDataEmail($forEmail)
    {
        $this->layout      = false;
        $this->autoRender = false;

        $baseUrl = Configure::read('WEBSITE_URL');
        $storeName = Configure::read('WEBSITE_NAME');


        $subject = $forEmail['name'].__(" bine ai venit pe ")." {$storeName}"."!";

        $viewVars = array(
            'baseUrl' => $baseUrl,
            'storeName' => $storeName,
            'email' => $forEmail['email'],
            'userId' =>  $forEmail['userid'],
            'name' => $forEmail['name']
        );

        App::uses('Email', 'Network/Email');
        $em = new CakeEmail('default');

        $em->from([
            Configure::read('SMTP_FROM_EMAIL')
            => Configure::read('SMTP_FROM_NAME')
        ]);
        $em->to($forEmail['email']);
        $em->subject($subject);
        $em->template('user_registration_data');
        $em->viewVars($viewVars);
        if ($em->send()) {
            return true;
        } else {
            return false;
        }

    }

    /**
     * The forgot password method
     *
     * This method helps an user to
     * reset his password
     *
     * @return redirect
     */
    public function forgotPassword()
    {
        if ($this->request->is('post')) {
            if (empty ($this->request->data['User']['email'])) {
                $this->respond(
                    __('Completati campul de email.'),
                    'error'
                );
            }

            if (!empty ($this->request->data['User']['email'])) {
                $email = $this->request->data['User']['email'];
                $this->User->recursive = -1;
                $user = $this->User->findByEmail($email);

                if (empty ($user)) {
                    $this->respond(
                        __('Adresa de email inexistenta.'),
                        'error'
                    );
                }
            }

            if ($this->_sendResetPasswordEmail($user)) {
                $this->respond(
                    __(
                        'Instructiunile de resetare a parolei ' .
                        'au fost trimise. Verificati adresa de email.'
                    ),
                    'success'
                );
            } else {
                $this->respond(
                    __(
                        'Instructiunile de resetare a parolei nu au putut ' .
                        'fi trimise. Incearca din nou.'
                    ),
                    'error'
                );
            }
        }
    }

    /**
     * The _sendResetPasswordEmail method
     *
     * @param array $user The user data
     *
     * @return boolean
     */
    private function _sendResetPasswordEmail($user)
    {
        $baseUrl = Configure::read('WEBSITE_URL');
        $storeName = Configure::read('WEBSITE_NAME');
        $subject = __('Resetare parola cont %s', $storeName);
        $userId = $user['User']['id'];

        $viewVars = [
            'baseUrl' => $baseUrl,
            'storeName' => $storeName,
            'userId' => $userId,
            'user' => $user
        ];

        App::uses('Email', 'Network/Email');
        $em = new CakeEmail('default');

        $em->from([
            Configure::read('SMTP_FROM_EMAIL')
            => Configure::read('SMTP_FROM_NAME')
        ]);
        $em->to($user['User']['email']);
        $em->subject($subject);
        $em->template('reset_password');
        $em->viewVars($viewVars);

        if ($em->send()) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * The reset method
     *
     * @param string $hash The hash
     *
     * @return redirect
     */
    public function reset($hash = null)
    {
        $this->Session->delete('backlink');

        if ($this->request->is('post')) {
            $pass = $this->request->data['User']['password'];
            $npass = $this->request->data['User']['confirm_password'];

            if ($npass != $pass) {
                return $this->respond(
                    __('Parolele nu se potrivesc'),
                    'error',
                    $this->request->referer()
                );
            }

            $this->User->recursive = -1;
            $user = $this->User->findByHash($hash);

            if (empty ($user)) {
                return $this->respond(
                    __('Cod incorect.'),
                    'error',
                    '/'
                );
            }

            $user['User']['password'] = $pass;
            $this->User->set($user);
            unset($this->User->validate['phone']);
            $errors = $this->User->invalidFields();

            if (!empty ($errors)) {
                $msg = '';
                foreach ($errors as $error) {
                    foreach ($error as $message) {
                        $msg .=  $message."<br />";
                    }
                }
                return $this->respond(
                    $msg,
                    'error',
                    $this->request->referer()
                );
            }

            $this->User->id = $user['User']['id'];
            $this->User->saveField('password', sha1($pass));

            return $this->respond(
                __('Parola a fost schimbata. Va puteti autentifica'
                    . ' folosind formularul de autentificare'),
                'success',
                Router::url([
                    'controller' => 'users',
                    'action' => 'authentication'
                ])
            );
        }

        if (is_null($hash)) {
            return $this->respond(
                __('Cod incorect.'),
                'error',
                Router::url(['controller' => 'home', 'action' => 'index'])
            );
        }

        $this->User->recursive = -1;
        $user = $this->User->findByHash($hash, array('User.id'));

        if (empty ($user)) {
            return $this->respond(
                __('Cod incorect.'),
                'error',
                Router::url(['controller' => 'home', 'action' => 'index'])
            );
        }
    }

    /**
     * The getRomanianCities method
     *
     * Get cities from selected county
     *
     * @return mixed
     */
    public function getRomanianCities()
    {
        if (!$this->request->is('post')) {
            die();
        }

        $this->layout = false;
        $this->autoRender = false;

        $county_id = $this->request->data['county'];

        $cities = $this->City->find(
            'list', [
                'conditions' => [
                    'county_id' => $county_id
                ],
                'order' => [
                    'county_seat DESC',
                    'name ASC'
                ]
            ]
        );

        $arr_cities = [];

        $return = '<option value="">' . __('Selecteaza sau cauta localitatea...') . '</option>';

        foreach ($cities as $c) {
            $arr_cities[] = $c;
            $return .= "<option value='" . $c . "'>" . $c . "</option>";
        }
        die($return);
    }

    public function myAccount()
    {
        $user_id = $this->checkIfLoggedIn();
        $user = $this->User->findById($user_id);
        $this->set(compact('user'));

        if ($this->request->is('post')) {
            if (!empty($this->request->data['edit_personal_data_flag'])) {
                $this->User->unbindValidation(
                    'remove',
                    [
                        'password'
                    ],
                    true
                );

                $old_u_data = $this->User->findById($user_id);

                $this->request->data['User']['id'] = $old_u_data['User']['id'];
                $this->request->data['Person']['id'] = $old_u_data['Person']['id'];

                $this->User->set($this->request->data);
                $this->User->id = $user_id;

                if ($old_u_data['User']['email'] != $this->request->data['User']['email']) {
                    $logout_redirect = ['controller' => 'users', 'action' => 'logout'];
                }

                if ($this->User->saveAll($this->request->data)) {
                    $this->refreshIdentity();

                    if (!empty($logout_redirect)) {
                        $this->respond(
                            __('Datele au fost salvate.') . ' '
                            . __('Va trebui sa va autentificati din nou'),
                            'success',
                            $logout_redirect
                        );
                    } else {
                        $this->respond(
                            __('Datele au fost salvate cu succes.'),
                            'success'
                        );
                    }
                } else {
                    $errors = $this->User->invalidFields();
                    $msg = $this->parseErrorsMessages($errors);

                    $this->respond(
                        $msg,
                        'error'
                    );
                }
            }
        }
    }

    /**
     * The myAccountResetPassword method
     *
     * Reset current user password
     *
     * @return mixed
     * @throws Exception
     */
    public function myAccountResetPassword()
    {
        $user_id = $this->checkIfLoggedIn();

        if ($this->request->is('post')) {
            $this->User->recursive = -1;
            $user = $this->User->findById($user_id);

            $old_password = $this->request->data['User']['old-password'];

            if (sha1($old_password) != $user['User']['password']) {
                return $this->respond(
                    __('Parola actuala a fost introdusa gresit'),
                    'error'
                );
            }

            $password = $this->request->data['User']['password'];
            $confirm_password = $this->request->data['User']['confirm-password'];

            if ($password != $confirm_password) {
                return $this->respond(
                    __('Parolele nu se potrivesc'),
                    'error'
                );
            }

            $this->User->set($this->request->data);

            if ($this->User->validates(
                [
                    'fieldList' => [
                        'password'
                    ]
                ]
            )) {
                $user['User']['password'] = sha1($password);
                $user['User']['md5_password'] = md5($password);

                if ($this->User->save($user, ['validate' => false])) {
                    return $this->respond(
                        __('Parola a fost schimbata cu succes.'),
                        'success'
                    );
                } else {
                    return $this->respond(
                        __('Parola nu a putut fi salvata. Incearca din nou.'),
                        'error'
                    );
                }
            } else {
                $errors = $this->User->validationErrors;
                $msg = $this->parseErrorsMessages($errors);

                return $this->respond(
                    $msg,
                    'error'
                );
            }
        } else {
            $this->redirect($this->request->referer());
        }
    }

    public function myCompanies()
    {
        $user_id = $this->checkIfLoggedIn();
        $companies = $this->UserCompany->find(
            'all',
            [
                'conditions' => [
                    'UserCompany.user_id' => $user_id
                ]
            ]
        );
        $this->set(compact(
            'companies'
        ));

        if ($this->request->is('post')) {
            if ($this->request->data['UserCompany']['copy_data']) {
                unset($this->request->data['UserCompany']['copy_data']);
                $copy_data = [];
                $copy_data['UserPayment'] = $this->request->data['UserCompany'];
            }

            $this->UserCompany->create();
            if ($this->UserCompany->save($this->request->data)) {

                if (!empty($copy_data)) {
                    $this->UserPayment->create();
                    $this->UserPayment->save($copy_data);
                }

                return $this->respond(
                    __('Datele au fost adaugate cu succes'),
                    'success'
                );
            }
        } else {
            $this->request->data['UserCompany']['user_id'] = $user_id;
        }
    }

    public function editCompany($user_company_id)
    {
        if (empty($user_company_id)) {
            return $this->respond(
                __('Companie nespecificata'),
                'error'
            );
        }

        $user_company = $this->UserCompany->findById($user_company_id);
        if (empty($user_company)) {
            return $this->respond(
                __('Companie inexistenta'),
                'error'
            );
        }

        $cities = $this->City->find(
            'list', array(
                'conditions' => array(
                    'county_id' => $user_company['UserCompany']['county_id']
                ),
                'fields' => [
                    'City.name',
                    'City.name'
                ],
                'order' => [
                    'county_seat DESC',
                    'name ASC'
                ]
            )
        );
        $this->set(compact('cities'));

        if ($this->request->is(['post', 'put'])) {
            if ($this->UserCompany->save($this->request->data)) {
                return $this->respond(
                    __('Compania a fost modificata cu succes'),
                    'success'
                );
            }
        } else {
            unset($user_company['User']);
            $this->request->data = $user_company;
        }
    }

    public function myNotices()
    {
        $user_id = $this->checkIfLoggedIn();

        $this->Notice->bindModel([
            'belongsTo' => [
                'DocumentType',
                'NoticeStatus'
            ],
            'hasOne' => [
                'Invoice'
            ],
            'hasMany' => [
                'NoticeResponse'
            ]
        ]);
        $this->Notice->contain([
            'NoticeStatus',
            'DocumentType',
            'Invoice',
            'NoticeResponse' => [
                'NoticeResponseType'
            ]
        ]);

        $this->Paginator->settings = [
            'paramType' => 'querystring',
            'limit' => 10,
            'order' => [
                'Notice.created_at' => 'DESC'
            ],
            'conditions' => [
                'Notice.user_id' => $user_id,
                'Notice.notice_status_id' => [3,4]
            ]
        ];

        $notices = $this->Paginator->paginate('Notice');
        $this->set(compact('notices'));
    }

    public function getUserCompanyDetails($user_company_id)
    {
        $this->layout = false;
        $this->autoRender = false;

        $user_company = $this->UserCompany->findById($user_company_id);

        die(json_encode(['company_data' => $user_company['UserCompany']]));
    }

    public function getUserPaymentDetails($user_payment_id)
    {
        $this->layout = false;
        $this->autoRender = false;

        $user_payment = $this->UserPayment->findById($user_payment_id);

        die(json_encode(['payment_data' => $user_payment['UserPayment']]));
    }

    public function myPayments()
    {
        $user_id = $this->checkIfLoggedIn();
        $payments = $this->UserPayment->find(
            'all',
            [
                'conditions' => [
                    'UserPayment.user_id' => $user_id
                ]
            ]
        );
        $this->set(compact(
            'payments'
        ));

        if ($this->request->is('post')) {
            if ($this->UserPayment->save($this->request->data)) {
                return $this->respond(
                    __('Datele de facturare au fost adaugate cu succes'),
                    'success'
                );
            }
        } else {
            $this->request->data['UserPayment']['user_id'] = $user_id;
        }
    }

    public function editPayment($user_payment_id)
    {
        $user_id = $this->checkIfLoggedIn();
        if (empty($user_payment_id)) {
            return $this->respond(
                __('Date nespecificate'),
                'error'
            );
        }

        $user_payment = $this->UserPayment->findById($user_payment_id);
        if (empty($user_payment)) {
            return $this->respond(
                __('Date inexistente'),
                'error'
            );
        }

        $cities = $this->City->find(
            'list', array(
                'conditions' => array(
                    'county_id' => $user_payment['UserPayment']['county_id']
                ),
                'fields' => [
                    'City.name',
                    'City.name'
                ],
                'order' => [
                    'county_seat DESC',
                    'name ASC'
                ]
            )
        );
        $this->set(compact(
            'cities',
            'user_payment'
        ));

        if ($this->request->is(['post', 'put'])) {
            if ($this->UserPayment->save($this->request->data)) {
                return $this->respond(
                    __('Datele au fost modificate cu succes'),
                    'success'
                );
            }
        } else {
            unset($user_payment['User']);
            $this->request->data = $user_payment;
        }
    }

    public function deletePayment($user_payment_id)
    {
        $user_id = $this->checkIfLoggedIn();
        if (empty($user_payment_id)) {
            return $this->respond(
                __('Date nespecificate'),
                'error'
            );
        }

        $user_payment = $this->UserPayment->findById($user_payment_id);
        if (empty($user_payment)) {
            return $this->respond(
                __('Date inexistente'),
                'error'
            );
        }

        if (!$this->UserPayment->delete($user_payment_id)) {
            return $this->respond(
                __('Stergerea datelor de facturare a esuat. Va rugam reincercati!'),
                'warning'
            );
        }

        return $this->respond(
            __('Datele de facturare au fost sterse cu succes'),
            'success',
            ['controller' => 'users', 'action' => 'myPayments']
        );
    }

    public function serveUserNoticeFile($response_id)
    {
        $this->layout = false;
        $this->autoRender = false;

        $user_id = $this->checkIfLoggedIn();

        if (empty($response_id)) {
            return $this->respond(
                __('Fisier nespecificat'),
                'error'
            );
        }

        $response = $this->NoticeResponse->findById($response_id);
        if (empty($response)) {
            return $this->respond(
                __('Eroare la gasirea cererii!'),
                'error',
                ['controller' => 'users', 'action' => 'myNotices']
            );
        }

        // verific user aviz vs. user logat
        if ($response['Notice']['user_id'] != $user_id) {
            return $this->respond(
                __('Nu aveti dreptul sa accesati acest fisier'),
                'error',
                ['controller' => 'users', 'action' => 'myNotices']
            );
        }

        $file_path = WWW_ROOT . $response['NoticeResponse']['filepath'];
        if (!file_exists($file_path)
            || (file_exists($file_path) && is_dir($file_path))
        ) {
            return $this->respond(
                __('Fisier inexistent'),
                'error',
                ['controller' => 'users', 'action' => 'myNotices']
            );
        }

        $pathi = pathinfo($file_path);

        $final_file_name = $response['NoticeResponseType']['name']
            . '_' . $response['Notice']['registry_number']
            . '.' . $pathi['extension'];

        header('Content-Type: application/force-download');
        header('Content-Disposition: attachment; filename="'.$final_file_name.'"');
        header('Content-Transfer-Encoding: binary');
        readfile($file_path);
    }
}