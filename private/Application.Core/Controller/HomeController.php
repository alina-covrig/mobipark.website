<?php
/**
* Home Controller
*
* PHP version 5.4
*
* @category  Frontend
* @package   Generic
* @author    Lucian Vasile <lucian.vasile@live.com>
* @copyright 2014 Evercloud
* @license   http://www.php.net/license/3_01.txt  PHP License 3.01
* @version   SVN: $Id$
* @link      /frontend/core/controller/HomeController.php
* @since     1.0
*/

App::uses('AppController', 'Controller');
/**
* HomeController Class
*
* Homepage logic
*
* @category  Frontend
* @package   Generic
* @author    Lucian Vasile <lucian.vasile@live.com>
* @copyright 2014 Evercloud
* @license   http://www.php.net/license/3_01.txt  PHP License 3.01
* @version   Release: 1.0
* @link      /frontend/core/controller/HomeController.php
* @since     Class available since Release 1.0
* @property  DocumentType $DocumentType
* @property  DocType $DocType
*/
class HomeController extends AppController
{
    public $uses = [
        'Description'
    ];

    public $components = [
        'RequestHandler',
        'Paginator',
        'Flash'
    ];

    public $cacheAction = array(
        'index' => array('callbacks' => true, 'duration' => 1200)
    );

    /**
     * Homepage
     * @return void
     */
    public function index()
    {
        $descriptions = $this->Description->find('all');

       $this->set('descriptions', $descriptions);
        $this->render('/Pages/index');
    }
}
