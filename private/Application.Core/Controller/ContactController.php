<?php
/**
* Contact Controller
*
* PHP version 5.4
*
* @category  Frontend
* @package   Generic
* @author    Catalin Prodan <clinpan@gmail.com>
* @copyright 2014 Evercloud
* @license   http://www.php.net/license/3_01.txt  PHP License 3.01
* @version   SVN: $Id$
* @link      /frontend/core/controller/ContactController.php
* @since     1.0
*/

App::uses('AppController', 'Controller');
/**
* ContactController Class
*
* Contact logic
*
* @category  Frontend
* @package   Generic
* @author    Catalin Prodan <clinpan@gmail.com>
* @copyright 2014 Evercloud
* @license   http://www.php.net/license/3_01.txt  PHP License 3.01
* @version   Release: 1.0
* @link      /frontend/core/controller/ContactController.php
* @since     Class available since Release 1.0
*/
class ContactController extends AppController
{
    public $components = array('Paginator');

    public $helpers = array('Image');

    public $uses = array(
        'Page',
        'ProductAliasName',
        'Contact',
        'Product',
        'Category',
        'CategoriesUserType',
        'CmsBlock'
    );

    public $cacheAction = array(
        'index' => array('callbacks' => true, 'duration' => 1200)
    );

    /**
     * The index method
     *
     * @return void
     */
    public function index()
    {
        if ($this->request->is('post')) {
            $ret = $this->_sendMessage();
            $this->respond(
                $ret['m'],
                $ret['success'] ? 'success' : 'error'
            );
        }

        $meta = $this->CmsBlock->findByAlias('meta-for-contact');
        $this->set('meta_description', $meta['CmsBlock']['meta_description']);
        $this->set('title_for_layout', $meta['CmsBlock']['meta_title']);
        $this->set('meta_keywords', $meta['CmsBlock']['meta_keywords']);

        $box_contact = $this->CmsBlock->findByAlias('box-contact');
        $this->set('box_contact', $box_contact['CmsBlock']['content']);
    }

    /**
     * The resellers method
     *
     * This method controls the resellers contact page and its form
     *
     * @return void
     */
    public function resellers() 
    {
        if ($this->request->is('post')) {
            $ret = $this->_sendMessage('Reselleri');
            $this->Session->setFlash(
                $ret['m'],
                'Alerts/'.($ret['success']? 'success' : 'warning')
            );
        }

        $discounts = array();
        $buffer_discounts = $this->CategoriesUserType->find('all');
        foreach ($buffer_discounts as $discount) {
            $discounts
                [$discount['CategoriesUserType']['category_id']]
                [$discount['CategoriesUserType']['user_type_id']]
                    = $discount['CategoriesUserType']['discount_percent'];
        }

        $categories = $this->Category->find(
            'all', array(
                'conditions' => array(
                    'Category.is_active' => true
                ),
                'fields' => array(
                    'Category.id',
                    'Category.name',
                    'Category.rght',
                    'Category.lft',
                ),
                'group' => array(
                    'Category.id HAVING `Category`.`rght` - `Category`.`lft` = 1'
                ),
            )
        );

        $user_types = $this->UserType->find(
            'all', array(
                'fields' => array(
                    'UserType.id','UserType.name', 'UserType.discount',
                ),
                'order' => array(
                    'UserType.points_from' => 'ASC'
                )
            )
        );

        foreach ($categories as $key => $value) {
            $parents = $this->Category->getPath($value['Category']['id']);
            $categories[$key ]['Category']['the_parents'] = $parents;
        }

        $this->set('rt_user_types', $user_types);
        $this->set('rt_categories', $categories);
        $this->set('rt_discounts', $discounts);

        $meta = $this->CmsBlock->findByAlias('meta-for-resllers');
        $this->set('meta_description', $meta['CmsBlock']['meta_description']);
        $this->set('title_for_layout', $meta['CmsBlock']['meta_title']);
        $this->set('meta_keywords', $meta['CmsBlock']['meta_keywords']);

        $box_reseller = $this->CmsBlock->findByAlias('box-reseller');
        $this->set('box_reseller', $box_reseller['CmsBlock']['content']);
    }
    
    /**
     * The _sendMessage method
     * 
     * @param string $contact_form_name Reselleri or Contact
     * 
     * @return array
     */
    private function _sendMessage($contact_form_name = "Contact") 
    {
        App::uses('Email', 'Network/Email');
        
        if (empty($this->request->data['email'])
            || !Validation::email($this->request->data['email'])
        ) {
            $ret = array(
                'm' => __('Adresa de email invalida'),
                'success' => 0,
            );
        } else if (empty($this->request->data['name'])) {
            $ret = array(
                'm' => __('Este necesar numele dumneavoastra'),
                'success' => 0,
            );            
        }

        $em = new CakeEmail('default');
        
        if ($contact_form_name == 'Reselleri') {
            if (empty($this->request->data['certificat']['tmp_name'])) {
                $ret = array(
                    'm' => __('Este necesar certificatul de inregistrare al firmei'),
                    'success' => 0,
                );
            } else if (empty($this->request->data['telephone'])) {
                $ret = array(
                    'm' => __('Este necesar numarul de telefon'),
                    'success' => 0,
                );
            } else {
                $em->attachments(
                    array(
                        $this->request->data['certificat']['name'] => array(
                        'file'
                            => $this->request->data['certificat']['tmp_name'],
                        ),
                    )
                );
            }
        }
        
        if (empty($ret)) {
            $em->to(Configure::read('STORE_CONTACT_EMAIL'));
            $em->replyTo($this->request->data['email']);
            $em->subject($this->request->data['subject']);
            $msg = 'Mesaj expediat de '.$this->request->data['name'].' ('
                .$this->request->data['email']
                .') de pe pagina de contact - '.$contact_form_name
                .' a site-ului '
                .Configure::read('WEBSITE_URL')
                .'
                ---
                    
                '
                .$this->request->data['message']
                .'

                ----
                ';
            
            if ($contact_form_name == 'Reselleri') {
                $msg .= ' Nr. telefon al expeditorului: '
                    .$this->request->data['telephone']
                    .'
                    ';
            }
            
            if ($em->send(nl2br($msg))) {
                $ret = array(
                   'm' => __('Mesajul dumneavoastra ne-a fost trimis. Multumim!'),
                   'success' => 1,
                );
            } else {
                $ret = array(
                   'm' => __('Mesajul nu a fost trimis'),
                   'success' => 0,
                );
            }
        }
        
        return $ret;
    }

    /**
     * The help method
     *
     * @return void
     */
    public function help()
    {
        $cmss = $this->Page->find(
            'all', array(
            'conditions' => array(
                'title LIKE' => 'PARENT%'
            ),
            'order' => array(
                'lft' => 'ASC'
            )
            )
        );

        foreach ($cmss as &$cms) {
            $sub_cms = $this->Page->find(
                'all', array(
                'conditions' => array(
                    'parent_id' => $cms['Page']['id']
                ),
                'fields' => array(
                    'title',
                    'alias'
                )
                )
            );
            $cms['children'] = $sub_cms;
        }
        unset($cms);

        $this->set('info_menu', $cmss);
    }


    public function sendContact()
    {
        $viewVars = [
            //fixed vars
            'name' => $this->request->data['nume'],
            'email' => $this->request->data['email'],
            'phone' => $this->request->data['phone'],
            'subject' => $this->request->data['subject'],
        ];
        App::uses('Email', 'Network/Email');

        $em = new CakeEmail('default');
        $em->to('alyna.ali26@yahoo.com');
        $em->subject(__('E-mail contact mobipark.ro'));
        $em->template('contact', 'default_simple');
        $em->viewVars($viewVars);

	    $em->send();

        return $this->respond('Mailul a fost trimis cu succes!','success');

    }
}