<?php
/**
 * FilesController
 *
 * This file is the Files Controller
 *
 * PHP version 5.4
 *
 * @category  Frontend
 * @package   Generic
 * @author    Lucian Vasile <lucian.vasile@live.com>
 * @copyright 2015 Evercloud
 * @license   http://www.php.net/license/3_01.txt  PHP License 3.01
 * @version   SVN: $Id$
 * @link      /frontend/core/controller/FilesController.php
 * @since     1.0
 */

App::uses('AppController', 'Controller');

/**
 * FilesController Class
 *
 * Inside this Controller application-wide logic methods
 * will be placed
 *
 * @category  Frontend
 * @package   Generic
 * @author    Lucian Vasile <lucian.vasile@live.com>
 * @copyright 2015 Evercloud
 * @license   http://www.php.net/license/3_01.txt  PHP License 3.01
 * @version   Release: 1.0
 * @link      /frontend/core/controller/FilesController.php
 * @since     Class available since Release 1.0
 *            
 * @property User User
 */
class FilesController extends AppController
{
    public $uses = array(
        'User'
    );

    /**
     * The beforeFilter callback method
     *
     * @return void
     */
    public function beforeFilter() 
    {
        if (!isset($this->request->data['email'])
            || !isset($this->request->data['hash'])
        ) {
            throw new MethodNotAllowedException('Zona interzisa');
        }
        $admin_request = $this->User->find(
            'first', array(
            'conditions' => array(
                'User.email' => $this->request->data['email'],
                'User.hash' => $this->request->data['hash'],
                'User.is_admin' => true,
            )
            )
        );
        if (!$admin_request) {
            throw new ForbiddenException('Zona interzisa');
        }
    }

    /**
     * The upload method
     *
     * @return redirect
     */
    public function upload() 
    {
        $this->layout = false;
        $this->autoRender = false;

        if (!isset($this->request->params['form']['file']['error'])) {
            return $this->respond(
                __('Date inconsistente'),
                'error'
            );
        }

        if (!isset($this->request->data['path'])) {
            return $this->respond(
                __('Date inconsistente'),
                'error'
            );
        }

        $path = trim($this->request->data['path'], '/');
        if ($this->request->params['form']['file']['error'] !== 0) {
            return $this->respond(
                __(
                    'Eroare la incarcare (cod; %s)',
                    $this->request->data['file']['error']
                ),
                'error'
            );
        }
        $file = $this->request->params['form']['file'];
        if (is_uploaded_file($file['tmp_name'])) {
            if (!is_dir(WWW_ROOT . $path)) {
                mkdir(WWW_ROOT . $path, 0755, true);
            }
            $new_name = WWW_ROOT . $path . DS . $this->request->data['name'];
            if (!move_uploaded_file($file['tmp_name'], $new_name)) {
                return $this->respond(
                    __('Eroare la incarcarea documentului. 2')
                );
            } else {
                return $this->respond(
                    __('Document incarcat!'),
                    'success'
                );
            }

        } else {
            return $this->respond(
                __('Eroare la incarcarea documentului. 3')
            );
        }
    }

    /**
     * The delete method
     *
     * @return redirect
     */
    public function delete() 
    {
        $this->layout = false;
        $this->autoRender = false;
        if (!isset($this->request->data['path'])) {
            return $this->respond(
                __('Date inconsistente'),
                'error'
            );
        }
        $path = trim($this->request->data['path'], '/');
        if (!file_exists(WWW_ROOT . $path)) {
            return $this->respond(
                __('Niciun fisier de sters'),
                'warning'
            );
        }

        if (!unlink(WWW_ROOT . $path)) {
            return $this->respond(
                __('Nu s-a putut sterge'),
                'error'
            );
        }
        return $this->respond(
            __('Fisier sters'),
            'success'
        );
    }

    /**
     * The rename method
     *
     * @return redirect
     */
    public function rename()
    {
        $this->layout = false;
        $this->autoRender = false;
        if (!isset($this->request->data['path'])) {
            return $this->respond(
                __('Date inconsistente'),
                'error'
            );
        }
        $path = explode("|", $this->request->data['path']);
        if (!isset($path[1])) {
            return $this->respond(
                __('Date inconsistente'),
                'error'
            );
        }
        $path[0] = trim($path[0], '/');
        $path[1] = trim($path[1], '/');
        if (!file_exists(WWW_ROOT . $path[0])) {
            return $this->respond(
                __('Niciun fisier de redenumit'),
                'warning'
            );
        }

        if (!rename(WWW_ROOT . $path[0], WWW_ROOT . $path[1])) {
            return $this->respond(
                __('Nu s-a putut redenumi'),
                'error'
            );
        }
        return $this->respond(
            __('Fisier redenumit'),
            'success'
        );
    }

    /**
     * The serve method
     *
     * @return redirect
     */
    public function serve()
    {
        $this->layout = false;
        $this->autoRender = false;
        if (!isset($this->request->data['path'])) {
            return $this->respond(
                __('Date inconsistente'),
                'error'
            );
        }
        $path = trim($this->request->data['path'], '/');
        if (!file_exists(WWW_ROOT . $path)) {
            return $this->respond(
                __('Fisier inexistent'),
                'warning'
            );
        }
        return $this->respond(
            __('Fisier atasat'),
            'success',
            null,
            array(
                'file' => base64_encode(file_get_contents(WWW_ROOT . $path)),
                'headers' => pathinfo(WWW_ROOT . $path)
            )
        );
    }

    /**
     * The copy method
     *
     * @return redirect
     */
    public function copy($from_product_id = null, $to_product_id = null)
    {
        $this->layout = false;
        $this->autoRender = false;

        $src = WWW_ROOT . 'img' . DS . 'products' . DS . $from_product_id;
        $dst = WWW_ROOT . 'img' . DS . 'products' . DS . $to_product_id;

        if (!is_dir($src)) {
            return $this->respond(
                __('Produsul nu are poze'),
                'error'
            );
        }

        $this->recurse_copy($src, $dst);
        return $this->respond(
            __('Pozele au fost copiate cu succes'),
            'success'
        );
    }

    function recurse_copy($src,$dst) {
        $dir = opendir($src);
        @mkdir($dst);
        while(false !== ( $file = readdir($dir)) ) {
            if (( $file != '.' ) && ( $file != '..' )) {
                if ( is_dir($src . '/' . $file) ) {
                    recurse_copy($src . '/' . $file,$dst . '/' . $file);
                }
                else {
                    copy($src . '/' . $file,$dst . '/' . $file);
                }
            }
        }
        closedir($dir);
    }
}
