<?php
/**
 * PagesController
 *
 * This file is the Pages Controller
 *
 * PHP version 5.4
 *
 * @category  Frontend
 * @package   Generic
 * @author    Lucian Vasile <lucian.vasile@live.com>
 * @copyright 2014 Evercloud
 * @license   http://www.php.net/license/3_01.txt  PHP License 3.01
 * @version   SVN: $Id$
 * @link      /adfrontendmin/core/controller/PagesController.php
 * @since     1.0
 */

use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Style\Alignment;
use PhpOffice\PhpSpreadsheet\Style\Fill;
use PhpOffice\PhpSpreadsheet\Writer\Xls;

App::uses('AppController', 'Controller');

/**
 * PagesController Class
 *
 * Inside this Controller application-wide logic methods
 * will be placed
 *
 * @param Sale Sale
 * @param Page Page
 *
 * @category  Frontend
 * @package   Generic
 * @author    Lucian Vasile <lucian.vasile@live.com>
 * @copyright 2014 Evercloud
 * @license   http://www.php.net/license/3_01.txt  PHP License 3.01
 * @version   Release: 1.0
 * @link      /frontend/core/controller/PagesController.php
 * @since     Class available since Release 1.0
 */
class PagesController extends AppController
{

    public $uses = array(
        'Page',
        'Description'
    );

    public $components = array(
        'RequestHandler',
        'Session'
    );

    /**
     * The index method
     *
     * This method recieves all the CMS requests
     *
     * @param string $fullPath full path to page
     *
     * @return void
     */
    public function static_page($alias = null)
    {
        $page = $this->Page->find(
            'first', array(
                'conditions' => array(
                    'alias' => $alias
                )
            )
        );
        $this->set(compact('page'));
        $this->render('/Pages/static');


    }




    public function buildings()
    {
        $this->set('meta_index', 'noindex, nofollow');

    }

    public function building1()
    {
        $this->set('meta_index', 'noindex, nofollow');

    }

    public function gallery()
    {
        $this->set('meta_index', 'noindex, nofollow');

    }
    public function apartment_types()
    {
        $this->set('meta_index', 'noindex, nofollow');

    }
    public function apartment1()
    {
        $this->set('meta_index', 'noindex, nofollow');

    }
    public function apartment2()
    {
        $this->set('meta_index', 'noindex, nofollow');

    }
    public function apartment3()
    {
        $this->set('meta_index', 'noindex, nofollow');

    }
    public function apartment4()
    {
        $this->set('meta_index', 'noindex, nofollow');

    }
    public function apartment5()
    {
        $this->set('meta_index', 'noindex, nofollow');

    }
    public function apartment6()
    {
        $this->set('meta_index', 'noindex, nofollow');

    }
    public function apartment7()
    {
        $this->set('meta_index', 'noindex, nofollow');

    }
    public function apartment8()
    {
        $this->set('meta_index', 'noindex, nofollow');

    }
    public function apartment9()
    {
        $this->set('meta_index', 'noindex, nofollow');

    }
    public function apartment10()
    {
        $this->set('meta_index', 'noindex, nofollow');

    }
    public function apartment11()
    {
        $this->set('meta_index', 'noindex, nofollow');

    }
    public function apartment12()
    {
        $this->set('meta_index', 'noindex, nofollow');

    }
}