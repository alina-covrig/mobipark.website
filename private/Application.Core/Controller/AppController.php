<?php
/**
 * Application level Controller
 *
 * This file is application-wide controller file. You can put all
 * application-wide controller-related methods here.
 *
 * PHP version 5.4
 *
 * @category  Frontend
 * @package   Generic
 * @author    Lucian Vasile <lucian.vasile@live.com>
 * @copyright 2014 Evercloud
 * @license   http://www.php.net/license/3_01.txt  PHP License 3.01
 * @version   SVN: $Id$
 * @link      /frontend/core/controller/AppController.php
 * @since     1.0
 */
App::uses('Controller', 'Controller');
App::uses('Folder', 'Utility');
App::uses('File', 'Utility');
App::uses('ClearCache', 'ClearCache.Lib');

/**
 * AppController Class
 *
 * Inside this Controller application-wide logic methods
 * will be placed
 *
 * @category  Frontend
 * @package   Generic
 * @author    Lucian Vasile <lucian.vasile@live.com>
 * @copyright 2014 Evercloud
 * @license   http://www.php.net/license/3_01.txt  PHP License 3.01
 * @version   Release: 1.0
 * @link      /frontend/core/controller/AppController.php
 * @since     Class available since Release 1.0
 *
 * @property  Category $Category
 * @property  DocumentFile $DocumentFile
 */
class AppController extends Controller
{
    public $bid = null;

    /**
     * The ShoppingCart property
     *
     * Holds the shopping cart property to be available
     * in all controllers
     *
     * @var $sc
     */

    public $sc = null;
    public $loginData = null;
    public $login_cookie_expires = '8 hours';
    public $cacheAction = array(
        'home' => array('callbacks' => true, 'duration' => 1200),
    );

    public $helpers = array(
        'Html',
        'Form',
        'Paginator',
        'Price',
        'Cache'
    );

    public $components = array(
        'Cookie',
        'Session',
        'Email',
        'DebugKit.Toolbar'
    );

    public $uses = array(
        'Configuration',
        'DocumentFile',
        'Banner',
        'CmsCache'
    );

    public $theme = 'Default';

    /**
     * The beforeFilter method
     *
     * Callback method that runs before controller logic
     *
     * @return void
     */
    public function beforeFilter()
    {
        Cache::clear(false, '_cake_core_');
        Cache::clear(false, '_cake_model_');
        Cache::clear(false, 'configurations');
        //Cache::clear(false, 'routes');
        $this->_setRequestHeaders();
        $this->_getConfiguration();
        $this->_setCookieData();


        $this->_getBanners();
        $this->_getSmallBanners();


        if ($this->request->params['controller'] != 'files') {
            if (file_exists(
                dirname(ROOT) . DS . 'web' . DS
                . 'site_in_mentenanta.html'
            )) {
                if ($_SERVER['REMOTE_ADDR'] != '5.2.225.206' &&
                    $_SERVER['REMOTE_ADDR'] != '35.157.154.14' &&
                    $_SERVER['REMOTE_ADDR'] != '146.148.5.119' &&
                    $_SERVER['REMOTE_ADDR'] != '185.68.14.1') {

                    echo file_get_contents(
                        dirname(ROOT) . DS
                        . 'web' . DS . 'site_in_mentenanta.html'
                    );
                    die();
                }
            }
        }

        $this->set('title_for_layout', configure::read('STORE_NAME'));
        $this->set(
            'meta_description', configure::read('STORE_NAME') . ' - '
            . 'Magazin online accesorii'
            . ' telefoane si tablete, accesorii laptop, piese si componente, '
            . 'accesorii si scule service, stocare date, gadget-uri'
        );
        $this->set(
            'meta_keywords', 'telefoane, tablete, piese, '
            . 'laptop, service, gadget'
        );
    }

    /**
     * The _setRequestHeaders method
     *
     * IE Ajax
     *
     * @return void
     */
    private function _setRequestHeaders()
    {
        if ($this->request->is('ajax')) {
            header('Expires: -1');
        }
    }

    /**
     * The beforeRender method
     *
     * Callback method that runs after controller logic
     *
     * @return void
     */
    public function beforeRender()
    {
        if (isset($_GET['brillenputzuccher'])) {
            $db = ConnectionManager::getDataSource('default');
            echo json_encode($db->getLog());die();
        }
        //$this->_setJSVars();
    }

    /**
     * The getConfiguration method
     *
     * This method will get the configurations from
     * the database or from the cache and write them
     * to the Configure static object. They may be
     * used application-wide (Model, Controller,
     * View, Plugins, etc)
     *
     * @return void
     */
    private function _getConfiguration()
    {
        $configurations = $this->Configuration->find(
            'list',
            array(
                'fields' => array(
                    'Configuration.name',
                    'Configuration.value',
                )
            )
        );
        foreach ($configurations as $cname => $cvalue) {
            Configure::write($cname, $cvalue);
        }
        if (!Configure::read('HOME_URL')) {
            Configure::write('HOME_URL', '/home');
        }
    }



    /**
     * The _setCookieData method
     *
     * Sets the name of the cookie and other settins
     * Sets auth cookies to the view to be used
     *
     * @return void
     */
    private function _setCookieData()
    {
        $this->Cookie->name = Configure::read('COOKIE_NAME');
        $this->Cookie->time = 3600;
        $this->Cookie->path = '/';
        $this->Cookie->httpOnly = true;
        $this->Cookie->type('aes');
    }

    /**
     * The _getBanners method
     *
     * This method get the big banners
     *
     * @return array
     */
    private function _getBanners()
    {
        $banners = Cache::read('banners', 'roller');

        if (!$banners) {
            $banners = $this->Banner->find(
                'all', [
                    'conditions' => [
                        'Banner.is_active' => 1,
                        'Banner.is_little' => 0
                    ],
                    'order' => 'Banner.order ASC'
                ]
            );

            $this->CmsCache->write('banners', $banners, 'roller');
        }





        $this->set(compact('banners'));
    }


    /**
     * The _getSmallBanners method
     *
     * This method get the small banners
     *
     * @return array
     */
    private function _getSmallBanners()
    {
        $smallBanners = Cache::read('smallBanners', 'roller');

        if (!$smallBanners) {
            $smallBanners = $this->Banner->find(
                'all', array(
                    'conditions' => array(
                        'Banner.is_active' => 1,
                        'Banner.is_little' => 1
                    ),
                    'limit' => 2,
                    'order' => 'Banner.order ASC'
                )
            );
            $this->CmsCache->write('smallBanners', $smallBanners, 'roller');
        }

        $this->set(compact('smallBanners'));
    }


    /**
     * The setBrowserIdentity method
     *
     * This method is used to set a cookie to uniquely identify
     * a browser for a whole year
     *
     * @param mixed $new_id The new browser id
     *
     * @return void
     */
    protected function setBrowserIdentity($new_id = false)
    {
        if ($new_id) {
            $bid = $new_id;
            $this->Cookie->write('bid', $bid, true, '+1 Year');
        } else {
            $bid = $this->_getBrowserIdentity();
        }

        if (!$bid) {
            $bid = uniqid($this->Session->id());
            $this->Cookie->write('bid', $bid, true, '+1 Year');
        }

        $this->bid = $bid;
    }

    /**
     * The _getBrowserIdentity method
     *
     * Returns the current browser id or false otherwise
     *
     * @return string or boolean
     */
    private function _getBrowserIdentity()
    {
        return $this->Cookie->read('bid');
    }

    /**
     * The isMultidimensionalArray @method
     *
     * This method check if an array is
     * multidimensional
     *
     * @param array $array Array to check if multidimensional
     *
     * @return boolean
     */
    protected function isMultidimensionalArray($array = array())
    {
        if (count($array) == count($array, COUNT_RECURSIVE)) {
            return false;
        } else {
            return true;
        }
    }

    /**
     * Set global Shop JS configuration variables
     * @return  void
     */
    private function _setJSVars()
    {
    }

    /**
     * The respond method
     *
     * A convenience method for treating responses of the actions
     *
     * @param string $message     the message respons
     * @param string $type        type (success, info, error or warning)
     * @param mixed  $redirect    url
     * @param mixed  $append_data data to append to the response
     *
     * @return redirect
     */
    public function respond(
        $message,
        $type = 'info',
        $redirect = null,
        $append_data = null,
        $extra = null
    ) {
        $allowed_types = array(
            'success',
            'info',
            'error',
            'warning',
        );

        if (!in_array($type, $allowed_types)) {
            $type = 'info';
        }

        $this->Session->setFlash(
            $message,
            "Alerts/$type"
        );

        if (is_null($redirect)) {
            return $this->redirect($this->request->referer());
        }

        if ($redirect !== false) {
            return $this->redirect($redirect);
        }

        if ($redirect === false) {
            return;
        }

        return $this->redirect($this->request->referer());
    }


    /**
     * The _parseErrorsMessages method
     *
     * @param array $errors Errors array
     * @param array $msg    Errors message array
     *
     * @return string
     */
    public function parseErrorsMessages($errors = [], &$msg = [])
    {
        if (!empty ($errors)) {
            foreach ($errors as $fieldError) {
                if ($this->isMultidimensionalArray($fieldError)) {
                    $this->parseErrorsMessages($fieldError, $msg);
                } else {
                    foreach ($fieldError as $error) {
                        $msg[] = $error;
                    }
                }
            }
            $msg = array_unique($msg);
            return implode("<br />", $msg);
        }
    }



    /**
     * Generate request unique control key
     *
     * @return false|string
     */
    public function generateControlKey()
    {
        return strtoupper(substr(
            sha1(generateRandomString(32) . time()), 0, 8
        ));
    }
}
