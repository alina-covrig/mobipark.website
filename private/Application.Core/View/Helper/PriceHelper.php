<?php

//App::uses('AppHelper', 'View/Helper');

class PriceHelper extends AppHelper
{
    public $helpers = array('Html');

    // public function __construct()
    // {
    //     //parent::__construct();
    //     // get user here
    //     // check if user is logged in
    // }


    public function priceArray($product)
    {
        $bargain_id = $this->_View->get('bargain_id');
        $loginData = $this->_View->get('loginData');

        $oldPrice = price($product['Product']['old_price']);
        $sellingPrice = price($product['Product']['selling_price']);
        $sellingCurrency = $product['Product']['selling_currency'];
        $sellingCurrency = str_replace('RON', 'LEI', $sellingCurrency);

        $arr_return['reseller']['tip'] = '';
        $arr_return['reseller']['amount'] = '';
        $arr_return['oldPrice'] = '';
        $arr_return['currentPrice'] = '';
        $arr_return['currency'] = $sellingCurrency;

        if (isset($loginData['UserSetting']['show_VAT'])
            && !$loginData['UserSetting']['show_VAT']
        ) {
            $oldPrice = price($oldPrice / (1 + (Configure::read('VAT_PERCENT') / 100)));
            $sellingPrice = price($sellingPrice / (1 + (Configure::read('VAT_PERCENT') / 100)));
        }

        $eof = isset($product['ProductAliasName']['is_end_of_life'])
            ? $product['ProductAliasName']['is_end_of_life']
            : $product['is_end_of_life'];

        $offer = isset($product['ProductAliasName']['is_offer'])
            ? $product['ProductAliasName']['is_offer']
            : $product['is_offer'];

        $pid = isset($product['ProductAliasName']['id'])
            ? $product['ProductAliasName']['id']
            : $product['id'];

        $stop_u_type_discount
            = (!empty($eof) ||
            !empty($offer) ||
            $pid == $bargain_id
        );

        if (!$stop_u_type_discount && !empty($loginData['User'])
            && !empty($loginData['UserType']) &&
            $loginData['UserType']['id'] != '') {

            if (isset($product['Category']['CategoriesUserType']['discount_percent'])
            ) {
                $discount = $product['Category']['CategoriesUserType']['discount_percent'];
            }
            else {
                $discount = $loginData['UserType']['discount'];
            }

            if (!$product['Product']['is_original']) {
                $discount += $discount;
            }

            $discountType = $loginData['UserType']['name'];
            $discountedPrice = p($sellingPrice, 0, $discount);

            $discountTypeDiv = $this->Html->tag(
                'span', '*' . 'discount ' . $discountType . ' -' . $discount . '%',
                ['class' => 'price-discount-info']
            );

            $discountedPriceDiv = $this->Html->div(
                'price-discounted', price($discountedPrice) . ' ' . $sellingCurrency . '*'
                . $discountTypeDiv
            );

            $arr_return['reseller']['tip'] = $discountType;
            $arr_return['reseller']['amount'] = $discount;
        }

        $oldPriceDiv = $this->Html->div(
            'price-prev', $oldPrice . ' ' . $sellingCurrency
        );

        $sellingPriceDiv = $this->Html->div(
            'price-current', price($sellingPrice) . ' ' . $sellingCurrency
        );

        $finalPrice = $sellingPrice;

        if (isset($loginData['UserSetting']['show_discount'])
            && !$loginData['UserSetting']['show_discount']
        ) {
            if (isset($discountedPrice)) {
                $arr_return['currentPrice'] = price($discountedPrice);
            } else {
                $arr_return['currentPrice'] = price($sellingPrice);
            }
        } else {
            if ($oldPrice > $sellingPrice) {
                if (isset($discountedPrice)) {
                    $arr_return['currentPrice'] = price($discountedPrice);
                    $arr_return['oldPrice'] = price($oldPrice);
                } else {
                    $arr_return['currentPrice'] = price($sellingPrice);
                    $arr_return['oldPrice'] = price($oldPrice);
                }
            } else {
                if (isset($discountedPrice)) {
                    $arr_return['currentPrice'] = price($discountedPrice);
                } else {
                    $arr_return['currentPrice'] = price($sellingPrice);
                }
            }
        }

        //qty price
        App::import('Model', 'QuantityPrice');

        $QuantityPrice = new QuantityPrice();
        $user_id = (isset($loginData['User']['id']))
            ? $loginData['User']['id'] : null;

        if (isset($loginData['User']['user_type_id'])
            && !empty($loginData['User']['user_type_id'])
        ) {
            $qty_price = $QuantityPrice->hasQuantityPrice(
                $user_id,
                $product['Product']['id'],
                $product['quantity']
            );

            if ($qty_price !== false) {
                $arr_return['currentPrice'] = $qty_price;
            }
        }

        return $arr_return;
    }

    /**
     * @param $product
     * @param string $type
     * @return float
     */

    //@ todo: loc de imbunatatiri.
    public function showPrice($product, $type = 'grid')
    {
        $loginData = $this->_View->get('loginData');
        $oldPrice = price($product['Product']['old_price']);
        $sellingPrice = price($product['Product']['selling_price']);
        $sellingCurrency = $product['Product']['selling_currency'];
        $sellingCurrency = str_replace('RON', 'LEI', $sellingCurrency);

        if (isset($loginData['UserSetting']['show_VAT'])
            && !$loginData['UserSetting']['show_VAT']
        ) {
            $oldPrice = price($oldPrice / (1 + (Configure::read('VAT_PERCENT') / 100)));
            $sellingPrice = price($sellingPrice / (1 + (Configure::read('VAT_PERCENT') / 100)));
        }

        $stop_u_type_discount
            = (!empty($product['ProductAliasName']['is_end_of_life']) ||
                !empty($product['ProductAliasName']['is_offer']) ||
                isset($this->_View->viewVars['isBargain']) ||
                isset($product['ProductAliasName']['is_bargain'])
        );
        
        if (!$stop_u_type_discount && !empty($loginData['User'])
            && !empty($loginData['UserType']) &&
                $loginData['UserType']['id'] != '')
        {
            if (isset($product['Category']['CategoriesUserType']['discount_percent'])) {
                $discount = $product['Category']['CategoriesUserType']['discount_percent'];
            } else {
                $discount = $loginData['UserType']['discount'];
            }

            if (!$product['Product']['is_original']) {
                $discount += $discount;
            }

            $discountType = $loginData['UserType']['name'];
            $discountedPrice = p($sellingPrice, 0, $discount);

            $discountTypeDiv = '';
            if ($discount != 0) {
                $discountTypeDiv = $this->Html->tag(
                    'span', '*' . 'discount ' . $discountType . ' -' . $discount . '%',
                    [
                        'class' => 'price-discount-info'
                    ]
                );
            }

            $discountedPriceDiv = $this->Html->div(
                'price-discounted', price($discountedPrice) . ' ' . $sellingCurrency . '*'
                . $discountTypeDiv
            );
        }

        $old_price_class = 'price-prev';

        if($this->request->params['action'] == 'listSpecialOffers') {
            $old_price_class .= ' special-offer-price';
        }

        if ($product['Product']['old_price'] != 0) {
            $discountPercentDiv = $this->Html->div(
                'discount-percent',
                '-' . ceil(
                    ($product['Product']['old_price'] - $product['Product']['selling_price'])
                    * 100
                    / $product['Product']['old_price'])
                . '%'
            );
        } else {
            $discountPercentDiv = '';
        }

        $oldPriceDiv = $this->Html->div(
            $old_price_class,
            $oldPrice . ' ' . $sellingCurrency . $discountPercentDiv
        );

        // empty 20px height div to fill .price-prev's place (for nice alignment)
        $oldPriceDiv_dummy = $this->Html->div('dummy-price', '');

        $sellingPriceDiv = $this->Html->div(
            'price-current', price($sellingPrice) . ' ' . $sellingCurrency
        );

        $finalPrice = $sellingPrice;
        //  nu vrem sa vedem reducerile
        if (isset($loginData['UserSetting']['show_discount'])
            && !$loginData['UserSetting']['show_discount']
        ) {
            //se afiseaza pretul curent, indiferent daca e redus sau nu
            $content = $oldPriceDiv_dummy;
            $content .= isset($discountedPrice)
                ? $this->Html->div(
                    'price-current', price($discountedPrice) . ' ' . $sellingCurrency
                )
                : $this->Html->div(
                    'price-current', price($sellingPrice) . ' ' . $sellingCurrency
                );
                ;
        } else {
            //dorim sa vedem reducerile
            if ($oldPrice > $sellingPrice) {
                //avem o reducere obisnuita
                if (isset($discountedPrice)) {
                    //reducere pentru reselleri
                    $sellingPriceDiv = $this->Html->div(
                        'price-current price-line-through'
                        , price($sellingPrice) . ' ' . $sellingCurrency
                    );
                    $content = $oldPriceDiv . $sellingPriceDiv . $discountedPriceDiv;
                    $finalPrice = $discountedPrice;
                } else {
                    //reducere obisnuita, nu pentru reselleri
                    $content = $oldPriceDiv . $sellingPriceDiv;
                }
            } else {
                //reducere exclusiv pentru reselleri
                if (isset($discountedPrice) && $discount != 0) {
                    $sellingPriceDiv = $this->Html->div(
                        'price-current price-line-through'
                        , price($sellingPrice) . ' ' . $sellingCurrency
                    );
                    $content = $oldPriceDiv_dummy.$sellingPriceDiv.$discountedPriceDiv;
                    $finalPrice = $discountedPrice;
                } else {
                    $content = $oldPriceDiv_dummy . $sellingPriceDiv;
                }
            }
        }

        if (isset($product['ProductAliasName']['is_coming_soon'])
            && $product['ProductAliasName']['is_coming_soon']) {
            $content =
                $this->Html->div('dummy-price','') .
                $this->Html->div(
                'price-current text-violet', __('In curand!')
            );
        }

        /* modifica putin div-ul container, in functie de layout-ul paginii
           in care urmeaza sa fie afisat */
        if ($type == 'grid') {
            $container = $this->Html->div('prices', $content);
        } elseif ($type == 'list') {
            $container = $this->Html->div('prices-list', $content);
        } elseif ($type == 'final_price') {
            $container = $finalPrice;
        }

        return $container;
    }

    /**
     *  main function of the helper
     *  returns an array of old_price, selling_price,
     *  discountend_price and selling_currency
     *
     * @param $product
     * @return array
     */
    public function price($product)
    {
        //date de logare si de chilipir preluate din view
        $loginData = $this->_View->get('loginData');
        $bargain_id =  $this->_View->get('bargain_id');

        // necesar fiindca nu avem modele in helper
        App::import('Model', 'QuantityPrice');

        $QuantityPrice = new QuantityPrice();
        $user_id = (isset($loginData['User']['id']))
            ? $loginData['User']['id'] : null;

        $changed = false;

        if (isset($loginData['User']['user_type_id'])
            && !empty($loginData['User']['user_type_id'])
        ) {
            //verificam daca pentru cantitatile de produs, avem QuantityPrice diferit
            $changed = (isset($product['quantity']))
                ? $QuantityPrice->hasQuantityPrice(
                    $user_id,
                    $product['Product']['id'],
                    $product['quantity']
                )
                : false;
        }

        //pretul vechi
        $oldPrice = price($product['Product']['old_price']);
        /*  core.price() -> functie estetica
            selling price == changed daca s-a modificat, altfel e pretul de vanzare
            initial */
        $sellingPrice = ($changed)
            ? price($changed)
            : price($product['Product']['selling_price']);
        $sellingCurrency = $product['Product']['selling_currency'];

        $eof = isset($product['ProductAliasName']['is_end_of_life'])
            ? $product['ProductAliasName']['is_end_of_life']
            : $product['is_end_of_life'];

        $offer = isset($product['ProductAliasName']['is_offer'])
            ? $product['ProductAliasName']['is_offer']
            : $product['is_offer'];

        $pid = isset($product['ProductAliasName']['id'])
            ? $product['ProductAliasName']['id']
            : $product['id'];

        //situatii in care nu se mai ofera discountul de reseller
        $stop_u_type_discount = (!empty($eof) || !empty($offer) || $bargain_id == $pid || $changed);

        if (!$stop_u_type_discount 
            && !empty($loginData['User']) && !empty($loginData['UserType']) &&
            $loginData['UserType']!='') {

            if (isset($product['Category']['CategoriesUserType']['discount_percent'])) {
                //aplicam discountul per categorie pentru reselleri
                $discount = $product['Category']['CategoriesUserType']['discount_percent'];
            } else {
                //mai mereu 0
                $discount = $loginData['UserType']['discount'];
            }

            //discount dublu daca produsul nu e "original"
            if (!$product['Product']['is_original']) {
                $discount += $discount;
            }

            //se aplica discountul, dupa core.p();
            $discountedPrice = p($sellingPrice, 0, $discount);
        }

        if (isset($loginData['UserSetting']['show_VAT'])
            && !$loginData['UserSetting']['show_VAT']
        ) {
            //preturi din care s-a exclus TVA-ul
            return [
                'old_price' => price($oldPrice / (1 + (Configure::read('VAT_PERCENT')/100))),
                'selling_price' => price($sellingPrice / (1 + (Configure::read('VAT_PERCENT')/100))),
                'discounted_price' => (isset($discountedPrice) ? price($discountedPrice/(1+(Configure::read('VAT_PERCENT')/100))) : 0),
                'selling_currency' => $sellingCurrency
            ];
        }
        //preturi cu tot cu tva
        return [
            'old_price' => $oldPrice,
            'selling_price' => $sellingPrice,
            'discounted_price' => (isset($discountedPrice) ? $discountedPrice : 0),
            'selling_currency' => $sellingCurrency
        ];
    }

    /*
    public function discount($product)
    {
        $loginData = $this->_View->get('loginData');

        if (!empty($loginData['User']) && !empty($loginData['UserType'])) {
            if ($product['Category']['CategoriesUserType']['discount_percent'])
                $discount =
                    $product['Category']['CategoriesUserType']['discount_percent'];
            else
                $discount = $loginData['UserType']['discount'];
            $discountType    = $loginData['UserType']['name'];
            $discountedPrice =
                round($sellingPrice * ((100-$discount) / 100), 2);
        }

        return (isset($discountedPrice) ? $discountedPrice : 0);
    }


    public function percent($product)
    {
        $loginData = $this->_View->get('loginData');

        if (!empty($loginData['User']) && !empty($loginData['UserType'])) {
            $discount        = $loginData['UserType']['discount'];
            $discountType    = $loginData['UserType']['name'];
            $discountedPrice =
                round($sellingPrice * ((100-$discount) / 100), 2);
        }

        return (isset($discount) ? $discount : 0);
    }
    */

    public function salePrice($saleProduct)
    {
        $price = $saleProduct['price'];
        $discount = $saleProduct['discount_percent'];
        $quantity = $saleProduct['quantity'];

        return [
            'price' => p($price, 0, $discount) . ' ' .
                        Configure::read('LOCAL_CURRENCY'),
            'total' => price($quantity * p($price, 0, $discount)) . ' ' .
                        Configure::read('LOCAL_CURRENCY')
        ];
    }
}