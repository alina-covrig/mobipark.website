<?php

App::uses('AppHelper', 'View/Helper');

class ImageHelper extends AppHelper
{
    public function __construct()
    {
        //parent::__construct();
        $this->image_path   = Configure::read('IMG_PATH');
        $this->thumb_path   = Configure::read('THUMBS_PATH');
        $this->img_height   = Configure::read('IMG_HEIGHT');
        $this->img_width    = Configure::read('IMG_WIDTH');
        $this->thumb_height = Configure::read('THUMB_HEIGHT');
        $this->thumb_width  = Configure::read('THUMB_WIDTH');
    }
    public function product($type, $image, $product_id, $options = array(), $only_src = false, $data_src = false)
    {
        $class  = isset($options['class'])  ? $options['class']  : '';
        $style  = isset($options['style'])  ? $options['style']  : '';
        $alt    = isset($options['alt'])    ? $options['alt']    : '';
        $title  = isset($options['title'])  ? $options['title']  : '';

        $image_url = $this->image_path.'products/'.$product_id.'/'.$image;

        if ($type === 'full') {
            $height = isset($options['height']) ? $options['height'] : $this->img_height;
            $width  = isset($options['width'])  ? $options['width']  : $this->img_width;

        } elseif ($type === 'thumb') {
            $height = isset($options['height']) ? $options['height'] : $this->thumb_height;
            $width  = isset($options['width'])  ? $options['width']  : $this->thumb_width;

        } else {
            $height = isset($options['height']) ? $options['height'] : $this->img_height;
            $width  = isset($options['width'])  ? $options['width']  : $this->img_width;
        }

        if ($width > 400 && $this->checkFileExists( $this->image_path.'products_mari/'.$product_id.'/'.$image)) {
            $image_url = $this->image_path.'products_mari/'.$product_id.'/'.$image;
        }

        if (!empty($image) && $this->checkFileExists($image_url)) {
            if ($width > 400 && $this->checkFileExists( $this->image_path.'products_mari/'.$product_id.'/'.$image)) {
                $src = $this->thumb_path.$width.'x'.$height.'/products_mari/'.$product_id.'/'.$image;
            } else {
                $src = $this->thumb_path . $width . 'x' . $height . '/products/' . $product_id . '/' . $image;
            }
        } else {
            $src = $this->thumb_path.$width.'x'.$height.'/no_image.png';
        }
        if (!$only_src) {

            $loader = '';
            if ($data_src) {
                $img_src = "data-src='".$src."'";
                $loader = "src='" . $this->image_path . "loader.gif'";
            } else {
                $img_src = "src='".$src."'";
            }

            return '<img '.$img_src.' '.$loader.
            ((isset($options['number']) && $options['number'] == 0) ? 'itemprop="image"' : '').
            '
                    class="'.$class.'"
                    alt="'.$alt.'"
                    title="'.$title.'"
                    width="'.$width.'"
                    height="'.$height.'"
                    style="'.$style.'" />';
        } else {

            return $src;
        }
    }

    public function category($type, $image, $category_id, $options = array()) // @todo: remove the category_id param
    {
        $class  = isset($options['class'])  ? $options['class']  : '';
        $style  = isset($options['style'])  ? $options['style']  : '';
        $alt    = isset($options['alt'])    ? $options['alt']    : '';
        $title  = isset($options['title'])  ? $options['title']    : '';

        $image_url = $this->image_path.'categories/'.$image;

        if ($type === 'full') {
            $height = isset($options['height']) ? $options['height'] : $this->img_height;
            $width  = isset($options['width'])  ? $options['width']  : $this->img_width;

        } elseif ($type === 'thumb') {
            $height = isset($options['height']) ? $options['height'] : $this->thumb_height;
            $width  = isset($options['width'])  ? $options['width']  : $this->thumb_width;

        } elseif ($type === 'menu') {
            $height = isset($options['height']) ? $options['height'] : $this->thumb_height;
            $width  = isset($options['width'])  ? $options['width']  : $this->thumb_width;

            $image_url = $this->image_path.'categories/menu-images/'.$image;

        } else {
            // to err is human
        }
        if (!empty($image) && $this->checkFileExists($image_url)) {
            if ($type === 'menu') {
                $src = $this->thumb_path.$width.'x'.$height.'/categories/menu-images/'.$image;
            } else {
                $src = $this->thumb_path.$width.'x'.$height.'/categories/'.$image;
            }
        } else {
            if ($type === 'menu') {
                $src = $this->thumb_path.$width.'x'.$height.'/categories/menu-images/no_image.png';
            } else {
                $src = $this->thumb_path.$width.'x'.$height.'/categories/no_image.png';
            }
        }

        return '<img src="'.$src.'"
                    class="'.$class.'"
                    alt="'.$alt.'"
                    title="'.$title.'"
                    width="'.$width.'"
                    height="'.$height.'"
                    style="'.$style.'" />';
    }

    public function manufacturers($type, $image, $options = array())
    {
        $class  = isset($options['class'])  ? $options['class']  : '';
        $style  = isset($options['style'])  ? $options['style']  : '';
        $alt    = isset($options['alt'])    ? $options['alt']    : '';

        $image_url = $this->image_path.'manufacturers/'.$image;

        if ($type === 'full') {
            $height = isset($options['height']) ? $options['height'] : $this->img_height;
            $width  = isset($options['width'])  ? $options['width']  : $this->img_width;

        } elseif ($type === 'thumb') {
            $height = isset($options['height']) ? $options['height'] : $this->thumb_height;
            $width  = isset($options['width'])  ? $options['width']  : $this->thumb_width;

        } else {
            // to err is human
        }

        if (!empty($image) && $this->checkFileExists($image_url)) {
            $src = $image_url;
        } else {
            $src = $this->thumb_path . '100x100/no_image.png';
        }

        return '<img src="'.$src.'"
                    class="'.$class.'"
                    alt="'.$alt.'"
                    width="'.$width.'"
                    height="'.$height.'"
                    style="'.$style.'" />';
    }

    public function phone_manufacturer($image, $manufacturer_name = '', $options = [])
    {
        $class  = isset($options['class'])  ? $options['class']  : '';
        $style  = isset($options['style'])  ? $options['style']  : '';
        $alt    = isset($options['alt'])    ? $options['alt']    : '';

        $image_url = $this->image_path . 'phones_manufacturers/' . $image;
        $height = isset($options['height']) ? $options['height'] : 'auto';
        $width  = isset($options['width'])  ? $options['width']  : 'auto';

        if (!empty($image) && $this->checkFileExists($image_url)) {
            if (isset($options['return_url'])) {
                return $image_url;
            }
            return '<img src="' . $image_url . '"
                        class="' . $class . '"
                        alt="' . $alt . '"
                        width="' . $width . '"
                        height="' . $height . '"
                        style="' . $style . '">';
        } else {
            if (isset($options['return_url'])) {
                return null;
            }
            return '<div style="height: '.(isset($options['height']) ? $options['height'].'px' : 'auto').';"></div>';
        }
    }

    public function phone($image, $options = [], $data_src = false, $src_only = false)
    {
        $class  = isset($options['class'])  ? $options['class']  : '';
        $style  = isset($options['style'])  ? $options['style']  : '';
        $alt    = isset($options['alt'])    ? $options['alt']    : '';
        $title  = isset($options['title'])  ? $options['title']  : '';

        $image_url = $this->image_path . 'phones/' . $image;
        $height = isset($options['height']) ? $options['height'] : 'auto';
        $width  = isset($options['width'])  ? $options['width']  : 'auto';

        if (!empty($image) && $this->checkFileExists($image_url)) {
            $src = $image_url;
        } else {
            $src = $this->thumb_path . '100x100/no_image.png';
            $width = 'auto';
            $height = 'auto';
        }

        $loader = '';

        if ($data_src) {
            $img_src = "data-src='".$src."'";
            $loader = "src='" . $this->image_path . "loader.gif'";
        } else {
            $img_src = "src='".$src."'";
        }

        if ($src_only) {
            return $src;
        }

        return '<img '.$img_src.' '.$loader.' 
                    class="'.$class.'"
                    alt="'.$alt.'"
                    title="'.$title.'"
                    width="'.$width.'"
                    height="'.$height.'"
                    style="'.$style.'" />';
    }

    private function checkFileExists($image_file)
    {
        if (Configure::read('debug') == 2) {
            return true; //TESTING ONLY.
        }
        $file = WWW_ROOT . substr($image_file, strpos($image_file, '/img/') + 1);
        return file_exists($file);
        $file_headers = Cache::read('image_$' . md5($image_file), 'products');
        if (!$file_headers) {
            $file_headers = file_exists();
            Cache::write('image_$' . md5($image_file), $file_headers, 'products');
        }

        if($file_headers[0] == 'HTTP/1.1 404 Not Found'){
            return false;
        } else if ($file_headers[0] == 'HTTP/1.1 302 Found' && $file_headers[7] == 'HTTP/1.1 404 Not Found'){
            return false;
        } else {
            return true;
        }
    }
}