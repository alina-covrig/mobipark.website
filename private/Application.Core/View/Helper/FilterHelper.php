<?php

App::uses('AppHelper', 'View/Helper');

class FilterHelper extends AppHelper
{
    private $filters = null;

    public function getChecked($key, $value, $currentFilterString, $checked = true, $return_bool = false) {

        $filters = $this->_getParsedFilters($currentFilterString);

        if (!isset($filters['s'])) {
            if ($key == "s" && $value == "0") {
                if ($return_bool) {
                    return true;
                } else {
                    return 'checked="checked"';
                }
            }
        }

        if (!isset($filters[$key])) {
            if ($return_bool) {
                return false;
            }
            return '';
        }

        $exists = array_search($value, $filters[$key]);

        if ($return_bool) {
            if ($exists !== false) {
                return true;
            } else {
                return false;
            }
        }

        if ($exists !== false) {
            if ($return_bool) {
                return true;
            }
            if ($checked) {
                return 'checked="checked"';
            }
            return 'selected="selected" ';
        }

        if ($return_bool) {
            return false;
        }

        return '';
    }

    public function getLink($key, $value, $currentFilterString = '', $current_link, $json = false) {
        $before = $this->_getBefore($current_link);
        $after = $this->_getAfter($current_link);
        $filters = $this->_getParsedFilters($currentFilterString);
        $remove = false;

        if (isset($filters[$key])) {
            $remove = array_search($value, $filters[$key]);
        }

        if ($remove !== false) {
            unset($filters[$key][$remove]);
            if (empty($filters[$key])) {
                unset ($filters[$key]);
            }
        } else {
            if ($key == 'pret' || $key == 'ord') {
                $filters[$key][0] = $value;
            } else {
                $filters[$key][] = $value;
            }

            sort($filters[$key]);

            if ($key == "s" && $value == 0) {
                unset($filters["s"]);
            }
        }

        ksort($filters);
        $filter_string = $this->_getFilterString($filters);

        $final = $before . $filter_string . $after;

        //remove json from the middle and add it to the end of the url
        if ($json) {
            return str_replace('.json', '', $final) . '.json';
        }

        return $final;
    }

    //it adds the stock & main products filter to the tag categories
    public function getLinkTagCategories($currentFilterString) {
        $filter_string = '';
        $filters = $this->_getParsedFilters($currentFilterString);

        if(isset($filters['s'])) {
            $filter_string = $this->_getFilterString($filters);
        }

        return $filter_string;
    }

    public function getFilterCategoriesAlias($currentFilterString) {
        $filters = $this->_getParsedFilters($currentFilterString);
        if (!isset($filters['c'][0])) {
            return '';
        }
        return $filters['c'][0];
    }

    public function getFilterTags($currentFilterString, $link) {
        $filters = $this->_getParsedFilters($currentFilterString);
        if(count($filters) > 0) {
            echo '<span class="text-danger">Sterge filtre</span>';
        }
        foreach ($filters as $f => $values) {
            foreach ($values as $value) {
                $link = $this->getLink($f, $value, $currentFilterString, $link);
                $prepend = Inflector::humanize($f) . ':&nbsp;';
                $value = Inflector::humanize($value);
                if ($f == 'p' || $f == 'c') {
                    $prepend = '';
                }
                $aname = $prepend.$value;
                if ($f == 's' && $value == 2) {
                    $aname = __('Doar produse principale');
                }
                if ($f == 's' && $value == 1) {
                    $aname = __('In stoc');
                }
                if ($f == 'pret') {
                    $aname .= '&nbsp;' . Configure::read('LOCAL_CURRENCY');
                }
                if ($f == 'bf') {
                    $aname = __('Black Friday');
                }
                if ($f == 'ord' && $value == 'Rating') {
                    $aname = $prepend.' '.__('Nr. Review-uri');
                }
                echo "<a href='" . $link . "' title='Sterge " . $aname . "' 
                        class='btn btn-sm btn-outline btn-danger label-filter' "
                        . isDoFollow($link) . "
                      >
                    <i class='fa fa-times'></i> " . $aname . "
                </a>";
            }
        }
    }

    public function getPriceValue($min, $max, $currentFilterString) {
        $filters = $this->_getParsedFilters($currentFilterString);
        if (!isset($filters['pret'])) {
            return array(
                floor($min),
                ceil($max),
            );
        }
        $range = explode('-', $filters['pret'][0]);
        return array(
            floor($range[0]),
            ceil($range[1]),
        );
    }

    public function reset($link) {
        $before = $this->_getBefore($link);
        $after = $this->_getAfter($link);
        return $before . $after;
    }

    private function _getAfter($link) {
        $start = strpos($link, '?');
        if ($start === false) {
            return '';
        }
        return substr($link, $start);
    }

    private function _getBefore($link) {
        $length = strpos($link, '/f:');
        if ($length === false) {
            $length = strpos($link, '/page:');
        }
        if ($length === false) {
            return $link;
        }
        return substr($link, 0, $length);
    }

    private function _getFilterString($filters)
    {
        if (empty ($filters)) {
            return '';
        }
        foreach ($filters as $key => &$values) {
            $values = implode(',', $values);
            $values = $key . '=' . $values;
        }
        return '/f:' . implode(';', $filters);
    }

    private function _getParsedFilters($filter_string = '')
    {
        if (!is_null($this->filters)) {
            return $this->filters;
        }
        if (empty($filter_string)) {
            return array();
        }
        $filters = explode(';', $filter_string);
        sort($filters);
        foreach ($filters as $key => $filter_line) {
            $the_line = explode('=', $filter_line);
            $filters[$the_line[0]] = explode(',', $the_line[1]);
            sort($filters[$the_line[0]]);
            unset($filters[$key]);
        }
        $this->filters = $filters;
        return $filters;
    }

    /**
     *  getPriceOptions -- generates price intervals for filters
     *
     * @param $min_max - array of min and max prices over category
     * @param int $steps - number of intervals desired
     * @return array
     */
    public function getPriceOptions($min_max, $steps = 3) {
        $options = [];
        $min = $min_max['min'];
        $max = $min_max['max'];

        //return empty options if the price interval < $steps
        if (($max - $min) < $steps) {
            return $options;
        }

        $range = ($max - $min + 1) / $steps;

        for ($i = 0; $i < $steps; $i++) {
            if($i == 0) {
                $options[$i]['min'] = floor($range * $i + $min);
                $options[$i]['max'] = ceil($options[$i]['min'] + $range);
                continue;
            }
            $options[$i]['min'] = ceil($range * $i + $min);
            $options[$i]['max'] = ceil($options[$i]['min'] + $range - 1);
        }

        return $options;
    }
}