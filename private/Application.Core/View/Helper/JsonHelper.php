<?php
App::uses('AppHelper', 'View/Helper');

class JsonHelper extends AppHelper
{
    //replaces empty string values from an array with null (recursively)
    public function arrayCleanup($arr) {
        array_walk_recursive($arr, function(&$item, $key) {
            if ($item === '') {
                $item = null;
            }
        });

        return $arr;
    }
}
?>