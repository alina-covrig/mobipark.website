<?php
/**
 * Application level View Helper
 *
 * This file is application-wide helper file. You can put all
 * application-wide helper-related methods here.
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.View.Helper
 * @since         CakePHP(tm) v 0.2.9
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */

App::uses('Helper', 'View');

/**
 * Application helper
 *
 * Add your application-wide methods in the class below, your helpers
 * will inherit them.
 *
 * @package       app.View.Helper
 */
class AppHelper extends Helper
{
    public function url($url = null, $full = false)
    {
        if (is_array($url))
        {
            $url = array_merge(
                array(
                    'controller' => $this->request->controller,
                    'plugin' => $this->request->plugin,
                    'action' => $this->action
                ),
                $url);
            ksort($url);
        }

        $key = 'routes_$' . md5(serialize($url).$full);
        if (($link = Cache::read($key, 'router')) === false)
        {
            $link = h(Router::url($url, $full));
            Cache::write($key, $link, 'router');
        }
        return $link;
    }

    public function pretty_price($price)
    {
        $price = round($price, 2);
        return number_format($price, 2, '.', ',');
    }
}
