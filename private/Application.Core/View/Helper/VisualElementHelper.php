<?php

App::uses('AppHelper', 'View/Helper');

class VisualElementHelper extends AppHelper
{
    /**
     * display visual function - chooses between established campaign visual
     * or default value, taken from view.
     *
     * @param $visual - array of CampaignVisuals
     * @param $element - html element we are defining - category, product, etc
     * @param $type - property we are defining - background color, image path etc
     * @return mixed - int value, float value, hex color string or file path
     */

    public function display($visual, $element, $type)
    {
        $defaults = $this->_View->get('defaults');

        if(!isset($visual[$element][$type])) {
            return $defaults[$element][$type]['default_value'];
        }

        switch($visual[$element][$type]['asset_type']){//I know
            case 'color' : {
                return (!empty($visual[$element][$type]['color']))
                    ? $visual[$element][$type]['color']
                    : $visual[$element][$type]['default_value'];
                break;
            }
            case 'value' : {
                if($visual[$element][$type]['value'] === 0)  //actual 0
                {
                    return 0;
                }
                else return !empty($visual[$element][$type]['value'])
                    ? $visual[$element][$type]['value']
                    : $visual[$element][$type]['default_value'];
                break;
            }
        }
    }

    /**
     * Display campaign visual image - used to determine if custom campaign
     * image will be served, or placeholder default
     *
     * @param $img - image file, possibly empty
     * @param $element - html element we are defining - category, product, etc
     * @param $type - property we are defining - background color, image path etc
     * @param $category - determines platform for which the content is served - Web, iOS, Android, etc?
     * @return string - image path
     *
     */

    public function displayImage($img, $element, $type, $category = 'web') {
        $defaults = $this->_View->get('defaults');
        if ( !empty($img[$element][$type]['image']) && ($img[$element][$type]['image'] != '')) {
            return $this->_getImageLocation($img[$element][$type]['image'], $category);
        } else {
            return $this->_getImageLocation($defaults[$element][$type]['default_value'], $category, true);
        }
    }

    /**
     * As above - returns campaign image, but only if it's reviewed,
     * serves default value otherwise
     * @param $images - array of images
     * @param $element - element defined
     * @param $type - property defined
     * @param string $category - platform of content
     * @return string - image path
     */

    public function displayReviewedImage($images, $element, $type, $category = 'web') {
        $defaults = $this->_View->get('defaults');

        if( !empty($images[$element][$type])) {
            if ($images[$element][$type]['is_reviewed'] == 1) {
                if (!empty($images[$element][$type]['image'])
                    && $images[$element][$type]['image'] != '') {
                    return $this->_getImageLocation(
                        $images[$element][$type]['image'], $category
                    );
                }
            }
        } else {
            return $this->_getImageLocation(
                $defaults[$element][$type]['default_value'],
                $category,
                true
            );
        }
    }

    private function _getImageLocation($image_file, $category, $is_default = false) {
        $default_img_path = Configure::read('WEBSITE_URL')
            . 'theme/default/assets/images/default-campaign/';

        if($category == 'ios') {
            $default_img_path .= 'ios-icons/';
        }

        if($category == 'android') {
            //sa fie intr-un ceas bun
            $default_img_path .= 'android-icons/';
        }

        $campaign = $this->_View->viewVars['campaign'];
        $camp_alias = alias($campaign['Campaign']['name']); //Temporary, we will switch to id-based
        $camp_id = $campaign['Campaign']['id'];

        if($is_default != true) {
            return Configure::read('WEBSITE_URL') . 'campaigns/' . $camp_id
                . '/assets/' . $image_file;
        } else {
            return $default_img_path . $image_file;
        }
    }

    public function hasSetValue($visual, $element, $type)
    {
        return !empty($visual[$element]) && !empty($visual[$element][$type]);
    }
}