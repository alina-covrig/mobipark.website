
<?php echo $this->element('small_banner');?>

<div class="blocks">
    <div class="container">
        <div class="text-center italic-title pt-3 pb-2">
            <?=__('Ansamblul rezidential Mobipark')?>
        </div>
        <div class="text-center black-title pb-4">
            <?=__('Etapa 1')?>
            <div class="title-border m-auto pt-1"></div>
        </div>


        <div class="row pb-3">
            <div class="col-12">
                <div class="position-relative overflow-hidden h300">
                    <a href="/bloc-1">
                        <img src="/img/roller/17.png" class="smooth-effect">
                        <div class="text-right bloc-text">
                            MOBIPARK<br>01
                        </div>
                    </a>
                </div>
            </div>
        </div>

        <div class="black-title text-center">
            <?=__('Etapa 2');?>
            <div class="title-border m-auto pt-1"></div>
        </div>

        <div class="row pt-4 pb-3">
            <div class="col-12 col-md-6 mb-4 mb-md-0">
                <div class="position-relative overflow-hidden h300">
                    <img src="/img/roller/11.png"  class="smooth-effect">
                    <div class="text-right bloc-text">
                        MOBIPARK<br>02
                    </div>
                </div>
            </div>
            <div class="col-12 col-md-6">
                <div class="position-relative overflow-hidden h300">
                    <img src="/img/roller/11.png"  class="smooth-effect">
                    <div class="text-right bloc-text">
                        MOBIPARK<br>03
                    </div>
                </div>
            </div>
        </div>

        <div class="black-title text-center">
            <?=__('Etapa 3');?>
            <div class="title-border m-auto pt-1"></div>
        </div>

        <div class="row pt-4 pb-3">
            <div class="col-12 col-md-4 mb-4 mb-md-0">
                <div class="position-relative h300 overflow-hidden">
                    <img src="/img/roller/11.png" class="smooth-effect">
                    <div class="text-right bloc-text">
                        MOBIPARK<br>04
                    </div>
                </div>
            </div>
            <div class="col-12 col-md-4 mb-4 mb-md-0">
                <div class="position-relative h300 overflow-hidden">
                    <img src="/img/roller/11.png" class="smooth-effect">
                    <div class="text-right bloc-text">
                        MOBIPARK<br>05
                    </div>
                </div>
            </div>
            <div class="col-12 col-md-4">
                <div class="position-relative h300 overflow-hidden">
                    <img src="/img/roller/11.png" class="smooth-effect">
                    <div class="text-right bloc-text">
                        MOBIPARK<br>06
                    </div>
                </div>
            </div>
        </div>

    </div>
</div>

<div class="light-bg">
    <div class="container">
        <div class="row pt-3 pt-md-4 pb-3  pb-md-5">
            <div class="col-md-3">
                <div class="italic-title pb-1">Cateva cuvinte despre</div>
                <div class="black-title pb-3">Mobipark</div>
                <div class="title-border"></div>
            </div>
            <div class="col-md-9">
                <div class="description-text pt-3">
                  <?=__('Mobipark City Towers este un complex rezidential inovator, cu o suprafata administrata de 11,700mp, ce transpune in realitate
                    viziunea dezvoltatorului asupra necesitatii spatiului verde ca parte esentiala a vietii oamenilor. Din acest motiv, am ales sa cream 2800mp
                    de spatiu verde in centrul complexului, la care am adaugat
                    1000mp cu dale inierbate.')?>              </div>
            </div>
        </div>
    </div>
</div>
