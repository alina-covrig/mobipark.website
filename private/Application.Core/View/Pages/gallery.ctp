<?php echo $this->element('small_banner');?>

<section class="main-gallery">
    <div class="italic-title text-center dark-bg white pt-4 pb-4">
        <?=__('Galeria Mobipark')?>
    </div>

    <div class="row">
        <div class="col-6 col-lg-6 px-0">
            <a data-fancybox="gallery" href="/img/gallery/84.jpg"><img src="/img/gallery/84.jpg" class="w-100"></a>
        </div>
        <div class="col-6 col-lg-6 px-0">
            <a data-fancybox="gallery" href="/img/gallery/90.jpg"><img src="/img/gallery/90.jpg" class="w-100"></a>
        </div>
    </div>
    <div class="row">
        <div class="col-12 col-lg-4 px-0">
            <a data-fancybox="gallery" href="/img/gallery/91.jpg"><img src="/img/gallery/91.jpg" class="w-100"></a>
        </div>
        <div class="col-6 col-lg-4 px-0">
            <a data-fancybox="gallery" href="/img/gallery/70.jpg"><img src="/img/gallery/70.jpg" class="w-100"></a>
        </div>
        <div class="col-6 col-lg-4 px-0">
            <a data-fancybox="gallery" href="/img/gallery/92.jpg"><img src="/img/gallery/92.jpg" class="w-100"></a>
        </div>
    </div>
    <div class="row">
        <div class="col-12 col-lg-3 px-0">
            <a data-fancybox="gallery" href="/img/gallery/94.jpg"><img src="/img/gallery/94.jpg" class="w-100"></a>
        </div>
        <div class="col-6 col-lg-6 px-0">
            <a data-fancybox="gallery" href="/img/gallery/95.jpg"><img src="/img/gallery/95.jpg" class="w-100"></a>
        </div>
        <div class="col-6 col-lg-3 px-0">
            <a data-fancybox="gallery" href="/img/gallery/96.jpg"><img src="/img/gallery/96.jpg" class="w-100"></a>
        </div>
    </div>
    <div class="row">
        <div class="col-12 col-lg px-0">
            <a data-fancybox="gallery" href="/img/gallery/72.jpg"><img src="/img/gallery/72.jpg" class="w-100"></a>
        </div>
        <div class="col-6 col-lg-5 px-0">
            <a data-fancybox="gallery" href="/img/gallery/97.jpg"><img src="/img/gallery/97.jpg" class="w-100"></a>
        </div>
        <div class="col-6 col-lg px-0">
            <a data-fancybox="gallery" href="/img/gallery/73.jpg"><img src="/img/gallery/73.jpg" class="w-100"></a>
        </div>
    </div>


</section>