
<?php echo $this->element('small_banner');?>

<div class="container">
    <div class="text-center italic-title pt-3 pb-3">
        <?=__('Tipuri de apartament')?>
    </div>



    <ul class="nav nav-pills mb-3" id="ap-types" role="tablist">
        <li class="nav-item position-relative d-none d-md-block">
            <a class="nav-link active"  data-toggle="pill" href="#studio" role="tab" aria-controls="studio" aria-selected="true"><?=__('Studio')?></a>
            <div class="oblique-border"></div>
        </li>

        <li class="nav-item position-relative d-none d-md-block">
            <a class="nav-link"  data-toggle="pill" href="#room2" role="tab" aria-controls="room2" aria-selected="false"><?=__('2 Camere')?></a>
            <div class="oblique-border"></div>
        </li>
        <li class="nav-item position-relative d-none d-md-block">
            <a class="nav-link" data-toggle="pill" href="#room3" role="tab" aria-controls="room3" aria-selected="false"><?=__('3 Camere')?></a>
            <div class="oblique-border"></div>
        </li>
        <li class="nav-item position-relative d-none d-md-block">
            <a class="nav-link" data-toggle="pill" href="#room4" role="tab" aria-controls="room4" aria-selected="false"><?=__('4 Camere')?></a>
            <div class="oblique-border"></div>
        </li>
        <li class="nav-item position-relative d-none d-md-block">
            <a class="nav-link" data-toggle="pill" href="#duplex" role="tab" aria-controls="room4" aria-selected="false"><?=__('Duplex')?></a>
            <div class="oblique-border"></div>
        </li>

        <li class="nav-item dropdown d-block d-md-none w-100 text-center mobile-app-types">
          <!--  <a class="nav-link dropdown-toggle" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">Alege tipul de apartament</a>
            <div class="dropdown-menu">
                <a class="dropdown-item" data-toggle="pill" href="#studio" role="tab"><?=__('Studio')?></a>
                <div class="dropdown-divider"></div>
                <a class="dropdown-item" data-toggle="pill" href="#room2" role="tab"><?=__('2 camere')?></a>
                <div class="dropdown-divider"></div>
                <a class="dropdown-item" data-toggle="pill" href="#room3" role="tab"><?=__('3 camere')?></a>
                <div class="dropdown-divider"></div>
                <a class="dropdown-item" data-toggle="pill" href="#room4" role="tab"><?=__('4 camere')?></a>
                <div class="dropdown-divider"></div>
                <a class="dropdown-item" data-toggle="pill" href="#duplex" role="tab"><?=__('duplex')?></a>
            </div>-->
            <select id="choose-apartment">
                <option value="studio"><?=__('Studio')?></option>
                <option  value="room2"><?=__('2 camere')?></option>
                <option  value="room3"><?=__('3 camere')?></option>
                <option  value="room4"><?=__('4 camere')?></option>
                <option  value="duplex"><?=__('Duplex')?></option>
            </select>
        </li>
    </ul>
    <div class="tab-content" id="pills-tabContent">

        <div class="tab-pane fade show active" id="studio" role="tabpanel">
            <div class="text-center black-title"><?=__('Tip 02 ')?> </div>
            <div class="text-center italic-title">46.2 m²</div>
            <div class="title-border m-auto"></div>
            <div class="row pt-3 pb-4">
                <div class="col-md-8">
                    <div class="text-center apartment-design">
                        <img src="/assets/template-img/Asset_1.png">
                    </div>
                </div>
                <div class="col-md-4">
                        <table class="table table-striped details-table">
                            <tbody>
                            <tr>
                                <td><strong>01</strong>. Hol</td>
                                <td><strong>3.39 m²</strong></td>
                            </tr>
                            <tr>
                                <td><strong>02</strong>. Camera de zi</td>
                                <td><strong>3.39 m²</strong></td>
                            </tr>
                            <tr>
                                <td><strong>03</strong>. Bucatarie</td>
                                <td><strong>7.97 m²</strong></td>
                            </tr>
                            <tr>
                                <td><strong>04</strong>. Dormitor 01</td>
                                <td><strong>10.74 m²</strong></td>
                            </tr>
                            <tr>
                                <td><strong>05</strong>. Dressing</td>
                                <td><strong>7.23 m²</strong></td>
                            </tr>
                            <tr>
                                <td><strong>06</strong>. Baie 01</td>
                                <td><strong>5.29 m²</strong></td>
                            </tr>
                            <tr>
                                <td><strong>07</strong>. Dormitor 02</td>
                                <td><strong>12.58 m²</strong></td>
                            </tr>
                            <tr>
                                <td><strong>08</strong>. Baie 02</td>
                                <td><strong>7.23 m²</strong></td>
                            </tr>
                            <tr>
                                <td><strong>09</strong>. Balcon 01</td>
                                <td><strong>6.76 m²</strong></td>
                            </tr>
                            <tr>
                                <td><strong>10</strong>. Balcon 02</td>
                                <td><strong>13.14 m²</strong></td>
                            </tr>
                            <tr>
                                <td><strong>Total</strong></td>
                                <td><strong>96.76 m²</strong></td>
                            </tr>
                            </tbody>
                        </table>
                    <a href="/bloc-1" class="green-button text-center white d-block"><?=__('Vezi disponibilitatea pe etaje')?></a>
                </div>
            </div>
            <div class="price-details light-bg p-4 d-none d-md-flex mb-4">
                <div class="d-flex">
                    <span class="italic-title mr-3"><?=__('Preturi studio Tip 02:')?></span>
                    <div class="confort-button p-2 d-flex mr-2">
                        <div class="text-left pr-2"><?=__('CONFORT ')?></div>
                        <div class="text-right"> <?=__('85 900 Euro')?></div>
                    </div>
                    <div class="premium-button p-2 d-flex">
                        <div class="text-left pr-2"><?=__('PREMIUM ')?></div>
                        <div class="text-right"> <?=__('92 900 Euro')?></div>
                    </div>
                </div>


                <div data-toggle="modal" data-target="#big-studio-details" class="cursor-pointer mobi-green"><?=__('Vezi toate detaliile de pret')?></div>

            </div>

            <!------------------STUDIO Tip 2 - small type --------------------->

            <div class="text-center black-title"><?=__('Tip 02 ')?> </div>
            <div class="text-center italic-title">44.6 m²</div>
            <div class="title-border m-auto"></div>
            <div class="row pt-3 pb-4">
                <div class="col-md-8">
                    <div class="text-center apartment-design">
                        <img src="/assets/template-img/Asset_1.png">
                    </div>
                </div>
                <div class="col-md-4">
                    <table class="table table-striped details-table">
                        <tbody>
                        <tr>
                            <td><strong>01</strong>. Studio</td>
                            <td><strong>33.17 m²</strong></td>
                        </tr>
                        <tr>
                            <td><strong>02</strong>. Baie</td>
                            <td><strong>5.19 m²</strong></td>
                        </tr>
                        <tr>
                            <td><strong>03</strong>. Bucatarie</td>
                            <td><strong>7.28 m²</strong></td>
                        </tr>
                        <tr>
                            <td><strong>Total</strong></td>
                            <td><strong>44.6 m²</strong></td>
                        </tr>
                        </tbody>
                    </table>
                    <a href="/bloc-1" class="green-button d-block text-center white"><?=__('Vezi disponibilitatea pe etaje')?></a>
                </div>
            </div>
            <div class="price-details light-bg p-4 d-none d-md-flex mb-4">
                <div class="d-flex">
                    <span class="italic-title mr-3"><?=__('Preturi studio Tip 02:')?></span>
                    <div class="confort-button p-2 d-flex mr-2">
                        <div class="text-left pr-2"><?=__('CONFORT ')?></div>
                        <div class="text-right"> <?=__('85 900 Euro')?></div>
                    </div>
                    <div class="premium-button p-2 d-flex">
                        <div class="text-left pr-2"><?=__('PREMIUM ')?></div>
                        <div class="text-right"> <?=__('92 900 Euro')?></div>
                    </div>
                </div>
                <div data-toggle="modal" data-target="#small-studio-details" class="cursor-pointer mobi-green"><?=__('Vezi toate detaliile de pret')?></div>
            </div>
        </div>
        <!------- 2 rooms ------>
        <div class="tab-pane fade" id="room2" role="tabpanel">
            <!------ Type 03 ------->
            <div class="text-center black-title"><?=__('Tip 03 ')?> </div>
            <div class="text-center italic-title">65.9 m²</div>
            <div class="title-border m-auto"></div>
            <div class="row pt-3 pb-4">
                <div class="col-md-8">
                    <div class="text-center apartment-design">
                        <img src="/assets/template-img/Asset_1.png">
                    </div>
                </div>
                <div class="col-md-4">
                    <table class="table table-striped details-table">
                        <tbody>
                        <tr>
                            <td><strong>01</strong>. Studio</td>
                            <td><strong>33.17 m²</strong></td>
                        </tr>
                        <tr>
                            <td><strong>02</strong>. Baie</td>
                            <td><strong>5.19 m²</strong></td>
                        </tr>
                        <tr>
                            <td><strong>03</strong>. Bucatarie</td>
                            <td><strong>7.28 m²</strong></td>
                        </tr>
                        <tr>
                            <td><strong>Total</strong></td>
                            <td><strong>44.6 m²</strong></td>
                        </tr>
                        </tbody>
                    </table>
                    <a href="/bloc-1" class="green-button d-block text-center white"><?=__('Vezi disponibilitatea pe etaje')?></a>
                </div>
            </div>
            <div class="price-details light-bg p-4 d-none d-md-flex mb-4">
                <div class="d-flex">
                    <span class="italic-title mr-3"><?=__('Preturi apartament Tip 03:')?></span>
                    <div class="confort-button p-2 d-flex mr-2">
                        <div class="text-left pr-2"><?=__('CONFORT ')?></div>
                        <div class="text-right"> <?=__('85 900 Euro')?></div>
                    </div>
                    <div class="premium-button p-2 d-flex">
                        <div class="text-left pr-2"><?=__('PREMIUM ')?></div>
                        <div class="text-right"> <?=__('92 900 Euro')?></div>
                    </div>
                </div>
                <div data-toggle="modal" data-target="#small-studio-details" class="cursor-pointer mobi-green"><?=__('Vezi toate detaliile de pret')?></div>
            </div>

            <!---- type 04 ----->
            <div class="text-center black-title"><?=__('Tip 04 ')?> </div>
            <div class="text-center italic-title">70.6 m²</div>
            <div class="title-border m-auto"></div>
            <div class="row pt-3 pb-4">
                <div class="col-md-8">
                    <div class="text-center apartment-design">
                        <img src="/assets/template-img/Asset_1.png">
                    </div>
                </div>
                <div class="col-md-4">
                    <table class="table table-striped details-table">
                        <tbody>
                        <tr>
                            <td><strong>01</strong>. Studio</td>
                            <td><strong>33.17 m²</strong></td>
                        </tr>
                        <tr>
                            <td><strong>02</strong>. Baie</td>
                            <td><strong>5.19 m²</strong></td>
                        </tr>
                        <tr>
                            <td><strong>03</strong>. Bucatarie</td>
                            <td><strong>7.28 m²</strong></td>
                        </tr>
                        <tr>
                            <td><strong>Total</strong></td>
                            <td><strong>44.6 m²</strong></td>
                        </tr>
                        </tbody>
                    </table>
                    <a href="/bloc-1" class="green-button d-block text-center white"><?=__('Vezi disponibilitatea pe etaje')?></a>
                </div>
            </div>
            <div class="price-details light-bg p-4 d-none d-md-flex mb-4">
                <div class="d-flex">
                    <span class="italic-title mr-3"><?=__('Preturi apartament Tip 04:')?></span>
                    <div class="confort-button p-2 d-flex mr-2">
                        <div class="text-left pr-2"><?=__('CONFORT ')?></div>
                        <div class="text-right"> <?=__('85 900 Euro')?></div>
                    </div>
                    <div class="premium-button p-2 d-flex">
                        <div class="text-left pr-2"><?=__('PREMIUM ')?></div>
                        <div class="text-right"> <?=__('92 900 Euro')?></div>
                    </div>
                </div>
                <div data-toggle="modal" data-target="#small-studio-details" class="cursor-pointer mobi-green"><?=__('Vezi toate detaliile de pret')?></div>
            </div>

            <!---- type 04 small ------>
            <div class="text-center black-title"><?=__('Tip 04 ')?> </div>
            <div class="text-center italic-title">67.1 m²</div>
            <div class="title-border m-auto"></div>
            <div class="row pt-3 pb-4">
                <div class="col-md-8">
                    <div class="text-center apartment-design">
                        <img src="/assets/template-img/Asset_1.png">
                    </div>
                </div>
                <div class="col-md-4">
                    <table class="table table-striped details-table">
                        <tbody>
                        <tr>
                            <td><strong>01</strong>. Studio</td>
                            <td><strong>33.17 m²</strong></td>
                        </tr>
                        <tr>
                            <td><strong>02</strong>. Baie</td>
                            <td><strong>5.19 m²</strong></td>
                        </tr>
                        <tr>
                            <td><strong>03</strong>. Bucatarie</td>
                            <td><strong>7.28 m²</strong></td>
                        </tr>
                        <tr>
                            <td><strong>Total</strong></td>
                            <td><strong>44.6 m²</strong></td>
                        </tr>
                        </tbody>
                    </table>
                    <a href="/bloc-1" class="green-button d-block text-center white"><?=__('Vezi disponibilitatea pe etaje')?></a>
                </div>
            </div>
            <div class="price-details light-bg p-4 d-none d-md-flex mb-4">
                <div class="d-flex">
                    <span class="italic-title mr-3"><?=__('Preturi apartament Tip 04:')?></span>
                    <div class="confort-button p-2 d-flex mr-2">
                        <div class="text-left pr-2"><?=__('CONFORT ')?></div>
                        <div class="text-right"> <?=__('85 900 Euro')?></div>
                    </div>
                    <div class="premium-button p-2 d-flex">
                        <div class="text-left pr-2"><?=__('PREMIUM ')?></div>
                        <div class="text-right"> <?=__('92 900 Euro')?></div>
                    </div>
                </div>
                <div data-toggle="modal" data-target="#small-studio-details" class="cursor-pointer mobi-green"><?=__('Vezi toate detaliile de pret')?></div>
            </div>

            <!---- type 05 ------->
            <div class="text-center black-title"><?=__('Tip 05 ')?> </div>
            <div class="text-center italic-title">71.3 m²</div>
            <div class="title-border m-auto"></div>
            <div class="row pt-3 pb-4">
                <div class="col-md-8">
                    <div class="text-center apartment-design">
                        <img src="/assets/template-img/Asset_1.png">
                    </div>
                </div>
                <div class="col-md-4">
                    <table class="table table-striped details-table">
                        <tbody>
                        <tr>
                            <td><strong>01</strong>. Studio</td>
                            <td><strong>33.17 m²</strong></td>
                        </tr>
                        <tr>
                            <td><strong>02</strong>. Baie</td>
                            <td><strong>5.19 m²</strong></td>
                        </tr>
                        <tr>
                            <td><strong>03</strong>. Bucatarie</td>
                            <td><strong>7.28 m²</strong></td>
                        </tr>
                        <tr>
                            <td><strong>Total</strong></td>
                            <td><strong>44.6 m²</strong></td>
                        </tr>
                        </tbody>
                    </table>
                    <a href="/bloc-1" class="green-button d-block text-center white"><?=__('Vezi disponibilitatea pe etaje')?></a>
                </div>
            </div>
            <div class="price-details light-bg p-4 d-none d-md-flex mb-4">
                <div class="d-flex">
                    <span class="italic-title mr-3"><?=__('Preturi apartament Tip 05:')?></span>
                    <div class="confort-button p-2 d-flex mr-2">
                        <div class="text-left pr-2"><?=__('CONFORT ')?></div>
                        <div class="text-right"> <?=__('85 900 Euro')?></div>
                    </div>
                    <div class="premium-button p-2 d-flex">
                        <div class="text-left pr-2"><?=__('PREMIUM ')?></div>
                        <div class="text-right"> <?=__('92 900 Euro')?></div>
                    </div>
                </div>
                <div data-toggle="modal" data-target="#small-studio-details" class="cursor-pointer mobi-green"><?=__('Vezi toate detaliile de pret')?></div>
            </div>

        </div>
        <!------- 3 rooms ------>
        <div class="tab-pane fade" id="room3" role="tabpanel">
            <!---- type 01 ------->
            <div class="text-center black-title"><?=__('Tip 01 ')?> </div>
            <div class="text-center italic-title">97.2 m²</div>
            <div class="title-border m-auto"></div>
            <div class="row pt-3 pb-4">
                <div class="col-md-8">
                    <div class="text-center apartment-design">
                        <img src="/assets/template-img/Asset_1.png">
                    </div>
                </div>
                <div class="col-md-4">
                    <table class="table table-striped details-table">
                        <tbody>
                        <tr>
                            <td><strong>01</strong>. Studio</td>
                            <td><strong>33.17 m²</strong></td>
                        </tr>
                        <tr>
                            <td><strong>02</strong>. Baie</td>
                            <td><strong>5.19 m²</strong></td>
                        </tr>
                        <tr>
                            <td><strong>03</strong>. Bucatarie</td>
                            <td><strong>7.28 m²</strong></td>
                        </tr>
                        <tr>
                            <td><strong>Total</strong></td>
                            <td><strong>44.6 m²</strong></td>
                        </tr>
                        </tbody>
                    </table>
                    <a href="/bloc-1" class="green-button d-block text-center white"><?=__('Vezi disponibilitatea pe etaje')?></a>
                </div>
            </div>
            <div class="price-details light-bg p-4 d-none d-md-flex mb-4">
                <div class="d-flex">
                    <span class="italic-title mr-3"><?=__('Preturi apartament Tip 01:')?></span>
                    <div class="confort-button p-2 d-flex mr-2">
                        <div class="text-left pr-2"><?=__('CONFORT ')?></div>
                        <div class="text-right"> <?=__('85 900 Euro')?></div>
                    </div>
                    <div class="premium-button p-2 d-flex">
                        <div class="text-left pr-2"><?=__('PREMIUM ')?></div>
                        <div class="text-right"> <?=__('92 900 Euro')?></div>
                    </div>
                </div>
                <div data-toggle="modal" data-target="#small-studio-details" class="cursor-pointer mobi-green"><?=__('Vezi toate detaliile de pret')?></div>
            </div>

            <!---- type 06 ------>
            <div class="text-center black-title"><?=__('Tip 06 ')?> </div>
            <div class="text-center italic-title">126.6 m²</div>
            <div class="title-border m-auto"></div>
            <div class="row pt-3 pb-4">
                <div class="col-md-8">
                    <div class="text-center apartment-design">
                        <img src="/assets/template-img/Asset_1.png">
                    </div>
                </div>
                <div class="col-md-4">
                    <table class="table table-striped details-table">
                        <tbody>
                        <tr>
                            <td><strong>01</strong>. Studio</td>
                            <td><strong>33.17 m²</strong></td>
                        </tr>
                        <tr>
                            <td><strong>02</strong>. Baie</td>
                            <td><strong>5.19 m²</strong></td>
                        </tr>
                        <tr>
                            <td><strong>03</strong>. Bucatarie</td>
                            <td><strong>7.28 m²</strong></td>
                        </tr>
                        <tr>
                            <td><strong>Total</strong></td>
                            <td><strong>44.6 m²</strong></td>
                        </tr>
                        </tbody>
                    </table>
                    <a href="/bloc-1" class="green-button d-block text-center white"><?=__('Vezi disponibilitatea pe etaje')?></a>
                </div>
            </div>
            <div class="price-details light-bg p-4 d-none d-md-flex mb-4">
                <div class="d-flex">
                    <span class="italic-title mr-3"><?=__('Preturi apartament Tip 06:')?></span>
                    <div class="confort-button p-2 d-flex mr-2">
                        <div class="text-left pr-2"><?=__('CONFORT ')?></div>
                        <div class="text-right"> <?=__('85 900 Euro')?></div>
                    </div>
                    <div class="premium-button p-2 d-flex">
                        <div class="text-left pr-2"><?=__('PREMIUM ')?></div>
                        <div class="text-right"> <?=__('92 900 Euro')?></div>
                    </div>
                </div>
                <div data-toggle="modal" data-target="#small-studio-details" class="cursor-pointer mobi-green"><?=__('Vezi toate detaliile de pret')?></div>
            </div>

            <!------ type 07 -------->
            <div class="text-center black-title"><?=__('Tip 07 ')?> </div>
            <div class="text-center italic-title">94.4 m²</div>
            <div class="title-border m-auto"></div>
            <div class="row pt-3 pb-4">
                <div class="col-md-8">
                    <div class="text-center apartment-design">
                        <img src="/assets/template-img/Asset_1.png">
                    </div>
                </div>
                <div class="col-md-4">
                    <table class="table table-striped details-table">
                        <tbody>
                        <tr>
                            <td><strong>01</strong>. Studio</td>
                            <td><strong>33.17 m²</strong></td>
                        </tr>
                        <tr>
                            <td><strong>02</strong>. Baie</td>
                            <td><strong>5.19 m²</strong></td>
                        </tr>
                        <tr>
                            <td><strong>03</strong>. Bucatarie</td>
                            <td><strong>7.28 m²</strong></td>
                        </tr>
                        <tr>
                            <td><strong>Total</strong></td>
                            <td><strong>44.6 m²</strong></td>
                        </tr>
                        </tbody>
                    </table>
                    <a href="/bloc-1" class="green-button d-block text-center white"><?=__('Vezi disponibilitatea pe etaje')?></a>
                </div>
            </div>
            <div class="price-details light-bg p-4 d-none d-md-flex mb-4">
                <div class="d-flex">
                    <span class="italic-title mr-3"><?=__('Preturi apartament Tip 07:')?></span>
                    <div class="confort-button p-2 d-flex mr-2">
                        <div class="text-left pr-2"><?=__('CONFORT ')?></div>
                        <div class="text-right"> <?=__('85 900 Euro')?></div>
                    </div>
                    <div class="premium-button p-2 d-flex">
                        <div class="text-left pr-2"><?=__('PREMIUM ')?></div>
                        <div class="text-right"> <?=__('92 900 Euro')?></div>
                    </div>
                </div>
                <div data-toggle="modal" data-target="#small-studio-details" class="cursor-pointer mobi-green"><?=__('Vezi toate detaliile de pret')?></div>
            </div>

        </div>
        <!------- 4 rooms ------>
        <div class="tab-pane fade" id="room4" role="tabpanel">

            <!---- type 08 ------->
            <div class="text-center black-title"><?=__('Tip 08 ')?> </div>
            <div class="text-center italic-title">235.9 m²</div>
            <div class="title-border m-auto"></div>
            <div class="row pt-3 pb-4">
                <div class="col-md-8">
                    <div class="text-center apartment-design">
                        <img src="/assets/template-img/Asset_1.png">
                    </div>
                </div>
                <div class="col-md-4">
                    <table class="table table-striped details-table">
                        <tbody>
                        <tr>
                            <td><strong>01</strong>. Studio</td>
                            <td><strong>33.17 m²</strong></td>
                        </tr>
                        <tr>
                            <td><strong>02</strong>. Baie</td>
                            <td><strong>5.19 m²</strong></td>
                        </tr>
                        <tr>
                            <td><strong>03</strong>. Bucatarie</td>
                            <td><strong>7.28 m²</strong></td>
                        </tr>
                        <tr>
                            <td><strong>Total</strong></td>
                            <td><strong>44.6 m²</strong></td>
                        </tr>
                        </tbody>
                    </table>
                    <a href="/bloc-1" class="green-button d-block text-center white"><?=__('Vezi disponibilitatea pe etaje')?></a>
                </div>
            </div>
            <div class="price-details light-bg p-4 d-none d-md-flex mb-4">
                <div class="d-flex">
                    <span class="italic-title mr-3"><?=__('Preturi apartament Tip 08:')?></span>
                    <div class="confort-button p-2 d-flex mr-2">
                        <div class="text-left pr-2"><?=__('CONFORT ')?></div>
                        <div class="text-right"> <?=__('85 900 Euro')?></div>
                    </div>
                    <div class="premium-button p-2 d-flex">
                        <div class="text-left pr-2"><?=__('PREMIUM ')?></div>
                        <div class="text-right"> <?=__('92 900 Euro')?></div>
                    </div>
                </div>
                <div data-toggle="modal" data-target="#small-studio-details" class="cursor-pointer mobi-green"><?=__('Vezi toate detaliile de pret')?></div>
            </div>

        </div>

        <!------- duplex ------>
        <div class="tab-pane fade" id="duplex" role="tabpanel">
            <!---- small ------->
            <div class="text-center black-title"><?=__('Duplex 1 ')?> </div>
            <div class="text-center italic-title">58.8 m²</div>
            <div class="title-border m-auto"></div>
            <div class="row pt-3 pb-4">
                <div class="col-md-8">
                    <div class="text-center apartment-design">
                        <img src="/assets/template-img/Asset_1.png">
                    </div>
                </div>
                <div class="col-md-4">
                    <table class="table table-striped details-table">
                        <tbody>
                        <tr>
                            <td><strong>01</strong>. Studio</td>
                            <td><strong>33.17 m²</strong></td>
                        </tr>
                        <tr>
                            <td><strong>02</strong>. Baie</td>
                            <td><strong>5.19 m²</strong></td>
                        </tr>
                        <tr>
                            <td><strong>03</strong>. Bucatarie</td>
                            <td><strong>7.28 m²</strong></td>
                        </tr>
                        <tr>
                            <td><strong>Total</strong></td>
                            <td><strong>44.6 m²</strong></td>
                        </tr>
                        </tbody>
                    </table>
                    <a href="/bloc-1" class="green-button d-block text-center white"><?=__('Vezi disponibilitatea pe etaje')?></a>
                </div>
            </div>
            <div class="price-details light-bg p-4 d-none d-md-flex mb-4">
                <div class="d-flex">
                    <span class="italic-title mr-3"><?=__('Preturi aduplex 1:')?></span>
                    <div class="confort-button p-2 d-flex mr-2">
                        <div class="text-left pr-2"><?=__('CONFORT ')?></div>
                        <div class="text-right"> <?=__('85 900 Euro')?></div>
                    </div>
                    <div class="premium-button p-2 d-flex">
                        <div class="text-left pr-2"><?=__('PREMIUM ')?></div>
                        <div class="text-right"> <?=__('92 900 Euro')?></div>
                    </div>
                </div>
                <div data-toggle="modal" data-target="#small-studio-details" class="cursor-pointer mobi-green"><?=__('Vezi toate detaliile de pret')?></div>
            </div>

            <!---- 2 ------->
            <div class="text-center black-title"><?=__('Duplex 2 ')?> </div>
            <div class="text-center italic-title">107.4 m²</div>
            <div class="title-border m-auto"></div>
            <div class="row pt-3 pb-4">
                <div class="col-md-8">
                    <div class="text-center apartment-design">
                        <img src="/assets/template-img/Asset_1.png">
                    </div>
                </div>
                <div class="col-md-4">
                    <table class="table table-striped details-table">
                        <tbody>
                        <tr>
                            <td><strong>01</strong>. Studio</td>
                            <td><strong>33.17 m²</strong></td>
                        </tr>
                        <tr>
                            <td><strong>02</strong>. Baie</td>
                            <td><strong>5.19 m²</strong></td>
                        </tr>
                        <tr>
                            <td><strong>03</strong>. Bucatarie</td>
                            <td><strong>7.28 m²</strong></td>
                        </tr>
                        <tr>
                            <td><strong>Total</strong></td>
                            <td><strong>44.6 m²</strong></td>
                        </tr>
                        </tbody>
                    </table>
                    <a href="/bloc-1" class="green-button d-block text-center white"><?=__('Vezi disponibilitatea pe etaje')?></a>
                </div>
            </div>
            <div class="price-details light-bg p-4 d-none d-md-flex mb-4">
                <div class="d-flex">
                    <span class="italic-title mr-3"><?=__('Preturi duplex 2:')?></span>
                    <div class="confort-button p-2 d-flex mr-2">
                        <div class="text-left pr-2"><?=__('CONFORT ')?></div>
                        <div class="text-right"> <?=__('85 900 Euro')?></div>
                    </div>
                    <div class="premium-button p-2 d-flex">
                        <div class="text-left pr-2"><?=__('PREMIUM ')?></div>
                        <div class="text-right"> <?=__('92 900 Euro')?></div>
                    </div>
                </div>
                <div data-toggle="modal" data-target="#small-studio-details" class="cursor-pointer mobi-green"><?=__('Vezi toate detaliile de pret')?></div>
            </div>

        </div>

    </div>


</div>


<div class="light-bg">
    <div class="container">
        <div class="row pt-3 pb-3 pt-md-5 pb-md-5">
            <div class="col-md-3">
                <div class="italic-title pb-1"><?=__('Cateva cuvinte despre')?></div>
                <div class="black-title pb-3"><?=__('Mobipark')?></div>
                <div class="title-border"></div>
            </div>
            <div class="col-md-9">
                <div class="description-text pt-3">
                    <?=__('Mobipark City Towers este un complex rezidential inovator, cu o suprafata administrata de 11,700mp, ce transpune in realitate
                        viziunea dezvoltatorului asupra necesitatii spatiului verde ca parte esentiala a vietii oamenilor. Din acest motiv, am ales sa cream 2800mp
                        de spatiu verde in centrul complexului, la care am adaugat
                        1000mp cu dale inierbate. ')?>
                </div>
            </div>
        </div>
    </div>
</div>

<?php echo $this->element('/app-types-modal/studio_1');?>
<?php echo $this->element('/app-types-modal/studio_2');?>

