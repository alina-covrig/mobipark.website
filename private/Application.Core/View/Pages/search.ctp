<?php /** @var array $articles */ ?>
<?php /** @var string $search_term */ ?>

<?php $this->start('meta_data'); ?>
    <title><?= __('Cautare: %s', $search_term) ?> | <?= Configure::read("WEBSITE_NAME"); ?></title>
    <meta name="description" content="<?= __('Cautare: %s', $search_term) ?>">
    <meta name="robots" content="index, follow">
<?php $this->end(); ?>

<!---------------- CONTENT ------------------------>
<div class="content-wrapper">
    <div class="container px-sm-0 pb-4">
        <h4 class="red-h4">
            <b><?= __('Cautare: %s', $search_term) ?></b>
        </h4>

        <div class="articles-wrapper pt-3">
            <?php foreach($articles as $article):?>
                <?php echo $this->element('Pages/article_template', ['article' => $article]); ?>
            <?php endforeach;?>

            <?php
            $counter_item = count($articles);
            $r = $counter_item % 4;
            for ($i = 0; $i < $r; $i++):?>
                <div class="article article-placeholder"></div>
            <?php endfor; ?>
        </div>

        <?php echo $this->element('pagination'); ?>

    </div>
</div>