<?php /** @var array $document_types */ ?>
<?php /** @var array $registry_types */ ?>

<?php
$about1 = __('Mobipark City Towers este un ansamblu rezidențial nou în Constanta, 
        amplasat între Strada Napoli și reprezentanta Renault de pe Bulevardul Tomis, 
         la doar doua minute de mers pe jos pana la complexul comercial Tom. Dezvoltatorii acestui proiect 
         si-au dorit sa dea o nouă valoare spațiului verde, ca parte integrantă a vieții de zi cu zi a riveranilor.');
$about2 = __('lorem Ipsum is simply dummy text of the printing and typesetting industry.
                     Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, 
                     when an unknown printer took a galley of type and scrambled it to make a type 
                     specimen book. It has survived not only five centuries when an unknown printer took a galley of type and scrambled it to make a type 
                     specimen book. It has survived not only five centuries');
$about3 = __('Ideea ambitioasa de la care au pornit dezvoltatorii acestui proiect este aceea de a construi un 
          complex nou, inovator și corect sistematizat, în jurul unui parc verde. Dacă istoria ne-a dovedit ca omul poate 
          distruge mediul înconjurător, noi ne dorim sa dovedim ca oamenii pot contribui și la reconstruirea habitatului 
          natural al oamenilor, în comuniune cu natura, nerenuntand la confortul oferit de inovațiile în domeniul construcțiilor 
          și amenajărilor interioare.'.'<br>'.' 
          Acest spațiu nou, ne dorim sa devină un refugiu al familiei moderne, aproape de natura, 
          rămânând în același timp ancorati la reteaua urbana atat de necesara vietii. Centrele comerciale Carrefour, Dedeman,
           Dechatlon și Selgros se afla la doar 5 minute de mers pe jos. În imediată apropiere se află de asemenea mall-ul VIVO
            și maternitatea Euromaterna, școli și grădinițe. În partea de nord se afla spitalul Ovidius Clinical Hospital astfel 
            incat locatarii ansamblului nu vor duce lipsa de nimic esential pentru desfasurarea armonioasa a vietii. 
            Intrarea în stațiunea Mamaia și Satul de vacanță aflandu-se la doar 5 minute de mers cu mașina, recomanda noile apartamente 
            ca investiție pentru persoanele din alte localități, care își doresc o casa de vacanță la mare, cu posibilitate de închiriere pe tot parcursul anului.
            ');
$about4 = __('Conceptul Mobipark a venit ca rezolvare a unei probleme recurente în viețile
                 locuitorilor din spațiul urban - desprinderea ireversibilă de natura. Aceasta desprindere
                  de cadrul natural aduce cu sine nenumărate probleme de sanatate, creșterea poluării,
                   accentuarea stresului si anxietatii, lipsa locurilor de joacă pentru copii și a parcurilor pentru
                    desfășurarea activităților fizice zilnice.
                   ');
$energetic_eficienty = __('Printr-o proiectare minuțioasă am reușit să reducem consumurile la minimum 
                prin limitarea punților termice și folosirea strategiilor pasive, realizând astfel o locuință
                 cu facturi lunare de întreținere mici. Pe termen lung, eficiența energetică determină economii
                  substanțiale de energie și costuri de întreținere a clădirilor cât și a locuințelor individuale. ');
$safety = __('În ceea ce privește construcția fiecare bloc folosește o structura de beton armat 
                cu plansee tip dala, stalpi de beton armat.');
$confort =__('Am ținut cont de parametrii de confort interior, de la temperatură și
                 umiditate până la nivelul de oxigen, CO2 și  iluminat natural.');
$automatization = __('Sistemul inteligent de automatizare îți permite să controlezi
                 și să monitorizezi casa de la distanță, fie că ești plecat în concediu și la întoarcere
                  vrei să ai confortul optim, fie că vrei să știi în fiecare secundă consumul case, 
                  acest sistem te va ajuta să fii mereu informat');
$building_descr = __('La nivel de bloc pentru a asigura eficiența energetică termosistemul este compus din 
                   fațada ventilată, izolație termică exterioară din vata bazaltică, zidarie din blocuri
                    bca de 30cm și tâmplărie termopan cu un coeficient de 0.9.');
$ventilated_facade = __('Am ales un sistem de fațada ventilată cu scopul a asigura termoizolarea eficientă a interiorului, 
                    iar acest lucru se produce datorită faptului că spaţiul dintre plăcile exterioare şi termoizolație creează 
                    un strat de aer ventilat, care în combinaţie cu acţiunea unui strat izolant aplicat pe pereţii clădirilor 
                    îmbunătăţeşte eficienţa termică a clădirii. Astfel, sistemul de faţadă ventilată protejează structura peretelui
                     şi creşte rezistenţa la variaţiile de temperatură ce se vor produce între mediul exterior şi cel interior.
                    La nivel arhitectural se vor folosi plăci ceramice albe de mari dimensiuni ');
$termic_isolation = __('Produsele din vată minerală bazaltică creează medii interioare sănătoase pentru 
                    oricine. Noi ne petrecem mult timp în interior, așadar clădirile au un impact major 
                    asupra sănătății și bunăstării noastre. Menținând totodată și temperatura optimă,
                     proprietățile termice ale bazaltului ajută la protejarea oamenilor și bunurilor
                      împotriva mucegaiului ferindu-ne de fenomenele vremii, dar oferind și un mediu 
                      interior sigur și confortabil care ne ajută să avem vieți sănătoase.').'<br><br>'.
                    __('Precum oamenii, clădirile trebuie să rămână sănătoase.
                 Vata minerală bazaltică este permeabilă, adică permite umezelii să 
                 treacă prin pereți și să iasă din clădire. Aceasta protejează clădirile împotriva 
                 deteriorărilor cauzate de putrezire, mucegai și umiditate. Produsele din vata minerala 
                 bazaltica permit clădirilor să respire, lucru care vă ajută pe dumneavoastră să respirați mai bine.');

?>

<?php $this->start('meta_data'); ?>
    <title><?= __('Homepage') ?> | <?= Configure::read("WEBSITE_NAME"); ?></title>
    <meta name="robots" content="index, follow">
<?php $this->end(); ?>

<!---------------- CONTENT ------------------------>
<?php echo $this->element('banners');?>

<section class="second-section">


        <div class="container pt-4 pb-3">
            <div class="italic-title"><?=__('Gaseste locuinta potrivita');?></div>
            <div class="black-title"><?=__('Apartamente')?></div>
            <div class="title-border mt-1 mt-md-3"></div>
        </div>

        <div class="light-bg">
            <div class="container pb-3 pt-3 d-none d-lg-block">
                <ul class="nav nav-pills text-uppercase" id="appartments-tabs" role="tablist">
                    <li class="nav-item">
                        <a class="nav-link" id="studio-tab" data-toggle="pill" href="#pills-studio" role="tab" aria-controls="pills-studio" aria-selected="true">
                            <div class="app-tab-title text-center"><?=__('Studio')?></div>

                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link active" id="tworooms-tab" data-toggle="pill" href="#pills-tworooms" role="tab" aria-controls="pills-tworooms" aria-selected="false">
                            <div class="app-tab-title text-center"><?=__('2 camere')?></div>

                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" id="threerooms-tab" data-toggle="pill" href="#pills-threerooms" role="tab" aria-controls="pills-threerooms" aria-selected="false">
                            <div class="app-tab-title text-center"><?=__('3 camere')?></div>

                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" id="fourrooms-tab" data-toggle="pill" href="#pills-fourrooms" role="tab" aria-controls="pills-fourrooms" aria-selected="false">
                            <div class="app-tab-title text-center"><?=__('4 camere')?></div>

                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" id="duplex-tab" data-toggle="pill" href="#pills-duplex" role="tab" aria-controls="pills-duplex" aria-selected="false">
                            <div class="app-tab-title text-center"><?=__('Duplex')?></div>
                            <!--<div class="small-tab-title text-center"><?=__('Incepand de la 95 000 €')?></div>-->
                        </a>
                    </li>
                </ul>
                <div class="tab-content" id="pills-appartments">
                    <div class="tab-pane fade" id="pills-studio" role="tabpanel" aria-labelledby="studio-tab">
                        <div class="app-templates">
                            <div class="template">
                                <a href="/studio2">
                                    <img src="/assets/template-img/01_studio_tip_02_46,2mp.png">
                                    <div class="row template-details pt-3">
                                        <div class="text-left col-6 text-uppercase mobi-green">Tip 02</div>
                                        <div class="text-right col-6"><strong>46.2 m²</strong></div>
                                    </div>
                                </a>
                            </div>
                            <div class="template">
                                <a href="/studio1">
                                    <img src="/assets/template-img/02_studio_tip_02_44,6mp.png">
                                    <div class="row template-details pt-3">
                                        <div class="text-left col-6 text-uppercase mobi-green">Tip 02</div>
                                        <div class="text-right col-6"><strong>44.6 m²</strong></div>
                                    </div>
                                </a>
                            </div>
                        </div>
                    </div>
                    <div class="tab-pane fade show active" id="pills-tworooms" role="tabpanel" aria-labelledby="tworooms-tab">
                        <div class="app-templates">
                            <div class="template">
                                <a href="/apartament-2camere-tip03">
                                    <img src="/assets/template-img/05_2_camere_tip_03_65,9mp.png">
                                    <div class="row template-details pt-3">
                                        <div class="text-left col-6 text-uppercase mobi-green">Tip 03</div>
                                        <div class="text-right col-6"><strong>65.9 m²</strong></div>
                                    </div>
                                </a>
                            </div>
                            <div class="template">
                                <a href="/apartament-2camere-tip04">
                                    <img src="/assets/template-img/07_2_camere_tip_04_70,6,mp.png">
                                    <div class="row template-details pt-3">
                                        <div class="text-left col-6 text-uppercase mobi-green">Tip 04</div>
                                        <div class="text-right col-6"><strong>70.6 m²</strong></div>
                                    </div>
                                </a>
                            </div>
                            <div class="template">
                                <a href="/apartament-2camere-small-tip04">
                                    <img src="/assets/template-img/08_2_camere_tip_04_67,1mp.png">
                                    <div class="row template-details pt-3">
                                        <div class="text-left col-6 text-uppercase mobi-green">Tip 04</div>
                                        <div class="text-right col-6"><strong>67.1 m²</strong></div>
                                    </div>
                                </a>
                            </div>
                            <div class="template">
                                <a href="/apartament-2camere-tip05">
                                    <img src="/assets/template-img/09_2_camere_tip_05_71,3mp.png">
                                    <div class="row template-details pt-3">
                                        <div class="text-left col-6 text-uppercase mobi-green">Tip 05</div>
                                        <div class="text-right col-6"><strong>71.3 m²</strong></div>
                                    </div>
                                </a>
                            </div>
                        </div>
                    </div>
                    <div class="tab-pane fade" id="pills-threerooms" role="tabpanel" aria-labelledby="threerooms-tab">
                        <div class="app-templates">
                            <div class="template">
                                <a href="/apartament-3camere-tip01">
                                <img src="/assets/template-img/12_3_camere_tip_01_97,2mp.png">
                                <div class="row template-details pt-3">
                                    <div class="text-left col-6 text-uppercase mobi-green">Tip 01</div>
                                    <div class="text-right col-6"><strong>97.2 m²</strong></div>
                                </div>
                                </a>
                            </div>
                            <div class="template">
                                <a href="/apartament-3camere-tip06">
                                    <img src="/assets/template-img/14_3_camere_tip_06_126,6mp.png">
                                    <div class="row template-details pt-3">
                                        <div class="text-left col-6 text-uppercase mobi-green">Tip 06</div>
                                        <div class="text-right col-6"><strong>126.6 m²</strong></div>
                                    </div>
                                </a>
                            </div>
                            <div class="template">
                                <a href="/apartament-3camere-tip07">
                                <img src="/assets/template-img/15_3_camere_tip_07_94,4mp.png">
                                <div class="row template-details pt-3">
                                    <div class="text-left col-6 text-uppercase mobi-green">Tip 07</div>
                                    <div class="text-right col-6"><strong>94.4 m²</strong></div>
                                </div>
                                </a>
                            </div>

                        </div>
                    </div>
                    <div class="tab-pane fade" id="pills-fourrooms" role="tabpanel" aria-labelledby="fourrooms-tab">
                        <div class="app-templates">
                            <div class="template">
                                <a href="/apartament-4camere-tip08">
                                    <img src="/assets/template-img/16_4_camere_tip_08_235,9mp.png">
                                    <div class="row template-details pt-3">
                                        <div class="text-left col-6 text-uppercase mobi-green">Tip 08</div>
                                        <div class="text-right col-6"><strong>235.9 m²</strong></div>
                                    </div>
                                </a>
                            </div>
                        </div>
                    </div>
                    <div class="tab-pane fade" id="pills-duplex" role="tabpanel" aria-labelledby="duplex-tab">
                        <div class="app-templates">
                            <div class="template">
                                <a href="/duplex1">
                                    <img src="/assets/template-img/17_duplex_59,8mp.png">
                                    <div class="row template-details pt-3">
                                        <div class="text-left col-6 text-uppercase mobi-green">1</div>
                                        <div class="text-right col-6"><strong>58.8 m²</strong></div>
                                    </div>
                                </a>
                            </div>
                            <div class="template">
                                <a href="/duplex2">
                                    <img src="/assets/template-img/18_duplex_107,4mp.png">
                                    <div class="row template-details pt-3">
                                        <div class="text-left col-6 text-uppercase mobi-green">2</div>
                                        <div class="text-right col-6"><strong>107.4 m²</strong></div>
                                    </div>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="text-center italic-title pt-2">
                    <?=__('Mai multe detalii, in curand')?>
                </div>
            </div>
            <!----  mobile-building-templates ---->
            <div class="container pt-3 pb-2 d-block d-lg-none">
                <div class="accordion">
                    <div class="accordion-item">
                        <h3 class="title">
                            <span><?=__('Studio')?></span>
                            <i class="fas fa-chevron-down"></i>
                        </h3>

                        <div class="accordion-content">
                            <div class="template">
                                <a href="/studio2">
                                    <img src="/assets/template-img/01_studio_tip_02_46,2mp.png">
                                    <div class="row template-details pt-3">
                                        <div class="text-left col-6 text-uppercase mobi-green">Tip 02</div>
                                        <div class="text-right col-6"><strong>46.2 m²</strong></div>
                                    </div>
                                </a>
                            </div>
                            <div class="template">
                                <a href="/studio1">
                                    <img src="/assets/template-img/02_studio_tip_02_44,6mp.png">
                                    <div class="row template-details pt-3">
                                        <div class="text-left col-6 text-uppercase mobi-green">Tip 02</div>
                                        <div class="text-right col-6"><strong>44.6 m²</strong></div>
                                    </div>
                                </a>
                            </div>
                        </div>

                    </div>
                    <div class="accordion-item">
                        <h3 class="title">
                            <span><?=__('2 camere')?></span>
                            <i class="fas fa-chevron-down"></i>
                        </h3>
                        <div class="accordion-content">
                            <div class="template">
                                <a href="/apartament-2camere-tip03">
                                    <img src="/assets/template-img/05_2_camere_tip_03_65,9mp.png">
                                    <div class="row template-details pt-3">
                                        <div class="text-left col-6 text-uppercase mobi-green">Tip 03</div>
                                        <div class="text-right col-6"><strong>65.9 m²</strong></div>
                                    </div>
                                </a>
                            </div>
                            <div class="template">
                                <a href="/apartament-2camere-tip04">
                                    <img src="/assets/template-img/07_2_camere_tip_04_70,6,mp.png">
                                    <div class="row template-details pt-3">
                                        <div class="text-left col-6 text-uppercase mobi-green">Tip 04</div>
                                        <div class="text-right col-6"><strong>70.6 m²</strong></div>
                                    </div>
                                </a>
                            </div>
                            <div class="template">
                                <a href="/apartament-2camere-small-tip04">
                                    <img src="/assets/template-img/08_2_camere_tip_04_67,1mp.png">
                                    <div class="row template-details pt-3">
                                        <div class="text-left col-6 text-uppercase mobi-green">Tip 04</div>
                                        <div class="text-right col-6"><strong>67.1 m²</strong></div>
                                    </div>
                                </a>
                            </div>
                            <div class="template">
                                <a href="/apartament-2camere-tip05">
                                    <img src="/assets/template-img/09_2_camere_tip_05_71,3mp.png">
                                    <div class="row template-details pt-3">
                                        <div class="text-left col-6 text-uppercase mobi-green">Tip 05</div>
                                        <div class="text-right col-6"><strong>71.3 m²</strong></div>
                                    </div>
                                </a>
                            </div>
                        </div>
                    </div>
                    <div class="accordion-item">
                        <h3 class="title">
                            <span><?=__('3 camere')?></span>
                            <i class="fas fa-chevron-down"></i>
                        </h3>
                        <div class="accordion-content">
                            <div class="template">
                                <a href="/apartament-3camere-tip01">
                                    <img src="/assets/template-img/12_3_camere_tip_01_97,2mp.png">
                                    <div class="row template-details pt-3">
                                        <div class="text-left col-6 text-uppercase mobi-green">Tip 01</div>
                                        <div class="text-right col-6"><strong>97.2 m²</strong></div>
                                    </div>
                                </a>
                            </div>
                            <div class="template">
                                <a href="/apartament-3camere-tip06">
                                    <img src="/assets/template-img/14_3_camere_tip_06_126,6mp.png">
                                    <div class="row template-details pt-3">
                                        <div class="text-left col-6 text-uppercase mobi-green">Tip 06</div>
                                        <div class="text-right col-6"><strong>126.6 m²</strong></div>
                                    </div>
                                </a>
                            </div>
                            <div class="template">
                                <a href="/apartament-3camere-tip07">
                                    <img src="/assets/template-img/15_3_camere_tip_07_94,4mp.png">
                                    <div class="row template-details pt-3">
                                        <div class="text-left col-6 text-uppercase mobi-green">Tip 07</div>
                                        <div class="text-right col-6"><strong>94.4 m²</strong></div>
                                    </div>
                                </a>
                            </div>
                        </div>
                    </div>
                    <div class="accordion-item">
                        <h3 class="title">
                            <span><?=__('4 camere')?></span>
                            <i class="fas fa-chevron-down"></i>
                        </h3>
                        <div class="accordion-content">
                            <div class="template">
                                <a href="/apartament-4camere-tip08">
                                    <img src="/assets/template-img/16_4_camere_tip_08_235,9mp.png">
                                    <div class="row template-details pt-3">
                                        <div class="text-left col-6 text-uppercase mobi-green">Tip 08</div>
                                        <div class="text-right col-6"><strong>235.9 m²</strong></div>
                                    </div>
                                </a>
                            </div>
                        </div>
                    </div>
                    <div class="accordion-item">
                        <h3 class="title">
                            <span><?=__('Duplex')?></span>
                            <i class="fas fa-chevron-down"></i>
                        </h3>
                        <div class="accordion-content">
                            <div class="template">
                                <a href="/duplex1">
                                    <img src="/assets/template-img/17_duplex_59,8mp.png">
                                    <div class="row template-details pt-3">
                                        <div class="text-left col-6 text-uppercase mobi-green">Duplex 1</div>
                                        <div class="text-right col-6"><strong>58.8 m²</strong></div>
                                    </div>
                                </a>
                            </div>
                            <div class="template">
                                <a href="/duplex2">
                                    <img src="/assets/template-img/18_duplex_107,4mp.png">
                                    <div class="row template-details pt-3">
                                        <div class="text-left col-6 text-uppercase mobi-green"> Duplex 2</div>
                                        <div class="text-right col-6"><strong>107.4 m²</strong></div>
                                    </div>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>


        </div>
</section>

    <div class="container text-left mt-4 mb-4">
        <div class="italic-title pb-2">
            <?=__('Ansamblul Mobipark');?>
        </div>
        <div class="black-title pb-2">
            <?=__('Descopera noul tau cartier');?>
        </div>
        <div class="title-border "></div>

        <div class="map pt-4 position-relative">
            <img src="/assets/images/map_03.jpg" class="d-block">
        </div>
    </div>


<div class="position-relative dark-bg key-points mb-4">
    <div class="container">
        <div class="col-lg-7">

                    <div class="features row pt-1 pb-4 col-12 d-flex">
                        <div class="col-12">
                            <div class="white-title  pt-4"><?=__('Puncte cheie')?></div>
                            <div class="line-wrapper  pb-4 pb-lg-5">
                                <div class="white-mini-border pt-1"></div>
                            </div>
                        </div>

                        <div class="col-6 col-md-4 pb-4 pb-lg-5 text-center text-md-left">
                            <img src="/assets/images/garden.png">
                            <div class="feature-txt text-uppercase  pt-3">
                                <?=__('Spatiu verde extins');?>
                            </div>
                        </div>
                        <div class="col-6 col-md-4 pb-4 pb-lg-5 text-center">
                            <img src="/assets/images/energy.png">
                            <div class="feature-txt text-uppercase pt-3">
                                <?=__('Energie verde');?>
                            </div>
                        </div>
                        <div class="col-6 col-md-4 pb-4 pb-lg-5 text-center">
                            <img src="/assets/images/gates.png">
                            <div class="feature-txt text-uppercase pt-3">
                                <?=__('Acces securizat');?>
                            </div>
                        </div>
                        <div class="col-6 col-md-4 text-center text-md-left">
                            <img src="/assets/images/stores.png">
                            <div class="feature-txt text-uppercase pt-3">
                                <?=__('Centre comerciale in apropiere');?>
                            </div>
                        </div>
                        <div class="col-6 col-md-4 text-center">
                            <img src="/assets/images/parking.png">
                            <div class="feature-txt text-uppercase pt-3">
                                <?=__('Parcare privata');?>
                            </div>
                        </div>
                        <div class="col-6 col-md-4 text-center">
                            <img src="/assets/images/prices.png">
                            <div class="feature-txt text-uppercase pt-3">
                                <?=__('Preturi competitive');?>
                            </div>
                        </div>
                    </div>

            </div>
        </div>
        <div class="col-md-5 features-block pr-0"></div>
</div>


<!--
    <div class="container pb-5">
        <div class="row features-2">
            <div class="col-md-4 bg-img mb-3 mb-md-0"></div>
            <div class="col-md-8 pl-auto pl-md-4">
                <div class="italic-title">
                    <?=__('Apartament special');?>
                </div>
                <div class="black-title mb-2">
                    <?=__('Duplex');?>
                </div>
                <div class="title-border"></div>
                <div class="description-text mt-3 mt-md-5 mb-md-4 mb-2 content-preview">
                    <?=$about2;?>
                    <div class="see-more-gradient mt-2 d-block d-sm-none">
                        <span class="see-more" id="see-more-phones">Citeste mai mult<i class="fas fa-chevron-down offers" style="padding-left:10px;"></i></span>
                    </div>
                </div>

                <hr class="mt-2 mt-md-4">
                <div class="facility-list text-center mt-md-5">


                        <div class="facility">
                            <img src="/assets/images/icon_baie@2x.png">
                            <div class="mt-2 gray"><?=__('3 Bai');?></div>
                        </div>
                        <div class="facility">
                            <img src="/assets/images/icon_sufragerie@2x.png">
                            <div class="mt-2 gray"><?=__('1 Sufragerie');?></div>
                        </div>
                        <div class="facility">
                            <img src="/assets/images/icon_dormitor@2x.png">
                            <div class="mt-2 gray"><?=__('2 Dormitoare');?></div>
                        </div>
                        <div class="facility">
                            <img src="/assets/images/icon_bucatarie@2x.png">
                            <div class="mt-2 gray"><?=__('1 Bucatarie');?></div>
                        </div>
                        <div class="facility">
                            <img src="/assets/images/icon_balcon@2x.png">
                            <div class="mt-2 gray"><?=__('1 Balcon');?></div>
                        </div>
                    <div class="facility">
                        <img src="/assets/images/icon_terasa@2x.png">
                        <div class="mt-2 gray"><?=__('1 Terasa');?></div>
                    </div>

                </div>
            </div>
        </div>
    </div>
--->


<div class="container" id="despre-noi">
    <div class="italic-title text-left text-md-center">
        <?=__('Conceptul Mobipark')?>
    </div>
    <div class="black-title text-left text-md-center mb-3">
        <?=__('Dor de verde ca parte din viața')?>
    </div>
    <div class="title-border m-md-auto"></div>
    <div class="mt-4 text-left pb-md-3 content-preview">
        <?=$descriptions[0]['Description']['description1'];?>
        <div class="see-more-gradient mt-2 d-block d-sm-none">
            <span class="see-more" id="see-more-phones">Citeste mai mult<i class="fas fa-chevron-down offers" style="padding-left:10px;"></i></span>
        </div>
    </div>

</div>
<div class="light-bg">
    <div class="container">
        <div class="row mt-2 mt-mb-4 mb-2 mb-md-4">
            <div class="col-12 pt-2 pt-md-4 pb-2 pb-md-4">
                <div class="description-text content-preview">
                    <?=$descriptions[0]['Description']['description2'];?>
                    <div class="see-more-gradient mt-2 d-block d-sm-none">
                        <span class="see-more light-bg" id="see-more-phones">Citeste mai mult<i class="fas fa-chevron-down offers" style="padding-left:10px;"></i></span>
                    </div>
                   </div>
            </div>
        </div>
    </div>
</div>

<!----- DETAILS -------->
<div class="position-relative">
    <div class="container">
        <div class="row mb-3 mb-md-5">
            <div class="col-12 col-md-7">
                <div class="italic-title"><?=__('Principiile folosite in proiectarea casei tale')?></div>
                <div class="mb-3 mt-3">
                    <?=$descriptions[0]['Description']['description3'];?>
                </div>
            </div>
            <div class="col-12 col-md-4 d-xl-none">
                <div class="right-side-img"></div>
            </div>
        </div>
    </div>
    <div class="right-side-img col-md-5"></div>
</div>

<div class="container">
    <div class="black-title text-left text-md-center pb-2">
        <?=__('Descriere constructiva la nivel de bloc')?>
    </div>
    <div class="title-border m-md-auto"></div>
</div>

<div class="light-bg">
    <div class="container">
        <div class="row mt-4 mb-0 mb-sm-4">
            <div class="col-12">
                <div class="description-text pt-4 pb-4">
                    <?=$descriptions[0]['Description']['description4'];?>
                </div>
            </div>
        </div>
    </div>
</div>


<!----- DETAILS -------->
<div class="position-relative">
    <div class="left-side-img d-none d-xl-block"></div>
<div class="container">
    <div class="row mb-0 mb-md-5">
        <div class="col-12 col-md-4 px-0 px-sm-auto">
            <div class="left-side-img d-block d-xl-none"></div>
        </div>
        <div class="col-12 col-md-8 pl-md-4">
            <div class="mb-3 mt-3">
                <div class="italic-title"><?=__('Fatada ventilata')?></div>
                <div class="mt-2 content-preview">
                    <?=$descriptions[0]['Description']['description5'];?>
                    <div class="see-more-gradient mt-2 d-block d-sm-none">
                        <span class="see-more" id="see-more-phones">Citeste mai mult<i class="fas fa-chevron-down offers" style="padding-left:10px;"></i></span>
                    </div>
                </div>
            </div>
            <div class="mb-3">
                <div class="italic-title"><?=__('Izolația termică - materiale')?></div>
                <div class="mt-2 content-preview">
                    <?=$descriptions[0]['Description']['description6'];?>
                    <div class="see-more-gradient mt-2 d-block d-sm-none">
                        <span class="see-more" id="see-more-phones">Citeste mai mult<i class="fas fa-chevron-down offers" style="padding-left:10px;"></i></span>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</div>

<!------ CONTACT ZONE ------>
<div class="contact-bg contact-zone position-relative" id="contact">
    <div class="container">
        <div class="row">
            <div class="col-md-6">
                <img src="/assets/images/logo_mobipark.png" class="d-none d-md-block position-absolute">
            </div>
            <div class="col-md-6">
                <div class="contact-form">
                    <div class="italic-title white pb-2"><?=__('Contacteaza-ne')?></div>
                    <h3 class="text-uppercase white pb-3"><?=__('Folosind formularul de mai jos')?></h3>
                    <form id="contact-form" method="post" action="/contact/sendContact" role="form" data-toggle="validator">
                        <div class="form-group">
                            <input type="text" class="form-control" id="exampleInputName" required name="name" placeholder="nume" data-error="* <?=__('Camp obligatoriu')?>">
                            <div class="help-block with-errors"></div>
                        </div>
                        <div class="form-group">
                            <input type="email" class="form-control" id="exampleInputEmail" required name="email" placeholder="email" data-error="* <?=__('Adresa de email invalida')?>">
                            <div class="help-block with-errors"></div>
                        </div>
                        <div class="form-group">
                            <input type="phone" class="form-control" id="exampleInputPhone" required name="phone" placeholder="telefon" data-error="* <?=__('Numar de telefon incorect')?>">
                            <div class="help-block with-errors"></div>
                        </div>
                        <div class="form-group">
                            <textarea class="form-control" id="exampleFormControlMesaj" rows="3" required name="subject" placeholder="mesaj" data-error="* <?=__('Camp obligatoriu')?>"></textarea>
                            <div class="help-block with-errors"></div>
                        </div>
                        <button type="submit" class="btn green-button white mb-2" id="send-contact">Trimite</button>
                        <div class="form-check">
                            <input type="checkbox" class="form-check-input" id="exampleCheck1" required>
                            <small><label class="form-check-label white" for="exampleCheck1"><?=__('*Am citit si sunt de acord cu')?>
                                    <a href="#" class="blue"><?=__('Termenii, conditiile')?></a> si
                            <a href="#" class="blue"><?=__('Politica de confidentialitate')?></a></label></small>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <div class="dark-layer"></div>
</div>




