<?php /** @var array $page */ ?>

<?php $this->start('meta_data'); ?>
<title><?= $page['Page']['title'] ?> | <?= Configure::read("WEBSITE_NAME"); ?></title>
<meta name="description" content="<?= $page['Page']['meta_description'] ?>">
<meta name="robots" content="index, follow">
<meta property="fb:app_id" content="<?=Configure::read('FB_APP_ID')?>" />
<meta property="og:title" content="<?= $page['Page']['title'] ?>" />
<meta property="og:type" content="article" />
<?php $this->end(); ?>

<div class="article-page">

    <div class="container px-sm-0">
        <div class="top-article-content position-relative">
            <div class="main-title pt-5 text-uppercase">
                <?= $page['Page']['title'] ?>
            </div>
        </div>

        <div class="main-content">
            <?php echo $page['Page']['content']; ?>
        </div>
    </div>

</div>
