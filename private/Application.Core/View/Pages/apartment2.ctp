
<?php echo $this->element('block_banner');?>

<div class="container">
    <div class="italic-title text-center pt-3">
        <?=__('46.2 m2')?>
    </div>
    <div class="black-title text-center pb-1 ">
        <?=__('Studio')?>
    </div>

    <div class="title-border m-auto "></div>

    <div class="row pt-5">
        <div class="col-md-6">
            <div class="cloudimage-360"
                 data-folder="assets/360-images/spinner_ap_01/"
                 data-filename="spinner-{index}.png"
                 data-amount="40"
                 data-speed="50"
                 data-ratio="1"
            >
            </div>
        </div>
        <div class="col-md-6">
            <div class="font13 mb-3">
                <?=__('Lorem Ipsum is simply dummy text of the printing 
                and typesetting industry. Lorem Ipsum has been the industry\'s
                 standard dummy text ever since the 1500s, when an unknown 
                 printer took a galley of type and scrambled it to make a 
                 type specimen book.It has survived not only five centuries, 
                 but also the leap into electronic typesetting, remaining essentially unchanged. ')?>
            </div>
            <div class="d-flex">
                <div class="icon-gallery mr-4">
                    <img src="/assets/images/icon_360_view_gray@2x.png">
                    <div class="font-12 text-center">
                        <?=__('vezi apartamentul in 360')?>
                    </div>
                </div>
                <div class="icon-gallery cursor-pointer">
                        <img src="/assets/images/icon_galery_green@2x.png">
                        <div class="font-12 text-center mobi-green"><?=__('vezi galeria')?></div>
                </div>
            </div>
            <div class="light-bg text-center p-2 mt-2">
                <div class="italic-title mb-3"><?=__('Pret:')?></div>
                <div class="d-flex justify-center mb-3">
                    <div class="confort-button p-2 d-flex mr-2">
                        <div class="text-left pr-2"><?=__('CONFORT')?> </div>
                        <div class="text-right"> 85 900 Euro</div>
                    </div>
                    <div class="premium-button p-2 d-flex">
                        <div class="text-left pr-2"><?=__('PREMIUM')?> </div>
                        <div class="text-right"> 92 900 Euro</div>
                    </div>
                </div>
                <div data-toggle="modal" data-target="#small-studio-details" class="text-center pb-3 cursor-pointer mobi-green"><?=__('Vezi toate detaliile de pret')?></div>
            </div>

        </div>
    </div>
    <!--=========  Mini Features  ===========-->
    <div class="row mt-5 mb-5 mini-features">
        <div class="col-4 col-md-2">
            <div class="text-center pt-2 pb-2">
                <img src="/assets/images/icon_suprafata.png">
              <div class="pt-3"><?=__('68.26 m2')?></div>
            </div>
        </div>
        <div class="col-4 col-md-2">
            <div class="text-center pt-2 pb-2">
                <img src="/assets/images/icon_dormitor_1.png">
                <div class="pt-3"><?=__('1 dormitor')?></div>
            </div>
        </div>
        <div class="col-4 col-md-2">
            <div class="text-center pt-2 pb-2">
                <img src="/assets/images/icon_sufragerie_1.png">
                <div class="pt-3"><?=__('1 sufragerie')?></div>
            </div>
        </div>
        <div class="col-4 col-md-2">
            <div class="text-center pt-2 pb-2">
                <img src="/assets/images/icon_baie_1.png">
                <div class="pt-3"><?=__('1 baie')?></div>
            </div>
        </div>
        <div class="col-4 col-md-2">
            <div class="text-center pt-2 pb-2">
                <img src="/assets/images/icon_balcon_1.png">
                <div class="pt-3"><?=__('1 balcon')?></div>
            </div>
        </div>
        <div class="col-4 col-md-2">
            <div class="text-center pt-2 pb-2">
                <img src="/assets/images/icon_bucatarie_1.png">
                <div class="pt-3"><?=__('1 bucatarie')?></div>
            </div>
        </div>
    </div>



    <div class="row pt-3 pb-4">
        <div class="col-md-4">
            <table class="table table-striped details-table">
                <tbody>
                <tr>
                    <td><strong>01</strong>. <?=__('Hol')?></td>
                    <td><strong>33.17 m²</strong></td>
                </tr>
                <tr>
                    <td><strong>02</strong>. <?=__('Camera de zi')?></td>
                    <td><strong>5.19 m²</strong></td>
                </tr>
                <tr>
                    <td><strong>02</strong>. <?=__('Bucatarie')?></td>
                    <td><strong>5.19 m²</strong></td>
                </tr>
                <tr>
                    <td><strong>03</strong>. <?=__('Baie')?></td>
                    <td><strong>7.28 m²</strong></td>
                </tr>
                <tr>
                    <td><strong>02</strong>. <?=__('Dormitor')?></td>
                    <td><strong>5.19 m²</strong></td>
                </tr>
                <tr>
                    <td><strong>02</strong>. <?=__('Balcon')?></td>
                    <td><strong>5.19 m²</strong></td>
                </tr>
                <tr>
                    <td><strong><?=__('Total')?></strong></td>
                    <td><strong>44.6 m²</strong></td>
                </tr>
                </tbody>
            </table>
        </div>
        <div class="col-md-8">
            <div class="text-center apartment-design">
                <img src="/assets/template-img/Asset_1.png">
            </div>
        </div>
    </div>

</div>

<div class="light-bg">
    <div class="container">
        <div class="row pt-3 pb-3 pt-md-5 pb-md-5">
            <div class="col-md-3">
                <div class="italic-title pb-1"><?=__('Cateva cuvinte despre')?></div>
                <div class="black-title pb-3"><?=__('Mobipark')?></div>
                <div class="title-border"></div>
            </div>
            <div class="col-md-9">
                <div class="description-text pt-3">
                    <?=__('Mobipark City Towers este un complex rezidential nou in Constanta, amplasat in zona de nord-vest, creat cu simt de raspundere
                         fata de oameni si natura.')?>
                </div>
            </div>
        </div>
    </div>
</div>

<?php echo $this->element('apartment_info');?>
