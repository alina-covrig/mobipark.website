<div class="home-big-banner hero">
    <!-- ========================================== SECTION – HERO ========================================= -->

    <div id="banner-carousel" class="carousel slide" data-ride="carousel">
        <ol class="carousel-indicators">
            <li data-target="#banner-carousel" data-slide-to="0" class="active"></li>
            <li data-target="#banner-carousel" data-slide-to="1"></li>
            <li data-target="#banner-carousel" data-slide-to="2"></li>

        </ol>
        <div class="carousel-inner">

            <?php if (!empty($banners)): ?>
                <?php foreach ($banners as $banner) : ?>
                    <div class="carousel-item">
                        <img src="http://mobipark.website/img/gallery/<?php echo rawurlencode($banner['Banner']['filename']) ?>" class="d-block w-100" alt="...">
                        <a href="<?=$banner['Banner']['link']?>"
                           class="promo-banner-trigger"
                           data-promo-id="desktop-banner-<?= $banner['Banner']['id'] ?>"
                           data-promo-name="Banner Desktop <?= $banner['Banner']['alt'] ?>"
                           data-promo-creative="Banner Desktop <?= $banner['Banner']['filename'] ?>"
                           data-promo-position="Desktop Home Page - slide <?= $banner['Banner']['order'] ?>"
                           title="<?=$banner['Banner']['alt']?>" style="display: block; width: 100%; height: 100%;">
                            <div class="container-fluid">
                                <?php echo $banner['Banner']['html']; ?>
                            </div><!-- /.container-fluid -->
                        </a>
                        <div class="banner-layer"></div>
                    </div>
                <?php endforeach;?>
            <?php endif;?>
        </div>
    </div>


    <div  class="d-none d-md-block banner-text">
        <div id="container">
            <div class="d-flex">
                <div class="text-right">
                    <span><?=__('Gaseste-ti')?></span><br>
                    <span><?=__('apartamentul potrivit')?></span>
                </div>
                <div class="italic-banner-title px-5"><?=__('sau')?></div>
                <div class="text-left">
                    <span> <?=__('Descopera')?></span><br>
                    <span> <?=__('noul complex in 360°')?></span>
                </div>
            </div>
        </div>
    </div>

    <div class="download-brochure position-absolute">
        <a href="/assets/brosura/mobipark_brosura_a4_rev02.pdf" class="position-relative text-uppercase"><?=__('Descarca Brosura')?>
            <i class="fas fa-download ml-2"></i>
        </a>
    </div>

    <div class="scroll-wrapper position-absolute d-none d-md-block">
        <button type="button" class="scroll-section position-relative"></button>
    </div>

    <!-- ========================================= SECTION – HERO : END ========================================= -->
</div><!-- /.homebanner-holder -->



