<div class="container">
    <div class="text-lg-center italic-title mt-4 mt-lg-5 pl-2 pl-lg-0"><?=__('Descriere constructiva')?></div>
    <div class="text-center text-uppercase black-title mt-2 mb-3 d-none d-lg-block"><?=__('La nivel de apartament')?></div>
    <div class="title-border m-auto d-none d-lg-block"></div>

    <div class="mt-5 d-none d-lg-block">
        <div class="italic-title mb-2"><?=__('Pereti interiori si tavane')?></div>
        <div class="mb-4">
            <?=__('Separarea între apartamente este realizată cu zidărie termoizolantă  din blocuri BCA, peste care se 
            aplică materiale fonoabsorbante și tencuială mecanizată.
            Pereții interiori din apartament sunt tot din cărămidă BCA care se aplică materiale fonoabsorbante și tencuială mecanizată.
            Tavanele și ghenele de instalații sunt făcute din gips-carton, fixat cu profile din oțel galvanizat, sub care se
             instalează materiale termoizolante și fonoabsorbante.')?>
        </div>
        <div class="italic-title mb-2"><?=__('Izolatie acustica')?></div>
        <div class="mb-4">
            <?=__('Zgomotul este un sunet neplăcut care afectează calitatea vieții, iar expunerea repetată la zgomot ne poate afecta sănătatea.  
                Izolarea acustică a fost luată în  considerare încă din faza de proiectare, astfel încât să nu fie necesară corecția ulterioară a anumitor probleme.
                Pe lângă propagarea sunetului prin peretele despărțitor și golurile acestuia, sunetul se poate propaga dintr-o cameră în alta 
                prin pereți laterali, tavane și planșee, precum și goluri cum ar fi cele de ventilație sau alte goluri similare. 
                ')?>
        </div>
        <div class="italic-title mb-2"><?=__('Tamplarie exterioara cu bariera termica Schuco')?></div>
        <div class="mb-4">
            <?=__('Profile din ALUMINIU vopsite RAL 7016 MAT (seaside) cu BARIERA TERMICA seria Schüco AWS/ADS 75.SI Super Isolation,
             sistem performant de profile aluminiu cu bariera termica. '.'<br>'.'
             S-au ales deschideri oscilo-batante, geamul triplu low-E, profile cu eficiență energetică mare pentru a preveni formarea punților termice. 
              Iar în combinație cu geamul termoizolant triplu va atinge o valoare medie a coeficientului global de transfer termic de 0,80 W/m²K . '.'<br><br>'.'
              Ferestrele sunt echipate cu sistemul de feronerie cu balamale ascunse Schuco Avantec SimplySmart. Pentru a beneficia 
              la maximum de calitatea superioara a ferestrelor toate elementele de tamplarie sunt dotate cu profil izolator la baza tip BASIS,
               izolator din poliamida pentru asigurarea ruperii puntii termice in zona glafurilor (exterior/interior). 
               Deasemenea pentru asigurarea parametrilor optimi de izolare in zona lufturilor de montaj perimetrali am inclus inchideri perimetrale cu 
               membrane de difuzie vapori. '.'<br><br>'.' Geamul termoizolator este TRIPLU. Confecţionat pe linie de producţie automata are in structura sa sticla cu 
               o planeitate si o claritate deosebite.Geamul este dotat cu baghete WARM EDGE (bagheta calda). Foaia de geam exterioara si cea din
                mijloc sunt securizate. Geamul din interior este laminat de tip DUPLEX, pentru siguranta si izolare fonica superioara.')?>
        </div>
        <div class="italic-title mb-2"><?=__('Usi')?></div>
        <div class="mb-4">
            <?=__('Ușile interioare sunt celulare de culoare albă, cu pervaz. Feroneria este cromată.
            Ușile de acces în apartamente sunt metalice și au culoarea albă.')?>
        </div>
    </div>
</div>

<div class="light-bg d-none d-lg-block">
    <div class="container">
        <!--=========  Mini Features  ===========-->
        <div class="text-center italic-title pt-3"><?=__('In vecinatate...')?></div>
        <div class="row mt-2  mini-features pb-2">
            <div class="col-4 col-md-2">
                <div class="text-center pt-2 pb-2">
                    <img src="/assets/images/icon_plaja.png">
                    <div class="pt-3"><?=__('Plaja')?></div>
                </div>
            </div>
            <div class="col-4 col-md-2">
                <div class="text-center pt-2 pb-2">
                    <img src="/assets/images/icon_mall.png">
                    <div class="pt-3"><?=__('Shopping mall')?></div>
                </div>
            </div>
            <div class="col-4 col-md-2">
                <div class="text-center pt-2 pb-2">
                    <img src="/assets/images/icon_magazin.png">
                    <div class="pt-3"><?=__('magazine alimentare')?></div>
                </div>
            </div>
            <div class="col-4 col-md-2">
                <div class="text-center pt-2 pb-2">
                    <img src="/assets/images/icon_spital.png">
                    <div class="pt-3"><?=__('Spital privat')?></div>
                </div>
            </div>
            <div class="col-4 col-md-2">
                <div class="text-center pt-2 pb-2">
                    <img src="/assets/images/icon_auto.png">
                    <div class="pt-3"><?=__('Reprezentante auto')?></div>
                </div>
            </div>
            <div class="col-4 col-md-2">
                <div class="text-center pt-2 pb-2">
                    <img src="/assets/images/icon_benzinarie.png">
                    <div class="pt-3"><?=__('Benzinarii')?></div>
                </div>
            </div>
        </div>
    </div>
</div>


<div class="container d-none d-lg-block">
    <div class="text-center black-title text-uppercase mt-5 mb-2">
        <?=__('Instalatii de incalzire si racire')?>
    </div>
    <div class="title-border m-auto"></div>
    <div class="italic-title mt-4 mb-2"><?=__('Incalzire si racire')?></div>
    <div class="mb-4">
        <?=__('În vederea asigurării încălzirii și răcirii fiecare apartament este echipat cu ventiloconvectoare în fiecare cameră
         și un sistem centralizat de ventilație cu recuperare de căldură. 
        Încălzirea și răcirea încăperilor se face prin ventiloconvectoare, cu agent termic, oferind un confort suplimentar proprietarilor
         și costuri reduse pe timp de iarnă.
        Ambientul camerelor va fi controlat prin termostate programabile. Funcțiile de control vor fi asigurare prin operare manuală 
        și aplicația de casă inteligentă compatibilă cu telefoanele mobile IOS și Android. '.'<br>'.' 
        Citirea consumului de agent termic se va efectua automat de la distanță. Datele vor fi centralizate și disponibile pentru vizualizare
         în aplicația de control a casei inteligente.')?>
    </div>
    <div class="italic-title mt-4 mb-2"><?=__('Calitatea aerului')?></div>
    <div class="mb-4">
        <?=__('Acum, mai mult ca oricand, am acordat  importanță aerului pe care îl respirăm pentru sanatate.
                Locuința este locul sigur în care ne retragem și ne petrecem cea mai mare parte a timpului.
                Calitatea aerului ne influențează sănătatea. Pe termen scurt prin cum ne simțim, cum ne odihnim, prin 
                evitarea gripelor, racelilor, alergiilor iar pe termen lung prin evitarea bolilor cronice grave cauzate de poluarea cu praf,
                 particule fine ( PM10, 2,5), radon, substanțe organice volatile (VOC). Ventilarea cu filtrarea-purificarea aerului la
                  intrarea în locuință este o soluție care asigură pe termen lung sănătatea familiei tale')?>
    </div>
</div>


<div class="position-relative light-bg h200 d-none d-lg-block">
    <div class="container">
        <div class="col-md-7">
            <div class="text-center italic-title pt-5 font28">
                <?=__('Casa nepoluata va deveni in scurt timp cea mai
                ravnita solutie locativa')?>
            </div>
        </div>
    </div>
    <div class="col-md-5 ap-right-img"></div>
</div>



<div class="container d-none d-lg-block">
    <div class="italic-title mt-4 mb-2"><?=__('Necesitatea ventilatiei / aerisitii in locuinta')?></div>
    <div class="mb-4">
        <?=__('În încercarea de a minimiza pierderile de căldură și costurile cu energia s-a ajuns la o
         izolare aproape perfectă a caselor și apartamentelor, blocând orice aport de aer proaspăt și o mișcare
          naturală a acestuia. În astfel de circumstanţe, ventilaţia naturală nu oferă suficient aport de aer 
          proaspăt și eliminarea aerului viciat, încărcat de umezeală, dioxid de carbon și alți poluanți interiori.
           Petrecem peste 80% din timp în interior iar calitatea aerului are un impact major asupra sănătății noastre.
            Oboseala, durerea de cap și alte simptome neplăcute sunt primii indicatori ai impactului negativ al aerului 
            viciat asupra sănătăţii noastre, dar rareori oamenii conștientizează și asociază aceste situații cu ventilatia insuficientă.')?>
    </div>
    <div class="italic-title mt-4 mb-2"><?=__('Ventilatia cu recuperare de caldura')?></div>
    <div class="mb-4">
        <?=__('Este cea mai nouă, modernă și eficientă soluție pentru economisirea energiei într-o locuință,
         pe lângă izolarea ei. Se asigură ventilația necesară unui mediu sănătos și aduce economii de până la 25% la cheltuielile cu energia termică.
        Sistemele de ventilație cu recuperare de căldură reprezintă o modalitate de aerisire a spațiilor, prin care aerul viciat
        expulzat cedează energia, căldura să, aerului proaspăt introdus.
        “Inima” sistemului este schimbatorul de căldură, prin care aerul introdus este adus la o temperatură foarte apropiata de 
        cea a aerului eliminat, folosind energia acestuia. Avand în vedere pierderile foarte mari de energie din timpul aerisirii
         camerelor, ventilație automată cu recuperarea căldurii este cea mai utilă tehnologie pentru economisirea costurilor.
        Se recuperează energie atat iarna (căldură) cât și vară (răcoare). Totodată funcționează automat și ne scapă de grija aerisirii locuinței.
        Un concept de izolație termică modernă este performant numai asistat de un concept de aerisire cu recuperarea căldurii.
         Izolație scumpă, ferestre noi, etanșe – pe care apoi trebuie să le deschizi ca sa aerisesti? Acest lucru este ilogic si contraproductiv!')?>
    </div>

    <div class="text-center black-title text-uppercase mb-2">
        <?=__('Finiaje si nivele de echipare')?>
    </div>
    <div class="title-border m-auto"></div>
    <div class="italic-title mt-4 mb-1"><?=__('Toate apartamentele din complexul Mobipark sunt gata finisate.')?></div>
    <div class="mb-4">
        <?=__('Acestea sunt disponibile in doua variante de  dotari, pentru a veni în sprijinul familiilor ocupate,
         care își doresc o casa “la cheie” in care se pot muta imediat. '.'<br>'.'
        Pachetul '.'<b>'.'CONFORT'.'</b>'.' va include pardoseli din parchet laminat sau gresie porțelanată de calitate superioară de la producători
         italieni, pereții finisați vopsiti, aparataj electric, obiecte sanitare din porțelan de culoare albă, dotate cu robineti 
         cromați, de la furnizori renumiți, usi de interior și exterior.'.'<br><br>'.'
        Pachetul '.'<b>'.'PREMIUM'.'</b>'.' va include mobilier pentru fiecare camera, electrocasnice bucatarie, corpuri de iluminat, pardoseli 
        din parchet laminat sau gresie porțelanată de calitate superioară de la producători italieni, pereți finisați vopsiti și
         obiecte sanitare din porțelan de culoare albă, dotate cu robineti cromați, de la furnizori renumiți, usi de interior si exterior.
        Toate finisajele vor fi puse in manopera conform variantei de design alese.')?>
    </div>



    <div class="text-center black-title text-uppercase mt-5 mb-2">
        <?=__('Design')?>
    </div>
    <div class="title-border m-auto"></div>
    <div class="mt-4 mb-5">
        <?=__('Alături de arhitectul și designerul Cora Cristea am pornit de la ideea de a 
        dărui soluții corecte de amenajare a spațiului interior, pentru apartamentele din acest ansamblu rezidențial.')?><br><br>
        <?=__('Casa ta ar trebui sa te reprezinte indiferent de stilul și bugetul pe care-l ai.
         Fie ca îți place stilul clasic, industrial, scandinav sau Boho Chic noi avem soluția. 
         Într-o lume ideală cu toții ne-am dori sa transpunem imaginile pe care le vedem în revistele de amenajări
          interioare sau pe Pinterest în propriile case. Din păcate suntem limitați de nenumărate aspecte ce țin 
          de utilitatea pieselor de mobilier, dimensiunile camerelor,buget și timp. Doar pentru a găsi produsele ideale se
           pierd zile întregi de cercetare a magazinelor fizice și/sau online. La Mobipark <b>design-ul ți-l oferim cadou.</b>')?><br><br>
        <?=__('Ca parte din design vei avea:')?><br><br>
        <ul>
            <li><?=__('O casa amenajata în stilul tau și al familiei tale')?></li>
            <li><?=__('Exploatare maximă a fiecărei părți a spațiului interior')?></li>
            <li><?=__('Grija pentru cheltuirea optimă a bugetului stabilit')?></li>
            <li><?=__('Nu mai pierzi timp prin magazine')?></li>
            <li><?=__('Soluții potrivite pentru a-ți crea locuință ideală')?></li>
        </ul>
    </div>

</div>


<div class="position-relative light-bg h200 d-none d-lg-block">
    <div class="col-md-5 ap-left-img"></div>
    <div class="container">
        <div class="row">
            <div class="col-md-5"></div>
            <div class="col-md-7 pt-4">
                <div class="text-center italic-title pt-5 font28">
                    <?=__('Design-ul ti-l oferim cadou!')?>
                </div>
            </div>
        </div>
    </div>
</div>



<div class="container d-none d-lg-block">
    <div class="text-center black-title text-uppercase mt-4 pb-2">
        <?=__('Casa inteligenta')?></div>
    <div class="title-border m-auto"></div>
    <div class="italic-title mt-4 mb-1"><?=__('Eficienta')?></div>
    <div class="mb-4">
        <?=__('Principalul beneficiu oferit de un sistem inteligent pentru apartamentul
         tău este acela de a genera economii însemnate folosind într-un mod eficient
          toate sistemele clădirii. Gestionarea optimă a resurselor poate genera economii însemnate,
           ajungând până la 25-30% din costurile lunare necesare întreținerii, oricare ar fi destinația
            sau mărimea acesteia.')?>
    </div>
    <div class="italic-title mt-4 mb-1"><?=__('Mobilitate')?></div>
    <div class="mb-4">
        <?=__('Tehnologia mobilă face deja parte din rutina zilnică și clar ne face viața mai ușoară.
         Plecând de la aceste premise, vă oferim capacitatea de a putea folosi orice tabletă sau smartphone 
         pentru a vă putea controla și monitoriza apartamentul, de acasă sau de oriunde din lume, fie pentru
          a obține confortul optim fie pentru a știi în orice moment consumul și starea casei tale. Ai totul
           la îndemână într-un mod foarte sigur și simplu de folosit.')?>
    </div>
    <div class="italic-title mt-4 mb-1"><?=__('Functiile casei inteligente')?></div>
    <div class="mb-4">
        <?=__('In aplicatia de control a casei inteligente ai functii pentru:')?><br>
        <strong><?=__('Siguranta si securitate')?></strong><br>
        <?=__('Controlul accesului în bloc și a barierelor de acces în complex.')?><br>
        <strong><?=__('Confort')?></strong><br>
        <?=__('Controlul climatizării în fiecare cameră, monitorizarea temperaturii și a umiditatii.')?><br>
        <strong><?=__('Managementul energiei')?></strong><br>
        <?=__('Folosind contoare inteligente cu citire de la distanță am reușit  sa monitorizăm 
        și sa centralizăm consumul de agent termic, energie și apă. Toate aceste date sunt disponibile
         în aplicația casei inteligente și raportate automat către sistemul de management al clădirii.')?><br>
        <strong><?=__('Functii avansate')?></strong><br>
        <?=__('Folosind module opționale sunt disponibile funcții pentru adăugarea 
        unor funcții suplimentare casei inteligente: controlul scenelor de lumini, controlul draperiilor,
         iluminat inteligent, senzori de monitorizarea aerului, integrare cu asistenți vocali Google
          Home și Alexa, senzori de fum și apă, senzori de prezență în apartament, notificări prin sms și email')?><br>

    </div>



    <div class="text-center black-title text-uppercase mt-4 pb-2">
        <?=__('Spatiu verde - loc de joaca')?></div>
    <div class="title-border m-auto"></div>
    <div class="italic-title mt-4 mb-1"><?=__('2800 mp de spațiu verde')?></div>
    <div class="mb-5">
        <?=__('In centrul complexului, la care am adaugat 1000 mp cu dale înierbate
         vor face din Mobipark un loc în care copiii vor putea sa alerge în voie și să se 
         joace într-un spațiu amenajat cu bun gust și jucarii rezistente în timp. Rezidentii
          se vor putea plimba și își vor putea savura liniștiți cafeaua pe băncile acestui parc, 
          avaând ocazia de a se împrieteni cu vecinii. Spațiul verde va fi îngrijit periodic de o
           echipa de gradinari și peisagiști experimentați, activitate ce va rămâne în administrarea
            permanentă a dezvoltatorului, pentru onorarea promisiunii și conservarea pe termen lung a 
            cadrului natural. Aici vom planta copaci ce vor umbri aleile, vor conferi intimitate și vor
             purifica aerul din perimetru. Însămânțarea gazonului, arbuștii și florile alese vor fi adaptate
              climatului dobrogean, în concordanță cu planul de amenajare peisagistică. 
        Ne dorim ca acasă la Mobipark sa fie ca o vacanță continuă în sânul naturii!')?>
    </div>
</div>


<!------------------------------ ACORDION MOBIE INFO -------------------------->

<div class="container  pb-2 d-block d-lg-none">
    <div class="accordion apartment-acordion" id="accordionSensor">
        <div class="card">
            <h3 class="card-header" id="headingOne">
                <button type="button" class="btn btn-link" data-toggle="collapse" data-target="#collapseOne">
                    <?=__('La nivel de apartament')?>
                    <i class="fas fa-chevron-down float-right mobi-green"></i>
                </button>
            </h3>
            <div id="collapseOne" class="collapse" data-parent="#accordionSensor">
                <div class="card-body">
                <div class="mt-1">
                    <div class="italic-title mb-2"><?=__('Pereti interiori si tavane')?></div>
                    <div class="mb-4 content-preview">
                        <?=__('Separarea între apartamente este realizată cu zidărie termoizolantă  din blocuri BCA, peste care se 
            aplică materiale fonoabsorbante și tencuială mecanizată.
            Pereții interiori din apartament sunt tot din cărămidă BCA care se aplică materiale fonoabsorbante și tencuială mecanizată.
            Tavanele și ghenele de instalații sunt făcute din gips-carton, fixat cu profile din oțel galvanizat, sub care se
             instalează materiale termoizolante și fonoabsorbante.')?>
                        <span class="see-more"><?=__('Citeste mai mult')?></span>
                    </div>
                    <div class="italic-title mb-2"><?=__('Izolatie acustica')?></div>
                    <div class="mb-4 content-preview">
                        <?=__('Zgomotul este un sunet neplăcut care afectează calitatea vieții, iar expunerea repetată la zgomot ne poate afecta sănătatea.  
                Izolarea acustică a fost luată în  considerare încă din faza de proiectare, astfel încât să nu fie necesară corecția ulterioară a anumitor probleme.
                Pe lângă propagarea sunetului prin peretele despărțitor și golurile acestuia, sunetul se poate propaga dintr-o cameră în alta 
                prin pereți laterali, tavane și planșee, precum și goluri cum ar fi cele de ventilație sau alte goluri similare. 
                ')?>
                        <span class="see-more"><?=__('Citeste mai mult')?></span>
                    </div>
                    <div class="italic-title mb-2"><?=__('Tamplarie exterioara cu bariera termica Schuco')?></div>
                    <div class="mb-4 content-preview">
                        <?=__('Profile din ALUMINIU vopsite RAL 7016 MAT (seaside) cu BARIERA TERMICA seria Schüco AWS/ADS 75.SI Super Isolation,
             sistem performant de profile aluminiu cu bariera termica. '.'<br>'.'
             S-au ales deschideri oscilo-batante, geamul triplu low-E, profile cu eficiență energetică mare pentru a preveni formarea punților termice. 
              Iar în combinație cu geamul termoizolant triplu va atinge o valoare medie a coeficientului global de transfer termic de 0,80 W/m²K . '.'<br><br>'.'
              Ferestrele sunt echipate cu sistemul de feronerie cu balamale ascunse Schuco Avantec SimplySmart. Pentru a beneficia 
              la maximum de calitatea superioara a ferestrelor toate elementele de tamplarie sunt dotate cu profil izolator la baza tip BASIS,
               izolator din poliamida pentru asigurarea ruperii puntii termice in zona glafurilor (exterior/interior). 
               Deasemenea pentru asigurarea parametrilor optimi de izolare in zona lufturilor de montaj perimetrali am inclus inchideri perimetrale cu 
               membrane de difuzie vapori. '.'<br><br>'.' Geamul termoizolator este TRIPLU. Confecţionat pe linie de producţie automata are in structura sa sticla cu 
               o planeitate si o claritate deosebite.Geamul este dotat cu baghete WARM EDGE (bagheta calda). Foaia de geam exterioara si cea din
                mijloc sunt securizate. Geamul din interior este laminat de tip DUPLEX, pentru siguranta si izolare fonica superioara.')?>
                        <span class="see-more"><?=__('Citeste mai mult')?></span>
                    </div>
                    <div class="italic-title mb-2"><?=__('Usi')?></div>
                    <div class="mb-4">
                        <?=__('Ușile interioare sunt celulare de culoare albă, cu pervaz. Feroneria este cromată.
            Ușile de acces în apartamente sunt metalice și au culoarea albă.')?>
                    </div>
                </div>
            </div>
            </div>
        </div>

        <div class="card">
            <h3 class="card-header" id="headingTwo">
                <button type="button" class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseTwo">
                    <?=__('Instalatii de incalzire si racire')?>
                    <i class="fas fa-chevron-down float-right mobi-green"></i>
                </button>
            </h3>
            <div id="collapseTwo" class="collapse"  data-parent="#accordionSensor">
                <div class="card-bodyt">
                <div class="italic-title mt-1 mb-2"><?=__('Incalzire si racire')?></div>
                <div class="mb-4 content-preview">
                    <?=__('În vederea asigurării încălzirii și răcirii fiecare apartament este echipat cu ventiloconvectoare în fiecare cameră
         și un sistem centralizat de ventilație cu recuperare de căldură. 
        Încălzirea și răcirea încăperilor se face prin ventiloconvectoare, cu agent termic, oferind un confort suplimentar proprietarilor
         și costuri reduse pe timp de iarnă.
        Ambientul camerelor va fi controlat prin termostate programabile. Funcțiile de control vor fi asigurare prin operare manuală 
        și aplicația de casă inteligentă compatibilă cu telefoanele mobile IOS și Android. '.'<br>'.' 
        Citirea consumului de agent termic se va efectua automat de la distanță. Datele vor fi centralizate și disponibile pentru vizualizare
         în aplicația de control a casei inteligente.')?>
                    <span class="see-more"><?=__('Citeste mai mult')?></span>
                </div>
                <div class="italic-title mt-4 mb-2"><?=__('Calitatea aerului')?></div>
                <div class="mb-4 content-preview">
                    <?=__('Acum, mai mult ca oricand, am acordat  importanță aerului pe care îl respirăm pentru sanatate.
                Locuința este locul sigur în care ne retragem și ne petrecem cea mai mare parte a timpului.
                Calitatea aerului ne influențează sănătatea. Pe termen scurt prin cum ne simțim, cum ne odihnim, prin 
                evitarea gripelor, racelilor, alergiilor iar pe termen lung prin evitarea bolilor cronice grave cauzate de poluarea cu praf,
                 particule fine ( PM10, 2,5), radon, substanțe organice volatile (VOC). Ventilarea cu filtrarea-purificarea aerului la
                  intrarea în locuință este o soluție care asigură pe termen lung sănătatea familiei tale')?>
                    <span class="see-more"><?=__('Citeste mai mult')?></span>
                </div>
                <div class="position-relative light-bg h200">
                    <div class="container">
                        <div class="col-md-7">
                            <div class="text-center italic-title pt-5 font28">
                                <?=__('Casa nepoluata va deveni in scurt timp cea mai
                ravnita solutie locativa')?>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-5 ap-right-img"></div>
                </div>
                <div class="italic-title mt-4 mb-2"><?=__('Necesitatea ventilatiei / aerisitii in locuinta')?></div>
                <div class="mb-4 content-preview">
                    <?=__('În încercarea de a minimiza pierderile de căldură și costurile cu energia s-a ajuns la o
         izolare aproape perfectă a caselor și apartamentelor, blocând orice aport de aer proaspăt și o mișcare
          naturală a acestuia. În astfel de circumstanţe, ventilaţia naturală nu oferă suficient aport de aer 
          proaspăt și eliminarea aerului viciat, încărcat de umezeală, dioxid de carbon și alți poluanți interiori.
           Petrecem peste 80% din timp în interior iar calitatea aerului are un impact major asupra sănătății noastre.
            Oboseala, durerea de cap și alte simptome neplăcute sunt primii indicatori ai impactului negativ al aerului 
            viciat asupra sănătăţii noastre, dar rareori oamenii conștientizează și asociază aceste situații cu ventilatia insuficientă.')?>
                    <span class="see-more"><?=__('Citeste mai mult')?></span>
                </div>
                <div class="italic-title mt-4 mb-2"><?=__('Ventilatia cu recuperare de caldura')?></div>
                <div class="mb-4 content-preview">
                    <?=__('Este cea mai nouă, modernă și eficientă soluție pentru economisirea energiei într-o locuință,
         pe lângă izolarea ei. Se asigură ventilația necesară unui mediu sănătos și aduce economii de până la 25% la cheltuielile cu energia termică.
        Sistemele de ventilație cu recuperare de căldură reprezintă o modalitate de aerisire a spațiilor, prin care aerul viciat
        expulzat cedează energia, căldura să, aerului proaspăt introdus.
        “Inima” sistemului este schimbatorul de căldură, prin care aerul introdus este adus la o temperatură foarte apropiata de 
        cea a aerului eliminat, folosind energia acestuia. Avand în vedere pierderile foarte mari de energie din timpul aerisirii
         camerelor, ventilație automată cu recuperarea căldurii este cea mai utilă tehnologie pentru economisirea costurilor.
        Se recuperează energie atat iarna (căldură) cât și vară (răcoare). Totodată funcționează automat și ne scapă de grija aerisirii locuinței.
        Un concept de izolație termică modernă este performant numai asistat de un concept de aerisire cu recuperarea căldurii.
         Izolație scumpă, ferestre noi, etanșe – pe care apoi trebuie să le deschizi ca sa aerisesti? Acest lucru este ilogic si contraproductiv!')?>
                    <span class="see-more"><?=__('Citeste mai mult')?></span>
                </div>
            </div>
            </div>
        </div>

        <div class="card">
            <h3 class="card-header" id="headingThree">
                <button type="button" class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseThree">
                    <?=__('Finisaje si nivele de echipare')?>
                    <i class="fas fa-chevron-down float-right mobi-green"></i>
                </button>
            </h3>

            <div id="collapseThree" class="collapse" data-parent="#accordionSensor">
                <div class="card-bodyt">
                <div class="italic-title mt-1 mb-1"><?=__('Toate apartamentele din complexul Mobipark sunt gata finisate.')?></div>
                <div class="mb-4">
                    <?=__('Acestea sunt disponibile in doua variante de  dotari, pentru a veni în sprijinul familiilor ocupate,
         care își doresc o casa “la cheie” in care se pot muta imediat. '.'<br>'.'
        Pachetul '.'<b>'.'CONFORT'.'</b>'.' va include pardoseli din parchet laminat sau gresie porțelanată de calitate superioară de la producători
         italieni, pereții finisați vopsiti, aparataj electric, obiecte sanitare din porțelan de culoare albă, dotate cu robineti 
         cromați, de la furnizori renumiți, usi de interior și exterior.'.'<br><br>'.'
        Pachetul '.'<b>'.'PREMIUM'.'</b>'.' va include mobilier pentru fiecare camera, electrocasnice bucatarie, corpuri de iluminat, pardoseli 
        din parchet laminat sau gresie porțelanată de calitate superioară de la producători italieni, pereți finisați vopsiti și
         obiecte sanitare din porțelan de culoare albă, dotate cu robineti cromați, de la furnizori renumiți, usi de interior si exterior.
        Toate finisajele vor fi puse in manopera conform variantei de design alese.')?>
                </div>
            </div>
            </div>
        </div>

        <div class="card">
            <h3 class="card-header" id="headingFour">
                <button type="button" class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseFour">
                    <?=__('Design')?>
                    <i class="fas fa-chevron-down float-right mobi-green"></i>
                </button>
            </h3>
            <div id="collapseFour" class="collapse" data-parent="#accordionSensor">
                <div class="card-body">
                <div class="mt-1 mb-5">
                    <?=__('Alături de arhitectul și designerul Cora Cristea am pornit de la ideea de a 
        dărui soluții corecte de amenajare a spațiului interior, pentru apartamentele din acest ansamblu rezidențial.')?><br><br>
                    <?=__('Casa ta ar trebui sa te reprezinte indiferent de stilul și bugetul pe care-l ai.
         Fie ca îți place stilul clasic, industrial, scandinav sau Boho Chic noi avem soluția. 
         Într-o lume ideală cu toții ne-am dori sa transpunem imaginile pe care le vedem în revistele de amenajări
          interioare sau pe Pinterest în propriile case. Din păcate suntem limitați de nenumărate aspecte ce țin 
          de utilitatea pieselor de mobilier, dimensiunile camerelor,buget și timp. Doar pentru a găsi produsele ideale se
           pierd zile întregi de cercetare a magazinelor fizice și/sau online. La Mobipark <b>design-ul ți-l oferim cadou.</b>')?><br><br>
                    <?=__('Ca parte din design vei avea:')?><br><br>
                    <ul>
                        <li><?=__('O casa amenajata în stilul tau și al familiei tale')?></li>
                        <li><?=__('Exploatare maximă a fiecărei părți a spațiului interior')?></li>
                        <li><?=__('Grija pentru cheltuirea optimă a bugetului stabilit')?></li>
                        <li><?=__('Nu mai pierzi timp prin magazine')?></li>
                        <li><?=__('Soluții potrivite pentru a-ți crea locuință ideală')?></li>
                    </ul>
                </div>
            </div>
            </div>
        </div>

        <div class="card">
            <h3 class="card-header" id="headingFive">
                <button type="button" class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseFive">
                    <?=__('Casa inteligenta')?>
                    <i class="fas fa-chevron-down float-right mobi-green"></i>
                </button>
            </h3>
            <div id="collapseFive" class="collapse" data-parent="#accordionSensor">
                <div class="card-body">
                <div class="italic-title mt-1 mb-1"><?=__('Eficienta')?></div>
                <div class="mb-4 content-preview">
                    <?=__('Principalul beneficiu oferit de un sistem inteligent pentru apartamentul
         tău este acela de a genera economii însemnate folosind într-un mod eficient
          toate sistemele clădirii. Gestionarea optimă a resurselor poate genera economii însemnate,
           ajungând până la 25-30% din costurile lunare necesare întreținerii, oricare ar fi destinația
            sau mărimea acesteia.')?>
                    <span class="see-more"><?=__('Citeste mai mult')?></span>
                </div>
                <div class="italic-title mt-4 mb-1"><?=__('Mobilitate')?></div>
                <div class="mb-4 content-preview">
                    <?=__('Tehnologia mobilă face deja parte din rutina zilnică și clar ne face viața mai ușoară.
         Plecând de la aceste premise, vă oferim capacitatea de a putea folosi orice tabletă sau smartphone 
         pentru a vă putea controla și monitoriza apartamentul, de acasă sau de oriunde din lume, fie pentru
          a obține confortul optim fie pentru a știi în orice moment consumul și starea casei tale. Ai totul
           la îndemână într-un mod foarte sigur și simplu de folosit.')?>
                    <span class="see-more"><?=__('Citeste mai mult')?></span>
                </div>
                <div class="italic-title mt-4 mb-1"><?=__('Functiile casei inteligente')?></div>
                <div class="mb-4 content-preview">
                    <?=__('In aplicatia de control a casei inteligente ai functii pentru:')?><br>
                    <strong><?=__('Siguranta si securitate')?></strong><br>
                    <?=__('Controlul accesului în bloc și a barierelor de acces în complex.')?><br>
                    <strong><?=__('Confort')?></strong><br>
                    <?=__('Controlul climatizării în fiecare cameră, monitorizarea temperaturii și a umiditatii.')?><br>
                    <strong><?=__('Managementul energiei')?></strong><br>
                    <?=__('Folosind contoare inteligente cu citire de la distanță am reușit  sa monitorizăm 
        și sa centralizăm consumul de agent termic, energie și apă. Toate aceste date sunt disponibile
         în aplicația casei inteligente și raportate automat către sistemul de management al clădirii.')?><br>
                    <strong><?=__('Functii avansate')?></strong><br>
                    <?=__('Folosind module opționale sunt disponibile funcții pentru adăugarea 
        unor funcții suplimentare casei inteligente: controlul scenelor de lumini, controlul draperiilor,
         iluminat inteligent, senzori de monitorizarea aerului, integrare cu asistenți vocali Google
          Home și Alexa, senzori de fum și apă, senzori de prezență în apartament, notificări prin sms și email')?>
                    <span class="see-more"><?=__('Citeste mai mult')?></span>
                </div>
            </div>
            </div>
        </div>

        <div class="card">
            <h3 class="card-header" id="headingSix">
                <button type="button" class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseSix">
                    <?=__('Spatiu verde - loc de joaca')?>
                    <i class="fas fa-chevron-down float-right mobi-green"></i>
                </button>
            </h3>
            <div id="collapseSix" class="collapse" data-parent="#accordionSensor">
                <div class="card-body">
                <div class="italic-title mt-1 mb-1"><?=__('2800 mp de spațiu verde')?></div>
                <div class="mb-4 content-preview">
                    <?=__('In centrul complexului, la care am adaugat 1000 mp cu dale înierbate
         vor face din Mobipark un loc în care copiii vor putea sa alerge în voie și să se 
         joace într-un spațiu amenajat cu bun gust și jucarii rezistente în timp. Rezidentii
          se vor putea plimba și își vor putea savura liniștiți cafeaua pe băncile acestui parc, 
          avaând ocazia de a se împrieteni cu vecinii. Spațiul verde va fi îngrijit periodic de o
           echipa de gradinari și peisagiști experimentați, activitate ce va rămâne în administrarea
            permanentă a dezvoltatorului, pentru onorarea promisiunii și conservarea pe termen lung a 
            cadrului natural. Aici vom planta copaci ce vor umbri aleile, vor conferi intimitate și vor
             purifica aerul din perimetru. Însămânțarea gazonului, arbuștii și florile alese vor fi adaptate
              climatului dobrogean, în concordanță cu planul de amenajare peisagistică. 
        Ne dorim ca acasă la Mobipark sa fie ca o vacanță continuă în sânul naturii!')?>
                    <span class="see-more"><?=__('Citeste mai mult')?></span>
                </div>
            </div>
            </div>
        </div>

    </div>
</div>


