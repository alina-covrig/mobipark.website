<div class="row">
    <div class="col-md-12">
        <div class="alert alert5 alertbottom alert-danger">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true"><i class="fa fa-times"></i></button>
            <?php echo $message?>
        </div>
    </div>
</div>