<?=$this->start('extra-scripts')?>
    <script type="text/javascript">
        Swal.fire({
            icon: 'error',
            title: 'Eroare',
            html: '<?= $message ?>'
        });
    </script>
<?=$this->end()?>