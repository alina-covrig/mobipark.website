
<div class="modal fade" id="small-studio-details" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-body">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <div class="packages">
                    <div class="text-center italic-title pb-1"><?=__('Preturi')?></div>
                    <div class="text-center black-title pb-2"><?=__('Tip studio 02')?></div><div class=""></div>
                    <div class="m-auto title-border"></div>


                    <div class="d-flex mt-4 pb-2">
                        <div class="confort col-md-6">
                            <div class="confort-button text-center text-uppercase p-3"><?=__('Confort')?></div>
                            <div class="details p-2 mb-5 h200">
                                <strong class="text-uppercase"><?=__('Pachet Basic')?></strong>  <br/>
                                +<br>
                                <span class="font-12 gray-text"><?=__('Pereti complet finisati'.'<br>'.'
                                            Amenajare completa cu gresie/parcet*'.'<br>'.'
                                            Usi interior*'.'
                                            Aparataj electric (prize si intrerupatoare')?>
                                </span>
                            </div>
                            <div class="confort-button text-center p-3">
                                <strong>85 900 Euro</strong>
                            </div>
                        </div>
                        <div class="premium col-md-6">
                            <div class="premium-button text-center text-uppercase p-3"><?=__('Premium')?></div>
                            <div class="details p-2 mb-5 h200">
                                <strong class="text-uppercase"><?=__('Pachet Confort')?></strong>  <br/>
                                +<br>
                                <span class="font-12 gray-text"><?=__('Mobila*'.'<br>'.'
                                            Corpuri de iluminat'.'<br>'.'
                                            Electrocasnice hota, plita, cuptor, firgider, masina de spalat rufe)')?>
                                    </span><br>
                            </div>
                            <div class="premium-button text-center p-3">
                                <strong>92 900 Euro</strong>
                            </div>
                        </div>
                    </div>

                    <div class="font-12 gray-text"><?=__('De provenienta Italia*')?></div>
                    <div class="font-12 gray-text"><?=__('**Toate preturile contin TVA')?></div>
                </div>
                <div class="light-bg mt-4">
                    <div class="text-center italic-title pt-3"><?=__('Sau')?></div>
                    <div class="black-title text-center text-uppercase pb-3"><?=__('Contacteaza-ne')?></div>

                    <form class="contact-prices px-3" method="post" class="p-3" action="/contact/sendContact" role="form" data-toggle="validator" novalidate="true">
                        <div class="form-row pb-md-3">
                            <div class="form-group mb-md-0 has-error has-danger col-md-4">
                                <input type="text" class="form-control"  required="" name="name" placeholder="nume" data-error="* Camp obligatoriu">
                                <div class="help-block with-errors"></div>
                            </div>
                            <div class="form-group mb-md-0 col-md-4">
                                <input type="email" class="form-control"  required="" name="email" placeholder="email" data-error="* Adresa de email invalida">
                                <div class="help-block with-errors"></div>
                            </div>
                            <div class="form-group mb-md-0 col-md-4">
                                <input type="phone" class="form-control"  required="" name="phone" placeholder="telefon" data-error="* Numar de telefon incorect">
                                <div class="help-block with-errors"></div>
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="form-group mb-md-0 col-md-10">
                                <input class="form-control"  required="" name="subject" placeholder="mesaj" data-error="* Camp obligatoriu"></input>
                                <div class="help-block with-errors"></div>
                            </div>
                            <button type="submit" class="btn col-md-2 green-button white">Trimite</button>
                        </div>
                        <div class="form-check">
                            <input type="checkbox" class="form-check-input" id="exampleCheck1" required="">
                            <small><label class="form-check-label gray-text" for="exampleCheck1"><?=__('*Am citit si sunt de acord cu')?>
                                    <a href="#" class="blue"><?=__('Termenii, conditiile'.'</a>'.' si')?>
                                        <a href="#" class="blue"><?=__('Politica de confidentialitate')?></a></label></small>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>