<ul class="nav nav-pills nav-fill user-menu">
    <li class="nav-item">
        <a class="nav-link <?= ($this->request->params['action'] == 'myAccount') ? 'active' : '' ?>"
           href="<?= Router::url(['controller' => 'users', 'action' => 'myAccount']) ?>"
        ><?= __('Date cont') ?></a>
    </li>
    <li class="nav-item mx-0 ml-0 ml-sm-2">
        <a class="nav-link <?= (in_array($this->request->params['action'], ['myCompanies', 'editCompany'])) ? 'active' : '' ?>"
           href="<?= Router::url(['controller' => 'users', 'action' => 'myCompanies']) ?>"
        ><?= __('Companiile mele') ?></a>
    </li>
    <li class="nav-item mx-0 ml-0 ml-sm-2">
        <a class="nav-link <?= (in_array($this->request->params['action'], ['myPayments', 'editPayment'])) ? 'active' : '' ?>"
           href="<?= Router::url(['controller' => 'users', 'action' => 'myPayments']) ?>"
        ><?= __('Date de facturare') ?></a>
    </li>
    <li class="nav-item ml-0 ml-sm-2">
        <a class="nav-link <?= ($this->request->params['action'] == 'myNotices') ? 'active' : '' ?>"
           href="<?= Router::url(['controller' => 'users', 'action' => 'myNotices']) ?>"
        ><?= __('Cereri') ?></a>
    </li>
</ul>