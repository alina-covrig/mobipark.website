<?php if ($this->Paginator->params()['pageCount'] > 1) : ?>
<div class="row">
    <div class="col-lg-12">
        <div class="pagination-small">
            <ul class="pagination mb-5">
                <?php
                echo $this->Paginator->prev(__('Inapoi'), array('tag' => 'li'), null, array('tag' => 'li','class' => 'page-item disabled', 'disabledTag' => 'a'));
                echo $this->Paginator->numbers(array('separator' => '','currentTag' => 'a', 'currentClass' => 'active','tag' => 'li', 'class' => 'page-item', 'first' => 1));
                echo $this->Paginator->next(__('Inainte'), array('tag' => 'li', 'currentClass' => 'page-item disabled'), null, array('tag' => 'li','class' => 'page-item disabled', 'disabledTag' => 'a'));
                ?>
            </ul>
        </div>
    </div>
</div>
<?php endif; ?>