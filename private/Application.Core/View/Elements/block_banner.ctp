<div class="block-banner hero">
    <div id="medium-banner" class="block1 carousel slide" data-ride="carousel">

        <ol class="carousel-indicators">
            <li data-target="#medium-banner" data-slide-to="0" class="active"></li>
            <li data-target="#medium-banner" data-slide-to="1"></li>
            <li data-target="#medium-banner" data-slide-to="2"></li>
        </ol>

        <div class="carousel-inner">
            <div class="carousel-item">
                <img src="/img/gallery/94.jpg">
                <div class="banner-layer"></div>
            </div>
            <div class="carousel-item">
                <img src="/img/gallery/95.jpg">
                <div class="banner-layer"></div>
            </div>
            <div class="carousel-item">
                <img src="/img/gallery/97.jpg">
                <div class="banner-layer"></div>
            </div>
        </div>
    </div>

    <div class="banner-details position-absolute">
        <div class="huge-title">
            MOBIPARK<br>
            01
        </div>
        <div class="big-title mb-3">2020</div>
        <a href="#" class="green-button"><?=__('Vezi disponibilitatea apartamentelor')?></a>
    </div>
</div>