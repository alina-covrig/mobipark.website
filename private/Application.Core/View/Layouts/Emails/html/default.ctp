<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01//EN">
<html>
<head>
    <title><?php echo $title_for_layout; ?></title>
</head>
<body>
<table cellspacing="0" cellpadding="0" width="100%" align="center" style="font-family:Arial; color:#626262; font-size:11px">
    <tr>
        <td align="center">
            <table cellspacing="0" cellpadding="0" width="100%" bgcolor="#f4f4f4" style="color:#626262;">
                <tr>
                    <td align="center">
                        <table cellspacing="0" cellpadding="0" width="660" style="color:#626262; margin:0 20px; font-size:12px">
                            <tr>
                                <td style="height: 110px; text-align: center; padding: 10px;">
                                    <img src="<?php echo $baseUrl; ?>assets/images/logo_confort_urban@2x.png" height="90" />
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
            <?php echo $this->fetch('content'); ?>
        </td>
    </tr>
</table>
</body>
</html>