<!doctype html>
<html lang="ro">
<head>
    <meta charset="UTF-8">
</head>
<body>
<div style="width: 700px;margin: 10px auto;">
    <hr/>
    <div style="margin: 20px 0px;">
        <?=$this->fetch('content');?>
    </div>
    <div style="text-align: center">
        <small style="font-size: 11px; color: #817a79;">Acest email este trimis de <?=Configure::read('WEBSITE_NAME')?></small>
    </div>
</div>
</body>
</html>