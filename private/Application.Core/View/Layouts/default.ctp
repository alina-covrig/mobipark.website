<!DOCTYPE HTML>
<html lang="ro">
<head>
    <meta charset="utf-8">
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no">
    <?= $this->fetch('meta_data'); ?>
    <meta name="application-name" content="<?= Configure::read('WEBSITE_NAME') ?>">
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700,800" rel="stylesheet" type="text/css">

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.13.1/css/all.min.css">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.13.1/css/all.min.css">
    <?php echo $this->fetch('css')?>
    <link rel="stylesheet" href="/assets/css/jquery.fancybox.min.css">
    <link href="/assets/css/styles.css" rel="stylesheet" type="text/css">
    <link href="/assets/css/responsive.css" rel="stylesheet" type="text/css">
    <link href="/assets/css/owl.carousel.css" rel="stylesheet" type="text/css">
    <link href="/assets/css/owl.theme.default.min.css" rel="stylesheet" type="text/css">
    <link rel="stylesheet" href="/assets/css/sweetalert2.min.css">

    <link href="/assets/css/mmenu.css" rel="stylesheet" />
    <script src="/assets/js/mmenu.js"></script>

    <!-- Fire the plugin -->
    <script>

            document.addEventListener(
                "DOMContentLoaded", () => {
                    new Mmenu("#my-menu", {
                        "extensions": [
                            "pagedim-black",
                            "theme-dark"
                        ],
                        "navbars": [
                            {
                                "position": "bottom",
                                "content": [
                                    "<a href='#/'><img src='/assets/images/icon_facebook_white@2x.png'></a>",
                                    "<a href='#/'><img src='/assets/images/icon_instagram_white@2x.png'></a>",
                                    "<a href='#/'><img src='/assets/images/icon_youtube_white@2x.png'></a>"
                                ]
                            },
                            {
                                "position": "bottom",
                                "content" : "<div class='text-center' style='min-height:20px;'><?=__('Copyright @Mobipark 2020')?></div>",
                                "style" : "min-height: 20px"
                            },
                            {
                                "position": "bottom",
                                "content" : "<div class='text-center'><?=__('Toate drepturile rezervate')?></div>",
                            }

                        ],

                    });
                }
            );

    </script>

    <script>
        dataLayer = window.dataLayer || [];
        let cookiesPage = "/politica-de-utilizare-a-cookie-urilor";
    </script>
</head>
<body>
<div id="app">

    <div class="flash-container">
        <?=$this->Session->flash();?>
    </div>

  <div class="top-header d-none d-xl-block">
      <div class="container">
          <ul>
              <li>
                  <?=__('Showroom: ')?>
                  <span><?=__('Str. Haga nr.6 LUNI-VINERI intre 09:00 18:00')?></span>
              </li>
              <li>
                  <?=__('Scrie-ne la: ')?>
                  <span><?=__('office@mobipark.ro')?></span>
              </li>
              <li class="ml-5">
                  <?=__('Suna-ne la :')?>
                  <span><?=__('0799 777 004')?></span>
              </li>

              <li class="pr-0 float-right h37">
                  <a href="#" class="mr-3"><div class="d-inline-block youtube"></div></a>
                  <a href="#" class="mr-3"><div class="d-inline-block instagram"></div></a>
                  <a href="#"><div class="d-inline-block facebook"></div></a>
              </li>
          </ul>
      </div>
  </div>


    <!------ meniu mobil -------------->
    <div class="menu-icon d-block d-lg-none">
        <a href="#my-menu">
            <img src="/assets/images/icon_menu_white.png">
        </a>
    </div>

    <!----- logo mobil ----->
    <div class="mobile-logo d-block d-lg-none">
        <a href="/">
            <img src="/assets/images/logo_mobipark@2x.png">
        </a>
    </div>

    <!-- The menu mobil-->
    <nav id="my-menu" class="d-lg-none">
        <ul>
            <li><a href="<?=Configure::read('WEBSITE_URL')?>"><?=__('Home')?></a></li>
          <!--  <li><a href="#"><?=__('Facilitati')?></a></li>
            <li><span><?=__('Blocuri')?></span>
                <ul>
                    <li><a href="/bloc-1"><?=__('Bloc 1')?></a></li>
                    <li><a href="/"><?=__('Bloc 2')?></a></li>
                    <li><a href="/"><?=__('Bloc 3')?></a></li>

                </ul>
            </li>-->
            <li><a href="/galerie"><?=__('Galerie')?></a></li>
            <li><a href="<?=Configure::read('WEBSITE_URL')?>#despre-noi"><?=__('Despre noi')?></a></li>
            <li><a href="<?=Configure::read('WEBSITE_URL')?>#contact"><?=__('Contact')?></a></li>

        </ul>
    </nav>


    <!--------- meniu desktop ---------->
    <nav class="navbar navbar-expand-sm navbar-hero d-none d-lg-block">
        <div class="container pt-3 position-relative">
        <a class="navbar-brand" href="/">
            <img src="/assets/images/logo_mobipark.png">
        </a>

        <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <ul class="navbar-nav ml-auto">
                <li class="nav-item">
                    <a class="nav-link" href="<?=Configure::read('WEBSITE_URL')?>"><?=__('Home')?></a>
                </li>
            <!--    <li class="nav-item subcateg">
                    <a class="nav-link" href="/blocuri"><?=__('Blocuri ')?>
                        <i class="fas fa-caret-down"></i>
                    </a>
                    <div class="top-megamenu-subcategories">
                        <div class="dropdown-mm position-absolute" style="display: none;">
                            <div class="subcategory-content">
                                    <ul class="subcat-list">
                                        <li>
                                            <a href="/bloc-1">
                                                <?=__('Bloc 1')?>
                                            </a>
                                        </li>
                                        <li>
                                            <a href="/">
                                                <?=__('Bloc 2')?>
                                            </a>
                                        </li>
                                        <li>
                                            <a href="/">
                                                <?=__('Bloc 3')?>
                                            </a>
                                        </li>
                                    </ul>
                            </div>
                        </div>
                    </div>
                </li> -->
                <li class="nav-item">
                    <a class="nav-link" href="/galerie"><?=__('Galerie')?></a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="<?=Configure::read('WEBSITE_URL')?>#despre-noi"><?=__('Despre noi')?></a>
                </li>
                <li class="nav-item">
                    <a class="nav-link contact-link" href="<?=Configure::read('WEBSITE_URL')?>#contact"><?=__('Contact')?></a>
                </li>
            </ul>
        </div>
        </div>
    </nav>



    <div class="page-content">
        <?=$this->fetch('content'); ?>
    </div>


    <div class="footer dark-bg">
        <div class="container">
            <div class="row">
                <div class="footer-logo text-left col-12 col-md-4 text-center text-md-left pt-4">
                    <img src="/assets/images/logo_mobipark.png">
                </div>
                <div class="footer-links text-left text-md-right text-uppercase col-8 pt-4 pt-lg-5 pb-0 pb-lg-5">
                    <ul>
                        <li class="d-block d-lg-inline-block">
                            <a href="<?=Configure::read('WEBSITE_URL')?>"><?=__('Home')?></a>
                        </li>
                        <li class="pl-0 pl-md-5 d-block d-lg-inline-block">
                            <a href="/galerie"><?=__('Galerie')?></a>
                        </li>
                        <li class="pl-0 pl-md-5 d-block d-lg-inline-block">
                            <a href="<?=Configure::read('WEBSITE_URL')?>#despre-noi"><?=__('Despre noi')?></a>
                        </li>
                        <li class="pl-0 pl-md-5 d-block d-lg-inline-block">
                            <a href="#contact"><?=__('Contact')?></a>
                        </li>
                    </ul>
                    <hr class="footer-line d-none d-md-block">
                    <div class="copyright d-none d-md-block text-capitalize font-weight-normal"><?=__('Copyright  Mobipark 2020. Toate drepturile rezervate')?></div>
                </div>
                <div class="col-12 d-block d-md-none">
                    <hr class="footer-line">
                    <div class="copyright text-capitalize pb-3 text-center font-weight-normal"><?=__('Copyright  Mobipark 2020.')?><br><?=__('Toate drepturile rezervate')?></div>

                </div>
            </div>
        </div>
    </div>



</div>




<!-- JavaScripts placed at the end of the document so the pages load faster -->
<script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
<script src="/assets/js/jquery.validator-new.js" type="text/javascript"></script>
<script src="/assets/js/owl.carousel.js" type="text/javascript"></script>
<script src="/assets/js/jquery.fancybox.min.js"></script>
<script src="/assets/js/sweetalert2.min.js"></script>
<script src="/assets/js/js-cloudimage-360-view.min.js"></script>

<script src="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.13.1/js/all.min.js"></script>
<?php echo $this->fetch('script')?>
<script src="/assets/js/disclaimer.js" type="text/javascript"></script>
<script src="/assets/js/scripts.js" type="text/javascript"></script>
<?=$this->fetch('extra-scripts')?>
</body>
</html>