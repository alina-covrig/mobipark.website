<?php
App::uses('Shell', 'Console');
App::uses('HttpSocket', 'Network/Http');
App::uses('CakeEmail', 'Network/Email');

/**
 * Class NotificationsShell
 *
 * @property Invoice Invoice
 * @property Sale Sale
 * @property SaleProduct SaleProduct
 * @property User User
 * @property Person Person
 * @property Model StockAtTime
 * @property Product Product
 * @property ProductAliasName ProductAliasName
 * @property EntryProduct EntryProduct
 * @property EntryProductFlow EntryProductFlow
 * @property Administration Administration
 */
class NotificationsShell extends AppShell
{
    public $components = ['SMS'];
    
    public $uses = [
        'Orders.Invoice',
        'Orders.Sale',
        'Orders.SaleProduct',
        'Catalog.ProductImage',
        'Catalog.Category',
        'Catalog.ProductAliasName',
        'Catalog.Product',
        'Dropships.EmagAssociateCategory',
        'Dropships.EmagCategory',
        'Dropships.EmagProductsStatus',
        'Merchandise.StockAtTime',
        'Customers.User',
        'Customers.Person',
        'Merchandise.EntryProductFlow',
        'Merchandise.Entry',
        'Merchandise.EntryProduct',
        'Merchandise.Administration',
        'Catalog.Bargain',
        'Prices.Currency',
        'Suppliers.SupplierProduct'
    ];

    public function updateQuantities()
    {
        $products = $this->StockAtTime->find(
            'all', [
                'conditions' => [
                    'is_sent' => 0
                ],
                'fields' => [
                    'product_id',
                    'max(time) as time',
                    'is_sent',
                ],
                'group' => [
                    'product_id'
                ]
            ]
        );

        $vat = (100 + Configure::read('VAT_PERCENT')) / 100;

        if (!empty($products)) {
            foreach ($products as $k => $p) {

                $this->Product->bindModel(
                    array(
                        'hasOne' => array(
                            'MainImage' => array(
                                'className' => 'Catalog.ProductImage',
                                'conditions' => array(
                                    'MainImage.is_main' => 1
                                )
                            ),
                        ),
                        'hasMany' => array(
                            'Image' => array(
                                'className' => 'Catalog.ProductImage',
                                'conditions' => array(
                                    'Image.is_main' => 0
                                )
                            )
                        ),
                    )
                );

                $this->Category->bindModel(
                    array(
                        'hasOne' => array(
                            'EmagAssociateCategory' => array(
                                'className' => 'Dropships.EmagAssociateCategory'
                            )
                        )
                    )
                );

                $this->ProductAliasName->bindModel(
                    [
                        'hasOne' => [
                            'EmagProductsStatus' => [
                                'className' => 'Dropships.EmagProductsStatus'
                            ]
                        ]
                    ]
                );

                $this->ProductAliasName->contain(
                    array(
                        'Category' => array(
                            'EmagAssociateCategory',
                        ),
                        'Manufacturer',
                        'Product' => array(
                            'MainImage',
                            'Image',
                            'Availability'
                        ),
                        'Manufacturer',
                        'EmagProductsStatus'
                    )
                );

                $this->Product->virtualFields = array(
                    'stock_total' => 'get_stock_with_supplier(Product.id)'
                );

                $product = $this->ProductAliasName->findByProductId(
                    $p['StockAtTime']['product_id']
                );

                if (empty($product['Category']['EmagAssociateCategory'])
                    || empty($product['EmagProductsStatus']['id'])
                ) {
                    //continue;
                }

                $data = Array(
                    Array(
                        "id" => $product['ProductAliasName']['id'],
                        "status" => $product['EmagProductsStatus']['status'],
                        "sale_price"
                        => price($product['Product']['selling_price'] / $vat),
                        "recommended_price" =>
                            ($product['Product']['old_price']
                                > price($product['Product']['selling_price']))
                                ? price($product['Product']['old_price'] / $vat) : null,
                        "availability" => Array(
                            Array(
                                "warehouse_id" => "1",
                                "id" => $this->_getEmagAvailabilityIdNotifications(
                                    $product['Product']['availability_id']
                                )
                            )
                        ),
                        "handling_time" => Array(
                            Array(
                                "warehouse_id" => "1",
                                "value" => "1"
                            )
                        ),
                        "stock" => Array(
                            Array(
                                "warehouse_id" => "1",
                                "value" => $product['Product']['stock_total']
                            )
                        ),
                        "commission" => Array(
                            "type" => "percentage",
                            "value" => "20"
                        ),
                        "vat_id" => "1"
                    )
                );

                $this->StockAtTime->updateAll(
                    array('StockAtTime.is_sent' => 1),
                    array('StockAtTime.product_id' => $p['StockAtTime']['product_id'])
                );

                $this->EmagProductsStatus->contactAPI($data, 'product_offer', 'save');
            }
        }
    }

    private function _getEmagAvailabilityIdNotifications($availability_id)
    {
        if ($availability_id == 1) {
            return 3; // in stoc
        }
        if ($availability_id == 3) {
            return 2; // stoc limitat
        }
        if ($availability_id == 4) {
            return 5; // lipsa stoc
        }
    }

    public function accountantPaymentOverdueReport()
    {
        $users = $this->User->find(
            'all', array(
                'conditions' => array(
                    'User.payment_delay >' => 0
                ),
                'fields' => array(
                    'User.id',
                    'User.email'
                )
            )
        );

        $unpaid_invoices = [];
        foreach ($users as $user) {
            $unpaid_invoices[$user['User']['id']] = $this->Invoice->find(
                'all',
                array(
                    'conditions' => array(
                        'Invoice.value >' => 0,
                        'Invoice.is_paid' => false,
                        'Invoice.payment_due_at < NOW()',
                        'Sale.user_id' => $user['User']['id'],
                    ),
                    'joins' => array('
                        LEFT JOIN `orders__sales` AS `Sale`' . ' '
                        . 'ON(`Invoice`.`sale_id` = `Sale`.`id`)'
                    ),
                )
            );
        }
        if ($unpaid_invoices) {
            $baseUrl = Configure::read('WEBSITE_URL');
            $storeName = Configure::read('STORE_NAME');
            $viewVars = array(
                //fixed vars
                'baseUrl' => $baseUrl,
                'storeName' => $storeName,
                'userId' => null,
                //--fixed vars
                'invoices' => $unpaid_invoices
            );

            $subject = __('Raport clienti si facturi cu termen de plata depasit');
            App::uses('CakeEmail', 'Network/Email');
            $em = new CakeEmail('default');
            $em->domain($baseUrl);
            $em->to(Configure::read('ACCOUNTANT_EMAIL'));
            $em->subject($subject);
            $em->template('report_unpaid_invoices');
            $em->viewVars($viewVars);
            if (!$em->send()) {
                $this->out('<error>' . __('Eroare la trimiterea raport '
                    . 'termen de plata depasit') . '</error>');
            } else {
                $this->out('<info>' . __('Raport termen de plata '
                    . 'depasit trimis') . '</info>');
            }
        } else {
            $this->out('<info>' . __('Niciun email de trimis') . '</info>');
        }
    }

    public function syncStockToDropshipper()
    {
        $this->Product->Behaviors->load('Containable');
        $this->Product->contain();

        $transaction = $this->Product->getDataSource();
        $transaction->begin();

        $stocks = $this->Product->find('all', [
            'conditions' => [
                'stock_is_sent' => false,
            ],
            'fields' => ['id', 'part_number', 'stock']
        ]);

        try {
            $this->Product->updateAll(
                [
                    'stock_is_sent' => 1
                ],
                [
                    'stock_is_sent' => 0
                ]
            );
        } catch (Exception $err) {
            $this->out("<error>" . $err->getMessage() . "</error>");
        }

        $transaction->commit();

        $sock = new HttpSocket();
        $data = [
            'email' => 'lucian.vasile@live.com',
            'hash' => '62ddc671374c02a87ff7803ab58b1d2b4ddc3f2b'
        ];
        foreach ($stocks as $s) {
            $data['part_number'] = $s['Product']['part_number'];
            $data['stock'] = $s['Product']['stock'];
            $response = $sock->get('http://backoffice.mobiparts.ro/dropships/general/updateStock.json', $data);
            $response = json_decode($response);
            if (isset($response->status)) {
                $this->out($response->message);
            } else {
                $this->out(pr($response));
            }
        }
    }

    /**
     * Trimite comenzile din mobiparts in gsmnet. Ruleaza pe mobiparts
     */
    public function sendOrdersToDropShipper()
    {
        $this->Sale->contain([
            'SaleProduct',
            'SaleComment',
            'SaleStatus',
            'PaymentStatus',
            'PaymentMethod',
            'DeliveryMethod',
            'User' => ['Person'],
        ]);
        $this->Sale->setDataSource('coco');
        $this->SaleProduct->setDataSource('coco');
        $this->User->setDataSource('coco');
        $this->Person->setDataSource('coco');
        $orders = $this->Sale->find('all', [
            'conditions' => [
                'Sale.created_at >' => date('Y-m-d H:i:s', time() - 4600)
            ]
        ]);
        foreach ($orders as &$o) {
            $o['Sale']['source'] = 'mobiparts';
            $o['emag_order_id'] = $o['Sale']['id'];
        }
        $sock = new HttpSocket();
        $url = 'http://backoffice.gsmnet.website/dropships/general/saveSale.json?';
        $data = [
            'email' => 'lucian.vasile@live.com',
            'hash' => '62ddc671374c02a87ff7803ab58b1d2b4ddc3f2b'
        ];
        $url .= http_build_query($data);
        foreach ($orders as $o) {
            $post_result = $sock->post($url, $o);
            $response = json_decode($post_result->body);
            if (isset($response->status) && ($response->status == 'success')) {
                $this->out($response->message);
                $this->Sale->id = $o['Sale']['id'];
                $this->Sale->saveField('created_at', date('Y-m-d H:i:s'));
                $em = new CakeEmail();
                $em->from('office@gsmnet.ro');
                $em->to('lucian.vasile@live.com');
                $em->subject(
                    __('Comanda noua #%s pe mobiparts.ro - COCO',
                        $o['Sale']['id']
                    ));
                $em->send('Verifica');
            } else {
                $this->out(pr($post_result));
                $this->out(
                    __('Eroare de conectare la web service la salvarea comenzii')
                );
            }
        }
    }

    /**
     * Ruleaza pe mobiparts citeste comenzile cu status schimbat din gsmnet
     */
    public function updateOrdersFromDropShipper()
    {
        $sock = new HttpSocket();
        $url = 'http://backoffice.gsmnet.ro/orders/sales.json';

        $data = [
            'email' => 'lucian.vasile@live.com',
            'hash' => '62ddc671374c02a87ff7803ab58b1d2b4ddc3f2b',
            'conditions' => [
                'Sale.source' => 'mobiparts',
                'Sale.modified_at >' => date('Y-m-d H:i:s', strtotime('-2 months')),
                'Sale.sale_status_id' => [
                    11, //Finalizata
                    12 //Anulata
                ],
               // 'Sale.modified_at >' => date('Y-m-d H:i:s', time() - 1800),
            ]
        ];

        $request = $sock->get($url, $data);
        $response = json_decode($request, 1);

        if (isset($response['sales']) && !empty($response['sales'])) {
            foreach ($response['sales'] as $s) {
                $file_path  = 'https://www.gsmnet.ro/users/serveFile' . $s['Invoice']['file_path']
                    . '?email=' . $s['User']['email']
                    . '&hash=' . $s['User']['hash'];

                //$this->Sale->id = $s['Sale']['emag_order_id'];
                //$this->Sale->saveField('sale_status_id', $s['Sale']['sale_status_id']);

                $status = $s['Sale']['sale_status_id'] == 11 ? 4 : 0;

                $this->Sale->contain();
                $sale = $this->Sale->findById($s['Sale']['emag_order_id']);
                //if (empty($sale)) continue;

                $emag_order_id = '22236117'; //$sale['Sale']['emag_order_id']
                $data = Array(
                    Array(
                        "id" => $emag_order_id,
                        "status" => $status,
                        /*"attachments" => [

                        ]*/
                    )
                );
                $this->out(pr($data));

                $api = $this->EmagProductsStatus->contactAPI($data,'order','save');
                $this->out(pr($api));
                die('asdasd');
            }
        }

    }

    public function updateAverage()
    {

        $this->EntryProductFlow->bindModel([
            'belongsTo' => [
                'EntryProduct' => [
                    'className' => 'Merchandise.EntryProduct'
                ]
            ]
        ]);
        $pids = $this->EntryProductFlow->find(
            'all', [
                'conditions' => [
                    'created_at >' => date('Y-m-d', strtotime('-1 year'))
                ],
                'fields' => ['EntryProduct.product_id'],
                'group' => ['EntryProduct.product_id'],
            ]
        );
        $body = [];
        foreach ($pids as $p) {
            try {
                $this->Product->query(
                    'UPDATE catalog__products SET average = get_average_sales(id) where id = '
                    . $p['EntryProduct']['product_id']
                );
                $body[] = $mess = __("Media produsului #%s a fost actualizata!",
                    $p['EntryProduct']['product_id']);
                echo $mess . "\r";
            } catch (Exception $er) {
                $body[] = $mess = "Eroare la actualizarea mediei #{$p['EntryProduct']['product_id']}: "
                    . $er->getMessage();
                echo $mess . "\r";
            }
        }
    }

    public function maintainStockIntegrity()
    {
        $list = $this->Product->find('list', [
            'conditions' => [
                'stock <> get_stock_with_supplier(Product.id)',
            ],
            ['fields' => ['Product.id']],
        ]);

        foreach ($list as $pid) {
            $this->Product->query('UPDATE catalog__products p
                SET p.stock = get_stock_with_supplier(p.id), p.stock_is_sent = 0 
                WHERE p.id = ' . $pid);
        }

        //maintain stock availability = 1
        $list = $this->Product->find('list', [
            'conditions' => [
                'availability_id = 4',
                'stock != 0'
            ],
            ['fields' => ['Product.id']],
        ]);

        foreach ($list as $pid) {
            $this->Product->query('UPDATE catalog__products p
                SET p.availability_id = 1, p.stock_is_sent = 0 
                WHERE p.id = ' . $pid);
        }

        //maintain stock availability = 4
        $list = $this->Product->find('list', [
            'conditions' => [
                'availability_id != 4',
                'stock = 0'
            ],
            ['fields' => ['Product.id']],
        ]);

        foreach ($list as $pid) {
            $this->Product->query('UPDATE catalog__products p
                SET p.availability_id = 4, p.stock_is_sent = 0 WHERE
                p.id = ' . $pid);
        }
    }

    public function maintainTagProductsCount() {
        //update products count to each tag
        $this->Product->query('UPDATE catalog__tags AS ct
            SET products_count = (
                    SELECT COUNT(*) 
                FROM catalog__product_alias_name_tags AS cpant 
                    LEFT JOIN catalog__product_alias_names AS cpan ON cpant.product_alias_name_id = cpan.id
                    LEFT JOIN catalog__products AS cp ON cpan.product_id = cp.id
                WHERE cpant.tag_id = ct.id AND cp.is_active = 1
            )');
    }

    public function migrateChars()
    {

        // 1. creem structura
        $this->Product->query('
            DROP TABLE IF EXISTS _cvp;
            DROP TABLE IF EXISTS _cv;
            DROP TABLE IF EXISTS _c;
        ');
        $this->Product->query('
            CREATE TABLE _c LIKE catalog__characteristics;
            ALTER TABLE `devgsmnet`.`_c` ADD COLUMN `old_char_id` bigint(20) NOT NULL AFTER `is_family`;
            CREATE TABLE _cv LIKE catalog__characteristic_values;
            ALTER TABLE `devgsmnet`.`_cv` ADD COLUMN `old_cv_id` bigint(20) NOT NULL AFTER `misc`;
            ALTER TABLE `devgsmnet`.`_cv` ADD CONSTRAINT `belongsToCharacteristic` FOREIGN KEY (`characteristic_id`) REFERENCES `devgsmnet`.`_c` (`id`)   ON UPDATE CASCADE ON DELETE CASCADE;
            CREATE TABLE _cvp LIKE catalog__characteristic_values_products;
            ALTER TABLE `devgsmnet`.`_cvp` ADD CONSTRAINT `belongsToCharValue` FOREIGN KEY (`characteristic_value_id`) REFERENCES `devgsmnet`.`_cv` (`id`)   ON UPDATE CASCADE ON DELETE CASCADE;
            ALTER TABLE `devgsmnet`.`_c` DROP INDEX `name`, ADD INDEX `name` USING BTREE (`name`, alias);
            ALTER TABLE `devgsmnet`.`_c` CHANGE COLUMN `category_id` `category_id` bigint(20) UNSIGNED NOT NULL, ADD INDEX  (category_id);

ALTER TABLE `devgsmnet`.`_c` ADD CONSTRAINT `char_belongs_to_category` FOREIGN KEY (`category_id`) REFERENCES `devgsmnet`.`catalog__categories` (`id`)   ON UPDATE CASCADE ON DELETE CASCADE;
ALTER TABLE `devgsmnet`.`_c` CHANGE COLUMN `group_id` `group_id` bigint(20) UNSIGNED NOT NULL, ADD INDEX  (group_id);

ALTER TABLE `devgsmnet`.`_c` ADD CONSTRAINT `char_belongs_to_group` FOREIGN KEY (`group_id`) REFERENCES `devgsmnet`.`catalog__categories_characteristics_groups` (`id`)   ON UPDATE CASCADE ON DELETE CASCADE;
        ');
        $this->Product->query('
            INSERT IGNORE INTO _c (category_id, group_id, name, alias, type, misc, `order`, is_catalog, is_family, is_filter, is_hidden, old_char_id)
            (SELECT cc.category_id, cc.group_id, c.name, c.alias, c.type, c.misc,
            cc.order, cc.is_catalog, cc.is_family, cc.is_filter, cc.is_hidden, c.id
            from catalog__categories_characteristics cc left join catalog__characteristics c on c.id = cc.characteristic_id);
        ');
        $this->Product->query('
            INSERT IGNORE INTO _cv (characteristic_id, value, alias, misc, old_cv_id)
            (SELECT _c.id, cv.value, cv.alias, cv.misc, cv.id FROM _c
            left join catalog__characteristic_values cv on cv.characteristic_id = _c.old_char_id);
        ');
        //$this->Product->query("SET FOREIGN_KEY_CHECKS=0;");
        $this->Product->query('
            INSERT IGNORE INTO _cvp (characteristic_value_id, product_id, text_value, alias)
            (SELECT _cv.id, cvp.product_id, cvp.text_value, cvp.alias from catalog__characteristic_values_products cvp
LEFT JOIN catalog__products p on cvp.product_id = p.id
LEFT JOIN catalog__product_alias_names pan on pan.product_id = p.id
LEFT JOIN _cv ON  _cv.old_cv_id = cvp.characteristic_value_id
LEFT JOIN _c on _c.id = _cv.characteristic_id
where pan.is_main = 1 and pan.category_id = _c.category_id);
        ');
        //$this->Product->query("SET FOREIGN_KEY_CHECKS=1;");
        $this->out('Done!');
        die();

    }

    public function computeStockValue() {
        $this->EntryProduct->query(
            'DROP TABLE IF EXISTS ld_total;
                CREATE TABLE ld_total LIKE merchandise__entry_products;
                INSERT INTO ld_total
                SELECT ep.* FROM merchandise__entry_products ep
                LEFT JOIN merchandise__entry_product_flows epf 
            ON epf.entry_product_id = ep.id
                WHERE ep.is_sold = 0
                GROUP BY ep.id
                HAVING SUM(operation) = 1;
            '
        );
    }

    public function notifyReceivedProducts()
    {
        $today = date('Y-m-d'/*, strtotime('-40 days')*/);

        $this->Entry->bindModel(
            array(
                'hasMany' => array(
                    'EntryProduct' => array(
                        'className' => 'Merchandise.EntryProduct',
                    )
                )
            )
        );
        $this->Entry->EntryProduct->bindModel(
            array(
                'belongsTo' => array(
                    'Product' => array(
                        'className' => 'Catalog.Product',
                    )
                )
            )
        );

        $this->Entry->contain([
            'EntryProduct' => [
                /* 'limit' => 2*/
            ],
            'EntryProduct.Product' => [
                'ProductAliasName' => [
                    'conditions' => [
                        'is_main' => 1
                    ]
                ]
            ]
        ]);
        $today_entries = $this->Entry->find(
            'all', [
                'conditions' => [
                    'date(Entry.created_at)' => $today,
                    'is_draft' => false
                ]
            ]
        );
        pr($today_entries);
        die('TBD cand se hotareste radu');
    }

    /**
     * Mutat din BargainsController
     */
    public function setBargain()
    {
        $today = date('Y-m-d');
        $yesterday = date('Y-m-d', strtotime("-1 days"));
        $error = false;

        $lastBargain = $this->Bargain->find(
            'first', [
                'conditions' => [
                    'date' => $yesterday
                ]
            ]
        );

        $nextBargain = $this->Bargain->find(
            'first', [
                'conditions' => [
                    'date' => $today
                ]
            ]
        );

        $transaction = $this->Product->getDataSource();
        $transaction->begin();

        if (!empty($lastBargain)) {
            //schimbam inapoi pretul produsului vechi
            $pan_id = $lastBargain['Bargain']['product_alias_name_id'];
            $product = $this->ProductAliasName->findById(
                $pan_id,
                ['product_id']
            );
            $this->Product->id = $product['ProductAliasName']['product_id'];
            $updateLastProduct['Product']['selling_price']
                = $lastBargain['Bargain']['old_price'];
            $updateLastProduct['Product']['selling_currency']
                = $lastBargain['Bargain']['old_price_currency'];
            if (!empty($lastBargain['Bargain']['old_reduced_price'])) {
                $updateLastProduct['Product']['old_price']
                    = $lastBargain['Bargain']['old_reduced_price'];
            }
            if (!$this->Product->save($updateLastProduct)) {
                $error = true;
            }
        }

        if (!empty($nextBargain)) {
            // salvam pretul actual la produsul nou
            // si il schimbam cu cel de "chilipir"
            $new_pan_id = $nextBargain['Bargain']['product_alias_name_id'];
            $new_product = $this->ProductAliasName->findById(
                $new_pan_id,
                ['product_id']
            );

            $this->Product->id = $new_product['ProductAliasName']['product_id'];
            $old_price = $this->Product->field('old_price');
            $actual_price = $this->Product->field('selling_price');

            $updateNextProduct['Product']['selling_price']
                = $nextBargain['Bargain']['new_price'];
            $updateNextProduct['Product']['selling_currency']
                = $nextBargain['Bargain']['new_price_currency'];
            if (!empty($nextBargain['Bargain']['reduced_price'])) {
                $updateNextProduct['Product']['old_price']
                    = $nextBargain['Bargain']['reduced_price'];
            }

            if (!$this->Product->save($updateNextProduct)) {
                $error = true;
            }

            $saveOldPrices['id'] = $nextBargain['Bargain']['id'];
            $saveOldPrices['old_price'] = $actual_price;
            $saveOldPrices['old_reduced_price'] = $old_price;

            if (!$this->Bargain->save($saveOldPrices)) {
                $error = true;
            }
        }

        if ($error) {
            $transaction->rollback();

            $subject = 'A aparut o eroare la schimbarea chilipirului zilei';

            App::uses('CakeEmail', 'Network/Email');
            $em = new CakeEmail('default');
            $em->to(Configure::read('STORE_CONTACT_EMAIL'));
            $em->emailFormat('text');
            $em->subject($subject);
            $em->message($subject);
            if (!$em->send()) {
                die('A aparut o eroare la schimbarea chilipirului '
                    . 'si nu s-a trimis emailul de instiintare');
            }

            die('A aparut o eroare la schimbarea chilipirului');
        }

        $transaction->commit();
        die('Chilipirul zilei a fost schimbat cu success');
    }

    public function getCurrencies()
    {
        App::uses('HttpSocket', 'Network/Http');
        $sock = new HttpSocket();
        try {
            $r = $sock->get('https://www.bnr.ro/nbrfxrates.xml');
            App::uses('Xml', 'Utility');
            $xml = Xml::toArray(Xml::build($r->body));
            $to_save = [];
            $k = 0;
            if (isset($xml['DataSet']['Body']['Cube']['Rate'])) {
                foreach ($xml['DataSet']['Body']['Cube']['Rate'] as $c) {
                    $to_save[$k]['currency'] = $c['@currency'];
                    $to_save[$k]['currency_code'] = $c['@currency'];
                    $to_save[$k++]['value'] = $c['@'];
                }
            }
            $this->Currency->saveMany($to_save);
        } catch (Exception $err) {

        }
    }

    public function getStockFromGsmnet()
    {
        $products = $this->ProductAliasName->find(
            'list', [
                'fields' => ['id']
            ]
        );

        $link = 'http://www.gsmnet.ro/pages/stock?email=lucian.vasile@live.com'
            . '&hash=62ddc671374c02a87ff7803ab58b1d2b4ddc3f2b';


        $path = dirname(ROOT) . DS . 'web' . DS . 'files' . DS . 'stock';

        if (!is_dir($path)) {
            mkdir($path, 0755, true);
        }

        file_put_contents($path . DS . 'get_stock.csv', file_get_contents($link));

        $csvFile = file($path . DS . 'get_stock.csv');
        $data = [];

        foreach ($csvFile as $line) {
            $line = str_getcsv($line, ";");
            if (in_array($line[0], $products)) {
                $data[] = $line;
            }
        }

        $message_array = [];

        foreach ($data as $line) {
            $this->ProductAliasName->contain([
                'Product.part_number'
            ]);

            $product = $this->ProductAliasName->findById(
                $line[0], [
                    'product_id'
                ]
            );

            $supplierProduct = $this->SupplierProduct->find(
                'first', [
                    'conditions' => [
                        'supplier_id' => 1, //mobiparts
                        'product_id'  => $product['ProductAliasName']['product_id']
                    ]
                ]
            );

            $save = [];

            if (!empty($supplierProduct['SupplierProduct']['id'])) {
                $this->SupplierProduct->id
                    = $supplierProduct['SupplierProduct']['id'];

            } else {
                $this->SupplierProduct->create();
                $save['inserted'] = date('Y-m-d H:i:s');
            }

            $save['supplier_id']   = 1;
            $save['product_id']    = $product['ProductAliasName']['product_id'];
            $save['part_number']   = $product['Product']['part_number'];
            $save['supplier_code'] = $product['Product']['part_number'];
            $save['quantity']      = $line[2];
            $save['price']         = $line[3];
            $save['currency']      = $line[4];

            if (!$this->SupplierProduct->save($save)) {
                $message_array[] = 'A aparut o eroare la salvarea produsului '
                    . $product['Product']['part_number'];
            }
        }

        if (empty($message_array)) {
            die('Sincronizarea s-a efectuat cu success');
        }
    }
    
    public function moveProductsFromAdministration3To1() {
        $this->EntryProductFlow->bindModel([
            'belongsTo' => [
                'EntryProduct' => [
                    'className' => 'Merchandise.EntryProduct'
                ]
            ]
        ]);
        $this->EntryProduct->virtualFields = [
            'sum_operation' => 'select sum(operation) from merchandise__entry_product_flows where entry_product_id = EntryProduct.id'
        ];
        $list = $this->EntryProductFlow->find('all', [
            'conditions' => [
                'EntryProduct.is_sold' => 0,
                'EntryProduct.is_reserved' => 0,
                'EntryProductFlow.type' => 'BACK',
                'EntryProduct.administration_id' => 3,
            ],
            'group' => 'EntryProduct.id HAVING EntryProduct__sum_operation = 1'
        ]);
        $transfer_products = [];
        foreach ($list as $p) {
            if (!isset($transfer_products[$p['EntryProduct']['product_id']])) {
                $transfer_products[$p['EntryProduct']['product_id']] = 1;
            } else {
                $transfer_products[$p['EntryProduct']['product_id']]++;
            }
        }
        $transfer = [
            'from_administration_id' => 3,
            'to_administration_id' => 1,
            'from_supplier_id' => 3,
            'to_supplier_id' => 3,
            'products' => $transfer_products
        ];
        
        if (!empty($transfer_products)) {
            $transaction = $this->Product->getDataSource();
            $transaction->begin();
            if (!$this->Administration->transfer($transfer, 1)) {
                $transaction->rollback();
            } else {
                $transaction->commit();
            }
        }
    }
    
    public function checkPrices() {
        $this->Product->contain([
            'ProductAliasName.id'
        ]);
        $list = $this->Product->find('all', [
            'conditions' => [
                'Product.selling_price' => 0,
                'Product.is_active' => 1
            ],
            'fields' => [
                'Product.id'
            ]
        ]);
        $mail_list = '';
        foreach ($list as $p) {
            $this->Product->id = $p['Product']['id'];
            $this->Product->saveField('is_active', 0);
            $mail_list .= $p['ProductAliasName']['id'] . '<br />';
        }
        if (!empty($mail_list)) {
            $text = "Lista de produse cu pret 0 care au fost dezactivate<br /><br />";
            $em = new CakeEmail();
            $em->from('office@gsmnet.ro', 'System GSMNET')
                ->to('office@gsmnet.ro')
                ->subject('Lista de produse cu pret 0 care au fost dezactivate')
                ->send($text . $mail_list);
        }
    }
    
    public function sendSMS() {
        App::uses('SMSComponent', 'Controller/Component');
        $SMS = new SMSComponent(new ComponentCollection());
        $list = [
            "0761608398",
            "0765444007",
            "0740857042",
            "0742941634",
            "0722869715",
            "0765399341",
            "0765624527",
            "0336802225",
            "0744828828",
            "0723646311",
            "0721903736",
            "0726330074",
            "0754625090",
            "0746761410",
            "0768256881",
            "0766200060",
            "0741041541",
            "0747638319",
            "0724002810",
            "0754529543",
            "0740186045",
            "0734951114",
            "0723876636",
            "0740408782",
            "0723340275",
            "0727106560",
            "0722774270",
            "0744776607",
            "0722605691",
            "0760116611",
            "0720545114",
            "0748999933",
            "0744454544",
            "0746327132",
            "0746924929",
            "0743701755",
            "0765502602",
            "0767988123",
            "0722666773",
            "0726388818",
            "0765523337",
            "0724524411",
            "0765359600",
            "0740070725",
            "0743335629",
            "0722381349",
            "0369809086",
            "0766274391",
            "0763481455",
            "0726174764",
            "0765419294",
            "0749683448",
            "0766524142",
            "0762087087",
            "0762605045",
            "0747370726",
            "0740776647",
            "0720920900",
            "0744135474",
            "0730816563",
            "0766142327",
            "0747928630",
            "0746228414",
            "0740416168",
            "0744255309",
            "0740312313",
            "0740288078",
            "0747955442",
            "0757437328",
            "0768333456",
            "0724207926",
            "0746541090",
            "0741754699",
            "0722622006",
            "0726234932",
            "0745750467",
            "0765491736",
            "0767701577",
            "0723761555",
            "0742616428",
            "0735983738",
            "0747812999",
            "0761078661",
            "0721376462",
            "0743866884",
            "0768899015",
            "0767853629",
            "0743824409",
            "0753049598",
            "0724110390",
            "0765550101",
            "0267372970",
            "0742946237",
            "0745662873",
            "0729927436",
            "0744999009",
            "0744706903",
            "0740766944",
            "0757319357",
            "0348803747",
            "0742555501",
            "0784026513",
            "0245774942",
            "0743976503",
            "0745663987",
            "0743884775",
            "0269443909",
            "0232715747",
            "0722900687",
            "0757435625",
            "0765389557",
            "0765412092",
            "0740510181",
            "0728466261",
            "0766993688",
            "0734302886",
            "0755614915",
            "0744158792",
            "0722688994",
            "0743388008",
            "0744769896",
            "0745829949",
            "0723721959",
            "0766837322",
            "0744983515",
            "0773839810",
            "0766882214",
            "0740423053",
            "0753950414",
            "0741798940",
            "0741540200",
            "0262355135",
            "0747173734",
            "0722644060",
            "0744581622",
            "0766665221",
            "0726835375",
            "0743584297",
            "0762514969",
            "0741999800",
            "0744666626",
            "0743029639",
            "0741202703",
            "0762656738",
            "0748109052",
            "0740917926",
            "0753868547",
            "0727053503",
            "0724957279",
            "0748052525",
            "0730527925",
            "0722254143",
            "0762108688",
            "0259476299",
            "0730918116",
            "0771794897",
            "0724907770",
            "0752459201",
            "0757644359",
            "0730015637",
            "0740219324",
            "0728789613",
            "0766630659",
            "0743172952",
            "0740234119",
            "0740408782",
            "0722489575",
            "0723329529",
            "0765523557",
            "0724386620",
            "0748076347",
            "0745577505",
            "0722858848",
            "0744494141",
            "0741980253",
            "0745328550",
            "0724001015",
            "0721991333",
            "0730129362",
            "0241768031",
            "0722899699",
            "0723022011",
            "0723601515",
            "0763120666",
            "0748861446",
            "0758518819",
            "0742925192",
            "0757671028",
            "0722438536",
            "0749550740",
            "0723554755",
            "0768595585",
            "0721157530",
            "0745125515",
            "0740064426",
            "0722272775",
            "0744384371",
            "0745516324",
            "0760677935",
            "0745436535",
            "0724687687",
            "0765339075",
            "0764161666",
            "0748502413",
            "0726293234",
            "0744543499",
            "0745564599",
            "0726935115",
            "0721270587",
            "0765366386",
            "0765353624",
            "0723013999",
            "0743838007",
            "0721436925",
            "0755331922",
            "0723370530",
            "0766407326",
            "0745638410",
            "0769512951",
            "0766285561",
            "0747342721",
            "0770992354",
            "0749496241",
            "0772205811",
            "0742853644",
            "0744399488",
            "0765012029",
            "0746877741",
            "0742542789",
            "0721848429",
            "0744401996",
            "0742902307",
            "0753460691",
            "0744182591",
            "0744811331",
            "0740060283",
            "0743807397",
            "0745302829",
            "0742894641",
            "0722369329",
            "0746151319",
            "0740339929",
            "0765350383",
            "0723192627",
            "0740242708",
            "0745389689",
            "0765366846",
            "0742252456",
            "0724020223",
            "0740901053",
            "0761990000",
            "0728504893",
            "0747362815",
            "0741266806",
            "0722605492",
            "0767180000",
            "0765383220",
            "0752757457",
            "0724152281",
            "0765284788",
            "0745202631",
            "0741075661",
            "0726260268",
            "0744274251",
            "0740543095",
            "0745363522",
            "0742432193",
            "0722877555",
            "0745425399",
            "0742046713",
            "0252361165",
            "0741036879",
            "0740034955",
            "0766778581",
            "0740537180",
            "0749458064",
            "0740621032",
            "0746098611",
            "0729122794",
            "0745609602",
            "0726142379",
            "0725336800",
            "0766319697",
            "0723790124",
            "0742161587",
            "0742376828",
            "0721910660",
            "0788788883",
            "0766313652",
            "0735722333",
            "0727917929",
            "0745425230",
            "0737061719",
            "0745875066",
            "0757777477",
            "0735866866",
            "0765393365",
            "0722330959",
            "0744903070",
            "0723488325",
            "0748881570",
            "0765520380",
            "0753933485",
            "0749289304",
            "0744522042",
            "0737687170",
            "0740520766",
            "0768601928",
            "0740288788",
            "0740614562",
            "0728951333",
            "0735225077",
            "0763355588",
            "0766200724",
            "0720344696",
            "0723614771",
            "0744994000",
            "0744262617",
            "0763760050",
            "0335409243",
            "0764155766",
            "0744823982",
            "0752063122",
            "0742662663",
            "0724221438",
            "0727944444",
            "0755888360",
            "0740986553",
            "0765337989",
            "0722226633",
            "0723452665",
            "0740972806",
            "0746656276",
            "0721192812",
            "0740411140",
            "0777777777",
            "0745313033",
            "0726956558",
            "0745265873",
            "0745944955",
            "0721236188",
            "0740674385",
            "0728093148",
            "0745241889",
            "0745313897",
            "0763111888",
            "0745888203",
            "0735107250",
            "0766960960",
            "0766472068",
            "0768484290",
            "0722622372",
            "0745392392",
            "0745500014",
            "0765388882",
            "0744899865",
            "0765452129",
            "0747666333",
            "0727126134",
            "0741426545",
            "0735386575",
            "0744293243",
            "0744630632",
            "0351789654",
            "0742785075",
            "0720256025",
            "0724130256",
            "0241581616",
            "0765475529",
            "0767293293",
            "0721762818",
            "0765510589",
            "0725402173",
            "0752785299",
            "0744402739",
            "0721738784",
            "0723003830",
            "0740763372",
            "0251417621",
            "0753335035",
            "0755664646",
            "0723356666",
            "0756260129",
            "0724630638",
            "0745130402",
            "0729122262",
            "0727393219",
            "0261710235",
            "0730027055",
            "0724471605",
            "0766690609",
            "0765052040",
            "0760322340",
            "0744202821",
            "0746314392",
            "0763699645",
            "0724309283",
            "0784090939",
            "0728515115",
            "0760468115",
            "0765368265",
            "0744544616",
            "0765394638",
            "0735998678",
            "0765339073",
            "0726291115",
            "0740555556",
            "0747675143",
            "0721464046",
            "0743522059",
            "0369450919",
            "0749662589",
            "0768477727",
            "0749571180",
            "0742311331",
            "0740613736",
            "0724387274",
            "0761662877",
            "0723191039",
            "0766849965",
            "0723272506",
            "0723339294",
            "0744601994",
            "0722527851",
            "0741142518",
            "0740396418",
            "0751996247",
            "0722228852",
            "0727373939",
            "0743561958",
            "0766400925",
            "0741314872",
            "0744697083",
            "0770681639",
            "0727074111",
            "0721848036",
            "0747708292",
            "0743588538",
            "0765472518",
            "0722722772",
            "0720660991",
            "0765188010",
            "0766777327",
            "0762430450",
            "0740064764",
            "0765499019",
            "0751151415",
            "0762188099",
            "0766688622",
            "0722302933",
            "0767411345",
            "0751292540",
            "0748943270",
            "0753801188",
            "0745526336",
            "0755765248",
            "0755868674",
            "0763844272",
            "0745822407",
            "0741416478",
            "0726541145",
            "0748892933",
            "0745848093",
            "0742932898",
            "0722617828",
            "0740277059",
            "0768865601",
            "0767879841",
            "0722643396",
            "0752293915",
            "0766295121",
            "0728135132",
            "0241627700",
            "0733087282",
            "0756151969",
            "0723337877",
            "0736034495",
            "0744818889",
            "0733001222",
            "0747539646",
            "0743086656",
            "0766404413",
            "0742687886",
            "0749776372",
            "0757109509",
            "0726944647",
            "0729441244",
            "0762098002",
            "0722266293",
            "0747854181",
            "0788332892",
            "0722346739",
            "0765444227",
            "0767854854",
            "0765318561",
            "0728428428",
            "0742583389",
            "0765332266",
            "0753647397",
            "0765341138",
            "0763758859",
            "0764725844",
            "0741071721",
            "0769603530",
            "0761328590",
            "0333106295",
            "0726603785",
            "0766200876",
            "0722410333",
            "0767322675",
            "0741483848",
            "0765601307",
            "0762033878",
            "0765175125",
            "0724557396",
            "0745085889",
            "0355803369",
            "0752029625",
            "0746609808",
            "0765384882",
            "0720750665",
            "0742544376",
            "0730080456",
            "0741149094",
            "0720496127",
            "0720714734",
            "0754652991",
            "0724311311",
            "0752818898",
            "0741352695",
            "0723581512",
            "0756012583",
            "0752821818",
            "0751377935",
            "0746277555",
            "0733001560",
            "0722601380",
            "0232712811",
            "0374441232",
            "0727819386",
            "0749082076",
            "0768957297",
            "0771083170",
            "0749988141",
            "0754023277",
            "0720518511",
            "0735962331",
            "0760062544",
            "0723440992",
            "0744774466",
            "0722418159",
            "0767126111",
            "0735771735",
            "0740958441",
            "0765340591",
            "0746241088",
            "0230312552",
            "0746524263",
            "0753900678",
            "0737301030",
            "0736431400",
            "0723206128",
            "0751202020",
            "0723655550",
            "0766527774",
            "0749677407",
            "0755400259",
            "0740012545",
            "0721110010",
            "0244297391",
            "0754364284",
            "0723226789",
            "0728541145",
            "0741432695",
            "0748168027",
            "0728092822",
            "0231616782",
            "0746949861",
            "0728203683",
            "0740626434",
            "0752163012",
            "0767555555",
            "0766529423",
            "0740185066",
            "0765377999",
            "0721120999",
            "0745255881",
            "0722680520",
            "0753405672",
            "0724404047",
            "0740251383",
            "0241660459",
            "0748884124",
            "0726987077",
            "0724171171",
            "0766429118",
            "0762658259",
            "0722222509",
            "0741057673",
            "0720893330",
            "0765175637",
            "0723805300",
            "0755520001",
            "0756800700",
            "0767977777",
            "0721117722",
            "0741024880",
            "0746528926",
            "0745337157",
            "0767733444",
            "0258713212",
            "0746629910",
            "0751598351",
            "0785799898",
            "0762673517",
            "0261711190",
            "0743562127",
            "0732334227",
            "0745323258",
            "0724725263",
            "0720900790",
            "0744698924",
            "0765206010",
            "0727733265",
            "0753319192",
            "0721463444",
            "0751238573",
            "0741352454",
            "0766201079",
            "0722694154",
            "0722780434",
            "0765388888",
            "0721448281",
            "0766353646",
            "0767981575",
            "0732722122",
            "0744331577",
            "0765558288",
            "0762163163",
            "0766533567",
            "0740531937",
            "0762722014",
            "0746836958",
            "0765045279",
            "0784399755",
            "0753084441",
            "0756795252",
            "0746183448",
            "0760357088",
            "0740121662",
            "0722105050",
            "0741151151",
            "0753366699",
            "0752292222",
            "0756216486",
            "0765436100",
            "0726236393",
            "0767999545",
            "0770488380",
            "0726744718",
            "0723174219",
            "0768845477",
            "0724871121",
            "0755227882",
            "0741337285",
            "0742338949",
            "0746777558",
            "0741156871",
            "0743573407",
            "0745172578",
            "0342738150",
            "0761084044",
            "0743585575",
            "0736366222",
            "0735334874",
            "0740180590",
            "0724001015",
            "0721046302",
            "0722873217",
            "0728162594",
            "0766585988",
            "0766247765",
            "0721286479",
            "0763564056",
            "0722235816",
            "0785237780",
            "0742245555",
            "0725952465",
            "0722410206",
            "0760040063",
            "0740970087",
            "0723576623",
            "0771521873",
            "0766100179",
            "0751619133",
            "0765210183",
            "0722694020",
            "0768388819",
            "0745613557",
            "0720915191",
            "0768311900",
            "0241522954",
            "0721515842",
            "0745492961",
            "0744378747",
            "0733007353",
            "0730773797",
            "0740321512",
            "0749346154",
            "0755884292",
            "0745883688",
            "0749112819",
            "0769402022",
            "0740664411",
            "0765559552",
            "0740498838",
            "0723737896",
            "0784822288",
            "0725324670",
            "0785379755",
            "0767462065",
            "0730213195",
            "0749786054",
            "0733890390",
            "0720567123",
            "0729125520",
            "0765383244",
            "0746707610",
            "0744352289",
            "0728285771",
            "0728476998",
            "0784222287",
            "0743154878",
            "0763924429",
            "0742366112",
            "0721050589",
            "0737508040",
            "0756229541",
            "0753575456",
            "0722962227",
            "0752878808",
            "0760322213",
            "0728610628",
            "0740485044",
            "0757460736",
            "0723916076",
            "0770500500",
            "0769194707",
            "0724208724",
            "0765348048",
            "0729588888",
            "0763565957",
            "0745694727",
            "0755263390",
            "0744226020",
            "0758706267",
            "0753550422",
            "0724587753",
            "0743843254",
            "0745750375",
            "0724358393",
            "0740961548",
            "0751159169",
            "0770512609",
            "0748222501",
            "0721556556",
            "0745200819",
            "0747085096",
            "0740135397",
            "0765476890",
            "0731620008",
            "0744636962",
            "0757545251",
            "0766724777",
            "0266364393",
            "0722683310",
            "0241619005,0727188798",
            "0761321971",
            "0721855890",
            "0765485378",
            "0745876606",
            "0721315000",
            "0754688961",
            "0769838223",
            "0748867429",
            "0743405754",
            "0723887756",
            "0721991333",
            "0762309000",
            "0721366588",
            "0746932170",
            "0741591649",
            "0765428755",
            "0723564360",
            "0742411502",
            "0745797778",
            "0722807147",
            "0768901726",
            "0722715198",
            "0726456702",
            "0723672652",
            "0751569475",
            "0754332619",
            "0733403590",
            "0757503700",
            "0752810160",
            "0765438828",
            "0740826672",
            "0730290639",
            "0761548284",
            "0745198623",
            "0765113411",
            "0746251914",
            "0767820371",
            "0788222221",
            "0735076202",
            "0762137589",
            "0761538866",
            "0760310000",
            "0751222204",
            "0765717708",
            "0740213392",
            "0721284821",
            "0348527342",
            "0760286746",
            "0743810009",
            "0765340083",
            "0356172319",
            "0732820980",
            "0729430491",
            "0213202160",
            "0725810881",
            "0757610536",
            "0722177749",
            "0741217580",
            "0751869101",
            "0726134847",
            "0751149350",
            "0744766331",
            "0741536220",
            "0235471140",
            "0769158289",
            "0265513670",
            "0788428841",
            "0722342907",
            "0763372404",
            "0769167087",
            "0747916500",
            "0216370069",
            "0767777778",
            "0745480102",
            "0721246112",
            "0749202153",
            "0769668234",
            "0763340247",
            "0727125080",
            "0754251507",
            "0722682698",
            "0755281234",
            "0748898998",
            "0768604020",
            "0744582618",
            "0722946829",
            "0723699990",
            "0741504712",
            "0723363628",
            "0232277780",
            "0767603602",
            "0765328712",
            "0731113224",
            "0721036056",
            "0766479371",
            "0763542517",
            "0723476033",
            "0766466901",
            "0757326246",
            "0745480997",
            "0722441822",
            "0768552286",
            "0756374711",
            "0754699577",
            "0722997656",
            "0742490055",
            "0743339320",
            "0746273262",
            "0727378378",
            "0758102108",
            "0749667744",
            "0742638949",
            "0724345111",
            "0769583073",
            "0357420697",
            "0745375037",
            "0755500202",
            "0741310383",
            "0740841214",
            "0755357372",
            "0756639606",
            "0761570441",
            "0727445338",
            "0727074951",
            "0784797379",
            "0765653440",
            "0766895895",
            "0745505468",
            "0743231789",
            "0744292759",
            "0723626045",
            "0769696230",
            "0742389137",
            "0760300330",
            "0745537518",
            "0724249360",
            "0721129767",
            "0743518604",
            "0213268204",
            "0726715507",
            "0722144544",
            "0723014135",
            "0741257357",
            "0726062026",
            "0765713088",
            "0765321389",
            "0767991661",
            "0756618356",
            "0729939927",
            "0765520280",
            "0765510783",
            "0744999309",
            "0740066636",
            "0751178351",
            "0746465127",
            "0740495306",
            "0766861515",
            "0755597460",
            "0769259897",
            "0745451303",
            "0758837495",
            "0765391760",
            "0731872357",
            "0723751642",
            "0760905677",
            "0730683919",
            "0748310457",
            "0727010763",
            "0742472985",
            "0722638322",
            "0740389914",
            "0732514540",
            "0755857236",
            "0741211342",
            "0761462477",
            "0744921530",
            "0745146969",
            "0765115811",
            "0748977083",
            "0762726022",
            "0726159889",
            "0728283441",
            "0740438298",
            "0745057571",
            "0749515681",
            "0721270240",
            "0722211900",
            "0744357528",
            "0752005378",
            "0764476956",
            "0746542111",
            "0752624462",
            "0724631290",
            "0753567065",
            "0740585280",
            "0761888826",
            "0744640386",
            "0765568917",
            "0740124115",
            "0766806078",
            "0745879735",
            "0735044044",
            "0724710605",
            "0760906104",
            "0731016825",
            "0721800091",
            "0767736501",
            "0722512280",
            "0736464009",
            "0765408610",
            "0721317656",
            "0763148767",
            "0765328925",
            "0721504219",
            "0752225134",
            "0723890075",
            "0721378571",
            "0766446282",
            "0758932346",
            "0761448879",
            "0753552183",
            "0758519975",
            "0744888868",
            "0731500600",
            "0740643458",
            "0740541887",
            "0756846669",
            "0761393939",
            "0727188798",
            "0723906616",
            "0723622041",
            "0760300010",
            "0767777777",
            "0742321164",
            "0741499670",
            "0722899286",
            "0764382999",
            "0745248357",
            "0726367040",
            "0749637527",
            "0741343102",
            "0737686515",
            "0720103307",
            "0733186254",
            "0740204050",
            "0723272438",
            "0726050600",
            "0753685757",
            "0756997788",
            "0721959089",
            "0729126916",
            "0747398610",
            "0749504858",
            "0723638369",
            "0740015519",
            "0747323718",
            "0732984359",
            "0744765709",
            "0747859697",
            "0741770313",
            "0763618822",
            "0737690800",
            "0745007722",
            "0746485385",
            "0721208204",
            "0728070624",
            "0722178283",
            "0723605711",
            "0756254274",
            "0767840352",
            "0766426942",
            "0762605045",
            "0768704492",
            "0747374268",
            "0765383180",
            "0722339730",
            "0723685767",
            "0744898949",
            "0751020718",
            "0765464082",
            "0767121416",
            "0722557245",
            "0753547599",
            "0728271276",
            "0785810489",
            "0740941326",
            "0741070558",
            "0740012563",
            "0753578075",
            "0722523539",
            "0727862892",
            "0766702721",
            "0764476797",
            "0723298974",
            "0731909057",
            "0761900076",
            "0728060211",
            "0766203838",
            "0758132968",
            "0744473603",
            "0746603220",
            "0752552605",
            "0727999099",
            "0751213110",
            "0722986578",
            "0745839902",
            "0726123279",
            "0744983714",
            "0741908175",
            "0761422500",
            "0769849093",
            "0767445511",
            "0772089602",
            "0742570079",
            "0755267171",
            "0727213258",
            "0742705323",
            "0749100704",
            "0761136393",
            "0742625390",
            "0762779026",
            "0766062929",
            "0755687510",
            "0741132061",
            "0766453089",
            "0730998090",
            "0744753175",
            "0735988200",
            "0723281623",
            "0746857429",
            "0737306333",
            "0724804052",
            "0727926213",
            "0766141098",
            "0757206900",
            "0753457454",
            "0768015800",
            "0734512324",
            "0728811446",
            "0265250509",
            "0762958303",
            "0784465575",
            "0213320104",
            "0746408888",
            "0763349966",
            "0758037058",
            "0765967245",
            "0745628628",
            "0746507409",
            "0766920389",
            "0740134247",
            "0766251271",
            "0737444677",
            "0734880288",
            "0741427364",
            "0741621096",
            "0230235255",
            "0763329264",
            "0744439901",
            "0765324820",
            "0722624879",
            "0764409960",
            "0768010616",
            "0749369901",
            "0726916478",
            "0752961012",
            "0727007263",
            "0724577644",
            "0745425455",
            "0740586152",
            "0724877734",
            "0760055546",
            "0722415522",
            "0745205353",
            "0723111111",
            "0767016222",
            "0745770479",
            "0745435656",
            "0767067327",
            "0765445313",
            "723112559",
            "0744370169",
            "0742697629",
            "0725131132",
            "0748596879",
            "0743335858",
            "0784098766",
            "0724109999",
            "0722434806",
            "0758868382",
            "0766711116",
            "0771495295",
            "0744448588",
            "0723149042",
            "0746157051",
            "0749489766",
            "0742488862",
            "0765232380",
            "0761101399",
            "0742079297",
            "0765646154",
            "0753363136",
            "0744138323",
            "0742453943",
            "0742027016",
            "0769789869",
            "0763883883",
            "0752403797",
            "0745879915",
            "0762422217",
            "0765144441",
            "0746063566",
            "0765276987",
            "0744399469",
            "0769456256",
            "0752375678",
            "0748530405",
            "0722515202",
            "0745755949",
            "0761097550",
            "0765354290",
            "0765163465",
            "0724390688",
            "0742018745",
            "0726327333",
            "0742145042",
            "0725011034",
            "0720430585",
            "0760556767",
            "0720603808",
            "0757172200",
            "0755379086",
            "0765351148",
            "0749526652",
            "0754678125",
            "0769610551",
            "0767617797",
            "0742074935",
            "0723517151",
            "0731616769",
            "0741025228",
            "0740541802",
            "0753848440",
            "0765114113",
            "0747842236",
            "0733087282",
            "0756417640",
            "0773838414",
            "0720720814",
            "0721738888",
            "0753436895",
            "0745403237",
            "0726933769",
            "0749278806",
            "0765438828",
            "0729631419",
            "0720611661",
            "0744709709",
            "0722792579",
            "0749387544",
            "0743647464",
            "0734854560",
            "0752134319",
            "0742617607",
            "0724001014",
            "0761529023",
            "0721595555",
            "0748508605",
            "0757595656",
            "0755057003",
            "0735169217",
            "0752945646",
            "0742800458",
            "0744123547",
            "0767103299",
            "0762980665",
            "0757078989",
            "0772238042",
            "0758544792",
            "0741018704",
            "0745200777",
            "0743756931",
            "0767358687",
            "0748824301",
            "0744205517",
            "0744898127",
            "0767373637",
            "0743888338",
            "0723368350",
            "0740607989",
            "0762051036",
            "0732989274",
            "0741978933",
            "0731020797",
            "0766665616",
            "0753222122",
            "0748521807",
            "0749146146",
            "0764801231",
            "0735220494",
            "0726881251",
            "0726399786",
            "0751228292",
            "0745063221",
            "0761328013",
            "0757584846",
            "0755779998",
            "0727060666",
            "0748826705",
            "0724341891",
            "0744659152",
            "0740474139",
            "0754263859",
            "0746754542",
            "0726665118",
            "0724533970",
            "0723614771",
            "0745103816",
            "0766221756",
            "0743086505",
            "0731910931",
            "0757013225",
            "0721706154",
            "0763209859",
            "0722638557",
            "0752947185",
            "0747673438",
            "0757371450",
            "0764200033",
            "0758264910",
            "0726494727",
            "0767333555",
            "0241643960",
            "0756555475",
            "0744442807",
            "0751431457",
            "0755349115",
            "0741587044",
            "0748485615",
            "0742604287",
            "0725586874",
            "0727248331",
            "0773917358",
            "0757777477",
            "0757542545",
            "0733379999",
            "0721986600",
            "0763532205",
            "0744246439",
            "0785989500",
            "0749229735",
            "0752272995",
            "0785614557",
            "0754867368",
            "0745500680",
            "0748647049",
            "0754908823",
            "0751338090",
            "0748190696",
            "0769953269",
            "0749726265",
            "0761889212",
            "0745220145",
            "0724036320",
            "0744780097",
            "0741340960",
            "0746037989",
            "0751778252",
            "0726238744",
            "0770451197",
            "0744672027",
            "0745358872",
            "0755059999",
            "0724988888",
            "0743627535",
            "0745766595",
            "0745323366",
            "0765272506",
            "0771176878",
            "0722279922",
            "0753659056",
            "0722935556",
            "0722445879",
            "0765964251",
            "0746081135",
            "0751391366",
            "0754635846",
            "0760390702",
            "0748436277",
            "0746076600",
            "0761003333",
            "0725881137",
            "0740480785",
            "0735853874",
            "0743409830",
            "0744698581",
            "0721807976",
            "0720635637",
            "0753933485",
            "0752961012",
            "0721800091",
            "0733998998",
            "0753402701",
            "0762920033",
            "0752207112",
            "0742644148",
            "0723085263",
            "0720779044",
            "0740300243",
            "0726227739",
            "0763639820",
            "0757172200",
            "0722743275",
            "0723445989",
            "0747378906",
            "0740508247",
            "0748030303",
            "0748811790",
            "0767536236",
            "0735046498",
            "0730299273",
            "0772047078",
            "0765232380",
            "0763793736",
            "0765790963",
            "0757965112",
            "0751549720",
            "0724622576",
            "0761020202",
            "0744857840",
            "0726767902",
            "0724515551",
            "0731457455",
            "0737957308",
            "0728000889",
            "0722617726",
            "0727588405",
            "0745593692",
            "0755647188",
            "0727162800",
            "0733642236",
            "+40767241414",
            "0763384569",
            "0727156050",
            "0721991333",
            "+40744357112",
            "0749389772",
            "0737004464",
            "0721830771",
            "0760340340",
            "0741741095",
            "0721915555",
            "0723829273",
            "0754480285",
            "0768462713",
            "0744849922",
            "0760 474 760",
            "0725222728",
            "0741619999",
            "0745055674",
            "0234311525",
            "0740840070",
            "0727477205",
            "0741111170",
            "0767710501",
            "0747407682",
            "0730929273",
            "0744664174",
            "0733379999",
            "0760609774",
            "0728282332",
            "0765477838",
            "0726020182",
            "0767666606",
            "0721772995",
            "0725622513",
            "0786626937",
            "0722201699",
            "0765956354",
            "0729796001",
            "0735413327",
            "0759062697",
            "0728533672",
            "0744504101",
            "0758613676",
            "0752635111",
            "0768449299",
            "0748563444",
            "0763308509",
            "0741275668",
            "0758862048",
            "0763139958",
            "0730844840",
            "0753098097",
            "0755192233",
            "0765713088",
            "0744565416",
            "0733538538",
            "0726659295",
            "0746408888",
            "0727230387",
            "0722597982",
            "0758115522",
            "0743400700",
            "0737943718",
            "0769889748",
            "0732349931",
            "0767727533",
            "0741489189",
            "0735665666",
            "0721856110",
            "0721813539",
            "0721363024",
            "0744582223",
            "0731089009",
            "0730081062",
            "0752750837",
            "0744472000",
            "0745282131",
            "0769822097",
            "0743574216",
            "0754810778",
            "0756915414",
            "0737126197",
            "0743221222",
            "0748561625",
            "0769779813",
            "0747630852",
            "0757836832",
            "0769878679",
            "0756840914",
            "0755709592",
            "0764476956",
            "0756569510",
            "0769978673",
            "0799575097",
            "0770545651",
            "0755701386",
            "0764803101",
            "0763759771",
            "0742018745",
            "0727905500",
            "0762998228",
            "0755160160",
            "0737395555",
            "0765510783",
            "0747387655",
            "0757218167",
            "0758535569",
            "0755275232",
            "0723400741",
            "0749848451",
            "0749679962",
            "0785848140",
            "0760322340",
            "0731472718",
            "0740050977",
            "0768650533",
            "0721125345",
            "0785114206",
            "0787808988",
            "0766485778",
            "0737827777",
            "0768885555",
            "0730194382",
            "0767606667",
            "0729631419",
            "0734534114",
            "0754337631",
            "0720811331",
            "0736667720",
            "0725227289",
            "0721952590",
            "0735185519",
            "0744339088",
            "0772166597",
            "0748384398",
            "0756496280",
            "0742765773",
            "0740786729",
            "0766965881",
            "0748366118",
            "0765211436",
            "0736347442",
            "0735872295",
            "0745198623",
            "0773971819",
            "0721336504",
            "004917638888001",
            "0754696666",
            "0725015686",
            "0736162301",
            "0765167592",
            "0742503019",
            "0767226921",
            "0753524997",
            "0728714208",
            "0722267755",
            "0764792133",
            "0735478143",
            "0723132455",
            "0769264414",
            "0769749058",
            "0736848284",
            "0721647281",
            "+40724230928",
            "0728700072",
            "0733333342",
            "0751430491",
            "0733302301",
            "0754262911",
            "0743565948",
            "0765453545",
            "0735828999",
            "0749667744",
            "0723284773",
            "0752009991",
            "0784543445",
            "0767495771",
            "0735781595",
            "0765355014",
            "0744582004",
            "0701222858",
            "0701291357",
            "0730855888",
            "0774467649",
        ];
        
        foreach ($list as $ph) {
            if ($SMS->send('Prinde ultimele 2 zile de CONCURSURI si REDUCERI EPICE de la GSMnet.ro! Acceseaza site-ul si serbeaza 10 ani alaturi de noi: http://bit.ly/2tDxydS', $ph)) {
                $this->out('Trimis la ' . $ph);
            } else {
                $this->out('Eroare la trimiterea catre ' . $ph);
            }
        }
    }
}
