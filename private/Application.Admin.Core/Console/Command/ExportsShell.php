<?php
App::uses('Shell', 'Console');
App::uses('HttpSocket', 'Network/Http');
App::uses('CakeEmail', 'Network/Email');

/**
 * Class ExportsShell
 *
 * @property Product Product
 * @property Export Export
 */
class ExportsShell extends AppShell
{

    public $uses = [
        'Export',
        'Catalog.Product',
        'Customers.User',
        'Catalog.ProductAliasName',
        'Catalog.Category',
        'Catalog.Characteristic',
        'Catalog.CharacteristicValue',
        'Catalog.CharacteristicValuesProduct',
    ];

    public function run() {

        $exports = $this->Export->find('all', [
            'conditions' => [
                'is_started' => false,
            ]
        ]);

        $this->Export->updateAll(['is_started' => true], ['is_started' => false,]);
        foreach ($exports as $export) {
            switch ($export['Export']['export_model']) {
                case 'Characteristic': {
                    $data = $this->exportChars($export['Export']['extra']);
                    break;
                }
                default: return;
            }

            $conclusion = [
                'id' => $export['Export']['id'],
                'is_ended' => true,
                'export_data' => $data,
                'ended_at' => date('Y-m-d H:i:s'),
            ];
            $name = time() . '__tmpex';
            $h = fopen(TMP. $name, 'w');
            fwrite($h, $data);
            fclose($h);
            $this->User->id = $export['Export']['user_id'];
            $email = $this->User->field('email');
            App::uses('CakeEmail', 'Network/Email');
            $em = new CakeEmail('default');
            $em->to($email);
            $em->subject(__('Export #%s - %s', [$export['Export']['id'], Configure::read('STORE_NAME')]));
            $em->attachments(
                array(
                    $name . '.csv' => array(
                        'file' => TMP . $name,
                        'mimetype' => 'text/csv',
                    )
                )
            );
            $em->send();
            unlink(TMP . $name);
            $this->Export->save($conclusion);
        }
    }

    public function exportChars($category_id) {

        $cache_validator = rand(1,9999);
        $headers = $this->Product->query("SELECT
            c.id, c.name, GROUP_CONCAT(ch.name order by ch.order asc separator ';') as header
            from catalog__characteristics ch
            left join catalog__categories c on c.id = ch.category_id
            where c.id = $category_id and $cache_validator = $cache_validator
            group by c.id
            order by ch.order
            limit 1;
        ");

        if (!isset($headers[0][0]['header']) || empty($headers[0][0]['header'])) {
            return __('Categoria #%s nu exista', $category_id);
        }

        $data = '';
        $data .= 'Product.id;' . $headers['0']['0']['header'];
        foreach ($headers as $h) {
            if ($h['c']['id']) {
                $this->out('Incep exportul caracteristicilor categoriei ' . alias($h['c']['name']));

                $list_of_chars = explode(';', $h['0']['header']);
                $products = $this->ProductAliasName->find('list', [
                    'conditions' => [
                        'category_id' => $h['c']['id'],
                        'is_main' => 1,
                        'is_active' => 1
                    ],
                    'fields' => [
                        'product_id'
                    ],
                    'order' => ['product_id' => 'asc']
                ]);
                foreach ($products as $panid => $pid) {
                    $values = [];
                    foreach ($list_of_chars as $chname) {
                        $value = $this->Product->query("SELECT GROUP_CONCAt(DISTINCT case when ch.type = 'text' then cvp.text_value else cv.value end) as val
from catalog__product_alias_names pan
left join catalog__products p on p.id = pan.product_id
left join catalog__characteristic_values_products cvp on cvp.product_id = p.id
left join catalog__characteristic_values cv on cv.id = cvp.characteristic_value_id
left join catalog__characteristics ch on cv.characteristic_id = ch.id
where cvp.product_id = {$pid} and ch.name='{$chname}' and pan.is_active=1
GROUP BY p.id;");

                        if (isset($value['0'][0]['val']) && !empty($value['0'][0]['val'])) {
                            $values[] = $value['0'][0]['val'];
                        } else {
                            $values[] = '';
                        }

                    }
                    if (!empty($values)) {
                        array_unshift($values, $panid);
                        $data .= "\n" . implode(';', $values);
                    }
                }
                return $data;
            }
        }
    }

    public function ri() {
        $list = [
            272262,
            272258,
            272081,
            272261,
            272091,
            272080,
            272259,
            272054,
            272307,
            272304,
            272303,
            272285,
            272309,
            272065,
            272103,
            272205,
            272306,
            272266,
            275032,
            274969,
            275021,
            275002,
            274970,
            275009,
            274985,
            274980,
            275062,
            275605,
            265435,
            275717,
            275718,
            275742,
            275757,
            275732,
            275752,
            275740,
            275726,
            275724,
            275722,
            275399,
            275759,
            275620,
            275760,
            275833,
            281990,
            282177,
            282194,
            282193,
            286623,
            286447,
            284506,
            286641,
            286120,
            315948,
            325626,
            342795,
            343364,
            344770,
            346912,
            348008,
            348063,
            350256,
            350368,
            352512,
            354543,
            351623,
            281378,
            357533,
            357494,
            357637,
            358518,
            359837,
            359698,
            360102,
            360609,
            360755,
            363266,
            362776,
            364387,
            365151,
            369259,
            369709,
            371302,
            372506,
            376451,
            377496,
            381230,
            281378,
            383598,
            387083,
            387083,
            389946,
            389956,
            393867,
            398389,
            398982,
            401328,
            403071,
            404237,
        ];
        foreach ($list as $oid) {
            echo file_get_contents('http://backoffice.gsmnet.ro/orders/sales?ZPinabgly6SxKvuX0n9aJThIsUdeXckjJkukjggs1ttMFjGPyFjIKOzoNiDDZ4H&regenerate=' . $oid) . "\n";
            sleep(1);
        }
    }

}



/*
 * define('WEB', dirname(dirname(dirname(dirname(dirname(__FILE__))))) . DS . 'web' . DS);

        if (!is_dir(WEB . 'chars')) {
            mkdir(WEB . 'chars', 0755);
        }
        foreach ($headers as $h) {
            if ($h['c']['id']) {
                $this->out('Incep crearea fisierului ' . alias($h['c']['name']) . '.csv');
                $start = microtime(true);
                $res = fopen(WEB . 'chars' . DS . alias($h['c']['name']) . '.csv', 'w');
                fwrite($res,'Cod produs;' . $h['0']['header'] . "\n");
                $list_of_chars = explode(';', $h['0']['header']);
                $products = $this->ProductAliasName->find('list', [
                    'conditions' => [
                        'category_id' => $h['c']['id'],
                        'is_main' => 1
                    ],
                    'fields' => [
                        'product_id'
                    ],
                    'order' => ['product_id' => 'asc']
                ]);
                foreach ($products as $panid => $pid) {
                    $values = [];
                    foreach ($list_of_chars as $chname) {
                        $value = $this->Product->query("SELECT GROUP_CONCAt(DISTINCT case when ch.type = 'text' then cvp.text_value else cv.value end) as val
from catalog__product_alias_names pan
left join catalog__products p on p.id = pan.product_id
left join catalog__characteristic_values_products cvp on cvp.product_id = p.id
left join catalog__characteristic_values cv on cv.id = cvp.characteristic_value_id
left join catalog__characteristics ch on cv.characteristic_id = ch.id
left join catalog__categories_characteristics cc on cc.characteristic_id = cv.characteristic_id
left
where cvp.product_id = {$pid} and ch.name='{$chname}'
GROUP BY p.id;");

                        if (isset($value['0'][0]['val']) && !empty($value['0'][0]['val'])) {
                            $values[] = $value['0'][0]['val'];
                        }
                    }
                    if (!empty($values)) {
                        array_unshift($values, $panid);
                        fputcsv($res, $values, ';');
                        echo "Adaugat $pid\r";
                    }
                }
                $this->out(alias($h['c']['name']) . '.csv creat in ' . round(microtime(true) - $start,2) . ' ms');
            }
        }
 */