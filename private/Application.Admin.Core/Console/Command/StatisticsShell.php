<?php
App::uses('Shell', 'Console');
App::uses('HttpSocket', 'Network/Http');
App::uses('CakeEmail', 'Network/Email');

/**
 * Class ExportsShell
 *
 * @property Product Product
 * @property Export Export
 * @property StSale StSale
 * @property StSaleProduct StSaleProduct
 * @property EntryProduct EntryProduct
 * @property EntryProductFlow EntryProductFlow
 * @property SaleProduct SaleProduct
 * @property Sale Sale
 */
class StatisticsShell extends AppShell
{
    
    public $uses = [
        'Catalog.Product',
        'Customers.User',
        'Catalog.ProductAliasName',
        'Catalog.Category',
        'Orders.Sale',
        'Orders.SaleProduct',
        'Statistics.StSale',
        'Statistics.StSaleProduct',
        'Merchandise.EntryProduct',
        'Merchandise.EntryProductFlow',
    ];
    
    public function writeDailyStatistics() {
        $day = '2015-04-01';
        $day_start = $this->EntryProduct->query('select max(day) from statistics__st_sales');
        if ($day_start[0][0]['max(day)']) {
            $day = $day_start[0][0]['max(day)'];
        }
        while (strtotime($day) < time()) {
            $this->dailySalesStatistics($day);
            $day = date('Y-m-d', strtotime($day) + 86400);
        }
    }
    
    public function dailySalesStatistics($day = null) {
        $start = microtime(true);
        if (is_null($day)) {
            $day = '2016-11-18';
        }
        $day_start = date('Y-m-d H:i:s', strtotime($day));
        $day_end = date('Y-m-d H:i:s', strtotime($day) + 86399);
        
        $this_day_channels = $this->Sale->find('list', [
            'conditions' => [
                'Sale.sale_status_id' => 11,
                'Sale.modified_at >=' => $day_start,
                'Sale.modified_at <=' => $day_end,
            ],
            'joins' => [
                [
                    'table' => 'orders__sale_products',
                    'alias' => 'SaleProduct',
                    'type' => 'LEFT',
                    'conditions' => [
                        'SaleProduct.sale_id = Sale.id',
                    ],
                ],
                [
                    'table' => 'catalog__product_alias_names',
                    'alias' => 'ProductAliasName',
                    'type' => 'LEFT',
                    'conditions' => [
                        'ProductAliasName.id = SaleProduct.product_alias_name_id',
                    ],
                ],
            ],
            'fields' => [
                'source',
            ],
            'group' => [
                'source'
            ]
        ]);
        $k = 0;
        $Y = date('Y', strtotime($day));
        $m = date('m', strtotime($day));
        $d = date('d', strtotime($day));
        $analytics_1_raw = $this->EntryProduct->query('CALL analytics_total_day(?,?,?)', [$Y, $m, $d]);
        $save_array = [];
        foreach ($this_day_channels as $channel) {
            $outs_value_raw = $this->EntryProduct->query("SELECT SUM(price) FROM  merchandise__entry_products ep
		LEFT JOIN merchandise__entry_product_flows epf ON epf.entry_product_id = ep.id 
		left join orders__sales s on s.id = epf.sale_id
			WHERE (epf.type = 'SALE') AND epf.created_at < ? AND epf.created_at > ? and s.source = ?;", [$day_end, $day_start, $channel]);
            
            
            $sales_value_raw = $this->EntryProduct->query("select sum(s.total_value), count(s.id) from orders__sales s where
created_at < ? AND created_at > ? and source = ? and sale_status_id=11;", [$day_end, $day_start, $channel]);
            
            
            $invoices_value_raw = $this->EntryProduct->query("select sum(i.value) from orders__invoices i 
left join orders__sales s on s.id = i.sale_id
where
i.created_at < ? AND i.created_at > ? and s.source = ?;", [$day_end, $day_start, $channel]);
    
            $sales_value_resellers_raw = $this->EntryProduct->query("select sum(s.total_value), count(s.id) from orders__sales s
left join customers__users u on u.id = s.user_id
where u.user_type_id is not null and
s.created_at < ? AND s.created_at > ? and source = ? and s.sale_status_id=11;", [$day_end, $day_start, $channel]);
            $sales_value_customers_raw = $this->EntryProduct->query("select sum(s.total_value), count(s.id) from orders__sales s
left join customers__users u on u.id = s.user_id
where u.user_type_id is null and
s.created_at < ? AND s.created_at > ? and source = ? and sale_status_id=11;", [$day_end, $day_start, $channel]);
            $resellers_no_raw = $this->EntryProduct->query('SELECT count(id) from customers__users where user_type_id is not null;');
            $new_customers_no_raw = $this->EntryProduct->query('SELECT count(id) from customers__users where created_at > ? and created_at < ?', [$day_start, $day_end]);
            $save_array[$k++] = [
                'day' => date('Y-m-d', strtotime($day)),
                'channel' => $channel,
                'stock_value_start' => $analytics_1_raw[0][0]['@fd_total'],
                'stock_value_end' => $analytics_1_raw[0][0]['@ld_total'],
                'entries_value' => $analytics_1_raw[0][0]['@entries_total'],
                'outs_value' => $outs_value_raw[0][0]['SUM(price)'],
                'sales_value' => $sales_value_raw[0][0]['sum(s.total_value)'],
                'invoices_value' => $invoices_value_raw[0][0]['sum(i.value)'],
                'sales_no' => $sales_value_raw[0][0]['count(s.id)'],
                'reseller_sales_no' => $sales_value_resellers_raw[0][0]['count(s.id)'],
                'customers_sales_no' => $sales_value_customers_raw[0][0]['count(s.id)'],
                'reseller_sales_value' => $sales_value_customers_raw[0][0]['sum(s.total_value)'],
                'customers_sales_value' => $sales_value_customers_raw[0][0]['sum(s.total_value)'],
                'resellers_no' => $resellers_no_raw[0][0]['count(id)'],
                'new_customers_no' => $new_customers_no_raw[0][0]['count(id)'],
            ];
        }
        if($save_array) {
            $this->StSale->saveMany($save_array);
            $executed_in = number_format(round((microtime(true) - $start), 2), 2);
            $this->out($day . ' statistics saved in ' . $executed_in . 's');
        } else {
            $executed_in = number_format(round((microtime(true) - $start), 2), 2);
            $this->out($day . ' statistics saved in ' . $executed_in . 's');
        }
        
    }
    
    public function dailyProductsStatistics() {
        $start = microtime(true);
        $from_time = $this->StSaleProduct->field('MAX(created_at)', []);
        //$from_time = date('Y-m-d H:i:s', strtotime('-1 month'));
        //$from_time = '2015-04-01 00:00:00';
        $transaction = $this->StSaleProduct->getDataSource();
        //$transaction->begin();
        $this->out('Start calculating statistics since ' . $from_time);
        try {
            $this->StSaleProduct->query('call ___st_get_average_sales(?);', [$from_time]);
        } catch (Exception $err) {
            $this->out('Error calculating statistics: ' . $err->getMessage());
            //$transaction->rollback();
            die();
        }
        //$transaction->commit();
        $end =  microtime(true);
        $this->out('Script executed in ' . ($end - $start) . 'seconds');
    }
    
    public function monthlySalesStatistics() {
        
    }
    
    public function monthlyProductsStatistics() {
        
    }
    
}