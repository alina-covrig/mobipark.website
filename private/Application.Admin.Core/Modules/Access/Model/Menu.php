<?php
/**
* Menu.php
*
* This file contains the Menu model
*
* PHP version 5.4
*
* @category  Admin
* @package   Access
* @author    Lucian Vasile <lucian.vasile@live.com>
* @copyright 2014 Evercloud
* @license   http://www.php.net/license/3_01.txt  PHP License 3.01
* @version   SVN: $Id$
* @link      /modules/access/models/menu
* @since     1.0
*/

App::uses('AccessAppModel', 'Access.Model');


/**
* Menu Model Class
*
* Inside this Model any application-wide
* data manipulation methods will be placed
*
* @category  Admin
* @package   Access
* @author    Lucian Vasile <lucian.vasile@live.com>
* @copyright 2014 Evercloud
* @license   http://www.php.net/license/3_01.txt  PHP License 3.01
* @version   Release: 1.0
* @link      /modules/access/models/menu
* @since     Class available since Release 1.0
*/
class Menu extends AccessAppModel
{
    /// properties {{{
    
    /**
    * Validation rules
    *
    * @var array
    */
    public $validate = array(
        'name' => array(
            'length' => array(
                'rule' => array('maxLength', 20),
                'required' => true,
                'message' => 'Maxim 20 de caractere'
            ),
        ),
    );
    
    /**
    * BelongsTo association models
    * 
    * @var array
    */
    public $belongsTo = array('Access.Action');
    
    /**
    * Behaviors attached to this model
    * 
    * @var array
    */
    public $actsAs = array('Tree');
    
    /// }}}
    
    /**
    * The beforeSave callback method
    * 
    * @param array $options the options for the save query
    * 
    * @return boolean
    */
    public function beforeSave($options = array()) 
    {
        parent::beforeSave($options);
        return true;
    }
    
    /**
    * The afterSave callback method
    * 
    * @param boolean $created true if INSERT false if UPDATE
    * @param array   $options the array passed to the query
    * 
    * @return void
    */
    public function afterSave($created, $options = array()) 
    {
        Cache::delete('Application.Menu');
    }
    
    /**
    * The afterDelete callback method
    * 
    * @return void
    */
    public function afterDelete() 
    {
        Cache::delete('Application.Menu');
    }
}
