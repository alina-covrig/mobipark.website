<?php
/**
* Permission.php
*
* This file contains the Permission model
*
* PHP version 5.4
*
* @category  Admin
* @package   Access
* @author    Badeci Gabriel <lucian.badeci@gmail.com>
* @copyright 2014 Evercloud
* @license   http://www.php.net/license/3_01.txt  PHP License 3.01
* @version   SVN: $Id$
* @link      /modules/access/models/permission
* @since     1.0
*/

App::uses('AccessAppModel', 'Access.Model');


/**
* Permission Class
*
* Inside this Permission any application-wide
* data manipulation methods will be placed
*
* @category  Admin
* @package   Access
* @author    Badeci Gabriel <lucian.badeci@gmail.com>
* @copyright 2014 Evercloud
* @license   http://www.php.net/license/3_01.txt  PHP License 3.01
* @version   Release: 1.0
* @link      /modules/access/models/permission
* @since     Class available since Release 1.0
*/
class GroupPermission extends AccessAppModel
{
    
}
