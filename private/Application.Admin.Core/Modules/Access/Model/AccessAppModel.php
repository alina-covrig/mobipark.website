<?php
/**
* AccessAppModel.php
*
* Holds the AccessAppModel Model
*
* PHP version 5.4
*
* @category  Admin
* @package   Access
* @author    Lucian Vasile <lucian.vasile@live.com>
* @copyright 2014 Evercloud
* @license   http://www.php.net/license/3_01.txt  PHP License 3.01
* @version   SVN: $Id$
* @link      /modules/access/models/access-app-model
* @since     1.0
*/

App::uses('AppModel', 'Model');


/**
* AccessAppModel Class
*
* Inside this Model any application-wide
* data manipulation methods will be placed
*
* @category  Admin
* @package   Access
* @author    Lucian Vasile <lucian.vasile@live.com>
* @copyright 2014 Evercloud
* @license   http://www.php.net/license/3_01.txt  PHP License 3.01
* @version   Release: 1.0
* @link      /modules/access/models/access-app-model
* @since     Class available since Release 1.0
*/
class AccessAppModel extends AppModel
{
    // properties {{{
    /**
    * The table prefix used by the Access module
    * 
    * @var 
    */
    public $tablePrefix = 'access__';
    // }}}
}
