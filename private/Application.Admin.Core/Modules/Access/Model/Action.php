<?php
/**
* Action.php
*
* This file contains the Action model
*
* PHP version 5.4
*
* @category  Admin
* @package   Access
* @author    Lucian Vasile <lucian.vasile@live.com>
* @copyright 2014 Evercloud
* @license   http://www.php.net/license/3_01.txt  PHP License 3.01
* @version   SVN: $Id$
* @link      /modules/access/models/action
* @since     1.0
*/

App::uses('AccessAppModel', 'Access.Model');


/**
* Action Model Class
*
* Inside this Model any application-wide
* data manipulation methods will be placed
*
* @category  Admin
* @package   Access
* @author    Lucian Vasile <lucian.vasile@live.com>
* @copyright 2014 Evercloud
* @license   http://www.php.net/license/3_01.txt  PHP License 3.01
* @version   Release: 1.0
* @link      /modules/access/models/action
* @since     Class available since Release 1.0
*/
class Action extends AccessAppModel
{
    /// properties {{{
    
    /**
    * Display field
    *
    * @var string
    */
    public $displayField = 'path';
    
    /**
    * Validation rules
    *
    * @var array
    */
    public $validate = array(
        'path' => array(
            'unique' => array(
                'rule' => 'isUnique',
                'required' => true,
                'message' => 'Metoda este inregistrata!',
                'on' => 'create',
            ),
        ),
    );
    
    /// }}}
    
    /**
    * The Action/populate method
    * 
    * This method deletes unexistent actions and
    * saves current actions in the database
    * 
    * @return boolean
    */
    public function populate() 
    {
        $list = $this->_getPrivilegeList();
        // trebuie sterse cele care nu mai sunt din
        // actions, menus si permissions
        $non_existent_actions = $this->find(
            'list', array(
            'conditions' => array(
                'NOT' => $list
            ),
            'fields' => array('id'),
            )
        );
        $this->deleteAll(array('Action.id' => $non_existent_actions));
        return $this->saveMany($list, array('validate' => true));
    }
    
    /**
    * The _getPrivilegeList method
    * 
    * This method will scan the Modules/{ModuleName}/Controller
    * directory for any php files and get all the public methods
    * 
    * @return array
    */
    private function _getPrivilegeList()
    {
        // get the modules
        $modules_dir = scandir(APP . 'Modules');
        $actions = array();
        $k = 0;
        foreach ($modules_dir as $module) {
            if ($module != '.' 
                && $module != '..' 
                && is_dir(APP . 'Modules' . DS . $module)
            ) {
                $module_dir_location 
                    = APP 
                    . 'Modules' 
                    . DS 
                    . $module 
                    . DS 
                    . 'Controller';
                if (file_exists(
                    $module_dir_location 
                    . DS 
                    . $module 
                    . 'AppController.php'
                )) {
                    include_once
                        $module_dir_location 
                        . DS 
                        . $module 
                        . 'AppController.php'
                    ;
                    $module_dir = scandir($module_dir_location);
                    foreach ($module_dir as $controller) {
                        if ($controller != '.'
                            && $controller != '..'
                            && !is_dir($module_dir_location . DS . $controller)
                            && $controller != $module . 'AppController.php'
                        ) {
                            // Controllers list
                            $controller_actions 
                                = $this->_getMethods(
                                    $controller, $module_dir_location
                                );
                            foreach ($controller_actions as $action) {
                                $actions[$k]['module'] 
                                    = strtolower(Inflector::underscore($module));
                                $actions[$k]['controller'] 
                                    = strtolower(
                                        Inflector::underscore(
                                            str_replace(
                                                'Controller.php', 
                                                '', 
                                                $controller
                                            )
                                        )
                                    );
                                $actions[$k]['action'] = $action;
                                $path = $this->_makePath(
                                    $module, $controller, $action
                                );
                                $id = $this->field(
                                    'id', array(
                                    'Action.path' => $path
                                    )
                                );
                                if ($id) {
                                    $actions[$k]['id'] = $id;
                                }
                                $actions[$k++]['path'] = $path;
                            }
                            
                        }
                    }
                }
            }
        }
        return $actions;
    }
    
    /**
    * The _getMethods method
    * 
    * This method instantiates a class,
    * reads and returns its public methods
    * 
    * @param string $controller A controller filename
    * @param string $folder     A path where the controller resides
    * 
    * @return array
    */
    private function _getMethods($controller, $folder) 
    {
        $return_methods = array();
        $info = pathinfo($controller);
        include_once$folder . DS .  $controller;
        $currClass = new ReflectionClass($info['filename']);
        $methods = $currClass->getMethods(ReflectionMethod::IS_PUBLIC);
        foreach ($methods as $method) {
            if ($method->class == $info['filename']) {
                $return_methods[] = $method->name;
            }
        }
        return $return_methods;
    }
    
    /**
    * The _makePath method
    * 
    * This method returns a concatenated string
    * from the params that it recieves
    * 
    * It ignores the index action
    * 
    * @param string $module     The module name
    * @param string $controller The Controller filename
    * @param string $action     The action name
    * 
    * @return string
    */
    private function _makePath($module, $controller, $action) 
    {
        $path = '/';
        $path .= strtolower($module);
        $controller = Inflector::underscore(
            str_replace('Controller.php', '', $controller)
        );
        $path .= '/' . $controller;
        if ($action != 'index') {
            $path .= '/' . $action;
        }
        return $path;
    }
}
