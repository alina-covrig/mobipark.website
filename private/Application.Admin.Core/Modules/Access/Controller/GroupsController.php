<?php
/**
* GroupsController.php
*
* Inside this Controller admin logic methods will
* be placed
*
* PHP version 5.4
*
* @category  Admin
* @package   Access
* @author    Badeci Gabriel <lucian.badeci@gmail.com>
* @copyright 2015 Evercloud
* @license   http://www.php.net/license/3_01.txt  PHP License 3.01
* @version   SVN: $Id$
* @link      /access/groups
* @since     1.0
*/

App::uses('Access.AccessAppController', 'Controller');

/**
* GroupsController Class
*
* Inside this Controller admin logic methods will
* be placed
*
* @category  Admin
* @package   Access
* @author    Badeci Gabriel <lucian.badeci@gmail.com>
* @copyright 2015 Evercloud
* @license   http://www.php.net/license/3_01.txt  PHP License 3.01
* @version   Release: 1.0
* @link      /access/groups
* @since     Class available since Release 1.0     
*/
class GroupsController extends AccessAppController
{
    /**
    * Models used by this controller
    * 
    * @var array
    */
    public $uses = array(
        'Customers.User',
        'Customers.People',
        'Access.Action',
        'Access.Group',
        'Access.GroupPermission',
    );
    
    /**
    * The permissions/index method
    * 
    * List groups
    *
    * @return void
    */
    public function index()
    {
        if ($this->request->is('post')) {
            if ($this->Group->save($this->request->data)) {
                $this->Session->setFlash(
                    __('Grup Adaugat'),
                    'Alerts/success'
                );
                $id = $this->Group->id;
                $this->redirect('/access/groups/edit/'.$id);
            } else {
                $this->Session->setFlash(
                    __('Eroare la adaugare grupului!'),
                    'Alerts/error'
                );
            }
        }

        $this->User->virtualFields['full_name'] = 'get_user_full_name(User.id)';
        $admins = $this->User->find(
            'list', [
                'fields' => [
                    'User.id',
                    'User.full_name'
                ],
                'conditions' => [
                    'User.is_admin' => 1
                ],
                'order' => [
                    'User.full_name' => 'ASC'
                ]
            ]
        );
        $this->set('admins', $admins);

        $this->Group->bindModel(
            array(
                'hasMany' => array(
                    'User' => array(
                        'className' => 'Customers.User',
                        'foreignKey' => 'group_id',
                        'conditions' => array(
                            'User.is_admin' => 1
                        ),
                        'fields' => array(
                            'User.id',
                            'User.full_name'
                        ),
                        'order' =>  array(
                            'User.full_name' => 'ASC'
                        )
                    )
                )
            )
        );
        $groups = $this->Group->find(
            'all', array(
            'contain' => array(
                'User'
            ),
            'order' => ['Group.name' => 'ASC']
            )
        );

        $this->set('groups', $groups);
    }

    /**
     * The permissions/index method
     *
     * Edit groups
     *
     * @param integer $group_id the id of the group
     *
     * @return void
     */
    public function edit($group_id = null)
    {
        if (!is_null($group_id)) {
            $this->Group->id = $group_id;
            if (!$this->Group->exists()) {
                $this->Session->setFlash(
                    __('Grupa inexistenta'),
                    'Alerts/warning'
                );
                $this->redirect('/access/groups');
            }

            $actions_list = $this->Action->find(
                'list', array(
                    'fields' => array(
                        'Action.id',
                        'Action.path',
                        'Action.module',
                    ),
                    'order' => ['Action.path' => 'ASC']
                )
            );

            $group = $this->Group->findById($group_id);

            $this->GroupPermission->virtualFields = array(
                'group_action'
                => 'CONCAT(GroupPermission.group_id, "_", GroupPermission.action_id)'
            );
            $options['conditions'] = array(
                'GroupPermission.group_id' => $group_id
            );
            $options['fields'] = array(
                'group_action',
                'id'
            );

            $permissions = $this->GroupPermission->find('list', $options);
            $this->set(compact('actions_list', 'group', 'permissions'));
        }
    }

    /**
     * The permissions/save  method
     *
     * save groups pers
     *
     * @param integer $id the id of the group
     *
     * @return void
     */
    public function save() 
    {
        if ($this->request->is('post')) {
            // golim permisiunile curente
            $group_id = $this->request->data[0]['group_id'];
            $conditions = array(
                'GroupPermission.group_id' => $group_id
            );
            // unset la alea care nu au bifata actiunea
            foreach ($this->request->data as $k => $p) {
                if (!isset($p['action_id'])) {
                    unset($this->request->data[$k]);
                }
            }
            $this->GroupPermission->deleteAll($conditions);
            if ($this->GroupPermission->saveMany($this->request->data)) {
                // request users login
                $users_to_request = $this->User->find(
                    'all', array(
                    'conditions' => array(
                        'User.group_id' => $group_id
                    )
                    )
                );
                foreach ($users_to_request as $user) {
                    $this->User->requestLogin($user['User']['id']);
                }

                $this->Session->setFlash(
                    __('Permisiuni la grup modificate!'),
                    'Alerts/success'
                );
            } else {
                $this->Session->setFlash(
                    __('Eroare la salvare permisiunilor!'),
                    'Alerts/error'
                );
            }
            $this->redirect('/access/groups');
        }
    }

    /**
     * The permissions/edituser  method
     *
     * edituser groups users
     *
     * @param $user_id $id the id of the user
     *
     * @return void
     */
    public function edituser($user_id = null)
    {
        if ($this->request->is('post')) {
            $this->User->id = $this->request->data['User']['user_id'];
            if (!$this->User->exists()) {
                $this->Session->setFlash(
                    __('Usernul nu exista sau date inconsistente'),
                    'Alerts/warning'
                );
                $this->redirect('/access/groups');
            } else {
                $this->User->validate = array();
                $this->User->id = $this->request->data['User']['user_id'];
                $this->User->set('group_id', $this->request->data['User']['group_id']);
                $this->User->save(false);

                $this->Session->setFlash(
                    __('Modificari salvate!'),
                    'Alerts/success'
                );
                $this->redirect('/access/groups');
            }
        } else {
            $this->Session->setFlash(
                __('Date inconsistente!'),
                'Alerts/error'
            );
            $this->redirect('/access/groups');
        }
    }

    /**
     * The permissions/delete  method
     *
     * delete groups
     *
     * @param integer $id the id of the group
     *
     * @return void
     */
    public function delete($group_id = null)
    {
        if ($group_id!=null) {
            $this->Group->id = $group_id;
            if (!$this->Group->exists()) {
                $this->Session->setFlash(
                    __('Grupul nu exista'),
                    'Alerts/warning'
                );
                $this->redirect('/access/groups');
            } else {

                $transaction = $this->Group->getDataSource();
                $transaction->begin();

                $users_to_request = $this->User->find(
                    'all', array(
                    'conditions' => array(
                        'User.group_id' => $group_id
                    )
                    )
                );

                // remove group permissions
                $conditions = array(
                    'GroupPermission.group_id' => $group_id
                );
                if (!$this->GroupPermission->deleteAll($conditions)) {
                    $transaction->rollback();
                    $this->Session->setFlash(
                        __('Date inconsistente 1!'),
                        'Alerts/error'
                    );
                    $this->redirect('/access/groups');
                }

                // remove user from groups but first collect them
                if(!$this->User->updateAll(
                    array('User.group_id' => 'NULL'),
                    array('User.group_id' => $group_id)
                )
                ) {
                    $transaction->rollback();
                    $this->Session->setFlash(
                        __('Date inconsistente 2!'),
                        'Alerts/error'
                    );
                    $this->redirect('/access/groups');
                }

                // remove group
                if (!$this->Group->delete()) {
                    $transaction->rollback();
                    $this->Session->setFlash(
                        __('Date inconsistente 3!'),
                        'Alerts/error'
                    );
                    $this->redirect('/access/groups');
                }
                $transaction->commit();

                //request login for users
                foreach ($users_to_request as $user) {
                    $this->User->requestLogin($user['User']['id']);
                }

                $this->Session->setFlash(
                    __('Grupul a fost sters!'),
                    'Alerts/success'
                );
                $this->redirect('/access/groups');

            }
        } else {
            $this->Session->setFlash(
                __('Date inconsistente!'),
                'Alerts/error'
            );
            $this->redirect('/access/groups');
        }
    }
}
