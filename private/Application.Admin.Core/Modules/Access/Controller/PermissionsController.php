<?php
/**
* PermissionsController.php
*
* Inside this Controller admin logic methods will
* be placed
*
* PHP version 5.4
*
* @category  Admin
* @package   Access
* @author    Lucian Vasile <lucian.vasile@live.com>
* @copyright 2014 Evercloud
* @license   http://www.php.net/license/3_01.txt  PHP License 3.01
* @version   SVN: $Id$
* @link      /access/permissions
* @since     1.0
*/

App::uses('Access.AccessAppController', 'Controller');

/**
* PermissionsController Class
*
* Inside this Controller admin logic methods will
* be placed
*
* @category  Admin
* @package   Access
* @author    Lucian Vasile <lucian.vasile@live.com>
* @copyright 2014 Evercloud
* @license   http://www.php.net/license/3_01.txt  PHP License 3.01
* @version   Release: 1.0
* @link      /access/permissions
* @since     Class available since Release 1.0     
*/
class PermissionsController extends AccessAppController
{
    /// properties {{{
    
    /**
    * The displayFields property
    * 
    * This property is used by dataTables.js to extract data
    * from the model and to generate the table in frontend
    * 
    * @var 
    */
    public $displayFields = array(
        'User' => array(
            'id' => 'ID',
            'email' => 'Email',
            'phone' => 'Telefon',
        ),
        'Person' => array(
            'fname' => 'Prenume',
            'lname' => 'Nume',
        )
    );
    
    /**
    * Models used by this controller
    * 
    * @var array
    */
    public $uses = array(
        'Customers.User',
        'Access.Permission',
        'Access.Action',
    );
    
    /// }}}
    
    /**
    * The permissions/index method
    * 
    * Lists the current admin users and allows 
    * to change their permissions
    * 
    * @param integer $user_id the user_id to give permissions to
    * 
    * @return void
    */
    public function index($user_id = null) 
    {
        if (!is_null($user_id)) {
            $this->User->id = $user_id;
            if (!$this->User->exists()) {
                $this->Session->setFlash(
                    __('Utilizator inexistent'), 
                    'Alerts/warning'
                );
                $this->redirect('/access/permissions');
            }
            
            $actions_list = $this->Action->find(
                'list', array(
                'fields' => array(
                    'Action.id',
                    'Action.path',
                    'Action.module',
                )
                )
            );
            
            $options = array(
                'conditions' => array(
                    'User.id' => $user_id
                )
            );
            $user = $this->User->find('first', $options);
            
            $this->Permission->virtualFields = array(
                'user_action' 
                    => 'CONCAT(Permission.user_id, "_", Permission.action_id)'
            );
            $options['conditions'] = array(
                'Permission.user_id' => $user_id
            );
            $options['fields'] = array(
                'user_action',
                'id'
            );
            $permissions = $this->Permission->find('list', $options);
            $this->set(compact('actions_list', 'user', 'permissions'));
            return $this->render('edit');
        }
        $this->set('displayFields', $this->displayFields);
    }
    
    /**
    * The save permissions method
    * 
    * Saves selected permissions in the database
    * 
    * @todo reset user's AccessList
    * 
    * @return redirect
    */
    public function save() 
    {
        if ($this->request->is('post')) {
            // golim permisiunile curente
            $conditions = array(
                'Permission.user_id' => $this->request->data[0]['user_id']
            );
            // unset la alea care nu au bifata actiunea
            foreach ($this->request->data as $k => $p) {
                if (!isset($p['action_id'])) {
                    unset($this->request->data[$k]);
                }
            } 
            $this->Permission->deleteAll($conditions);
            if ($this->Permission->saveMany($this->request->data)) {
                // re-set permissions to the user!
                $this->User->requestLogin($this->request->data[0]['user_id']);
                $this->Session->setFlash(
                    __('Permisiuni modificate!'), 
                    'Alerts/success'
                );
            } else {
                $this->Session->setFlash(
                    __('Eroare la salvare permisiunilor!'), 
                    'Alerts/error'
                );
            }
            $this->redirect('/access/permissions');
        }
    }
}
