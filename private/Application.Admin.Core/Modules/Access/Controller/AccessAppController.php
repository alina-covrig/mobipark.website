<?php
/**
* Module level Controller
*
* This file is module-wide controller file. You can put all
* module-wide controller-related methods here.
*
* PHP version 5.4
*
* @category  Admin
* @package   Access
* @author    Lucian Vasile <lucian.vasile@live.com>
* @copyright 2014 Evercloud
* @license   http://www.php.net/license/3_01.txt  PHP License 3.01
* @version   SVN: $Id$
* @link      /modules/access/controller/access-app-controller
* @since     1.0
*/

App::uses('AppController', 'Controller');

/**
* AccessAppController Class
*
* Inside this Controller module-wide logic methods
* will be placed
*
* @category  Admin
* @package   Access
* @author    Lucian Vasile <lucian.vasile@live.com>
* @copyright 2014 Evercloud
* @license   http://www.php.net/license/3_01.txt  PHP License 3.01
* @version   Release: 1.0
* @link      /modules/access/controller/access-app-controller
* @since     Class available since Release 1.0
*/
class AccessAppController extends AppController
{
    
}
