<?php
/**
* ActionsController.php
*
* Inside this Controller admin logic methods will
* be placed
*
* PHP version 5.4
*
* @category  Admin
* @package   Access
* @author    Lucian Vasile <lucian.vasile@live.com>
* @copyright 2014 Evercloud
* @license   http://www.php.net/license/3_01.txt  PHP License 3.01
* @version   SVN: $Id$
* @link      /access/menus
* @since     1.0
*/

App::uses('Access.AccessAppController', 'Controller');

/**
* ActionsController Class
*
* Inside this Controller admin logic methods will
* be placed
*
* @category  Admin
* @package   Access
* @author    Lucian Vasile <lucian.vasile@live.com>
* @copyright 2014 Evercloud
* @license   http://www.php.net/license/3_01.txt  PHP License 3.01
* @version   Release: 1.0
* @link      /access/menus
* @since     Class available since Release 1.0     
*/
class MenusController extends AccessAppController
{
    /// properties {{{
    
    /**
    * Models used by this controller
    * 
    * @var 
    */
    public $uses = array(
        'Access.Action',
        'Access.Menu',
    );
    
    /// }}}
    
    /**
    * The Menu/index method
    * 
    * List the current menu
    * 
    * @return void
    */
    public function index() 
    {
        
        if ($this->request->is('post')) {
            if ($this->Menu->save(
                $this->request->data,
                array('validate' => true)
            )
            ) {
                $this->Session->setFlash(
                    __('Elementul de meniu a fost adaugat!'), 
                    'Alerts/success'
                );
                return $this->redirect('/access/menus');
            }
        }
        
        $id = $this->Cookie->read(sha1('User') . '.id');
        $actions = $this->Action->find(
            'list', 
            array(
                'fields' => array(
                    'Action.id',
                    'Action.path',
                    'Action.module',
                ),
                'conditions' => array(
                    'Action.path' => $this->getAccessList()
                ),
            )
        );
        $menu = $this->Menu->find(
            'all', array(
                'order' => 'Menu.lft',
            )
        );
        $parents = $this->Menu->generateTreeList(null, null, null, ' » ');
        $this->set(compact('actions', 'menu', 'parents'));
    }
    
    /**
    * The Edit menu element method
    * 
    * This method allows editing an element from the menu
    * Renders /Access/View/Menu/edit.ctp
    * 
    * @param integer $id the id of the menu element to edit
    * 
    * @return void or redirect on put or error
    */
    public function edit($id = null) 
    {
        $this->Menu->id = $id;
        if (!$this->Menu->exists()) {
            $this->Session->setFlash(
                __('Element inexistent'), 
                'Alerts/warning'
            );
            return $this->redirect('/access/menus');
        }
        
        if ($this->request->is('put')) {
            if ($this->Menu->save($this->request->data)) {
                $this->Session->setFlash(
                    __('Elementul a fost modificat!'), 
                    'Alerts/success'
                );
                return $this->redirect('/access/menus');
            }
        }
        
        $actions = $this->Action->find(
            'list', 
            array(
                'fields' => array(
                    'Action.id',
                    'Action.path',
                    'Action.module',
                ),
                'conditions' => array(
                    'Action.path' => $this->getAccessList(),
                ),
            )
        );
        $this->set('actions', $actions);
        
        $parents = $this->Menu->generateTreeList(null, null, null, ' » ');
        $this->set('parents', $parents);
        
        $options = array(
            'conditions' => array(
                'Menu.id' => $id,
            )
        );
        $this->request->data = $this->Menu->find('first', $options);
    }
    
    /**
    * The move up method
    * 
    * This method will move up the menu one step up
    * 
    * @param integer $id the id of the menu to be moved up
    * 
    * @return redirect to index
    */
    public function moveup($id = null) 
    {
        
        $this->Menu->id = $id;
        if (!$this->Menu->exists()) {
            $this->Session->setFlash(
                __('Elementul de meniu nu exista in baza de date'), 
                'Alerts/warning'
            );
            return $this->redirect('/access/menus');
        }
        
        $this->Menu->moveUp($this->Menu->id, 1);
        $this->Session->setFlash(
            __('Elementul de meniu a fost mutat cu succes!'), 
            'Alerts/success'
        );
        
        return $this->redirect('/access/menus');
    }
    
    /**
    * The move down method
    * 
    * This method will move down the menu one step
    * 
    * @param integer $id the id of the menu to be moved down
    * 
    * @return redirect to index
    */
    public function movedown($id = null) 
    {
        
        $this->Menu->id = $id;
        if (!$this->Menu->exists()) {
            $this->Session->setFlash(
                __('Elementul de meniu nu exista in baza de date'), 
                'Alerts/warning'
            );
            return $this->redirect('/access/menus');
        }
        
        $this->Menu->moveDown($this->Menu->id, 1);
        $this->Session->setFlash(
            __('Elementul de meniu a fost mutat cu succes!'), 
            'Alerts/success'
        );
        
        return $this->redirect('/access/menus');
    }
    
    /**
    * The delete Menu item method
    *
    * @param integer $id the id of the menu element to be deleted
    * 
    * @return redirect
    */
    public function delete($id = null) 
    {
        $this->Menu->id = $id;
        if (!$this->Menu->exists()) {
            $this->Session->setFlash(
                __('Element inexistent'), 
                'Alerts/warning'
            );
            return $this->redirect('/access/menus');
        }
        if ($this->Menu->delete($id)) {
            $this->Session->setFlash(
                __('Element sters'), 
                'Alerts/success'
            );
        } else {
            $this->Session->setFlash(
                __('Eroare la stergere'), 
                'Alerts/error'
            );
        }
        return $this->redirect('/access/menus');
    }
}
