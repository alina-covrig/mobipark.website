<?php
/**
* ActionsController.php
*
* Inside this Controller admin logic methods will
* be placed
*
* PHP version 5.4
*
* @category  Admin
* @package   Access
* @author    Lucian Vasile <lucian.vasile@live.com>
* @copyright 2014 Evercloud
* @license   http://www.php.net/license/3_01.txt  PHP License 3.01
* @version   SVN: $Id$
* @link      /access/actions
* @since     1.0
*/

App::uses('Access.AccessAppController', 'Controller');

/**
* ActionsController Class
*
* Inside this Controller admin logic methods will
* be placed
*
* @category  Admin
* @package   Access
* @author    Lucian Vasile <lucian.vasile@live.com>
* @copyright 2014 Evercloud
* @license   http://www.php.net/license/3_01.txt  PHP License 3.01
* @version   Release: 1.0
* @link      /access/action
* @since     Class available since Release 1.0     
*/
class ActionsController extends AccessAppController
{
    /**
    * The beforeFilter callback method
    *
    * @todo problem with populate routing
    *
    * @return void
    */
    public function beforeFilter()
    {
        if ($this->request->here == '/access/actions/populate') {
            $this->populate();
        }
    }

    /**
    * The populate method
    * 
    * This calls the Action->populate method
    * The model method scans the Modules directory
    * and returns a list of all public methods
    * 
    * @return redirect to referer
    */
    public function populate()
    {
        $this->layout = false;
        $this->autoRender = false;
        ini_set('max_execution_time', 1200); 
        if ($this->Action->populate()) {
            $this->Session->setFlash(
                __('Actiunile au fost re-populate'), 
                'Alerts/success'
            );
        } else {
            $this->Session->setFlash(
                __('Eroare la re-popularea actiunilor.'), 
                'Alerts/error'
            );
        }
        $this->redirect($this->request->referer());
    }

    public function getLog($Model = null, $limit = 20, $offset = 0, $entity_id = null, $source_id = null, $event = null) 
    {
        $conditions = [];
        if ($event == 'NULL') {
            $event = null;
        }
        if ($entity_id == 'NULL') {
            $entity_id = null;
        }
        if ($source_id == 'NULL') {
            $source_id = null;
        }

        if(!is_null($event)) {
            $event = explode(';', $event);
        }
        if (is_string($Model)) {
            $Model = explode(';', $Model);
            $entity_ids = explode(';', $entity_id);
        }

        if (!is_null($source_id)) {
            $conditions['source_id'] = $source_id;
        }
        if (!is_null($Model)) {
            foreach ($Model as $k => $m) {
                if (isset($entity_ids[$k])) {
                    if (isset($entity_ids[$k]) && $entity_ids[$k] !== '') {
                        $conditions['OR'][$k]['AND']['entity_id'] = explode(',', $entity_ids[$k]);
                        $conditions['OR'][$k]['AND']['model'] = $m;
                    }
                } else {
                    $conditions['OR'][$k]['AND'] = [
                        'model' => $m,
                    ];
                }
                if (!is_null($event)) {
                    $conditions['OR'][$k]['AND']['event'] = $event;
                }
            }
        }

        $this->Audit->Behaviors->load('Containable');
        $this->Audit->contain(
            [
            'User.id',
            'User' => [
                'Person.fname',
                'Person.lname',
                'AdminDetail.picture',
            ],
            'AuditDelta'
            ]
        );
        $this->Audit->bindModel(
            [
            'belongsTo' => [
                'User' => [
                    'className' => 'Customers.User',
                    'foreignKey' => 'source_id'
                ]
            ]
            ]
        );
        $this->Audit->User->bindModel(
            [
            'hasOne' => [
                'AdminDetail' => [
                    'className' => 'Customers.AdminDetail'
                ]
            ]
            ]
        );

        $logs = $this->Audit->find(
            'all', [
            'conditions' => $conditions,
            'limit' => $limit,
            'offset' => $offset,
            'order' => [
                'created' => 'DESC'
            ]
            ]
        );

        $i = 0;

        foreach ($logs as &$l) {
            if (isset($l['User']['Person'])) {
                $l['User']['color'] =
                    stringToColorCode($l['User']['Person']['fname'].$l['User']['Person']['lname']);
            } else {
                $l['User']['color'] =
                    stringToColorCode('default');
                $l['User']['Person']['fname'] = '';
                $l['User']['Person']['lname'] = '';
                $l['User']['id'] = 0;
            }

            $l['Audit']['model'] = __($l['Audit']['model']);
            $l['Audit']['verb'] = $l['Audit']['event'];
            foreach ($l['AuditDelta'] as &$ad) {
                $ad['property_name'] = __($ad['property_name']);
            }
        }

        $this->set('_serialize', 'logs');
        $this->set('logs', $logs);
    }

}
