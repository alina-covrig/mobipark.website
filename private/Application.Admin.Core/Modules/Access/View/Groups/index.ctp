<?php echo $this->element('Plugins/treejs')?>
<?php echo $this->element('Plugins/multiple-select')?>
<?php $this->start('extra-scripts')?>
<script type="text/javascript">
$(document).ready(function() {
        $.fn.multipleSelect.defaults["selectAll"] = false;
        $.fn.multipleSelect.defaults["filter"] = true;
        $.fn.multipleSelect.defaults["placeholder"] = 'Alege Admin';
        $('.add-users-to-group').multipleSelect({
            single: true,
            selectAll: false,
            filter: true,
        });
});
</script>

<style>
    .admin {
        width: 312px;
    }
</style>
<?php $this->end() ?>

<?php $this->start('page-title') ?>
<strong><?php echo __("Grupuri Permisiuni") ?></strong>
<?php $this->end() ?>

<div class="row">
    <div class="col-lg-4 col-md-8">
        <?= $this->Panel->start(__("Adauga Grup nou"), 'panel-info', 1, 1) ?>
        <?php echo $this->Form->create('Group'); ?>
        <?php echo $this->Form->input('name', array('label' => __('Denumire grup'))); ?>
        <?php echo $this->Form->submit(__('Adauga'), array('class' => "btn btn-success")); ?>
        <?php echo $this->Form->end(); ?>
        <?= $this->Panel->end(); ?>
    </div>
    <div class="col-lg-2 col-md-5">
        <?= $this->H->bslk(
            "/access/actions/populate",
            'btn btn-info btn-block',
            $this->Html->icon('fa fa-sitemap') . __(' Repopuleaza actiunile'),
            __("Repopuleaza actiunile")
        ) ?>
    </div>
    <div class="col-lg-3">
        <?= $this->H->bslk(
            "/access/permissions",
            'btn btn-success btn-block',
            $this->Html->icon('fa fa-key') . __(' Marcheaza userii care au accese individuale'),
            __("Marcheaza userii care au accese individuale")
        )?>
    </div>
</div>
<div class="row">
    <div class="col-md-12">
        <div class="table-responsive">
            <table id="menuList" class="table tree table-striped table-bordered table-hover">
                <thead>
                <tr>
                    <th><?php echo __("Denumire") ?></th>
                    <th><?php echo __("Asociati") ?></th>
                    <th><?php echo __("Optiuni") ?></th>
                </tr>
                </thead>
                <tbody>
                <?php $count = count($groups);?>
                <?php foreach ($groups as $group) : ?>
                    <tr>
                        <td style="min-width: 300px">
                            <h4><?php echo $group['Group']['name'] ?></h4>
                            <form method="post" action="/access/groups/edituser" class="form-inline"
                                  style="float:left; margin:5px">
                                <input type="hidden" name="data[User][group_id]"
                                       value="<?php echo $group['Group']['id'] ?>">
                                <div class="input-group inline-input" style=" z-index: <?= $count--;?>">
                                    <select class="form-control add-users-to-group" name="data[User][user_id]"
                                    style="width: 250px;">
                                        <option value="">Alege admin</option>
                                        <?php
                                        foreach ($admins as $admin_id => $admin) {
                                            echo '<option value="' . $admin_id . '">' . $admin . '</option>';
                                        }
                                        ?>
                                    </select>
                                    <span class="input-group-btn">
                                            <button class="btn btn-success" type="submit"
                                                    title="Adauga in grup acest admin"><i
                                                        class="fa fa-plus"></i></button>
                                        </span>
                                </div>
                            </form>
                        </td>
                        <td>
                            <?php foreach ($group['User'] as $user): ?>
                                <div class="col-lg-6 col-md-12 col-sm-12">
                                    <form method="post" action="/access/groups/edituser" style="float:left; margin:5px">

                                        <input type="hidden" name="data[User][user_id]"
                                               value="<?php echo $user['id']; ?>">
                                        <input type="hidden" name="data[User][group_id]" value="">

                                        <div class="input-group inline-input admin">
                                            <span class="form-control"
                                                  placeholder="Search for..."><?php echo $user['full_name']; ?></span>
                                            <span class="input-group-btn">
                                                <a href="/customers/admin_details/edit/<?php echo $user['id']; ?>"
                                                   class="btn btn-info btn-md btn-outline" target="_blank"
                                                   data-toggle="tooltip" data-placement="top" data-container="body"
                                                   title="" data-original-title="Editeaza date admin">
                                                    <i class="fa fa fa-pencil"></i>
                                                </a>
                                                <button class="btn btn-danger" type="submit"
                                                        data-toggle="tooltip" data-placement="top" data-container="body"
                                                        title="" data-original-title="Elimina din acest grup">
                                                    <i class="fa fa-times"></i>
                                                </button>
                                            </span>
                                        </div>
                                    </form>
                                </div>
                            <?php endforeach; ?>
                        </td>
                        <td align="right" width="145">
                            <div class="btn-group btn-group-md">
                                <?= $this->H->bslk(
                                    "/access/groups/delete/" . $group['Group']['id'],
                                    'btn btn-danger btn-md btn-outline',
                                    $this->Html->icon('fa fa-trash-o'),
                                    __("Sterge"),
                                    'top',
                                    __("Esti sigur ca vrei sa stergi grupul? Se va sterge accesul tuturor membrilor!")
                                ) ?>
                                <?= $this->H->bslk(
                                    "/access/groups/edit/" . $group['Group']['id'],
                                    'btn btn-info btn-md btn-outline',
                                    $this->Html->icon('fa fa-pencil'),
                                    __("Modifica")
                                ) ?>
                            </div>
                        </td>
                    </tr>
                <?php endforeach; ?>
                </tbody>
            </table>
        </div>
    </div>
</div>
<!-- /.row -->