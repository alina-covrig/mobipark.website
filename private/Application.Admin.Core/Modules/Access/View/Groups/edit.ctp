<?php $this->start('extra-scripts') ?>
<script type="text/javascript">
    $(document).ready(function() {
        $('.checkAllPermissions').change(function () {
            var checked = false;
            if ($(this).is(':checked')) {
                checked = true;
            }
            $(this).parent().parent().parent().parent().find('input[type=checkbox]').prop('checked', checked);
        });

        $('#scroll-to-unauthorized-action').click(function() {
            var first_action = $("td.action-unchecked").first();

            if (first_action.length > 0) {
                $('html, body').animate({
                    scrollTop: first_action.offset().top
                }, 1000, function () {
                    first_action.removeClass('action-unchecked');
                    $('#scroll-to-unauthorized-action').val('<?=__('Du-te la urm. actiune neautorizata.')?>');
                });
            }
        });

        $('.modules-list .list-group-item').on('click', function() {
            let $this = $(this);
            let targetModule = 'module-' + $this.data('module');
            let $target = $('a[name="' + targetModule + '"]');
            scrollToElement($target);
            return false;
        });

        let scrollToElement = function($element, custom_callback) {
            let $header = $('.fixed-header');
            $('html,body').animate({
                scrollTop: $element.offset().top
            }, 500, function() {
                if(typeof custom_callback === 'function') {
                    custom_callback();
                }
            });
        };
    });
</script>
<style>
    div.action-name {
        color: #f0ad4e;
        font-style: italic;
        display: inline;
    }

    div.action-key {
        font-weight: bold;
        text-underline:;
    }

    input[type=checkbox]:checked ~ div.action-name {
        font-style: normal;
        color: #28a745;
    }

    .modules-list .list-group-item {
        padding: 4px 8px;
    }
</style>
<?php $this->end(); ?>

<?php $this->start('page-title') ?>
<strong><?php echo __("Permisiuni pentru grupul ") ?><?php echo ucwords($group['Group']['name']) ?></strong>
<?php $this->end() ?>


<div class="row">
    <?php echo $this->Form->create('Group', array('form-style' => 'inline', 'url' => 'save')) ?>
    <div class="col-lg-10 col-md-7">
        <div class="table-responsive">
            <table class="table table-striped table-bordered table-hover no-footer table-condensed">
                <thead>
                <tr>
                    <th width="25%">Modul</th>
                    <th width="65%">Actiune</th>
                </tr>
                </thead>
                <?php $k = 0; ?>
                <?php $modules_list = []; ?>
                <?php foreach ($actions_list as $module => $list): ?>
                <tbody>
                <tr>
                    <td rowspan="<?php echo count($list) ?>">
                        <a name="module-<?=$module?>"></a>
                        <h3><?php echo ucwords($module) ?></h3>
                        <?php $modules_list[] = $module; ?>
                        <p>Selecteaza toate actiunile din modul:
                            <input type="checkbox" class="checkAllPermissions"/>
                        </p>
                    </td>
                    <td>
                        <input type="checkbox" name="data[<?php echo $k ?>][action_id]"
                               value='<?php echo key($list) ?>' <?php echo (isset($permissions[$group['Group']['id'] . '_' . key($list)])) ? 'checked="checked"' : '' ?> />
                        <input type="hidden" name="data[<?php echo $k++ ?>][group_id]"
                               value='<?php echo $group['Group']['id'] ?>'/>
                        <div class="action-name action-key">
                            <?php echo $list[key($list)];
                            unset($list[key($list)]) ?>
                        </div>
                    </td>
                </tr>
                <?php foreach ($list as $action_id => $action): ?>
                    <tr>
                        <td <?php echo (!isset($permissions[$group['Group']['id'] . '_' . $action_id])) ? 'class="action-unchecked"' : '' ?>>
                            <input type="checkbox" name="data[<?php echo $k ?>][action_id]"
                                   value='<?php echo $action_id ?>' <?php echo (isset($permissions[$group['Group']['id'] . '_' . $action_id])) ? 'checked="checked"' : '' ?>/>
                            <input type="hidden" name="data[<?php echo $k++ ?>][group_id]"
                                   value='<?php echo $group['Group']['id'] ?>'/>
                            <div class="action-name">
                                <?php echo $action ?>
                            </div>
                        </td>
                    </tr>
                <?php endforeach; ?>
                <?php endforeach; ?>
            </table>
        </div>
    </div>
    <div class="col-lg-2 col-md-5">
        <div class="panel panel-default affix">
            <div class="panel-heading text-strong"><strong><?php echo ucwords($group['Group']['name']) ?></strong></div>
            <div class="panel-body" style="display: block;">
                <?= $this->H->bslk(
                    "/access/actions/populate",
                    'btn btn-info btn-block',
                    $this->Html->icon('fa fa-sitemap') . __(' Repopuleaza actiunile'),
                    __("Repopuleaza actiunile")
                ) ?>
                <input class="btn btn-warning btn-block" type="button" value="<?=__('Du-te la prima actiune neautorizata')?>" id="scroll-to-unauthorized-action">
                <input class="btn btn-success btn-block" type="submit" value="<?=__('Modifica permisiuni')?>">
                <?= $this->H->bslk(
                    "/access/groups",
                    'btn btn-default btn-block',
                    $this->Html->icon('fa fa-arrow-left') . __(' Inapoi la grupuri'),
                    __("Inapoi la grupuri")
                ) ?>
            </div>
            <?php if (count($modules_list) > 0) : ?>
                <ul class="list-group modules-list small">
                    <?php foreach ($modules_list as $module) : ?>
                        <a class="list-group-item" data-module="<?=$module?>" href="#"><?=ucfirst($module)?></a>
                    <?php endforeach; ?>
                </ul>
            <?php endif; ?>
        </div>
    </div>
    <?php echo $this->Form->end() ?>
</div>
