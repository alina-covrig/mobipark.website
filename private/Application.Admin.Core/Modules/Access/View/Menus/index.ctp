<?php echo $this->element('Plugins/treejs')?>
<?php $this->start('extra-scripts')?>
    <script type="text/javascript">
        $('.tree').treegrid({
            saveState: true
        });   
    </script>
<?php $this->end()?>
<div class="row">
    <div class="col-md-9">
        <h1 class="page-header"><?php echo __("Meniu")?></h1>
        <?php if(empty($menu)) :?>
            <div class="well well-sm">
                <h4>Niciun element in meniu</h4>
                <p>Adauga elemente in meniu folosindu-te de formularul din dreapta</p>
            </div>
        <?php else: ?>
            <div class="table-responsive">
                <table id="menuList" class="table tree table-striped table-bordered table-hover">
                    <thead>
                        <tr>
                            <th><?php echo __("Optiune meniu")?></th>
                            <th><?php echo __("Cale")?></th>
                            <th><?php echo __("Optiuni")?></th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php foreach($menu as $m) :?>
                            <tr class="treegrid-<?php echo $m['Menu']['id']?> <?php echo ($m['Menu']['parent_id']) ? 'treegrid-parent-' . $m['Menu']['parent_id'] : ''?>">
                                <td><i class="fa <?php echo $m['Menu']['class']?>"></i> <?php echo $m['Menu']['name']?></td>
                                <td><?php echo $m['Action']['path']?></td>
                                <td align="right" width="145">
                                    <div class="btn-group btn-group-xs">
                                        <?= $this->H->bslk(
                                            "/access/menus/moveup/" . $m['Menu']['id'],
                                            'btn btn-warning btn-xs btn-outline',
                                            $this->Html->icon('fa fa-arrow-up'),
                                            __("Muta categoria mai sus")
                                        )?>
                                        <?= $this->H->bslk(
                                            "/access/menus/movedown/" . $m['Menu']['id'],
                                            'btn btn-warning btn-xs btn-outline',
                                            $this->Html->icon('fa fa-arrow-down'),
                                            __("Muta categoria mai jos")
                                        )?>
                                        <?= $this->H->bslk(
                                            "/access/menus/delete/" . $m['Menu']['id'],
                                            'btn btn-danger btn-xs btn-outline',
                                            $this->Html->icon('fa fa-trash-o'),
                                            __("Sterge"),
                                            'top',
                                            __("Esti sigur ca vrei sa stergi elementul din meniu?")
                                        )?>
                                        <?= $this->H->bslk(
                                            "/access/menus/edit/" . $m['Menu']['id'],
                                            'btn btn-info btn-xs btn-outline',
                                            $this->Html->icon('fa fa-pencil'),
                                            __("Modifica")
                                        )?>
                                    </div>
                                </td>
                            </tr>
                        <?php endforeach;?>
                    </tbody>
                </table>
            </div>
        <?php endif;?>
        <div class="row">
            <div class="col-md-6">
                <?= $this->H->bslk(
                    "/access/actions/populate",
                    'btn btn-info',
                    __("Regenereaza actiunile"),
                    __("Regenereaza actiunile")
                )?>
                <button type="button" class="btn btn-default btn-circle" data-toggle='modal' data-target='#help-regen-actions'><i class="fa fa-question"></i></button>
                <?php 
                    echo $this->H->modal(
                        'help-regen-actions', 
                        'help-regen-actions-titile', 
                        'Ce inseamna regenereaza actiunile?',
                        'Regenerarea actiunilor presupune scanarea tuturor modulelor din' 
                            . ' aplcatie si salvarea lor in baza de date.' 
                            . ' Acest lucru va repopula actiunile pentru regenerarea meniului si'
                            . ' acordarea permisiunilor.'
                    );
                ?>
            </div>
        </div>
    </div>
    <div class="col-md-3 mt20">
        <div class="mg20"></div>
        <h2>Element in meniu</h2>
        <?php echo $this->Form->create('Menu');?>
            <?php echo $this->Form->input('parent_id', array('label' => __('Parinte'), 'empty' => __('(element principal)')));?>
            <?php echo $this->Form->input('name', array('label' => __('Optiune meniu')));?>
            <?php echo $this->Form->input('action_id', array('label' => __('Actiune'), 'empty' => __('(fara actiune)')));?>
            <?php echo $this->Form->input('class', array('label' => __('Clasa Font-awesome')));?>
            <?php echo $this->Form->submit(__('Adauga'), array('class' => "btn btn-success"));?>
        <?php echo $this->Form->end();?>
    </div>
</div>
<!-- /.row -->