<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header"><?php echo __("Modifica element din meniu")?></h1>
    </div>
    <!-- /.col-lg-12 -->
</div>
<!-- /.row -->
<div class="row">
    <div class="col-lg-6">
        <?= $this->H->bslk(
            "/access/menus",
            'btn btn-warning btn-block',
            $this->Html->icon('fa fa-arrow-left') . __(' Inapoi la editare meniu'),
            __("Inapoi la editare meniu")
        )?>
        <div class="mg20"></div>
    </div>
    <!-- /.col-lg-6 -->
    <div class="col-lg-6">
        <?= $this->H->bslk(
            "/access/menus/delete/" . $this->request->data['Menu']['id'],
            'btn btn-danger btn-block',
            $this->Html->icon('fa fa-trash-o') . ' Sterge element',
            __("Sterge element"),
            'top',
            __('Esti sigur ca vrei sa stergi acest element din meniu?')
        )?>
        <div class="mg20"></div>
    </div>
    <!-- /.col-lg-6 -->
</div>
<!-- /.row -->
<div class="row">
    <div class="col-md-12">
        <?php echo $this->Form->create('Menu', array('type' => 'file'));?>
            <?php echo $this->Form->input('id');?>
            <?php echo $this->Form->input('parent_id', array('label' => __('Parinte'), 'empty' => __('(element principal)')));?>
            <?php echo $this->Form->input('name', array('label' => __('Optiune meniu')));?>
            <?php echo $this->Form->input('action_id', array('label' => __('Actiune'), 'empty' => __('(fara actiune)')));?>
            <?php echo $this->Form->input('class', array('label' => __('Clasa Font-awesome')));?>
            <?php echo $this->Form->submit(__('Modifica elementul'), array('class' => "btn btn-success"));?>
        <?php echo $this->Form->end();?>
    </div>
</div>