<?php echo $this->element('Plugins/dataTables')?>
<?php $this->start('extra-scripts')?>
    <script type="text/javascript">
        $(document).ready(function() {
            $('#browserList').dataTable({
                "bProcessing": true,
                "bServerSide": true,
                "sAjaxSource": "/customers/users/getData/User/User.is_admin=1",
                stateSave: true
            });
        });
    </script>
<?php $this->end()?>
<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header"><?php echo __("Lista administratori")?></h1>
    </div>
    <!-- /.col-lg-12 -->
</div>
<!-- /.row -->
<div class="row">
    <div class="col-lg-4">
        <?= $this->H->bslk(
            "/customers/users/add",
            'btn btn-warning btn-block',
            $this->Html->icon('fa fa-plus') . __(' Adauga utilizator nou'),
            __("Adauga utilizator nou")
        )?>
        <div class="mg20"></div>
    </div>
    <div class="col-lg-4">
        <?= $this->H->bslk(
            "/access/actions/populate",
            'btn btn-info btn-block',
            $this->Html->icon('fa fa-sitemap') . ' Repopuleaza actiunile',
            __("Repopuleaza actiunile")
        )?>
        <div class="mg20"></div>
    </div>
</div>
<!-- /.row -->
<div class="row">
    <div class="table-responsive">
        <table id="browserList" class="table table-striped table-bordered table-hover dataTable no-footer table-condensed">
            <thead>
                <tr>
                    <?php foreach($displayFields as $Model => $fields) :?>
                        <?php foreach($fields as $field => $displayName) :?>
                                <th><?php echo  $displayName ?></th>
                        <?php endforeach;?>
                    <?php endforeach;?>
                </tr>
            </thead>
            <tbody data-location="/access/permissions/index/">
                <tr>
                    <td colspan="99" class="dataTables_empty">Asteapta...</td>
                </tr>
            </tbody>
        </table>
    </div>
</div>