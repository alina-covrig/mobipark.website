<?php $this->start('extra-scripts')?>
    <script type="text/javascript">
        $('.checkAllPermissions').click(function() {
            if($(this).is(':checked')) {
                $(this).parent().parent().parent().parent().find('input[type=checkbox]').attr('checked', 'checked');
            } else {
                $(this).parent().parent().parent().parent().find('input[type=checkbox]').removeAttr('checked');
            }
        });
    </script>
<?php $this->end();?>
<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header"><?php echo __("Acorda permisiuni lui ")?> <?php echo $user['User']['email']?> [ID:<?php echo $user['User']['id']?>]</h1>
    </div>
    <!-- /.col-lg-12 -->
</div>
<div class="row">
    <?php echo $this->Form->create('Permission', array('form-style' => 'inline', 'url' => 'save'))?>
        <div class="table-responsive">
            <table class="table table-striped table-bordered table-hover no-footer table-condensed">
                <thead>
                    <tr>
                        <th width="35%">Modul</th>
                        <th width="10%">OK</th>
                        <th width="55%">Actiune</th>
                    </tr>
                </thead>
                    <?php $k = 0;?>
                <?php foreach($actions_list as $module => $list):?>
                    <tbody>
                        <tr>
                            <td rowspan="<?php echo count($list)?>">
                                <h3><?php echo $module?></h3>
                                <p>Acorda permisiuni tuturor actiunilor acestui modul: 
                                    <input type="checkbox" class="checkAllPermissions"/>
                                </p>
                            </td>
                            <td align="center">
                                <input type="checkbox" name="data[<?php echo $k?>][action_id]" value='<?php echo key($list)?>' <?php echo (isset($permissions[$user['User']['id'] . '_' . key($list)])) ? 'checked="checked"' : ''?> />
                                <input type="hidden" name="data[<?php echo $k++?>][user_id]" value='<?php echo $user['User']['id']?>' />
                            </td>
                            <td><?php echo $list[key($list)]; unset($list[key($list)])?></td>
                        </tr>
                        <?php foreach($list as $action_id => $action):?>
                            <tr>
                                <td align="center">
                                    <input type="checkbox" name="data[<?php echo $k?>][action_id]" value='<?php echo $action_id?>' <?php echo (isset($permissions[$user['User']['id'] . '_' . $action_id])) ? 'checked="checked"' : ''?>/>
                                    <input type="hidden" name="data[<?php echo $k++?>][user_id]" value='<?php echo $user['User']['id']?>' />
                                </td>
                                <td>
                                    <?php echo $action?>
                                </td>
                            </tr>
                        <?php endforeach;?>
                        <tr>
                            <td colspan="99">
                                <?php echo $this->Form->submit(__('Modifica permisiuni'), array('class' => "btn btn-success btn-outline"));?>
                            </td>
                        </tr>
                    </tbody>
                <?php endforeach;?>
            </table>
        </div>
    <?php echo $this->Form->end()?>
</div>