<?php $this->start('page-title')?>
<b><?=__('cereri plata credit magazin')?></b>
<?php $this->end()?>
<div class="row">
    <div class="col-md-12 table-responsive">
        <div class="batch-actions-container mb20">
            <form action="<?=Router::url(['action' => 'batchActions'])?>" method="post" class="lock-form">
                <div class="row">
                    <div class="col-lg-2" style="padding-right: 0;">
                        <select name="batch_actions" id="batch_actions" class="form-control">
                            <option value="0">-- <?= __('Alege actiunea') ?> --</option>
                            <option value="1"><?= __('Genereaza fisier de plata') ?></option>
                            <option value="2"><?= __('Plateste') ?></option>
                        </select>
                    </div>
                    <div class="col-lg-1">
                        <button type="submit" name="go_batch"
                                id="go-batch"
                                class="btn btn-primary btn-form-submit"
                                data-processing="<?=__('Se proceseaza actiunea')?>"
                        ><i class="fa fa-paper-plane"></i></button>
                    </div>
                </div>
            </form>
        </div>

        <table class="table table-condensed small">
            <thead>
            <tr>
                <th width="30"><input type="checkbox" name="check_requests" class="ckeck-all" value="1" /></th>
                <th><?=__('ID cerere')?></th>
                <th></th>
                <th><?=__('ID user')?></th>
                <th><?=__('Nume complet')?></th>
                <th><?=__('Persoana (J/F)')?></th>
                <th><?=__('Tip plata')?></th>
                <th><?=__('Banca')?></th>
                <th><?=__('Cont')?></th>
                <th><?=__('Judet')?></th>
                <th><?=__('Localitate')?></th>
                <th><?=__('Sucursala')?></th>
                <th><?=__('Data solicitarii')?></th>
                <th><?=__('Valoare returnabila')?></th>
                <th><?=__('Valoare returnata')?></th>
                <th width="60"><?=__('Nota')?></th>
                <th><?=__('Document achitare')?></th>
                <th><?=__('Din facturile')?></th>
                <th><?=__('Achita cu document')?></th>
                <th style="min-width: 60px;"><?=__('Actiuni')?></th>
            </tr>
            <tr class="remove_form_group">
                <?php echo $this->element('Toolkit.filterForm')?>
            </tr>
            </thead>
            <tbody>
            <?php foreach ($requests as $r):?>
                <tr style=<?php if($r['StoreCreditRequest']['is_paid']):?>"background-color:#C2F0FF;"<?php else:?>"background-color:#D6F5D6;"<?php endif;?>>
                    <td>
                        <?php if (!$r['StoreCreditRequest']['is_paid'] && $r['StoreCreditRequest']['type'] == 'bank') : ?>
                            <input type="checkbox"
                                   name="requests[]"
                                   class="checkbox-custom"
                                   value="<?=$r['StoreCreditRequest']['id']?>"
                                   data-pending="<?=($r['StoreCreditRequest']['is_pending']) ? 'true' : 'false'?>"
                                   disabled
                            />
                        <?php endif; ?>
                    </td>
                    <td><?=$r['StoreCreditRequest']['id']?></td>
                    <td>
                        <?php if ($r['StoreCreditRequest']['is_pending'] && !$r['StoreCreditRequest']['is_paid']) : ?>
                            <a href="<?=Router::url(['action' => 'removePendingStatus', $r['StoreCreditRequest']['id']])?>"
                               data-toggle="tooltip"
                               data-html="true"
                               class="text-red"
                               title="<?=__("<b><u>IN ASTEPTARE</u></b><br>fisier de plata generat<br><small>(Click pentru a dezactiva)</small>")?>"
                               onclick="return confirm('Esti sigur(a) ca vrei sa dezactivezi statutul de IN ASTEPTARE?');"
                            ><i class="fa fa-spin fa-circle-o-notch"></i></a>
                        <?php endif; ?>
                    </td>
                    <td><?=$r['StoreCreditRequest']['user_id']?></td>
                    <td>
                        <a target="_blank" href="<?php echo Configure::read("STORE_URL")."login/".$r['user_hash']; ?>" title="<?= __('Login ca utilizator');?>">
                            <?=$r['StoreCreditRequest']['full_name']?> <i class="fa fa-external-link"></i>
                        </a>
                    </td>
                    <td><?=($r['StoreCreditRequest']['entity']) ? $r['StoreCreditRequest']['entity'] : __('Persoana fizica')?></td>
                    <td><?=$r['StoreCreditRequest']['type']?></td>
                    <td><?=$r['StoreCreditRequest']['bank']?></td>
                    <td><?=$r['StoreCreditRequest']['bank_account']?></td>
                    <td><?=$r['StoreCreditRequest']['state']?></td>
                    <td><?=$r['StoreCreditRequest']['city']?></td>
                    <td><?=$r['StoreCreditRequest']['bank_branch']?></td>
                    <td><?=$r['StoreCreditRequest']['created']?></td>
                    <td style="text-align: right">
                        <?php if(!$r['StoreCreditRequest']['is_paid']): ?>
                            <?=$r['StoreCreditRequest']['total_value']?>
                        <?php else: ?>
                            0
                        <?php endif; ?>
                    </td>
                    <td style="text-align: right"><?=$r['StoreCreditRequest']['value']?></td>
                    <td>
                        <a
                                href="#"
                                class="request-note"
                                data-pk="<?=$r['StoreCreditRequest']['id']?>"
                                data-type="textarea"
                                data-name="StoreCreditRequest[note]"
                                data-url="/customers/store_credits/editRequestNote/<?=$r['StoreCreditRequest']['id']?>.json"
                                data-action="/customers/store_credits/editRequestNote/<?=$r['StoreCreditRequest']['id']?>.json"
                                data-original-title="<?=__('Modifica nota')?>"
                        ><?=$r['StoreCreditRequest']['note']?></a>
                    </td>
                    <td>
                        <?php if (!empty($r['StoreCreditRequest']['document_path'])):?>
                            <?php $doc_info = pathinfo($this->H->storePath($r['StoreCreditRequest']['document_path']))?>
                            <a href="/remoteGet<?=$r['StoreCreditRequest']['document_path']?>" title="<?=$doc_info['basename']?>" target="_blank">
                                <?=$doc_info['basename']?> <i class="fa fa-external-link"></i>
                            </a>
                        <?php else : ?>
                            <?php echo $r['StoreCreditRequest']['document']; ?>
                        <?php endif;?>
                    </td>
                    <td><?=$r['invoices_names']?></td>
                    <td>
                        <?php if ($r['StoreCreditRequest']['type'] == 'bank' && !$r['StoreCreditRequest']['is_paid']):?>
                            <form action="/customers/store_credits/payRequest/<?=$r['StoreCreditRequest']['id']?>" method="post" enctype="multipart/form-data">
                                <input required type="file" name="data[payment_document]" style="display: inline-block" class="form-inline"/>
                                <button type="submit" class="btn btn-success btn-xs">
                                    <i class="fa fa-usd"></i>
                                </button>
                            </form>
                        <?php endif;?>
                    </td>
                    <td align="right" width="50">
                        <div class="btn-group btn-group-xs">
                            <?php if ($r['StoreCreditRequest']['type'] == 'cash' && !$r['StoreCreditRequest']['is_paid']):?>
                                <a href="/customers/store_credits/payRequest/<?=$r['StoreCreditRequest']['id']?>" class="btn btn-success btn-xs" data-toggle="tooltip" data-placement="top" data-container="body" title="" data-original-title="<?=__('Emite chitanta')?>"><i class="fa fa-usd"></i></a>
                            <?php endif;?>
                            <a href="/customers/store_credits/edit/<?=$r['StoreCreditRequest']['id']?>" class="btn btn-info btn-xs btn-outline" data-toggle="tooltip" data-placement="top" data-container="body" title="" data-original-title="<?=__('Modifica')?>"><i class="fa fa fa-pencil"></i></a>
                        </div>
                    </td>
                </tr>
            <?php endforeach;?>
            </tbody>
        </table>
        <?php echo $this->element('Navigation/pagination'); ?>
    </div>
</div>

<?php echo $this->element('Plugins/inline-edit')?>
<?php $this->start('extra-scripts')?>
<style type="text/css">
    a.request-note { height: 32px; overflow: hidden; display: inline-block; border: 0; }
    a.request-note:hover { border: 0; }
</style>
<script type="text/javascript">
    $(document).ready(function() {
        $('.request-note').editable();

        $('input[type="checkbox"][name="check_requests"]').on('click', function() {
            $('input.checkbox-custom').not('[disabled]').prop('checked', $(this).prop('checked'));
        });

        $('select#batch_actions').on('change', function() {
            let value = parseInt($(this).val());
            $('input.checkbox-custom')
                .prop('disabled', true)
                .prop('checked', false);
            $('input[type="checkbox"][name="check_requests"]').prop('checked', false);
            if (value === 1) {
                $('input.checkbox-custom[data-pending="false"]').prop('disabled', false);
            } else if (value === 2) {
                $('input.checkbox-custom[data-pending="true"]').prop('disabled', false);
            }
        });

        $('#go-batch').on('click', function() {
            let $form = $(this).parents('form');
            let $actionSelect = $('select[name="batch_actions"]');
            let action = parseInt($actionSelect.val());
            let $button = $('#go-batch');
            $form.find('input.generated-fields').remove();
            let checked_index = 0;
            let checked_elements = [];
            $('input.checkbox-custom').each(function(i, v) {
                if ($(v).prop('checked')) {
                    $form.append('<input type="hidden" class="generated-fields" name="requests[]" value="' + $(v).val() + '" />');
                    checked_elements.push($(v).val());
                    checked_index++;
                }
            });
            if (checked_index === 0) {
                alert('Nu ati selectat nicio cerere din lista');
                return false;
            }

            if (action === 0) {
                alert('Nu ati selectat nicio actiune pentru cererile selectate');
                return false;
            }
            else {
                if (action === 1) {
                    // add `is_pending` data to selected inputs
                    $('input.checkbox-custom').each(function (i, v) {
                        if ($(v).prop('checked')) {
                            $(v).attr('data-pending', 'true');
                        }
                    });
                }
                $form.submit();
                setTimeout(function() {
                    window.location = window.location;
                }, 1000);
            }
        });
    });
</script>
<?php $this->end()?>
