<style>
    td {
        height: 22px;
    }
    table.receiptRow {
        border-collapse: collapse;width:100%;border:1px;margin-top:10px;
    }
    * {
        font-family: Arial,sans-serif;
    }
</style>
<div style="width: 100%;font-size:11px;font-family: Arial,sans-serif;padding:30px;">
    <table style="border-collapse: collapse; width: 100%;">
        <tr>
            <td>
                <table style="border-collapse: collapse; width: 400px;">
                    <tr><td style="vertical-align: middle;width:100px;"><?= __("Unitatea")?>:</td><td><strong style="font-size: 18px;"><?=Configure::read('COMPANY_NAME')?></strong></td></tr>
                    <tr><td><?= __("Cod fiscal")?>:</td><td><strong><?=Configure::read('COMPANY_VAT')?></strong></td></tr>
                    <tr><td><?= __("Nr. Reg. Com.")?>:</td><td><strong><?=Configure::read('COMPANY_REGISTERED')?></strong></td></tr>
                    <tr><td><?= __("Sediul")?>:</td><td><strong><?=Configure::read('COMPANY_ADDRESS_1')?></strong></td></tr>
                    <tr><td><?= __("Capital social")?>:</td><td><strong><?=Configure::read('COMPANY_CAPITAL')?></strong></td></tr>
                </table>
            </td>
            <td style="text-align:right;font-size:12px;">
                <table style="border-collapse: collapse;height: 20px;width:310px;">
                    <tr>
                        <td style="vertical-align:middle;white-space: nowrap;"><?= __("Seria")?>: <?=Configure::read('RECEIPT_SERIES')?>B</td>
                        <td style="vertical-align:middle;width:50px;"><?= __("Nr")?>.</td>
                        <td style="vertical-align:middle;text-align:left;"><strong style="font-size: 16px;"><?php echo $number;?></strong></td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>

    <div style="text-align: center;font-size: 14px;">
        <br/>
        <table style="border-collapse: collapse;margin-left:150px;font-size: 22px;">
            <tr>
                <td style="vertical-align: middle;">
                    <strong>CHITANTA Nr. </strong>
                </td>
                <td style="vertical-align: middle;border-bottom: 2px #000000 solid;width: 180px;text-align: left;">
                    <?php echo $number;?>
                </td>
            </tr>
        </table>

        <div style="height: 10px;"></div>

        <table style="border-collapse: collapse;margin-left:200px;">
            <tr>
                <td style="width: 60px;">Data</td>
                <td style="border-bottom: 2px #000000 solid;"><?php
                    echo date('d.m.y');?></td>
            </tr>
        </table>
    </div>

    <div style="text-align: left;font-size: 14px;margin-top: 20px;">
        <table class="receiptRow">
            <tr>
                <td style="width: 50px;white-space: nowrap;">
                    <?php echo __('Am primit de la');?>
                </td>
                <td style="border-bottom: 2px #000000 solid;text-align: left;font-style:italic;">
                    <?=$request['StoreCreditRequest']['full_name']?> <?=$request['StoreCreditRequest']['entity']?>
                </td>
            </tr>
        </table>

        <table class="receiptRow">
            <tr>
                <td style="width: 50px;white-space: nowrap;">
                    <?php echo __('Adresa');?>
                </td>
                <td style="border-bottom: 2px #000000 solid;text-align: left;font-style:italic;">
                    <?=$request['StoreCreditRequest']['address_string']?>
                </td>
            </tr>
        </table>

        <table class="receiptRow">
            <tr>
                <td style="width: 50px;white-space: nowrap;">
                    <?php echo __('Suma de');?>
                </td>
                <td style="border-bottom: 2px #000000 solid;text-align: left;font-style:italic;">
                    &nbsp;-<?=$request['StoreCreditRequest']['value']?> <?=Configure::read('LOCAL_CURRENCY')?>
                </td>
            </tr>
        </table>

        <table class="receiptRow">
            <tr>
                <td style="width: 50px;white-space: nowrap;">
                    <?php echo __('Reprezentand');?>
                </td>
                <td style="border-bottom: 2px #000000 solid;text-align: left;font-style:italic;">
                    <?=$request['StoreCreditRequest']['payment_string']?>
                </td>
            </tr>
        </table>

    </div>
    <div style="text-align: right;font-size: 18px;margin-top: 30px;width:70%;">
        <strong>Casier,</strong>
    </div>
</div>