<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header"><?php echo __("Modificare cerere plata credit magazin")?></h1>
    </div>
    <!-- /.col-lg-12 -->
</div>
<!-- /.row -->
<div class="row">
    <div class="col-lg-12">
        <?= $this->H->bslk(
            "/customers/store_credits/requests/",
            'btn btn-warning btn-block',
            $this->Html->icon('fa fa-arrow-left') . __(' Inapoi la toate cererile'),
             __(' Inapoi la toate cererile')
        )?>
        <div class="mg20"></div>
    </div>
    <!-- /.col-lg-6 -->
</div>
<!-- /.row -->
<div class="row">
    <div class="col-md-12">
        <?php echo $this->Form->create('StoreCreditRequest');?>
            <?php echo $this->Form->input('id');?>
            <div class="row">
                <div class="col-md-4">
                    <?php echo $this->Form->input('full_name', array('label' => 'Nume'));?>
                    <?php echo $this->Form->input('bank', array('label' => 'Banca'));?>
                    <?php echo $this->Form->input('bank_account', array('label' => 'Cont Bancar'));?>
                </div>
                <div class="col-md-4">
                    <?php echo $this->Form->input('city', array('label' => __('Localitate')));?>
                    <?php echo $this->Form->input('state', array('label' => __('Judet')));?>
                    <?php echo $this->Form->input('bank_branch', array('label' => __('Sucursala')));?>
                </div>
            </div>
            <?php echo $this->Form->submit(__('Modifica cerere'), array('class' => "btn btn-success btn-block btn-lg"));?>
        <?php echo $this->Form->end();?>
    </div>
</div>
