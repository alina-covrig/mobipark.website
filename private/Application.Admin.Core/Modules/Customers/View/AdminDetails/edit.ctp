<?php $this->start('extra-scripts'); ?>
    <script type="text/javascript">

    </script>
<?php $this->end(); ?>
<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header"><?php echo __("Editeaza Detalii admin pentru")?> <?php echo $this->request->data['User']['email']?></h1>
    </div>
    <!-- /.col-lg-12 -->
</div>
<!-- /.row -->
<div class="row">
    <div class="col-lg-6">
        <?= $this->H->bslk(
            "/customers/users/edit/" . $this->request->data['User']['id'],
            'btn btn-warning btn-block',
            $this->Html->icon('fa fa-arrow-left') . __(' Inapoi la') . ' ' . $this->request->data['User']['email'],
             __(' Inapoi la contul lui ') . ' ' . $this->request->data['User']['email']
        )?>
        <div class="mg20"></div>
    </div>
    <!-- /.col-lg-6 -->
</div>
<!-- /.row -->
<div class="row">
    <div class="col-md-9">
        <?php echo $this->Form->create('AdminDetail', array('type' => 'file'));?>
            <?php echo $this->Form->input('id');?>
            <div class="row">
                <div class="col-lg-12">
                    <?php echo $this->Form->input('AdminDetail.picture', array('label' => 'Poza patrata maxim 200x200 PX', 'type' => 'file'));?>
                    <?php echo $this->Form->input('AdminDetail.homepage', array('label' => 'Pagina de start'));?>
                    <?php echo $this->Form->input('AdminDetail.is_home_employee', array('label' => __('Angajat Mobiparts'),'title' => __('Este angajat direct S.C. MOBIPARTS S.R.L.')))?>
                    <?php echo $this->Form->input('AdminDetail.is_picker', array('label' => __('Procesator colete'),'title' => __('Este picker pt coletele cu expeditie nationala si / sau ridicare de la sediu')))?>
                </div>
            </div>
            <?php echo $this->Form->submit(__('Modificati Detalii Admin'), array('class' => "btn btn-success btn-block btn-lg"));?>
            
        <?php echo $this->Form->end();?>
    </div>
    <div class="col-md-3">
        <?php
            if(!empty($this->request->data['AdminDetail']['picture'])) {
                echo '<img src="'.$this->request->data['AdminDetail']['picture'].'" style="max-width: 100%;">';
            } else {
                echo 'Fara Poza';
            }
        ?>
    </div>
</div>