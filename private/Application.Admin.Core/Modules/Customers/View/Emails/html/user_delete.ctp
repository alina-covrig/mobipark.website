<table  align="center" width="660" style="margin:0 20px;  font-size: 16px; color: #5f5f5f;">
    <tr>
        <td style="padding-top:25px; font-size:24px; font-weight:bold; color:#333">
            Cerere stergere date personale
        </td>
    </tr>
    <tr>
        <td>
            <br>
            <p>
                In urma solicitarii dumneavoastra de a va sterge contul realizat pe site-ul <?=$storeName?>,
                contul a fost dezactivat, e-mailul si numarul de telefon au fost sterse
                si ati fost automat dezabonat din baza noastra de date de email marketing.
            </p>
            <p>
                Va dorim o zi buna si speram sa reveniti curand alaturi de noi in calitate de client sau colaborator!
            </p>
        </td>
    </tr>
</table>