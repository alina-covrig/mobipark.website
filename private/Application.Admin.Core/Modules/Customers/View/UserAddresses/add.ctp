<?php $this->start('extra-scripts'); ?>
<script type="text/javascript">
    $(document).ready(function(){

        checkZipCode();

        $('#UserAddressCountry').on('change', function(){
            var country = $(this).val();
            $('.to-clear').val('');
            var text = null;
            if(country != 'Romania'){
                text = '<dt><label for="select_county_int" class="control-label">Judet</label></dt>'
                    +'<dd><input type="text" name="data[UserAddress][state]"'
                    +'class="form-control to-clear input-xs"'
                    +'autocomplete="off"/></dd>';
                text += '<dt><label for="select_city_int" class="control-label">Localitate</label></dt>'
                    +'<dd><input type="text" name="data[UserAddress][city]"'
                    +'class="form-control to-clear input-xs"'
                    +'autocomplete="off"/></dd>';
                text += '<dt><label for="select_street_int" class="control-label">Strada</label></dt>'
                    +'<dd><input type="text" name="data[UserAddress][street_name]"'
                    +'class="form-control to-clear input-xs"'
                    +'autocomplete="off"/></dd>';
                $('#address_details_wrapper').html(text);
            }else{
                text = '<dt><label for="select_county" class="control-label">Judet</label></dt>'
                    + '<dd><select name="data[UserAddress][state]"'
                    + 'class="form-control to-clear input-xs"'
                    + 'autocomplete="off" id="UserAddressState" /></dd>';
                text += '<dt><label for="select_city" class="control-label">Localitate</label></dt>'
                    + '<dd><select name="data[UserAddress][city]"'
                    + 'class="form-control to-clear input-xs"'
                    + 'autocomplete="off" id="UserAddressCity"/></dd>';
                text += '<dt><label for="select_street" class="control-label">Strada</label></dt>'
                    + '<dd><select name="data[UserAddress][street_name]"'
                    + 'class="form-control to-clear input-xs"'
                    + 'autocomplete="off" id="UserAddressStreet"/></dd>';
                $('#address_details_wrapper').html(text);

                $.ajax({
                    url:'/customers/user_addresses/getRomanianCounties',
                    type: 'post',
                    dataType: 'html',
                    data:{
                        country:country
                    },
                    success: function(r){
                        $('#UserAddressState').html(r);
                    }
                });


            }
        });

        $('body').on('change', '#UserAddressState', function(){
            var county = $(this).val();
            $('#UserAddressCity').val('');
            $.ajax({
                url: '/customers/user_addresses/getRomanianCities',
                type: 'post',
                dataType: 'html',
                data: {
                    county: county
                },
                success: function (r) {
                    $('#UserAddressCity').html(r)
                }
            })
        });

        $('body').on('change', '#UserAddressCity', function(){
            var city = $(this).val();
            var currentInput = $(this);
            $.ajax({
                url: '/customers/user_addresses/getStreets',
                type: 'post',
                dataType: 'json',
                data: {
                    city: city
                },
                success: function (r) {
                    $('.to-clear').val('');
                    if (r.status == 2) {
                        $('.zip_code').val(r.code);

                        var text = '<dt><label for="select_street" class="control-label">Strada</label></dt>'
                            + '<dd><input type="text" name="data[UserAddress][street_name]" '
                            + 'class="form-control to-clear"'
                            + 'autocomplete="off" /><div class="help-block"></div></dd>';
                        $('#street_wrapper').html(text);
                    } else if (r.status == 1) {
                        var select = '<dt><label for="select_street" class="control-label">Strada</label></dt>'
                            + '<dd><select name="UserAddress[street_name_2]"'
                            + 'class="form-control to-clear" id="select_street"'
                            + 'data-toggle="validator" data-live-search="true" >';
                        for(i = 0; i < r.streets.length; i++) {
                            select = select + '<option value="' + r.streets[i].id  +'">' + r.streets[i].name + '</option>';
                        }
                        select += '</select><div class="help-block"></div></dd>';
                        $('#street_wrapper').html(select);
                    }
                }
            })
        })
    })

    function checkZipCode()
    {
        $(document).on('change', '#select_street, .street_nr', function(){
            if (
                $('#select_street').val() != undefined
                && $('#select_street').val() != ''
                && $('.street_nr').val() != ''
            ) {
                var street = $('#select_street').val();
                var number = $('.street_nr').val();

                $.ajax({
                    url: '/customers/user_addresses/getZipCode',
                    type: 'post',
                    dataType: 'JSON',
                    data: {
                        street: street,
                        number: number
                    },
                    success: function(r) {
                        if (r.success == 1) {
                            $('.zip_code').val(r.code);
                        } else {
                            $('.zip_code').val('');
                        }
                    }
                })
            }
        })
    }
</script>
<?php $this->end(); ?>

<?php $full_name = $user['Person']['fname'] . ' ' . $user['Person']['lname']?>

<div class="row">
    <div class="col-lg-6">
        <?= $this->H->bslk(
            "/customers/users/edit/" . $user['User']['id'],
            'btn btn-warning btn-block',
            $this->Html->icon('fa fa-arrow-left') . __(' Inapoi la') . ' ' . $full_name,
            __(' Inapoi la contul lui ') . ' ' . $full_name
        )?>
        <div class="mg20"></div>
    </div>
</div>

<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header">
            <?php echo __("Adauga adresa pentru <span class='text-primary'>%s</span>", $full_name)?>
        </h1>
    </div>
</div>

<div class="row">
    <div class="col-md-4 col-md-offset-3">
        <?= $this->Form->create('UserAddress');?>
        <?=$this->Form->input('user_id',
            [
                'type' => 'hidden',
                'value' => $user['User']['id']
            ]);?>
        <?= $this->Form->input('country',
            [
                'label' => __('Tara'),
                'type' => 'select',
                'default' =>
                    [
                        'Romania' => 'Romania'
                    ]
            ]);?>
        <div class="row">
            <div class="col-md-6">
                <?= $this->Form->input('state',
                    [
                        'label' => __('Judet'),
                        'empty' => __('Selectati un judet')
                    ]);?>
            </div>
            <div class="col-md-6">
                <?= $this->Form->input('city',
                    [
                        'label' => __('Localitate'),
                        "type" => "select",
                        'empty' => __('Selectati o localitate')
                    ]);?>
            </div>
        </div>
        <div id="address_details_wrapper">
            <div id="street_wrapper">
                <?php if(isset($streets) && !empty($streets)): ?>
                    <?= $this->Form->input('street_name_2',
                        [
                            'label' => __('Strada'),
                            'options' => $streets,
                            'empty' => __('Selectati o strada'),
                            'selected' => $userStreet['Street']['id'],
                            'id' => 'select_street'
                        ]);?>
                <?php else: ?>
                    <?= $this->Form->input('street_name',
                        [
                            'label' => __('Strada')
                        ]);?>
                <?php endif; ?>
            </div>
        </div>
        <?= $this->Form->input('street_nr',
            [
                'label' => __('Numar'),
                'class' => 'form-control street_nr to-clear'
            ]);?>
        <?= $this->Form->input('zip',
            [
                'label' => __('Cod Postal'),
                'class' => 'form-control zip_code to-clear',
                'required' => true
            ]);?>
        <div class="row">
            <div class="col-md-6">
                <?= $this->Form->input('address_line_1',
                    [
                        'label' => __('Adresa (linia 1)')
                    ]);?>
            </div>
            <div class="col-md-6">
                <?= $this->Form->input('address_line_2',
                    [
                        'label' => __('Adresa (linia 2)')
                    ]);?>
            </div>
        </div>
        <?= $this->Form->submit(__('Adauga adresa'),
            [
                'class' => "btn btn-success btn-block btn-lg"
            ]);?>
        <?= $this->Form->end();?>
    </div>
</div>