<?=$this->start('page-title');?>
    <strong><?php echo __("Lista reselleri")?></strong>
<?=$this->end()?>
<div class="row mb20">
    <div class="col-lg-2 pull-right">
        <?= $this->H->bslk(
            "/customers/user_types/manage"
            . "?button_filter=1&User%5Bsales_last_month%5D%5Bnumeric_interval%"
            . "5D%5B%5D=&User%5Bsales_last_month%5D%5Bnumeric_interval%5D%5B%5D=0",
            'btn btn-success btn-xs btn-block',
            $this->Html->icon('fa fa-eye') . ' ' . __('Fara vanzari'),
            __("Fara vanzari")
        )?>
    </div>

    <div class="col-lg-2 pull-right">
        <?= $this->H->bslk(
            "/customers/user_types/manage"
            . "?button_filter=1&UserType%5Bupgrade%5D=1",
            'btn btn-success btn-xs btn-block',
            $this->Html->icon('fa fa-eye') . ' ' . __('Upgrade'),
            __("Upgrade")
        )?>
    </div>

    <div class="col-lg-2 pull-right">
        <?= $this->H->bslk(
            "/customers/user_types/manage"
            . "?button_filter=1&UserType%5Bdowngrade%5D=1",
            'btn btn-success btn-xs btn-block',
            $this->Html->icon('fa fa-eye') . ' ' . __('Downgrade'),
            __("Downgrade")
        )?>
    </div>
</div>
<div class="row manage-user-types">
    <div class="col-md-12 table-responsive">
        <table class="table table-condensed small">
            <thead>
            <tr>
                <th><?=__('ID')?></th>
                <th><?=__('Comentarii')?></th>
                <th><?=__('Nume')?></th>
                <th><?=__('Prenume')?></th>
                <th><?=__('Firma')?></th>
                <th><?=__('Email')?></th>
                <th><?=__('Tip')?></th>
                <th><?=__('Telefon')?></th>
                <th class="text-center"><?=__('Comenzi onorate')?></th>
                <th class="text-center"><?=__('Retururi')?></th>
                <th class="text-center"><?=__('Vanzari luna anterioara')?></th>
                <th class="text-center"><?=__('Vanzari luna curenta')?></th>
                <th style="min-width: 140px;"><?=__('Actiuni')?></th>
            </tr>
            <tr class="remove_form_group">
                <?php echo $this->element('Toolkit.filterForm')?>
            </tr>
            </thead>
            <tbody>
            <?php foreach ($users as $u):?>
                <tr>
                    <td><?=$u['User']['id']?></td>
                    <td>
                        <a data-link="/customers/user_types/showLog/<?=$u['User']['id']?>" class="show-log" data-name="<?=$u['Person']['lname']?> <?=$u['Person']['fname']?>" data-user_id="<?=$u['User']['id']?>" style="<?=($u['User']['comments']) ? 'color:#a94442;':'';?>">
                            <i class="fa fa-comments" aria-hidden="true"></i>
                        </a>
                    </td>
                    <td><?=$u['Person']['lname']?></td>
                    <td><?=$u['Person']['fname']?></td>
                    <td>
                        <?php if(!empty($u['UserCompany'])): ?>
                            <?php foreach($u['UserCompany'] as $company): ?>
                                <?=$company['name']?><br />
                            <?php endforeach; ?>
                        <?php endif; ?>
                    </td>
                    <td>
                        <a href="<?php echo Configure::read("STORE_URL")."login/".$u['User']['hash'].'/2/'.$u['User']['id']; ?>" title="<?= __('Login ca utilizator');?>">
                            <?=$u['User']['email']?>
                        </a>
                    </td>
                    <td class="text-center">
                        <?php if (isset($u['UserType']['hex_color'])):?>
                            <i class="fa fa-user" style="color: <?=$u['UserType']['hex_color']?>;" title="<?=$u['UserType']['name']?>"></i>
                        <?php else:?>
                            <i class="fa fa-user" style="color: #dddddd;" title="<?=__('Nimic')?>"></i>
                        <?php endif;?>
                    </td>
                    <td><?=$u['User']['phone']?></td>
                    <td class="text-center">
                        <a
                            target="_blank"
                            href="<?php echo "/orders/sales"
                                .'?button_filter=0'
                                .'&Sale%5Bsale_status_id%5D%5Bn%5D%5B%5D=11'
                                .'&Sale%5Buser_id%5D='.$u['User']['id'];?>">
                            <?=$u['User']['honored_orders']?>
                        </a>
                    </td>
                    <td class="text-center">
                        <a
                            target="_blank"
                            href="<?php echo "/rma/requests"
                                .'?User%5Bemail%5D='.$u['User']['email'];?>">
                            <?=$u['User']['rma_no']?>
                        </a>

                    </td>
                    <td class="text-center"><?=empty($u['User']['sales_last_month']) ? 0 : $u['User']['sales_last_month'];?> </td>
                    <td class="text-center"><?=empty($u['User']['sales_current_month']) ? 0 : $u['User']['sales_current_month'];?></td>
                    <td align="right">
                        <?=$this->Form->create(
                            'UserType', [
                                'onsubmit' => "return confirm('Sunteti sigur?');"
                            ]
                        )?>
                        <?=$this->Form->input(
                            'user_id', [
                                'type' => 'hidden',
                                'value' => $u['User']['id']
                            ]
                        );?>
                        <div class="inline-block">
                            <?=$this->Form->input(
                                'user_type_id', [
                                    'class' => 'form-control input-xs',
                                    'label' => false,
                                    'selected' => $u['User']['user_type_id']
                                ]
                            );?>
                        </div>
                        <div class="inline-block">
                            <?=$this->Form->submit(
                                'Aplica', [
                                    'class' => 'btn btn-info btn-xs'
                                ]
                            )?>
                        <?=$this->Form->end();?>
                        </div>
                    </td>
                </tr>
            <?php endforeach;?>
            </tbody>
        </table>
        <?php echo $this->element('Navigation/pagination'); ?>
    </div>
</div>
<div class="modal fade" id="comments-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Comentarii utilizator <span id="user-name"></span></h4>
            </div>
            <div class="modal-body">
                <div class="modal-text">

                </div>
                <hr />
                <form action="/customers/user_types/addLog" method="post">
                    <input type="hidden" name="log[user_id]" id="user-id" />
                    <textarea name="log[comment]" class="form-control input-xs" placeholder="Scrie comentariu" autocomplete="off" style="min-height: 120px;"></textarea>
                    <br/>
                    <button type="submit" class="btn btn-success btn-xs">
                        <i class="fa fa-plus"></i>&nbsp;Adauga comentariu</button>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>

<?=$this->start('extra-scripts');?>
<script>
    $(document).ready(function(){
        $(document).on('click', '.show-log', function(){
            var name = $(this).data('name');
            var user_id = $(this).data('user_id');
            var link = $(this).data('link');

            $.ajax({
                url: link,
                dataType: 'html',
                success: function(r) {
                    $('.modal-text').html(r);
                    $('#user-name').text(name);
                    $('#user-id').val(user_id);
                    $('#comments-modal').modal('show');
                }

            })
        });

        $('.manage-user-types form').on('click', 'input[type="submit"]', function(e) {
            e.preventDefault();
            var $form = $(this).parents('form');
            var form_data = $form.serialize();
            $.ajax({
                'url': '/customers/user_types/manage.json',
                'type': 'POST',
                'data': form_data,
                success: function () {
                    $form.parents("tr").effect('highlight', {}, 800);
                }
            });
            return false;
        });
    })
</script>
<?=$this->end();?>
