<?=$this->start('page-title');?>
<?php echo __("discount")?>
<?=$this->end() ?>

<div class="row">
    <div class="col-lg-12">
        <?= $this->H->bslk(
            "javascript:void(0)",
            'btn btn-success btn-block',
            $this->Html->icon('fa fa-save') . ' ' . __('Salveaza discount-urile'),
            __('Salveaza discount-urile'),
            'top',
            null,
            array('onClick' => "document.getElementById('CategoriesUserTypeDiscountsForm').submit();")
        )?>
        <div class="mg20"></div>
    </div>
    <!-- /.col-lg-6 -->
</div>
<!-- /.row -->

<div class="row">
    <div class="col-lg-12">
        <?php echo $this->Form->create('CategoriesUserType', array('onsubmit'=>'myFunction()')); ?>
        <table class="table table-floated small table-striped table-condensed">
            <thead>
            <tr>
                <th><?php echo __("Categorie")?></th>
                <?php foreach($user_types as $user_type){ ?>
                    <th width="100">
                        <div class="input-group text-center">
                            <?php echo $user_type['UserType']['name']; ?>
                        </div>
                    </th>
                <?php } ?>
            </tr>
            </thead>

            <tbody>
                <?php foreach($categories as $category): ?>
                    <tr>
                        <td>
                            <b></b><?php echo $category['Category']['name']; ?></b>
                        </td>
                        <?php foreach($user_types as $user_type): ?>
                            <td width="100">
                                <div class="input-group input-group-xs">
                                    <?php $value = 0; if(isset($discounts[$category['Category']['id'] . '_' . $user_type['UserType']['id']])):?>
                                        <?php $value = $discounts[$category['Category']['id'] . '_' . $user_type['UserType']['id']]['discount_percent'];?>
                                        <input class="unq-<?=$category['Category']['id']?>_<?=$user_type['UserType']['id']?>" type="hidden" data-name="data[<?=$category['Category']['id']?>_<?=$user_type['UserType']['id']?>][id]" value="<?=$discounts[$category['Category']['id'] . '_' . $user_type['UserType']['id']]['id']?>"/>
                                    <?php endif;?>
                                    <input class="unq-<?=$category['Category']['id']?>_<?=$user_type['UserType']['id']?>" type="hidden" data-name="data[<?=$category['Category']['id']?>_<?=$user_type['UserType']['id']?>][category_id]" value="<?=$category['Category']['id']?>"/>
                                    <input class="unq-<?=$category['Category']['id']?>_<?=$user_type['UserType']['id']?>" type="hidden" data-name="data[<?=$category['Category']['id']?>_<?=$user_type['UserType']['id']?>][user_type_id]" value="<?=$user_type['UserType']['id']?>"/>
                                    <input type="number" min="0" max="<?=Configure::read('MAX_PERCENT_COUPON')?>" data-unq="<?=$category['Category']['id']?>_<?=$user_type['UserType']['id']?>" data-name="data[<?=$category['Category']['id']?>_<?=$user_type['UserType']['id']?>][discount_percent]" value="<?=$value?>" class="form-control input-xs text-right customers-user-types-discounts unq-<?=$category['Category']['id']?>_<?=$user_type['UserType']['id']?>">
                                    <span class="input-group-addon input-xs" id="basic-addon2">%</span>
                                </div>
                            </td>
                        <?php endforeach; ?>
                    </tr>
                <?php endforeach; ?>
            </tbody>
        </table>
        <?php echo $this->Form->end(); ?>
    </div>
</div>
<?php $this->start('extra-scripts')?>
    <!-- html where are the misc js and css files?! -->
    <style>
        table.floatThead-table {
            border-top: none;
            border-bottom: none;
            background-color: #FFF;
        }
    </style>

    <script>
        $("table.table-floated").floatThead()
        $(document).on('change keyup', '.customers-user-types-discounts', function() {
            $('.unq-' + $(this).data('unq')).each(function() {
                $(this).attr('name', $(this).data('name'))
            });
        });

    </script>
    <!-- html where are the misc js and css files?! -->
<?php $this->end()?>