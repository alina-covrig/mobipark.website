<?php echo $this->element('Plugins/dataTables')?>
<?php $this->start('extra-scripts')?>
    <script type="text/javascript">
        $(document).ready(function() {
            $('#userTypesList').dataTable({
                "bProcessing": true,
                "bServerSide": true,
                "sAjaxSource": "/customers/user_types/getData/UserType.json",
            });
        });
    </script>
<?php $this->end()?>
<div class="row">
    <div class="col-md-9">
        <h1 class="page-header"><?php echo __("Lista tipuri utilizatori")?></h1>
        <div class="table-responsive">
            <table id="userTypesList" class="table table-striped table-bordered table-hover dataTable table-condensed">
                <thead>
                    <tr>
                        <?php foreach($displayFields as $Model => $fields) :?>
                            <?php foreach($fields as $field => $displayName) :?>
                                    <th><?php echo  $displayName ?></th>
                            <?php endforeach;?>
                        <?php endforeach;?>
                    </tr>
                </thead>
                <tbody data-location="/customers/user_types/edit/"/>
                    <tr>
                        <td colspan="99" class="dataTables_empty">Asteapta...</td>
                    </tr>
                </tbody>
            </table>
        </div>
        <div class="row">
            <div class="col-md-6">
                <?= $this->H->bslk(
                    "/customers/users",
                    'btn btn-info btn-block',
                    $this->Html->icon('fa fa-arrow-left') . __(' Vezi toti utilizatorii'),
                    __("Vezi toti utilizatorii")
                )?>
            </div>
        </div>
    </div>
    <div class="col-md-3 mt20">
        <div class="mg20"></div>
        <h2>Adauga tip utilizator</h2>
        <?php echo $this->Form->create('UserType', array('type' => 'file'));?>
            <?php echo $this->Form->input('name', array('label' => __('Denumire')));?>
            <?php echo $this->Form->input('icon', array('type' => 'file', 'label' => __('Icon')));?>
            <?php echo $this->Form->input('points_from', array('label' => __('Puncte de la')));?>
            <?php echo $this->Form->input('points_to', array('label' => __('Pana la')));?>
            <?php echo $this->Form->input('discount', ['label' => __('Val. Discount')]);?>
            <?php echo $this->Form->input('hex_color', ['label' => __('Cod hex')]);?>
            <?php echo $this->Form->input('delivery_nr', ['label' => __('Numar transporturi'), 'required' => false]);?>
            <?php echo $this->Form->input('delivery_method_id', ['label' => __('Metoda transport'), 'empty' => 'Selecteaza metoda de transport',  'required' => false]);?>
            <?php echo $this->Form->submit(__('Adauga'), array('class' => "btn btn-success"));?>
        <?php echo $this->Form->end();?>
    </div>
</div>
<!-- /.row -->