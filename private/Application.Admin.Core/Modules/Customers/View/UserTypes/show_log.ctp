<?php if(empty($comments)): ?>
    Nu exista comentarii pentru acest user
<?php else: ?>
    <?php $l = 0; foreach ($comments as $c):?>
        <div style="line-height: 18px;">
            <?php if(!empty($c['User']['AdminDetail']['picture'])) {
                echo '<img style="inline-block; width: 18px;  height: 18px;  border-radius: 18px; border:1px solid #DDD; box-shadow:1px 1px 2px #888" src="'.$c['User']['AdminDetail']['picture'].'">';
            } else {
                ?>
                <span style="display: inline-block;width: 18px; height: 18px; border-radius: 50px; color: <?=t_color(stringToColorCode($c['User']['Person']['fname'].$c['User']['Person']['lname']))?>;background-color: #<?=stringToColorCode($c['User']['Person']['fname'].$c['User']['Person']['lname'])?>;text-transform: uppercase;text-align: center;line-height: 18px;font-size: 9px;"><?=substr($c['User']['Person']['fname'], 0, 1)?><?=substr($c['User']['Person']['lname'], 0, 1)?></span>
            <?php } ?>
            <strong style="line-height: 18px;">
                <?=$c['User']['Person']['fname']?> <?=$c['User']['Person']['lname']?>
            </strong> <span class="small"><i
                    class="fa fa-clock-o fa-fw"></i> <?=$c['UserTypeLog']['created_at']?></span>:
            <?=$c['UserTypeLog']['comment']?>
        </div>
    <?php endforeach;?>
<?php endif; ?>
