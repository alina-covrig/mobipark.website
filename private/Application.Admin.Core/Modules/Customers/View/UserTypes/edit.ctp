<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header"><?php echo __("Modifica tip utilizator")?></h1>
    </div>
    <!-- /.col-lg-12 -->
</div>
<!-- /.row -->
<div class="row">
    <div class="col-lg-6">
        <?= $this->H->bslk(
            "/customers/user_types",
            'btn btn-warning btn-block',
            $this->Html->icon('fa fa-arrow-left') . __(' Inapoi la lista de tipuri utilizatori'),
            __("Inapoi la lista de tipuri utilizator")
        )?>
        <div class="mg20"></div>
    </div>
    <!-- /.col-lg-6 -->
    <div class="col-lg-6">
        <?= $this->H->bslk(
            "/customers/user_types/delete/" . $this->request->data['UserType']['id'],
            'btn btn-danger btn-block',
            $this->Html->icon('fa fa-trash-o') . ' Sterge tip utilizator',
            __("Sterge tip utilizator"),
            'top',
            __('Esti sigur ca vrei sa stergi acest tip de utilizator?')
        )?>
        <div class="mg20"></div>
    </div>
    <!-- /.col-lg-6 -->
</div>
<!-- /.row -->
<div class="row">
    <div class="col-lg-6">
        <?= $this->H->bslk(
            "/customers/users",
            'btn btn-info btn-block',
            $this->Html->icon('fa fa-arrow-left') . __(' Vezi toti utilizatorii'),
            __("Vezi toti utilizatorii")
        )?>
        <div class="mg20"></div>
    </div>
    <!-- /.col-lg-6 -->
</div>
<!-- /.row -->
<div class="row">
    <div class="col-md-12">
        <?php echo $this->Form->create('UserType', array('type' => 'file'));?>
            <?php echo $this->Form->input('id');?>
            <?php echo $this->Form->input('name', array('label' => __('Denumire')));?>
            <?php echo $this->Form->input('icon', array('type' => 'file', 'label' => __('Icon')));?>
            <?php echo $this->Form->input('points_from', array('label' => __('Puncte de la')));?>
            <?php echo $this->Form->input('points_to', array('label' => __('Pana la')));?>
            <?php echo $this->Form->input('discount');?>
            <?php echo $this->Form->input('hex_color');?>
            <?php echo $this->Form->input('delivery_nr', ['label' => __('Numar transporturi'), 'required' => false]);?>
            <?php echo $this->Form->input('delivery_method_id', ['label' => __('Metoda transport'), 'empty' => 'Selecteaza metoda de transport',  'required' => false]);?>
            <?php echo $this->Form->submit(__('Modifica tipul de utilizator'), array('class' => "btn btn-success"));?>
        <?php echo $this->Form->end();?>
    </div>
</div>