
<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header">
            <?= __("Atribuie metode de livrare pentru ")
            . $user['Person']['fname'] . ' ' . $user['Person']['lname']?>
        </h1>
    </div>
    <!-- /.col-lg-12 -->
</div>
<!-- /.row -->
<div class="row">
    <div class="col-lg-6">
        <?= $this->H->bslk(
            "/customers/users/edit/" . $user['User']['id'],
            'btn btn-warning btn-block',
            $this->Html->icon('fa fa-arrow-left') . __(' Inapoi la cont'),
            __(' Inapoi la cont ')
        )?>
        <div class="mg20"></div>
    </div>
    <!-- /.col-lg-6 -->
</div>
<!-- /.row -->
<div class="row">
    <div class="col-md-12">
        <?=$this->Form->create('UserDelivery');?>
        <?=$this->Form->input('user_id', array('type' => 'hidden', 'value' => $user['User']['id']));?>
        <?=$this->Form->input('delivery_method_id', ['label' => __('Metoda de transport')])?>
        <?=$this->Form->input('quantity', ['label' => __('Cantitate')])?>
        <?=$this->Form->submit(__('Adauga metoda de transport'), array('class' => "btn btn-success btn-block btn-lg"));?>
        <?=$this->Form->end();?>
    </div>
</div>