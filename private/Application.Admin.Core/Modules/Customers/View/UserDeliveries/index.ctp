<?=$this->start('page-title');?>
    <strong><?php echo __("Istoric metode de transport asignate")?></strong>
<?=$this->end()?>
    <div class="row" style="padding-top: 20px;">
        <div class="col-lg-3">
            <?= $this->H->bslk(
                "/customers/users/edit/" . $user_id,
                'btn btn-warning btn-block',
                $this->Html->icon('fa fa-arrow-left') . __(' Inapoi la utilizator'),
                __("Inapoi la utilizator")
            )?>
            <div class="mg20"></div>
        </div>
        <!-- /.col-lg-3 -->
    </div>
    <!-- /.row -->

    <div class="row">
        <div class="col-md-9">
            <table class="table table-condensed small">
                <thead>
                <tr>
                    <th><?=__('ID')?></th>
                    <th><?=__('Metoda transport')?></th>
                    <th><?=__('Comanda')?></th>
                    <th><?=__('Acordat de')?></th>
                    <th><?=__('Creat la')?></th>
                    <th style="min-width: 120px;"><?=__('Actiuni')?></th>
                </tr>
                <tr class="remove_form_group">
                    <?php echo $this->element('Toolkit.filterForm')?>
                </tr>
                </thead>
                <tbody>
                <?php foreach ($deliveries as $d):?>
                    <tr>
                        <td><?=$d['UserDelivery']['id']?></td>
                        <td><?=$d['DeliveryMethod']['name']?></td>
                        <td>
                            <?php if(!empty($d['UserDelivery']['sale_id'])): ?>
                                <a href="/orders/sales/getSaleData/<?=$d['UserDelivery']['sale_id']?>" title="Vezi comanda">
                                    <?=$d['UserDelivery']['sale_id']?>
                                </a>
                            <?php endif; ?>
                        </td>
                        <td>
                            <?=(!empty($d['Admin']['id'])) ? $d['Admin']['Person']['fname'] . ' ' . $d['Admin']['Person']['lname'] : "System"?>
                        </td>
                        <td><?=date('d.m.Y H:i', strtotime($d['UserDelivery']['created_at']))?></td>
                        <td align="right" width="50">
                            <div class="btn-group btn-group-xs">
                                <a href="/customers/user_deliveries/delete/<?=$d['UserDelivery']['id']?>/<?=$user_id?>"
                                   class="btn btn-danger btn-xs btn-outline"
                                   data-toggle="tooltip"
                                   data-placement="top"
                                   data-container="body"
                                   title=""
                                   data-original-title="Sterge"
                                   onclick="return confirm(&quot;Esti sigur ca vrei sa stergi transportul?&quot;)"><i class="fa fa fa-trash-o"></i></a>
                            </div>
                        </td>
                    </tr>
                <?php endforeach;?>
                </tbody>
            </table>
            <?php echo $this->element('Navigation/pagination'); ?>
        </div>
        <div class="col-md-3">
            <?=$this->Panel->start(__("Adauga metoda de transport"), 'panel-info', 1, 0)?>
                <?=$this->Form->create('UserDelivery', ['url' => ['action' => '/add/'. $user_id, 'controller' => 'user_deliveries']]);?>
                <?=$this->Form->input('user_id', array('type' => 'hidden', 'value' => $user_id));?>
                <?=$this->Form->input('delivery_method_id', ['label' => __('Metoda de transport')])?>
                <?=$this->Form->input('quantity', ['label' => __('Cantitate')])?>
                <?=$this->Form->submit(__('Adauga metoda de transport'), array('class' => "btn btn-success btn-block btn-lg"));?>
                <?=$this->Form->end();?>
            <?=$this->Panel->end()?>
        </div>
    </div>