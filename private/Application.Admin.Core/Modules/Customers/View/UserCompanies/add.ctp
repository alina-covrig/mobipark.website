<?php $this->start('extra-scripts'); ?>
<script type="text/javascript">
    $(document).ready(function(){
        $('#UserCompanyCountry').on('change', function(){
            var country = $(this).val();
            $('.to-clear').val('');
            var text = null;
            if(country != 'Romania'){
                text = '<div class="row"><div class="col-md-6">';
                text += '<dt><label for="select_county_int" class="control-label">Judet</dt></label>'
                    +'<dd><input type="text" name="data[UserCompany][state]"'
                    +'class="form-control to-clear"'
                    +'autocomplete="off"/></dd>';
                text += '</div><div class="col-md-6">';
                text += '<dt><label for="select_city_int" class="control-label">Localitate</dt></label>'
                    +'<dd><input type="text" name="data[UserCompany][city]"'
                    +'class="form-control to-clear"'
                    +'autocomplete="off"/></dd>';
                text += '</div>';
                $('#address_details_wrapper').html(text);
            }else{
                text = '<div class="row"><div class="col-md-6">';
                text += '<dt><label for="select_county" class="control-label">Judet</dt></label>'
                    + '<dd><select name="data[UserCompany][state]"'
                    + 'class="form-control to-clear"'
                    + 'autocomplete="off" id="UserCompanyState" /></dd>';
                text += '</div><div class="col-md-6">';
                text += '<dt><label for="select_city" class="control-label">Localitate</dt></label>'
                    + '<dd><select name="data[UserCompany][city]"'
                    + 'class="form-control to-clear"'
                    + 'autocomplete="off" id="UserCompanyCity"/></dd>';
                text += '</div>';
                $('#address_details_wrapper').html(text);

                $.ajax({
                    url:'/customers/user_addresses/getRomanianCounties',
                    type: 'post',
                    dataType: 'html',
                    data:{
                        country:country
                    },
                    success: function(r){
                        $('#UserCompanyState').html(r);
                    }
                });


            }
        });

        $('body').on('change', '#UserCompanyState', function() {
            var county = $(this).val();
            $('#UserCompanyCity').val('');
            $.ajax({
                url: '/customers/user_addresses/getRomanianCities',
                type: 'post',
                dataType: 'html',
                data: {
                    county: county
                },
                success: function (r) {
                    $('#UserCompanyCity').html(r)
                }
            })
        })
    })
</script>
<?php $this->end(); ?>

<?php $full_name = $user['Person']['fname'] . ' ' . $user['Person']['lname'] ?>

<div class="row">
    <div class="col-lg-6">
        <?= $this->H->bslk(
            "/customers/users/edit/" . $user['User']['id'],
            'btn btn-warning btn-block',
            $this->Html->icon('fa fa-arrow-left') . __(' Inapoi la') . ' ' . $full_name,
            __(' Inapoi la contul lui ') . ' ' . $full_name
        )?>
        <div class="mg20"></div>
    </div>
</div>

<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header">
            <?= __("Adauga companie pentru <span class='text-primary'>%s</span>", $full_name) ?>
        </h1>
    </div>
</div>

<div class="row">
    <div class="col-md-4 col-md-offset-3">
        <?= $this->Form->create('UserCompany');?>
        <?= $this->Form->input('user_id',
            [
                'type' => 'hidden',
                'value' => $user['User']['id']
            ]);?>

        <?= $this->Form->input('name',
            [
                'label' => __('Denumire'),
            ]);?>
        <div class="row">
            <div class="col-md-6">
                <?= $this->Form->input('VAT',
                    [
                        'label' => __('C.I.F.'),
                    ]);?>
            </div>
            <div class="col-md-6">
                <?= $this->Form->input('registered',
                    [
                        'label' => __('Nr. Registrul Comertului')
                    ]);?>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6">
                <?= $this->Form->input('bank_name',
                    [
                        'label' => __('Denumirea bancii')
                    ]);?>
            </div>
            <div class="col-md-6">
                <?= $this->Form->input('bank_account',
                    [
                        'label' => __('Cont IBAN')
                    ]);?>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6">
                <?= $this->Form->input('address_line_1',
                    [
                        'label' => __('Adresa (linia 1)')
                    ]);?>
            </div>
            <div class="col-md-6">
                <?= $this->Form->input('address_line_2',
                    [
                        'label' => __('Adresa (linia 2)')
                    ]);?>
            </div>
        </div>
        <?= $this->Form->input('country',
            [
                'label' => __('Tara'),
                'type' => 'select',
                'default' =>
                    [
                        'Romania' => 'Romania'
                    ]
            ]);?>
        <div id="address_details_wrapper">
            <div class="row">
                <div class="col-md-6">
                    <?= $this->Form->input('state',
                        [
                            'label' => __('Judet'),
                            'empty' => __('Selectati un judet'),
                            'class' => 'form-control to-clear',
                        ]);?>
                </div>
                <div class="col-md-6">
                    <?= $this->Form->input('city',
                        [
                            'label' => __('Localitate'),
                            "type" => "select",
                            'empty' => __('Selecteaza localitatea'),
                            'class' => 'form-control to-clear'
                        ]);?>
                </div>
            </div>
        </div>
        <?= $this->Form->input('zip',
            [
                'label' => 'Cod postal',
                'class' => 'form-control to-clear'
            ]);?>
        <?= $this->Form->input('emails',
            [
                'label' => 'Adrese de e-mail (despartite prin ";")',
                'type' => 'text',
                'class' => 'form-control to-clear'
            ]);?>
        <?= $this->Form->submit(__('Adauga companie'),
            [
                'class' => "btn btn-success btn-block btn-lg"
            ]);?>
        <?= $this->Form->end();?>
    </div>
</div>