<?=$this->start('page-title');?>
<strong><?php echo __("Lista utilizatori inactivati")?></strong>
<?=$this->end()?>
<div class="row" style="padding-top: 20px;">
    <div class="col-lg-2">
        <?= $this->H->bslk(
            "/customers/users",
            'btn btn-warning btn-block',
            $this->Html->icon('fa fa-eye') . __(' Vezi lista de utilizatori activi'),
            __("Vezi lista de utilizatori activi")
        )?>
        <div class="mg20"></div>
    </div>
</div>

<div class="row">
    <div class="col-md-12 table-responsive">
        <table class="table table-condensed table-striped small">
            <thead>
                <tr>
                    <th><?=__('ID')?></th>
                    <th><?=__('Nume')?></th>
                    <th><?=__('Prenume')?></th>
                    <th><?=__('Firma')?></th>
                    <th><?=__('Email')?></th>
                    <th><?=__('Tip')?></th>
                    <th><?=__('Telefon')?></th>
                    <th><?=__('Localitate')?></th>
                    <th><?=__('Judet')?></th>
                    <th><?=__('Comenzi')?><br /><?= __('Noi/Onorate')?></th>
                    <th><?=__('Data inregistrare')?></th>
                    <th><?=__('Note')?></th>
                    <th width="150"><?=__('Motiv anulare')?></th>
                    <th><?=__('Obs.')?></th>
                    <th></th>
                </tr>
                <tr class="remove_form_group">
                    <?php echo $this->element('Toolkit.filterForm')?>
                </tr>
            </thead>
            <tbody>
            <?php foreach ($users as $u):?>
                <tr>
                    <td>
                        <a href="/orders/invoices?Invoice%5Buser_id%5D=<?=$u['User']['id']?>" target="_blank">
                            <?=$u['User']['id']?>
                        </a>
                    </td>
                    <td><?=$u['Person']['lname']?></td>
                    <td><?=$u['Person']['fname']?></td>
                    <td></td>
                    <td>
                        <?=$u['User']['email']?>
                    </td>
                    <td class="text-center">
                    	<?php if (isset($s['User']['UserType']['hex_color'])):?>
                            <i class="fa fa-user" style="color: <?=$r['User']['UserType']['hex_color']?>;" title="<?=$r['User']['UserType']['name']?>"></i>
                        <?php else:?>
                            <i class="fa fa-user" style="color: #dddddd;" title="<?=__('Nimic')?>"></i>
                        <?php endif;?>
                    </td>
                    <td><?=$u['User']['phone']?></td>
                    <td>
                        <?php echo isset($u['UserAddress'][0]['city']) ? $u['UserAddress'][0]['city'] : ''; ?>
                    </td>
                    <td>
                        <?php echo isset($u['UserAddress'][0]['state']) ? $u['UserAddress'][0]['state'] : ''; ?>
                    </td>
                    <td>
                        <a
                            target="_blank"
                            href="<?php echo "/orders/sales"
                            .'?button_filter=0'
                            .'&Sale%5Bsale_status_id%5D%5Bn%5D%5B%5D=6'
                            .'&Sale%5Bsale_status_id%5D%5Bn%5D%5B%5D=9'
                            .'&Sale%5Bsale_status_id%5D%5Bn%5D%5B%5D=2'
                            .'&Sale%5Bsale_status_id%5D%5Bn%5D%5B%5D=10'
                            .'&Sale%5Bsale_status_id%5D%5Bn%5D%5B%5D=3'
                            .'&Sale%5Buser_id%5D='.$u['User']['id'];?>">
                            <?=$u['User']['not_honored_or_canceled_orders']; ?>
                        </a> / <a
                            target="_blank"
                            href="<?php echo "/orders/sales"
                            .'?button_filter=0'
                            .'&Sale%5Bsale_status_id%5D%5Bn%5D%5B%5D=11'
                            .'&Sale%5Buser_id%5D='.$u['User']['id'];?>"> 
                            <?=$u['User']['honored_orders']?>
                        </a>
                    </td>
                    <td><?=date('d.m.Y H:i', strtotime($u['User']['created_at']))?></td>
                    <td class="user_notes">
                       <a
                        href="#"
                        class="user-notes"
                        data-type="textarea"
                        data-pk="<?=$u['User']['id']?>"
                        data-name="User[notes]"
                        data-url="/customers/users/addNote/<?=$u['User']['id']?>.json"
                        data-action="/customers/users/addNote/<?=$u['User']['id']?>.json"
                        data-original-title="<?=__('Introdu nota admin-client')?>"
                        ><?=$u['User']['notes']?>
                        </a>
                    </td>
                    <td><?=$u['User']['cancel_reason']?></td>
                    <td class="user_notes">
                       <a
                        href="#"
                        class="user-notes"
                        data-type="textarea"
                        data-pk="<?=$u['User']['id']?>"
                        data-name="User[extra_notes]"
                        data-url="/customers/users/addExtraNote/<?=$u['User']['id']?>.json"
                        data-action="/customers/users/addExtraNote/<?=$u['User']['id']?>.json"
                        data-original-title="<?=__('Introdu mentiuni admin-client')?>"
                        ><?=$u['User']['extra_notes']?>
                        </a>
                    </td>
                    <td style="text-align: right;">
                        <a href="/customers/users/delete/<?=$u['User']['id']?>/undo"
                           class="btn btn-success btn-xs btn-outline"
                           data-toggle="tooltip"
                           data-placement="top"
                           data-container="body"
                           title="" data-original-title="Reactiveaza"
                           onclick="return confirm(&quot;Esti sigur ca vrei sa reactivezi utilizatorul?&quot;)"
                        ><i class="fa fa-share fa-flip-horizontal"></i></a>
                    </td>
                </tr>
            <?php endforeach;?>
            </tbody>
        </table>
        <?php echo $this->element('Navigation/pagination'); ?>
    </div>
</div>
<div style="display: none;" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true" id="customers-users-send-email-modal">
    <a href="#" data-toggle="modal" data-target="#customers-users-send-email-modal" id="customers-users-send-email-modal-trigger"></a>
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h4><?=__('Trimite email')?></h4>
            </div>
            <div class="modal-body">
                <label for="mail-subject"><?=__('Subiect');?></label>
                <input type="text" name="subject" id="mail-subject" class="form-control add-field" />
                <br />
                <label for="mail-message"><?=__('Mesaj');?></label>
                <textarea class="form-control add-field" name="message" id="mail-message"></textarea>
                <br />
                <button class="submit-batch-action pull-right btn btn-success"><?=__('Trimite');?></button>
                <div class="clearfix"></div>
            </div>
        </div>
    </div>
</div>
<?php echo $this->element('Plugins/inline-edit')?>
<?=$this->start('extra-scripts')?>
<script type="text/javascript" >
    $('.user-notes').editable();    
</script>
    <style type="text/css">
        td.user_notes .form-inline .form-control{
            width: 400px !important;
            height: 55px;
        }
        a.user-notes, a.sale-user_comments {
            text-decoration: none;
            color: inherit;
            /*width: 14px;*/
            border: none;
            display: inline-flex;
        }
    </style>
<?=$this->end()?>