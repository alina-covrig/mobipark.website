<!DOCTYPE html>
<html lang="ro">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title><?php echo __("Administrare ")?> | <?php echo Configure::read('STORE_NAME')?></title>
    <?php echo $this->Html->css('/admin/css/bootstrap.min.css')?>
    <?php echo $this->Html->css('/admin/css/sb-admin-2.css')?>
    <?php echo $this->Html->css('/admin/css/plugins/metisMenu/metisMenu.min.css')?>
    <?php echo $this->fetch('css')?>
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>

<body>

    <div class="container">
        <div class="row">
            <div class="col-md-4 col-md-offset-4">
                <div class="login-panel panel panel-default">
                    <div class="panel-heading">
                        <h3 class="panel-title"><?php echo __("Eroare 403 - zona neautorizata")?></h3>
                    </div>
                    <div class="panel-body">
                        <p>Imi pare rau<?php echo (isset($Person['fname']))?' ' . $Person['fname']:'';?>, dar ai ajuns intr-o zona neautorizata. Contacteaza-ti superiorul pentru acces la aceasta zona.</p>
                        <a href="javascript:window.history.back()" class="btn btn-danger btn-block">
                            <i class="fa fa-arrow-left"></i> Inapoi
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>


    <?php echo $this->Html->script('/admin/js/jquery-1.11.0.js')?>
    <?php echo $this->Html->script('/admin/js/bootstrap.min.js')?>
    <?php echo $this->Html->script('/admin/js/plugins/metisMenu/metisMenu.min.js')?>
    <?php echo $this->fetch('script')?>
    <?php echo $this->Html->script('/admin/js/sb-admin-2.js')?>
    <?php echo $this->fetch('extra-scripts')?>

</body>

</html>
