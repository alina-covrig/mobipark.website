<?php $this->start('extra-scripts')?>
    <script type="text/javascript">
        $('#UserIsActive').parent().parent().addClass('form-inline');
        $('#UserIsAdmin').parent().parent().addClass('form-inline');
    </script>
    <style>
        #PersonBdayMonth {
            width: 55%;
            display: inline-block;
        }
        #PersonBdayDay {
            width: 15%;
            display: inline-block;
        }
        #PersonBdayYear {
            width: 20%;
            display: inline-block;
        }
        label[for=PersonBdayMonth] {
            display: block;
        }
    </style>
<?php $this->end()?>
<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header"><?php echo __("Adauga utilizator nou")?></h1>
    </div>
</div>

<div class="row">
    <div class="col-lg-3">
        <?= $this->H->bslk(
            "/customers/users",
            'btn btn-warning btn-block',
            $this->Html->icon('fa fa-arrow-left') . __(' Inapoi la lista de utilizatori'),
            __("Inapoi la lista de utilizatori")
        )?>
        <div class="mg20"></div>
    </div>
</div>

<div class="row">
    <div class="col-lg-12">
        <?php echo $this->Form->create('User');?>
            <?php echo $this->Form->input('is_email_confirmed', array('type' => 'hidden'))?>
            <div class="row">
                <div class="col-lg-6">
                    <h3><?php echo __("Date personale")?></h3>
                    <?php echo $this->Form->input('Person.fname', array('label' => __('Prenume')))?>
                    <?php echo $this->Form->input('Person.lname', array('label' => __('Nume')))?>
                </div>
                <div class="col-lg-6">
                    <h3><?php echo __("Informatii utilizator")?></h3>
                    <div class="row">
                        <div class="col-lg-6">
                            <?php echo $this->Form->input('email', array('label' => __('E-Mail')))?>
                        </div>
                        <div class="col-lg-6">
                            <?php echo $this->Form->input('phone', array('label' => __('Telefon')))?>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-6">
                            <?php echo $this->Form->input('password', array('label' => __('Parola')))?>
                        </div>
                        <div class="col-lg-6">
                            <?php echo $this->Form->input('re-password', array('label' => __('Inca o data'), 'type' => 'password'))?>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-6">
                            <?php echo $this->Form->input('is_active', array('label' => __('Utilizator activ')))?>
                        </div>
                        <div class="col-lg-6">
                            <?php echo $this->Form->input('is_admin', array('label' => __('Administrator')))?>
                        </div>
                    </div>
                </div>
            </div>
            <?php echo $this->Form->submit(__('Adauga utilizator'), array('class' => "btn btn-success btn-lg btn-block", 'name' => 'data[User][add_user]'));?>
        <?php echo $this->Form->end();?>
    </div>
    <!-- /.col-lg-12 -->
</div>
<!-- /.row -->