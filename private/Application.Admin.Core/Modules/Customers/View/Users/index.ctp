<?=$this->start('page-title');?>
<strong><?php echo __("Lista utilizatori")?></strong>
<?=$this->end()?>
<div class="row" style="padding-top: 20px;">
    <div class="col-lg-5">
        <div class="row">
            <div class="col-lg-4">
                <?= $this->H->bslk(
                    "/customers/users/add",
                    'btn btn-warning btn-block',
                    $this->Html->icon('fa fa-plus') . __(' Adauga utilizator nou'),
                    __("Adauga utilizator nou")
                )?>
                <div class="mg20"></div>
            </div>
        </div>
    </div>
</div>
<!-- /.row -->

<div class="row">
    <div class="col-md-12 table-responsive">
        <table class="table table-condensed table-striped small">
            <thead>
                <tr>
                    <th><?=__('ID')?></th>
                    <th><?=__('Nume')?></th>
                    <th><?=__('Prenume')?></th>
                    <th><?=__('Email')?></th>
                    <th><?=__('Telefon')?></th>
                    <th><?=__('Data inregistrare')?></th>
                    <th><?=__('Note')?></th>
                    <th><?=__('Obs.')?></th>
                    <th style="min-width: 120px;"><?=__('Actiuni')?></th>
                </tr>
                <tr class="remove_form_group">
                    <?php echo $this->element('Toolkit.filterForm')?>
                </tr>
            </thead>
            <tbody>
            <?php foreach ($users as $u):?>
                <tr>
                    <td><?=$u['User']['id']?></td>
                    <td><?=$u['Person']['lname']?></td>
                    <td><?=$u['Person']['fname']?></td>
                    <td><?=$u['User']['email']?></td>
                    <td><?=$u['User']['phone']?></td>
                    <td><?=date('d.m.Y H:i', strtotime($u['User']['created_at']))?></td>
                    <td class="user_notes">
                       <a
                        href="#"
                        class="user-notes"
                        data-type="textarea"
                        data-pk="<?=$u['User']['id']?>"
                        data-name="User[notes]"
                        data-url="/customers/users/addNote/<?=$u['User']['id']?>.json"
                        data-action="/customers/users/addNote/<?=$u['User']['id']?>.json"
                        data-original-title="<?=__('Introdu nota admin-client')?>"
                        ><?=$u['User']['notes']?>
                        </a>
                    </td>
                    <td class="user_notes">
                       <a
                        href="#"
                        class="user-notes"
                        data-type="textarea"
                        data-pk="<?=$u['User']['id']?>"
                        data-name="User[extra_notes]"
                        data-url="/customers/users/addExtraNote/<?=$u['User']['id']?>.json"
                        data-action="/customers/users/addExtraNote/<?=$u['User']['id']?>.json"
                        data-original-title="<?=__('Introdu mentiuni admin-client')?>"
                        ><?=$u['User']['extra_notes']?>
                        </a>
                    </td>
                    <td align="right" width="50">
                        <div class="btn-group btn-group-xs">                          
                            <a href="/customers/users/delete/<?=$u['User']['id']?>" class="btn btn-danger btn-xs btn-outline" data-toggle="tooltip" data-placement="top" data-container="body" title="" data-original-title="Sterge" onclick="return confirm(&quot;Esti sigur ca vrei sa stergi utilizatorul?&quot;)"><i class="fa fa fa-trash-o"></i></a>
                            <a href="/customers/users/edit/<?=$u['User']['id']?>" class="btn btn-info btn-xs btn-outline" data-toggle="tooltip" data-placement="top" data-container="body" title="" data-original-title="Modifica"><i class="fa fa fa-pencil"></i></a>
                        </div>
                    </td>
                </tr>
            <?php endforeach;?>
            </tbody>
        </table>
        <?php echo $this->element('Navigation/pagination'); ?>
    </div>
</div>
<div style="display: none;" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true" id="customers-users-send-email-modal">
    <a href="#" data-toggle="modal" data-target="#customers-users-send-email-modal" id="customers-users-send-email-modal-trigger"></a>
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h4><?=__('Trimite email')?></h4>
            </div>
            <div class="modal-body">
                <label for="mail-subject"><?=__('Subiect');?></label>
                <input type="text" name="subject" id="mail-subject" class="form-control add-field" />
                <br />
                <label for="mail-message"><?=__('Mesaj');?></label>
                <textarea class="form-control add-field" name="message" id="mail-message"></textarea>
                <br />
                <button class="submit-batch-action pull-right btn btn-success"><?=__('Trimite');?></button>
                <div class="clearfix"></div>
            </div>
        </div>
    </div>
</div>
<?php echo $this->element('Plugins/inline-edit')?>
<?=$this->start('extra-scripts')?>
<script type="text/javascript" >
    $('.user-notes').editable();    
</script>
    <style type="text/css">
        td.user_notes .form-inline .form-control{
            width: 400px !important;
            height: 55px;
        }
        a.user-notes, a.sale-user_comments {
            text-decoration: none;
            color: inherit;
            /*width: 14px;*/
            border: none;
            display: inline-flex;
        }
    </style>
<?=$this->end()?>