<?php
/**
* UserTypesController
*
* Inside this Controller admin logic methods will
* be placed
*
* PHP version 5.4
*
* @category  Admin
* @package   Customers
* @author    Lucian Vasile <lucian.vasile@live.com>
* @copyright 2014 Evercloud
* @license   http://www.php.net/license/3_01.txt  PHP License 3.01
* @version   SVN: $Id$
* @link      /customers/user_types
* @since     1.0
*/

App::uses('Customers.CustomersAppController', 'Controller');

/**
* UserTypesController Class
*
* Inside this Controller admin logic methods will
* be placed
*
* @category  Admin
* @package   Customers
* @author    Lucian Vasile <lucian.vasile@live.com>
* @copyright 2014 Evercloud
* @license   http://www.php.net/license/3_01.txt  PHP License 3.01
* @version   Release: 1.0
* @link      /Customers/user_types
* @since     Class available since Release 1.0
* @property  Model Invoice
*/
class UserTypesController extends CustomersAppController
{
    /**
    * The components used by this controller
    * 
    * @var 
    */
    public $components = array(
        'Paginator',
        'Toolkit.FilterForm',
    );

    /**
     * The models used by this controller
     *
     * @var
     */
    public $uses = array(
        'Customers.UserType',
        'Customers.CategoriesUserType',
        'Catalog.Category',
        'Customers.Users',
        'Orders.Sale',
        'Orders.Invoice',
        'Orders.DeliveryMethod',
        'Customers.UserTypeLog'

    );

    /**
    * The displayFields property
    * 
    * This property is used by dataTables.js to extract data
    * from the model and to generate the table in frontend
    * 
    * @var 
    */
    public $displayFields = array(
        'UserType' => array(
            'id' => 'ID',
            'name' => 'Denumire',
            'points_from' => 'Puncte de la',
            'points_to' => 'Pana la',
            'discount' => 'Discount',
            'hex_color' => 'Cod culoare',
            'delivery_nr' => 'Numar transporturi',
            'delivery_method_name' => 'Metoda transport',
        ),
    );
    
    /**
    * The index method
    * 
    * This method list the user types and a form
    * to add a new user type
    * It also contains the logic to add a new user type
    * Renders /Customers/View/UserTypes/index.ctp
    * 
    * @return void or redirect on success
    */
    public function index() 
    {
        if ($this->request->is('post')) {
            if ($this->UserType->save($this->request->data)) {
                $this->Session->setFlash(
                    __('Tipul de utilizator a fost adaugat!'), 
                    'Alerts/success'
                );
                return $this->redirect('/customers/user_types');
            }
        }

        $deliveryMethods = $this->DeliveryMethod->find(
            'list', [
                'conditions' => [
                    'is_active' => 1
                ]
            ]
        );
        $this->set('displayFields', $this->displayFields);
        $this->set(compact('deliveryMethods'));
    }
    
    /**
    * The Edit user type method
    * 
    * This method allows editing an user type
    * Renders /Customers/View/UserTypes/edit.ctp
    * 
    * @param integer $user_type_id the id of the user type to edit
    * 
    * @return void or redirect on put or error
    */
    public function edit($user_type_id = null) 
    {
        if ($this->request->is('put')) {
            if ($this->UserType->save($this->request->data)) {
                $this->Session->setFlash(
                    __('Tipul a fost modificat!'), 
                    'Alerts/success'
                );
                return $this->redirect('/customers/user_types');
            }
        }

        $deliveryMethods = $this->DeliveryMethod->find(
            'list', [
                'conditions' => [
                    'is_active' => 1
                ]
            ]
        );

        $this->UserType->id = $user_type_id;
        if (!$this->UserType->exists()) {
            $this->Session->setFlash(
                __('Tip utilizator inexistent'), 
                'Alerts/warning'
            );
            return $this->redirect('/customers/user_types');
        }
        $options = array(
            'conditions' => array(
                'UserType.id' => $user_type_id
            )
        );
        $this->request->data = $this->UserType->find('first', $options);
        $this->set(compact('deliveryMethods'));
    }
    
    /**
    * The delete User type method
    * 
    * Currently not implemented
    *
    * @param integer $user_type_id the id of the user type to be deleted
    * 
    * @todo delete only if no user has this type
    * 
    * @return redirect
    */
    public function delete($user_type_id = null) 
    {
        $this->UserType->id = $user_type_id;
        if (!$this->UserType->exists()) {
            $this->Session->setFlash(
                __('Tip utilizator inexistent'), 
                'Alerts/warning'
            );
            return $this->redirect('/customers/user_types');
        }
        // Stergere tipuri utilizatori???
        $this->Session->setFlash(
            __('TBD despre stergerea tipurilor utilizatorilor'), 
            'Alerts/warning'
        );
        return $this->redirect('/customers/user_types');
    }


    /**
     * The discounts type method
     *
     * List of discounts user type X categories
     *
     * @return redirect
     */
    public function discounts()
    {
        if ($this->request->is('post')) {
            sort($this->request->data);
            if (!$this->CategoriesUserType->saveMany($this->request->data)) {
                $a = isset(
                    $this->CategoriesUserType
                        ->validationErrors[0]['discount_percent'][0]
                );
                if ($a) {
                    return $this->respond(
                        $this->CategoriesUserType
                            ->validationErrors[0]['discount_percent'][0],
                        'error'
                    );
                }

                return $this->respond(
                    __('Eroare la salvarea discounturilor :('),
                    'error'
                );
            }
            return $this->respond(
                __('Discounturi salvate!'),
                'success'
            );
        }

        $categories = $this->Category->find(
            'all', array(
                'fields' => array(
                    'Category.id','Category.name', 'Category.rght',
                    'Category.lft',
                ),
                'group' => array(
                    'Category.id HAVING `Category`.`rght` - `Category`.`lft` = 1'
                ),
            )
        );

        $user_types = $this->UserType->find(
            'all', array(
                'fields' => array(
                    'UserType.id','UserType.name',
                ),
                'order' => array(
                    'UserType.points_from' => 'ASC'
                )
            )
        );
        $this->CategoriesUserType->Behaviors->load('Containable');
        $this->CategoriesUserType->contain();
        $this->CategoriesUserType->virtualFields = array(
            'key' => 'CONCAT(category_id, "_", user_type_id)'
        );
        $discounts = $this->CategoriesUserType->find('all');
        $pd = array();

        foreach ($discounts as $discount) {
            $pd[$discount['CategoriesUserType']['key']]['id']
                = $discount['CategoriesUserType']['id'];
            $pd[$discount['CategoriesUserType']['key']]['category_id']
                = $discount['CategoriesUserType']['category_id'];
            $pd[$discount['CategoriesUserType']['key']]['user_type_id']
                = $discount['CategoriesUserType']['user_type_id'];
            $pd[$discount['CategoriesUserType']['key']]['discount_percent']
                = $discount['CategoriesUserType']['discount_percent'];
        }


        $this->set('user_types', $user_types);
        $this->set('categories', $categories);
        $this->set('discounts', $pd);
    }

    /**
     * The updateUserTypes method
     *
     * Updates
     *
     * @return redirect
     */
    public function updateUserTypes()
    {
        $month = (int) date('m');
        $last_month = $month - 1;
        $this->User->query(
            'CALL calculate_new_user_types_based_on_'
            . 'invoices_in_given_month(' . $last_month . ')'
        );
        die('successfully called');
    }

    /**
     * The updateUserTypesEmailCron method
     *
     * Send e-mail when updated cron
     *
     * @return redirect
     */
    public function updateUserTypesEmailCron()
    {
        $utype_changes = $this->User->query(
            'SELECT * FROM backups__new_user_types WHERE '
            .' user_type_id != new_user_type_id '
        );

        foreach ($utype_changes as $utc) {
            $this->updateUserTypesEmail(
                $utc['backups__new_user_types']['user_id'],
                $utc['backups__new_user_types']['new_user_type_id']
            );
        }
        die('done');
    }

    /**
     * Manage resellers
     *
     * @return mixed
     */
    public function manage()
    {
        if ($this->request->is('ajax')) {
            $this->layout = false;
            $this->autoRender = false;

            $data = $this->request->data['UserType'];
            $this->User->id = $data['user_id'];
            if ($this->User->saveField('user_type_id', $data['user_type_id'])) {
                return $this->respond(
                    __('Tipul de utilizator a fost schimbat cu succes'),
                    'success'
                );
            }

            return $this->respond(
                __('A intervenit o eroare la schimbarea tipului de utilizator'),
                'error'
            );
        }

        $this->_setFilterFields();

        $conditions = [];
        $joins = [];

        if (!empty($this->request->query)) {
            $conditions
                = $this->FilterForm->parseConditions($this->request->query);
        }

        // query optimization in order to use `created_at` index
        $first_day_last_month = date(
            'Y-m-1 00:00:00',
            strtotime('first day of last month')
        );
        $last_day_last_month = date(
            'Y-m-t 23:59:59',
            strtotime('first day of last month')
        );

        $sales_last_month_query = "SELECT COALESCE(SUM(value),0) "
            . "FROM orders__invoices i "
            . "INNER JOIN orders__sales s ON i.sale_id = s.id "
            . "WHERE i.created_at BETWEEN '" . $first_day_last_month . "' "
            . "AND '" . $last_day_last_month . "' "
            . "AND user_id = User.id AND sale_status_id = 11";

        $sales_current_month_query = "SELECT COALESCE(SUM(value),0) "
            . "FROM orders__invoices i "
            . "INNER JOIN orders__sales s ON i.sale_id = s.id "
            . "WHERE i.created_at > '" . $last_day_last_month . "' "
            . "AND user_id = User.id AND sale_status_id = 11";

        if (isset($conditions['UserType.upgrade'])) {
            $conditions[] = [
                '(' . $sales_last_month_query. ") > points_to"
            ];

            unset($conditions['UserType.upgrade']);
        }

        if (isset($conditions['UserType.downgrade'])) {
            $conditions[] = [
                '(' . $sales_last_month_query .") < points_from"
            ];

            unset($conditions['UserType.downgrade']);
        }

        if (isset($conditions['UserCompany.name LIKE'])
            || isset($conditions['UserCompany.name LIKE'])
        ) {
            $joins[] = array(
                'table' => 'customers__user_companies',
                'alias' => 'UserCompany',
                'conditions' => 'UserCompany.user_id = User.id',
                'type' => 'LEFT'
            );
        }

        $this->User->bindModel(
            array(
                'hasMany' => array(
                    'UserAddress' => array(
                        'className' => 'Customers.UserAddress'
                    ),
                    'UserCompany' => array(
                        'className' => 'Customers.UserCompany',
                        'fields' => ['name']
                    ),
                )
            )
        );

        $this->User->virtualFields = array(
            'honored_orders' => 'SELECT count(id) '
                . 'FROM orders__sales os '
                . 'WHERE os.user_id = User.id '
                . 'AND sale_status_id = 11',
            'rma_no' => 'SELECT count(id) '
                . 'FROM rma__requests WHERE user_id = User.id',
            'sales_last_month' => $sales_last_month_query,
            'sales_current_month' => $sales_current_month_query,
            'comments' => 'SELECT count(id) '
                . 'FROM customers__user_type_logs '
                . 'WHERE user_id = User.id'
        );

        $userTypes = $this->User->UserType->find('list');

        $conditions[] = [
            'NOT' => [
                ['User.user_type_id' => null]
            ]
        ];

        $this->Paginator->settings = array(
            'conditions' => $conditions,
            'joins' => $joins,
            'order' => array(
                'User.id' => 'ASC'
            ),
            'group' => array("User.id")
        );

        $users = $this->Paginator->paginate('User');
        $this->set(compact('users', 'userTypes'));
    }

    /**
     * The _setFilterFields method
     *
     * This method sets the filterForm variable to the view to generate the
     * filter row
     *
     * @return void
     */
    private function _setFilterFields()
    {
        $filterForm = array(
            'name' => '',
            'reset' => '/customers/user_types/manage',
            'action' => '/users',
            'fields' => array(
                'User[id]' => array(
                    'name' => 'User[id]',
                    'label' => 'Id',
                    'width' => 50
                ),
                null,
                'Person[lname]' => array(
                    'name' => 'Person[lname]',
                    'label' => __('Nume'),
                ),
                'Person[fname]' => array(
                    'name' => 'Person[fname]',
                    'label'=> __('Prenume'),
                ),
                'UserCompany[name]' => array(
                    'name' => 'UserCompany[name]',
                    'label'=> __('Firma'),
                    'width' => '100',
                ),
                'User[email]' => array(
                    'name' => 'User[email]',
                    'label' => __('Email')
                ),
                'User[user_type_id]' => array(
                    'name' => 'User[user_type_id]',
                    'label' => __('Tip'),
                    'data' => 'userTypes',
                    'width' => '75'
                ),
                'User[phone]' => array(
                    'name' => 'User[phone]',
                    'label' => __('Telefon'),
                    'width' => '75'
                ),
                'User[honored_orders]' => array(
                    'name' => 'User[honored_orders]',
                    'label' => __('Comenzi onorate'),
                    'width' => '30',
                    'is_interval' => true,
                ),
                'User[rma_no]' => array(
                    'name' => 'User[rma_no]',
                    'label' => __('Retururi'),
                    'width' => '30',
                    'is_interval' => true,
                ),
                'User[sales_last_month]' => array(
                    'name' => 'User[sales_last_month]',
                    'label' => __('Vanzari luna anterioara'),
                    'width' => '30',
                    'is_interval' => true,
                ),
                    'User[sales_current_month]' => array(
                    'name' => 'User[sales_current_month]',
                    'label' => __('Vanzari luna curenta'),
                    'width' => '30',
                    'is_interval' => true,
                ),
            ),
        );
        $this->set(compact('filterForm'));
    }

    /**
     * Add commentary
     *
     * @return mixed
     */
    public function addLog()
    {
        if (!$this->request->is('post')) {
            return $this->respond(
                __('Date inconsistente'),
                'error'
            );
        }

        $data = $this->request->data['log'];
        $data['admin_id'] = $this->getCurrentUserId();
        $data['created_at'] = date('Y-m-d H:i:s');

        if ($this->UserTypeLog->save($data)) {
            return $this->respond(
                __('Comentariul a fost salvat cu succes'),
                'success'
            );
        }

        return $this->respond(
            __('A aparut o eroare la salvarea comentariului'),
            'error'
        );
    }

    /**
     * Show user type log
     *
     * @param int $id - user id
     *
     * @return mixed
     */
    public function showLog($id)
    {
        $this->layout = false;

        $this->UserTypeLog->bindModel(
            [
                'belongsTo' => [
                    'User' => [
                        'className' => 'Customers.User',
                        'foreignKey' => 'admin_id'
                    ],
                ],
            ]
        );

        $this->User->bindModel(
            [
                'hasOne' => [
                    'AdminDetail' => [
                        'className' => 'Customers.AdminDetail'
                    ]
                ]
            ]
        );

        $this->UserTypeLog->Behaviors->load('Containable');
        $this->UserTypeLog->contain(
            [
            'User' => [
                'AdminDetail',
                'Person'
            ]
            ]
        );

        $comments = $this->UserTypeLog->find(
            'all', [
                'conditions' => [
                    'user_id' => $id
                ],
                'order' => ['UserTypeLog.created_at DESC']
            ]
        );

        $this->set(compact('comments'));
    }
}
