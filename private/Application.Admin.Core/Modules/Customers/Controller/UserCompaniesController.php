<?php
/**
* UserCompaniesController
*
* Inside this Controller admin logic methods will
* be placed
*
* PHP version 5.4
*
* @category  Admin
* @package   Customers
* @author    Lucian Vasile <lucian.vasile@live.com>
* @copyright 2014 Evercloud
* @license   http://www.php.net/license/3_01.txt  PHP License 3.01
* @version   SVN: $Id$
* @link      /customers/user_companies
* @since     1.0
*/

App::uses('Customers.CustomersAppController', 'Controller');

/**
* UserCompaniesController Class
*
* Inside this Controller admin logic methods will
* be placed
*
* @category  Admin
* @package   Customers
* @author    Lucian Vasile <lucian.vasile@live.com>
* @copyright 2014 Evercloud
* @license   http://www.php.net/license/3_01.txt  PHP License 3.01
* @version   Release: 1.0
* @link      /Customers/user_companies
* @since     Class available since Release 1.0     
*/
class UserCompaniesController extends CustomersAppController
{
    /**
    * The components used by this controller
    * 
    * @var 
    */
    public $component = array('Paginator');
    
    /**
    * The models used by this controller
    * 
    * @var 
    */
    public $uses = array(
        'Customers.User',
        'Customers.UserCompany',
        'Customers.Country',
        'Customers.County',
        'Customers.City',
    );
    
    /**
    * The displayFields property
    * 
    * This property is used by dataTables.js to extract data
    * from the model and to generate the table in frontend
    * 
    * @var 
    */
    public $displayFields = array(
        'UserCompany' => array(
            'id' => 'ID',
            'name' => 'Adresa',
        ),
    );
    
    /**
    * The Add company method
    * 
    * This method adds an adress for a user
    * Renders /Customers/View/UserCompanies/add.ctp
    * 
    * @param integer $user_id the user id to add an company to
    *
    * @return mixed
    */
    public function add($user_id = null) 
    {
        $this->User->id = $user_id;
        if (!$this->User->exists()) {
            $this->Session->setFlash(
                __('Utilizator inexistent'), 
                'Alerts/warning'
            );
            return $this->redirect('/customers/users');
        }

        $user = $this->User->findById($user_id);
        
        if ($this->request->is('post')) {
            $update = (
                $user['User']['source'] == Configure::read('USER_EXPORT_TYPE')
            )
                ? $this->UserCompany->save(
                    $this->request->data,
                    ['validate' => false]
                )
                : $this->UserCompany->save($this->request->data);
            if ($update) {
                return $this->respond(
                    __('Compania a fost adaugata!'),
                    'success',
                    '/customers/users/edit/' . $user['User']['id']
                );
            }
        }
        $options = array(
            'conditions' => array(
                'User.id' => $user_id
            )
        );
        $countries = $this->_getCountries();
        $this->set(compact('countries'));

        $states = $this->_getCounties();
        $this->set(compact('states'));
        
        $user = $this->User->find('first', $options);
        $this->set(compact('user'));
    }
    
    /**
    * The Edit company method
    * 
    * This method allows editing an user company
    * Renders /Customers/View/UserCompanies/edit.ctp
    * 
    * @param integer $id the id of the company to edit
    * 
    * @return mixed
    */
    public function edit($id = null) 
    {
        $this->UserCompany->id = $id;
        if (!$this->UserCompany->exists()) {
            $this->Session->setFlash(
                __('Adresa inexistenta'), 
                'Alerts/warning'
            );
            return $this->redirect('/customers/users');
        }
        $options = array(
            'conditions' => array(
                'UserCompany.id' => $id
            )
        );
        $this->UserCompany->recursive = 2;
        $user_company = $this->UserCompany->find('first', $options);
        $this->set('user', array('User' => $user_company['User']));
        $this->set('company', $user_company['UserCompany']);

        if ($this->request->is('put')) {
            $update = (
                $user_company['User']['source']
                == Configure::read('USER_EXPORT_TYPE')
            )
                ? $this->UserCompany->save(
                    $this->request->data,
                    ['validate' => false]
                )
                : $this->UserCompany->save($this->request->data);
            if ($update) {
                return $this->respond(
                    __('Compania a fost modificata!'),
                    'success',
                    '/customers/users/edit/' . $user_company['User']['id']
                );
            }
        }

        $countries = $this->_getCountries();
        $this->set(compact('countries'));

        $companyCountry = $user_company['UserCompany']['country'];
        $country = $this->Country->findByName($companyCountry);
        
        $states = $this->_getCounties();
        $countyName = $user_company['UserCompany']['state'];
        $county = $this->County->findByName($countyName);

        if ($country == $this->Country->findByName('Romania')) {
            $cities = $this->City->find(
                'list', array(
                    'conditions' => array(
                        'City.county_id' => $county['County']['id']
                    ),
                    'fields' => array(
                        'City.name',
                        'City.name'
                    )
                )
            );
            $this->set(compact('states', 'cities'));
        }

        $this->request->data = $user_company;
    }
    
    /**
    * The Delete company method
    * 
    * This method deletes an company from an user
    * 
    * @param integer $id      the id of the company to be deleted
    * @param integer $user_id the id of the user (helper for redirect)
    * 
    * @return redirect
    */
    public function delete($id = null, $user_id = null) 
    {
        
        $this->User->id = $user_id;
        if (!$this->User->exists()) {
            $this->Session->setFlash(
                __('Utilizator inexistent'), 
                'Alerts/warning'
            );
            return $this->redirect('/customers/users');
        }
        
        $this->UserCompany->id = $id;
        if (!$this->UserCompany->exists()) {
            $this->Session->setFlash(
                __('Companie inexistenta'), 
                'Alerts/warning'
            );
            return $this->redirect('/customers/users/edit/' . $user_id);
        }
        // Stergere companii utilizatori???
        if ($this->UserCompany->delete($id)) {
            $this->Session->setFlash(
                __('Companie stearsa'), 
                'Alerts/success'
            );
        } else {
            $this->Session->setFlash(
                __('Eroare la stergerea companiei'), 
                'Alerts/error'
            );
        }
        return $this->redirect('/customers/users/edit/' . $user_id);
    }

    /**
     * The _getCountries method
     *
     * Returns array of [Country.name]=>[CountryName]
     *
     * @return array
     */
    private function _getCountries()
    {
        $countries = $this->Country->find(
            'list',
            [
                'order' => 'name',
                'fields' => [
                    'Country.name',
                    'Country.name'
                ]
            ]
        );

        return $countries;
    }
    
    /**
     * The _getCounties method
     *
     * This method get the Counties
     *
     * @return array
     */
    private function _getCounties()
    {
        $counties = $this->County->find(
            'list', array(
            'order' => 'name',
            'fields' => array(
                'County.name', 
                'County.name'
            )
            )
        );
            
        return $counties;
    }

    /**
     * The searchAutocomplete method
     *
     * This method is used by typeahead.js to return products
     * in a text field
     *
     * @return respond
     */
    public function searchAutocomplete()
    {
        if (isset($_GET['query']) && !empty($_GET['query'])) {
            $data = $this->UserCompany->search($_GET['query']);
            $returnData = array();
            foreach ($data as $key => $value) {
                $returnData[$key]['id'] = $value['UserCompany']['id'];
                $returnData[$key]['name'] = $value['UserCompany']['name'];
            }

            die(json_encode($returnData));
        } else {
            return $this->respond(
                __('Intoduceti un termen de cautare')
            );
        }
    }
}
