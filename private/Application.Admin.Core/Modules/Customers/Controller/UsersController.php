<?php
/**
* UsersController
*
* Inside this Controller admin logic methods will
* be placed
*
* PHP version 5.4
*
* @category  Admin
* @package   Customers
* @author    Lucian Vasile <lucian.vasile@live.com>
* @copyright 2014 Evercloud
* @license   http://www.php.net/license/3_01.txt  PHP License 3.01
* @version   SVN: $Id$
* @link      /customers/users
* @since     1.0
*/

App::uses('Customers.CustomersAppController', 'Controller');

/**
* UsersController Class
*
* Inside this Controller admin logic methods will
* be placed
*
* @category  Admin
* @package   Customers
* @author    Lucian Vasile <lucian.vasile@live.com>
* @copyright 2014 Evercloud
* @license   http://www.php.net/license/3_01.txt  PHP License 3.01
* @version   Release: 1.0
* @link      /Customers/users
* @since     Class available since Release 1.0     
*/
class UsersController extends CustomersAppController
{
    /**
    * The components used by this controller
    * 
    * @var 
    */    
    public $components = array(
        'Paginator',
        'Toolkit.FilterForm',
    );
    
    /**
    * The displayFields property
    * 
    * This property is used by dataTables.js to extract data
    * from the model and to generate the table in frontend
    * 
    * @var 
    */
    public $displayFields = array(
        'User' => array(
            'id' => 'ID',
            'email' => 'Email',
            'phone' => 'Telefon',
        ),
        'Person' => array(
            'fname' => 'Prenume',
            'lname' => 'Nume',
        )
    );

    /**
     * The models used by this controller
     *
     * @var
     */
    public $uses = array(
        'Customers.AdminDetail',
        'Customers.UserSetting'
    );

    /**
    * The index method
    * 
    * This method lists the current users
    * It sets the $displayFields to the view to generate the dataTables
    * Renders /Customers/View/Users/index.ctp
    * 
    * @return void
    */
    public function index() 
    {
        ini_set('memory_limit', '2048M');
        $this->_setFilterFields();
        
        $conditions = array();
        $joins = array();
        
        if (isset($this->request->query['report'])) {
            unset($this->request->query['report']);
        } 
        
        if (!empty($this->request->query)) {
            $conditions
                = $this->FilterForm->parseConditions($this->request->query);
        }

        if (empty($this->request->query)) {
            $optimization_conditions =
                [
                    'User.is_admin' => 1
                ];
        }

        $conditions['User.is_active'] = 1;
        
        $this->User->contain(
            [
                'Person',
                'AdminDetail',
                'UserType'
            ]
        );
        
        if (isset($_GET['report'])) {

            pr("report");
            
            ini_set('memory_limit', '-1');
            ini_set('max_execution_time', 0);
            
            $path = "files/exports/";
            if (!file_exists($path)) { 
                mkdir($path, 755, true); 
            }
            
            $name = time().'_users.csv';
            $file_name = $path.$name;
            
            $users = $this->User->find(
                'all', array(
                'conditions' => $conditions,
                'joins' => $joins,
                'order' => array(
                    'User.id' => 'ASC'
                )
                )
            );
            
            $fh           = fopen($file_name, 'w');
            $hp1 = 'Id;Nume;Prenume;Email;"Tip Reseller";';
            $hp2 = 'Localitate;Judet;"Nr comenzi(Noi sau in procesare/';
            $header = $hp1.$hp2.'Onorate)";"Data inregistrare";Note;';
            $table_header = $header."\n";

            fwrite($fh, $table_header);
            
            foreach ($users as $u) {
                $line = $u['User']['id'].'; "';
                $line .= $u['Person']['lname'].'"; "';
                $line .= $u['Person']['fname'].'"; "';
                $line .= (isset($u['UserType']['name']) ?
                        $u['UserType']['name'] : '-') .'"; "';
                $line .= (isset($u['UserAddress'][0]['city']) ?
                        $u['UserAddress'][0]['city'] : '-') .'"; "';
                $line .= (isset($u['UserAddress'][0]['state']) ?
                        $u['UserAddress'][0]['state'] : '-') .'"; "';
                $line .= $u['User']['not_honored_or_canceled_orders']." / "
                    .$u['User']['honored_orders'].'"; "';
                $line .= date('d.m.Y H:i', strtotime($u['User']['created_at']))
                    .'"; "';
                $line .= $u['User']['notes'].'"; ';
                $line .= "\n";
                fwrite($fh, $line);
            }
            fclose($fh);

            header("Content-type: application/force-download");
            header('Content-Disposition: inline; filename='.$name);
            header("Content-Transfer-Encoding: Binary");
            header("Content-length: ".filesize($file_name));
            header('Content-Type: application/octet-stream');
            header('Content-Disposition: attachment; filename='.$name);
            readfile($file_name);

            $this->set('download_URL', $file_name);
           
            die();
        }

        $userTypes = $this->User->UserType->find('list');

        if (!empty($optimization_conditions)) {
            $conditions += $optimization_conditions;
        }

        $this->Paginator->settings = array(
            'conditions' => $conditions,
            'joins' => $joins,
            'order' => array(
                'User.id' => 'ASC'
            ),
            'group' => array("User.id")
        );     
        
        $users = $this->Paginator->paginate('User');
        //$db = $this->User->getDataSource(); debug($db->getLog()); die;
        $this->set(compact('users', 'userTypes'));
        //$this->set('displayFields', $this->displayFields);
    }
    
    /**
    * The Add user method
    * 
    * This method adds an user
    * Renders /Customers/View/Users/add.ctp
    * 
    * @return void or redirect on post
    */
    public function add() 
    {
        if ($this->request->is('post')) {
            if ($this->User->saveAssociated($this->request->data)) {
                $this->Session->setFlash(
                    __('Utilizatorul a fost adaugat!'), 
                    'Alerts/success'
                );
                return $this->redirect('/customers/users');
            }
        }

        $this->set('user_sources', $this->sources);
    }
    
    /**
    * The Edit user method
    * 
    * This method allows editing an user
    * Renders /Customers/View/Users/edit.ctp
    * 
    * @param integer $user_id the id of the user to edit
    * 
    * @return void or redirect on put or error
    */
    public function edit($user_id = null) 
    {
        if ($this->request->is(['post', 'put'])) {
            if ($this->User->saveAssociated($this->request->data)) {
                $this->Session->setFlash(
                    __('Utilizatorul a fost modificat!'), 
                    'Alerts/success'
                );
                return $this->redirect('/customers/users/edit/'.$user_id);
            }
        }

        $this->User->id = $user_id;
        if (!$this->User->exists()) {
            $this->Session->setFlash(
                __('Utilizator inexistent'), 
                'Alerts/warning'
            );
            return $this->redirect('/customers/users');
        }
        $options = array(
            'conditions' => array(
                'User.id' => $user_id
            )
        );
        $this->request->data = $user = $this->User->find('first', $options);
        $this->set(compact('user'));
    }

    /**
     * The delete User method
     *
     * @param integer $user_id the id of the user to be deleted
     * @param string  $undo    inactivate|reactivate user
     *
     * @return mixed
     * @throws Exception
     */
    public function delete($user_id = null, $undo = '')
    {
        $this->layout = false;
        $this->autoRender = false;

        $this->User->id = $user_id;
        if (!$this->User->exists()) {
            return $this->respond(
                __('Utilizator inexistent'), 
                'error'
            );
        }

        $user = $this->User->findById($user_id);

        if ($undo == '') {
            // inactivate
            $pieces = explode('@', $user['User']['email']);
            $new_email = $pieces[0] . '_' . $user_id . '@' . $pieces[1];
            $new_password = generatePassword();

            $to_save = [
                'id' => $user_id,
                'email' => $new_email,
                'is_active' => false,
                'request_login' => true,
                'hash' => sha1($new_email . $new_password),
                'md5_password' => $user['User']['hash']
            ];

            if (!$this->User->save($to_save, ['validate' => false])) {
                return $this->respond(
                    __('Eroare la inactivarea utilizatorului'),
                    'error'
                );
            }

            $this->_unsubscribeFromAllNewsletters($user['User']['email']);

            $this->_sendDeleteAccountEmail($user['User']['email']);

            return $this->respond(
                __('Utilizatorul a fost inactivat cu succes'),
                'success',
                '/customers/users'
            );
        } elseif ($undo == 'undo') {
            // reactivate
            $new_email = str_replace(
                '_' . $user['User']['id'],
                '',
                $user['User']['email']
            );
            $to_save = [
                'id' => $user['User']['id'],
                'email' => $new_email,
                'is_active' => true,
                'request_login' => false,
                'hash' => $user['User']['md5_password'],
                'md5_password' => '',
                'cancel_reason' => null
            ];

            if (!$this->User->save($to_save, ['validate' => false])) {
                return $this->respond(
                    __('Eroare la reactivarea utilizatorului'),
                    'error'
                );
            }

            return $this->respond(
                __('Utilizatorul a fost reactivat cu succes'),
                'success',
                ['action' => 'index']
            );
        }
    }

    /**
     * Send account delete e-mail to customer
     *
     * @param string $user_email - user email
     *
     * @return void
     */
    private function _sendDeleteAccountEmail($user_email)
    {
        App::uses('CakeEmail', 'Network/Email');
        $em = new CakeEmail('default');

        $em->to($user_email)
            ->subject(__('Cerere stergere date personale'))
            ->viewVars(
                [
                'storeName' => Configure::read('STORE_NAME')
                ]
            )
            ->template('Customers.user_delete', 'simple_to_customer');

        try {
            $em->send();
        } catch (Exception $e) {
        }
    }

    /**
     * Unsubscribe from Sendy newsletters
     *
     * @param string $user_email - user email
     *
     * @return void
     */
    private function _unsubscribeFromAllNewsletters($user_email)
    {
        // gasim si stergem din Newsletter Subscriber.
        $nl_subscriber = $this->NewsletterSubscriber->find(
            'first',
            [
                'conditions' => [
                    'email' => $user_email
                ],
            ]
        );

        if (!empty($nl_subscriber)) {
            $list_array = $this->_getSubscriptionStatus($user_email);
            $summary = [];
            if (!empty($list_array)) {
                foreach ($list_array as $list) {
                    $postfields = array(
                        'api_key' => Configure::read('SENDY_API_KEY'),
                        'list_id' => $list['list_id'],
                        'email' => $user_email,
                    );

                    $ch = curl_init();
                    curl_setopt(
                        $ch, CURLOPT_URL,
                        'http://newsletter.evercloud.ro/api/subscribers/delete.php'
                    );
                    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                    //curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
                    curl_setopt($ch, CURLOPT_POST, 1);
                    curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
                    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
                    curl_setopt($ch, CURLOPT_POSTFIELDS, $postfields);
                    $result = curl_exec($ch);
                    curl_close($ch);
                }
            }

            $this->NewsletterSubscriber->id
                = $nl_subscriber['NewsletterSubscriber']['id'];
            $this->NewsletterSubscriber->delete();
        }
    }

    /**
     * Get all Sendy lists where an email is present
     *
     * @param string $user_email - user email
     *
     * @return array|bool
     */
    private function _getSubscriptionStatus($user_email) 
    {
        $subscriber = $this->NewsletterSubscriber->find(
            'first',
            [
                'conditions' => [
                    'email' => $user_email
                ]
            ]
        );

        if (empty($subscriber)) {
            return false;
        }

        $list_array = $this->NewsletterSendyList->find(
            'list',
            [
                'fields' => [
                    'list_id'
                ]
            ]
        );

        $subscription_array = [];
        $i = 0;

        foreach ($list_array as $list) {
            $postfields = array(
                'api_key' => Configure::read('SENDY_API_KEY'),
                'email' => $user_email,
                'list_id' => $list,
            );

            $ch = curl_init();
            curl_setopt(
                $ch, CURLOPT_URL,
                'http://newsletter.evercloud.ro/api/subscribers/'
                . 'subscription-status.php'
            );
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_POST, 1);
            curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $postfields);
            $result = curl_exec($ch);
            curl_close($ch);

            if ($result != 'Email does not exist in list') {
                $subscription_array[$i++] = [
                    'status' => $result,
                    'list_id' => $list
                ];
            }
        }

        if (!empty($subscription_array)) {
            return $subscription_array;
        }

        return false;
    }
    
    /**
    * The login method
    * 
    * This method logs a user in the application
    * This is achieved by writing three arrays to the cookie
    * 
    * The names of the arrays are sha1 encrypted
    * 
    * The arrays are User, Person and AccessList
    * 
    * It also saves last_access field to the User
    * 
    * If the AccessList is present it redirects the user
    * 
    * @return redirect
    */
    public function login() 
    {
        if ($this->getAccessList()) {
            $this->redirect('/dash');
        }
        $this->layout = false;
        if ($this->request->is('post')) {
            $options = array(
                'conditions' => array(
                    'User.email' => trim($this->data['User']['email']),
                    'User.password' => sha1($this->data['User']['password']),
                    'User.is_admin' => 1,    
                    'User.is_email_confirmed' => 1,    
                    'User.is_active' => 1,    
                ),
            );
            $this->User->unbindModel(
                array(
                'belongsTo' => array('UserType')
                )
            );
            $user = $this->User->find('first', $options);
            if (!$user) {
                $this->Session->setFlash(
                    __('E-mail sau parola gresite'),
                    'Alerts/warning'
                );
                $this->redirect('/');
            }
            
            $user_permissions_raw = $this->User->query(
                'SELECT access__actions.path from access__actions 
                left join access__permissions 
                    on access__permissions.action_id = access__actions.id
                left join customers__users 
                    on access__permissions.user_id = customers__users.id
                where customers__users.id = ' . $user['User']['id']
            );
            $user_permissions = array();
            foreach ($user_permissions_raw as $up) {
                $user_permissions[] = $up['access__actions']['path'];
            }

            // group premissions
            $this->User->id = $user['User']['id'];
            $group_id = $this->User->field('group_id');
            if ($group_id !='') {
                $this->GroupPermission->bindModel(
                    array(
                        'belongsTo' => array(
                            'Action' => array(
                                'className' => 'Access.Action',
                                'foreignKey' => 'action_id'
                            )
                        )
                    )
                );

                $group_permissions = $this->GroupPermission->find(
                    'all', array(
                    'conditions' => array(
                        'GroupPermission.group_id' => $group_id
                    ),
                    'contain' => array(
                        'Action'
                    ),
                    )
                );

                foreach ($group_permissions as $group_permission) {
                    $user_permissions[] = $group_permission['Action']['path'];
                }
            }
            $user_permissions = array_unique($user_permissions);
            
            if (isset($_GET['mobile_auth'])) {
                $this->request->params['ext'] = 'json';
                return $this->respond(
                    __('Autentificare realizata cu success'),
                    'success',
                    null,
                    [
                        'user' => $user,
                        'permissions' => $user_permissions
                    ]
                );
            }
            
            $cookie_expires = '1 year';
            $cache_config = 'oney';
            if (isset($this->request->data['remember'])) {
                $cookie_expires = '1 year';
                $cache_config = 'oney';
            }

            $redirect = '/';
            $ad = $this->AdminDetail->findByUserId($user['User']['id']);
            if (!empty($ad)) {
                $user['User']['picture'] = $ad['AdminDetail']['picture'];
                $redirect = $ad['AdminDetail']['homepage'];
            }

            $this->Cookie->write(
                sha1('User'), 
                $user['User'], 
                true, 
                $cookie_expires
            );
            
            $this->Cookie->write(
                sha1('Person'), 
                $user['Person'], 
                true, 
                $cookie_expires
            );
            
            Cache::write(
                'AccessList' . $user['User']['id'], 
                $user_permissions, 
                $cache_config
            );
            $this->User->id = $user['User']['id'];
            $this->User->saveField('last_login', date('Y-m-d H:i:s'));

            $this->redirect($redirect);
        }
    }
    
    /**
    * The forbidden method
    * 
    * This method only renders a view
    * 
    * @return void
    */
    public function forbidden() 
    {
        $this->layout = false;
    }
    
    /**
    * The logout method
    * 
    * This method deletes the 'AccessList' . $id cache thus
    * cutting access to every page
    * 
    * @return redirect
    */
    public function logout() 
    {
        $this->deleteAccessList();
        $this->redirect('/');
    }
    
    /**
     * The _setFilterFields method
     *
     * This method sets the filterForm variable to the view to generate the
     * filter row
     *
     * @return void
     */
    private function _setFilterFields() 
    {
        $filterForm = array(
            'name' => '',
            'reset' => '/customers/users',
            'action' => '/users',
            'fields' => array(
                'User[id]' => array(
                    'name' => 'User[id]',
                    'label' => 'Id',
                    'width' => 50
                ),
                'Person[lname]' => array(
                    'name' => 'Person[lname]',
                    'label' => __('Nume'),
                    'width' => 100
                ),
                'Person[fname]' => array(
                    'name' => 'Person[fname]',
                    'label'=> __('Prenume'),
                    'width' => 100
                ),
                'User[email]' => array(
                    'name' => 'User[email]',
                    'label' => __('Email')
                ),
                'User[phone]' => array(
                    'name' => 'User[phone]',
                    'label' => __('Telefon'),
                    'width' => 80
                ),
                'User[created_at]' => array(
                    'name' => 'User[created_at]',
                    'label' => __('Data inregistrare'),
                    'width' => 100,
                    'is_datepicker' => true,
                    'readonly'
                ),
                'User[notes]' => array(
                    'name' => 'User[notes]',
                    'label' => __('Note')
                ),
                'User[extra_notes]' => array(
                    'name' => 'User[extra_notes]',
                    'label' => __('Obs.')
                ),
            ),
        );
        $this->set(compact('filterForm'));
    }
    
    /**
    * The Add note method
    * 
    * This method adds an user note
    *
    * @param int $userId the user id
    *
    * @return void
    */
    public function addNote($userId)
    {
        $this->layout = false;
        $this->autoRender = false;
        $this->User->id = $userId;
        if ($this->User->saveField('notes', $this->request->data['value'])) {
            return true;        
        }
        $this->response->body(__('Eroare la salvarea datelor'));
        $this->response->statusCode(403);
        return false;
    }

    /**
    * The Add extra note method
    *
    * This method adds an user extra note
    *
    * @param int $userId the user id
    *
    * @return void
    */
    public function addExtraNote($userId)
    {
        $this->layout = false;
        $this->autoRender = false;
        $this->User->id = $userId;
        if ($this->User->saveField('extra_notes', $this->request->data['value'])) {
            return true;
        }
        $this->response->body(__('Eroare la salvarea datelor'));
        $this->response->statusCode(403);
        return false;
    }

    /**
     * The searchAutocomplete method
     * 
     * This method searches a user
     * 
     * @return redirect
     */
    public function searchAutocomplete() 
    {
        if (isset($_GET['query']) && !empty($_GET['query'])) {
            $data = $this->User->search($_GET['query']);
            $returnData = array();
            foreach ($data as $key => $value) {
                $returnData[$key]['id'] = $value['User']['id'];
                $returnData[$key]['name'] = $value['User']['email'];
            }
            $count = count($data);
            die(json_encode($returnData));
        } else {
            return $this->respond(
                __('Intoduceti un termen de cautare')
            );
        }
    }

    /**
     * Inactive users list
     *
     * @return mixed
     */
    public function inactiveUsers()
    {
        $conditions['User.is_active'] = false;

        $this->User->bindModel(
            array(
                'hasMany' => array(
                    'UserAddress' => array(
                        'className' => 'Customers.UserAddress'
                    ),
                )
            )
        );

        $this->User->contain(
            [
                'Person',
                'AdminDetail',
                'UserType'
            ]
        );

        $this->User->virtualFields = array(
            'honored_orders' => "SELECT count(id)
                FROM orders__sales os
                WHERE os.user_id = User.id
                AND sale_status_id = 11",
            'not_honored_or_canceled_orders' => "SELECT count(id)
                FROM orders__sales os
                WHERE os.user_id = User.id
                AND sale_status_id <> 11
                AND sale_status_id <> 12"
        );

        $this->Paginator->settings = array(
            'conditions' => $conditions,
            'order' => array(
                'User.id' => 'ASC'
            ),
            'group' => array("User.id")
        );

        $users = $this->Paginator->paginate('User');
        $this->set(compact('users'));
    }
}
