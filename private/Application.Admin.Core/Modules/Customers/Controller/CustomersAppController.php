<?php
/**
* Module level Controller
*
* This file is module-wide controller file. You can put all
* module-wide controller-related methods here.
*
* PHP version 5.4
*
* @category  Admin
* @package   Customers
* @author    Lucian Vasile <lucian.vasile@live.com>
* @copyright 2014 Evercloud
* @license   http://www.php.net/license/3_01.txt  PHP License 3.01
* @version   SVN: $Id$
* @link      /modules/Customers/controller/customers-app-controller
* @since     1.0
*/

App::uses('AppController', 'Controller');

/**
* CustomersAppController Class
*
* Inside this Controller module-wide logic methods
* will be placed
*
* @category  Admin
* @package   Customers
* @author    Lucian Vasile <lucian.vasile@live.com>
* @copyright 2014 Evercloud
* @license   http://www.php.net/license/3_01.txt  PHP License 3.01
* @version   Release: 1.0
* @link      /modules/Customers/controller/customers-app-controller
* @since     Class available since Release 1.0
*/
class CustomersAppController extends AppController
{

    public $theme = 'Default';

    public $uses = array(
        'Customers.UserType',
    );

    /**
     * The updateUserTypesEmail method
     *
     * @param integer $user_id      the id of the user to edit
     * @param integer $user_type_id the id of the user_type
     *
     * @return redirect
     */
    public function updateUserTypesEmail(
        $user_id=1,
        $user_type_id=''
    ) {
        $baseUrl = Configure::read('WEBSITE_URL');
        $storeName = Configure::read('STORE_NAME');
        $user = $this->User->findById($user_id, array('fields' => 'email'));
        $email = $user['User']['email'];
        $userId = $user_id;

        $user_types = $this->UserType->find('list');
        $user_types[''] = 'Nu mai indepliniti criteriile pentru a fi reseller.';

        $viewVars = array(
            //fixed vars
            'baseUrl' => $baseUrl,
            'storeName' => $storeName,
            'userId' => $userId,
            //--fixed vars
            'reseller_name' => $user_types[$user_type_id],
        );

        $subject = 'S-a modificat tipul contului tau de pe '.$storeName;

        App::uses('CakeEmail', 'Network/Email');
        $em = new CakeEmail('default');
        $em->to($email);
        $em->subject($subject);
        $em->template('update_user_type');
        $em->viewVars($viewVars);
        if (!$em->send()) {
            return false;
        } else {
            return true;
        }
    }


    public function updateVIPEmail($userId = null) 
    {
        $baseUrl = Configure::read('WEBSITE_URL');
        $storeName = Configure::read('STORE_NAME');
        $user = $this->User->find(
            'first',
            [
                'conditions'=> [
                    'User.id' => $userId
                ],
                'fields' => [
                    'Person.fname',
                    'email',
                    'hash',
                    'user_type_id'
                ]
            ]
        );
        $email = $user['User']['email'];

        $user_types = $this->UserType->find('list');
        $viewVars = array(
            //fixed vars
            'baseUrl' => $baseUrl,
            'storeName' => $storeName,
            'userId' => $userId,
            //--fixed vars
            'user_name' => $user['Person']['fname'],
            'user_email' => $email,
            'user_hash' => $user['User']['hash'],
            'reseller_name' => $user_types[$user['User']['user_type_id']]
        );

        $subject = "Bine ai venit in Cubul VIP GSMnet";

        App::uses('CakeEmail', 'Network/Email');
        $em = new CakeEmail('default');
        $em->to($email);
        $em->subject($subject);
        $em->template('vip_thank_you');
        $em->viewVars($viewVars);
        if (!$em->send()) {
            return false;
        } else {
            return true;
        }
    }
}
