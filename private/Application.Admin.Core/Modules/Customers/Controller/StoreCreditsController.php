<?php
/**
 * StoreCredits
 *
 * Inside this Controller admin logic methods will
 * be placed
 *
 * PHP version 5.4
 *
 * @category  Admin
 * @package   Customers
 * @author    Lucian Vasile <lucian.vasile@live.com>
 * @copyright 2014 Evercloud
 * @license   http://www.php.net/license/3_01.txt  PHP License 3.01
 * @version   SVN: $Id$
 * @link      /customers/store-credits
 * @since     1.0
 */

App::uses('Customers.CustomersAppController', 'Controller');

/**
 * StoreCredits Class
 *
 * Inside this Controller admin logic methods will
 * be placed
 *
 * @category  Admin
 * @package   Customers
 * @author    Lucian Vasile <lucian.vasile@live.com>
 * @copyright 2014 Evercloud
 * @license   http://www.php.net/license/3_01.txt  PHP License 3.01
 * @version   Release: 1.0
 * @link      /Customers/store-credits
 * @since     Class available since Release 1.0
 * @property  StoreCredit StoreCredit
 * @property  StoreCreditRequest StoreCreditRequest
 * @property  UserCompany UserCompany
 * @property  UserAddress UserAddress
 * @property  Invoice Invoice
 */
class StoreCreditsController extends CustomersAppController
{
    public $uses = array(
        'Customers.StoreCreditRequest',
        'Customers.StoreCredit',
        'Customers.UserCompany',
        'Customers.UserAddress',
        'Customers.User',
        'Orders.Invoice',
        'Orders.SaleComment',
        'Customers.SalesStoreCredit',
    );
    /**
     * The components used by this controller
     *
     * @var
     */
    public $components = array (
        'Paginator',
        'Toolkit.FilterForm',
        'RequestHandler' => array(
            'viewClassMap' => array('pdf' => 'CakePdf.Pdf')
        ),
    );

    /**
     * The _setFilterFields method
     *
     * @return void
     */
    private function _setFilterFields()
    {
        $filterForm = array(
            'name' => 'StoreCredit',
            'action' => '/store_credits/requests',
            'reset' => '/customers/store_credits/requests',
            'fields' => array(
                null,
                'StoreCreditRequest[id]' => array(
                    'name' => 'StoreCreditRequest[id]',
                    'label' => 'Id.Req.',
                    'width' => 40
                ),
                null,
                'StoreCreditRequest[user_id]' => array(
                    'name' => 'StoreCreditRequest[user_id]',
                    'label' => 'Id.User',
                    'width' => 40
                ),
                'StoreCreditRequest[full_name]' => array(
                    'name' => 'StoreCreditRequest[full_name]',
                    'width' => 80
                ),
                'StoreCreditRequest[entity]' => array(
                    'name' => 'StoreCreditRequest[entity]',
                    'label' => 'Entitate',
                ),
                'StoreCreditRequest[type]' => array(
                    'name' => 'StoreCreditRequest[type]',
                    'data' => 'types',
                    'label' => 'Tip plata'
                ),
                'StoreCreditRequest[bank]' => array(
                    'name' => 'StoreCreditRequest[bank]',
                    'label' => 'Cod',
                    'width' => 100
                ),
                'StoreCreditRequest[bank_account]' => array(
                    'name' => 'StoreCreditRequest[bank_account]',
                    'label' => 'Cont',
                    'width' => 200
                ),
                'StoreCreditRequest[state]' => array(
                    'name' => 'StoreCreditRequest[state]',
                    'label' => 'Judet',
                    'width' => 100
                ),
                'StoreCreditRequest[city]' => array(
                    'name' => 'StoreCreditRequest[city]',
                    'label' => 'Oras',
                    'width' => 70
                ),
                'StoreCreditRequest[bank_branch]' => array(
                    'name' => 'StoreCreditRequest[bank_branch]',
                    'label' => 'Disponibilitate',
                    'data' => 'availabilities',
                ),
                'StoreCreditRequest[created]' => array(
                    'name' => 'StoreCreditRequest[created]',
                    'label' => 'Data adaugarii',
                    'width' => 50,
                    'is_intervalpicker' => true,
                    'readonly'
                ),
                null,
                'StoreCreditRequest[value]' => array(
                    'name' => 'StoreCreditRequest[value]',
                ),
                null,
                null,
                null,
                null,
                'additional_fields' => array(
                    'add_additional_row' => true,
                ),
                'StoreCreditRequest[is_paid]' => array(
                    'name' => 'StoreCreditRequest[is_paid]',
                    'label' => 'Neachitate',
                    'type' => 'checkbox',
                    'invertSelect' => true
                ),
            )
        );

        $all_types = $this->StoreCreditRequest->find(
            'all',
            [
                'fields' => [
                    'DISTINCT(type) as dist_type'
                ]
            ]
        );
        $types = [];
        foreach ($all_types as $t) {
            $value = $t['StoreCreditRequest']['dist_type'];
            $types[$value] = $value;
        }

        $this->set(
            compact(
                'filterForm',
                'types'
            )
        );
    }

    /**
     * The requests method
     *
     * List the requests to pay store credits
     *
     * @return void
     */
    public function requests() 
    {
        $this->_setFilterFields();

        $conditions = $this->FilterForm->parseConditions($this->request->query);

        $this->Paginator->settings = array(
            'conditions' => $conditions,
            'order' => array(
                'StoreCreditRequest.created' => 'DESC'
            ),
        );

        $requests = $this->Paginator->paginate('StoreCreditRequest');

        $i=0;
        foreach ($requests as $sc) {
            $user = $this->User->findById($sc['StoreCreditRequest']['user_id']);
            $requests[$i]['user_hash'] = $user['User']['hash'];

            $this->StoreCredit->bindModel(
                array(
                'belongsTo' => array(
                    'Invoice' => array(
                        'className' => 'Orders.Invoice'
                    ),
                )
                )
            );

            $this->StoreCredit->virtualFields = array(
                'real_value' => "StoreCredit.value - StoreCredit.used_value"
            );
            /*$this->StoreCredit->Behaviors->load('Containable');
            $this->StoreCredit->contain(array(
                'Invoice.id',
                'Invoice.number',
                'Invoice.series',
            ));*/
            $store_credits = $this->StoreCredit->find(
                'all', array(
                'conditions' => array(
                    'StoreCredit.user_id' => $user['User']['id'],
                    'StoreCredit.company_VAT' => $sc['StoreCreditRequest']['entity'],
                    'StoreCredit.real_value >' => 0,
                )
                )
            );

            $invoices_names = '';
            foreach ($store_credits as $store_credit) {
                $invoices_names
                    .=$store_credit['Invoice']['series']
                    .$store_credit['Invoice']['number'].', ';
            }

            $requests[$i]['invoices_names'] = rtrim($invoices_names, ', ');

            $storeCreditValue = $this->StoreCredit->query(
                'SELECT get_store_credit_value
                ('.$sc['StoreCreditRequest']['user_id']
                .', "'.$sc['StoreCreditRequest']['entity'].'")
                as credit_store_value'
            );
            $requests[$i]['StoreCreditRequest']['total_value']
                = $storeCreditValue[0][0]['credit_store_value'];
            $i++;
        }
        
        $this->set(compact('requests'));
    }

    /**
     * The Edit user method
     *
     * This method allows editing an user
     *
     * @param integer $request_id the id of the user to edit
     *
     * @return void or redirect on put or error
     */
    public function edit($request_id = null)
    {
        if ($this->request->is('put')) {
            if ($this->StoreCreditRequest->save($this->request->data)) {
                $this->Session->setFlash(
                    __('Cererea a fost modificata!'),
                    'Alerts/success'
                );
                return $this->redirect('/customers/store_credits/edit/'.$request_id);
            }
        }
        $this->StoreCreditRequest->id = $request_id;
        if (!$this->StoreCreditRequest->exists()) {
            $this->Session->setFlash(
                __('Cerere inexistenta'),
                'Alerts/warning'
            );
            return $this->redirect('/customers/store_credits/request/');
        }

        $this->request->data = $this->StoreCreditRequest->findById($request_id);
    }

    /**
     * The payRequest method
     *
     * @param integer $id      The id of the request
     * @param bool    $respond Do respond
     *
     * @return string|redirect
     * @throws Exception
     */
    public function payRequest($id, $respond = true)
    {
        $request = $this->StoreCreditRequest->findById($id);
        if (!$request) {
            if (!$respond) {
                return __('%s: Date inconsistente', $id);
            }
            return $this->respond(
                __('Date inconsistente')
            );
        }

        $transaction = $this->StoreCredit->getDataSource();
        $this->Sale->query("SET SESSION tx_isolation='SERIALIZABLE';");
        $transaction->begin();

        $this->StoreCredit->virtualFields = array(
            'real_value' => "value-used_value"
        );
        $store_credits = $this->StoreCredit->find(
            'all', array(
                'conditions' => array(
                    'StoreCredit.user_id'
                        => $request['StoreCreditRequest']['user_id'],
                    'slug(trim_tab(StoreCredit.company_VAT))'
                        => alias(trim($request['StoreCreditRequest']['entity'])),
                    'StoreCredit.real_value >' => 0
                ),
            )
        );

        if (!$store_credits) {
            if (!$respond) {
                return __('%s: Niciun credit magazin de achitat', $id);
            }
            return $this->respond(
                __('Niciun credit magazin de achitat.')
            );
        }

        // 1 update la store credit
        $full_value = 0;
        $sc_raw = $this
            ->StoreCredit
            ->query(
                "SELECT get_store_credit_value"
                . "({$request['StoreCreditRequest']['user_id']}, "
                . "'{$request['StoreCreditRequest']['entity']}') as sc"
            );
        $sc_value = $sc_raw[0][0]['sc'];

        $store_credits_ids = $this->StoreCredit->find(
            'list', array(
            'conditions' => array(
                'StoreCredit.user_id' => $request['StoreCreditRequest']['user_id'],
                'slug(trim_tab(StoreCredit.company_VAT))'
                    => alias(trim($request['StoreCreditRequest']['entity'])),
                'StoreCredit.real_value >' => 0,
            ),
            'fields' => array(
                'invoice_id'
            )
            )
        );
        
        $invoices = $this->Invoice->find(
            'list', array(
            'conditions' => array(
                'id' => $store_credits_ids
            ),
            'fields' => array(
                'number',
                'series'
            )
            )
        );
        if (empty($invoices)) {
            $transaction->rollback();
            if (!$respond) {
                return __(
                    '%s: Nu exista documente de achitare pentru '
                    . 'credite magazin.', $id
                );
            }
            return $this->respond(
                __('Nu exista documente de achitare pentru credite magazin:'),
                'error'
            );
        }
        $payment_string = __('contravaloarea');
        $payment_string .= ' ';
        if (count($invoices) > 1) {
            $payment_string .= __('facturilor');
        } else {
            $payment_string .= __('facturii');
        }
        $payment_string .= ' ';
        foreach ($invoices as $nr => $series) {
            $payment_string .= $series . ' ' . $nr . ', ';
        }
        $payment_string = trim($payment_string, ', ');

        $address_string = '';
        if (!empty($request['StoreCreditRequest']['entity'])) {
            $user_company = $this->UserCompany->find(
                'first', array(
                'conditions' => array(
                    'UserCompany.user_id'
                        => $request['StoreCreditRequest']['user_id'],
                    'slug(UserCompany.VAT)'
                        => alias($request['StoreCreditRequest']['entity'])
                )
                )
            );
            if (!$user_company) {
                $transaction->rollback();
                if (!$respond) {
                    return __(
                        '%s: Eroare la gasirea companiei pentru achitare',
                        $id
                    );
                }
                return $this->respond(
                    __('Eroare la gasirea companiei pentru achitare'),
                    'error'
                );
            }
            $address = $user_company['UserCompany'];
        } else {
            $user_address = $this->UserAddress->find(
                'first', array(
                'conditions' => array(
                    'UserAddress.user_id'
                        => $request['StoreCreditRequest']['user_id'],
                )
                )
            );
            $address = $user_address['UserAddress'];
        }
        $address_string .= $address['address_line_1'] . ' '
            . $address['address_line_2'] . ' '
            . $address['city'] . ' '
            . (($address['city'] != $address['state'])
            ? $address['state'] . ' ' : '');

        $request['StoreCreditRequest']['payment_string'] = $payment_string;
        $request['StoreCreditRequest']['address_string'] = $address_string;
        foreach ($store_credits as $sc) {
            $saveArray = array();
            $saveArray['used_value'] = $sc['StoreCredit']['real_value'];
            $saveArray['store_credit_request_id'] = $id;
            $full_value += $sc['StoreCredit']['real_value'];
            $this->StoreCredit->id =  $sc['StoreCredit']['id'];
            if (!$this->StoreCredit->save($saveArray)) {
                $transaction->rollback();
                if (!$respond) {
                    return __('%s: Eroare la achitare', $id);
                }
                return $this->respond(
                    __('Eroare la achitare!'),
                    'error'
                );
            }
        }
        
        // 1b. validam suma
        if (price($sc_value) != price($full_value)) {
            $transaction->rollback();
            if (!$respond) {
                return __('%s: Eroare la validarea valorii creditului.', $id);
            }
            return $this->respond(
                __('Eroare la validarea valorii creditului. Incearca din nou.'),
                'error'
            );
        }

        if ($request['StoreCreditRequest']['type'] == 'cash') {
            // 2.0 vedem next number la chitanta
            $last_number = $this->StoreCreditRequest->field('MAX(receipt_no)');
            $number = (int) $last_number + 1;
            $document = Configure::read('RECEIPT_SERIES') . 'B ' . $number;
            $document_path = '/pdf/'
                . Configure::read('RECEIPT_SERIES') . 'B'
                . $number . '.pdf';

            // 2. emitem chitanta
            $request['StoreCreditRequest']['value'] = $full_value;
            if (!$this->_generateReceipt($request, $number)) {
                $transaction->rollback();
                if (!$respond) {
                    return __('%s: Eroare la generarea chitantei.', $id);
                }
                return $this->respond(
                    __('Eroare la generarea chitantei!'),
                    'error'
                );
            }
        } else {
            $document = 'transfer ' . date('d.m.Y');
            $document_path = '';
            if ($respond) {
                if (!isset($this->request->data['payment_document']['error'])
                    || $this->request->data['payment_document']['error'] !== 0
                ) {
                    $transaction->rollback();
                    return $this->respond(
                        __('Eroare la incarcarea documentului. 1')
                    );
                }
                $file = $this->request->data['payment_document'];
                if (is_uploaded_file($file['tmp_name'])) {
                    if (!$this->remoteUpload(
                        $file['tmp_name'],
                        '/pdf/',
                        $file['name']
                    )) {
                        $transaction->rollback();
                        return $this->respond(
                            __('Eroare la incarcarea documentului. 2')
                        );
                    }
                } else {
                    $transaction->rollback();
                    return $this->respond(
                        __('Eroare la incarcarea documentului. 3')
                    );
                }
                $document = $file['name'];
                $document_path = '/pdf/' . $file['name'];
            }
        }
        // 3. punem si in request
        $requestArray = array(
            'id' => $request['StoreCreditRequest']['id'],
            'admin_id' => $this->getCurrentUserId(),
            'value' => $full_value,
            'document' => $document,
            'document_path' => $document_path,
            'paid_at' => date('Y-m-d H:i:s'),
            'is_paid' => true,
        );
        if ($request['StoreCreditRequest']['type'] == 'cash') {
            $requestArray['receipt_no'] = $number;
            $requestArray['receipt_series']
                = Configure::read('RECEIPT_SERIES') . 'B';
        }
        if (!$this->StoreCreditRequest->save($requestArray)) {
            $transaction->rollback();
            if (!$respond) {
                return __('%s: Eroare la achitarea creditului.', $id);
            }
            return $this->respond(
                __('Eroare la achitarea creditului.'),
                'error'
            );
        }
        $store_credit_request_id = $this->StoreCreditRequest->getLastInsertID();

        // achitam si valoare facturilor

        $this->Invoice->bindModel(
            array(
            'hasMany' => array(
                'InvoicePayment' => array(
                    'className' => 'Orders.InvoicePayment'
                ),
            ),
            'hasOne' => array(
                'StoreCredit' => array(
                    'className' => 'Customers.StoreCredit'
                ),
            ),

            )
        );
        $this->StoreCredit->bindModel(
            array(
            'hasMany' => array(
                'SalesStoreCredit' => array(
                    'className' => 'Customers.SalesStoreCredit'
                )
            )
            )
        );
        $this->Invoice->Behaviors->load('Containable');
        $this->Invoice->contain(
            array(
            'InvoicePayment',
            'StoreCredit' => array(
                'SalesStoreCredit'
            ),
            )
        );
        $invoices = $this->Invoice->find(
            'all', array(
                'conditions' => array(
                    'Invoice.id' => $store_credits_ids
                ),
                'fields' => array(
                    'id',
                    'value'
                )
            )
        );
        //pr($invoices);die();
        foreach ($invoices as $i) {
            // checkpoint 1
            $value_to_pay_1 = $i['Invoice']['value'];
            foreach ($i['StoreCredit']['SalesStoreCredit'] as $ssc) {
                $value_to_pay_1 += $ssc['value'];
            }
            // checkpoint 2
            $value_to_pay_2 = $i['Invoice']['value'];
            foreach ($i['InvoicePayment'] as $ip) {
                $value_to_pay_2 -= $ip['value'];
            }
            /*if (price($value_to_pay_1) != price($value_to_pay_2)) {
                $transaction->rollback();
                return $this->respond(
                    __(
                        'Eroare la achitarea facturilor. '
                        . 'Valoare de achitat nu este consistenta.'
                    ),
                    'error'
                );
            }*/
            if (!$this->Invoice->payWithStoreCreditRequest(
                $i['Invoice']['id'],
                $store_credit_request_id,
                $document,
                $value_to_pay_2
            )) {
                $transaction->rollback();
                if (!$respond) {
                    return __('%s: Eroare la achitarea facturilor storno.', $id);
                }
                return $this->respond(
                    __('Eroare la achitarea facturilor storno!')
                );
            }
        }

        $transaction->commit();
        if ($respond) {
            return $this->respond(
                __('Credit achitat!'),
                'success',
                '/customers/store_credits/requests#' . $id
            );
        } else {
            return __('%s: Credit achitat!', $id);
        }
    }


    /**
     * The _generateReceipt method
     *
     * This method generates a pdf based on a sale and saves it on the disk
     *
     * @param array  $request The array of the request
     * @param number $number  The number of the invoice
     *
     * @return bool
     */
    private function _generateReceipt($request, $number)
    {
        $name = Configure::read('RECEIPT_SERIES') . 'B' . $number . '.pdf';
        $this->set(compact('request', 'number'));
        $this->layout = false;
        $this->RequestHandler->renderAs($this, 'pdf');
        ob_start();
        $this->render('receipt');
        ob_end_clean();

        $file = new File(WWW_ROOT . 'pdf' . DS . $name, true);
        return $file->write($this->response->body());
    }
    
    /**
     * The cancelSaleStoreCredits method
     * This method deletes the store credits attached to a sale
     * 
     * @param int $sale_id the id of the sale
     * 
     * @return mixed
     */
    public function cancelSaleStoreCredits($sale_id) 
    {
        $this->Sale->contain();
        $sale = $this->Sale->findById($sale_id);
        if (in_array($sale['Sale']['sale_status_id'], [11,12])) {
            return $this->respond(
                __('Nu poti anula creditul magazin la o comanda finalizata'),
                'error'
            );
        }

        $sale_s_credits = $this->SalesStoreCredit->findAllBySaleId($sale_id);
        if (!$sale_s_credits) {
            return;
        }
        
        $this->SalesStoreCredit->deleteAll(array('sale_id' => $sale_id));
        foreach ($sale_s_credits as $sale_s_credit) {
            $store_credit_id = $sale_s_credit['SalesStoreCredit']['store_credit_id'];
            $sale_s_credit_value = $sale_s_credit['SalesStoreCredit']['value'];
            $this->StoreCredit->id = $store_credit_id;
            $this->StoreCredit->saveField('used_value', -1 * $sale_s_credit_value);
        }

        $saveSaleComment = array(
            'sale_id' => $sale_id,
            'user_id' => $this->getCurrentUserId(),
            'comment' => "#" . $sale_id . " - "
                . "Anulare credit magazin in valoare de "
                . $sale_s_credit_value
        );

        $this->SaleComment->create();
        $this->SaleComment->save($saveSaleComment);

        return $this->respond(
            __('Credit magazin eliminat!')
        );
    }

    /**
     * Perform actions on checked store credit requests
     *
     * @return mixed
     */
    public function batchActions()
    {
        $this->layout = false;
        $this->autoRender = false;

        if (empty($this->request->data['batch_actions'])) {
            return $this->respond(
                __('Nu ati selectat nicio actiune'),
                'error'
            );
        }

        if (empty($this->request->data['requests'])) {
            return $this->respond(
                __('Nu ati selectat nicio cerere'),
                'error'
            );
        }

        if ($this->request->data['batch_actions'] == 1) {
            $stored_filename = sanitizeFilename('credit_request_payment') . '.csv';

            $requests = $this->StoreCreditRequest->find(
                'all',
                [
                    'conditions' => [
                        'StoreCreditRequest.id' => $this->request->data['requests']
                    ],
                    'order' => [
                        'StoreCreditRequest.created' => 'DESC'
                    ]
                ]
            );

            foreach ($requests as $i => $r) {
                $storeCreditValue = $this->StoreCredit->query(
                    'SELECT get_store_credit_value
                    ('.$r['StoreCreditRequest']['user_id']
                    .', "'.$r['StoreCreditRequest']['entity'].'")
                    as credit_store_value'
                );
                $requests[$i]['StoreCreditRequest']['total_value']
                    = $storeCreditValue[0][0]['credit_store_value'];
            }

            $csv = [];
            $i = 1;
            foreach ($requests as $r) {
                if ($r['StoreCreditRequest']['total_value'] == 0) {
                    continue;
                }
                $temp = [
                    $i,
                    $r['StoreCreditRequest']['total_value'],
                    'RON',
                    strtoupper(Configure::read('PAYER_IBAN_RON')),
                    trim($r['StoreCreditRequest']['full_name']),
                    '',
                    '',
                    '',
                    strtoupper($r['StoreCreditRequest']['bank_account']),
                    'Req ' . $r['StoreCreditRequest']['id'],
                    '',
                    '',
                    '',
                    date('ymd'),
                    'NORMAL'
                ];
                $csv[] = $temp;
                $i++;
            }

            $dir_name = WWW_ROOT . 'files' . DS . 'store_credit';
            if (!is_dir($dir_name)) {
                mkdir($dir_name, 0755, true);
            }
            $fp = fopen($dir_name . DS . $stored_filename, 'w');

            foreach ($csv as $line) {
                if (!fwrite($fp, implode(',', $line) . "\n")) {
                    return false;
                }
                /*if (!fputcsv($fp, $line, ',', '')) {
                    return false;
                }*/
            }

            fclose($fp);

            // add `is_pending`
            try {
                if (!$this->StoreCreditRequest->updateAll(
                    ['StoreCreditRequest.is_pending' => true],
                    ['StoreCreditRequest.id' => $this->request->data['requests']]
                )) {
                    return $this->respond(
                        __('Eroare la trecerea cererilor in IN ASTEPTARE'),
                        'error'
                    );
                }
            } catch (PDOException $e) {
                return $this->respond(
                    __('Eroare la baza de date:') . '<br>' . $e->getMessage(),
                    'error'
                );
            }

            header('Content-Type: text/csv');
            header("Content-Disposition: attachment; filename=$stored_filename");
            readfile($dir_name . DS . $stored_filename);
            die;
        } elseif ($this->request->data['batch_actions'] == 2) {
            $message = [];
            foreach ($this->request->data['requests'] as $req_id) {
                try {
                    $message[] = $this->payRequest($req_id, false);
                } catch (Exception $e) {
                }
            }

            return $this->respond(
                implode('<br>', $message),
                'success'
            );
        }
    }

    /**
     * Remove pending status for a store credit request
     *
     * @param int $store_credit_request_id - store credit request id
     *
     * @return mixed
     */
    public function removePendingStatus($store_credit_request_id)
    {
        if (empty($store_credit_request_id)) {
            return $this->respond(
                __('Cerere nespecificata'),
                'error'
            );
        }

        $request = $this->StoreCreditRequest->findById($store_credit_request_id);
        if (empty($request)) {
            return $this->respond(
                __('Cerere inexistenta'),
                'error'
            );
        }

        $to_save= [
            'id' => $store_credit_request_id,
            'is_pending' => false
        ];
        if (!$this->StoreCreditRequest->save($to_save)) {
            return $this->respond(
                __('Eroare la dezactivarea starii de IN ASTEPTARE a cererii'),
                'error'
            );
        }

        return $this->respond(
            __('Starea de IN ASTEPTARE a cererii a fost modificata cu succes'),
            'success'
        );
    }

    /**
     * Edit the request note (editable plugin)
     *
     * @param int $store_credit_request_id - the store credit request id
     *
     * @return bool
     */
    public function editRequestNote($store_credit_request_id)
    {
        $this->layout = false;
        $this->autoRender = false;
        $this->StoreCreditRequest->id = $store_credit_request_id;
        if ($this->StoreCreditRequest->saveField(
            'note',
            $this->request->data['value']
        )) {
            return true;
        }
        $this->response->body(__('Eroare la salvarea datelor'));
        $this->response->statusCode(403);
        return false;
    }
}
