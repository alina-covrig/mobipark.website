<?php
/**
* UserAddressesController
*
* Inside this Controller admin logic methods will
* be placed
*
* PHP version 5.4
*
* @category  Admin
* @package   Customers
* @author    Lucian Vasile <lucian.vasile@live.com>
* @copyright 2014 Evercloud
* @license   http://www.php.net/license/3_01.txt  PHP License 3.01
* @version   SVN: $Id$
* @link      /customers/user_types
* @since     1.0
*/

App::uses('Customers.CustomersAppController', 'Controller');

/**
* UserAddressesController Class
*
* Inside this Controller admin logic methods will
* be placed
*
* @category  Admin
* @package   Customers
* @author    Lucian Vasile <lucian.vasile@live.com>
* @copyright 2014 Evercloud
* @license   http://www.php.net/license/3_01.txt  PHP License 3.01
* @version   Release: 1.0
* @link      /Customers/user_types
* @since     Class available since Release 1.0     
*/
class UserAddressesController extends CustomersAppController
{
    /**
    * The components used by this controller
    * 
    * @var 
    */
    public $component = array('Paginator');
    
    /**
    * The displayFields property
    * 
    * This property is used by dataTables.js to extract data
    * from the model and to generate the table in frontend
    * 
    * @var 
    */
    public $displayFields = array(
        'UserAddress' => array(
            'id' => 'ID',
            'displayName' => 'Adresa',
        ),
    );
    
    /**
    * The models used by this controller
    * 
    * @var 
    */
    public $uses = array(
        'Customers.User',
        'Customers.Person',
        'Customers.UserAddress',
        'Customers.Country',
        'Customers.County',
        'Customers.City',
        'Customers.Street',
        'Customers.ZipCode',
    );
    
    /**
    * The Add address method
    * 
    * This method adds an adress for a user
    * Renders /Customers/UserAddresses/add.ctp
    * 
    * @param integer $user_id the user id to add an address to
    * 
    * @return void or redirect on post or error
    */
    public function add($user_id = null) 
    {
        $this->User->id = $user_id;
        if (!$this->User->exists()) {
            $this->Session->setFlash(
                __('Utilizator inexistent'), 
                'Alerts/warning'
            );
            return $this->redirect('/customers/users');
        }
        
        if ($this->request->is('post')) {
            if (isset($this->request->data['UserAddress']['street_name_2'])) {
                $street = $this->Street->findById(
                    $this->request->data['UserAddress']['street_name_2'],
                    ['name', 'type']
                );
                $this->request->data['UserAddress']['street_name']
                    = $street['Street']['name'] . ' ('
                    . $street['Street']['type'] . ')';
            }
            if ($this->UserAddress->save($this->request->data)) {
                $this->Session->setFlash(
                    __('Adresa a fost adaugata!'), 
                    'Alerts/success'
                );
                return $this->redirect('/customers/users/edit/' . $user_id);
            }
        }
        $options = array(
            'conditions' => array(
                'User.id' => $user_id
            )
        );

        $countries = $this->_getCountries();
        $this->set(compact('countries'));
        
        $states = $this->_getCounties();
        $this->set(compact('states'));
        $user = $this->User->find('first', $options);
        $this->set(compact('user'));
    }
    
    /**
    * The Edit address method
    * 
    * This method allows editing an user address
    * Renders /Customers/UserAddresses/edit.ctp
    * 
    * @param integer $id the id of the address to edit
    * 
    * @return void or redirect on put or error
    */
    public function edit($id = null) 
    {
        $this->UserAddress->id = $id;
        if (!$this->UserAddress->exists()) {
            $this->Session->setFlash(
                __('Adresa inexistenta'), 
                'Alerts/warning'
            );
            return $this->redirect('/customers/users');
        }
        $options = array(
            'conditions' => array(
                'UserAddress.id' => $id
            )
        );
        $this->UserAddress->recursive = 2;
        $user_address = $this->UserAddress->find('first', $options);
        $this->set('user', array('User' => $user_address['User']));
        if ($this->request->is('put')) {

            if (isset($this->request->data['UserAddress']['street_name_2'])) {
                $street = $this->Street->findById(
                    $this->request->data['UserAddress']['street_name_2'],
                    ['name', 'type']
                );
                $this->request->data['UserAddress']['street_name']
                    = $street['Street']['name'] . ' ('
                    . $street['Street']['type'] . ')';
            }

            if ($this->UserAddress->save($this->request->data)) {
                $this->Session->setFlash(
                    __('Adresa a fost modificata!'), 
                    'Alerts/success'
                );
                return $this->redirect(
                    '/customers/users/edit/' 
                    . $user_address['User']['id']
                );
            }
        }
        $countries = $this->_getCountries();
        $this->set(compact('countries'));

        $userCountry = $user_address['UserAddress']['country'];
        $country = $this->Country->findByName($userCountry);

        if ($country == $this->Country->findByName('Romania')) {
            $states = $this->_getCounties();
            $countyName = $user_address['UserAddress']['state'];
            $county = $this->County->findByName($countyName);

            $cities = array();
            if (!empty($county['County']['id'])) {
                $cities = $this->City->find(
                    'list', array(
                        'conditions' => array(
                            'City.county_id' => $county['County']['id']
                        ),
                        'fields' => array(
                            'City.name',
                            'City.name'
                        ),
                        'order' => [
                            'City.county_seat' => 'DESC',
                            'City.name'
                        ]
                    )
                );
            }

            $this->City->bindModel(
                [
                    'belongsTo' => [
                        'County'
                    ]
                ]
            );

            $city = $this->City->find(
                'first', [
                    'conditions' => [
                        'City.name' => $user_address['UserAddress']['city'],
                        'County.name' => $user_address['UserAddress']['state']
                    ],
                    'fields' => [
                        'City.id'
                    ]
                ]
            );

            $this->Street->virtualFields = [
                'display_name' => "CONCAT(Street.name, ' (', type, ')')"
            ];

            $streets = $this->Street->find(
                'list', [
                    'conditions' => [
                        'city_id' => $city['City']['id']
                    ],
                    'fields' => ['display_name']
                ]
            );

            if (!empty($streets)) {
                foreach ($streets as $id => $name) {
                    if (strpos($name, "Ridicare din sediul FAN") === 0) {
                        unset($streets[$id]);
                    }
                }
            }

            if (count($streets) > 1) {
                $this->Street->bindModel(
                    [
                        'belongsTo' => [
                            'City'
                        ]
                    ]
                );

                $userStreet = $this->Street->find(
                    'first', [
                        'conditions' => [
                            'Street.display_name'
                            => $user_address['UserAddress']['street_name'],
                            'City.name'
                            => $user_address['UserAddress']['city']
                        ],
                        'fields' => ['Street.id']
                    ]
                );

                foreach ($streets as $id => $name) {
                    if (empty($name)) {
                        unset($streets[$id]);
                    }
                }
                $this->set(compact('streets', 'userStreet'));
            }

            $this->set(compact('states', 'cities'));
        }
        $this->request->data = $user_address;
    }
    
    /**
    * The Delete address method
    * 
    * This method deletes an address from an user
    * 
    * @param integer $id      the id of the address to be deleted
    * @param integer $user_id the id of the user (helper for redirect)
    * 
    * @return redirect
    */
    public function delete($id = null, $user_id = null) 
    {
        $this->User->id = $user_id;
        if (!$this->User->exists()) {
            $this->Session->setFlash(
                __('Utilizator inexistent'), 
                'Alerts/warning'
            );
            return $this->redirect('/customers/users');
        }
        
        $this->UserAddress->id = $id;
        if (!$this->UserAddress->exists()) {
            $this->Session->setFlash(
                __('Adresa inexistenta'), 
                'Alerts/warning'
            );
            return $this->redirect('/customers/users/edit/' . $user_id);
        }
        // Stergere adrese utilizatori???
        if ($this->UserAddress->delete($id)) {
            $this->Session->setFlash(
                __('Adresa stearsa'), 
                'Alerts/success'
            );
        } else {
            $this->Session->setFlash(
                __('Eroare la stergerea adresei'), 
                'Alerts/error'
            );
        }
        return $this->redirect('/customers/users/edit/' . $user_id);
    }
    
    /**
     * The _getCounties method
     *
     * This method get the Counties
     *
     * @return array
     */
    private function _getCounties()
    {
        $counties = $this->County->find(
            'list', array(
            'order' => 'name',
            'fields' => array(
                'County.name', 
                'County.name'
            )
            )
        );
            
        return $counties;
    }

    /**
     * The _getCountries method
     *
     * Returns array of [Country.name]=>[CountryName]
     *
     * @return array
     */
    private function _getCountries()
    {
        $countries = $this->Country->find(
            'list',
            [
                'order' => 'name',
                'fields' => [
                    'Country.name',
                    'Country.name'
                ]
            ]
        );

        return $countries;
    }

    /**
     * The getRomanianCounties method
     *
     * Get all the counties of Romania
     *
     * @return string
     */
    public function getRomanianCounties()
    {
        if (!$this->request->is('post')) {
            die();
        }

        $this->layout = false;
        $this->autoRender = false;

        /*$countryName = 'Romania';
        $country = $this->Country->findByName(
            $countryName,
            [
                'field' => 'id'
            ]
        );*/

        $counties = $this->County->find(
            'list',
            [
                /*'conditions' =>
                    [
                        'country_id' => $country['Country']['id']
                    ],*/
                'order' => 'name'
            ]
        );

        $return = '<option value="">'.__('Selecteaza judetul...').'</option>';
        foreach ($counties as $c) {
            $return .= "<option value='".$c."'>".$c."</option>";
        }
        echo $return;
    }

    /**
    * The getRomanianCities method
    *
    * Get cities from selected county
    *
    * @return string
    */
    public function getRomanianCities()
    {
        if (!$this->request->is('post')) {
            die();
        }
        $this->layout = false;
        $this->autoRender = false;
        
        $countyName = $this->request->data['county'];
        $county = $this->County->findByName(
            $countyName, array(
            'field' => 'id'
            )
        );
        $cities = $this->City->find(
            'list',
            [
                'conditions' => array(
                    'county_id' => $county['County']['id']
                ),
                'order' => [
                    'county_seat' => 'DESC',
                    'name'
                ]
            ]
        );
        
        $return = '<option value="">'.__('Selecteaza localitatea...').'</option>';
        foreach ($cities as $c) {
            $return .= "<option value='".$c."'>".$c."</option>";
        }
        echo $return;
    }

    /**
     * The getStreet method
     *
     * Get zip code
     *
     * @return string
     */
    public function getZipCode()
    {
        $this->layout = false;
        $this->autoRender = false;

        $nr = trim($this->request->data['number']);
        $nr = str_replace(' ', '', $nr);

        preg_match(
            "/([a-zA-Z])?(\d+)?([a-zA-Z])?(\d+)?([a-zA-Z])?(\d+)?/",
            $nr,
            $matches
        );

        $result = [];

        if (isset($matches[1]) && isset($matches[2]) && isset($matches[3])) {
            if (!empty($matches[1]) && !empty($matches[2]) && !empty($matches[3])) {
                $query = 'CALL get_zip_code('
                    . $this->request->data['street'] . ', "'
                    . $matches[1] . '", "'
                    . $matches[2] . '", "'
                    . $matches[3] .'")';
                $zip = $this->Street->query($query);
                if (!empty($zip)) {
                    $result = $zip;
                }
            }

            if (empty($matches[1]) && !empty($matches[2]) && !empty($matches[3])) {
                $query = "CALL get_zip_code("
                    . $this->request->data['street'] . ", '', '"
                    . $matches[2] . "', '"
                    . $matches[3] ."')";
                $zip = $this->Street->query($query);
                if (!empty($zip)) {
                    $result = $zip;
                }

                $query = 'CALL get_zip_code('
                    . $this->request->data['street'] . ", '"
                    . $matches[2] . "', '"
                    . $matches[3] . "', '')";
                $zip = $this->Street->query($query);
                if (!empty($zip)) {
                    $result = $zip;
                }
            }

        } elseif (isset($matches[1]) && isset($matches[2]) && !isset($matches[3])) {
            if (!empty($matches[1]) && !empty($matches[2])) {
                $query = "CALL get_zip_code("
                    . $this->request->data['street'] . ", '', '"
                    . $matches[1] . "', '"
                    . $matches[2] ."')";
                $zip = $this->Street->query($query);
                if (!empty($zip)) {
                    $result = $zip;
                }

                $query = 'CALL get_zip_code('
                    . $this->request->data['street'] . ", '"
                    . $matches[1] . "', '"
                    . $matches[2] . "', '')";
                $zip = $this->Street->query($query);
                if (!empty($zip)) {
                    $result = $zip;
                }
            } elseif (empty($matches[1]) && !empty($matches[2])) {
                $query = "CALL get_zip_code("
                    . $this->request->data['street'] . ", '', '"
                    . $matches[2] . "', '')";
                $zip = $this->Street->query($query);
                if (!empty($zip)) {
                    $result = $zip;
                }
            }
        } elseif (isset($matches[1]) && !isset($matches[2]) && !isset($matches[3])) {
            if (!empty($matches[1])) {
                $query = "CALL get_zip_code("
                    . $this->request->data['street'] . ", '', '"
                    . $matches[1] . "', '')";
                $zip = $this->Street->query($query);
                if (!empty($zip)) {
                    $result = $zip;
                }
            }
        }

        if (!empty($result)) {
            die(
                json_encode(
                    [
                    'success' => 1,
                    'code'    => $result[0]['zc']['code']
                    ]
                )
            );
        }

        $nr = preg_replace("/[^0-9]/", "", $nr);
        $query = "CALL get_zip_code("
            . $this->request->data['street'] . ", '', '"
            . $nr . "', '')";
        $zip = $this->Street->query($query);

        if (!empty($zip)) {
            die(
                json_encode(
                    [
                    'success' => 1,
                    'code'    => $zip[0]['zc']['code']
                    ]
                )
            );
        }

        die(
            json_encode(
                [
                'success' => 0,
                ]
            )
        );
    }

    /**
     * The getStreets method
     *
     * Get streets from selected city
     *
     * @return string
     */
    public function getStreets()
    {
        $this->layout = false;
        $this->autoRender = false;

        $cityName = $this->request->data['city'];
        $city = $this->City->findByName(
            $cityName, array(
                'field' => 'id'
            )
        );

        $this->Street->virtualFields = array(
            'display_name' => "CONCAT(name, ' (', type, ')')"
        );

        $streets = $this->Street->find(
            'list', array(
                'conditions' => array(
                    'city_id' => $city['City']['id'],
                ),
                'order' => 'name',
                'fields' => [
                    'id',
                    'display_name',
                ]

            )
        );

        if (!empty($streets)) {
            foreach ($streets as $id => $name) {
                if (strpos($name, "Ridicare din sediul FAN") === 0) {
                    unset($streets[$id]);
                }
            }
        }

        $k = 0;
        if (count($streets) > 1) {
            foreach ($streets as $id => $name) {
                if (empty($name)) {
                    continue;
                }
                $return[$k]['id'] = $id;
                $return[$k++]['name'] = $name;
            }

            die(
                json_encode(
                    [
                    'status' => 1,
                    'streets' => $return
                    ]
                )
            );
        }

        if (count($streets) == 1 && empty(array_values($streets)[0])) {

            $street_id = array_keys($streets)[0];
            $zip = $this->ZipCode->findByStreetId($street_id, ['code']);

            die(
                json_encode(
                    [
                    'status' => 2,
                    'code' => $zip['ZipCode']['code']
                    ]
                )
            );
        }
    }
}
