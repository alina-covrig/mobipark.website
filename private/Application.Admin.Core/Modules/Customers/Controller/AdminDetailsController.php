<?php
/**
* AdminDetailsController
*
* Inside this Controller admin logic methods will
* be placed
*
* PHP version 5.4
*
* @category  Admin
* @package   Customers
 * @author    Badeci Gabriel <lucian.badeci@gmail.com>
* @copyright 2014 Evercloud
* @license   http://www.php.net/license/3_01.txt  PHP License 3.01
* @version   SVN: $Id$
* @link      /customers/admin_details
* @since     1.0
*/

App::uses('Customers.CustomersAppController', 'Controller');

/**
* AdminDetailsController Class
*
* Inside this Controller admin logic methods will
* be placed
*
* @category  Admin
* @package   Customers
 * @author    Badeci Gabriel <lucian.badeci@gmail.com>
* @copyright 2014 Evercloud
* @license   http://www.php.net/license/3_01.txt  PHP License 3.01
* @version   Release: 1.0
* @link      /Customers/admin_details
* @since     Class available since Release 1.0     
*/
class AdminDetailsController extends CustomersAppController
{
    /**
    * The models used by this controller
    * 
    * @var 
    */
    public $uses = array(
        'Customers.User',
        'Customers.AdminDetail',
    );
    
    /**
    * The Edit address method
    * 
    * This method allows editing user details
    *
    * @param integer $user_id the id of the user
    * 
    * @return mixed
    */
    public function edit($user_id = null)
    {
        if ($this->request->is(['post', 'put'])) {
            if ((int) $this->request->data['AdminDetail']['picture']['error']
                === UPLOAD_ERR_NO_FILE
            ) {
                unset($this->request->data['AdminDetail']['picture']);
            } else {
                if (!is_dir(WWW_ROOT . 'admin_pictures')) {
                    mkdir(WWW_ROOT . 'admin_pictures');
                }
                $name = "/admin_pictures/" . time() . '_'
                    . $this->request->data['AdminDetail']['picture']['name'];
                $dest = WWW_ROOT . $name;
                if (!move_uploaded_file(
                    $this->request->data['AdminDetail']['picture']['tmp_name'],
                    $dest
                )) {
                    $this->Session->setFlash(
                        __('Poza nu a putut fi urcata!'),
                        'Alerts/error'
                    );
                } else {
                    $this->request->data['AdminDetail']['picture'] = $name;
                }
            }

            $this->request->data['AdminDetail']['user_id'] = $user_id;
            if ($this->AdminDetail->save($this->request->data)) {
                $this->Session->setFlash(
                    __('Detaliile despre admin au fost modificate!'),
                    'Alerts/success'
                );
                return $this->redirect(
                    '/customers/admin_details/edit/'
                    . $user_id
                );
            }
        }

        $this->User->bindModel(
            [
            'hasOne' => [
                'AdminDetail' => [
                    'className' => 'Customers.AdminDetail',
                    'order' => 'AdminDetail.id DESC',
                    'limit' => 1
                ]
            ]
            ]
        );

        $admin_details = $this->User->findById($user_id);
        $this->request->data = $admin_details;
    }
}
