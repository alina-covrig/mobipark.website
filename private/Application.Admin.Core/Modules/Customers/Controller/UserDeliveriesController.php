<?php
/**
 * UserDeliveriesController
 *
 * Inside this Controller admin logic methods will
 * be placed
 *
 * PHP version 5.4
 *
 * @category  Admin
 * @package   Customers
 * @author    Daniel Ionascu <daniel.ionascu88@gmail.com>
 * @copyright 2016 Evercloud
 * @license   http://www.php.net/license/3_01.txt  PHP License 3.01
 * @version   SVN: $Id$
 * @link      /customers/user_deliveries
 * @since     1.0
 */

App::uses('Customers.CustomersAppController', 'Controller');

/**
 * UserDeliveriesController Class
 *
 * Inside this Controller admin logic methods will
 * be placed
 *
 * @category  Admin
 * @package   Customers
 * @author    Daniel Ionascu <daniel.ionascu88@gmail.com>
 * @copyright 2016 Evercloud
 * @license   http://www.php.net/license/3_01.txt  PHP License 3.01
 * @version   Release: 1.0
 * @link      /Customers/user_deliveries
 * @since     Class available since Release 1.0
 */
class UserDeliveriesController extends CustomersAppController
{
    /**
     * The components used by this controller
     *
     * @var
     */
    public $component = array('Paginator');

    public function index($user_id = null)
    {
        $this->User->id = $user_id;
        if (!$this->User->exists()) {
            $this->Session->setFlash(
                __('Utilizator inexistent'),
                'Alerts/warning'
            );
            return $this->redirect($this->request->referer());
        }

        $this->UserDelivery->bindModel(
            [
                'belongsTo' => [
                    'Admin' => [
                        'className' => 'Customers.User',
                        'foreignKey' => 'admin_id'
                    ],
                ],
            ]
        );

        $deliveryMethods = $this->DeliveryMethod->find(
            'list', [
                'conditions' => [
                    'is_active' => 1,
                    'is_bonus'  => 1
                ]
            ]
        );

        $this->UserDelivery->contain([
            'Sale.id',
            'User.id',
            'User.Person',
            'DeliveryMethod',
            'Admin.Person',
            'Admin.id'
        ]);

        $conditions = [
            'UserDelivery.user_id' => $user_id
        ];

        $this->Paginator->settings = [
            'order' => 'UserDelivery.created_at DESC',
            'conditions' => $conditions
        ];

        $deliveries = $this->Paginator->paginate();
        $this->set(compact(
            'deliveries',
            'user_id',
            'deliveryMethods'
        ));

    }

    /**
     * The models used by this controller
     *
     * @var
     */
    public $uses = array(
        'Customers.UserDelivery',
        'Customers.User',
        'Orders.DeliveryMethod',
        'Cms.Notification'
    );

    /**
     * The Add address method
     *
     * This method adds an adress for a user
     * Renders /Customers/UserAddresses/add.ctp
     *
     * @param integer $user_id the user id to add an address to
     *
     * @return void or redirect on post or error
     */
    public function add($user_id = null)
    {
        $this->User->id = $user_id;
        if (!$this->User->exists()) {
            $this->Session->setFlash(
                __('Utilizator inexistent'),
                'Alerts/warning'
            );
            return $this->redirect($this->request->referer());
        }

        if ($this->request->is('post')) {
            $data = $this->request->data;
            $data['UserDelivery']['admin_id'] = $this->getCurrentUserId();
            $data['UserDelivery']['created_at'] = date('Y-m-d H:i:s');

            for($i = 1; $i <= $data['UserDelivery']['quantity']; $i++) {
                $this->UserDelivery->create();
                $this->UserDelivery->save($data);
            }

            $notification['user_id']    = $user_id;
            $notification['created_at'] = date('Y-m-d H:i:s');
            $notification['is_deleted'] = false;
            $notification['is_viewed']  = false;
            $notification['subject']    = 'Contul dumneavoastra a '
                . 'fost creditat cu ' . $data['UserDelivery']['quantity']
                . ' transport(uri) gratuit(e)';
            $notification['message']    = $notification['subject'];

            $this->Notification->create();
            $this->Notification->save($notification);

            return $this->respond(
                __('Datele au fost salvate cu succes'),
                'succes  '
            );
        }

        $deliveryMethods = $this->DeliveryMethod->find(
            'list', [
                'conditions' => [
                    'is_active' => 1
                ]
            ]
        );

        $options = [
            'conditions' => [
                'User.id' => $user_id
            ]
        ];
        $user = $this->User->find('first', $options);

        $this->set(compact('deliveryMethods', 'user'));
    }

    public function delete($id = null, $user_id = null)
    {
        $this->User->id = $user_id;
        if (!$this->User->exists()) {
            $this->respond(
                __('Utilizator inexistent'),
                'error'
            );
        }

        $userDelivery = $this->UserDelivery->findById($id);
        if (empty($userDelivery)) {
            return $this->respond(
                __('Transportul nu exista'),
                'error'
            );
        }

        if ($userDelivery['UserDelivery']['is_used']) {
            return $this->respond(
                __('Un transport folosit nu poate fi sters'),
                'error'
            );
        }

        if (!$this->UserDelivery->delete($id)) {
            return $this->respond(
                __('Eroare la stergerea transportului'),
                'error'
            );
        }

        return $this->respond(
            __('Transportul a fost sters cu succes'),
            'success'
        );
    }

    public function resetResellersDeliveries()
    {
        $userTypes = $this->UserType->find(
            'all', [
                'fields' => [
                    'id', 'delivery_nr', 'delivery_method_id'
                ]
            ]
        );

        $transaction = $this->UserDelivery->getDataSource();
        $transaction->begin();

        foreach($userTypes as $type) {
            $users = $this->User->find(
                'list', [
                    'conditions' => [
                        'user_type_id' => $type['UserType']['id'],
                        'is_active' => 1
                    ],
                    'fields' => [
                        'id'
                    ]
                ]
            );

            if (!empty($users)) {
                foreach ($users as $u) {
                    $deliveries = $this->UserDelivery->find(
                        'list', [
                            'conditions' => [
                                'UserDelivery.user_id' => $u,
                                'is_used' => 0,
                            ],
                            'fields' => ['id']
                        ]
                    );

                    $this->UserDelivery->deleteAll([
                        'UserDelivery.id' => $deliveries
                    ]);

                    for ($i = 1; $i <= $type['UserType']['delivery_nr']; $i++) {
                        $save['user_id'] = $u;
                        $save['created_at'] = date('Y-m-d H:i:s');
                        $save['delivery_method_id']
                            = $type['UserType']['delivery_method_id'];

                        $this->UserDelivery->create();
                        if (!$this->UserDelivery->save($save)) {
                            $transaction->rollback();

                            $subject = 'A aparut o eroare la setarea '
                                . 'transporturilor pentru reselleri';

                            App::uses('CakeEmail', 'Network/Email');
                            $em = new CakeEmail('default');
                            $em->to(Configure::read('STORE_CONTACT_EMAIL'));
                            $em->emailFormat('text');
                            $em->subject($subject);
                            $em->message($subject);
                            if (!$em->send()) {
                                die('A aparut o eroare la setarea '
                                    . 'transporturilor pentru reselleri si '
                                    . 'nu s-a trimis email');
                            }

                            die('A aparut o eroare la setarea '
                                . 'transporturilor pentru reselleri');
                        }
                    }

                    $notifications['user_id'] = $u;
                    $notifications['subject'] = 'Contul dumneavoastra a fost '
                        . 'creditat cu '
                        . $type['UserType']['delivery_nr']
                        . ' transport(uri) gratuit(e)';
                    $notifications['message'] = $notifications['subject'];
                    $notifications['is_deleted'] = false;
                    $notifications['is_viewed']  = false;
                    $notifications['created_at'] = date('Y-m-d H:i:s');

                    $Notification = ClassRegistry::init('Cms.Notification');
                    $Notification->create();
                    $Notification->save($notifications);

                }
            }
        }

        $transaction->commit();

        $subject = 'Transporturile pentru reselleri au fost resetate cu succes.';

        App::uses('CakeEmail', 'Network/Email');
        $em = new CakeEmail('default');
        $em->to(Configure::read('STORE_CONTACT_EMAIL'));
        $em->emailFormat('text');
        $em->subject($subject);
        $em->message($subject);
        if (!$em->send()) {
            die('Transporturile pentru reselleri au fost resetate cu succes dar '
                . 'nu s-a trimis email');
        }
        die ($subject);
    }
}
