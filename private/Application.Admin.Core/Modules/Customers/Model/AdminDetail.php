<?php
/**
* AdminDetail.php
*
* Inside this Model any module-wide
* data manipulation methods will be placed
*
* PHP version 5.4
*
* @category  Admin
* @package   Customers
* @author    Badeci Gabriel <lucian.badeci@gmail.com>
* @copyright 2014 Evercloud
* @license   http://www.php.net/license/3_01.txt  PHP License 3.01
* @version   SVN: $Id$
* @link      /modules/Customers/models/admindetail
* @since     1.0
*/

App::uses('CustomersAppModel', 'Customers.Model');


/**
* AdminDetail Model Class
*
* Inside this Model any application-wide
* data manipulation methods will be placed
*
 * @category  Admin
 * @package   Customers
 * @author    Badeci Gabriel <lucian.badeci@gmail.com>
 * @copyright 2014 Evercloud
 * @license   http://www.php.net/license/3_01.txt  PHP License 3.01
 * @version   Release: 1.0
 * @link      /modules/Customers/models/admindetail
 * @since     1.0
*/
class AdminDetail extends CustomersAppModel
{

    /**
    * Display field
    *
    * @var string
    */
    public $displayField = 'user_id';

    /**
    * ActsAs field
    *
    * @var string
    */
    public $actsAs = array('Containable');
    
    /**
    * HasOne model relationship
    * 
    * @var 
    */
    public $belongsTo = array('Customers.User');
}
