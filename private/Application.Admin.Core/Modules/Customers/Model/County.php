<?php
/**
* County.php
*
* Inside this Model any module-wide
* data manipulation methods will be placed
*
* PHP version 5.4
*
* @category  Admin
* @package   Customers
* @author    Daniel Ionascu <daniel.ionascu88@gmail.com>
* @copyright 2014 Evercloud
* @license   http://www.php.net/license/3_01.txt  PHP License 3.01
* @version   SVN: $Id$
* @link      /modules/Customers/models/counties
* @since     1.0
*/

App::uses('CustomersAppModel', 'Customers.Model');


/**
* County Model Class
*
* Inside this Model any application-wide
* data manipulation methods will be placed
*
* @category  Admin
* @package   Customers
* @author    Daniel Ionascu <daniel.ionascu88@gmail.com>
* @copyright 2014 Evercloud
* @license   http://www.php.net/license/3_01.txt  PHP License 3.01
* @version   Release: 1.0
* @link      /modules/Customers/models/counties
* @since     Class available since Release 1.0
*/
class County extends CustomersAppModel
{

    /**
    * Display field
    *
    * @var string
    */
    public $displayField = 'name';

    /**
     * The getCounties method
     *
     * This method get the Counties
     *
     * @return array
     */
    public function getCounties ()
    {
        $counties = $this->find(
            'list', array(
                'order' => 'name',
                'fields' => array(
                    'name',
                    'name'
                )
            )
        );

        return $counties;
    }
}
