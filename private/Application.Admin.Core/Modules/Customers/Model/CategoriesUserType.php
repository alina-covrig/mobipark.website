<?php
/**
* CategoriesUserType.php
*
* Inside this Model any module-wide
* data manipulation methods will be placed
*
* PHP version 5.4
*
* @category  Admin
* @package   Customers
* @author    Gabriel Lucian Badeci <lucian.badeci@gmail.com>
* @copyright 2014 Evercloud
* @license   http://www.php.net/license/3_01.txt  PHP License 3.01
* @version   SVN: $Id$
* @link      /modules/Customers/models/categories-user-type
* @since     1.0
*/

App::uses('CustomersAppModel', 'Customers.Model');

/**
* UserType Model Class
*
* Inside this Model any application-wide
* data manipulation methods will be placed
*
* @category  Admin
* @package   Customers
* @author    Gabriel Lucian Badeci <lucian.badeci@gmail.com>
* @copyright 2014 Evercloud
* @license   http://www.php.net/license/3_01.txt  PHP License 3.01
* @version   Release: 1.0-type
* @link      /modules/Customers/models/categories-user-type
* @since     Class available since Release 1.0
*/

class CategoriesUserType extends CustomersAppModel
{
    /**
     * BelongsTo model relationship
     *
     * @var
     */
    public $belongsTo = array(
        'Category' => array(
            'className' => 'Catalog.Category',
        ),
        'UserType' => array(
            'className' => 'Customers.UserType',
        )
    );

    /**
     * Validation rules
     *
     * @var array
     */
    public $validate = array(
        'discount_percent' => array(
            'nat' => array(
                'rule' => array('naturalNumber', true),
                'message' => 'Introduceti un numar natural!',
            ),
            'myRange' => array(
                'rule' => array('myRangeTest'),
                'message' => 'Discount prea mare',
            )
        ),
    );

    /**
     * Validation rules
     *
     * @param integer $value discount value
     *
     * @return bool
     */
    public function myRangeTest($value)
    {
        if ($value['discount_percent'] <= Configure::read('MAX_PERCENT_COUPON')) {
            return true;
        } else {
            return false;
        }
    }
}
