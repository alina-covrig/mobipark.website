<?php
/**
* UserAddress.php
*
* Inside this Model any module-wide
* data manipulation methods will be placed
*
* PHP version 5.4
*
* @category  Admin
* @package   Customers
* @author    Lucian Vasile <lucian.vasile@live.com>
* @copyright 2014 Evercloud
* @license   http://www.php.net/license/3_01.txt  PHP License 3.01
* @version   SVN: $Id$
* @link      /modules/Customers/models/user-address
* @since     1.0
*/

App::uses('CustomersAppModel', 'Customers.Model');


/**
* UserAddress Model Class
*
* Inside this Model any application-wide
* data manipulation methods will be placed
*
* @category  Admin
* @package   Customers
* @author    Lucian Vasile <lucian.vasile@live.com>
* @copyright 2014 Evercloud
* @license   http://www.php.net/license/3_01.txt  PHP License 3.01
* @version   Release: 1.0
* @link      /modules/Customers/models/user-address
* @since     Class available since Release 1.0
*/
class UserAddress extends CustomersAppModel
{

    /**
    * Display field
    *
    * @var string
    */
    public $displayField = 'displayName';
    
    /**
    * Virtual fields for every select query
    * 
    * @var 
    */
    public $virtualFields = array(
        'displayName'
        => "CONCAT(
            IF(LENGTH(`UserAddress.street_name`),CONCAT(`UserAddress.street_name`, ' ') ,''),
            IF(LENGTH(`UserAddress.street_nr`),CONCAT('nr. ',`UserAddress.street_nr`, ', ') ,''),
            IF(LENGTH(`UserAddress.address_line_1`),CONCAT(`UserAddress.address_line_1`, ', ') ,''),
            UserAddress.city
        )"
    );
    
    public $belongsTo = array('Customers.User');

    /**
    * Validation rules
    *
    * @var array
    */
    public $validate = array(
    //         'address_line_1' => array(
    //             'length' => array(
    //                 'rule'    => array('minLength', '3'),
    //                 'message' => 'Minim 3 caractere'
    //             ),
    //         ),
    //         'city' => array(
    //             'required' => array(
    //                 'rule'    => 'notBlank',
    //                 'message' => 'Completati localitatea'
    //             ),
    //         ),
    //         'state' => array(
    //             'required' => array(
    //                 'rule'    => 'notBlank',
    //                 'message' => 'Completati judetul'
    //             ),
    //         ),
    //         'country' => array(
    //             'required' => array(
    //                 'rule'    => 'notBlank',
    //                 'message' => 'Completati tara'
    //             ),
    //         ),
        'fname' => array(
            'required' => array(
                'rule'    => 'notBlank',
                'message' => 'Completati prenumele'
            ),
            'is_name' => array(
                'rule' => REGEX_FULL_USERNAME,
                'message'
                    => 'Prenumele trebuie sa contina doar litere, fara diacritice'
            ),
        ),
        'lname' => array(
            'required' => array(
                'rule'    => 'notBlank',
                'message' => 'Completati numele'
            ),
            'is_name' => array(
                'rule' => REGEX_FULL_USERNAME,
                'message'
                    => 'Numele trebuie sa contina doar litere, fara diacritice'
            ),
        ),
        /*'phone' => array(
            'notBlank' => array(
                'rule' => array('notBlank'),
                'message' => 'Completati telefonul',
                'required' => true,
            ),
            'is_phone' => array(
                'rule' => REGEX_PHONE,
                'message' => 'Telefonul nu este valid'
            ),
        ),*/
        'zip' => array(
            'size' => array(
                'rule'    => array('between', 4, 10),
                'message' => 'Cod postal invalid'
            ),
        ),
    );
}
