<?php
/**
 * UserDelivery.php
 *
 * Inside this Model any module-wide
 * data manipulation methods will be placed
 *
 * PHP version 5.4
 *
 * @category  Admin
 * @package   Customers
 * @author    Daniel Ionascu <daniel.ionscu88@gmail.com>
 * @copyright 2016 Evercloud
 * @license   http://www.php.net/license/3_01.txt  PHP License 3.01
 * @version   SVN: $Id$
 * @link      /modules/Customers/models/user-deliveries
 * @since     1.0
 */

App::uses('CustomersAppModel', 'Customers.Model');


/**
 * UserDelivery Model Class
 *
 * Inside this Model any application-wide
 * data manipulation methods will be placed
 *
 * @category  Admin
 * @package   Customers
 * @author    Daniel Ionascu <daniel.ionscu88@gmail.com>
 * @copyright 2016 Evercloud
 * @license   http://www.php.net/license/3_01.txt  PHP License 3.01
 * @version   Release: 1.0
 * @link      /modules/Customers/models/user-deliveries
 * @since     Class available since Release 1.0
 */
class UserDelivery extends CustomersAppModel
{

    /**
     * Display field
     *
     * @var string
     */
    public $displayField = 'displayName';

    public $actsAs = array('Containable');

    public $belongsTo = [
        'Customers.User',
        'Orders.DeliveryMethod',
        'Orders.Sale'
    ];
}
