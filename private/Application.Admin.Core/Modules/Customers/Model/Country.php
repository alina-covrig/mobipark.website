<?php

App::uses('CustomersAppModel', 'Customers.Model');

/**
 * Country Model Class
 *
 * Inside this Model any application-wide
 * data manipulation methods will be placed
 *
 * @category  Admin
 * @package   Customers
 * @author    Mihai-Octavian Bita <mihaioctavianbita@gmail.com>
 * @copyright 2016 Evercloud
 * @license   http://www.php.net/license/3_01.txt  PHP License 3.01
 * @version   Release: 1.0
 * @link      /modules/Customers/models/countries
 * @since     Class available since Release 1.0
 */

class Country extends CustomersAppModel
{

    /**
     * Display field
     *
     * @var string
     */
    public $displayField = 'name';

    /**
     * The getCountries method
     *
     * This method gets the Countries
     *
     * @return array
     */
    public function getCountries()
    {
        $counties = $this->find(
            'list', array(
                'order' => 'name',
                'fields' => array(
                    'name',
                    'name'
                )
            )
        );

        return $counties;
    }
}
