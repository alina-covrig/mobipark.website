<?php
/**
 * StoreCreditRequest.php
 *
 * Inside this Model any module-wide
 * data manipulation methods will be placed
 *
 * PHP version 5.4
 *
 * @category  Admin
 * @package   Customers
 * @author    Lucian Vasile <lucian.vasile@live.com>
 * @copyright 2014 Evercloud
 * @license   http://www.php.net/license/3_01.txt  PHP License 3.01
 * @version   SVN: $Id$
 * @link      /modules/Customers/models/store-credit-request
 * @since     1.0
 */

App::uses('CustomersAppModel', 'Customers.Model');


/**
 * StoreCreditRequest Model Class
 *
 * Inside this Model any application-wide
 * data manipulation methods will be placed
 *
 * @category  Admin
 * @package   Customers
 * @author    Lucian Vasile <lucian.vasile@live.com>
 * @copyright 2014 Evercloud
 * @license   http://www.php.net/license/3_01.txt  PHP License 3.01
 * @version   Release: 1.0
 * @link      /modules/Customers/models/store-credit-request
 * @since     Class available since Release 1.0
 */
class StoreCreditRequest extends CustomersAppModel
{
    
}
