<?php
/**
 * SalesStoreCredit.php
 *
 * Inside this Model any module-wide
 * data manipulation methods will be placed
 *
 * PHP version 5.4
 *
 * @category  Admin
 * @package   Customers
 * @author    Lucian Vasile <lucian.vasile@live.com>
 * @copyright 2014 Evercloud
 * @license   http://www.php.net/license/3_01.txt  PHP License 3.01
 * @version   SVN: $Id$
 * @link      /modules/Customers/models/sales-tore-credit
 * @since     1.0
 */

App::uses('CustomersAppModel', 'Customers.Model');


/**
 * SalesStoreCredit Model Class
 *
 * Inside this Model any application-wide
 * data manipulation methods will be placed
 *
 * @category  Admin
 * @package   Customers
 * @author    Lucian Vasile <lucian.vasile@live.com>
 * @copyright 2014 Evercloud
 * @license   http://www.php.net/license/3_01.txt  PHP License 3.01
 * @version   Release: 1.0
 * @link      /modules/Customers/models/sales-store-credit
 * @since     Class available since Release 1.0
 *
 * @property StoreCredit StoreCredit
 */
class SalesStoreCredit extends CustomersAppModel
{
    public function getInvoices($sale_id) {
        $this->bindModel(array(
            'belongsTo' => array(
                'StoreCredit' => array(
                    'className' => 'Customers.StoreCredit'
                )
            )
        ));
        $this->StoreCredit->bindModel(array(
            'belongsTo' => array(
                'Invoice' => array(
                    'className' => 'Orders.Invoice'
                )
            )
        ));
        $this->Behaviors->load('Containable');
        $this->contain(array(
            'StoreCredit' => array(
                'Invoice'
            )
        ));
        $invoices = $this->find('all', array(
            'conditions' => array(
                'SalesStoreCredit.sale_id' => $sale_id
            ),
        ));
        $inv_array = array();
        foreach ($invoices as $k => $i) {
            $inv_array[$k]['id'] = $i['StoreCredit']['Invoice']['id'];
            $inv_array[$k]['value'] = $i['StoreCredit']['Invoice']['value'];
            $inv_array[$k]['used_value'] = $i['SalesStoreCredit']['value'];
            $inv_array[$k]['series'] = $i['StoreCredit']['Invoice']['series'];
            $inv_array[$k]['number'] = $i['StoreCredit']['Invoice']['number'];
        }
        return $inv_array;
    }
}
