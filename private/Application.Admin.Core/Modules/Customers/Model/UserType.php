<?php
/**
 * UserType.php
 *
 * Inside this Model any module-wide
 * data manipulation methods will be placed
 *
 * PHP version 5.4
 *
 * @category  Admin
 * @package   Customers
 * @author    Lucian Vasile <lucian.vasile@live.com>
 * @copyright 2014 Evercloud
 * @license   http://www.php.net/license/3_01.txt  PHP License 3.01
 * @version   SVN: $Id$
 * @link      /modules/Customers/models/user-type
 * @since     1.0
 */

App::uses('CustomersAppModel', 'Customers.Model');


/**
 * UserType Model Class
 *
 * Inside this Model any application-wide
 * data manipulation methods will be placed
 *
 * @category  Admin
 * @package   Customers
 * @author    Lucian Vasile <lucian.vasile@live.com>
 * @copyright 2014 Evercloud
 * @license   http://www.php.net/license/3_01.txt  PHP License 3.01
 * @version   Release: 1.0
 * @link      /modules/Customers/models/user-type
 * @since     Class available since Release 1.0
 */
class UserType extends CustomersAppModel
{

    /**
     * Display field
     *
     * @var string
     */
    public $displayField = 'name';

    /**
     * Validation rules
     *
     * @var array
     */
    public $validate = array(
        'name'        => array(
            'unique' => array(
                'rule'    => 'isUnique',
                'required'=> true,
                'message' => 'Denumirea exista deja!'
            ),
            'max' => array(
                'rule' => array('maxLength', 20),
                'message' => 'Lungimea maxima e de 20 caractere'
            ),
        ),
        'icon' => array(
            'upload' => array(
                'rule' => 'uploadUserTypeIcon',
                'allowEmpty' => true,
                'message' => "Eroare la incarcarea imaginii. Incercati din nou."
            ),
            'smallImage' => array(
                'rule' => array('fileSize', '<=', '50KB'),
                'allowEmpty' => true,
                'message' => "Imaginea trebuie sa fie mai mica de 50 Kb"
            ),
        ),
        'points_from' => array(
            'rule' => array('naturalNumber', true),
            'message' => 'Introduceti un numar natural!'
        ),
        'points_to' => array(
            'natural' => array(
                'rule' => array('naturalNumber', true),
                'message' => 'Introduceti un numar natural!'
            ),
            'gt_points_from' => array(
                'rule' => 'greaterPointsFrom',
                'message' => 'Introduceti un numar mai mare decat `De la`;',
            ),
        ),
        'discount' => array(
            'nat' => array(
                'rule' => array('naturalNumber', true),
                'message' => 'Introduceti un numar natural!',
            ),
            'range' => array(
                'rule' => array('range', -1, 50),
                'message' => 'Introduceti un numar intre 0 si 99',
            ),
        ),
        'delivery_nr' => array(
            'needBoth' => array(
                'rule' => 'checkBoth',
                'message' => 'Numarul de transporturi si metoda de transport trebuie completate impreuna',
            ),
            'number' => array(
                'allowEmpty' => true,
                'rule' => array('naturalNumber', true),
                'message' => 'Introduceti un numar natural!'
            )
        ),
        'delivery_method_id' => array(
            'needBoth' => array(
                'rule' => 'checkBoth',
                'message' => 'Numarul de transporturi si metoda de transport trebuie completate impreuna',
            ),
        )
    );

    function checkBoth($field) {
        if(
            (
                empty($this->data[$this->alias]['delivery_nr']) &&
                empty($this->data[$this->alias]['delivery_method_id'])
            )
            ||
            (
                !empty($this->data[$this->alias]['delivery_nr']) &&
                !empty($this->data[$this->alias]['delivery_method_id'])
            )
        )
        {
            return true;
        } else {
            return false;
        }
    }
    
    /**
     * The uploadUserTypeIcon method
     *
     * Checks if $this->data contains an uploaded file, saves it
     * and alters the $this->data['UserType']['icon'] accordingly
     *
     * @param mixed $check contains data from the $this->data['UserType']['icon']
     *
     * @return void
     */
    public function uploadUserTypeIcon($check)
    {
        if ((int) $check['icon']['error'] === UPLOAD_ERR_NO_FILE) {
            $this->data['UserType']['icon'] = '';
            return true;
        }
        if (!((int) $check['icon']['error'] === UPLOAD_ERR_OK)) {
            return false;
        }
        if (!is_uploaded_file($check['icon']['tmp_name'])) {
            return false;
        }
        
        $pathi = pathinfo($check['icon']['name']);
        if (!isset($pathi['extension'])
            || !in_array(
                strtolower($pathi['extension']),
                array('jpg', 'jpeg', 'png')
            )
        ) {
            return false;
        }
        
        $destination_dir = 'img' . DS . 'user_types';
        $new_name = time() . '_' . $check['icon']['name'];
        if (!$this->remoteUpload(
            $check['icon']['tmp_name'],
            $destination_dir, $new_name
        )) {
            return false;
        }
        $this->data['UserType']['icon'] = $new_name;
        return true;
    }
    
    /**
     * The greaterPointsFrom validation method
     *
     * This method is called upon validation in order to check
     * if the points from is less than points tp
     *
     * @param array $check the field to be validated
     *
     * @return bool
     */
    public function greaterPointsFrom($check)
    {
        if ($check['points_to'] <= $this->data['UserType']['points_from']) {
            return false;
        }
        return true;
    }
}
