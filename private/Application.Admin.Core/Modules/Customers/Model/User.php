<?php
/**
* User.php
*
* Inside this Model any module-wide
* data manipulation methods will be placed
*
* PHP version 5.4
*
* @category  Admin
* @package   Customers
* @author    Lucian Vasile <lucian.vasile@live.com>
* @copyright 2014 Evercloud
* @license   http://www.php.net/license/3_01.txt  PHP License 3.01
* @version   SVN: $Id$
* @link      /modules/Customers/models/user
* @since     1.0
*/

App::uses('CustomersAppModel', 'Customers.Model');


/**
* User Model Class
*
* Inside this Model any application-wide
* data manipulation methods will be placed
*
* @category  Admin
* @package   Customers
* @author    Lucian Vasile <lucian.vasile@live.com>
* @copyright 2014 Evercloud
* @license   http://www.php.net/license/3_01.txt  PHP License 3.01
* @version   Release: 1.0
* @link      /modules/Customers/models/user
* @since     Class available since Release 1.0
*/
class User extends CustomersAppModel
{

    /**
    * Display field
    *
    * @var string
    */
    public $displayField = 'email';

    /**
    * ActsAs field
    *
    * @var string
    */
    public $actsAs = array('Containable');
    
    /**
    * HasOne model relationship
    * 
    * @var 
    */
    public $hasOne = array(
        'Customers.Person',
        'Customers.AdminDetail',
        'Customers.UserSetting'
    );
    
    /**
    * BelongsTo model relationship
    * 
    * @var 
    */
    public $belongsTo = array('Customers.UserType');

    /**
    * Validation rules
    *
    * @var array
    */
    public $validate = array(
        'email' => array(
            'email' => array(
                'rule' => array('email'),
                'required' => true,
                'message' => 'Va rugam sa introduceti un email'
            ),
            'unique' => array(
                'rule' => 'isUnique',
                'required' => true,
                'message' => 'Emailul este deja inregistrat'
            ),
        ),
        'password' => array(
            'notBlank' => array(
                'rule' => array('notBlank'),
                'message' => 'Completati parola',
                'required' => true,
                'on' => 'create'
            ),
            'complexity' => array(
                'rule' => array('minLength', '6'),
                'message' => 'Parola trebuie sa aiba minim 6 caractere',
                'required' => true,
                'on' => 'create'
            ),
            'identical' => array(
                'rule' => "checkRepassword",
                'message' => 'Parolele nu se potrivesc',
                'required' => true,
                'on' => 'create'
            ),
        ),
        'new-password' => array(
            'complexity' => array(
                'rule' => array('minLength', '6'),
                'message' => 'Parola trebuie sa aiba minim 6 caractere',
                'required' => false,
                'on' => 'update',
                'allowEmpty' => true,
            ),
            'identical' => array(
                'rule' => "checkRepassword",
                'message' => 'Parolele nu se potrivesc',
                'required' => false,
                'on' => 'update',
                'allowEmpty' => true,
            ),
        ),
        'phone' => array(
            'notBlank' => array(
                'rule' => array('notBlank'),
                'message' => 'Completati telefonul',
                'required' => true,
            ),
            'is_phone' => array(
                'rule' => REGEX_PHONE,
                'message' => 'Telefonul nu este valid'
            ),
        ),
    );
    
    /**
    * The beforeValidate callback method
    * 
    * This method is called before $this->data is validated
    * 
    * @param array $options query options
    * 
    * @return true
    */
    public function beforeValidate($options = array()) 
    {
        parent::beforeValidate($options);
        $this->data['User']['callable_phone'] = preg_replace(
            "/[^0-9]/", 
            '', 
            str_replace('+', '00', $this->data['User']['phone'])
        );
        return true;
    }
    
    /**
    * The beforeSave callback method
    * 
    * This method is called before $this->data is saved but after
    * the fields are validated
    * 
    * @param array $options query options
    * 
    * @return true
    */
    public function beforeSave($options = array()) 
    {
        parent::beforeSave($options);
        if (isset($this->data['User']['password']) 
            && !empty($this->data['User']['password'])
        ) {
            $this->data['User']['password'] 
                = sha1($this->data['User']['password']);
        } elseif (isset($this->data['User']['new-password']) 
            && !empty($this->data['User']['new-password'])
        ) {
            $this->data['User']['password'] 
                = sha1($this->data['User']['new-password']);
        } else {
            unset($this->data['User']['password']);
        }
        if (isset($this->data['User']['add_user'])) {
            $u = $this->data['User'];
            $this->data['User']['hash'] = sha1($u['email'] . $u['password']);
            $this->data['User']['is_phone_confirmed'] = true;
            $this->data['User']['is_email_confirmed'] = true;
            $this->data['User']['is_active'] = true;
        }

        if (isset($this->data['User']['user_type_id'])) {
            $this->new = $this->data['User']['user_type_id'];
            $this->contain(['UserType.name']);
            $this->old = $this->findById(
                $this->data['User']['id'], [
                    'user_type_id',
                ]
            );

            if ($this->new != $this->old['User']['user_type_id']) {
                $userType = $this->UserType->find(
                    'first', [
                        'conditions' => [
                            'UserType.id' => $this->new
                        ],
                        'fields' => [
                            'id',
                            'delivery_method_id',
                            'delivery_nr',
                            'name'
                        ]
                    ]
                );

                if (isset($this->data['User']['id'])
                    && !empty($this->data['User']['id'])
                ) {
                    $notification = [];
                    $Notification = ClassRegistry::init('Cms.Notification');
                    $notification['user_id']    = $this->data['User']['id'];
                    $notification['created_at'] = date('Y-m-d H:i:s');
                    $notification['is_deleted'] = false;
                    $notification['is_viewed']  = false;
                    $notification['subject']
                        = 'A fost schimbat pragul de reseller.';

                    if (!empty($this->old['UserType']['id'])
                        && !empty($userType['UserType']['id'])
                    ) {
                        $message = 'Pragul de reseller a fost schimbat din '
                            . $this->old['UserType']['name']
                            . ' in '
                            . $userType['UserType']['name'];
                    } elseif (empty($this->old['UserType']['id'])) {
                        $message = 'Felicitari! Ati devenit reseller! '
                            . 'Sunteti utilizator de tip '
                            . $userType['UserType']['name'];
                    } else {
                        $message = 'Ne pare rau dar nu ati atins pragul '
                            . 'minim pentru a fi reseller.';
                    }

                    $notification['message']    = $message;

                    $Notification->create();
                    $Notification->save($notification);
                }

                $transportsNr = (empty($userType))
                    ? 0 : $userType['UserType']['delivery_nr'];

                $userDelivery = ClassRegistry::init('Customers.UserDelivery');
                $userDelivery->deleteAll(
                    [
                    'UserDelivery.user_id' => $this->data['User']['id'],
                    'UserDelivery.is_used' => 0
                    ]
                );

                if ($transportsNr) {
                    for ($i = 1; $i <= $transportsNr; $i++) {
                        $save['user_id']   = $this->data['User']['id'];
                        $save['delivery_method_id']
                            = $userType['UserType']['delivery_method_id'];
                        $save['created_at'] = date('Y-m-d H:i:s');
                        $save['admin_id']   = 1;
                        $userDelivery->create();
                        if (!$userDelivery->save($save)) {
                            return false;
                        }
                    }

                    $notification = [];

                    $Notification = ClassRegistry::init('Cms.Notification');
                    $notification['user_id']    = $this->data['User']['id'];
                    $notification['created_at'] = date('Y-m-d H:i:s');
                    $notification['is_deleted'] = false;
                    $notification['is_viewed']  = false;
                    $notification['message']    = 'Contul dumneavoastra a '
                        . 'fost creditat cu ' . $transportsNr
                        . ' transport(uri) gratuit(e)';
                    $notification['subject']    = $notification['message'];

                    $Notification->create();
                    $Notification->save($notification);
                }
            }
        }

        return true;
    }
    
    /**
    * The _checkRepassword validation method
    * 
    * This method is called upon validation in order to check
    * if the password matches the re-typed password
    * 
    * @param array $check the field to be validated
    * 
    * @return bool
    */
    public function checkRepassword($check) 
    {
        if (current($check) === $this->data['User']['re-password']) {
            unset($this->data['User']['re-password']);
            return true;
        }
        return false;
    }
    
    /**
    * The requestLogin method
    * 
    * @param int $id the user id to request login
    * 
    * @return boolean
    */
    public function requestLogin($id) 
    {
        if (!$this->exists($id)) {
            return false;
        }
        $this->id = $id;
        return $this->saveField('request_login', 1);
    }

    /**
     * The search method
     * 
     * @param string $term the string to search for
     * 
     * @return array
     */
    public function search($term)
    {
        $data = $this->find(
            'all', array(
                'conditions' => array('User.email LIKE' => "%{$term}%"),
                'fields' => array(
                    'User.id',
                    'User.email',
                 ),
                'limit' => 20
            )
        );
        return $data;
    }
}
