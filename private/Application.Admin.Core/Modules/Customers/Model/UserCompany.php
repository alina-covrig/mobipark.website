<?php
/**
 * UserCompany.php
 *
 * Inside this Model any module-wide
 * data manipulation methods will be placed
 *
 * PHP version 5.4
 *
 * @category  Frontend
 * @package   Generic
 * @author    Daniel Ionascu <daniel.ionascu88@gmail.com>
 * @copyright 2014 Evercloud
 * @license   http://www.php.net/license/3_01.txt  PHP License 3.01
 * @version   SVN: $Id$
 * @link      /modules/Customers/models/user_address
 * @since     1.0
 */

/**
 * UserCompany.php
 *
 * Inside this Model any module-wide
 * data manipulation methods will be placed
 *
 * PHP version 5.4
 *
 * @category  Frontend
 * @package   Generic
 * @author    Daniel Ionascu <daniel.ionascu88@gmail.com>
 * @copyright 2014 Evercloud
 * @license   http://www.php.net/license/3_01.txt  PHP License 3.01
 * @version   Release: 1.0
 * @link      /users_address/
 * @since     1.0
 */
class UserCompany extends AppModel
{
    /**
     * The table prefix used by the User model
     *
     * @var
     */
    public $tablePrefix = 'customers__';

    /**
     * Display field
     *
     * @var string
     */
    public $displayField = 'name';

    /**
     * BelongsTo model association
     *
     * @var
     */
    public $belongsTo = array('User');

    /**
     * Validation rules
     *
     * @var array
     */
    public $validate = array(
        'name' => array(
            'name' => array(
                'rule'    => array('minLength', '5'),
                'message' => 'Numele trebuie sa contina minim 5 caractere'
            ),
            'max' => array(
                'rule'    => array('maxLength', '80'),
                'message' => 'Numele trebuie sa contina
                    maxim 80 de caractere'
            ),
        ),
        'bank_account' => array(
            'rgx' => array(
                'rule'    => REGEX_IBAN,
                'message' => 'Cont bancar invalid',
                'allowEmpty' => true,
            ),
            'max' => array(
                'rule'    => array('maxLength', '34'),
                'message' => 'Maxim 34 de caractere',
                'allowEmpty' => true
            ),

        ),
        'registered' => array(
            'max' => array(
                'rule' => array('maxLength', '20'),
                'message' => 'Maxim 20 de caractere',
            )
        ),
        'address_line_1' => array(
            'name' => array(
                'rule'    => array('minLength', '3'),
                'message' => 'Adresa trebuie sa contina
                    minim 3 caractere'
            ),
            'max' => array(
                'rule'    => array('maxLength', '50'),
                'message' => 'Adresa trebuie sa contina
                    maxim 50 de caractere'
            ),
        ),
        'city' => array(
            'required' => array(
                'rule'    => 'notBlank',
                'message' => 'Completati localitatea'
            ),
            'max' => array(
                'rule'    => array('maxLength', '50'),
                'message' => 'Localitatea trebuie sa contina
                    maxim 50 de caractere',
            ),
        ),
        'country' => array(
            'required' => array(
                'rule'    => 'notBlank',
                'message' => 'Completati tara'
            ),
            'max' => array(
                'rule'    => array('maxLength', '50'),
                'message' => 'Tara trebuie sa contina
                    maxim 50 de caractere'
            ),
        ),
        'zip' => array(
            'size' => array(
                'rule'    => array('between', 4, 10),
                'message' => 'Cod postal invalid',
                'allowEmpty' => true,
            ),
            'max' => array(
                'rule'    => array('maxLength', '20'),
                'message' => 'Codul postal trebuie sa contina
                    maxim 20 de caractere'
            ),
        ),
    );

    /**
     * The checkCui method
     *
     * @param integer $check The value to be checked
     *
     * @return bool
     */
    public function checkCui($check)
    {
        if (strtolower(trim($this->data['UserCompany']['VAT'])) == 'ro' 
            || strtolower(trim($this->data['UserCompany']['VAT'])) == ''
        ) {
            return (checkCIF($check['VAT_number']));
        }
        return true;
    }

    public function checkRegisterNo()
    {
        if (strtolower(trim($this->data['UserCompany']['VAT'])) == 'ro' 
            || strtolower(trim($this->data['UserCompany']['VAT'])) == ''
        ) {
            $this->validator()->add(
                'registered', 'rgx',
                array(
                    'rule'    => REGEX_REGISTRATION_ROMANIA,
                    'message' => 'Cod invalid (ex: j13/1644/2001)',
                    'allowEmpty' => true,
                )
            );
        }
        return true;
    }

    /**
     * The checkBankNo method
     *
     * This method validates bank acc
     *
     * @return nothing :))
     */
    public function checkBankNo()
    {
        $this->validator()->add(
            'bank_account', 'rgx',
            array(
                'rule'    => REGEX_IBAN,
                'message' => 'Cont bancar invalid',
                'allowEmpty' => true,
            )
        );
    }

    /**
     * The search method
     *
     * This method searches through companies
     *
     * @param string $term The term to search against
     *
     * @return array
     */
    public function search($term)
    {
        $term_wildcard = implode('.*', explode(" ", $term));
        $data = $this->find(
            'all', array(
                'conditions' => array(
                    'UserCompany.name REGEXP' => "{$term_wildcard}",
                ),
                'fields' => array(
                    'id',
                    'name',
                ),
                'limit' => 10
            )
        );
        return $data;
    }
}
