<?php
/**
* CustomersAppModel
*
* Inside this Model any module-wide
* data manipulation methods will be placed
*
* PHP version 5.4
*
* @category  Admin
* @package   Customers
* @author    Lucian Vasile <lucian.vasile@live.com>
* @copyright 2014 Evercloud
* @license   http://www.php.net/license/3_01.txt  PHP License 3.01
* @version   SVN: $Id$
* @link      /modules/Customers/models/customers-app-model
* @since     1.0
*/

App::uses('AppModel', 'Model');


/**
* CustomersAppModel Class
*
* Inside this Model any application-wide
* data manipulation methods will be placed
*
* @category  Admin
* @package   Customers
* @author    Lucian Vasile <lucian.vasile@live.com>
* @copyright 2014 Evercloud
* @license   http://www.php.net/license/3_01.txt  PHP License 3.01
* @version   Release: 1.0
* @link      /modules/Customers/models/customers-app-model
* @since     Class available since Release 1.0
*/
class CustomersAppModel extends AppModel
{
    // properties {{{
    /**
    * The table prefix used by the Customers module
    * 
    * @var 
    */
    public $tablePrefix = 'customers__';
    // }}}
}
