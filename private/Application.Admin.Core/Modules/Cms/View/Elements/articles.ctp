<?php /** @var array $article */ ?>

<td><?= $this->App->articleImage($article['MainImage']['image_path']) ?></td>
<td><h5 class="mt0 mb0"><?= $article['Article']['title']?></h5></td>
<td><?= date('d-m-Y', strtotime($article['Article']['publish_date'])) ?></td>
<td><?= $article['Category']['name']?></td>
<td>
    <?php if (!$article['Article']['is_published']) : ?>
        <a href="<?= Router::url(['action' => 'publish', $article['Article']['id'], 1]) ?>"
           class="btn btn-success btn-outline btn-xs"
           onclick="return confirm('Esti sigur(a) ca vrei sa publici articolul?');">publica</a>
    <?php else : ?>
        <a href="<?= Router::url(['action' => 'publish', $article['Article']['id'], 0]) ?>"
           class="btn btn-default btn-xs"
           onclick="return confirm('Esti sigur(a) ca vrei sa retragi articolul?');">retrage</a>
    <?php endif; ?>
</td>
<td style="text-align: right;">
	<div class="btn-group btn-group-xs">
		<a href="<?= Router::url(['controller' => 'article_contents', 'action' => 'index', $article['Article']['id']]) ?>"
		   class="btn btn-primary btn-xs btn-outline" data-toggle="tooltip" data-placement="top"
		   data-container="body" title="" data-original-title="Continut"
		><i class="fa fa-object-group"></i></a>

		<a href="<?= Router::url(['controller' => 'articles', 'action' => 'gallery', $article['Article']['id']]) ?>"
		   class="btn btn-info btn-xs btn-outline" data-toggle="tooltip" data-placement="top"
		   data-container="body" title="" data-original-title="Galerie imagini"
		><i class="fa fa-picture-o"></i></a>

		<a href="<?= Router::url(['controller' => 'articles', 'action' => 'delete', $article['Article']['id']]) ?>"
		   class="btn btn-danger btn-xs btn-outline" data-toggle="tooltip" data-placement="top"
		   data-container="body" title="" data-original-title="Sterge"
		   onclick="return confirm('Esti sigur(a) ca vrei sa stergi articolul?')"
		><i class="fa fa-trash-o"></i></a>

        <a href="<?= Router::url(['controller' => 'articles', 'action' => 'preview', $article['Article']['id']]) ?>"
           class="btn btn-info btn-xs btn-outline" data-toggle="tooltip" data-placement="top"
           data-container="body" title="" data-original-title="Preview articol"
        ><i class="fa fa-search"></i></a>

		<a href="<?= Router::url(['controller' => 'articles', 'action' => 'edit', $article['Article']['id']]) ?>"
		   class="btn btn-info btn-xs btn-outline" data-toggle="tooltip" data-placement="top"
		   data-container="body" title="" data-original-title="Modifica"
		><i class="fa fa-pencil"></i></a>
	</div>
</td>