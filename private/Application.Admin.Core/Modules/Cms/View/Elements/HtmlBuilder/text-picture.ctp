<div class="sim-row container" data-id="3" style="width: 90%">
	<div class="product-highlight-text reverse">
        <div class="wrapper-right">
            <ul class="product-highlight-text-right sim-row-edit" data-type="text">
                Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi ligula
                est, commodo eget fermentum quis, iaculis id nulla. Pellentesque facilisis,
                tortor et mattis lobortis, urna ex bibendum ex, vel scelerisque purus
                lacus sed nisl. Donec et varius magna, ornare molestie lectus.
                Morbi condimentum ex et quam gravida tincidunt. Nam a ullamcorper
                nisi. Aenean accumsan ex ut metus finibus, id finibus urna commodo.
                Sed sed neque in nulla elementum pulvinar. Sed venenatis velit vitae
                dolor facilisis, vitae faucibus diam pharetra. Ut at libero vitae
                magna pretium porta.
            </ul>
        </div>
		<div class="sim-row-edit wrapper-left" data-type="image">
			<img class="product-img-left offset-image lazy" src="<?=Configure::read('WEBSITE_URL')  . Configure::read('IMAGES_PATH_ARTICLE_CONTENTS')  ?>585x400.png">
		</div>
	</div>
</div>