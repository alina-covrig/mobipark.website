
<style>
    .show-tool:hover {
        color: #c00;
        text-decoration: none;
    }
    .show-tool:hover:after {
        background: #111;
        background: rgba(0,0,0,.8);
        border-radius: .5em;
        bottom: 1.35em;
        color: #fff;
        content: attr(title);
        display: block;
        left: 1em;
        padding: .3em 1em;
        position: absolute;
        text-shadow: 0 1px 0 #000;
        white-space: nowrap;
        z-index: 98;
    }
    .show-tool:hover:before {
        border: solid;
        border-color: #111 transparent;
        border-color: rgba(0,0,0,.8) transparent;
        border-width: .4em .4em 0 .4em;
        bottom: 1em;
        content: "";
        display: block;
        left: 2em;
        position: absolute;
        z-index: 99;
    }
</style>

<div class="sim-row"  data-id="11" data-target="selectNav">
    <div class="h-hero-banner container">
        <div>
            <a class="sim-row-edit show-tool" data-type="image-link"  title="Editeaza Banner Desktop (1170x500)" href="">
                <img src="<?= Configure::read('IMG_PATH') . 'highlights/active2/banner1.png' ?>" class="banner-sm lazy" style="width:100%;">
            </a>
        </div>
        <div class="mobile-banner-landing">
            <a class="sim-row-edit show-tool" data-type="image-link" title="Editeaza Banner Mobil (480x250)" href="">
                <img src="<?= Configure::read('IMG_PATH') . 'highlights/active2/banner2.png' ?>" class="banner-xs lazy">
            </a>
        </div>
    </div>
</div>

