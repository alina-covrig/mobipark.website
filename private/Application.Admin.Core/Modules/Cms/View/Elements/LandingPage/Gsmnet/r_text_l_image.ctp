<div class="sim-row container" data-id="19">
<h2 class="fluff-header overlap sim-row-edit" data-type="text">Autonomie pentru întreaga zi</h2>
<div class="highlight-text last-text">
    <div class="sim-row-edit" data-type="image">
        <img class="highlight-img-left offset-image lazy" src="https://www.gsmnet.ro/img/highlights/active2/promo3.jpg">
    </div>
    <div class="highlight-text-right sim-row-edit" data-type="text">
        Galaxy Watch Active2 este dotat cu baterie 340 mA ce promite performanțe de excepție.
        Odată întors acasă, este suficient să îl așezi pe încărcătorul wireless magnetic pentru a reîncărca
        bateria fără conectori sau cabluri. Galaxy Watch Active2 este rezistent la apă și praf conform
        standardelor IP68 / 5 ATM, la care se adaugă o rezistență la șocuri de clasă militară,
        ce permite utilizarea sa în orice conditii.
    </div>
</div>
</div>