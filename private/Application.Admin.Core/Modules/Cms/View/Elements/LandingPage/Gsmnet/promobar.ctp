
<div class="promo-bar sim-row" data-id="1">
    <div class="promo-bar-container container">
        <div class="promo-logo sim-row-content3-center-tab-image sim-row-edit" data-type="image">
            <img src="<?= Configure::read('IMG_PATH') . 'highlights/active2/samsung.png' ?>">
        </div>
        <div class="promo-sep"></div>
        <div class="promo-name sim-row-edit" data-type="text">
            Galaxy Watch Active 2
        </div>
    </div>
</div>