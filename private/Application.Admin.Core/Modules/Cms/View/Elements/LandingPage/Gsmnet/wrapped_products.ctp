<style>
    .show-tool:hover {
        color: #c00;
        text-decoration: none;
    }
    .show-tool:hover:after {
        background: #111;
        background: rgba(0,0,0,.8);
        border-radius: .5em;
        bottom: 1.35em;
        color: #fff;
        content: attr(title);
        display: block;
        left: 1em;
        padding: .3em 1em;
        position: absolute;
        text-shadow: 0 1px 0 #000;
        white-space: nowrap;
        z-index: 98;
    }
    .show-tool:hover:before {
        border: solid;
        border-color: #111 transparent;
        border-color: rgba(0,0,0,.8) transparent;
        border-width: .4em .4em 0 .4em;
        bottom: 1em;
        content: "";
        display: block;
        left: 2em;
        position: absolute;
        z-index: 99;
    }
</style>
<div class="sim-row container" data-id="14">
    <div class="h-products-container">
        <div class="h-product-container show-tool category-placeholder sim-row-edit" style="position:relative;" data-type="text" title="Bloc pentru mobil">
            <span>40 mm<br>Stainless Steel</span>
        </div>
        <div style="vertical-align:top;  position:relative;" class="add_product h-product-container" align="center">
            <a class="sim-row-content3-center-tab-image sim-row-edit"  data-type="image-link" href="#">
                <div class="gray-backdrop gray1"></div>
                <span class="h-product-color sim-row-edit" data-type="text" style="position:relative;font-weight: bold;">Black</span>
                <img src="<?=Configure::read("STORE_URL")?>theme/default/assets/images/newsletters/object1.png"
                     class="h-product-image lazy"/>
                <span class="preorder-link">Comanda</span>
            </a>
        </div>
        <div style="vertical-align:top;  position:relative; " class="add_product h-product-container" align="center">
            <a class="sim-row-content3-center-tab-image sim-row-edit"  data-type="image-link" href="#">
                <div class="gray-backdrop gray2"></div>
                <span class="h-product-color sim-row-edit" data-type="text" style="position:relative;font-weight: bold;">Silver</span>
                <img src="<?=Configure::read("STORE_URL")?>theme/default/assets/images/newsletters/object2.png"
                     class="h-product-image lazy"/>
                <span class="preorder-link">Comanda</span>
            </a>
        </div>
        <div style="vertical-align:top;  position:relative; " class="add_product h-product-container" align="center">
            <a class="sim-row-content3-center-tab-image sim-row-edit"  data-type="image-link" href="#">
                <div class="gray-backdrop gray3"></div>
                <span class="h-product-color sim-row-edit" data-type="text" style="position:relative;font-weight: bold;">Gold</span>
                <img src="<?=Configure::read("STORE_URL")?>theme/default/assets/images/newsletters/object3.png"
                     class="h-product-image lazy"/>
                <span class="preorder-link">Comanda</span>
            </a>
        </div>
    </div>
</div>
