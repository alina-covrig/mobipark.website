
<div class="sim-row container" data-id="18">
<h2 class="fluff-header sim-row-edit" data-type="text">Combină și Potrivește #withGalaxy</h2>
    <div class="highlight-text">
        <div class="highlight-text-left sim-row-edit" data-type="text">
            Exprimă-ți personalitatea și starea de spirit cu temele asortate culorii și materialului
            curelei. Poți alege dintr-o gama largă de curele de 20mm în diferite nuanțe și texturi.
            Când cumperi o curea nouă și scanezi codul QR de pe pachet, vei primi automat un link
            către Galaxy Store, pentru a descărca tema. Fă-ți un selfie cu ținuta pe care o porți
            în fiecare zi și Galaxy Watch Active2 îți va afișa o temă asortata care te va scoate
            din tipare.
        </div>
        <div class="sim-row-edit" data-type="image">
        <img class="highlight-img-right lazy" src="https://www.gsmnet.ro/img/highlights/active2/promo2.jpg">
        </div>
    </div>
</div>