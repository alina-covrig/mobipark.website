<div class="selection-menu-placeholder sim-row" data-id="23">
    <ul class="selection-menu nav sim-row-edit_add" role="tablist" id="selectNav" data-type="selection-menu">
        <li class="active cell">
            <a class="menu-option sim-row-edit_custom" href="#40mm-stainless-steel" data-type="link">
                40mm<br>Stainless Steel
            </a>
        </li>
        <li class="cell">
            <a class="menu-option sim-row-edit_custom" href="#40mm-aluminium" data-type="link">
                40mm<br>Aluminium
            </a>
        </li>
        <li class="cell">
            <a class="menu-option sim-row-edit_custom" href="#44mm-stainless-steel" data-type="link">
                44mm<br>Stainless Steel
            </a>
        </li>
        <li class="cell">
            <a class="menu-option sim-row-edit_custom" href="#44mm-aluminium" data-type="link">
                44mm<br>Aluminium
            </a>
        </li>
    </ul>
</div>