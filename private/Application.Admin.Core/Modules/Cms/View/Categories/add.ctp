<?php /** @var array $parent_categories */ ?>

<?=$this->start('page-title');?>
    <strong><?php echo __("Adauga categorie")?></strong>
<?=$this->end()?>

<div class="row" style="padding-top: 20px;">
    <div class="col-lg-3">
        <?= $this->H->bslk(
            Router::url(['action' => 'index']),
            'btn btn-warning btn-block',
            $this->Html->icon('fa fa-arrow-left mr5') . __(' Inapoi la categorii'),
            __("Inapoi la lista de categorii")
        )?>
    </div>
</div>

<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header"><?php echo __("Adauga categorie noua")?></h1>
    </div>
</div>

<div class="row">
    <div class="col-lg-12">
        <?= $this->Form->create('Category', ['autocomplete' => 'off']); ?>
        <div class="row">
            <div class="col-md-6">
                <?= $this->Form->input('parent_id', ['label' => __('Categorie parinte'), 'empty' => __('Categorie radacina'), 'required' => false]); ?>
            </div>
            <div class="col-md-6">
                <?= $this->Form->input('name', ['label' => __('Denumire')]); ?>
            </div>
        </div>
        <?= $this->Form->submit('Adauga', ['class' => 'btn btn-success']); ?>
        <?= $this->Form->end(); ?>
    </div>
</div>
