<?php /** @var array $categories */ ?>

<?=$this->start('page-title');?>
<strong><?php echo __("Lista categorii")?></strong>
<?=$this->end()?>

<div class="row" style="padding-top: 20px;">
    <div class="col-lg-3">
        <?= $this->H->bslk(
            Router::url(['action' => 'add']),
            'btn btn-warning btn-block',
            $this->Html->icon('fa fa-plus') . __(' Adauga categorie'),
            __("Adauga categorie noua")
        )?>
        <div class="mg20"></div>
    </div>
</div>

<div class="row">
    <div class="col-md-12 table-responsive">
        <table class="table table-condensed table-striped small tree">
            <thead>
            <tr>
                <th style="width: 100px;"><?=__('ID')?></th>
                <th><?=__('Denumire')?></th>
                <th style="width: 120px; text-align: right;"><?=__('Actiuni')?></th>
            </tr>
            </thead>
            <tbody>
            <?php foreach ($categories as $c) : ?>
            <tr class="treegrid-<?php echo $c['Category']['id']?> <?php echo ($c['Category']['parent_id']) ? 'treegrid-parent-' . $c['Category']['parent_id'] : ''?>">
                <td><?= $c['Category']['id']?></td>
                <td><?= $c['Category']['name']?></td>
                <td style="text-align: right;">
                    <div class="btn-group btn-group-xs">
                        <a href="<?= Router::url(['action' => 'moveUp', $c['Category']['id']]) ?>"
                           class="btn btn-warning btn-xs btn-outline" data-toggle="tooltip" data-placement="top"
                           data-container="body" title="" data-original-title="Muta in sus"
                        ><i class="fa fa-arrow-up"></i></a>
                        <a href="<?= Router::url(['action' => 'moveDown', $c['Category']['id']]) ?>"
                           class="btn btn-warning btn-xs btn-outline" data-toggle="tooltip" data-placement="top"
                           data-container="body" title="" data-original-title="Muta in jos"
                        ><i class="fa fa-arrow-down"></i></a>
                        <a href="<?= Router::url(['action' => 'delete', $c['Category']['id']]) ?>"
                           class="btn btn-danger btn-xs btn-outline" data-toggle="tooltip" data-placement="top"
                           data-container="body" title="" data-original-title="Sterge"
                           onclick="return confirm('Esti sigur(a) ca vrei sa stergi categoria?')"
                        ><i class="fa fa-trash-o"></i></a>
                        <a href="<?= Router::url(['action' => 'edit', $c['Category']['id']]) ?>"
                           class="btn btn-info btn-xs btn-outline" data-toggle="tooltip" data-placement="top"
                           data-container="body" title="" data-original-title="Modifica"
                        ><i class="fa fa-pencil"></i></a>
                    </div>
                </td>
            </tr>
            <?php endforeach; ?>
            </tbody>
        </table>
    </div>
</div>

<?php echo $this->element('Plugins/treejs')?>
<?php $this->start('extra-scripts')?>
<script type="text/javascript">
    $('.tree').treegrid({
        saveState: true
    });
</script>
<?php $this->end()?>
