<?=$this->start('page-title');?>
    <strong><?php echo __("Descrieri website Mobipark")?></strong>
<?=$this->end()?>

<div class="row">
    <div class="col-lg-12">
        <table class="table tree table-bordered table-striped table-condensed">
            <thead>
            <tr>
                <th><?php echo __("id")?></th>
                <th><?php echo __("Pagina cu descrieri editabile")?></th>
            </tr>
            </thead>
            <tbody>
            <?php foreach($descriptions as $description) :?>
                <tr>
                    <td><?php echo $description['Description']['id']?></td>
                    <td><?php echo $description['Description']['editable_page']?></td>
                    <td align="right" width="145">
                        <div class="btn-group btn-group-xs">
                            <?= $this->H->bslk(
                                "/cms/descriptions/edit/" . $description['Description']['id'],
                                'btn btn-info btn-xs btn-outline',
                                $this->Html->icon('fa fa-pencil'),
                                __("Modifica")
                            )?>
                        </div>
                    </td>
                </tr>
            <?php endforeach;?>
            </tbody>
        </table>
    </div>
</div>
