<?= $this->element('Plugins/summernote')?>
<?= $this->Html->script('/admin/js/plugins/summernote/summernote.min.js', array('inline' => false));?>
<?= $this->Html->css('/admin/js/plugins/summernote/summernote.css', array('inline' => false));?>
<?= $this->start('extra-scripts')?>
<script type="text/javascript">
    $(document).ready(function() {

    <?php $summernote_aj_url='/catalog/products/editorUploadsJson.json';?>
    $('#note_1').summernote({
        'maxHeight': 700,
        callbacks: {
            onImageUpload: function (files, editor, welEditable) {
                uploadImgSummernote(
                    files,
                    editor,
                    welEditable,
                    this,
                    '/catalog/products/editorUploadsJson.json'
                );
            }
        }
    });

    $('#note_2').summernote({
        'height': 100,
        'toolbar': [  ['style', ['bold', 'italic', 'underline', 'clear']]]
    });

    $('#note_3').summernote({
            callbacks: {
                onImageUpload: function (files, editor, welEditable) {
                    uploadImgSummernote(
                        files,
                        editor,
                        welEditable,
                        this,
                        '/catalog/products/editorUploadsJson.json'
                    );
                }
            }
        });
        $('#note_4').summernote({
            'height': 100,
            'toolbar': [  ['style', ['bold', 'italic', 'underline', 'clear']]]
        });
        $('#note_5').summernote({
            callbacks: {
                onImageUpload: function (files, editor, welEditable) {
                    uploadImgSummernote(
                        files,
                        editor,
                        welEditable,
                        this,
                        '/catalog/products/editorUploadsJson.json'
                    );
                }
            }
        });
        $('#note_6').summernote({
            callbacks: {
                onImageUpload: function (files, editor, welEditable) {
                    uploadImgSummernote(
                        files,
                        editor,
                        welEditable,
                        this,
                        '/catalog/products/editorUploadsJson.json'
                    );
                }
            }
        });
});
</script>
<?php $this->end()?>


<div class="row">
    <div class="col-lg-12">
        <h2 class="page-header"><?php echo __("Modificare descrieri")?></h2>
    </div>
    <!-- /.col-lg-12 -->
</div>
<div class="row">
    <div class="col-lg-3">
        <?= $this->H->bslk(
            "/cms/descriptions",
            'btn btn-warning btn-block',
            $this->Html->icon('fa fa-arrow-left') . __(' Pagina precedenta'),
            __("Pagina precedenta")
        )?>
        <div class="mg20"></div>
    </div>
</div>
<div>
    <?php echo $this->Form->create('Description');?>
    <?php echo $this->Form->input('id')?>
    <div class="row">
        <div class="col-md-6">
            <label for="note_1"><?=__('Dor de verde ca parte din viata')?></label>
            <?php echo $this->Form->input('Description.description1',
                [
                        'id' => 'note_1',
                        'value' => $description['Description']['description1']
                ]);?>
        </div>
        <div class="col-md-6">
            <label for="note_2"><?=__('Descriere scurta (zona gri)')?></label>
            <?php echo $this->Form->input('description2',
                [
                    'id' => 'note_2',
                    'value' => $description['Description']['description2']
                ]);?>
        </div>
    </div>
    <div class="row">
        <div class="col-md-6">
            <label for="note_2"><?=__('Principiile folosite in dezvoltarea casei tale)')?></label>
            <?php echo $this->Form->input('description3',
                [
                    'id' => 'note_3',
                    'value' => $description['Description']['description3']
                ]);?>
        </div>
        <div class="col-md-6">
            <label for="note_4"><?=__('Descriere constructiva la nivel de bloc')?></label>
            <?php echo $this->Form->input('description4',
                [
                    'id' => 'note_4',
                    'value' => $description['Description']['description4']
                ]);?>
        </div>
    </div>
    <div class="row">
        <div class="col-md-6">
            <label for="note_5"><?=__('Fatada ventilata')?></label>
            <?php echo $this->Form->input('description5',
                [
                    'id' => 'note_5',
                    'value' => $description['Description']['description5']
                ])?>
        </div>
        <div class="col-md-6">
            <label for="note_6"><?=__('Izolatie termica - materiale')?></label>
            <?php echo $this->Form->input('description6',
                [
                    'id' => 'note_6',
                    'value' => $description['Description']['description6']
                ])?>
        </div>
    </div>


    <?php echo $this->Form->submit(__('Modifica descrieri'), array('class' => "btn btn-success btn-lg btn-block"));?>


    <?php echo $this->Form->end();?>

</div>