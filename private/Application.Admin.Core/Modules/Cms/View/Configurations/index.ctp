<?php echo $this->element('Plugins/inline-edit')?>
<?=$this->start('extra-scripts')?>
<script type="text/javascript" >
    $('.configuration-value').editable();
</script>
<?=$this->end()?>
<?php $this->start('page-title')?>
<strong><?php echo __("Lista optiuni configurare")?></strong>
<?php $this->end()?>
<div class="row">
<div class="col-md-12">
        <div class="table-responsive">
            <table id="productReviewsList" class="table table-striped table-hover table-condensed small">
                <thead>
                    <tr>
                        <th>ID</th>
                        <th><?php echo __('Denumire'); ?></th>
                        <th><?php echo __("Valoare")?></th>
                        <th><?php echo __("Tip")?></th>
                    </tr>
                    <tr class="remove_form_group">
                        <?php //echo $this->element('Toolkit.filterForm')?>
                    </tr>
                </thead>
                <tbody>
                    <?php foreach($configurations as $c) :?>
                        <tr>
                            <td><?php echo $c['Configuration']['id']?></td>
                            <td>
                                <?php echo $c['Configuration']['name'];?>
                            </td>
                            <td>
                                <a
                                    href="#"
                                    class="configuration-value"
                                    data-type="text"
                                    data-pk="<?=$c['Configuration']['id']?>"
                                    data-name="Configuration[value]"
                                    data-url="/cms/configurations/updateValue/<?=$c['Configuration']['id']?>.json"
                                    data-action="/cms/configurations/updateValue/<?=$c['Configuration']['id']?>.json"
                                    data-original-title="<?=__('Introdu valoarea pentru \'').$c['Configuration']['name'].'\'';?>"
                                    ><?php echo $c['Configuration']['value'];?>
                                </a>
                            </td>
                            <td>
                                <?php echo $c['Configuration']['type'];?>
                            </td>
                        </tr>
                    <?php endforeach;?>
                </tbody>
            </table>
            <?php echo $this->element('Navigation/pagination'); ?>
        </div>
    </div>
</div>
<!-- /.row -->