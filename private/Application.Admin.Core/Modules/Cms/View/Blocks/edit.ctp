<?php $this->Html->script('/admin/js/plugins/summernote/summernote.min.js', array('inline' => false));?>
<?php $this->Html->css('/admin/js/plugins/summernote/summernote.css', array('inline' => false));?>
<script type="text/javascript">
    replace_txt = '#BlockContent';
</script>
<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header"><?php echo __("Modificare block")?></h1>
    </div>
    <!-- /.col-lg-12 -->
</div>
<!-- /.row -->
<div class="row">
    <div class="col-lg-6">
        <?= $this->H->bslk(
            "/cms/blocks",
            'btn btn-warning btn-block',
            $this->Html->icon('fa fa-arrow-left') . __(' Inapoi la block-uri'),
            __("Inapoi la block-uri")
        )?>
        <div class="mg20"></div>
    </div>
    <div class="col-lg-6">
        <?= $this->H->bslk(
            "/cms/blocks/delete/" . $block['Block']['id'],
            'btn btn-danger btn-block',
            $this->Html->icon('fa fa-trash-o') . ' ' . __('Sterge block'),
            __("Sterge block"),
            'top',
            __('Esti sigur ca vrei sa stergi block-ul?')
        )?>
        <div class="mg20"></div>
    </div>
</div>
<!-- /.row -->
<div class="row">
    <div class="col-md-12">
        <?php echo $this->Form->create('Block');?>
            <?php echo $this->Form->input('id')?>
            <?php echo $this->Form->input('title', array('label' => __('Denumire')))?>
            <?php echo $this->Form->input('alias', array('label' => __('Alias apelare')))?>
            <?php echo $this->Form->input('meta_title', array('label' => __('Meta title')))?>
            <?php echo $this->Form->input('meta_description', array('label' => __('Meta descriere')))?>
            <?php echo $this->Form->input('meta_keywords', array('label' => __('Meta keywords')))?>
            <?php echo $this->Form->input('content', array('label' => __('Meta keywords')))?>
            <?php echo $this->Form->input('active', array('label' => __('Activ')))?>
            <?php echo $this->Form->submit(__('Modifica block'), array('class' => "btn btn-success btn-lg btn-block"));?>
        <?php echo $this->Form->end();?>
    </div>
<!-- /.row -->