<?=$this->start('page-title');?>
    <strong><?php echo __("Block-uri de text")?></strong>
<?=$this->end()?>
<!-- /.row -->
<div class="row">
    <div class="col-lg-6">
        <?= $this->H->bslk(
            "/cms/blocks/add",
            'btn btn-warning btn-block',
            $this->Html->icon('fa fa-plus') . __(' Adauga block nou'),
            __("Adauga pagina noua")
        )?>
        <div class="mg20"></div>
    </div>
    <!-- /.col-lg-6 -->
</div>
<!-- /.row -->
<div class="row">
    <div class="col-lg-12">
        <table class="table tree table-bordered table-striped table-condensed">
            <thead>
            <tr>
                <th><?php echo __("Nume")?></th>
                <th><?php echo __("Alias")?></th>
                <th><?php echo __("Optiuni")?></th>
            </tr>
            </thead>
            <tbody>
            <?php foreach($blocks as $block) :?>
                <tr>
                    <td><?php echo $block['Block']['title']?></td>
                    <td><?php echo $block['Block']['alias']?></td>
                    <td align="right" width="145">
                        <div class="btn-group btn-group-xs">
                            <?= $this->H->bslk(
                                "/cms/blocks/delete/" . $block['Block']['id'],
                                'btn btn-danger btn-xs btn-outline',
                                $this->Html->icon('fa fa-trash-o'),
                                __("Sterge"),
                                'top',
                                __("Esti sigur ca vrei sa stergi block-ul?")
                            )?>
                            <?= $this->H->bslk(
                                "/cms/blocks/edit/" . $block['Block']['id'],
                                'btn btn-info btn-xs btn-outline',
                                $this->Html->icon('fa fa-pencil'),
                                __("Modifica")
                            )?>
                        </div>
                    </td>
                </tr>
            <?php endforeach;?>
            </tbody>
        </table>
    </div>
</div>
<!--/.row-->