<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header"><?php echo __("Management Cache")?></h1>
    </div>
    <!-- /.col-lg-12 -->
</div>
<!-- /.row -->

<div class="row">
    <div class="col-lg-3">
        <?= $this->H->bslk(
            "/cms/cms_cache/delete",
            'btn btn-success btn-block',
            $this->Html->icon('fa fa-trash-o') . __(' Goleste tot cache-ul'),
            __("Goleste tot cache-ul")
        )?>
        <div class="mg20"></div>
    </div>

    <?php foreach($cacheNonEditable as $c): ?>
        <div class="col-lg-3">
            <?= $this->H->bslk(
                "/cms/cms_cache/delete/" . $c['CmsCache']['id'],
                'btn btn-success btn-block',
                $this->Html->icon('fa fa-trash-o') . __(' Goleste ') . $c['CmsCache']['name'],
                __(' Goleste ') . $c['CmsCache']['name']
            )?>
            <div class="mg20"></div>
        </div>
    <?php endforeach; ?>
</div>
<div class="alert alert-danger" role="alert">Dupa dezactivarea unui cache acesta trebuie sters.</div>

<div class="row">
    <div class="col-lg-12">
        <table class="table tree table-bordered table-striped table-condensed">
            <thead>
            <tr class="text-center">
                <th><?php echo __('ID') ?></th>
                <th><?php echo __("Nume")?></th>
                <th><?php echo __("Descriere")?></th>
                <th><?php echo __("Activ?")?></th>
                <th><?php echo __("Actiuni")?></th>
            </tr>
            </thead>
            <tbody>
            <?php foreach($cacheEditable as $c) :?>
                <tr class="text-center">
                    <td><?=$c['CmsCache']['id']?></td>
                    <td class="text-left"><?=$c['CmsCache']['name']?></td>
                    <td class="text-left"><?=$c['CmsCache']['description']?></td>
                    <td>
                        <div class="onoffswitch">
                            <input type="checkbox" name="onoffswitch" class="onoffswitch-checkbox" id="myonoffswitch-<?=$c['CmsCache']['id']?>" <?=$c['CmsCache']['is_active'] ? 'checked' : '';?> data-id="<?=$c['CmsCache']['id']?>">
                            <label class="onoffswitch-label" for="myonoffswitch-<?=$c['CmsCache']['id']?>"></label>
                        </div>

                    </td>
                    <td align="right" width="50">
                        <div class="btn-group btn-group-xs">
                            <a href="/cms/cms_cache/delete/<?=$c['CmsCache']['id']?>" class="btn btn-danger btn-xs btn-outline" data-toggle="tooltip" data-placement="top" data-container="body" title="Sterge" data-original-title="Sterge" onclick="return confirm('Esti sigur?')"><i class="fa fa fa-trash-o"></i></a>
                        </div>
                    </td>
                </tr>
            <?php endforeach;?>
            </tbody>
        </table>
    </div>
</div>

<div id="dialog" title="Asteptati...">
    <div id="ajax-loader"></div>
    <p id="ajax-message"></p>
</div>

<?=$this->start('extra-scripts')?>
<script type="text/javascript" >
    $(document).ready(function(){
        $(document).on('click', '.onoffswitch-checkbox',  function(){
            var box = $(this);
            var cache_id = $(this).data('id');
            var checked = $(this).prop('checked');
            $.ajax({
                'url': '/cms/cms_cache/changeStatus',
                'type': 'post',
                'dataType': 'json',
                'data': {
                    'id': cache_id,
                    'checked': checked
                },
                beforeSend: function() {
                    $( "#dialog" ).dialog({
                        resizable: false,
                        height:120,
                        modal: true,
                    });
                    $( "#ajax-loader" ).show();
                },
                success: function(r) {
                    $( "#dialog" ).dialog( "close" );
                }
            })
        })
    });

</script>
<?=$this->end()?>