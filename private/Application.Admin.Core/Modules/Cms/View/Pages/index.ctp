<?php echo $this->element('Plugins/treejs')?>
<?php $this->start('extra-scripts')?>
    <script type="text/javascript">
        $('.tree').treegrid({
            saveState: true
        });
    </script>
<?php $this->end()?>
<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header"><?php echo __("Pagini statice")?></h1>
    </div>
    <!-- /.col-lg-12 -->
</div>
<!-- /.row -->
<div class="row">
    <div class="col-lg-6">
        <?= $this->H->bslk(
            "/cms/pages/add",
            'btn btn-warning btn-block',
            $this->Html->icon('fa fa-plus') . __(' Adauga pagina noua'),
            __("Adauga pagina noua")
        )?>
        <div class="mg20"></div>
    </div>
    <!-- /.col-lg-6 -->
</div>
<!-- /.row -->
<div class="row">
    <div class="col-lg-12">
        <table class="table tree table-bordered table-striped table-condensed">
            <thead>
                <tr>
                    <th><?php echo __("Nume")?></th>
                    <th><?php echo __("Full Path")?></th>
                    <th><?php echo __("Alias")?></th>
                    <th><?php echo __("Continut")?></th>
                    <th><?php echo __("Activ")?></th>
                    <th><?php echo __("Optiuni")?></th>
                </tr>
            </thead>
            <tbody>
                <?php foreach($pages as $page) :?>
                    <tr class="treegrid-<?php echo $page['Page']['id']?> <?php echo ($page['Page']['parent_id']) ? 'treegrid-parent-' . $page['Page']['parent_id'] : ''?>">
                        <td><?php echo $page['Page']['title']?></td>
                        <td><?php echo $page['Page']['full_path']?></td>
                        <td><?php echo $page['Page']['alias']?></td>
                        <th><?php echo (trim(strip_tags($page['Page']['content'])) != '') ? '<i class="fa fa-check"></i>' : ''?></th>
                        <td><?php echo $page['Page']['active']?></td>
                        <td align="right" width="145">
                            <div class="btn-group btn-group-xs">
                                <?= $this->H->bslk(
                                    "/cms/pages/moveup/" . $page['Page']['id'],
                                    'btn btn-warning btn-xs btn-outline',
                                    $this->Html->icon('fa fa-arrow-up'),
                                    __("Muta pagina mai sus")
                                )?>
                                <?= $this->H->bslk(
                                    "/cms/pages/movedown/" . $page['Page']['id'],
                                    'btn btn-warning btn-xs btn-outline',
                                    $this->Html->icon('fa fa-arrow-down'),
                                    __("Muta pagina mai jos")
                                )?>
                                <?= $this->H->bslk(
                                    "/cms/pages/delete/" . $page['Page']['id'],
                                    'btn btn-danger btn-xs btn-outline',
                                    $this->Html->icon('fa fa-trash-o'),
                                    __("Sterge"),
                                    'top',
                                    __("Esti sigur ca vrei sa stergi pagina?")
                                )?>
                                <?= $this->H->bslk(
                                    "/cms/pages/edit/" . $page['Page']['id'],
                                    'btn btn-info btn-xs btn-outline',
                                    $this->Html->icon('fa fa-pencil'),
                                    __("Modifica")
                                )?>
                            </div>
                        </td>
                    </tr>
                <?php endforeach;?>
            </tbody>
        </table>
    </div>
</div>
<!--/.row-->