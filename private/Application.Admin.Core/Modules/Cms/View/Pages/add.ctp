<?php $this->Html->script('/admin/js/plugins/summernote/summernote.min.js', array('inline' => false));?>
<?php $this->Html->css('/admin/js/plugins/summernote/summernote.css', array('inline' => false));?>
<script type="text/javascript">
    replace_txt = '#PageContent';
</script>
<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header"><?php echo __("Adauga pagina noua")?></h1>
    </div>
    <!-- /.col-lg-12 -->
</div>
<!-- /.row -->
<div class="row">
    <div class="col-lg-6">
        <?= $this->H->bslk(
            "/cms/pages",
            'btn btn-warning btn-block',
            $this->Html->icon('fa fa-arrow-left') . __(' Inapoi la pagini statice'),
            __("Inapoi la pagini statice")
        )?>
        <div class="mg20"></div>
    </div>
    <!-- /.col-lg-6 -->
</div>
<!-- /.row -->
<div class="row">
    <div class="col-lg-12">
        <?php echo $this->Form->create('Page');?>
            <?php echo $this->Form->input('parent_id', array('empty' => 'Pagina parinte'))?>
            <?php echo $this->Form->input('title', array('label' => __('Denumire')))?>
            <?php echo $this->Form->input('meta_title', array('label' => __('Meta title')))?>
            <?php echo $this->Form->input('meta_description', array('label' => __('Meta descriere')))?>
            <?php echo $this->Form->input('meta_keywords', array('label' => __('Meta keywords')))?>
            <?php echo $this->Form->input('content', array('label' => __('Meta keywords')))?>
            <?php echo $this->Form->submit(__('Adauga pagina'), array('class' => "btn btn-success btn-lg btn-block"));?>
        <?php echo $this->Form->end();?>
    </div>
    <!-- /.col-lg-12 -->
</div>
<!-- /.row -->