<style>
    .fa-remove::before, .fa-close::before, .fa-times::before{
        display: none;
    }
    .sim-row-delete {
        display: none;
    }
    .h-product-color {
        position: absolute;
        color:
                #2f2f2f;
        font-size: 18px;
        text-align: center;
        top: 30px;
        left: 0;
        right: 0;
        z-index: 90;
        font-family: Poppins, sans-serif;
        font-weight: 500 !important;
    }
    .selection-menu {
        position: relative;
        background-color:
                #fff;
        padding: 10px 0;
        display: flex;
        justify-content: space-between;
        max-width: 1200px;
        margin: 0 auto;
    }
    .preorder-link {
        position: absolute;
        right: 0;
        bottom: -45px;
        background-color: #1385d4;
        padding: 10px 20px;
        color: #fff;
        text-transform: uppercase;
        border-radius: 3px;
        transition: ease-in 0.2s;
        width: 115px;
        margin: 0 auto;
        left: 0;
    }
    a:hover, a:focus {
        text-decoration: none;
    }
</style>
<?=$this->start('page-title');?>
    <strong><?php echo __("Continut Landing Page")?></strong>
<?=$this->end()?>

    <br>
    <h4>Denumire Pagina: <b><?=$landing_page['LandingPage']['name']; ?></b></h4>
    <br>

    <div style="text-align: center"><?php echo $landing_page['LandingPage']['content'];?></div>
