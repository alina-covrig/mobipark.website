<style>
    .h-products-container .sim-row-edit {
        position: static;
    }
    .preorder-link {
        position: absolute !important;
    }
    .sim-edit {
        z-index: 100;
    }
    .h-product-color {
        color: #686565;
    }
    body {
        overflow-x: hidden;
        overflow-y: auto;
        background:white !important;
    }
    #newsletter-builder-area {
        overflow-y: auto;
        overflow-x: hidden;
    }
    .banner-xs {
        display: block;
    }
    .h-product-container.category-placeholder {
        display: block;
    }
    .h-product-container.category-placeholder {
        background-color: #2f2f2f;
        display: flex;
        align-content: center;
        justify-content: center;
        border-radius: 10px;
        flex-basis: 2%;
    }
    .h-product-container.category-placeholder span {
        align-self: center;
        text-align: center;
        padding: 30px 10px;
        font-size: 26px;
        line-height: 30px;
        font-weight: bold;
        color: #fff;
    }
    #newsletter-builder-area-center-frame-buttons-dropdown {
        z-index: 1000;
    }
    .selection-menu {
        background-color: transparent;
    }
    .mobile-banner-landing .sim-row-edit-hover {
        width: 480px !important;
        background: #428bca !important;
        height: 40px !important;
        top: -120px !important;
        position: relative;
        margin-bottom: -40px;
    }
    .h-product-color.sim-row-edit {
        font-size: 16px;
    }

    #newsletter-builder-area-center {
        margin-left: 40px;
    }
    .highlight-text.last-text {
        overflow: hidden;
    }
    .display-2 .gray-backdrop, .display-1 .gray-backdrop, .display-3 .gray-backdrop {
        width: auto;
    }
    .display-2 .sim-row-edit-hover_custom, .display-1 .sim-row-edit-hover_custom, .display-3 .sim-row-edit-hover_custom {
        width: 12%;
        z-index: 100;
        height: 12%;
    }
    .display-2 .sim-row-edit-hover, .display-1 .sim-row-edit-hover, .display-3 .sim-row-edit-hover {
        width: 50%;
        height: 100%;
       top: 0;
        left: 25%;
    }
    .video-container iframe,
    .video-container object,
    .video-container embed {
        position: absolute;
        top: 0;
        left: 0;
        width: 100%;
        height: 100%;
    }

</style>
    <?=$this->element('Plugins/landingPage')?>

    <div id="newsletter-preloaded-download">
     <!--   <form id="export-form" action="/loyalty/newsletters/edit/<?=$newsletter['Newsletter']['id']?>" method="post" name="export-form">
            <textarea id="export-textarea" name="export-textarea"></textarea>
            <input type="text" name="newsletter-name" id="newsletter-name" />
        </form>
    -->
    </div>

    <div id="newsletter-preloaded-export"></div>
    <div id="newsletter-preloaded-rows">
        <?=$this->element('/LandingPage/Gsmnet/promobar')?>
        <?=$this->element('/LandingPage/Gsmnet/banner')?> <!-- banner mare-->
       <!--  <?=$this->element('/LandingPage/Gsmnet/static_3_products')?>
        <?=$this->element('/LandingPage/Gsmnet/static_2_products')?>-->
        <?=$this->element('/LandingPage/Gsmnet/static_2_wrapped_products')?>
      <!--  <?=$this->element('/LandingPage/Gsmnet/single_product')?>-->
        <?=$this->element('/LandingPage/Gsmnet/single_wrapped_product')?>
        <?=$this->element('/LandingPage/Gsmnet/text_plus_button')?>
        <?=$this->element('/LandingPage/Gsmnet/wrapped_products')?>
        <?=$this->element('/LandingPage/Gsmnet/selection-menu')?>
        <?=$this->element('/LandingPage/Gsmnet/left_text')?>
        <?=$this->element('/LandingPage/Gsmnet/black_button')?>
        <?=$this->element('/LandingPage/Gsmnet/middle_banner')?>
        <?=$this->element('/LandingPage/Gsmnet/l_text_r_image')?>
        <?=$this->element('/LandingPage/Gsmnet/r_text_l_image')?>
        <?=$this->element('/LandingPage/Gsmnet/video')?>
        <?=$this->element('/LandingPage/Gsmnet/video2')?>
        <?=$this->element('/LandingPage/Gsmnet/video3')?>
        <?=$this->element('/LandingPage/Gsmnet/footer1')?>
        <?=$this->element('/LandingPage/Gsmnet/promobar_footer')?>
    </div>



<div id="newsletter-builder" class="row">

    <div class="col-lg-3">
        <?= $this->Form->create('LandingPage', ['type' => 'file', 'id' =>'export-form', 'name' => 'export-form']);?>
        <?= $this->Form->input('name', [
            'label' => 'Denumire Landing Page',
            'value' => $landing_page['LandingPage']['name']
        ]);?>
        <?= $this->Form->input('start_date',
            [
                'type' => 'text',
                'id' => 'CampaignStart',
                'value' => $landing_page['LandingPage']['start_date'],
                'label' => __('Data inceput')
            ]
        )
        ?>
        <?= $this->Form->input('end_date',
            [
                'type' => 'text',
                'id' => 'CampaignEnd',
                'value' => $landing_page['LandingPage']['end_date'],
                'label' => __('Data incheiere')
            ]
        )
        ?>
        <?= $this->Form->input('path',
            [
                'type' => 'text',
                'label' => 'URL specific paginii. Exemplu: "christmas-page"',
                'value' => $landing_page['LandingPage']['path']
            ]
        )?>
        <?= $this->Form->input('page_title',
            [
                'type' => 'text',
                'label' => __('Meta Title'),
                'value' => $landing_page['LandingPage']['page_title']
            ]
        )?>
        <?= $this->Form->input('meta_description',
            [
                'type' => 'textarea',
                'style' => 'width:100%;',
                'value' => $landing_page['LandingPage']['meta_description'],
                'label' => 'Meta description'
            ]
        )?>
        <div id="checkbox-container">
        <?= $this->Form->input('is_active',
            [
                'type' => 'checkbox',
                'label' => 'Activ?',
                'style' => 'margin-left: 0',
                'value' => $landing_page['LandingPage']['is_active'],
                'checked' => ($landing_page['LandingPage']['is_active'] == 1) ? 1 : 0
            ]) ?>
        <?= $this->Form->input('is_forever',
            [
                'type' => 'checkbox',
                'label' => 'Bifeaza daca vrei ca pagina sa fie vizibila pentru totdeauna',
                'style' => 'margin-left: 0',
                'checked' => ($landing_page['LandingPage']['is_forever'] == 1) ? 1 : 0
            ]) ?>
        </div>
        <input type="hidden" name="data[LandingPage][content]" id="landing-page-content"/>
        <?= $this->Form->submit('Editeaza Pagina',
            [
                'type' => 'submit',
                'class' => 'btn btn-block btn-primary',
                'style' => 'border:none; border-radius:5px; color: white',
                'id' => 'submit-lp2',
            ]);?>
        <?= $this->Form->end();?>



        <div class="mb20"></div>

        <?= $this->Panel->start(__('Informatii utile'), 'panel-default', 0) ?>
        <div class="panel-heading">
            <h4>Utilizarea editorului</h4>
            <ol>
                <li>
                    Atunci cand editati texte ce contin taguri ca de exemplu
                    <code>&lt;div&gt;</code>,<code>&lt;br/&gt;</code>, <code>&lt;span&gt;</code> etc
                    va rog sa <b>nu</b> le stergeti, ci sa scrieti in jurul lor.
                </li>
                <li> Atunci cand adaugati un meniu cu butoane albastre (de categorii)
                    nu veti putea sa editati din prima continutul lor.
                    Trebuie sa salvati pagina si sa o accesati din nou.
                </li>
                <li>
                    URL-ul pe care il veti completa in box-urile albastre din
                    meniul cu categorii trebuie sa corespunda cu id-ul butoanelor
                    negre care se afla deasupra blocurilor cu 3 produse
                </li>
                <li>
                    Pentru inserarea unei noi categorii, apasati pe butonul rosu
                    din dreptul meniului cu categorii.
                </li>
            </ol>
        </div>
        <?= $this->Panel->end() ?>
    </div>


    <div id="newsletter-builder-area" class="resize-height col-lg-9" style="float:none;">
        <div id="newsletter-builder-area-center" style="width:90%">
            <div id="newsletter-builder-area-center-frame" style="width:100%">

                <div id="newsletter-builder-area-center-frame-buttons">
                    <div id="newsletter-builder-area-center-frame-buttons-add">Add&nbsp;&nbsp;<i class="fa fa-caret-down"></i>
                        <div id="newsletter-builder-area-center-frame-buttons-dropdown">
                            <div class="newsletter-builder-area-center-frame-buttons-dropdown-tab" id="add-header"><p>Header</p><i class="fa fa-caret-right"></i></div>
                            <div class="newsletter-builder-area-center-frame-buttons-dropdown-tab" id="add-content"><p>Content</p><i class="fa fa-caret-right"></i></div>
                            <div class="newsletter-builder-area-center-frame-buttons-dropdown-tab" id="add-footer"><p>Footer</p><i class="fa fa-caret-right"></i></div>
                            <div class="newsletter-builder-area-center-frame-buttons-content">
                                <div class="newsletter-builder-area-center-frame-buttons-content-tab" data-type="header" data-id="1">
                                    <img src="<?=Configure::read('STORE_URL')?>theme/default/assets/images/landing-pages/promobar.JPG"/>
                                    Promobar
                                </div>
                                <div class="newsletter-builder-area-center-frame-buttons-content-tab" data-type="content" data-id="11">
                                    <img src="<?=Configure::read('STORE_URL')?>theme/default/assets/images/landing-pages/banner.JPG"/>
                                    Banner
                                </div>
                           <!--     <div class="newsletter-builder-area-center-frame-buttons-content-tab" data-type="content" data-id="12">
                                    <img src="<?=Configure::read('STORE_URL')?>theme/default/assets/images/landing-pages/staticprod.JPG"/>
                                    Bloc cu 3 produse
                                </div>
                                <div class="newsletter-builder-area-center-frame-buttons-content-tab" data-type="content" data-id="24">
                                    <img src="<?=Configure::read('STORE_URL')?>theme/default/assets/images/landing-pages/tempsnip.jpg"/>
                                    Bloc cu 2 produse
                                </div>
                                <div class="newsletter-builder-area-center-frame-buttons-content-tab" data-type="content" data-id="26">
                                    <img src="<?=Configure::read('STORE_URL')?>theme/default/assets/images/landing-pages/prod1.jpg"/>
                                    1 singur produs
                                </div>  -->
                                <div class="newsletter-builder-area-center-frame-buttons-content-tab" data-type="content" data-id="25">
                                    <img src="<?=Configure::read('STORE_URL')?>theme/default/assets/images/landing-pages/prod2.JPG"/>
                                    Bloc cu 2 produse cu fundal
                                </div>
                                <div class="newsletter-builder-area-center-frame-buttons-content-tab" data-type="content" data-id="27">
                                    <img src="<?=Configure::read('STORE_URL')?>theme/default/assets/images/landing-pages/single-prod.JPG"/>
                                    1 singur produs cu fundal
                                </div>
                                <div class="newsletter-builder-area-center-frame-buttons-content-tab" data-type="content" data-id="14">
                                    <img src="<?=Configure::read('STORE_URL')?>theme/default/assets/images/landing-pages/wrappedprod.JPG"/>
                                    3 produse cu fundal
                                </div>
                                <div class="newsletter-builder-area-center-frame-buttons-content-tab" data-type="content" data-id="13">
                                    <img src="<?=Configure::read('STORE_URL')?>theme/default/assets/images/landing-pages/text_button.JPG"/>
                                    Text + buton
                                </div>
                                <div class="newsletter-builder-area-center-frame-buttons-content-tab" data-type="content" data-id="23">
                                    <img src="<?=Configure::read('STORE_URL')?>theme/default/assets/images/landing-pages/selection-menu.JPG"/>
                                    Selection menu
                                </div>
                                <div class="newsletter-builder-area-center-frame-buttons-content-tab" data-type="content" data-id="15">
                                    <img src="<?=Configure::read('STORE_URL')?>theme/default/assets/images/landing-pages/text-simplu.JPG" />
                                    Text simplu
                                </div>
                                <div class="newsletter-builder-area-center-frame-buttons-content-tab" data-type="content" data-id="16">
                                    <img src="<?=Configure::read('STORE_URL')?>theme/default/assets/images/landing-pages/buton-simplu.JPG" />
                                    Buton simplu
                                </div>
                                <div class="newsletter-builder-area-center-frame-buttons-content-tab" data-type="content" data-id="17">
                                    <img src="<?=Configure::read('STORE_URL')?>theme/default/assets/images/landing-pages/middle-banner.JPG" />
                                    Banner cu detalii
                                </div>
                                <div class="newsletter-builder-area-center-frame-buttons-content-tab" data-type="content" data-id="18">
                                    <img src="<?=Configure::read('STORE_URL')?>theme/default/assets/images/landing-pages/text-img-r.JPG" />
                                    Text cu imagine in dreapta
                                </div>
                                <div class="newsletter-builder-area-center-frame-buttons-content-tab" data-type="content" data-id="19">
                                    <img src="<?=Configure::read('STORE_URL')?>theme/default/assets/images/landing-pages/text-l-image.JPG" />
                                    Text cu imagine in stanga
                                </div>
                                <div class="newsletter-builder-area-center-frame-buttons-content-tab" data-type="content" data-id="28">
                                    <img src="<?=Configure::read('STORE_URL')?>theme/default/assets/images/landing-pages/video-img.JPG" />
                                    Video
                                </div>
                                <div class="newsletter-builder-area-center-frame-buttons-content-tab" data-type="content" data-id="29">
                                    <img src="<?=Configure::read('STORE_URL')?>theme/default/assets/images/landing-pages/video2.JPG" />
                                    Video 2
                                </div>
                                <div class="newsletter-builder-area-center-frame-buttons-content-tab" data-type="content" data-id="30">
                                    <img src="<?=Configure::read('STORE_URL')?>theme/default/assets/images/landing-pages/video3.JPG" />
                                    Video 3
                                </div>

                                <div class="newsletter-builder-area-center-frame-buttons-content-tab" data-type="footer" data-id="21">
                                    <img src="<?=Configure::read('STORE_URL')?>theme/default/assets/images/landing-pages/footer.JPG" />
                                </div>
                                <div class="newsletter-builder-area-center-frame-buttons-content-tab" data-type="footer" data-id="22">
                                    <img src="<?=Configure::read('STORE_URL')?>theme/default/assets/images/landing-pages/promobar.JPG" />
                                    Promobar + spatiu
                                </div>
                            </div>
                        </div>

                    </div>
                    <div id="newsletter-builder-area-center-frame-buttons-reset"><a href="/cms/landing_pages/edit/<?=$landing_page['LandingPage']['id']?>"><i class="icon-reload-CCW"></i>&nbsp;&nbsp;Reset</a></div>
                </div>
                <div id="newsletter-builder-area-center-frame-content" style="width:100%">
                    <?=$landing_page['LandingPage']['content'];?>
                </div>
            </div>
        </div>
    </div>
</div>

    <div class="sim-edit" id="sim-edit-export">
        <div class="sim-edit-box" style="height:390px;">
            <div class="sim-edit-box-title">Export Template</div>

            <div class="sim-edit-box-content">
                <div class="sim-edit-box-content-text">Select and copy the entire text below</div>
                <div class="sim-edit-box-content-field"><textarea class="sim-edit-box-content-field-textarea text"></textarea></div>
            </div>
            <div class="sim-edit-box-buttons">
                <div class="sim-edit-box-buttons-cancel" style="margin-left:0px;">Cancel</div>
            </div>
        </div>
    </div>

    <div class="sim-edit" id="sim-edit-image">
        <div class="sim-edit-box" style="height:370px;" >
            <div class="sim-edit-box-title">Edit Image</div>
            <div class="sim-edit-box-content">
                <div class="sim-edit-box-content-text">URL:<span>(full address including http://)</span></div>
                <div class="sim-edit-box-content-field"><input type="text" class="sim-edit-box-content-field-input image"/></div>
            </div>
            <div class="sim-edit-box-buttons">
                <div class="sim-edit-box-buttons-save">Save!</div>
                <div class="sim-edit-box-buttons-cancel">Cancel</div>
            </div>
            <div class="sim-edit-box-title">Incarca image</div>
            <form enctype="multipart/form-data" method="post" id="upload_image_form">
                <input type="file" class="sim-edit-box-content-field" name="file" id="upload_image" />
                <input type="submit" class="sim-upload-image-save"  value="Incarca poza si salveaza"/>
            </form>
        </div>
    </div>

    <div class="sim-edit" id="sim-edit-link">
        <div class="sim-edit-box" style="height:340px;">
            <div class="sim-edit-box-title">Edit Link</div>

            <div class="sim-edit-box-content">
                <div class="sim-edit-box-content-text">Title</div>
                <div class="sim-edit-box-content-field"><input type="text" class="sim-edit-box-content-field-input title"/></div>
                <div class="sim-edit-box-content-text">URL:<span>(full address including http://)</span></div>
                <div class="sim-edit-box-content-field"><input type="text" class="sim-edit-box-content-field-input url"/></div>
            </div>
            <div class="sim-edit-box-buttons">
                <div class="sim-edit-box-buttons-save">Save</div>
                <div class="sim-edit-box-buttons-cancel">Cancel</div>
            </div>
        </div>
    </div>

    <!--============ custom editor for id $ text ============-->
    <div class="sim-edit" id="sim-edit-id-text">
        <div class="sim-edit-box" style="height:440px;">

            <div class="sim-edit-box-content">
                <div class="sim-edit-box-content-text">ID:</div>
                <div class="sim-edit-box-content-field"><input type="text" class="sim-edit-box-content-field-input id"/></div>
                <div class="sim-edit-box-content-text">Text</div>
                <div class="sim-edit-box-content-field"><textarea class="sim-edit-box-content-field-textarea text"></textarea></div>
            </div>

            <div class="sim-edit-box-buttons">
                <div class="sim-edit-box-buttons-save">Save</div>
                <div class="sim-edit-box-buttons-cancel">Cancel</div>
            </div>
        </div>
    </div>


    <!--============ custom editor background-color ============-->
    <div class="sim-edit" id="sim-edit-styles">
        <div class="sim-edit-box" style="height:440px;">

            <div class="sim-edit-box-content">
                <div class="sim-edit-box-content-text">Culoare fundal:</div>
                <div class="sim-edit-box-content-field"><input type="color" class="sim-edit-box-content-field-input background"/></div>
            </div>

            <div class="sim-edit-box-buttons">
                <div class="sim-edit-box-buttons-save">Save</div>
                <div class="sim-edit-box-buttons-cancel">Cancel</div>
            </div>
        </div>
    </div>

    <!--==== custom edit category menu (selection menu) ====-->
    <div class="sim-edit" id="sim-edit-selection-menu">
        <div class="sim-edit-box" style="height:280px;">

            <div class="sim-edit-box-content">
                <div class="sim-edit-box-content-text">Insereaza denumire pe <br> 2 randuri ca in exemplu: 40mm < br> Aluminium</div>
                <div class="sim-edit-box-content-field"><input type="text" class="sim-edit-box-content-field-input categ"/></div>
            </div>

            <div class="sim-edit-box-buttons">
                <div class="sim-edit-box-buttons-save">Save</div>
                <div class="sim-edit-box-buttons-cancel">Cancel</div>
            </div>
        </div>
    </div>



    <div class="sim-edit" id="sim-edit-image-link">
        <div class="sim-edit-box" style="height:505px;">
            <div class="sim-edit-box-content">
                <div class="sim-edit-box-content-text">Alt</div>
                <div class="sim-edit-box-content-field"><input type="text" class="sim-edit-box-content-field-input title"/></div>
                <div class="sim-edit-box-content-text">URL:</div>
                <div class="sim-edit-box-content-field"><input type="text" class="sim-edit-box-content-field-input url"/></div>
                <div class="sim-edit-box-content-text">URL Imagine:</div>
                <div class="sim-edit-box-content-field"><input type="text" class="sim-edit-box-content-field-input image"/></div>
            </div>
            <div class="sim-edit-box-buttons">
                <div class="sim-edit-box-buttons-save">Save</div>
                <div class="sim-edit-box-buttons-cancel">Cancel</div>
            </div>
            <div class="sim-edit-box-title">Upload image</div>
            <form enctype="multipart/form-data" method="post" id="upload_image_link_form">
                <input type="file" class="sim-edit-box-content-field" name="file" id="upload_image_link" />
                <input type="submit" class="sim-upload-image-save" value="Incarca poza">
            </form>
        </div>
    </div>

    <div class="sim-edit" id="sim-edit-title">
        <div class="sim-edit-box" style="height:230px;">
            <div class="sim-edit-box-title">Edit Title</div>

            <div class="sim-edit-box-content">
                <div class="sim-edit-box-content-text">Title</div>
                <div class="sim-edit-box-content-field"><input type="text" class="sim-edit-box-content-field-input title"/></div>
            </div>
            <div class="sim-edit-box-buttons">
                <div class="sim-edit-box-buttons-save">Save</div>
                <div class="sim-edit-box-buttons-cancel">Cancel</div>
            </div>
        </div>
    </div>


    <div class="sim-edit" id="sim-edit-text">
        <div class="sim-edit-box" style="height:390px;">
            <div class="sim-edit-box-title">Edit Text</div>

            <div class="sim-edit-box-content">
                <div class="sim-edit-box-content-text">Text</div>
                <div class="sim-edit-box-content-field"><textarea class="sim-edit-box-content-field-textarea text"></textarea></div>
            </div>
            <div class="sim-edit-box-buttons">
                <div class="sim-edit-box-buttons-save">Save</div>
                <div class="sim-edit-box-buttons-cancel">Cancel</div>
            </div>
        </div>
    </div>

     <!---------- edit iframe ---------->
    <div class="sim-edit" id="sim-edit-iframe">
        <div class="sim-edit-box" style="height:390px;">
            <div class="sim-edit-box-title">Edit Iframe</div>

            <div class="sim-edit-box-content">
                <div class="sim-edit-box-content-text">Iframe</div>
                <div class="sim-edit-box-content-field"><textarea class="sim-edit-box-content-field-textarea iframe"></textarea></div>
            </div>
            <div class="sim-edit-box-buttons">
                <div class="sim-edit-box-buttons-save">Save</div>
                <div class="sim-edit-box-buttons-cancel">Cancel</div>
            </div>
        </div>
    </div>

    <div class="sim-edit" id="sim-edit-product">
        <div class="sim-edit-box" style="height:290px;">
            <div class="sim-edit-box-title">Introduceti ID Produs:</div>
            <div class="sim-edit-box-content">
                <div class="sim-edit-box-content-field"><input type="text" class="sim-edit-box-content-field-input title"/></div>
            </div>
            <br/>
            <div class="sim-edit-box-content">
                Pentru a regenera informatiile analitice, salvati din nou produsul!
            </div>

            <div class="sim-edit-box-buttons">
                <div class="sim-edit-box-buttons-save">Save</div>
                <div class="sim-edit-box-buttons-cancel">Cancel</div>
            </div>
        </div>
    </div>

    <div class="sim-edit" id="sim-edit-icon">
        <div class="sim-edit-box" style="height:580px;">
            <div class="sim-edit-box-title">Edit Icon</div>

            <div class="sim-edit-box-content">
                <div class="sim-edit-box-content-text">Select Icon</div>
                <?=$this->element('icons');?>
            </div>
            <div class="sim-edit-box-buttons">
                <div class="sim-edit-box-buttons-cancel" style="margin-left:0px;">Cancel</div>
            </div>
        </div>
    </div>


    <div class="sim-edit" id="sim-edit-youtube">
        <div class="sim-edit-box">
            <div class="sim-edit-box-title">Edit Youtube Link</div>
            <div class="sim-edit-box-content">
                <div class="sim-edit-box-content-text">Youtube Link</div>
                <div class="sim-edit-box-content-field">
                    <input type="text" class="sim-edit-box-content-field-input" id="yt-link"/>
                </div>
            </div>
            <div class="sim-edit-box-buttons">
                <div class="sim-edit-box-buttons-save">Save</div>
                <div class="sim-edit-box-buttons-cancel">Cancel</div>
            </div>
        </div>
    </div>



<?=$this->start('extra-scripts')?>
    <script type="text/javascript" >

        $('#submit-lp2').on('click', function(){

            var $content = $("#newsletter-builder-area-center-frame-content").html();
            $('#landing-page-content').val($content);

            $("#export-form").submit();

        });

        $( "#CampaignStart" ).datetimepicker({
            numberOfMonths: 1,
            dayOfWeekStart: 1,
            dateFormat: 'dd-mm-yy',
            timeFormat:  'hh:mm',
            lang:'ro'
        });

        $( "#CampaignEnd" ).datetimepicker({
            numberOfMonths: 1,
            dayOfWeekStart: 1,
            dateFormat: 'dd-mm-yy',
            timeFormat:  'hh:mm',
            lang:'ro'
        });

        function edit_product() {
            var parent;
            $(document).on('click', '.product-edit-hover', function(){
                $("#sim-edit-product").fadeIn(500);
                $("#sim-edit-product .sim-edit-box").slideDown(500);
                parent = $(this).parent();
            });

            $("#sim-edit-product .sim-edit-box-buttons-save").click(function() {
                $(this).parent().parent().parent().fadeOut(500);
                $(this).parent().parent().slideUp(500);

                var value = $(this).closest('.sim-edit-box').find('.sim-edit-box-content-field-input').val();
                var size ='354x300';


                $.ajax({
                    url: '/catalog/product_alias_names/getProductDataForLandingPage',
                    type: 'post',
                    dataType: 'json',
                    data: {
                        panid: value,
                        size: size,
                    },
                    success: function(r) {
                        if (r.success == 1) {
                            parent.find('.h-product-color').text(r.name);
                            parent.find('.sim-row-content3-center-tab-image img').attr('src', r.image).attr('alt', r.name);
                            var $imageLink =  parent.find('.sim-row-content3-center-tab-image');
                            $imageLink.attr({
                                'href': r.path,
                                'data-promo-id': 'lp-' + <?= $landing_page['LandingPage']['id'] ?> + '-' + value,
                                'data-promo-name': '<?= $landing_page['LandingPage']['name'] ?>' + '-' + r.name,
                                'data-promo-creative': '<?= $landing_page['LandingPage']['name'] ?> Product Item',
                                'data-promo-position': '<?= $landing_page['LandingPage']['name'] ?> Landing Page'
                            });
                            $imageLink.addClass('promo-trigger');

                        } else {
                            alert('Produs inexistent');
                        }
                    }

                })
            });
        }
        edit_product();


        var checkboxValues = JSON.parse(localStorage.getItem('checkboxValues')) || {},
            $checkboxes = $("#checkbox-container :checkbox");

        $checkboxes.on("change", function(){
            $checkboxes.each(function(){
                checkboxValues[this.id] = this.checked;
            });

            localStorage.setItem("checkboxValues", JSON.stringify(checkboxValues));
        });

        // On page load
        $.each(checkboxValues, function(key, value) {
            $("#" + key).prop('checked', value);
        });

    </script>
<?=$this->end()?>

