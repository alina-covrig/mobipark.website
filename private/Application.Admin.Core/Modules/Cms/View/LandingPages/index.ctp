<?=$this->start('page-title');?>
    <strong><?php echo __("Landing Pages")?></strong>
<?=$this->end()?>
    <div class="row" style="padding-top: 20px;">
        <div class="col-lg-3">
            <?= $this->H->bslk(
                "/cms/landing_pages/add",
                'btn btn-warning btn-block',
                $this->Html->icon('fa fa-plus') . __(' Adauga Landing Page'),
                __("Adauga Landing page")
            )?>
            <div class="mg20"></div>
        </div>
        <!-- /.col-lg-3 -->

    </div>
    <!-- /.row -->

    <div class="row">
        <div class="col-md-12">
            <table class="table table-condensed small">
                <thead>
                <tr>
                    <th><?=__('ID')?></th>
                    <th><?=__('Nume')?></th>
                    <th><?=__('Data de start')?></th>
                    <th><?=__('Data de finish')?></th>
                    <th><?=__('Link pagina')?></th>
                    <th><?=__('Actiuni')?></th>
                </tr>
                </thead>
                <tbody>
                <?php foreach ($landing_pages as $ln):?>
                    <tr>
                        <td><?=$ln['LandingPage']['id']?></td>
                        <td><?=$ln['LandingPage']['name']?></td>
                        <td><?=date('d.m.Y / h:m', strtotime($ln['LandingPage']['start_date']))?></td>
                        <td><?=date('d.m.Y / h:m', strtotime($ln['LandingPage']['end_date']))?></td>

                        <td><a href="<?=Configure::read("STORE_URL").$ln['LandingPage']['path']?>" target="_blank">Acceseaza pagina</a></td>
                        <td align="right" width="100">
                            <div class="btn-group btn-group-xs">
                                <a href="/cms/landing_pages/edit/<?=$ln['LandingPage']['id']?>"
                                   class="btn btn-info btn-xs btn-outline"
                                   title="Editeaza"
                                   data-original-title="Editeaza"
                                   target="_blank">
                                    <i class="fa fa fa-pencil"></i>
                                </a>
                                <a href="/cms/landing_pages/view/<?=$ln['LandingPage']['id']?>"
                                   class="btn btn-info btn-xs btn-outline"
                                   data-toggle="tooltip"
                                   data-placement="top"
                                   data-container="body"
                                   title="Vizualizare"
                                   data-original-title="Vizualizare"
                                   target="_blank">
                                    <i class="fa fa-eye"></i>
                                </a>
                            </div>
                        </td>
                    </tr>
                <?php endforeach;?>
                </tbody>
            </table>
            <?php echo $this->element('Navigation/pagination'); ?>
        </div>
    </div>

