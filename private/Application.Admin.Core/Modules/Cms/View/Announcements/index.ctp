<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header"><?php echo __("Anunturi")?></h1>
    </div>
    <!-- /.col-lg-12 -->
</div>
<!-- /.row -->
<div class="row">
    <div class="col-lg-6">
        <?= $this->H->bslk(
            "/cms/announcements/add",
            'btn btn-warning btn-block',
            $this->Html->icon('fa fa-plus') .' '. __('Adauga anunt'),
            __("Adauga anunt")
        )?>
        <div class="mg20"></div>
    </div>
    <!-- /.col-lg-6 -->
</div>
<!-- /.row -->
<div class="row">
    <div class="col-lg-12">
        <table class="table tree table-bordered table-striped table-condensed">
            <thead>
            <tr class="text-center">
                <th><?php echo __('ID') ?></th>
                <th><?php echo __("Frontend")?></th>
                <th><?php echo __("Backend")?></th>
                <th><?php echo __("De la")?></th>
                <th><?php echo __("Pana la")?></th>
                <th><?php echo __('Ordine') ?></th>
                <th><?php echo __('Mesaj') ?></th>
                <th><?php echo __("Actiuni")?></th>
            </tr>
            </thead>
            <tbody>
            <?php foreach($announcements as $k => $a) :?>
                <tr class="text-center">
                    <td><?=$a['Announcement']['id'] ?></td>
                    <td><?=$a['Announcement']['is_frontend'] ? __("DA") : __("NU");?></td>
                    <td><?=$a['Announcement']['is_backend'] ? __("DA") : __("NU");?></td>
                    <td><?php echo $a['Announcement']['from']; ?></td>
                    <td><?php echo $a['Announcement']['to']; ?></td>
                    <td><?php echo $a['Announcement']['order']; ?></td>
                    <td><?php echo $a['Announcement']['message']; ?></td>
                    <td align="right" width="145">
                        <div class="btn-group btn-group-xs">
                            <?= $this->H->bslk(
                                "/cms/announcements/delete/" . $a['Announcement']['id'],
                                'btn btn-danger btn-xs btn-outline',
                                $this->Html->icon('fa fa-trash-o'),
                                __("Sterge"),
                                'top',
                                __("Esti sigur ca vrei sa stergi anuntul?")
                            )?>
                            <?= $this->H->bslk(
                                "/cms/announcements/edit/" . $a['Announcement']['id'],
                                'btn btn-info btn-xs btn-outline',
                                $this->Html->icon('fa fa-pencil'),
                                __("Modifica")
                            )?>
                        </div>
                    </td>
                </tr>
            <?php endforeach;?>
            </tbody>
        </table>
    </div>
</div>
<!--/.row-->