
<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header"><?php echo __("Modificare anunt")?></h1>
    </div>
    <!-- /.col-lg-12 -->
</div>
<!-- /.row -->
<div class="row">
    <div class="col-lg-6">
        <?= $this->H->bslk(
            "/cms/announcements",
            'btn btn-warning btn-block',
            $this->Html->icon('fa fa-arrow-left') . __(' Inapoi la anunturi'),
            __("Inapoi la banere")
        )?>
        <div class="mg20"></div>
    </div>
    <div class="col-lg-6">
        <?= $this->H->bslk(
            "/cms/announcements/delete/" . $this->request->data['Announcement']['id'],
            'btn btn-danger btn-block',
            $this->Html->icon('fa fa-trash-o') . ' ' . __('Sterge anuntul'),
            __("Sterge anuntul"),
            'top',
            __('Esti sigur ca vrei sa stergi anuntul?')
        )?>
        <div class="mg20"></div>
    </div>
</div>
<!-- /.row -->
<div class="row">
    <div class="col-md-12">
        <?php echo $this->Form->create('Announcement');?>
        <?php echo $this->Form->input('id')?>
        <?php echo $this->Form->input('is_frontend', array('label' => 'Frontend?', 'class' => 'form-control'))?>
        <?php echo $this->Form->input('is_backend', array('label' => 'Backend?', 'class' => 'form-control'))?>
        <?php echo $this->Form->input('from', array('label' => 'Activ de la', 'class' => 'form-control add_datetimepicker', 'type' => 'text'))?>
        <?php echo $this->Form->input('to', array('label' => 'Pana la', 'class' => 'form-control add_datetimepicker', 'type' => 'text'))?>
        <?php echo $this->Form->input('order', array('label' => __('Order')))?>
        <?php echo $this->Form->input('message', array('label' => __('Anunt')))?>
        <?php echo $this->Form->submit(__('Modifica anunt'), array('class' => "btn btn-success btn-lg btn-block"));?>
        <?php echo $this->Form->end();?>
    </div>
    <!-- /.row -->