<?=$this->start('page-title');?>
    <strong><?php echo __("Teme speciale")?></strong>
<?=$this->end()?>
<!-- /.row -->
<div class="row">
    <div class="col-lg-6">
        <?= $this->H->bslk(
            "/cms/themes/add",
            'btn btn-warning btn-block',
            $this->Html->icon('fa fa-plus') . __(' Adauga tema noua'),
            __("Adauga tema noua")
        )?>
        <div class="mg20"></div>
    </div>
    <!-- /.col-lg-6 -->
    <div class="col-lg-6">
        <?= $this->H->bslk(
            "/cms/themes/preview/0",
            'btn btn-success btn-block',
            $this->Html->icon('fa fa-plus') . __(' Anuleaza previzualizare tema'),
            __("Anuleaza previzualizare tema")
        )?>
        <div class="mg20"></div>
    </div>
</div>
<!-- /.row -->
<div class="row">
    <div class="col-lg-12">
        <table class="table tree table-bordered table-striped table-condensed">
            <thead>
            <tr>
                <th><?php echo __("Nume")?></th>
                <th><?php echo __("Active")?></th>
                <th><?php echo __("Optiuni")?></th>
            </tr>
            </thead>
            <tbody>
            <?php foreach($themes as $theme) :?>
                <tr>
                    <td><?php echo $theme['Theme']['name']?></td>
                    <td>
                        <?php
                            if($theme['Theme']['active']) {
                               echo $this->H->bslk(
                                    "/cms/themes/turn/" . $theme['Theme']['id'] ."/0",
                                    'btn btn-danger btn-xs',
                                    'Tema activa - dezactiveaza',
                                    __("Dezactiveaza"),
                                    'top',
                                    __("Esti sigur ca vrei sa dezativezi tema?")
                                );
                            } else {
                                echo $this->H->bslk(
                                    "/cms/themes/turn/" . $theme['Theme']['id'] ."/1",
                                    'btn btn-info btn-xs',
                                    'Activeaza tema aceasta',
                                    __("Activeaza"),
                                    'top',
                                    __("Esti sigur ca vrei sa activezi tema?")
                                );
                            }

                            echo "&nbsp;";


                            if($theme['Theme']['preview']) {
                                echo $this->H->bslk(
                                    "/cms/themes/preview/" . $theme['Theme']['id'] ."/0",
                                    'btn btn-danger btn-xs',
                                    'Tema in Preview - dezactiveaza',
                                    __("Dezactiveaza"),
                                    'top',
                                    __("Esti sigur ca vrei sa dezativezi tema?")
                                );
                            } else {
                                echo $this->H->bslk(
                                    "/cms/themes/preview/" . $theme['Theme']['id'] ."/1",
                                    'btn btn-success btn-xs',
                                    'Preview tema aceasta',
                                    __("Activeaza"),
                                    'top',
                                    __("Esti sigur ca vrei sa activezi tema?")
                                );
                            }



                            ?>


                    </td>
                    <td align="right" width="145">
                        <div class="btn-group btn-group-xs">
                            <?= $this->H->bslk(
                                "/cms/themes/delete/" . $theme['Theme']['id'],
                                'btn btn-danger btn-xs btn-outline',
                                $this->Html->icon('fa fa-trash-o'),
                                __("Sterge"),
                                'top',
                                __("Esti sigur ca vrei sa stergi tema?")
                            )?>
                            <?= $this->H->bslk(
                                "/cms/themes/edit/" . $theme['Theme']['id'],
                                'btn btn-info btn-xs btn-outline',
                                $this->Html->icon('fa fa-pencil'),
                                __("Modifica")
                            )?>
                        </div>
                    </td>
                </tr>
            <?php endforeach;?>
            </tbody>
        </table>
    </div>
</div>
<!--/.row-->