<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header"><?php echo __("Modificare tema")?></h1>
    </div>
    <!-- /.col-lg-12 -->
</div>
<!-- /.row -->
<div class="row">
    <div class="col-lg-6">
        <?= $this->H->bslk(
            "/cms/themes",
            'btn btn-warning btn-block',
            $this->Html->icon('fa fa-arrow-left') . __(' Inapoi la teme'),
            __("Inapoi la teme")
        )?>
        <div class="mg20"></div>
    </div>
    <div class="col-lg-6">
        <?= $this->H->bslk(
            "/cms/themes/delete/" . $this->request->data['Theme']['id'],
            'btn btn-danger btn-block',
            $this->Html->icon('fa fa-trash-o') . ' ' . __('Sterge tema'),
            __("Sterge tema"),
            'top',
            __('Esti sigur ca vrei sa stergi tema?')
        )?>
        <div class="mg20"></div>
    </div>
</div>
<!-- /.row -->
<div class="row">
    <div class="col-md-12">
        <?php echo $this->Form->create('Theme', array('type' => 'file'));?>
            <?php echo $this->Form->input('id')?>
            <?php echo $this->Form->input('name', array('label' => __('Denumire')))?>

            <?php echo $this->Form->input('img_logo', array('type' => 'file', 'label' => __('Logo (W:213px, H:54px)')))?>
            <?php
                if(!empty($this->request->data['Theme']['img_logo'])) {
                    echo '<img src="'.Configure::read('STORE_URL').'img/specialthemes/'.$this->request->data['Theme']['img_logo'].'">';
                }
            ?>

            <?php echo $this->Form->input('img_top_left', array('type' => 'file', 'label' => __('Sus Stanga (W:534px, H:129px)')))?>
            <?php
                if(!empty($this->request->data['Theme']['img_top_left'])) {
                    echo '<img src="'.Configure::read('STORE_URL').'img/specialthemes/'.$this->request->data['Theme']['img_top_left'].'">';
                }
            ?>
            <?php echo $this->Form->input('img_top_right', array('type' => 'file', 'label' => __('Sus Dreapta (W:534px, H:129px)')))?>
            <?php
                if(!empty($this->request->data['Theme']['img_top_right'])) {
                    echo '<img src="'.Configure::read('STORE_URL').'img/specialthemes/'.$this->request->data['Theme']['img_top_right'].'">';
                }
            ?>

            <?php echo $this->Form->input('img_bottom_left', array('type' => 'file', 'label' => __('Jos Stanga (W:1151px, H:416px)')))?>
                <?php
                if(!empty($this->request->data['Theme']['img_bottom_left'])) {
                    echo '<img src="'.Configure::read('STORE_URL').'img/specialthemes/'.$this->request->data['Theme']['img_bottom_left'].'">';
                }
            ?>

            <?php echo $this->Form->input('img_bottom_right', array('type' => 'file', 'label' => __('Jos Dreapta (W:1151px, H:416px)')))?>
            <?php
                if(!empty($this->request->data['Theme']['img_bottom_right'])) {
                    echo '<img src="'.Configure::read('STORE_URL').'img/specialthemes/'.$this->request->data['Theme']['img_bottom_right'].'">';
                }
            ?>

            <?php echo $this->Form->input('img_bottom_center', array('type' => 'file', 'label' => __('Jos Central (W:2691px, H:119px)')))?>
            <?php
                if(!empty($this->request->data['Theme']['img_bottom_center'])) {
                    echo '<img src="'.Configure::read('STORE_URL').'img/specialthemes/'.$this->request->data['Theme']['img_bottom_center'].'">';
                }
            ?>

        <?php echo $this->Form->submit(__('Modifica tema'), array('class' => "btn btn-success btn-lg btn-block"));?>

        <?php echo $this->Form->end();?>
    </div>
<!-- /.row -->


