<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header"><?php echo __("Adauga tema noua")?></h1>
    </div>
    <!-- /.col-lg-12 -->
</div>
<!-- /.row -->
<div class="row">
    <div class="col-lg-6">
        <?= $this->H->bslk(
            "/cms/themes",
            'btn btn-warning btn-block',
            $this->Html->icon('fa fa-arrow-left') . __(' Inapoi la teme'),
            __("Inapoi la teme")
        )?>
        <div class="mg20"></div>
    </div>
    <!-- /.col-lg-6 -->
</div>
<!-- /.row -->
<div class="row">
    <div class="col-lg-12">
        <?php echo $this->Form->create('Theme', array('type' => 'file'));?>
            <?php echo $this->Form->input('name', array('label' => __('Denumire')))?>
            <?php echo $this->Form->input('img_logo', array('type' => 'file', 'label' => __('Logo (W:213px, H:54px)')))?>
            <?php echo $this->Form->input('img_top_left', array('type' => 'file', 'label' => __('Sus Stanga (W:534px, H:129px)')))?>
            <?php echo $this->Form->input('img_top_right', array('type' => 'file', 'label' => __('Sus Dreapta (W:534px, H:129px)')))?>
            <?php echo $this->Form->input('img_bottom_left', array('type' => 'file', 'label' => __('Jos Stanga (W:1151px, H:416px)')))?>
            <?php echo $this->Form->input('img_bottom_right', array('type' => 'file', 'label' => __('Jos Dreapta (W:1151px, H:416px)')))?>
            <?php echo $this->Form->input('img_bottom_center', array('type' => 'file', 'label' => __('Jos Central (W:2691px, H:119px)')))?>
            <?php echo $this->Form->submit(__('Adauga tema'), array('class' => "btn btn-success btn-lg btn-block"));?>
        <?php echo $this->Form->end();?>
    </div>
    <!-- /.col-lg-12 -->
</div>
<!-- /.row -->