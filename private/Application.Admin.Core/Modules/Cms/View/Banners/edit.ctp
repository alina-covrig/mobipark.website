<?php $this->Html->script('/admin/js/plugins/summernote/summernote.min.js', [ 'inline' => false ]);?>
<?php $this->Html->css('/admin/js/plugins/summernote/summernote.css', [ 'inline' => false ]);?>
<script type="text/javascript">
    replace_txt = '#BannerHtml';
</script>
<style>
    .checkbox input[type="checkbox"]{
        margin-left:0;
    }

    .toggle-input, #toggle-campaign-banner{
        display:none;
    }
</style>
<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header"><?= __("Modificare baner")?></h1>
    </div>
    <!-- /.col-lg-12 -->
</div>
<!-- /.row -->
<div class="row">
    <div class="col-lg-6 col-lg-offset-3 no-margin no-padding">
        <div class="col-lg-6">
            <?= $this->H->bslk(
                "/cms/banners",
                'btn btn-warning btn-block',
                $this->Html->icon('fa fa-arrow-left') . __(' Inapoi la banere'),
                __("Inapoi la banere")
            )?>
            <div class="mg20"></div>
        </div>
        <div class="col-lg-6">
            <?= $this->H->bslk(
                "/cms/banners/delete/" . $this->request->data['Banner']['id'],
                'btn btn-danger btn-block',
                $this->Html->icon('fa fa-trash-o') . ' ' . __('Sterge banerul'),
                __("Sterge banerul"),
                'top',
                __('Esti sigur ca vrei sa stergi banerul?')
            )?>
            <div class="mg20"></div>
        </div>
    </div>
</div>

<!-- /.row -->
<div class="row">
    <div class="col-lg-6 col-lg-offset-3" style="margin-bottom: 50px">
        <?= $this->Form->create('Banner', ['type' => 'file']);?>
            <?= $this->Form->input('id')?>
                <?= $this->Form->input('alt', ['label' => 'Alt Tag'], ['empty' => 'Alt'])?>
            <?= $this->Form->input('link', ['label' => 'Link Banner'], ['empty' => 'Link'])?>

            <?= $this->Form->input('order', ['label' => __('Order')])?>
            <?= $this->Form->input('image', ['type' => 'file', 'label' => __('Imagine')])?>
            <?= $this->Form->input('is_active', ['label' => 'Activ'])?>
            <?= $this->Form->input('html', ['label' => 'Html'])?>
            <?= $this->Form->submit(__('Modifica banner'), ['class' => "btn btn-success btn-lg btn-block"]);?>

        <?= $this->Form->end();?>
    </div>
</div>
<!-- /.row -->
<?php $this->start('extra-scripts') ?>
<script>

    $(document).ready(function(){
        let $action_val = $('#BannerMobileAction').val();
        if ($action_val.length > 0 ) {
            $('.link-toggle').show();
            $('.link-toggle input').prop('checked', true);
            if($action_val == 'campaign') {
                $('.campaign-input').addClass('show-custom-input');
            } else {
                $('.custom-link-input').addClass('show-custom-input');
            }
            $('.show-custom-input').show();
        }

        let $campaign_banner_val = $('#campaign_banner').prop('checked');

        if($campaign_banner_val == true ) {
            $('#toggle-campaign-banner').show();
        }
    });


    $('.btn-success').on('click', function(e){
        let $mobile_action = $('#BannerMobileAction').val();
        if($mobile_action.length == 0 ) {
            e.preventDefault();
            $('#confirm_action').modal("show");
        }
    });

    $('#campaign_banner').on('click', function(){
        let $campaign_banner_container = $('#toggle-campaign-banner');

        if($campaign_banner_container.is(":visible")) {
            $campaign_banner_container.hide();
            $campaign_banner_container.find('select')[0].selectedIndex = 0;
        } else {
            $campaign_banner_container.show();
        }
    });

    $('#BannerMobileAction').on('change', function(){
        let $mobile_action = $(this).val();
        $('.toggle-input input').val('');
        if($mobile_action.length > 0 ) {
            $('.link-toggle').show();
            $('.show-custom-input').removeClass('show-custom-input').hide();
            if($mobile_action == 'campaign') {
                $('.campaign-input').addClass('show-custom-input');
            } else {
                $('.custom-link-input').addClass('show-custom-input');
            }

            if($('#custom-link-toggle').is(':checked')) {
                $('.show-custom-input').show();
                $('.custom-link-input input').val('').hide();
            }
        } else {
            $('.link-toggle').hide();
        }
    });

    $('#custom-link-toggle').on('click', function(){
        if($(this).is(':checked')) {
            $('.show-custom-input').show();
        } else {
            $('.show-custom-input').hide();
            $('.custom-link-input input').val('');
        }
    })

</script>
<?php $this->end();?>


<div class="modal fade" id="confirm_action" tabindex="-1" role="dialog"
     aria-labelledby="confirm_action_title" aria-hidden="true" style="display: none;">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h4 class="modal-title" id="confirm_action_title">
                    <?=__('Confirma Actiunea Mobila')?>
                </h4>
            </div>
            <div class="modal-body">
                <div class="col-md-12">
                    <p>
                        <?= __('Nu ati setat actiunea mobila! Noul banner nu va fi vizibil in aplicatie.') ?>
                    </p>
                    <button type="button" class="btn btn-warning pull-right" data-dismiss="modal" aria-hidden="true">
                        <?=__('Anuleaza') ?>
                    </button>
                    <button type="button" class="btn btn-default pull-right mr10" onclick="$('#BannerEditForm').submit()">
                        <?=__('Confirma') ?>
                    </button>
                </div>
                <div class="clearfix"></div>
            </div>
        </div>
    </div>
</div>