<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header">
            <?= __('Bannere')?>
        </h1>
    </div>
    <!-- /.col-lg-12 -->
</div>
<!-- /.row -->
<div class="row">
    <div class="col-lg-6">
        <?= $this->H->bslk(
            '/cms/banners/add',
            'btn btn-warning btn-block',
            $this->Html->icon('fa fa-plus') . ' ' . __('Adauga baner'),
            __('Adauga baner')
        )?>
        <div class="mg20"></div>
    </div>
    <!-- /.col-lg-6 -->
</div>
<!-- /.row -->
<div class="row">
    <div class="col-lg-12">
        <table class="table tree table-bordered table-striped table-condensed">
            <thead>
                <tr class="text-center">
                    <th width="30"><?= __('Nr.') ?></th>
                    <th width="800"><?= __('Imagine')?></th>
                    <th width="30"><?= __('Ordine')?></th>
                    <th width="250"><?= __('Activ?')?></th>
                    <th><?= __('Actiuni')?></th>
                </tr>
            </thead>
            <tbody>
                <?php foreach($banners as $k => $banner) :?>
                    <tr class="text-center">
                        <td><?= $k + 1; ?></td>
                        <td><img src="<?=$this->H->storePath('/img/gallery/' . $banner['Banner']['filename'])?>" width="853" height="100"></td>
                        <td><?= $banner['Banner']['order']; ?></td>
                        <td>
                            <?php if ($banner['Banner']['is_active']): ?>
                                <i class="fa fa-check-circle fa-lg" style="color: olivedrab;"></i>&nbsp;
                            <?php else:?>
                                <i class="fa fa-times-circle fa-lg" style="color: #ac2925;"></i>&nbsp;
                            <?php endif;?>
                            <?php if(!empty($banner['Banner']['attached_campaign_id'])):?>
                                <hr/><span class="grayed">Campanie: </span>
                                <b><?= $recent_campaigns[$banner['Banner']['attached_campaign_id']] ?></b>
                            <?php endif;?>
                        </td>
                        <td align="right" width="145">
                            <div class="btn-group btn-group-xs">
                                <?= $this->H->bslk(
                                    '/cms/banners/delete/' . $banner['Banner']['id'],
                                    'btn btn-danger btn-xs btn-outline',
                                    $this->Html->icon('fa fa-trash-o'),
                                    __('Sterge'),
                                    'top',
                                    __('Esti sigur ca vrei sa stergi bannerul?')
                                )?>
                                <?= $this->H->bslk(
                                    '/cms/banners/edit/' . $banner['Banner']['id'],
                                    'btn btn-info btn-xs btn-outline',
                                    $this->Html->icon('fa fa-pencil'),
                                    __('Modifica')
                                )?>
                            </div>
                        </td>
                    </tr>
                <?php endforeach;?>
            </tbody>
        </table>
    </div>
</div>
<!--/.row-->