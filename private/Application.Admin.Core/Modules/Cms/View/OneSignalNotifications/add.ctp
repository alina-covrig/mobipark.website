<div class="row">
	<div class="col-md-8 col-lg-offset-2">
		<?=$this->Form->create('AddMsg', ['url' => 'add']);?>
        <div class="panel panel-default">
            <div class="panel-heading">
                <h2><?=__('Adauga Mesaj')?></h2>
                <div class="form-group">
                    <label for="AddMsgTitle" class="control-label"><?=__('Titlu')?></label>
                    <p class="lead emoji-picker-container">
                        <input name="data[AddMsg][title]" class="form-control" type="text" id="AddMsgTitle" data-emojiable="true" maxlength="40"
                            value="<?php isset($this->request->data['AddMsg']['title']) ? print $this->request->data['AddMsg']['title'] : ''?>">
                    </p>
                </div>
                <div class="form-group">
                    <label for="AddMsgTitle" class="control-label"><?=__('Subtitlu')?></label>
                    <p class="lead emoji-picker-container">
                        <input name="data[AddMsg][sub-title]" class="form-control" type="text" id="AddMsgSubTitle" data-emojiable="true" maxlength="60"
                            value="<?php isset($this->request->data['AddMsg']['sub-title']) ? print $this->request->data['AddMsg']['sub-title'] : ''?>">
                    </p>
                </div>
                <div class="form-group">
                    <label for="AddMsgTitle" class="control-label"><?=__('Mesaj')?></label>
                    <p class="lead emoji-picker-container">
                        <textarea name="data[AddMsg][message]" class="form-control" cols="30" rows="6" id="AddMsgMessage" data-emojiable="true" maxlength="500">
                            <?php isset($this->request->data['AddMsg']['message']) ? print $this->request->data['AddMsg']['message'] : ''?>
                        </textarea>
                    </p>
                </div>
	            <?=$this->Form->input('selected',
		            [
			            'label'=>__('Tipuri Notificari'),
			            'type'=>'select',
			            'options' => $this->request->data['AddMsg']['types'],
			            'default' => $selected
		            ]
	            );?>
                <div class="form-group campaign-select" style="display: <?=$campaign_select_visual?>">
                    <label for="AddMsgTitle" class="control-label"><?=__('Campanii')?></label>
                    <?=$this->Form->input('campaign_select',
                        [
                            'label'=> false,
                            'type'=>'select',
                            'options' => $campaign_select,
                            'empty' => 'Selecteaza Campanii'
                        ]
                    );?>
                </div>
            </div>
            <div class="panel-body"></div>
            <div style="padding: 10px">
                <button class= "btn btn-default" type= "button" onclick= "add_new_fields()">
                    <?=__('Adauga Camp Nou')?>
                </button>
            </div>
            <div class="panel-heading">
                <h3><?=__('Programare mesaje')?></h3>
	            <?=$this->Form->input('sending_immediately',
		            [
			            'label'=>__('Trimitere Instantanee'),
			            'type'=>'radio',
			            'options' => $sending_immediately,
			            'default' => $sending_immediately_selected
		            ]
	            );?>
                <div class="programming-messages-data" style="display: <?=$programming_messages_data_display?>;">
                    <input
                        name="data[AddMsg][date]"
                        class="form-control"
                        style="width: auto; display: inline"
                        autocomplete="off"
                        value="<?=date('Y-m-d H:i:s')?>"
                        type="text"
                        id="dataAddMsgDate"
                    >
                </div>
                <h3><?=__('Segmente')?></h3>
	            <?=$this->Form->input('segment',
		            [
			            'label'=>__('Alege un segment pentru trimitere notificari'),
			            'type'=>'select',
			            'default' => $selected_segment
		            ]
	            );?>
            </div>
            <div class="panel-heading">
                <div class="row">
                    <div class="col-md-12">
                        <div style="padding: 20px">
                            <?php $msg = __('Esti sigur ca vrei sa trimiti mesajul ?');?>
                            <?=$this->Form->submit(__('Trimite Mesaj'),
                                [
                                    "onclick" => "return confirm('" . $msg . "')",
                                    "class" => "btn btn-success"
                                ]);
                            ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
		<?=$this->Form->end();?>
	</div>
</div>
<?php $this->start('extra-scripts')?>
<style>
    .additional-data-global{
        display: flex;
        justify-content: flex-start;
        padding: 5px;
    }
    .additional-data-lable{
        width: 9%;
        padding-top: 5px;
    }
    .additional-data-delete{
        width: 3%;
        padding-right: 5px;
        padding-top: 10px;
    }
    .additional-data-input{
        width: 44%;
        padding-right: 20px;
    }
</style>
<?= $this->element('Plugins/emoji')?>
<script type="text/javascript">
    let allKeys = <?=json_encode($this->request->data['AddMsg']['key'])?>;
    let allValues = <?=json_encode($this->request->data['AddMsg']['value'])?>;

    $( "#dataAddMsgDate" ).datetimepicker({
        numberOfMonths: 2,
        dayOfWeekStart: 1,
        minDate: new Date(),
        lang:'ro'
    })

    $(document).ready(function() {
        add_aditional_data();

        $( "#AddMsgSelected" ).change(function() {
            add_aditional_data();
        });
        $(document).on('click', '#AddMsgSendingImmediatelyParticularTime', function(){
            $('.programming-messages-data').show();
        });
        $(document).on('click', '#AddMsgSendingImmediatelyImmediately', function(){
            $('.programming-messages-data').hide();
        });
        $(document).on('click', '#AddMsgSelected', function(){
            let valueSelect = $('#AddMsgSelected option:selected').val();

            if(valueSelect == 2){
                $('.campaign-select').show();
                set_campaingn();
            } else {
                $('.campaign-select').hide();
            }
        });
        $(document).on('click', '#AddMsgCampaignSelect', function(){
            set_campaingn();
        });
    });
    function set_campaingn(){
        let campaignVal = $('#AddMsgCampaignSelect option:selected').val();
        let campaignText = $('#AddMsgCampaignSelect option:selected').text();

        if(campaignVal != ''){
            $('#dataAddMsgValue_2_1').val('<?=Configure::read('STORE_URL')?>campanii/' + campaignVal);
            $('#dataAddMsgValue_2_2').val(campaignText);
        }
    }

    function additional_data_delete(key){
        let selectedAditionalData = $('#AddMsgSelected').val();
        Object.keys(allKeys).forEach(function (additionalData) {
            if(additionalData == selectedAditionalData){
                Object.keys(allKeys[selectedAditionalData]).forEach(function (additionalDataKey) {
                    console.log(additionalDataKey);
                    allKeys[additionalData][additionalDataKey] = $('#dataAddMsgKey_' + selectedAditionalData + '_'+ additionalDataKey).val();
                    allValues[additionalData][additionalDataKey] = $('#dataAddMsgValue_' + selectedAditionalData + '_'+ additionalDataKey).val();
                })
            }
		})


        delete allKeys[selectedAditionalData][key];
        delete allValues[selectedAditionalData][key];

        add_aditional_data();
    }

    function add_aditional_data(){
        let selectedAditionalData = $('#AddMsgSelected').val();
        let htmlAditionalData = '';
        let counting = 0;

        allKeys[selectedAditionalData].forEach(
            function logArrayElements (element, index){
                counting ++;
                htmlAditionalData += return_html(
                    index,
                    counting,
                    element,
                    selectedAditionalData);

            }
        );

        $('.panel-body').html(htmlAditionalData);
        counting = $('.additional-data-global').length;

        if(counting == 1){
            $( ".additional-data-delete" ).empty();
        }
    }

    function return_html(index, counting, element, selectedAditionalData){
        let returning = '' +
        '<div class="additional-data-global additional-data-' + index + '">' +
            '<div class="additional-data-lable">' +
                '<b><?=__('Camp')?> ' + counting + '</b>' +
            '</div>' +
            '<div class="additional-data-delete">' +
                '<i class="fa fa-times-circle fa-lg" style="color: #ac2925;" onclick="additional_data_delete(\'' + index + '\')"></i>' +
            '</div>' +
            '<div class="input-group additional-data-input">' +
                '<span class="input-group-addon"><?=__('Cheie')?></span>' +
                '<input name="data[AddMsg][key_][' + selectedAditionalData + '][' + index + ']" class="form-control" value="' + element + '" type="text" id="dataAddMsgKey_' + selectedAditionalData + '_' + index + '" >' +
            '</div>' +
            '<div class="input-group additional-data-input">' +
                '<span class="input-group-addon"><?=__('Valoare')?></span>' +
            '<input name="data[AddMsg][value_][' + selectedAditionalData + '][' + index + ']" class="form-control" value="' + allValues[selectedAditionalData][index] + '" type="text" id="dataAddMsgValue_' + selectedAditionalData + '_' + index + '" >' +
            '</div>' +
        '</div>';
        return returning;
    }

    function add_new_fields(){
        let index = $('.additional-data-global').length;
        let counting = index + 1;
        let element = '';
        let selectedAditionalData = $('#AddMsgSelected').val();
        allKeys[selectedAditionalData][index] = '';
        allValues[selectedAditionalData][index] = '';

        let htmlAditionalData = return_html(
            index,
            counting,
            element,
            selectedAditionalData);

        $('.panel-body').append(htmlAditionalData);

        if(index == 1){
            $('.additional-data-delete').first().after().append(
                   '<i class="fa fa-times-circle fa-lg" style="color: #ac2925;" onclick="additional_data_delete(\'0\')"></i>'
                );
        }
    }
</script>
<?php $this->end()?>