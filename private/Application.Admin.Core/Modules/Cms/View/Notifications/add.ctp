
<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header"><?php echo __("Adauga notificari")?></h1>
    </div>
    <!-- /.col-lg-12 -->
</div>
<!-- /.row -->

<div class="row">
    <div class="col-lg-12">
        <?php echo $this->Form->create('Notification');?>
        <?php echo $this->Form->input('user_type_id', array('empty' => 'Tip utilizator', 'required' => true))?>
        <?php echo $this->Form->input('subject', array('label' => __('Subiect'), 'required' => true))?>
        <?php echo $this->Form->input('message', array('label' => __('Mesaj'), 'type' => 'textarea', 'required' => true))?>
        <?php echo $this->Form->input('products', array('label' => __('ID Produse <small>(Cate un produs pe linie noua)</small>'), 'type' => 'textarea'))?>
        <?php echo $this->Form->submit(__('Adauga pagina'), array('class' => "btn btn-success btn-lg btn-block"));?>
        <?php echo $this->Form->end();?>
    </div>
    <!-- /.col-lg-12 -->
</div>
<!-- /.row -->