<?php
/**
* Inside this Controller admin logic methods will
* be placed
*
* PHP version 5.4
*
* @category  Admin
* @package   Cms
* @author    Badeci Gabriel Lucian <lucian.badeci@gmail.com>
* @copyright 2015 Evercloud
* @license   http://www.php.net/license/3_01.txt  PHP License 3.01
* @version   SVN: $Id$
* @link      /Cms/Blocks
* @since     1.0
*/

App::uses('Cms.CmsAppController', 'Controller');

/**
* ThemesController Class
*
* Inside this Controller admin logic methods will
* be placed
*
* @category  Admin
* @package   Cms
* @author    Badeci Gabriel Lucian <lucian.badeci@gmail.com>
* @copyright 2015 Evercloud
* @license   http://www.php.net/license/3_01.txt  PHP License 3.01
* @version   Release: 1.0
* @link      /Cms/Blocks
* @since     Class available since Release 1.0
*/
class ThemesController extends CmsAppController
{

    /**
    * The index method
    * 
    * This method gets the Themes tree and
    * sets it to the view
    * 
    * @return void
    */
    public function index()
    {
        $themes = $this->Theme->find('all');
        $this->set(compact('themes'));
    }
    
    /**
    * The Themes add method
    * 
    * Themes are addedd through this method.
    * If there's no POST then this method will
    * display /modules/cms/views/Themes/add.ctp
    * If there's a Theme entity in POST then validation
    * and insertion will occur
    * 
    * @return void
    */
    public function add() 
    {
        if ($this->request->is('post')) {
            $name = $this->request->data['Theme']['name'];
            $find = $this->Theme->findByName($name);
            if (!empty($find)) {
                $this->Session->setFlash(
                    __('Tema exista in baza de date!'),
                    'Alerts/warning'
                );
                return $this->redirect('/cms/themes');
            }

            if ($this->Theme->save($this->request->data)) {
                $this->Session->setFlash(
                    __('Tema a fost adaugat!'),
                    'Alerts/success'
                );
                return $this->redirect('/cms/themes');
            }
        }
    }
    
    /**
    * The Themes edit method
    * 
    * Themes can be edited and modified as wished
    * The name, description, parent_id and image
    * 
    * @param integer $id the Themes id to be edited
    * 
    * @return void
    */
    public function edit($id)
    {
        $this->Theme->id = $id;
        if (!$this->Theme->exists()) {
            $this->Session->setFlash(
                __('Tema nu exista!'),
                'Alerts/error'
            );
            return $this->redirect($this->request->referer());
        }
        if ($this->request->is('put')) {
            if ($this->Theme->save($this->request->data)) {
                $this->Session->setFlash(
                    __('Tema a fost modificata!'),
                    'Alerts/success'
                );
                return $this->redirect($this->request->referer());
            } else {
                //pr($this->Theme->validationErrors);die();
            }
        }

        $theme = $this->Theme->findById($id);
        $this->request->data = $theme;
    }
    
    /**
    * Remove Themes method
    * 
    * A Themes cannot be removed if it has products,
    * characteristics
    * 
    * @param int $id the Block id to be deleted
    * 
    * @return redirect to Blocks
    */
    public function delete(int $id)
    {
        $this->Theme->id = $id;
        if (!$this->Theme->exists()) {
            $this->Session->setFlash(__('Tema inexistenta'), 'Alerts/warning');
            return $this->redirect('/cms/themes');
        }
        if ($this->Theme->delete()) {
            $this->Session->setFlash(
                __('Tema a fost stearsa'),
                'Alerts/success'
            );
        } else {
            $this->Session->setFlash(
                __('Eroare la stergerea Temei'),
                'Alerts/error'
            );
        }
        return $this->redirect('/cms/themes');
    }

    /**
     * Preview Themes method
     *
     * @param int $id     theme id for preview
     * @param int $method theme id for preview
     *
     * @return redirect to Themes
     */
    public function preview($id, $method)
    {
        $this->Theme->id = $id;
        if (!$this->Theme->exists()) {
            $this->Session->setFlash(__('Tema inexistenta'), 'Alerts/warning');
            return $this->redirect('/cms/themes');
        }

        $this->Theme->validate = array();
        $this->Theme->updateAll(
            array('preview' => false)
        );
        $this->Theme->id = $id;
        $this->Theme->set(
            array(
                'preview' => $method
            )
        );
        $this->Theme->validate = array();
        if ($this->Theme->save()) {
            $this->Session->setFlash(
                __('Tema a fost activata pentru preview'),
                'Alerts/success'
            );
        } else {
            $this->Session->setFlash(
                __('Eroare la activarea temei pentru preview'),
                'Alerts/error'
            );
        }
        return $this->redirect('/cms/themes');
    }

    /**
     * Activate Themes method
     *
     * @param int $id     the theme to pe modified
     * @param int $method the action to be applied
     *
     * @return redirect to Blocks
     */
    public function turn($id, $method)
    {
        $this->Theme->id = $id;
        if (!$this->Theme->exists()) {
            $this->Session->setFlash(__('Tema inexistenta'), 'Alerts/warning');
            return $this->redirect('/cms/themes');
        }

        $this->Theme->validate = array();
        $this->Theme->updateAll(
            array('active' => false)
        );
        $this->Theme->id = $id;
        $this->Theme->set(
            array(
            'active' => $method
            )
        );
        $this->Theme->validate = array();
        if ($this->Theme->save()) {
            $this->Session->setFlash(
                __('Tema a fost modificata'),
                'Alerts/success'
            );
        } else {
            $this->Session->setFlash(
                __('Eroare la modificarea Temei'),
                'Alerts/error'
            );
        }
        return $this->redirect('/cms/themes');
    }
}