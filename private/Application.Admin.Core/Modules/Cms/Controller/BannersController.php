<?php
/**
* Inside this Controller admin logic methods will
* be placed
*
* PHP version 5.4
*
* @category  Admin
* @package   Cms
* @author    Daniel Ionascu <daniel.ionascu88@gmail.com>
* @copyright 2014 Evercloud
* @license   http://www.php.net/license/3_01.txt  PHP License 3.01
* @version   SVN: $Id$
* @link      /Cms/banners
* @since     1.0
*/

App::uses('Cms.CmsAppController', 'Controller');

/**
* PagesController Class
*
* Inside this Controller admin logic methods will
* be placed
*
* @category  Admin
* @package   Cms
* @author    Daniel Ionascu <daniel.ionascu88@gmail.com>
* @copyright 2014 Evercloud
* @license   http://www.php.net/license/3_01.txt  PHP License 3.01
* @version   Release: 1.0
* @link      /Cms/banners
* @since     Class available since Release 1.0
*/
class BannersController extends CmsAppController
{

   /* public $uses = [
       'Cms.Banner',
        'Loyalty.Campaign'
    ];*/
    /**
    * The index method
    * 
    * This method gets the pages tree and
    * sets it to the view
    * 
    * @return void
    */
    public function index()
    {
        $banners = $this->Banner->find(
            'all', [
            'order' => [
                    'is_little' => 'ASC',
                    'order' => 'ASC'
                ]
            ]
        );


        $this->set(compact('banners'));
    }
    
    /**
    * The banner add method
    * 
    * Banners are addedd through this method.
    * If there's no POST then this method will
    * display /modules/cms/views/banners/add.ctp
    * If there's a category entity in POST then validation
    * and insertion will occur
    * 
    * @return mixed
    */
    public function add() 
    {

        if ($this->request->is('post')) {
            if (!empty($this->request->data['Banner']['image']['name'])) {
                $fn = $this->Banner->uploadBanner($this->request->data['Banner']['image']);
                $this->request->data['Banner']['filename'] = $fn;
            }

            if ($this->Banner->save($this->request->data)) {
                return $this->respond(
                    __('Bannerul a fost adaugat cu succes!'),
                    'success',
                    '/cms/banners'
                );
            } else {
                return $this->respond(
                    __('Bannerul nu a putut fi adaugat!'),
                    'error',
                    '/cms/banners'
                );
            }
        }
    }
    
    /**
    * The banner edit method
    * 
    * @param integer $banner_id the banner id to be edited
    * 
    * @return mixed
    */
    public function edit($banner_id) 
    {

        if ($this->request->is('put')) {
            if (!empty($this->request->data['Banner']['image']['name'])) {
                $fn = $this->Banner->uploadBanner($this->request->data['Banner']['image']);
                $this->request->data['Banner']['filename'] = $fn;
            }


            if ($this->Banner->save($this->request->data)) {
                return $this->respond(
                    __('Bannerul a fost modificat!'), 
                    'Alerts/success',
                    '/cms/banners'
                );

            }
        }


        $this->Banner->id = $banner_id;

        if (!$this->Banner->exists()) {
            return $this->respond(
                __('Bannerul nu exista in baza de date'),
                'error',
                '/cms/banners'
            );
        }
        $options = array(
            'conditions' => array(
                'Banner.id' => $banner_id,
            ),
        );
        $this->request->data = $this->Banner->find('first', $options);
    }
    
    /**
    * Remove banner method
    * 
    * @param int $banner_id the banner id to be deleted
    * 
    * @return redirect to banners
    */
    public function delete($banner_id)
    {
        $this->Banner->id = $banner_id;
        if (!$this->Banner->exists()) {
            $this->Session->setFlash(__('Banner inexistent'), 'Alerts/warning');
            return $this->redirect('/cms/banners');
        }
        $banner = $this->Banner->findById($banner_id);
        $path_image = WWW_ROOT
            . 'img' 
            . DS 
            . 'roller' 
            . DS
            . $banner['Banner']['filename'];
        $file = new File($path_image);
        $file->delete();


        if ($this->Banner->delete()) {
            $this->Session->setFlash(
                __('Bannerul a fost sters'), 
                'Alerts/success'
            );
        } else {
            $this->Session->setFlash(
                __('Eroare la stergerea bannerului'), 
                'Alerts/error'
            );
        }
        
        return $this->redirect('/cms/banners');
    }
}
