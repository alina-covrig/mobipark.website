<?php
/**
 * Inside this Controller admin logic methods will
 * be placed
 *
 * PHP version 5.4
 *
 * @category  Admin
 * @package   Cms
 * @author    Daniel Ionascu <daniel.ionascu88@gmail.com>
 * @copyright 2014 Evercloud
 * @license   http://www.php.net/license/3_01.txt  PHP License 3.01
 * @version   SVN: $Id$
 * @link      /Cms/announcements
 * @since     1.0
 */

App::uses('Cms.CmsAppController', 'Controller');

/**
 * PagesController Class
 *
 * Inside this Controller admin logic methods will
 * be placed
 *
 * @category  Admin
 * @package   Cms
 * @author    Daniel Ionascu <daniel.ionascu88@gmail.com>
 * @copyright 2014 Evercloud
 * @license   http://www.php.net/license/3_01.txt  PHP License 3.01
 * @version   Release: 1.0
 * @link      /Cms/announcements
 * @since     Class available since Release 1.0
 */
class AnnouncementsController extends CmsAppController
{


    /**
     * The index method
     *
     * This method gets the pages tree and
     * sets it to the view
     *
     * @return void
     */
    public function index()
    {
        $announcements = $this->Announcement->find(
            'all', array(
                'order' => array(
                    'from'  => 'ASC',
                    'order' => 'ASC'
                )
            )
        );
        $this->set(compact('announcements'));
    }

    /**
     * The announcement add method
     *
     * Announcements are addedd through this method.
     *
     * @return void
     */
    public function add()
    {
        if ($this->request->is('post')) {
            if (empty ($this->request->data['Announcement']['order'])) {
                $this->request->data['Announcement']['order'] = 0;
            }
            $this->request->data['Announcement']['message']
                = strip_tags(
                    $this->request->data['Announcement']['message'],
                    '<p><a><br><b><i><u>'
                );
            if ($this->Announcement->save($this->request->data)) {
                $this->Session->setFlash(
                    __('Anuntul a fost adaugat!'),
                    'Alerts/success'
                );
            } else {
                $this->Session->setFlash(
                    __('Announcement nu a putut fi adaugat!'),
                    'Alerts/warnings'
                );
            }
            return $this->redirect('/cms/announcements');
        }
    }

    /**
     * The announcement edit method
     *
     * @param integer $announcement_id the announcement id to be edited
     *
     * @return void
     */
    public function edit($announcement_id)
    {
        if ($this->request->is('put')) {
            if (empty ($this->request->data['Announcement']['order'])) {
                $this->request->data['Announcement']['order'] = 0;
            }
            $this->request->data['Announcement']['message']
                = strip_tags(
                $this->request->data['Announcement']['message'],
                '<p><a><br><b><i><u>'
            );
            if ($this->Announcement->save($this->request->data)) {
                $this->Session->setFlash(
                    __('Anuntul a fost modificat!'),
                    'Alerts/success'
                );
                return $this->redirect('/cms/announcements');
            }
        }
        $this->Announcement->id = $announcement_id;

        if (!$this->Announcement->exists()) {
            $this->Session->setFlash(
                __('Anuntul nu exista in baza de date'),
                'Alerts/warning'
            );
            return $this->redirect('/cms/announcements');
        }

        $options = array(
            'conditions' => array (
                'Announcement.id' => $announcement_id,
            ),
        );
        $this->request->data = $this->Announcement->find('first', $options);
    }

    /**
     * Remove announcement method
     *
     * @param int $announcement_id the announcement id to be deleted
     *
     * @return redirect to $announcement
     */
    public function delete (int $announcement_id)
    {
        $this->Announcement->id = $announcement_id;
        if (!$this->Announcement->exists()) {
            $this->Session->setFlash(__('Anunt inexistent'), 'Alerts/warning');
            return $this->redirect('/cms/announcements');
        }

        if ($this->Announcement->delete()) {
            $this->Session->setFlash(
                __('Anuntul a fost sters'),
                'Alerts/success'
            );
        } else {
            $this->Session->setFlash(
                __('Eroare la stergerea anuntului'),
                'Alerts/error'
            );
        }

        return $this->redirect('/cms/announcements');
    }
}
