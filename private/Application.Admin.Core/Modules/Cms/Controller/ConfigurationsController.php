<?php
/**
* Inside this Controller admin logic methods will
* be placed
*
* PHP version 5.4
*
* @category  Admin
* @package   Cms
* @author    Catalin Prodan <clinpan@gmail.com>
* @copyright 2014 Evercloud
* @license   http://www.php.net/license/3_01.txt  PHP License 3.01
* @version   SVN: $Id$
* @link      /Cms/pages
* @since     1.0
*/

App::uses('Cms.CmsAppController', 'Controller');

/**
* ConfigurationController Class
*
* Inside this Controller admin logic methods will
* be placed
*
* @category  Admin
* @package   Cms
* @author    Catalin Prodan <clinpan@gmail.com>
* @copyright 2014 Evercloud
* @license   http://www.php.net/license/3_01.txt  PHP License 3.01
* @version   Release: 1.0
* @link      /Cms/pages
* @since     Class available since Release 1.0
*/
class ConfigurationsController extends CmsAppController
{
    public $helpers = array(
    );
    
    // {{{ properties
    /**
     * The models the product reviews controller uses
     *
     * @var $uses
     */
     public $uses = array(
         'Configuration',
    );

    // }}}

    /**
    * The index method
    * 
    * @return void
    */
    public function index()
    {
        if ($this->request->is('post')) {
            if (empty($this->request->data)) {
                return $this->respond(
                    __('Date inconsistente'),
                    'error'
                );
            }

            $group = $this->request->data['Configuration']['group'];
            $subgroup = $this->request->data['Configuration']['subgroup'];

            unset($this->request->data['Configuration']['group']);
            unset($this->request->data['Configuration']['subgroup']);

            foreach ($this->request->data['Configuration'] as $id => $value) {
                $save[] = [
                    'id' => $id,
                    'value' => $value
                ];
            }

            if (!$this->Configuration->saveMany($save)) {
                return $this->respond(
                    __('A aparut o eroare la salvarea datelor'),
                    'error'
                );
            }

            return $this->respond(
                __('Configurarile au fost salvate cu succes'),
                'success',
                '/cms/configurations?group='
                . $group
                . '&subgroup='
                . $subgroup
            );
        }

        $configurations = $this->Configuration->find(
            'all', [
                'conditions' => [
                    'name <>' => ''
                ],
                'group' => [
                    'group_name',
                    'subgroup',
                    'name'
                ]
            ]
        );

        $groups = [];
        foreach ($configurations as $c) {
            $groups
            [$c['Configuration']['group_name']]
            [$c['Configuration']['subgroup']]
            [] = $c['Configuration'];
        }

        $this->set(compact('groups'));
    }

    /**
     * The edit method
     *
     * @param int $id the if of the comment
     *
     * @return void
     */
    public function edit($id)
    {
        $configuration = $this->Configuration->findById($id);
        if (!$configuration) {
            $this->Session->setFlash(
                __('Optiunea de configurare nu exista!'),
                'Alerts/error'
            );
        } elseif ($this->request->is('put')) {
            $this->Configuration->id = $id;
            if ($this->Configuration->save($this->request->data['Configuration'])) {
                $configuration = $this->Configuration->findById($id);
                $this->Session->setFlash(
                    __('Optiunea de configurare a fost salvata!'),
                    'Alerts/success'
                );
            } else {
                $this->Session->setFlash(
                    __('Optiunea de configurare nu a putut fi salvata!'),
                    'Alerts/error'
                );
            }
        }
    
        $this->request->data = $configuration;
        $this->set('configuration', $configuration);
    }
    
    /**
     * The updateValue method
     * This method updates the value of configuration option via ajax
     * @param int    $configuration_id     the id of the Configuration
     * @return boolean
     */
    public function updateValue($configuration_id) 
    {
        $this->layout = false;
        $this->autoRender = false;
        $this->Configuration->id = $configuration_id;
        if ($this->Configuration->saveField(
            'value',
            $this->request->data['value']
        )) {
                return true;
        }
        $this->response->body(__('Eroare la salvarea datelor'));
        $this->response->statusCode(403);
        return false;
    }
}
