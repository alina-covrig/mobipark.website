<?php
/**
 * Created by PhpStorm.
 * User: Virgiliu
 * Date: 2/5/2020
 * Time: 11:45 AM
 */
class OneSignalNotificationsController extends CmsAppController
{
    public $uses = [
    'Cms.OneSignalNotification',
    'Loyalty.Campaign',
    ];

    public function add()
    {
        $selected = 1; //Tipuri Notificari
        $selected_segment = 5; //lista de testare
        $campaign_select_visual = 'none';

        $this->Campaign->Behaviors->load('Containable');
        $this->Campaign->contain();

        $campaign = $this->Campaign->find(
            'all',
            [
            'limit' => 30,
            'order' => ['Campaign.id' => 'DESC'],
            'fields' => ['id', 'name', 'from', 'to']
            ]
        );
        $campaign_select = [];
        foreach($campaign as $value ){
            $campaign_select[$value['Campaign']['id']] =
             $value['Campaign']['name'];
        }

        $segments = $this->OneSignalNotification->getSegments();

        $this->request->data['AddMsg']['types'] =
         $this->OneSignalNotification->getTypes;

        $this->request->data['AddMsg']['key'] =
         $this->OneSignalNotification->getKey;

        $this->request->data['AddMsg']['value'] =
         $this->OneSignalNotification->getValue();

        $sending_immediately = [
         'immediately' => __('Trimitere Imediata'),
         'particular_time' => __('Selecteaza Data Si Ora Alertei')
        ];

        $sending_immediately_selected = 'immediately';
        $programming_messages_data_display = 'none';

        if ($this->request->is('post')) {
            $data = [];
            $err_msg = '';

            $selected = $this->request->data['AddMsg']['selected'];

            if($this->request->data['AddMsg']['title'] == '') {
                $err_msg .=
	                $this->_errMsg(__('Titlu'));
            }

            if($this->request->data['AddMsg']['sub-title'] == '') {
                $err_msg .=
	                $this->_errMsg(__('Subtitlu'));
            }

            $this->request->data['AddMsg']['message'] =
             ltrim($this->request->data['AddMsg']['message'], ' ');

            if($this->request->data['AddMsg']['message'] == '') {
                $err_msg .=
	                $this->_errMsg(__('Mesaj'));
            }

            if($this->request->data['AddMsg']['selected'] == '2' 
                && $this->request->data['AddMsg']['campaign_select'] == ''
            ) {
                $err_msg .=
	                $this->_errMsg(__('Campanii'));
            }

            if(isset($this->request->data['AddMsg']['key_'])) {

                foreach($this->request->data['AddMsg']['key_'] as
                     $key_notification_type => $value_notification_type ) {

                    unset(
                        $this->request->data
                        ['AddMsg']['key'][$key_notification_type]
                    );

                    foreach($value_notification_type as $key => $value ) {
                        if($value == '') {
                            $err_msg .= $this ->_errMsg(
                                __('Cheie Camp ') .
                                ($key + 1)
                            );
                        }

                        $data[$value] =
                         $this->request
	                         ->data['AddMsg']['value_'][$key_notification_type][$key];

                        $this->request
                            ->data['AddMsg']['key'][$key_notification_type][] =
                         $value;
                    }

                }

                foreach($this->request->data['AddMsg']['value_'] as
                     $key_notification_type => $value_notification_type ){

                    unset(
                        $this->request->data
                        ['AddMsg']['value'][$key_notification_type]
                    );

                    foreach($value_notification_type as $key_value => $value ) {
                        if($value == '') {
                            $err_msg .= $this ->_errMsg(
                                __('Valuare Camp ') .
                                ($key_value + 1)
                            );
                        }

                        if($key_value == 1 
                            && $value == Configure::read('STORE_URL') .
	                        'model-link'
                            && $key_notification_type > 2
                        ) {
                            $err_msg .= $this ->_errMsg(
                                __('Valoare Camp ') .
                                ($key_value + 1) .
                                __(' modificati linkul predefinit')
                            );
                        }

                        $this->request->data
                        ['AddMsg']['value'][$key_notification_type][] = $value;

                    }
                }
            }

            if($err_msg != '') {
                $err_msg = __('Capuri Obligatorii :') .
                 '<br/>' .
                 substr($err_msg, 0, -2).'.';

                $this->Session->setFlash(
                    $err_msg,
                    'Alerts/error'
                );

            }

	        $send_after = false;
            if($this->request->data['AddMsg']['sending_immediately'] ==
	            'particular_time'
            ) {
                $programming_messages_data_display = '';
	            $send_after =
		            date(
			            'Y-m-d H:i',
			            strtotime(
				            '-2 hours',
				            strtotime($this->request->data['AddMsg']['date'])
			            )
		            );
            }

            if($err_msg == '') {

                $response = $this->OneSignalNotification->pushNotification(
                    [
	                    [
	                    'lang' => 'ro',
	                    'title' => $this->request->data['AddMsg']['title']
	                    ],
                        [
                            'lang' => 'en',
                            'title' => $this->request->data['AddMsg']['title']
                        ],
                    ],
                    [
                        [
                            'lang' => 'ro',
                            'content' =>
	                            $this->request->data['AddMsg']['message']
                        ],
                        [
                            'lang' => 'en',
                            'content' =>
	                            $this->request->data['AddMsg']['message']
                        ]
                    ],
                    [], // buttons with individual links
                    [$segments[$this->request->data['AddMsg']['segment']]],
	                [], // no players_ids because we use segments
                    $data,
                    $send_after,
                    [
                    	'en' => $this->request->data['AddMsg']['sub-title'],
	                    'ro' => $this->request->data['AddMsg']['sub-title']
	                ]
                );
                $response = json_decode($response);
                if (isset($response->errors)) {
                    foreach ($response->errors as $err_value) {
                        $err_msg .=
	                        $this->_errMsg($err_value);
                    }
                          $err_msg = __('Eroare One Signal: ') .
                           '<br/>' .
                           substr($err_msg, 0, -2) . '.';

                          $this->Session->setFlash(
                              $err_msg,
                              'Alerts/error'
                          );
                }
                if($err_msg == '') {
	                $this->Session->setFlash(
		                __('Mesaj Salvat cu Succes '),
		                'Alerts/success'
	                );
                }
            }

        }

        if($selected == 2) {
            $campaign_select_visual = '';
        }

        $this->set(
            compact(
                'campaign_select',
                'campaign_select_visual',
                'selected',
                'sending_immediately',
                'sending_immediately_selected',
                'programming_messages_data_display',
                'segments',
                'selected_segment',
                $this->request->data
            )
        );
    }

	private function _errMsg($text)
	{
		return sprintf("<br/> \"<i></b> %s </i></b>\" ; ", $text);

	}
}
