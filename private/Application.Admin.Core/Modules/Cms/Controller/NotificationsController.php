<?php
/**
 * Inside this Controller admin logic methods will
 * be placed
 *
 * PHP version 5.4
 *
 * @category  Admin
 * @package   Cms
 * @author    Daniel Ionascu <daniel.ionascu88@gmail.com>
 * @copyright 2014 Evercloud
 * @license   http://www.php.net/license/3_01.txt  PHP License 3.01
 * @version   SVN: $Id$
 * @link      /Cms/notifications
 * @since     1.0
 */

App::uses('Cms.CmsAppController', 'Controller');

/**
 * PagesController Class
 *
 * Inside this Controller admin logic methods will
 * be placed
 *
 * @category  Admin
 * @package   Cms
 * @author    Daniel Ionascu <daniel.ionascu88@gmail.com>
 * @copyright 2014 Evercloud
 * @license   http://www.php.net/license/3_01.txt  PHP License 3.01
 * @version   Release: 1.0
 * @link      /Cms/notifications
 * @since     Class available since Release 1.0
 */
class NotificationsController extends CmsAppController
{

    public $uses = [
        'Cms.Notification',
        'Customers.UserType',
        'Catalog.Product',
        'Catalog.ProductAliasName'
    ];

    /**
     * The add method
     *
     * This method add a notification to a user type
     *
     * @return void
     */
    public function add()
    {
        if ($this->request->is('post')) {

            set_time_limit(3600);
            ini_set('memory_limit', '2048M');

            if (!empty($this->request->data['Notification']['products'])) {
                $products_ids = explode(
                    PHP_EOL,
                    $this->request->data['Notification']['products']
                );

                $this->request->data['Notification']['products']
                    = json_encode($products_ids);
            }

            $users = $this->User->find(
                'list', [
                    'conditions' => [
                        'user_type_id' => $this->request->data['Notification']['user_type_id']
                    ],
                    'fields' => ['id']
                ]
            );

            $data = $this->request->data['Notification'];

            $transaction = $this->Notification->getDataSource();
            $transaction->begin();

            foreach ($users as $u) {
                $notification = [];
                $notification['user_id'] = $u;
                $notification['subject'] = $data['subject'];
                $notification['message'] = $data['message'];
                $notification['is_deleted'] = false;
                $notification['is_viewed']  = false;
                $notification['created_at'] = date('Y-m-d H:i:s');
                $notification['products']  = $data['products'];

                $this->Notification->create();
                if (!$this->Notification->save($notification)) {
                    $transaction->rollback();
                    return $this->respond(
                        __('A aparut o eroare la salvarea notificarilor. '
                        . 'Va rugam incercati din nou'),
                        'error'
                    );
                }
            }

            $transaction->commit();
            return $this->respond(
                __('Notificarile au fost salvate cu success'),
                'success'
            );
        }

        $userTypes = $this->UserType->find(
            'list', [
                'order' => 'points_from ASC'
            ]
        );

        $this->set(compact('userTypes'));
    }

}
