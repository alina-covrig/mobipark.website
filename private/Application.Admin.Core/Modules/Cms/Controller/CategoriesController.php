<?php
/**
 * Inside this Controller admin logic methods will
 * be placed
 *
 * PHP version 7.1.10
 *
 * @category  Admin
 * @package   Cms
 * @author    Valentin Ioan <valentin.ioan@gmail.com>
 * @copyright 2020 Evercloud
 * @license   http://www.php.net/license/3_01.txt  PHP License 3.01
 * @version   SVN: $Id$
 * @link      /cms/categories
 * @since     1.0
 */

App::uses('Cms.CmsAppController', 'Controller');

/**
 * ArticlesController Class
 *
 * Inside this Controller admin logic methods will
 * be placed
 *
 * @category  Admin
 * @package   Cms
 * @author    Valentin Ioan <valentin.ioan@gmail.com>
 * @copyright 2020 Evercloud
 * @license   http://www.php.net/license/3_01.txt  PHP License 3.01
 * @version   Release: 1.0
 * @link      /cms/categories
 * @since     Class available since Release 1.0
 * @property  Category Category
 */
class CategoriesController extends CmsAppController
{
    public $uses = [
        'Cms.Category'
    ];

    /**
     * Categories listing
     */
    public function index()
    {
        $categories = $this->Category->find(
            'all',
            [
                'order' => 'Category.lft'
            ]
        );
        $this->set(
            compact(
                'categories'
            )
        );
    }

    /**
     * Add a category
     *
     * @return bool|mixed
     */
    public function add()
    {
        $parent_categories = $this->Category->generateTreeList(null, '{n}.Category.id', null, '---');
        $this->set('parents', $parent_categories);

        if ($this->request->is('post')) {
            try {
                if ($this->Category->save($this->request->data)) {
                    return $this->respond(
                        __('Categoria a fost adaugata cu succes'),
                        'success',
                        Router::url(['action' => 'index'])
                    );
                }
            } catch (Exception $e) {
                return $this->respond(
                    __('Eroare la adaugarea categoriei: %s', $e->getMessage()),
                    'error'
                );
            }
        }
        return true;
    }

    /**
     * Edit a category
     *
     * @param int $category_id - category id
     *
     * @return bool|mixed
     */
    public function edit($category_id)
    {
        if (empty($category_id)) {
            return $this->respond(
                __('Categorie nespecificata'),
                'error'
            );
        }

        $category = $this->Category->findById($category_id);
        if (empty($category)) {
            return $this->respond(
                __('Categorie inexistenta'),
                'error'
            );
        }

        $parent_categories = $this->Category->generateTreeList(null, '{n}.Category.id', null, '---');
        $this->set('parents', $parent_categories);

        if ($this->request->is(['post', 'put'])) {
            try {
                if ($this->Category->save($this->request->data)) {
                    return $this->respond(
                        __('Categoria a fost modificata cu succes'),
                        'success',
                        Router::url(['action' => 'index'])
                    );
                }
            } catch (Exception $e) {
                return $this->respond(
                    __('Eroare la modificarea categoriei: %s', $e->getMessage()),
                    'error'
                );
            }
        } else {
            $this->request->data = $category;
        }
        return true;
    }

    /**
     * Delete a category
     *
     * @param int $category_id - category id
     *
     * @return mixed
     */
    public function delete($category_id)
    {
        if (empty($category_id)) {
            return $this->respond(
                __('Categorie nespecificata'),
                'error'
            );
        }

        $category = $this->Category->findById($category_id);
        if (empty($category)) {
            return $this->respond(
                __('Categorie inexistenta'),
                'error'
            );
        }

        if (!$this->Category->isLeaf($category_id)) {
            return $this->respond(
                __('Categoria nu poate fi stearsa deoarece are subcategorii'),
                'error'
            );
        }

        $category_articles = $this->Category->getArticles($category_id);
        if (!empty($category_articles)) {
            return $this->respond(
                __('Categoria nu poate fi stearsa deoarece are articole'),
                'error'
            );
        }

        if (!$this->Category->delete($category_id)) {
            return $this->respond(
                __('Eroare la stergerea categoriei'),
                'error'
            );
        }

        return $this->respond(
            __('Categoria a fost stearsa cu succes'),
            'success'
        );
    }

    public function moveUp($category_id)
    {
        if (empty($category_id)) {
            return $this->respond(
                __('Categorie nespecificata'),
                'error'
            );
        }

        $category = $this->Category->findById($category_id);
        if (empty($category)) {
            return $this->respond(
                __('Categorie inexistenta'),
                'error'
            );
        }

        $this->Category->moveUp($category_id, 1);
        return $this->respond(
            __('Categorie mutata'),
            'success'
        );
    }

    public function moveDown($category_id)
    {
        if (empty($category_id)) {
            return $this->respond(
                __('Categorie nespecificata'),
                'error'
            );
        }

        $category = $this->Category->findById($category_id);
        if (empty($category)) {
            return $this->respond(
                __('Categorie inexistenta'),
                'error'
            );
        }

        $this->Category->moveDown($category_id, 1);
        return $this->respond(
            __('Categorie mutata'),
            'success'
        );
    }
}