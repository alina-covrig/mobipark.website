<?php
/**
* Inside this Controller admin logic methods will
* be placed
*
* PHP version 5.4
*
* @category  Admin
* @package   Cms
* @author    Badeci Gabriel Lucian <lucian.badeci@gmail.com>
* @copyright 2015 Evercloud
* @license   http://www.php.net/license/3_01.txt  PHP License 3.01
* @version   SVN: $Id$
* @link      /Cms/Blocks
* @since     1.0
*/

App::uses('Cms.CmsAppController', 'Controller');

/**
* BlocksController Class
*
* Inside this Controller admin logic methods will
* be placed
*
* @category  Admin
* @package   Cms
* @author    Badeci Gabriel Lucian <lucian.badeci@gmail.com>
* @copyright 2015 Evercloud
* @license   http://www.php.net/license/3_01.txt  PHP License 3.01
* @version   Release: 1.0
* @link      /Cms/Blocks
* @since     Class available since Release 1.0
*/
class BlocksController extends CmsAppController
{

    /**
    * The index method
    * 
    * This method gets the Blocks tree and
    * sets it to the view
    * 
    * @return void
    */
    public function index()
    {
        $blocks = $this->Block->find('all');
        $this->set(compact('blocks'));
    }
    
    /**
    * The Block add method
    * 
    * Blocks are addedd through this method.
    * If there's no POST then this method will
    * display /modules/cms/views/Blocks/add.ctp
    * If there's a Block entity in POST then validation
    * and insertion will occur
    * 
    * @return void
    */
    public function add() 
    {
        if ($this->request->is('post')) {
            $alias = alias($this->request->data['Block']['alias']);
            $find = $this->Block->findByAlias($alias);
            if (!empty($find)) {
                $this->Session->setFlash(
                    __('Aliasul de block exista in baza de date!'),
                    'Alerts/warning'
                );
                return $this->redirect('/cms/blocks');
            }

            if ($this->Block->save($this->request->data)) {
                $this->Session->setFlash(
                    __('Pagina a fost adaugata!'), 
                    'Alerts/success'
                );
                return $this->redirect('/cms/blocks');
            }
        }
    }
    
    /**
    * The block edit method
    * 
    * Blocks can be edited and modified as wished
    * The name, description, parent_id and image
    * 
    * @param integer $block_id the Block id to be edited
    * 
    * @return void
    */
    public function edit($block_id)
    {
        if ($this->request->is('put')) {

            $alias = alias($this->request->data['Block']['alias']);
            $find = $this->Block->find(
                'first', array(
                'conditions' => array(
                    'alias' => $alias,
                    'id <>' => $block_id
                )
                )
            );
            if (!empty($find)) {
                $this->Session->setFlash(
                    __('Acest alias de block exista in baza de date!'),
                    'Alerts/warning'
                );
                return $this->redirect('/cms/blocks');
            }
            if ($this->Block->save($this->request->data)) {
                $this->Session->setFlash(
                    __('Block-ul a fost modificat!'),
                    'Alerts/success'
                );
                return $this->redirect('/cms/blocks');
            }
        }

        $this->Block->id = $block_id;
        if (!$this->Block->exists()) {
            $this->Session->setFlash(
                __('Block-ul nu exista in baza de date'),
                'Alerts/warning'
            );
            return $this->redirect('/cms/blocks');
        }
        
        $block = $this->Block->findById($block_id);
        $this->request->data = $block;
        $this->set('block', $block);
    }
    
    /**
    * Remove Block method
    * 
    * A Block cannot be removed if it has products,
    * characteristics
    * 
    * @param int $Block_id the Block id to be deleted
    * 
    * @return redirect to Blocks
    */
    public function delete(int $Block_id) 
    {
        $this->Block->id = $Block_id;
        if (!$this->Block->exists()) {
            $this->Session->setFlash(__('Block inexistent'), 'Alerts/warning');
            return $this->redirect('/cms/blocks');
        }
        if ($this->Block->delete()) {
            $this->Session->setFlash(
                __('Block-ul a fost sters'),
                'Alerts/success'
            );
        } else {
            $this->Session->setFlash(
                __('Eroare la stergerea Block-ului'),
                'Alerts/error'
            );
        }
        return $this->redirect('/cms/blocks');
    }
}
