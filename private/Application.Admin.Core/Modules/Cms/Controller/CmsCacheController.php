<?php
/**
 * Inside this Controller admin logic methods will
 * be placed
 *
 * PHP version 5.4
 *
 * @category  Admin
 * @package   Cms
 * @author    Daniel Ionascu <daniel.ionascu88@gmail.com>
 * @copyright 2016 Evercloud
 * @license   http://www.php.net/license/3_01.txt  PHP License 3.01
 * @version   SVN: $Id$
 * @link      /Cms/CmsCache
 * @since     1.0
 */

App::uses('Cms.CmsAppController', 'Controller');

/**
 * CmsCacheController Class
 *
 * Inside this Controller admin logic methods will
 * be placed
 *
 * @category  Admin
 * @package   Cms
 * @author    Daniel Ionascu <daniel.ionascu88@gmail.com>
 * @copyright 2016 Evercloud
 * @license   http://www.php.net/license/3_01.txt  PHP License 3.01
 * @version   Release: 1.0
 * @link      /Cms/CmsCache
 * @since     Class available since Release 1.0
 */
class CmsCacheController extends CmsAppController
{

    /**
     * The index method
     *
     * This method gets the Blocks tree and
     * sets it to the view
     *
     * @return void
     */
    public function index()
    {
        $cacheEditable = $this->CmsCache->find(
            'all', [
                'conditions' => [
                    'is_editable' => 1
                ]
            ]
        );

        $cacheNonEditable = $this->CmsCache->find(
            'all', [
                'conditions' => [
                    'is_editable' => 0
                ]
            ]
        );
        $this->set(compact(
            'cacheEditable',
            'cacheNonEditable'
        ));
    }

    public function changeStatus()
    {
        if (!$this->request->is('ajax')) {
            $this->respond(
                __('Date inconsistente'),
                'error'
            );
        }

        $this->CmsCache->id = $this->request->data['id'];
        $this->CmsCache->saveField(
            'is_active',
            $this->request->data['checked'] == 'true' ? true : false
        );

        die(json_encode([
            'success' => 1
        ]));
    }

    public function delete($id = null, $quiet = false)
    {
        $remote_site = Configure::read('WEBSITE_URL')
            . 'cache/delete'
            . (!is_null($id) ? '/' . $id : '');

        App::uses('HttpSocket', 'Network/Http');

        $HttpSocket = new HttpSocket();

        $results = $HttpSocket->get(
            $remote_site,
            'secretKey=DEA3398BCD9CDD87E1CB1272E714BB589BB5C6476A34F1287E51355B9D2B'
        );

        $response = json_decode($results->body, 1);

        if( $quiet == false) {
            if ($response['success']) {
                return $this->respond(
                    $response['msg'][0],
                    'success'
                );
            }

            return $this->respond(
                'Mesaj eroare primit: <br />'
                . implode('<br />', $response['msg']),
                'error'
            );
        }

    }
}
