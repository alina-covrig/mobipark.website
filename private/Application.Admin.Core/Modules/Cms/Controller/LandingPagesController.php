<?php
/**
 * LandingPagesController
 *
 * Inside this Controller admin logic methods will
 * be placed
 *
 * PHP version 5.4
 *
 * @category  Admin
 * @package   CMS
 * @author    Covrig Alina <alyna.ali26@yahoo.com>
 * @copyright 2014 Evercloud
 * @license   http://www.php.net/license/3_01.txt  PHP License 3.01
 * @version   SVN: $Id$
 * @link      /cms/landingpages
 * @since     1.0
 */

App::uses('Cms.CmsAppController', 'Controller');

class LandingPagesController extends CmsAppController
{

    public $uses = [
        'Cms.LandingPage'
    ];

    /**
     * The index method
     *
     * This method lists landing pages
     *
     * @return void
     */
    public function index()
    {

        $this->Paginator->settings['order'] = [
            'LandingPage.id' => 'DESC'
        ];

        $landing_pages = $this->Paginator->paginate('LandingPage');


        $this->set('landing_pages', $landing_pages);
    }

    public function add()
    {
        if ($this->request->is('post')) {
            $date = date('dmY');
            $path = 'landing_page/' . $date . '/';

            if (!is_dir($path)) {
                mkdir($path, 0755, true);
            }

            if(!$this->LandingPage->save($this->request->data)) {
                $errors =  $this->LandingPage->getValidationErrors();
                return $this->respond(
                    __('Nu am putut salva Landing page: ') . val2mess($errors),
                    'error'
                );
            } else {
                return $this->respond(
                    __('Noul landing page a fost salvat cu succes!'),
                    'success',
                    '/cms/landing_pages'
                );
            }
        }
    }

    public function edit($id = null)
    {
        $landing_page = $this->LandingPage->findById($id);
        $landing_page['LandingPage']['content'] =
            html_entity_decode($landing_page['LandingPage']['content']);

        if (empty($landing_page)) {
            $this->respond(
                __('Landing page inexistent'),
                'Alerts/error',
                '/cms/landing_pages'
            );
        }
        if ($this->request->is('post')) {

            $save['id'] = $id;
            $save['content'] = $this->request->data['LandingPage']['content'];
            $save['start_date'] =$this->request->data['LandingPage']['start_date'];
            $save['end_date'] = $this->request->data['LandingPage']['end_date'];
            $save['name']    = $this->request->data['LandingPage']['name'];
            $save['is_forever'] = $this->request->data['LandingPage']['is_forever'];
            $save['is_active'] = $this->request->data['LandingPage']['is_active'];
            $save['path'] = $this->request->data['LandingPage']['path'];
            $save['page_title'] = $this->request->data['LandingPage']['page_title'];
            $save['meta_description'] = $this->request->data['LandingPage']['meta_description'];
            

            if ($this->LandingPage->save($save)) {
                return $this->respond(
                    __('Landing page modificat cu succes'),
                    'success',
                    '/cms/landing_pages/edit/'.$id
                );
            } else {
                $errors = $this->LandingPage->getValidationErrors();
                return  $this->respond(
                    __('Landing page-ul nu a putut fi modificat') . val2mess($errors),
                    'error'
                );
            }
        }
        $this->set(compact('landing_page'));
    }

    public function view($id = null)
    {

        if(!$id) {
            //fara validation errors aici - validarea are loc atunci cand vrei sa
            //salvezi sau sa modifici ceva in BD. In clipa asta, nici n-am incercat
            //sa citim din baza de date, ci doar verificam daca metoda e apelata
            //cu un id valid..
            return  $this->respond(
                __('Pagina invalida!'),
                'error'
            );
        }

        $landing_page = $this->LandingPage->findById($id);

        //verificarea cu empty() e mai sigura, in ceea ce priveste obiectele
        //returnate la cautari.
        if(empty($landing_page)) {
            //iarasi, fara eroare de validare.
            return  $this->respond(
                __('Pagina promotionala nu a putut fi gasita!'),
                'error'
            );
        }

        $this->set('landing_page', $landing_page);
    }

}
