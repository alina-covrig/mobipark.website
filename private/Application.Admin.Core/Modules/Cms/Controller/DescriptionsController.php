<?php
/**
 * Inside this Controller admin logic methods will
 * be placed
 *
 * PHP version 5.4
 *
 * @category  Admin
 * @package   Cms
 * @author    Daniel Ionascu <daniel.ionascu88@gmail.com>
 * @copyright 2014 Evercloud
 * @license   http://www.php.net/license/3_01.txt  PHP License 3.01
 * @version   SVN: $Id$
 * @link      /Cms/banners
 * @since     1.0
 */

App::uses('Cms.CmsAppController', 'Controller');

/**
 * PagesController Class
 *
 * Inside this Controller admin logic methods will
 * be placed
 *
 * @category  Admin
 * @package   Cms
 * @author    Daniel Ionascu <daniel.ionascu88@gmail.com>
 * @copyright 2014 Evercloud
 * @license   http://www.php.net/license/3_01.txt  PHP License 3.01
 * @version   Release: 1.0
 * @link      /Cms/banners
 * @since     Class available since Release 1.0
 */
class DescriptionsController extends CmsAppController
{
    public $uses = [
        'Cms.Description',
    ];

    public function index()
    {
        $descriptions = $this->Description->find('all');


        $this->set(compact('descriptions'));
        //$this->render('/cms/home_texts');
    }

    public function edit($id)
    {
        $description = $this->Description->findById($id);
        if ($this->request->is(['post'])) {

            $save['id'] = $id;
            $save['description1'] = $this->request->data['Description']['description1'];
            $save['description2'] =$this->request->data['Description']['description2'];
            $save['description3'] = $this->request->data['Description']['description3'];
            $save['description4']    = $this->request->data['Description']['description4'];
            $save['description5'] = $this->request->data['Description']['description5'];
            $save['description6'] = $this->request->data['Description']['description6'];


            if ($this->Description->save($save)) {
                return $this->respond(
                    __('Descrieri modificate cu succes'),
                    'success',
                    '/cms/descriptions/edit/'.$id
                );
            } else {
                $errors = $this->Description->getValidationErrors();
                return  $this->respond(
                    __('Descrierile nu au putut fi modificate') . val2mess($errors),
                    'error'
                );
            }
        }
        $this->set('description', $description);

    }


}