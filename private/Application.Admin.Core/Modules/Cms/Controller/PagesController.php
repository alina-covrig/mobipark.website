<?php
/**
* Inside this Controller admin logic methods will
* be placed
*
* PHP version 5.4
*
* @category  Admin
* @package   Cms
* @author    Daniel Ionascu <daniel.ionascu88@gmail.com>
* @copyright 2014 Evercloud
* @license   http://www.php.net/license/3_01.txt  PHP License 3.01
* @version   SVN: $Id$
* @link      /Cms/pages
* @since     1.0
*/

App::uses('Cms.CmsAppController', 'Controller');

/**
* PagesController Class
*
* Inside this Controller admin logic methods will
* be placed
*
* @category  Admin
* @package   Cms
* @author    Daniel Ionascu <daniel.ionascu88@gmail.com>
* @copyright 2014 Evercloud
* @license   http://www.php.net/license/3_01.txt  PHP License 3.01
* @version   Release: 1.0
* @link      /Cms/pages
* @since     Class available since Release 1.0
*/
class PagesController extends CmsAppController
{

    /**
    * The index method
    * 
    * This method gets the pages tree and
    * sets it to the view
    * 
    * @return void
    */
    public function index()
    {
        $pages = $this->Page->find(
            'all', array(
            'order' => 'Page.lft'
            )
        );
        $this->set(compact('pages'));
    }
    
    /**
    * The move up method
    * 
    * This method will move up the page one step
    * 
    * @param integer $id the id of the page to be moved up
    * 
    * @return redirect to index
    */
    public function moveup($id = null) 
    {
        
        $this->Page->id = $id;
        if (!$this->Page->exists()) {
            $this->Session->setFlash(
                __('Pagina nu exista in baza de date'), 
                'Alerts/warning'
            );
            return $this->redirect('/cms/pages');
        }
        
        $this->Page->moveUp($this->Page->id, 1);
        $this->Session->setFlash(
            __('Pagina a fost mutata cu succes!'), 
            'Alerts/success'
        );
        
        return $this->redirect('/cms/pages');
    }
    
    /**
    * The move down method
    * 
    * This method will move down the page one step
    * 
    * @param integer $id the id of the pages to be moved down
    * 
    * @return redirect to index
    */
    public function movedown($id = null) 
    {
        
        $this->Page->id = $id;
        if (!$this->Page->exists()) {
            $this->Session->setFlash(
                __('Pagina nu exista in baza de date'), 
                'Alerts/warning'
            );
            return $this->redirect('/cms/pages');
        }
        
        $this->Page->moveDown($this->Page->id, 1);
        $this->Session->setFlash(
            __('Pagina a fost mutata cu succes!'), 
            'Alerts/success'
        );
        
        return $this->redirect('/cms/pages');
    }
    
    /**
    * The page add method
    * 
    * Pages are addedd through this method.
    * If there's no POST then this method will
    * display /modules/cms/views/pages/add.ctp
    * If there's a page entity in POST then validation
    * and insertion will occur
    * 
    * @return void
    */
    public function add() 
    {
        if ($this->request->is('post')) {
            
            $alias = alias($this->request->data['Page']['title']);
            $find = $this->Page->findByAlias($alias);
            if (!empty($find)) {
                $this->Session->setFlash(
                    __('Numele paginii exista in baza de date!'), 
                    'Alerts/warning'
                );
                return $this->redirect('/cms/pages');
            }
            
            if ($this->Page->save($this->request->data)) {
                $this->Session->setFlash(
                    __('Pagina a fost adaugata!'), 
                    'Alerts/success'
                );
                return $this->redirect('/cms/pages');
            }
        }
        $pages_list = $this->Page
            ->generateTreeList(null, null, null, ' » ');
        $this->set('parents', $pages_list);
    }
    
    /**
    * The page edit method
    * 
    * Pages can be edited and modified as wished
    * The name, description, parent_id and image
    * 
    * @param integer $page_id the page id to be edited
    * 
    * @return void
    */
    public function edit($page_id) 
    {
        if ($this->request->is('put')) {
            $alias = alias($this->request->data['Page']['title']);
            $find = $this->Page->find(
                'first', array(
                'conditions' => array(
                    'alias' => $alias,
                    'id <>' => $page_id
                )
                )
            );
            if (!empty($find)) {
                $this->Session->setFlash(
                    __('Numele paginii exista in baza de date!'), 
                    'Alerts/warning'
                );
                return $this->redirect('/cms/pages');
            }
            if ($this->Page->save($this->request->data)) {
                $this->Session->setFlash(
                    __('Pagina a fost modificata!'), 
                    'Alerts/success'
                );
                return $this->redirect('/cms/pages');
            }
        }
        $this->Page->id = $page_id;
        if (!$this->Page->exists()) {
            $this->Session->setFlash(
                __('Pagina nu exista in baza de date'), 
                'Alerts/warning'
            );
            return $this->redirect('/cms/pages');
        }
        
        $options = array(
            'conditions' => array (
                'Page.id' => $page_id,
            ),
        );
        $page = $this->Page->find('first', $options);;
        $this->request->data = $page;
        
        $pages_list = $this->Page
            ->generateTreeList(null, null, null, ' » ');
        $this->set('parents', $pages_list);
        $this->set('page', $page);
    }
    
    /**
    * Remove page method
    * 
    * A page cannot be removed if it has products,
    * characteristics
    * 
    * @param int $page_id the page id to be deleted
    * 
    * @return redirect to pages
    */
    public function delete(int $page_id) 
    {
        $this->Page->id = $page_id;
        if (!$this->Page->exists()) {
            $this->Session->setFlash(__('Pagina inexistenta'), 'Alerts/warning');
            return $this->redirect('/cms/pages');
        }
        if ($this->Page->delete()) {
            $this->Session->setFlash(
                __('Pagina si subpaginile au fost sterse'), 
                'Alerts/success'
            );
        } else {
            $this->Session->setFlash(
                __('Eroare la stergerea paginii'), 
                'Alerts/error'
            );
        }
        return $this->redirect('/cms/pages');
    }
}
