<?php
/**
 * Created by PhpStorm.
 * User: Virgiliu
 * Date: 2/20/2020
 * Time: 1:40 PM
 */

App::uses('CmsAppModel', 'Cms.Model');

class OneSignalNotification extends CmsAppModel
{
	public $getTypes = [
		0 => 'Notificari Generale',
		1 => 'Transport',
		2 => 'CAMPAIGN',
		3 => 'PRODUCT',
		4 => 'SEARCH',
		5 => 'CATALOG',
	];

	public $getKey = [
		0 => [
			0 => 'random'
		],
		1 => [
			0 => 'random'
		],
		2 => [
			0 => 'mobile_action',
			1 => 'link',
			2 => 'alt',
		],
		3 => [
			0 => 'mobile_action',
			1 => 'link',
		],
		4 => [
			0 => 'mobile_action',
			1 => 'link',
			2 => 'alt',
		],
		5 => [
			0 => 'mobile_action',
			1 => 'link',
			2 => 'alt',
		],
	];

	public function getValue(){
		return [
			0 => [
				0 => '0001'
			],
			1 => [
				0 => '0001'
			],
			2 => [
				0 => $this->getTypes[2],
				1 => '',
				2 => '',
			],
			3 => [
				0 => $this->getTypes[3],
				1 => Configure::read('STORE_URL') . 'model-link',
			],
			4 => [
				0 => $this->getTypes[4],
				1 => Configure::read('STORE_URL') . 'model-link',
				2 => '',
			],
			5 => [
				0 => $this->getTypes[5],
				1 => Configure::read('STORE_URL') . 'model-link',
				2 => '',
			]
		];
	}

	public function getSegments(){
		return [
			1 => 'Subscribed Users',
			2 => 'Active Users',
			3 => 'Engaged Users',
			4 => 'Inactive Users',
			5 => 'Test Users',
			6 => 'Web Only',
		];
	}
}