<?php
/**
* Page model file
*
* Inside this Model any data-manipulation
* methods will be placed
*
* PHP version 5.4
*
* @category  Admin
* @package   Page
* @author    Daniel Ionascu <daniel.ionascu88@gmail.com>
* @copyright 2014 Evercloud
* @license   http://www.php.net/license/3_01.txt  PHP License 3.01
* @version   SVN: $Id$
* @link      /modules/pages/models/page
* @since     1.0
*/

App::uses('CmsAppModel', 'Cms.Model');


/**
* Page model
*
* Inside this Model any data-manipulation
* methods will be placed
*
* @category  Admin
* @package   Page
* @author    Daniel Ionascu <daniel.ionascu88@gmail.com>
* @copyright 2014 Evercloud
* @license   http://www.php.net/license/3_01.txt  PHP License 3.01
* @version   Release: 1.0
* @link      /modules/pages/models/page
* @since     Class available since Release 1.0
*/
class Page extends CmsAppModel
{
    
    /**
    * ActsAs property
    * 
    * This property is an array containing the 
    * attached behaviors of the current model
    * 
    * @var 
    */
    public $actsAs = array('Tree', 'Containable');
    
    /**
    * Validation rules
    *
    * @var array
    */
    public $validate = array(
        'title' => array(
            'notBlank' => array(
                        'rule' => array('notBlank'),
                        'message' => 'Numele paginii trebuie completat',
            ),
        ),
    );
    
    /**
    * The beforeValidate callback method
    * 
    * @param array $options the set of options for the query
    * 
    * @return boolean
    */
    public function beforeValidate($options = array()) 
    {
        $this->data['Page']['alias'] 
            = alias($this->data['Page']['title']);
        $full_path = '';
        
        if ($this->data['Page']['parent_id'] != '') {
            $path = $this->getPath($this->data['Page']['parent_id']);
            foreach ($path as $c) {
                $full_path .= $c['Page']['alias'] . '/';
            }
        }
        $this->data['Page']['full_path'] 
            = $full_path . alias($this->data['Page']['title']);
        return true;
    }
    
}
