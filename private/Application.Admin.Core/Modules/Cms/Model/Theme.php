<?php
/**
* Block model file
*
* Inside this Model any data-manipulation
* methods will be placed
*
* PHP version 5.4
*
* @category  Admin
* @package   Page
* @author    Badeci Gabriel Lucian <lucian.badeci@gmail.com>
* @copyright 2015 Evercloud
* @license   http://www.php.net/license/3_01.txt  PHP License 3.01
* @version   SVN: $Id$
* @link      /modules/pages/models/block
* @since     1.0
*/

App::uses('CmsAppModel', 'Cms.Model');


/**
* Page model
*
* Inside this Model any data-manipulation
* methods will be placed
*
* @category  Admin
* @package   Page
* @author    Badeci Gabriel Lucian <lucian.badeci@gmail.com>
* @copyright 2015 Evercloud
* @license   http://www.php.net/license/3_01.txt  PHP License 3.01
* @version   Release: 1.0
* @link      /modules/pages/models/block
* @since     Class available since Release 1.0
*/
class Theme extends CmsAppModel
{
    /**
     * Validate property
     *
     * @var $validate contains the fields to validate
     * before any INSERT or UPDATE
     */
    public $validate = array(
        'name' => array(
            'notBlank' => array(
                'rule'     => 'notBlank',
                'required' => true,
                'message'  => 'Camp obligatoriu',
            ),
        ),
        'img_logo' => array(
            'upload' => array(
                'rule' => array('uploadThemeImage','img_logo','213', '54'),
                'allowEmpty' => true,
                'required' => false,
                'message' => "Eroare la incarcarea imaginii sau marime incorecta.
                 Incercati din nou."
            ),
        ),
        'img_top_left' => array(
            'upload' => array(
                'rule' => array('uploadThemeImage','img_top_left', '534', '129'),
                'allowEmpty' => true,
                'required' => false,
                'message' => "Eroare la incarcarea imaginii sau marime incorecta.
                 Incercati din nou."
            ),
        ),
        'img_top_right' => array(
            'upload' => array(
                'rule' => array('uploadThemeImage','img_top_right', '534', '129'),
                'allowEmpty' => true,
                'required' => false,
                'message' => "Eroare la incarcarea imaginii sau marime incorecta.
                 Incercati din nou."
            ),
        ),
        'img_bottom_left' => array(
            'upload' => array(
                'rule' => array('uploadThemeImage','img_bottom_left', '1151', '416'),
                'allowEmpty' => true,
                'required' => false,
                'message' => "Eroare la incarcarea imaginii sau marime incorecta.
                 Incercati din nou."
            ),
        ),
        'img_bottom_right' => array(
            'upload' => array(
                'rule' => array('uploadThemeImage','img_bottom_right', '1151','416'),
                'allowEmpty' => true,
                'required' => false,
                'message' => "Eroare la incarcarea imaginii sau marime incorecta.
                 Incercati din nou."
            ),
        ),
        'img_bottom_center' => array(
            'upload' => array(
                'rule' => array('uploadThemeImage','img_bottom_center','2691','119'),
                'allowEmpty' => true,
                'required' => false,
                'message' => "Eroare la incarcarea imaginii sau marime incorecta.
                 Incercati din nou."
            ),
        )

    );


    /**
     * The uploadThemeImage method
     *
     * Checks if $this->data contains an uploaded file, saves it
     * and alters the $this->data['Tag']['image'] accordingly
     *
     * @param mixed $check      contains data from the $this->data['Tag']['image']
     * @param mixed $field_name contains data from the $this->data['Tag']['image']
     * @param mixed $w          width
     * @param mixed $h          height
     *
     * @return bool
     *      */
    public function uploadThemeImage($check, $field_name, $w=0, $h=0)
    {
        if ((int) $check[$field_name]['error'] === 4) {
            unset($this->data['Theme'][$field_name]);
            return true;
        }
        if (!((int) $check[$field_name]['error'] === UPLOAD_ERR_OK)) {
            return false;
        }
        if (!is_uploaded_file($check[$field_name]['tmp_name'])) {
            return false;
        }

        $pathi = pathinfo($check[$field_name]['name']);

        if (!isset($pathi['extension'])
            || !in_array(
                strtolower($pathi['extension']),
                array('jpg', 'jpeg', 'png', 'gif')
            )
        ) {
            return false;
        }

        $destination_dir = 'img/specialthemes/';
        $new_name = time() . '_' . $check[$field_name]['name'];


        $img_size = getimagesize($check[$field_name]['tmp_name']);
        if ($img_size[0] != $w || $img_size[1] != $h) {
            return false;
        }

        if (!$this->remoteUpload(
            $check[$field_name]['tmp_name'],
            $destination_dir,
            $new_name
        )) {
            echo false;
        }

        $this->data['Theme'][$field_name] = $new_name;
        return true;
    }

}
