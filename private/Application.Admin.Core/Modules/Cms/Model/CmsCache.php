<?php
/**
 * CmsCache model file
 *
 * Inside this Model any data-manipulation
 * methods will be placed
 *
 * PHP version 5.4
 *
 * @category  Admin
 * @package   Cms
 * @author    Daniel Ionascu <daniel.ionascu88@gmail.com>
 * @copyright 2016 Evercloud
 * @license   http://www.php.net/license/3_01.txt  PHP License 3.01
 * @version   SVN: $Id$
 * @link      /modules/cms/models/CmsCache
 * @since     1.0
 */

App::uses('CmsAppModel', 'Cms.Model');


/**
 * Page model
 *
 * Inside this Model any data-manipulation
 * methods will be placed
 *
 * @category  Admin
 * @package   Cms
 * @author    Daniel Ionascu <daniel.ionascu88@gmail.com>
 * @copyright 2016 Evercloud
 * @license   http://www.php.net/license/3_01.txt  PHP License 3.01
 * @version   Release: 1.0
 * @link      /modules/Cms/models/CmsCache
 * @since     Class available since Release 1.0
 */
class CmsCache extends CmsAppModel
{
    public $useTable = 'caches';
}
