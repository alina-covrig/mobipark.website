<?php
/**
* Configuration model file
*
* Inside this Model any data-manipulation
* methods will be placed
*
* PHP version 5.4
*
* @category  Admin
* @package   Configuration
* @author    Catalin Prodan <clinpan@gmail.com>
* @copyright 2014 Evercloud
* @license   http://www.php.net/license/3_01.txt  PHP License 3.01
* @version   SVN: $Id$
* @link      /modules/cms/models/configuration
* @since     1.0
*/

App::uses('CmsAppModel', 'Cms.Model');


/**
* Configuration model
*
* Inside this Model any data-manipulation
* methods will be placed
*
* @category  Admin
* @package   Configuration
* @author    Catalin Prodan <clinpan@gmail.com>
* @copyright 2014 Evercloud
* @license   http://www.php.net/license/3_01.txt  PHP License 3.01
* @version   Release: 1.0
* @link      /modules/cms/models/configuration
* @since     Class available since Release 1.0
*/
class Configuration extends CmsAppModel
{
    
    /**
    * Validation rules
    *
    * @var array
    */
    public $validate = array(
    );
}
