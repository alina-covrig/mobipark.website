<?php
/**
* Banner model file
*
* Inside this Model any data-manipulation
* methods will be placed
*
* PHP version 5.4
*
* @category  Admin
* @package   Cms
* @author    Daniel Ionascu <daniel.ionascu88@gmail.com>
* @copyright 2014 Evercloud
* @license   http://www.php.net/license/3_01.txt  PHP License 3.01
* @version   SVN: $Id$
* @link      /cms/models/banner
* @since     1.0
*/

App::uses('CmsAppModel', 'Cms.Model');


/**
* BannerModel Class
*
* Inside this Model class the admin data manipulation
* methods will be placed
*
* @category  Admin
* @package   Banner
* @author    Daniel Ionascu <daniel.ionascu88@gmail.com>
* @copyright 2014 Evercloud
* @license   http://www.php.net/license/3_01.txt  PHP License 3.01
* @version   Release: 1.0
* @link      /cms/models/banner
* @since     Class available since Release 1.0
*/
class Banner extends CmsAppModel
{
    
    /**
    * ActsAs property
    * 
    * This property is an array containing the 
    * attached behaviors of the current model
    * 
    * @var 
    */
    public $actsAs = array('Containable');
    
    /**
    * Validation rules
    *
    * @var array
    */
    public $validate = array(
        'path' => array(
            'notBlank' => array(
                        'rule' => array('notBlank'),
                        'message' => 'Calea imaginii nu poate fi goala',
            ),
        ),
        'order' => array(
            'pos_integer' => array(
                'rule'     => array('naturalNumber', true),
                'message'  => 'Valoare incorecta pentru ordine',
            ),
        ),
    );
    
    /**
    * The uploadBanner method
    * 
    * Checks if $this->data contains uploaded files, saves them
    * and alters the $this->data['Banner']['image'] accordingly
    * 
    * @param mixed $check contains data from the $this->data['Banner']
    * 
    * @return string 
    */
    public function uploadBanner ($check) 
    {
        if ((int) $check['error'] === UPLOAD_ERR_NO_FILE) {
            $this->data['image'] = '';
            return true;
        }
        if (!((int) $check['error'] === UPLOAD_ERR_OK)) {
            return false;
        }
        if (!is_uploaded_file($check['tmp_name'])) {
            return false;
        }
        
        $pathi = pathinfo($check['name']);
        if (!isset($pathi['extension']) 
            || !in_array(
                strtolower($pathi['extension']), 
                array('jpg', 'jpeg', 'png')
            )
        ) {
            return false;
        }
        $destination_dir = 'img'
            . DS 
            . 'gallery';
            
        $new_name = time() . '_' . $check['name'];
        $this->remoteUpload(
            $check['tmp_name'],
            $destination_dir,
            $new_name
        );
        return $new_name;
    }
}
