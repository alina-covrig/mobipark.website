<?php
/**
 * Newsletter.php
 *
 * Inside this Model any module-wide
 * data manipulation methods will be placed
 *
 * PHP version 5.4
 *
 * @category  Admin
 * @package   Loyalty
 * @author    Daniel Ionascu <daniel.ionascu88@gmail.com>
 * @copyright 2015 Evercloud
 * @license   http://www.php.net/license/3_01.txt  PHP License 3.01
 * @version   SVN: $Id$
 * @link      /modules/Loyalty/models/newsletter
 * @since     1.0
 */

App::uses('CmsAppModel', 'Cms.Model');

class LandingPage extends CmsAppModel
{

    /**
     * ActsAs property
     *
     * This property is an array containing the
     * attached behaviors of the current model
     *
     * @var
     */
    public $actsAs = ['Containable'];

    public $validate = [
        'name' => [
            'notBlank' => [
                'rule' => ['notBlank'],
                'message' => 'Numele Landing Page nu poate fi gol.',
            ],
        'start_date' => [
                'rule' => ['notBlank'],
                'message' => 'Data de start este necesara',
          ]

        ]
    ];

    /**
     * The upload method
     *
     * Landing pages images are added through this method.
     *
     * @return mixed
     */
    public function upload($logo_file)
    {

        if ((int) $logo_file['error'] === UPLOAD_ERR_NO_FILE) {
            return;
        }
        if (!((int) $logo_file['error'] === UPLOAD_ERR_OK)) {
            return false;
        }
        if (!is_uploaded_file($logo_file['tmp_name'])) {
            return false;
        }

        $pathi = pathinfo($logo_file['name']);
        if (!isset($pathi['extension'])
            || !in_array(
                strtolower($pathi['extension']),
                array('jpg', 'jpeg', 'png')
            )
        ) {
            return false;
        }
        $date = date('Ymd');
        $destination_dir = 'landing_page/' . $date . '/img/';

        if (!is_dir($destination_dir)) {
            mkdir($destination_dir, 0755, true);
        }

        $new_name =  time() . '_' . $pathi['filename'] . '.'. strtolower($pathi['extension']);
        if (!$this->remoteUpload(
            $logo_file['tmp_name'],
            $destination_dir,
            $new_name
        )) {
            return false;
        }

        return $new_name;

    }


}