<?php
/**
 * Block model file
 *
 * Inside this Model any data-manipulation
 * methods will be placed
 *
 * PHP version 5.4
 *
 * @category  Admin
 * @package   Page
 * @author    Badeci Gabriel Lucian <lucian.badeci@gmail.com>
 * @copyright 2015 Evercloud
 * @license   http://www.php.net/license/3_01.txt  PHP License 3.01
 * @version   SVN: $Id$
 * @link      /modules/pages/models/block
 * @since     1.0
 */

App::uses('CmsAppModel', 'Cms.Model');


/**
 * Page model
 *
 * Inside this Model any data-manipulation
 * methods will be placed
 *
 * @category  Admin
 * @package   Page
 * @author    Badeci Gabriel Lucian <lucian.badeci@gmail.com>
 * @copyright 2015 Evercloud
 * @license   http://www.php.net/license/3_01.txt  PHP License 3.01
 * @version   Release: 1.0
 * @link      /modules/pages/models/block
 * @since     Class available since Release 1.0
 */
class Description extends CmsAppModel
{
    /**
     * Validation rules
     *
     * @var array
     */
}
