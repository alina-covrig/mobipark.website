<?php
/**
* Category model file
*
* Inside this Model any data-manipulation
* methods will be placed
*
* PHP version 5.4
*
* @category  Admin
* @package   Category
* @author    Valentin Ioan <valentin.ioan@gmail.com>
* @copyright 2020 Evercloud
* @license   http://www.php.net/license/3_01.txt  PHP License 3.01
* @version   SVN: $Id$
* @link      /modules/pages/models/category
* @since     1.0
*/

App::uses('CmsAppModel', 'Cms.Model');


/**
* Category model
*
* Inside this Model any data-manipulation
* methods will be placed
*
* @category  Admin
* @package   Category
* @author    Valentin Ioan <valentin.ioan@gmail.com>
* @copyright 2020 Evercloud
* @license   http://www.php.net/license/3_01.txt  PHP License 3.01
* @version   Release: 1.0
* @link      /modules/pages/models/category
* @since     Class available since Release 1.0
* @property  Article Article
*/
class Category extends CmsAppModel
{
    public $actsAs = [
        'Tree',
        'Containable'
    ];
    
    public $validate = [
        'name' => [
            'notBlank' => [
                'rule' => ['notBlank'],
                'message' => 'Numele categoriei trebuie completat',
            ],
        ],
        'parent_id' => [
            'has_articles' => [
                'rule' => ['checkArticles'],
                'message' => 'Aceasta categorie are deja articole si nu i se pot adauga subcategorii'
            ]
        ]
    ];

    /**
    * The beforeValidate callback method
    *
    * @param array $options the set of options for the query
    *
    * @return boolean
    */
    public function beforeValidate($options = [])
    {
	    $slug = alias($this->data['Category']['name']);

	    //we check if slug already exists in db
	    // if exist we put a timestamp on end

	    $slug_db = $this->find(
		    'first',
		    [
			    'conditions' =>[
				    'Category.alias' => $slug
			    ]
		    ]
	    );


	    if(count($slug_db) > 0){
		    $slug = $slug . time();
	    };

	    $this->data['Category']['alias'] = $slug;

        return true;
    }

    /**
     * Get articles of this category
     *
     * @param int $category_id - category id
     * @param array $conditions - extra conditions
     *
     * @return array|null
     */
    public function getArticles($category_id, $conditions = [])
    {
        $this->bindModel([
            'hasMany' => [
                'Article' => [
                    'className' => 'Cms.Article'
                ]
            ]
        ]);
        return $this->Article->find(
            'all',
            [
                'conditions' => array_merge(
                    $conditions,
                    [
                        'Category.id' => $category_id
                    ]
                )
            ]
        );
    }

    /**
     * Validation check to see if the target parent category has articles or not
     *
     * @param $check - the category id
     *
     * @return bool
     */
    public function checkArticles($check)
    {
        $articles = $this->getArticles($check['parent_id']);
        return empty($articles);
    }

    /**
     * The isLeaf method
     *
     * Checks if rght-lft = 1
     * This means the category is an articles holder
     *
     * @param integer $id the id of the category to check
     *
     * @return boolean
     */
    public function isLeaf($id)
    {
        $this->id = $id;
        $this->virtualFields = array(
            'is_leaf' => 'Category.rght - Category.lft'
        );
        return ((int) $this->field('is_leaf') === 1);
    }
}
