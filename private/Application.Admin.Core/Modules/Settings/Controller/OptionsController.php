<?php
/**
 * OptionsController
 *
 * Inside this Controller admin logic methods will
 * be placed
 *
 * PHP version 5.4
 *
 * @category  Admin
 * @package   Merchandise
 * @author    Lucian Vasile <lucian.vasile@live.com>
 * @copyright 2014 Evercloud
 * @license   http://www.php.net/license/3_01.txt  PHP License 3.01
 * @version   SVN: $Id$
 * @link      /settings/options
 * @since     1.0
 */

App::uses('Settings.SettingsAppController', 'Controller');

/**
 * EntriesController Class
 *
 * Inside this Controller admin logic methods will
 * be placed
 *
 * @category  Admin
 * @package   Merchandise
 * @author    Lucian Vasile <lucian.vasile@live.com>
 * @copyright 2014 Evercloud
 * @license   http://www.php.net/license/3_01.txt  PHP License 3.01
 * @version   Release: 1.0
 * @link      /settings/options
 * @since     Class available since Release 1.0
 */
class OptionsController extends SettingsAppController
{
    public $uses = [
        'Configuration'
    ];

    public function index() 
    {
        $this->set('maintenance', false);
        $path = 'site_in_mentenanta.html';
        $headers = get_headers(Configure::read('WEBSITE_URL') . $path);

        if (strpos($headers[0], '200') !== false) {
            $this->set('maintenance', true);
        }

        $this->set('trim_the_fat', Configure::read('TRIM_THE_FAT'));
    }

    /**
     * The maintenanceYes method
     *
     * @return redirect
     */
    public function maintenanceYes() 
    {
        if (!$this->remoteRename(
            'site__not-in_mentenanta.html|site_in_mentenanta.html'
        )) {
            return $this->respond(
                __('Eroare la plasarea site-ului in mentenanta!')
            );
        }
        return $this->respond(
            __('Site-ul este in mentenanta!')
        );
    }

    /**
     * The maintenanceNo method
     *
     * @return redirect
     */
    public function maintenanceNo() 
    {
        if (!$this->remoteRename(
            'site_in_mentenanta.html|site__not-in_mentenanta.html'
        )) {
            return $this->respond(
                __('Eroare la scoaterea site-ului din mentenanta!')
            );
        }
        return $this->respond(
            __('Site-ul este scos din mentenanta!')
        );
    }

    /**
     * The trimTheFat method
     *
     * @param  $value
     * @return mixed
     */
    public function trimTheFat($value) 
    {
        if (is_bool($value) !== false) {
            return $this->respond(
                __('Actiune incorecta!')
            );
        }

        $this->Configuration->updateAll(
            [
            'value' => $value
            ], [
            'name' => 'TRIM_THE_FAT'
            ]
        );

        // delete backend cache configuration
        Cache::delete('Application.Configurations', 'default');

        // delete frontend cache configuration
        App::uses('HttpSocket', 'Network/Http');

        $sock = new HttpSocket();
        $sock->get(
            Configure::read('BACKEND_URL') . 'cms/cms_cache/delete/1'
            . '.json?'
            . 'email=' . Configure::read('STORE_ADMIN')
            . '&hash=' . Configure::read('STORE_ADMIN_H')
        );

        return $this->respond(
            __(
                'Optimizarea pentru heavy load a fost %s!',
                ($value)?'activata':'dezactivata'
            )
        );
    }
}
