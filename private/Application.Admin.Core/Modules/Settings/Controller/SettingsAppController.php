<?php
/**
 * Module level Controller
 *
 * This file is module-wide controller file. You can put all
 * module-wide controller-related methods here.
 *
 * PHP version 5.4
 *
 * @category  Admin
 * @package   Settings
 * @author    Lucian Vasile <lucian.vasile@live.com>
 * @copyright 2015 Evercloud
 * @license   http://www.php.net/license/3_01.txt  PHP License 3.01
 * @version   SVN: $Id$
 * @link      /modules/settings/controller/settings-app-controller
 * @since     1.0
 */

App::uses('AppController', 'Controller');

/**
 * SettingsAppController Class
 *
 * Inside this Controller module-wide logic methods
 * will be placed
 *
 * @category  Admin
 * @package   Merchandise
 * @author    Lucian Vasile <lucian.vasile@live.com>
 * @copyright 2015 Evercloud
 * @license   http://www.php.net/license/3_01.txt  PHP License 3.01
 * @version   Release: 1.0
 * @link      /modules/settings/controller/settings-app-controller
 * @since     Class available since Release 1.0
 */
class SettingsAppController extends AppController
{
    
}
