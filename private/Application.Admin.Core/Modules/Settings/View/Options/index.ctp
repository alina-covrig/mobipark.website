<?=$this->start('page-title');?>
    <?=__('Setari site')?>
<?=$this->end()?>

<div class="row" style="padding-top: 10px;">
    <div class="col-lg-3 mb20">
        <?php if (!$maintenance): ?>
            <?=$this->H->bslk(
                '/settings/options/maintenanceYes',
                'btn btn-danger btn-block',
                $this->Html->icon('fa fa-close') . ' ' . __('Opreste site-ul'),
                __('Opreste site-ul')
            )
            ?>
        <?php else: ?>
            <?=$this->H->bslk(
                '/settings/options/maintenanceNo',
                'btn btn-success btn-block',
                $this->Html->icon('fa fa-check-circle-o') . ' ' . __('Porneste site-ul'),
                __('Porneste site-ul')
            )
            ?>
        <?php endif; ?>

        <hr>

        <?php if ($trim_the_fat): ?>
            <?=$this->H->bslk(
                '/settings/options/trimTheFat/0',
                'btn btn-info btn-block',
                $this->Html->icon('fa fa-close') . ' ' . __('Dezactiveaza optimizare pt Heavy Load'),
                __('Dezactiveaza optimizare pt Heavy Load')
            )
            ?>
        <?php else: ?>
            <?=$this->H->bslk(
                '/settings/options/trimTheFat/1',
                'btn btn-warning btn-block',
                $this->Html->icon('fa fa-close') . ' ' . __('Activeaza optimizare pt Heavy Load'),
                __('Activeaza optimizare pt Heavy Load')
            )
            ?>
        <?php endif; ?>
    </div>
    <!-- /.col-lg-3 -->
</div>
