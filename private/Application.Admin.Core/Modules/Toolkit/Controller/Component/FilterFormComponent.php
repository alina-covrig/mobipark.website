<?php
/**
 * Filter form controller
 *
 * This file holds the controller for the filterform form
 *
 * PHP version 5.4
 *
 * @category  Admin
 * @package   FilterForm
 * @author    Daniel Ionascu <daniel.ionascu88@gmail.com>
 * @copyright 2014 Evercloud
 * @license   http://www.php.net/license/3_01.txt  PHP License 3.01
 * @version   SVN: $Id$
 * @link      ...
 * @since     1.0
 */

/**
 * FilterFormComponent Class
 *
 * Inside this Controller admin logic methods will
 * be placed
 *
 * @category  Admin
 * @package   FilterForm
 * @author    Daniel Ionascu <daniel.ionascu88@gmail.com>
 * @copyright 2014 Evercloud
 * @license   http://www.php.net/license/3_01.txt  PHP License 3.01
 * @version   Release: 1.0
 * @link      ...
 * @since     Class available since Release 1.0
 */
class FilterFormComponent extends Component
{

    /**
     * The parseConditions method
     * 
     * @param array $query the GET query
     * 
     * @return array
     */
    public function parseConditions($query) 
    {
        $newConditions = array();
        $having_conditions = "";
        foreach ($query as $model => $values) {
            
            if (empty($values) || !is_array($values)) {
                continue;
            }

            foreach ($values as $field => $value) {
                if (is_array($value)) {

                    if (isset($value['date_interval_days'][0])
                        && $value['date_interval_days'][0] != ''
                    ) {
                        $newConditions[$model . '.' . $field . ' >=']
                            = date(
                                'Y-m-d',
                                strtotime($value['date_interval_days'][0])
                            )
                            .' 00:00:00';
                    }
                    if (isset($value['date_interval_days'][1])
                        && $value['date_interval_days'][1] != ''
                    ) {
                        $newConditions[$model . '.' . $field . ' <=']
                            = date(
                                'Y-m-d',
                                strtotime($value['date_interval_days'][1])
                            )
                            .' 23:59:59';
                    }


                    if (isset($value['date_interval'][0])
                        && $value['date_interval'][0] != ''
                    ) {
                        $newConditions[$model . '.' . $field . ' >=']
                            = $value['date_interval'][0];
                    }
                    if (isset($value['date_interval'][1])
                        && $value['date_interval'][1] != ''
                    ) {
                        $newConditions[$model . '.' . $field . ' <=']
                            = $value['date_interval'][1];
                    }
                        
                    if (isset($value['numeric_interval'][0])
                        && $value['numeric_interval'][0] != ''
                    ) {
                        $newConditions[$model . '.' . $field . ' >=']
                            = $value['numeric_interval'][0];
                    }
                    if (isset($value['numeric_interval'][1])
                        && $value['numeric_interval'][1] != ''
                    ) {
                        $newConditions[$model . '.' . $field . ' <=']
                            = $value['numeric_interval'][1];
                    }
                        
                    if (isset($value['exact'])
                        && $value['exact'] != ''
                    ) {
                        $newConditions[$model . '.' . $field . ' LIKE']
                            = $value['exact'];
                    }
                        
                    if (isset($value['n'][0])) {
                        $multi_select_values = array('OR' => array());
                        foreach ($value['n'] as $numeric_filter) {
                            if ($numeric_filter != '') {
                                $multi_select_values['OR'][]
                                    = array(
                                        $model . '.' . $field
                                            => $numeric_filter
                                    );
                            }                             
                        }
                        if (!empty($multi_select_values['OR'])) {
                            $newConditions[] = $multi_select_values;
                        }
                    }
                } elseif ($value != '') {
                    if (preg_match("/^[0-9]+$/", $value)) {
                    //if (is_numeric($value)) {
                        if ($model == "NotFoundProduct" && $field == "email") {
                            $value = implode('%', explode(' ', $value));
                            $newConditions[$model . '.' . $field . ' LIKE']
                                = "%" . $value . "%";
                        } else {
                            $newConditions[$model . '.' . $field] = $value;
                        }
                    } elseif (is_string($value)) {
                        if ($model == "User" && $field == "full_name") {
                            $value = implode('%', explode(' ', $value));
                            $having_conditions .= $model . '__' . $field
                                . ' LIKE "%' . $value . '%"';
                        } elseif ($model == "Sale" && $field == "custom_code") {
                            $newConditions[$model . '.' . $field] = $value;
                        } elseif (strpos($value, ',') !== false) {
                            $newConditions[$model . '.' . $field . ' IN'] = explode(',', $value);
                        } else {
                            $value = implode('%', explode(' ', $value));
                            $newConditions[$model . '.' . $field . ' LIKE']
                                = "%" . $value . "%";
                        }
                    } 
                }
            }
        }
        if (!empty($having_conditions)) {
            $newConditions['LAST'] = 'HAVING '.$having_conditions;
        }
        return $newConditions;
    }


    /**
     * The getMultiSelectValues method
     *
     * @param array $query the GET query
     * @param array $model the model
     * @param array $field the field
     *
     * @return array
     */
    public function getMultiSelectValues($query, $model, $field) 
    {
        if (!isset($query[$model][$field]['n'])
            || !is_array($query[$model][$field]['n'])
        ) {
            return array();
        }
        return $query[$model][$field]['n'];
    }

    /**
     * The getDateString method
     *
     * @param array $query the GET query
     * @param array $model the model
     * @param array $field the field
     *
     * @return array
     */
    public function getDateString($query, $model, $field) 
    {
        $str = '';
        if (!empty($query[$model][$field])) {
            if (isset($query[$model][$field]['date_interval'])) {
                if (!empty($query[$model][$field]['date_interval'][0])
                    || !empty($query[$model][$field]['date_interval'][1])
                ) {
                    $str .= __('din perioada: ');
                    if (!empty($query[$model][$field]['date_interval'][0])) {
                        $str .= $query[$model][$field]['date_interval'][0];
                    }
                    $str .= ' '.__('pana pe').' ';
                    if (!empty($query[$model][$field]['date_interval'][1])) {
                        $str .= $query[$model][$field]['date_interval'][1];
                    } else {
                        $str .= _('prezent');
                    }
                }
            } else {
                $str .= $query[$model][$field];
            }
        }
        
        return $str;
    }
}