<label class="btn btn-xs"
       style="right: 20px;top: -10px;position: absolute;display: inline-block;">
    <select id="ms-<?php echo alias ($field_index) ?>"
            name="<?php echo $fieldName; ?>">
        <?php if (isset($field['data']) && isset(${$field['data']})): ?>
            <?php
            $group_is_open = false;
            foreach (${$field['data']} as $id => $ms_item):?>
                <?php $ms_selected = 0; ?>
                <?php if (isset($printer)): ?>
                    <?php $ms_selected = ($printer == $id) ? 1 : 0; ?>
                <?php endif; ?>
                <option
                    value="<?php echo $id; ?>"<?php if ($ms_selected): ?> selected<?php endif; ?>><?php echo $ms_item; ?></option>
            <?php endforeach; ?>
        <?php endif; ?>
    </select>
    <?php if (isset($field['label'])): ?>
        <?= $field['label'] ?>
    <?php endif; ?>
</label>