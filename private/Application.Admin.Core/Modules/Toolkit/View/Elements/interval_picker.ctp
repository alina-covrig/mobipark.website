<input
    type="text"
    autocomplete="off"
    class="form-control input-xs"
    name="<?php echo $fieldName; ?>[date_interval][]"
    id="<?php echo $options_field_1['id']; ?>"
    <?php if (isset($field['width'])): ?>
        style="width:<?php echo $field['width'] ?>px;display:inline-block;"
    <?php endif; ?>
    <?php if (isset($field['readonly'])): ?>
        readonly
    <?php endif; ?>
    <?php if (isset($date_interval_values[0])): ?>
        value="<?php echo $date_interval_values[0]; ?>"
    <?php endif; ?>
/> - <input
    type="text"
    autocomplete="off"
    class="form-control input-xs"
    name="<?php echo $fieldName; ?>[date_interval][]"
    id="<?php echo $options_field_2['id']; ?>"
    <?php if (isset($field['width'])): ?>
        style="width:<?php echo $field['width'] ?>px;display:inline-block;"
    <?php endif; ?>
    <?php if (isset($field['readonly'])): ?>
        readonly
    <?php endif; ?>
    <?php if (isset($date_interval_values[1])): ?>
        value="<?php echo $date_interval_values[1]; ?>"
    <?php endif; ?>
/>
<?php $this->start ('extra-scripts') ?>
<script type="text/javascript">
    $(function () {
        $("#<?=$options_field_1['id']?>").datetimepicker({
            numberOfMonths: 2,
            dayOfWeekStart: 1,
            lang: 'ro',
        })
        
        $("#<?=$options_field_2['id']?>").datetimepicker({
            numberOfMonths: 2,
            dayOfWeekStart: 1,
            lang: 'ro'
        })
    });
</script>
<?php $this->end () ?>