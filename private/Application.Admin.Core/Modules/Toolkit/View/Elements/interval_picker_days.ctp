<input
    type="text"
    autocomplete="off"
    class="form-control input-xs"
    name="<?php echo $fieldName; ?>[date_interval_days][]"
    id="<?php echo $options_field_1['id']; ?>"
    <?php if (isset($field['width'])): ?>
        style="width:<?php echo $field['width'] ?>px;display:inline-block;"
    <?php endif; ?>
    <?php if (isset($field['readonly'])): ?>
        readonly
    <?php endif; ?>
    <?php if (isset($date_interval_values[0])): ?>
        value="<?php echo $date_interval_values[0]; ?>"
    <?php endif; ?>
/> - <input
    type="text"
    autocomplete="off"
    class="form-control input-xs"
    name="<?php echo $fieldName; ?>[date_interval_days][]"
    id="<?php echo $options_field_2['id']; ?>"
    <?php if (isset($field['width'])): ?>
        style="width:<?php echo $field['width'] ?>px;display:inline-block;"
    <?php endif; ?>
    <?php if (isset($field['readonly'])): ?>
        readonly
    <?php endif; ?>
    <?php if (isset($date_interval_values[1])): ?>
        value="<?php echo $date_interval_values[1]; ?>"
    <?php endif; ?>
/>
<?php $this->start ('extra-scripts') ?>
<script type="text/javascript">
    $(function () {
        $("#<?=$options_field_1['id']?>").datetimepicker({
            timepicker: false,
            dayOfWeekStart: 1,
            closeOnDateSelect: true,
            lang: 'ro',
            format: 'Y/m/d',
            onShow: function (ct) {
                this.setOptions({
                    maxDate: $('#<?=$options_field_2['id']?>').val() ? $('#<?=$options_field_2['id']?>').val() : false
                })
            },
            onSelectDate: function (ct, $i) {

                $('#<?=$options_field_2['id']?>').focus()


            }

        })

        $("#<?=$options_field_2['id']?>").datetimepicker({
            timepicker: false,
            dayOfWeekStart: 1,
            closeOnDateSelect: true,
            lang: 'ro',
            format: 'Y/m/d',
            onShow: function (ct) {
                this.setOptions({
                    minDate: $('#<?=$options_field_1['id']?>').val() ? $('#<?=$options_field_1['id']?>').val() : false
                })
            }
        })
    });
</script>
<?php $this->end () ?>