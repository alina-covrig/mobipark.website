<?php
    if(!isset($filterForm)) return;

    echo preg_replace("/\/page:\d+/i", "", $this->Form->create(
        (empty($filterForm['action'])? null : $filterForm['action']),
        array(
            'type' => 'get',
            //'action' => (empty($filterForm['action'])? 'index' : $filterForm['action']),
            'formStyle' => 'inline',
            'autocomplete' => 'off',
        ) // @todo improve action logic
    ));
?>

<?php foreach($filterForm['fields'] as $field_index => $field) :?>

    <?php if(is_null($field)): ?>
        <td></td>
        <?php continue;?>
    <?php endif;?>
    
    <?php $options = array(
        'required' => FALSE,
        'class' => "form-control input-xs",
        'autocomplete' => 'off',
    );?>

    <?php if(is_array($field)):

        $fieldName = isset($field['name']) ? $field['name'] : '';

        if (isset($field['is_datepicker'])) {
            $options['class'] .= ' add_datetimepickerdateonly';
        }

        if (isset($field['selectpicker'])) {
            $options['class'] .= ' selectpicker';
        }

        if (isset($field['is_intervalpicker'])) {
            $options['class'] .= ' add_intervalpicker';
            $options_field_1['id'] = 'id-' . rand(1000,9999);
            $options_field_2['id'] = 'id-' . rand(1000,9999);
        }

        if (isset($field['is_intervalpicker_days'])) {
            $options['class'] .= ' add_intervalpicker_days';
            $options_field_1['id'] = 'id-x' . rand(1000,9999);
            $options_field_2['id'] = 'id-x' . rand(1000,9999);
        }

        if(isset($field['id'])) {
            $options['id'] = $field['id'];
        }

        if (isset($field['is_interval'])) {
            $options['class'] .= ' add_interval';
        }

        if (isset($field['width'])) {
            $options['style'] = 'width:' . $field['width'] . 'px;';
        }

        if(isset($field['label'])) {
            $options['label'] = $field['label'];
        }

        if (isset($field['data-live-search'])) {
            $options['data-live-search'] = "true";
        }

        if(isset($field['readonly'])) {
            $options['readonly'] = 'readonly';
        }

        if(isset($field['type'])) {
            $options['type'] = $field['type'];
        }

        if(isset($field['data']) && isset(${$field['data']})) {
            $options['options'] = ${$field['data']};
            if(isset($field['label'])) {
                $options['empty'] = $field['label'];
            }
        }

        if (isset($field['is_multipleselect'])) {
            if (isset($field['class'])) {
                $options['class'] .= ' ' . $field['class'];
            }
            if (isset($field['filter'])) {
                $options['filter'] = $field['filter'];
            }
            if (isset($field['single'])) {
                $options['single'] = $field['single'];
            }
            if (isset($field['select_all'])) {
                $options['select_all'] = $field['select_all'];
            }
        }

        // get FORM values
        $multiselect_values = array();
        $date_interval_values = array();
        $date_interval_days = array();
        $numeric_interval_values = array();
        $exact_match_value = '';
        if (!empty($fieldName) && strpos($fieldName,'[') && strpos($fieldName,']')) {
            $query_model_parts = explode('[', $fieldName);
            $query_model = $query_model_parts[0];
            $query_field_parts = explode(']', $query_model_parts[1]);
            $query_field = $query_field_parts[0];

            if (!empty($_GET[$query_model][$query_field])) {
                if (!is_array($_GET[$query_model][$query_field])) {
                    $field['value'] = $_GET[$query_model][$query_field];
                    $options['value'] = $_GET[$query_model][$query_field];
                    $options['selected'] = $_GET[$query_model][$query_field];
                } else {
                    if (isset($_GET[$query_model][$query_field]['n'])) {
                        $multiselect_values = $_GET[$query_model][$query_field]['n'];
                    } elseif (isset($_GET[$query_model][$query_field]['date_interval'])) {
                        $date_interval_values = $_GET[$query_model][$query_field]['date_interval'];
                    } elseif (isset($_GET[$query_model][$query_field]['date_interval_days'])) {
                        $date_interval_days = $_GET[$query_model][$query_field]['date_interval_days'];
                    } elseif (isset($_GET[$query_model][$query_field]['numeric_interval'])) {
                        $numeric_interval_values = $_GET[$query_model][$query_field]['numeric_interval'];
                    } elseif (isset($_GET[$query_model][$query_field]['exact'])) {
                        $exact_match_value = $_GET[$query_model][$query_field]['exact'];
                    }
                }
            } elseif (isset($_GET[$query_model][$query_field])
                && $_GET[$query_model][$query_field] == 0
                && $_GET[$query_model][$query_field] !== ''
            ) {
                $options['value'] = 0;
            } elseif (!empty($field['value'])) {
                $options['value'] = $field['value'];
                $options['selected'] = $field['value'];
            }
        }

        if (isset($field['printers'])): ?>
            <label class="btn btn-xs" style="right: 20px;top: -10px;position: absolute;display: inline-block;">
            <select id="ms-<?php echo alias($field_index)?>"name="<?php echo $fieldName;?>">
            <?php if(isset($field['data']) && isset(${$field['data']})):?>
                <?php
                $group_is_open = false;
                foreach (${$field['data']} as $id => $ms_item):?>
                    <?php $ms_selected = 0; ?>
                    <?php if (isset($printer)): ?>
                        <?php $ms_selected = ($printer == $id) ? 1 : 0;?>
                    <?php endif; ?>
                    <option value="<?php echo $id;?>"<?php if ($ms_selected):?> selected<?php endif;?>><?php echo $ms_item;?></option>
                <?php endforeach;?>
            <?php endif;?>
            </select>
            <?php if(isset($field['label'])):?>
                <?=$field['label']?>
            <?php endif;?>
            </label>
        <?php elseif (isset($field['type']) && $field['type'] == 'checkbox'): ?>
            <?php if (empty($additional_row_added)):?>
                <td class="text-center">
                    <label>
                        <input
                            style="margin-top: 0px; vertical-align: middle" type="checkbox"
                            name="<?=$fieldName?>"
                            <?php if (!empty($options['value'])):?> checked<?php endif;?>/>
                        <?=$field['label']?>
                    </label>
                </td>
             <?php else:?>
                <label class="btn btn-info btn-xs <?php echo(!empty($field['class']) ? $field['class'] : '' ) ?>">
                    <input
                        style="margin-top: 0px;vertical-align: middle"
                        type="checkbox" name="<?=$fieldName?>"
                        <?php if (!empty($options['value'])):?> checked<?php endif;?>
                         <?php if(isset($field['invertSelect']) && ($field['invertSelect'])): ?> value="0" <?php endif; ?>/>
                    <?=$field['label']?>
                </label>
            <?php endif;?>
        <?php elseif (isset($field['is_intervalpicker'])): ?>
            <td style="white-space: nowrap">
                <input
                    type="text"
                    autocomplete="off"
                    class="form-control input-xs"
                    name="<?php echo $fieldName;?>[date_interval][]"
                    id="<?php echo $options_field_1['id'];?>"
                    <?php if (isset($field['width'])):?>
                        style="width:<?php echo $field['width']?>px;display:inline-block;"
                    <?php endif;?>
                    <?php if (isset($field['readonly'])):?>
                        readonly
                    <?php endif;?>
                    <?php if (isset($date_interval_values[0])):?>
                        value="<?php echo $date_interval_values[0];?>"
                    <?php endif;?>
                />
                 -
                <input
                    type="text"
                    autocomplete="off"
                    class="form-control input-xs"
                    name="<?php echo $fieldName;?>[date_interval][]"
                    id="<?php echo $options_field_2['id'];?>"
                    <?php if (isset($field['width'])):?>
                        style="width:<?php echo $field['width']?>px;display:inline-block;"
                    <?php endif;?>
                    <?php if (isset($field['readonly'])):?>
                        readonly
                    <?php endif;?>
                    <?php if (isset($date_interval_values[1])):?>
                        value="<?php echo $date_interval_values[1];?>"
                    <?php endif;?>
                />
                <?php $this->start('extra-scripts')?>
                <script type="text/javascript">
                    $(function() {
                        $( "#<?=$options_field_1['id']?>" ).datetimepicker({
                            numberOfMonths: 2,
                            dayOfWeekStart: 1,
                            lang:'ro'
                        });

                        $( "#<?=$options_field_2['id']?>" ).datetimepicker({
                            numberOfMonths: 2,
                            dayOfWeekStart: 1,
                            lang:'ro'
                        })
                    });
                </script>
                <?php $this->end()?>
            </td>
        <?php elseif (isset($field['is_intervalpicker_days'])): ?>
            <td style="white-space: nowrap">
                <input
                    type="text"
                    autocomplete="off"
                    class="form-control input-xs"
                    name="<?php echo $fieldName;?>[date_interval_days][]"
                    id="<?php echo $options_field_1['id'];?>"
                    <?php if (isset($field['width'])):?>
                        style="width:<?php echo $field['width']?>px;display:inline-block;"
                    <?php endif;?>
                    <?php if (isset($field['readonly'])):?>
                        readonly
                    <?php endif;?>
                    <?php if (isset($date_interval_days[0])):?>
                        value="<?php echo $date_interval_days[0];?>"
                    <?php endif;?>
                />
                    -
                <input
                    type="text"
                    autocomplete="off"
                    class="form-control input-xs"
                    name="<?php echo $fieldName;?>[date_interval_days][]"
                    id="<?php echo $options_field_2['id'];?>"
                    <?php if (isset($field['width'])):?>
                        style="width:<?php echo $field['width']?>px;display:inline-block;"
                    <?php endif;?>
                    <?php if (isset($field['readonly'])):?>
                        readonly
                    <?php endif;?>
                    <?php if (isset($date_interval_days[1])):?>
                        value="<?php echo $date_interval_days[1];?>"
                    <?php endif;?>
                />
                <?php $this->start('extra-scripts')?>
                <script type="text/javascript">
                    $(function() {
                        $( "#<?=$options_field_1['id']?>" ).datetimepicker({
                            timepicker:false,
                            dayOfWeekStart: 1,
                            closeOnDateSelect: true,
                            lang:'ro',
                            format: 'Y/m/d',
                            onShow: function(ct){
                                this.setOptions({
                                    maxDate:$('#<?=$options_field_2['id']?>').val()?$('#<?=$options_field_2['id']?>').val():false
                                })
                            },
                            onSelectDate:function(ct,$i){
                                $('#<?=$options_field_2['id']?>').focus()
                            }
                        });

                        $( "#<?=$options_field_2['id']?>" ).datetimepicker({
                            timepicker:false,
                            dayOfWeekStart: 1,
                            closeOnDateSelect: true,
                            lang:'ro',
                            format: 'Y/m/d',
                            onShow: function(ct){
                                this.setOptions({
                                    minDate:$('#<?=$options_field_1['id']?>').val()?$('#<?=$options_field_1['id']?>').val():false
                                })
                            }
                        })
                    });
                </script>
                <?php $this->end()?>
            </td>
        <?php elseif (isset($field['is_interval'])): ?>
            <?php if (empty($additional_row_added)):?>
            <td><input
                type="text"
                autocomplete="off"
                class="form-control input-xs" 
                name="<?php echo $fieldName;?>[numeric_interval][]"
                <?php if (isset($field['width'])):?>
                    style="width:<?php echo $field['width']?>px;display:inline-block;"
                <?php endif;?>
                <?php if (isset($field['readonly'])):?>
                    readonly
                <?php endif;?>
                <?php if (isset($numeric_interval_values[0])):?>
                    value="<?php echo $numeric_interval_values[0];?>"
                <?php endif;?>
                /> - <input 
                type="text"
                autocomplete="off"
                class="form-control input-xs"
                name="<?php echo $fieldName;?>[numeric_interval][]" 
                <?php if (isset($field['width'])):?>
                    style="width:<?php echo $field['width']?>px;display:inline-block;"
                <?php endif;?>
                <?php if (isset($field['readonly'])):?>
                    readonly
                <?php endif;?>
                <?php if (isset($numeric_interval_values[1])):?>
                    value="<?php echo $numeric_interval_values[1];?>"
                <?php endif;?>
                /></td>
            <?php else:?>
                <div class="input-group input-group-sm" style="display:inline-flex;">
                    <span class="input-group-addon"><?=$field['label']?></span>
                    <input
                            type="text"
                            autocomplete="off"
                            class="form-control"
                            name="<?php echo $fieldName;?>[numeric_interval][]"
                        <?php if (isset($field['width'])):?>
                            style="width:<?php echo $field['width']?>px;display:inline-block;"
                        <?php endif;?>
                        <?php if (isset($field['readonly'])):?>
                            readonly
                        <?php endif;?>
                        <?php if (isset($numeric_interval_values[0])):?>
                            value="<?php echo $numeric_interval_values[0];?>"
                        <?php endif;?>
                    />
                    <input
                            type="text"
                            autocomplete="off"
                            class="form-control"
                            name="<?php echo $fieldName;?>[numeric_interval][]"
                        <?php if (isset($field['width'])):?>
                            style="width:<?php echo $field['width']?>px;display:inline-block;"
                        <?php endif;?>
                        <?php if (isset($field['readonly'])):?>
                            readonly
                        <?php endif;?>
                        <?php if (isset($numeric_interval_values[1])):?>
                            value="<?php echo $numeric_interval_values[1];?>"
                        <?php endif;?>
                    />
                </div>
            <?php endif;?>
        <?php elseif (isset($field['exact'])): ?>
            <td><input
                type="text"
                autocomplete="off"
                class="form-control input-xs"
                name="<?php echo $fieldName;?>[exact]"
                <?php if (isset($field['width'])):?>
                    style="width:<?php echo $field['width']?>px;display:inline-block;"
                <?php endif;?>
                <?php if (isset($field['readonly'])):?>
                    readonly
                <?php endif;?>
                <?php if (isset($exact_match_value)):?>
                    value="<?php echo $exact_match_value;?>"
                <?php endif;?>
                />
            </td>
        <?php elseif (isset($field['is_multipleselect'])): ?>
            <?php if (isset($additional_row_added)): ?>
                <?=$this->element('Toolkit.multiple_select', [
                    'field' => $field,
                    'fieldName' => $fieldName,
                    'field_index' => $field_index,
                    'multiselect_values' => $multiselect_values,
                    'class' => (isset($options['class']) ? $options['class'] : ""),
                    'data_live_search' => (isset($options['data-live-search']) ? $options['data-live-search'] : ""),
                    'filter' => (isset($options['filter']) && $options['filter'] == true) ? true : false,
                    'single' => (isset($options['single']) && $options['single'] == true) ? true : false,
                    'select_all' => (isset($options['select_all']) && $options['select_all'] == true) ? true : false,
                    'opt_groups' => (!empty($options['opt_groups']) ? $options['opt_groups'] : false)
                ])?>
            <?php else:?>
                <td>
                    <?=$this->element('Toolkit.multiple_select', [
                        'field' => $field,
                        'fieldName' => $fieldName,
                        'field_index' => $field_index,
                        'multiselect_values' => (isset($multiselect_values) && !empty($multiselect_values)) ? $multiselect_values : [],
                        'class' => (isset($options['class']) ? $options['class'] : ""),
                        'data_live_search' => (isset($options['data-live-search']) ? $options['data-live-search'] : ""),
                        'filter' => (isset($options['filter']) && $options['filter'] == true) ? true : false,
                        'single' => (isset($options['single']) && $options['single'] == true) ? true : false,
                        'select_all' => (isset($options['select_all']) && $options['select_all'] == true) ? true : false,
                        'opt_groups' => (!empty($options['opt_groups']) ? $options['opt_groups'] : false)
                    ])?>
                </td>
            <?php endif;?>
        <?php elseif (isset($field['add_additional_row'])): ?>
            <?php $additional_row_added = true;?>
                <td class="text-right">
                    <button class="btn btn-success btn-xs" type="submit"><?=__('Cauta')?></button>
                    <?php if (isset($filterForm['reset']))?>
                    <a href="<?=$filterForm['reset']?>"
                       class="btn btn-default btn-xs"><?=__('Reset')?></a>
                </td>
            </tr>
            <tr>
                <td colspan="999" class="text-right">
        <?php else:?>
            <?php if (empty($additional_row_added)):?>
                <?php echo '<td>'.$this->Form->input($fieldName, $options).'</td>'; ?>
            <?php else:?>
                <input
                        type="text"
                        autocomplete="off"
                        class="form-control input-xs"
                        name="<?=$fieldName?>"
                        placeholder="<?=$field['label']?>"
                    <?php if (isset($field['width'])):?>
                        style="width:<?php echo $field['width']?>px;display:inline-block;"
                    <?php endif;?>

                    <?php if (!empty($options['value'])):?>
                        value="<?=$options['value']?>"
                    <?php endif;?>
                />
            <?php endif;?>
        <?php endif;?>
        
    <?php else: ?>
        <?php echo '<td>'.$this->Form->input($field, $options).'</td>'?>
    <?php endif;?>
    
<?php endforeach;?>
<?php if (empty($additional_row_added)): ?>
    <td class="text-right">
        <button class="btn btn-success btn-xs" type="submit"><?=__('Cauta')?></button>
            <?php if (isset($filterForm['reset']))?>
            <a href="<?=$filterForm['reset']?>"
        class="btn btn-default btn-xs"><?=__('Reset')?></a>
    </td>
<?php endif;?>
</tr>
<?php echo $this->Form->end();?>