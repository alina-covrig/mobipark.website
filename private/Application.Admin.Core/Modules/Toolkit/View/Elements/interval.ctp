<input
    type="text"
    autocomplete="off"
    class="form-control input-xs"
    name="<?php echo $fieldName; ?>[numeric_interval][]"
    <?php if (isset($field['width'])): ?>
        style="width:<?php echo $field['width'] ?>px;display:inline-block;"
    <?php endif; ?>
    <?php if (isset($field['readonly'])): ?>
        readonly
    <?php endif; ?>
    <?php if (isset($numeric_interval_values[0])): ?>
        value="<?php echo $numeric_interval_values[0]; ?>"
    <?php endif; ?>
/> - <input
    type="text"
    autocomplete="off"
    class="form-control input-xs"
    name="<?php echo $fieldName; ?>[numeric_interval][]"
    <?php if (isset($field['width'])): ?>
        style="width:<?php echo $field['width'] ?>px;display:inline-block;"
    <?php endif; ?>
    <?php if (isset($field['readonly'])): ?>
        readonly
    <?php endif; ?>
    <?php if (isset($numeric_interval_values[1])): ?>
        value="<?php echo $numeric_interval_values[1]; ?>"
    <?php endif; ?>
/>