<input
    type="text"
    autocomplete="off"
    class="form-control input-xs"
    name="<?php echo $fieldName; ?>[exact]"
    <?php if (isset($field['width'])): ?>
        style="width:<?php echo $field['width'] ?>px;display:inline-block;"
    <?php endif; ?>
    <?php if (isset($field['readonly'])): ?>
        readonly
    <?php endif; ?>
    <?php if (isset($exact_match_value)): ?>
        value="<?php echo $exact_match_value; ?>"
    <?php endif; ?>
/>