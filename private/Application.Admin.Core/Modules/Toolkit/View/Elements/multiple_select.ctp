
<select
    id="<?php if (isset($field['id'])): ?><?= $field['id']; ?><?php else: ?>ms-<?php echo alias ($field_index); endif; ?>"
    multiple="multiple"
    name="<?php echo $fieldName; ?>[n][]"
    class="<?= (isset($class) ? $class : "" )?>"
    data-live-search="<?= (isset($data_live_search) ? $data_live_search : "false")?>"
    style="width: 130px"
>
    <?php if(isset($field['opt_groups']) && !empty($field['opt_groups'])):?>
        <?php foreach(${$field['data']} as $ms_groups):?>
            <optgroup label="<?= $ms_groups['group'] ?>">
                <?php foreach($ms_groups['items'] as $id => $item):?>
                    <?php $ms_selected = (isset($multiselect_values) && in_array ($id, $multiselect_values)); ?>
                    <option
                            value="<?php echo $id; ?>"
                            <?php echo($ms_selected)? 'selected':''?>>
                        <?php echo $item; ?>
                    </option>
                <?php endforeach;?>
            </optgroup>
        <?php endforeach;?>
    <?php else:?>
        <?php if (isset($field['data']) && isset(${$field['data']})): ?>
            <?php
            $group_is_open = false;
            foreach (${$field['data']} as $id => $ms_item):?>
                <?php $ms_selected = (isset($multiselect_values) && in_array ($id, $multiselect_values)); ?>
                <?php $data = ($field['data'] == 'categories' && $this->H->getParentCategory ($id)) ? $this->H->getParentCategory ($id) : 0; ?>
                <option
                    value="<?php echo $id; ?>"<?php if ($ms_selected): ?> selected<?php endif; ?>
                    <?php if ($data) : ?>data-parent="<?= $data ?>" <?php endif; ?>><?php echo $ms_item; ?></option>
            <?php endforeach; ?>
        <?php endif; ?>
    <?php endif;?>
</select>
<?php $this->start ('extra-scripts') ?>
<script type="text/javascript">
    $(function () {
        $('#ms-<?php echo alias ($field_index)?>').change(function () {
            $(this).siblings('div').addClass("multiple_select" );
        }).multipleSelect({

            <?php $ms_options = []; ?>

            <?php if (!empty($field['label'])) : ?>
                <?php $ms_options[] = "placeholder: '".$field['label']."'"; ?>
            <?php endif;?>

            <?php if (isset($field['width'])) : ?>
                <?php $ms_options[] = "width: '".$field['width']."px'"; ?>
            <?php else : ?>
                <?php $ms_options[] = "width: '100%'"; ?>
            <?php endif;?>

            <?php if (isset($field['filter']) && $field['filter'] == true) : ?>
                <?php $ms_options[] = "filter: true"; ?>
            <?php endif; ?>

            <?php if (isset($field['single']) && $field['single'] == true) : ?>
                <?php $ms_options[] = "single: true"; ?>
            <?php endif; ?>

            <?php if (isset($field['select_all']) && $field['select_all'] == false) : ?>
                <?php $ms_options[] = "selectAll: false"; ?>
            <?php endif; ?>

            <?php echo implode(",", $ms_options); ?>
        });

        $('#page-wrapper').click(function (e) {
            if ($(e.target).closest('.ms-parent').length != 0) {
                return;
            }
            if ($('.ms-parent .open').length) {
                $('.ms-choice').click();
            }
        });
    });
</script>
<?php $this->end () ?>