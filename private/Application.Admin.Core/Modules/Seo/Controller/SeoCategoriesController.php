<?php
/**
* SeoCategoriesController
*
* This file is module-wide controller file. You can put all
* module-wide controller-related methods here.
*
* PHP version 5.4
*
* @category  Admin
* @package   SeoCategories
* @author    Daniel Ionascu <daniel.ionascu88@gmail.com>
* @copyright 2014 Evercloud
* @license   http://www.php.net/license/3_01.txt  PHP License 3.01
* @version   SVN: $Id$
* @link      /modules/seo/controller/seo/seo_categories
* @since     1.0
*/

App::uses('AppController', 'Controller');

/**
* OrdersAppController Class
*
* Inside this Controller admin logic methods will
* be placed
*
* @category  Admin
* @package   SeoCategories
* @author    Daniel Ionascu <daniel.ionascu88@gmail.com>
* @copyright 2014 Evercloud
* @license   http://www.php.net/license/3_01.txt  PHP License 3.01
* @version   Release: 1.0
* @link      /modules/seo/controller/seo/seo_categories
* @since     Class available since Release 1.0
*/
class SeoCategoriesController extends SeoAppController
{
    public $uses = array(
        'Seo.SeoCategory',
        'Catalog.Category'
    );
    
    /**
     * The edit method
     *
     * Category seo can be edited and modified as wished
     *
     * @param integer $id the Category id to be edited
     *
     * @return void
     */
    public function edit($id) 
    {
        if (!$id) {
            $this->Session->setFlash(
                __('Trebuie specificata o categorie'), 
                'Alerts/warning'
            );
            return $this->redirect($this->request->referer());
        }
        
        $this->Category->recursive = -1;
        $this->Category->contain('SeoCategory');
        $category = $this->Category->findById($id);
        if (!$category) {
            $this->Session->setFlash(
                __('Categorie invalida'), 
                'Alerts/warning'
            );
            return $this->redirect($this->request->referer());
        }
        if ($this->request->is(array('post',  'put'))) {
            
            if (!empty ($category['SeoCategory']['id'])) {
                $seoId = $category['SeoCategory']['id'];
                $this->request->data['SeoCategory']['id'] = $seoId;
            }
            if ($this->SeoCategory->save($this->request->data)) {
                return $this->respond(
                    __('Actualizat!'),
                    'success'
                );
            }
        }
        
        // set post data
        if (!$this->request->data) {
            $this->request->data = $category;
        } else {
            $this->request->data['Category'] = $category['Category']; 
        }
    }
}
