<?php
/**
* SeoProductsController
*
* This file is module-wide controller file. You can put all
* module-wide controller-related methods here.
*
* PHP version 5.4
*
* @category  Admin
* @package   SeoProducts
* @author    Daniel Ionascu <daniel.ionascu88@gmail.com>
* @copyright 2014 Evercloud
* @license   http://www.php.net/license/3_01.txt  PHP License 3.01
* @version   SVN: $Id$
* @link      /modules/seo/controller/seo/seoproducts
* @since     1.0
*/

App::uses('AppController', 'Controller');

/**
* OrdersAppController Class
*
* Inside this Controller admin logic methods will
* be placed
*
* @category  Admin
* @package   SeoProducts
* @author    Daniel Ionascu <daniel.ionascu88@gmail.com>
* @copyright 2014 Evercloud
* @license   http://www.php.net/license/3_01.txt  PHP License 3.01
* @version   Release: 1.0
* @link      /modules/seo/controller/seo/seoproducts
* @since     Class available since Release 1.0
*/
class SeoProductsController extends SeoAppController
{
    public $uses = array(
        'Seo.SeoProduct',
        'Catalog.Product'
    );
    
    /**
     * The edit method
     *
     * Product seo can be edited and modified as wished
     *
     * @param integer $id the Product id to be edited
     *
     * @return void
     */
    public function edit($id) 
    {
        if (!$id) {
            $this->Session->setFlash(
                __('Trebuie specificat un produs'), 
                'Alerts/warning'
            );
            return $this->redirect('/catalog/products');
        }
        
        $this->Product->recursive = -1;
        $this->Product->contain('ProductAliasName.name', 'SeoProduct');
        $product = $this->Product->findById($id);
        if (!$product) {
            $this->Session->setFlash(
                __('Produs invalid'), 
                'Alerts/warning'
            );
            return $this->redirect('/catalog/products');
        }
        if ($this->request->is(array('post',  'put'))) {
            $seoProduct = $this->SeoProduct->findByProductId($id);
            if (!empty ($seoProduct)) {
                $seoId = $seoProduct['SeoProduct']['id'];
                $this->request->data['SeoProduct']['id'] = $seoId;
            }
            if ($this->SeoProduct->save($this->request->data)) {
                return $this->respond(
                    __('Actualizat!'),
                    'success'
                );
            }
        }
        
        // set post data
        if (!$this->request->data) {
            $this->request->data = $product; 
        } else {
            $productAlias = $product['ProductAliasName'];
            $this->request->data['ProductAliasName'] = $productAlias;
            $this->request->data['Product'] = $product['Product'];
        }
    }
}
