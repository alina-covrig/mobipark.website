<?php
/**
 * SeoCategoryTagKeywordsController
 *
 * This file is module-wide controller file. You can put all
 * module-wide controller-related methods here.
 *
 * PHP version 5.4
 *
 * @category  Admin
 * @package   Seo
 * @author    Daniel Ionascu <daniel.ionascu88@gmail.com>
 * @copyright 2014 Evercloud
 * @license   http://www.php.net/license/3_01.txt  PHP License 3.01
 * @version   SVN: $Id$
 * @link      /modules/seo/controller/seo/seo_category_tag_keywords
 * @since     1.0
 */

App::uses('AppController', 'Controller');

/**
 * SeoCategoryTagKeywordsController Class
 *
 * Inside this Controller admin logic methods will
 * be placed
 *
 * @category  Admin
 * @package   Seo
 * @author    Daniel Ionascu <daniel.ionascu88@gmail.com>
 * @copyright 2014 Evercloud
 * @license   http://www.php.net/license/3_01.txt  PHP License 3.01
 * @version   Release: 1.0
 * @link      /modules/seo/controller/seo/seo_category_tag_keywords
 * @since     Class available since Release 1.0
 */
class SeoCategoryTagKeywordsController extends SeoAppController
{
    public $uses = array(
        'Catalog.Tag',
        'Catalog.TagCategory',
        'Catalog.Category',
        'Catalog.ProductAliasName',
        'Catalog.ProductAliasNameTag',
        'Seo.CategoryTagKeyword',
        'Seo.AdWordsCampaign',
        'Seo.AdwordsAdGroup',
    );

    /**
     * The components used by this controller
     *
     * @var
     */
    public $components = array(
        'Paginator',
        'Toolkit.FilterForm',
    );

    //private $_customer_id = '878-514-4308'; // LOCAL
    private $_customer_id = '509-708-6365'; //ONLINE

    /**
     * The add method
     *
     * Add keywords to a specific category-tag combination
     *
     * @return void
     */
    public function add()
    {
        if ($this->request->is('post')) {

            $data = $this->request->data['CategoryTagKeyword'];

            $keywords = [];
            $message_array = [];

            foreach ($data['category_id'] as $k => $d) {

                $tag = $this->Tag->findByName($data['tag_id'][$k], ['id']);
                if (empty($tag)) {
                    $this->respondWithArray = true;
                    $message_array[] = __('Tagul ') . $data['tag_id'][$k]
                        . ' nu a putut fi asociat';
                    continue;
                }

                $find = $this->CategoryTagKeyword->find(
                    'first', [
                        'conditions' => [
                            'keyword' => trim($data['keyword'][$k]),
                            'ad_group_id' => $data['ad_group_id'][$k]
                        ]
                    ]
                );

                if (!empty($find)) {
                    $this->respondWithArray = true;
                    $message_array[] = "Keyword-ul " . $data['keyword'][$k]
                        . " exista deja in Ad Group-ul selectat";
                    continue;
                }

                $keywords[$k]['category_id'] = $d;
                $keywords[$k]['tag_id'] = $tag['Tag']['id'];
                $keywords[$k]['keyword'] = trim($data['keyword'][$k]);
                $keywords[$k]['url'] = $data['url'][$k];
                $keywords[$k]['ad_group_id'] = $data['ad_group_id'][$k];
                $keywords[$k]['created_at'] = date('Y-m-d H:i:s');
                $keywords[$k]['is_active'] = 0;
                $keywords[$k]['is_stopped'] = 1;
            }

            if (!$this->CategoryTagKeyword->saveMany($keywords)) {
                $this->respond(
                    __('A intervenit o eroare la salvarea datelor (1)'),
                    'error'
                );
            }

            $this->CategoryTagKeyword->initGoogle();

            try {

                $user = new AdWordsUser();
                $user->SetClientCustomerId($this->_customer_id);

                $user->LogAll();

                if ($this->_addKeywords($user, $keywords)) {

                    if (!empty($message_array)) {
                        $message_array[] = __('Keywordurile au fost '
                            . 'adaugate cu succes!');
                        $this->respondWithArray = false;
                        return $this->respond(
                            implode('<br />', $message_array),
                            'info'
                        );
                    }
                    $this->respond(
                        __('Keywordurile au fost adaugate cu succes!'),
                        'success'
                    );
                } else {
                    if (!empty($message_array)) {
                        $message_array[] = __("A intervenit o eroare la "
                            . "trimiterea de keywords!");
                        $this->respondWithArray = false;
                        return $this->respond(
                            implode('<br />', $message_array),
                            'info'
                        );
                    }
                    $this->respond(
                        __('A intervenit o eroare la trimiterea de keywords!'),
                        'success'
                    );
                }
            } catch (Exception $e) {
                $this->respond(
                    __("A aparut o eroare la trimitere: %s\n", $e->getMessage()),
                    'error'
                );
            }

            $this->respond(
                __('Datele au fost salvate cu succes'),
                'success'
            );
        }

        $categories = $this->Category->find(
            'list', array(
                'conditions' => array(
                    '(rght - lft)' => 1
                )
            )
        );

        $this->AdWordsCampaign->bindModel(
            [
                'hasMany' => [
                    'AdwordsAdGroup' => [
                        'className' => 'Seo.AdwordsAdGroup',
                        'foreignKey' => 'campaign_id',
                        'fields' => ['id', 'name']
                    ]
                ]
            ]
        );

        $campaigns = $this->AdWordsCampaign->find('all');
        $adGroups = [];

        if (!empty($campaigns)) {
            foreach ($campaigns as $c) {
                if (!empty($c['AdwordsAdGroup'])) {
                    foreach ($c['AdwordsAdGroup'] as $g) {
                        $adGroups[$c['AdWordsCampaign']['name']][$g['id']]
                            = $g['name'];
                    }
                }
            }
        }

        $tags = $this->Tag->find('list');

        $this->set(compact(
            'categories',
            'tags',
            'adGroups'
        ));
    }

    /**
     * The getTags method
     *
     * This method is used for typeahead
     *
     * @return void
     */
    public function getTags()
    {
        if (isset($_GET['query']) && !empty($_GET['query'])) {
            $data = $this->Tag->search($_GET['query']);
            $returnData = array();
            foreach ($data as $key => $value) {
                $returnData[$key]['id'] = $value['Tag']['id'];
                $returnData[$key]['name'] = $value['Tag']['name'];
            }

            die(json_encode($returnData));
        } else {
            return $this->respond(
                __('Intoduceti un termen de cautare')
            );
        }
    }

    /**
     * The getLinks method
     *
     * Get link, stock, products number for
     * current tag + category
     *
     * @return respond
     */
    public function getLinks()
    {
        if (!$this->request->is('ajax')) {
            $this->respond(
                __('A aparut o eroare!'),
                'error'
            );
        }

        $this->layout = false;
        $this->autoRender = false;

        $stock = 0;
        $noOfProducts = 0;

        $this->Category->id = $this->request->data['category'];
        $category = $this->Category->field('alias');

        $tag = $this->Tag->find(
            'first', [
                'conditions' => [
                    'Tag.name' => $this->request->data['tag']
                ],
                'fields' => ['Tag.alias', 'Tag.id']
            ]
        );

        if (!empty($tag)) {
            $productAliasNamesIds = $this->ProductAliasNameTag->find(
                'list', [
                    'conditions' => [
                        'tag_id' => $tag['Tag']['id']
                    ],
                    'fields' => ['product_alias_name_id']
                ]
            );

            $this->ProductAliasName->virtualFields = [
                'stock' => 'get_stock_with_supplier(product_id)'
            ];

            $this->ProductAliasName->contain(['Product']);

            $stock = $this->ProductAliasName->find(
                'list', [
                    'conditions' => [
                        'ProductAliasName.id' => $productAliasNamesIds,
                        'ProductAliasName.is_active' => 1,
                        'Product.is_active' => 1,
                        'ProductAliasName.category_id'
                        => $this->request->data['category']
                    ],
                    'fields' => ['stock'],
                    'group' => 'product_id'
                ]
            );

            $productCount = $this->ProductAliasName->find(
                'count', [
                    'conditions' => [
                        'ProductAliasName.id' => $productAliasNamesIds,
                        'ProductAliasName.is_active' => 1,
                        'ProductAliasName.category_id'
                        => $this->request->data['category'],
                        'Product.is_active' => 1,
                        'Product.stock >' => 0
                    ]
                ]
            );

            $noOfProducts = $productCount;
            $stock = array_sum($stock);
        }

        if (!empty($category) && !empty($tag)) {

            $url = Configure::read('STORE_URL')
                . 'accesorii-piese-telefoane/'
                . $category
                . '_'
                . $tag['Tag']['alias']
                . '/f:ord=pret-mic;s=1';

            return json_encode([
                'url' => $url,
                'stock' => $stock,
                'noOfProducts' => $noOfProducts
            ]);
        } else {
            return json_encode([
                'url' => '',
                'stock' => 0,
                'noOfProducts' => 0
            ]);
        }
    }

    /**
     * The index method
     *
     * This method displays the keywords
     *
     * @return void
     */
    public function index()
    {
        $conditions = [];
        $this->_setFilterFields();
        if (!empty($this->request->query)) {
            $conditions
                = $this->FilterForm->parseConditions($this->request->query);

            if (isset($conditions['AdwordsAdGroup.campaign LIKE'])) {
                $conditions['(SELECT name FROM seo__campaigns c '
                . 'WHERE c.id = AdwordsAdGroup.campaign_id) LIKE']
                    = $conditions['AdwordsAdGroup.campaign LIKE'];
                unset($conditions['AdwordsAdGroup.campaign LIKE']);
            }
        }

        $this->CategoryTagKeyword->bindModel(
            [
                'belongsTo' => [
                    'Category' => [
                        'className' => 'Catalog.Category',
                        'foreignKey' => 'category_id',
                        'fields' => ['name']
                    ],
                    'Tag' => [
                        'className' => 'Catalog.Tag',
                        'foreignKey' => 'tag_id',
                        'fields' => ['name']
                    ],
                    'AdwordsAdGroup' => [
                        'className' => 'Seo.AdwordsAdGroup',
                        'foreignKey' => 'ad_group_id',
                        'fields' => ['name', 'campaign']
                    ],
                ]
            ]
        );

        $this->AdwordsAdGroup->virtualFields = [
            'campaign' => 'SELECT name FROM seo__campaigns c '
                . 'WHERE c.id = AdwordsAdGroup.campaign_id'
        ];

        $this->Paginator->settings = [
            'order' => 'created_at DESC',
            'conditions' => $conditions
        ];

        $keywords = $this->Paginator->paginate('CategoryTagKeyword');

        foreach ($keywords as $i => $key) {
            $productAliasNamesIds = $this->ProductAliasNameTag->find(
                'list', [
                    'conditions' => [
                        'tag_id' => $key['CategoryTagKeyword']['tag_id']
                    ],
                    'fields' => ['product_alias_name_id']
                ]
            );

            $this->ProductAliasName->virtualFields = [
                'stock' => 'get_stock_with_supplier(product_id)'
            ];

            $this->ProductAliasName->contain();

            $stock = $this->ProductAliasName->find(
                'list', [
                    'conditions' => [
                        'ProductAliasName.id' => $productAliasNamesIds,
                        'ProductAliasName.is_active' => 1,
                        'ProductAliasName.category_id'
                        => $key['CategoryTagKeyword']['category_id']
                    ],
                    'fields' => ['stock'],
                    'group' => 'product_id'
                ]
            );

            $productCount = $this->ProductAliasName->find(
                'count', [
                    'conditions' => [
                        'ProductAliasName.id' => $productAliasNamesIds,
                        'ProductAliasName.is_active' => 1,
                        'ProductAliasName.category_id'
                        => $key['CategoryTagKeyword']['category_id'],
                        'Product.is_active' => 1,
                        'Product.stock >' => 0
                    ]
                ]
            );

            $keywords[$i]['CategoryTagKeyword']['stock']
                = array_sum($stock);
            $keywords[$i]['CategoryTagKeyword']['products_nr']
                = $productCount;
        }

        $this->AdWordsCampaign->bindModel(
            [
                'hasMany' => [
                    'AdwordsAdGroup' => [
                        'className' => 'Seo.AdwordsAdGroup',
                        'foreignKey' => 'campaign_id',
                        'fields' => ['id', 'name', 'ad_group_id']
                    ]
                ]
            ]
        );

        $campaigns = $this->AdWordsCampaign->find('all');
        $adGroups = [];

        if (!empty($campaigns)) {
            foreach ($campaigns as $c) {
                if (!empty($c['AdwordsAdGroup'])) {
                    foreach ($c['AdwordsAdGroup'] as $g) {
                        $adGroups[$c['AdWordsCampaign']['name']][$g['ad_group_id']]
                            = $g['name'];
                    }
                }
            }
        }

        $this->set(compact(
            'keywords',
            'adGroups'
        ));
    }

    /**
     * The _setFilterFields method
     *
     * This method sets the filterForm variable to the view to generate the
     * filter row
     *
     * @return void
     */
    private function _setFilterFields()
    {
        $this->set('boolDdn', [
            '0' => __('Paused'),
            '1' => __('Enabled')
        ]);
        $filterForm = array(
            'name' => '',
            'reset' => '/seo/seo_category_tag_keywords',
            'fields' => array(
                'CategoryTagKeyword[id]' => array(
                    'name' => 'CategoryTagKeyword[id]',
                    'label' => 'Id',
                    'width' => 50
                ),
                'AdwordsAdGroup[campaign]' => array(
                    'name' => 'AdwordsAdGroup[campaign]',
                    'label' => __('Campanie')
                ),
                'AdwordsAdGroup[name]' => array(
                    'name' => 'AdwordsAdGroup[name]',
                    'label' => __('Ad Group')
                ),
                'Category[name]' => array(
                    'name' => 'Category[name]',
                    'label' => __('Categorie'),
                ),
                'Tag[name]' => array(
                    'name' => 'Tag[name]',
                    'label' => __('Tag'),
                ),
                'CategoryTagKeyword[keyword]' => array(
                    'name' => 'CategoryTagKeyword[keyword]',
                    'label' => __('Keywords'),
                ),
                'CategoryTagKeyword[url]' => array(
                    'name' => 'CategoryTagKeyword[url]',
                    'label' => __('URL')
                ),
                null,
                null,
                'CategoryTagKeyword[is_active]' => array(
                    'name' => 'CategoryTagKeyword[is_active]',
                    'label' => __('---'),
                    'width' => 70,
                    'data' => 'boolDdn'
                ),
                'CategoryTagKeyword[is_stopped]' => array(
                    'name' => 'CategoryTagKeyword[is_stopped]',
                    'label' => __('Oprit'),
                    'type' => 'checkbox',
                ),
            ),
        );
        $this->set(compact('filterForm'));
    }

    /**
     * The _addKeywords method
     *
     * @param AdWordsUser $user The user object
     * @param array $keywords The keywords array to be send
     *
     * This method send keywords to adword account
     * and update keyword_id in database
     *
     * @return boolean
     */
    private function _addKeywords(AdWordsUser $user, $keywords)
    {

        $adGroupCriterionService =
            $user->GetService('AdGroupCriterionService', ADWORDS_VERSION);

        $operations = array();
        foreach ($keywords as $k) {
            $this->AdwordsAdGroup->id = $k['ad_group_id'];
            $adGroupId = $this->AdwordsAdGroup->field('ad_group_id');
            // Create keyword criterion.

            $keyword = new Keyword();
            $keyword->text = $this->_getKeywordText($k['keyword']);
            $keyword->matchType = $this->_getMatchType($k['keyword']);

            // Create biddable ad group criterion.
            $adGroupCriterion = new BiddableAdGroupCriterion();
            $adGroupCriterion->adGroupId = $adGroupId;
            $adGroupCriterion->criterion = $keyword;

            // Set additional settings (optional).
            $adGroupCriterion->userStatus = 'PAUSED';
            $adGroupCriterion->finalUrls = array($k['url']);

            // Set bids (optional).
            //$bid = new CpcBid();
            //$bid->bid =  new Money(50000);
            //$biddingStrategyConfiguration = new BiddingStrategyConfiguration();
            //$biddingStrategyConfiguration->bids[] = $bid;
            //$adGroupCriterion->biddingStrategyConfiguration =
            //    $biddingStrategyConfiguration;
            //$adGroupCriteria[] = $adGroupCriterion;

            // Create operation.
            $operation = new AdGroupCriterionOperation();
            $operation->operand = $adGroupCriterion;
            $operation->operator = "ADD";
            $operations[] = $operation;
        }

        $response = $adGroupCriterionService->mutate($operations);

        foreach ($response->value as $k => $r) {
            $keywordToFind = $this->_getDatabaseKeywordText(
                $r->criterion->text,
                $r->criterion->matchType
            );

            $ad_group = $this->AdwordsAdGroup->findByAdGroupId($r->adGroupId);

            $find = $this->CategoryTagKeyword->find(
                'first', [
                    'conditions' => [
                        'url' => $r->finalUrls->urls[0],
                        'keyword' => $keywordToFind,
                        'ad_group_id' => $ad_group['AdwordsAdGroup']['id']
                    ]
                ]
            );

            $this->CategoryTagKeyword->id = $find['CategoryTagKeyword']['id'];
            $this->CategoryTagKeyword->saveField('keyword_id', $r->criterion->id);
        }

        return empty($response->partialFailureErrors) ? true : false;
    }

    /**
     * The _getAdGroups method
     *
     * This method gets the Ad Groups from AdWords
     *
     * @param AdWordsUser $user The user object
     * @param integer $campaignId The Adwords Campaign Id
     *
     * @return array
     */
    private function _getAdGroups(AdWordsUser $user, $campaignId)
    {
        $adGroups = [];

        $adGroupService = $user->GetService('AdGroupService', ADWORDS_VERSION);

        $campaign_id_bd = $this->AdWordsCampaign->findByCampaignId(
            $campaignId, [
                'id'
            ]
        );

        if (empty($campaign_id_bd)) {
            $this->respond(
                __('Sincronizati campaniile inainte de grupuri.'),
                'error'
            );
        }

        $selector = new Selector();
        $selector->fields = array('Id', 'Name');
        $selector->ordering[] = new OrderBy('Name', 'ASCENDING');

        $selector->predicates[] =
            new Predicate('CampaignId', 'IN', array($campaignId));

        $selector->paging = new Paging(0, AdWordsConstants::RECOMMENDED_PAGE_SIZE);
        do {
            $page = $adGroupService->get($selector);

            if (isset($page->entries)) {
                foreach ($page->entries as $k => $adGroup) {
                    $adGroups[$k]['ad_group_id'] = $adGroup->id;
                    $adGroups[$k]['name'] = $adGroup->name;
                    $adGroups[$k]['campaign_id']
                        = $campaign_id_bd['AdWordsCampaign']['id'];
                }
            }

            $selector->paging->startIndex += AdWordsConstants::RECOMMENDED_PAGE_SIZE;
        } while ($page->totalNumEntries > $selector->paging->startIndex);

        return $adGroups;
    }

    /**
     * The _getCampaigns method
     *
     * This method gets the Campaigns from AdWords
     *
     * @param AdWordsUser $user The user object
     * @param integer $list Return 'list' or 'all' results
     *
     * @return array
     */
    private function _getCampaigns(AdWordsUser $user, $list = 0)
    {

        $campaignService = $user->GetService('CampaignService', ADWORDS_VERSION);
        $campaigns = [];

        $selector = new Selector();
        $selector->fields = array('Id', 'Name');
        $selector->ordering[] = new OrderBy('Name', 'ASCENDING');

        $selector->paging = new Paging(0, AdWordsConstants::RECOMMENDED_PAGE_SIZE);
        do {

            $page = $campaignService->get($selector);

            if (isset($page->entries)) {
                foreach ($page->entries as $k => $campaign) {
                    if ($list) {
                        $campaigns[$campaign->id] = $campaign->name;
                    } else {
                        $campaigns[$k]['campaign_id'] = $campaign->id;
                        $campaigns[$k]['name'] = $campaign->name;
                    }
                }
            }

            $selector->paging->startIndex += AdWordsConstants::RECOMMENDED_PAGE_SIZE;
        } while ($page->totalNumEntries > $selector->paging->startIndex);

        return $campaigns;
    }

    /**
     * The saveCampaigns method
     *
     * This method syncronize campaigns from AdWords account to database
     *
     * @return respond
     */
    public function saveCampaigns()
    {
        $this->layout = false;
        $this->autoRender = false;

        $this->CategoryTagKeyword->initGoogle();

        $user = new AdWordsUser();
        $user->SetClientCustomerId($this->_customer_id);

        $adwordsCampaigns = $this->_getCampaigns($user);

        $save = [];
        foreach ($adwordsCampaigns as $k => $campaign) {
            $find = $this->AdWordsCampaign->findByCampaignId(
                $campaign['campaign_id']
            );
            if (empty($find)) {
                $save[$k]['AdWordsCampaign'] = $campaign;
            }
        }

        if (empty($save)) {
            $this->respond(
                __('Nu a fost efectuata nicio modificare asupra campaniilor.'),
                'success'
            );
        }

        $this->AdWordsCampaign->create();
        if (!$this->AdWordsCampaign->saveMany($save)) {
            $this->respond(
                __('A aparut o eroare la sincronizarea campaniilor.'),
                'error'
            );
        }

        $this->respond(
            __('Sincronizarea campaniilor s-a efectuat cu succes.'),
            'success'
        );
    }

    /**
     * The saveAdGroups method
     *
     * This method syncronize Ad Groups from AdWords account to database
     *
     * @return respond
     */
    public function saveAdGroups()
    {
        $this->CategoryTagKeyword->initGoogle();

        $user = new AdWordsUser();
        $user->SetClientCustomerId($this->_customer_id);

        $campaigns = $this->_getCampaigns($user, 1);
        $save = [];

        if (!empty($campaigns)) {
            foreach ($campaigns as $id => $name) {
                $adGroup = $this->_getAdGroups($user, $id);
                foreach ($adGroup as $ad) {
                    $find = $this->AdwordsAdGroup
                        ->findByAdGroupId($ad['ad_group_id']);

                    if (empty($find)) {
                        $save[$ad['ad_group_id']] = $ad;
                    }
                }
            }
        }

        if (empty($save)) {
            $this->respond(
                __('Nu a fost efectuata nicio modificare.'),
                'success'
            );
        }

        $this->AdwordsAdGroup->create();
        if (!$this->AdwordsAdGroup->saveMany($save)) {
            $this->respond(
                __('A aparut o eroare la sincronizare.'),
                'error'
            );
        }

        $this->respond(
            __('Sincronizarea s-a efectuat cu succes.'),
            'success'
        );
    }

    /**
     * The _updateKeywordStatus method
     *
     * @param AdWordsUser $user The user object
     * @param array $keywords Keywords to be enabled or paused
     * @param string $status Status to be set
     *
     * @return bool
     */
    private function _updateKeywordStatus(
        AdWordsUser $user,
        $keywords,
        $status = 'ENABLED'
    )
    {
        $adGroupCriterionService =
            $user->GetService('AdGroupCriterionService', ADWORDS_VERSION);

        foreach ($keywords as $k) {
            $adGroupCriterion = new BiddableAdGroupCriterion();
            $adGroupCriterion->adGroupId = $k['ad_group_id'];

            $adGroupCriterion->criterion = new Criterion($k['keyword_id']);
            $adGroupCriterion->userStatus = $status;

            $operation = new AdGroupCriterionOperation();
            $operation->operand = $adGroupCriterion;
            $operation->operator = 'SET';
            $operations[] = $operation;
        }
        $result = $adGroupCriterionService->mutate($operations);
        return empty($result->partialFailureErrors) ? true : false;
    }

    /**
     * The changeKeywordStatus method
     *
     * This method change the keywords status in database
     *
     * return json
     */
    public function changeKeywordStatus()
    {
        if (!$this->request->is('ajax')) {
            $this->respond(
                __('A aparut o eroare!'),
                'error'
            );
        }

        $data = $this->request->data;

        $this->CategoryTagKeyword->bindModel(
            [
                'belongsTo' => [
                    'AdwordsAdGroup' => [
                        'className' => 'Seo.AdwordsAdGroup',
                        'foreignKey' => 'ad_group_id',
                        'fields' => ['ad_group_id']
                    ],
                ]
            ]
        );

        $keyword = $this->CategoryTagKeyword->findByKeywordId(
            $data['keyword_id']
        );

        $this->CategoryTagKeyword->id = $keyword['CategoryTagKeyword']['id'];
        $this->CategoryTagKeyword->saveField(
            'is_stopped', $data['checked'] == "true" ? 1 : 0
        );

        if ($data['checked'] == "true") {

            $this->CategoryTagKeyword->id = $keyword['CategoryTagKeyword']['id'];
            $this->CategoryTagKeyword->saveField('is_active', 0);

            $this->CategoryTagKeyword->initGoogle();

            $user = new AdWordsUser();
            $user->SetClientCustomerId($this->_customer_id);

            $send[0]['keyword_id'] = $keyword['CategoryTagKeyword']['keyword_id'];
            $send[0]['ad_group_id'] = $keyword['AdwordsAdGroup']['ad_group_id'];

            $result = $this->_updateKeywordStatus(
                $user,
                $send,
                ($data['checked']) ? "PAUSED" : "ENABLED"
            );

            if (!$result) {
                die(json_encode([
                    'success' => 0,
                    'message' => __('A aparut o eroare la oprirea keyword-ului.'
                        . 'Va rugam incercati mai tarziu')
                ]));
            }

            die(json_encode([
                'success' => 1,
                'message' => __('Keywordul a fost oprit definitiv')
            ]));
        }

        die(json_encode([
            'success' => 1,
            'message' => __('Keywordul a fost repornit si va '
                . 'intra in fluxul de pornire/repornire')
        ]));


    }

    /**
     * The updateKeywords method
     *
     * This method enables keywords that have products back on stock
     * and pause keywords the are out of stoc
     *
     * @return respond
     */
    public function updateKeywords()
    {
        $this->CategoryTagKeyword->bindModel(
            [
                'belongsTo' => [
                    'AdwordsAdGroup' => [
                        'className' => 'Seo.AdwordsAdGroup',
                        'foreignKey' => 'ad_group_id',
                    ],
                ]
            ]
        );

        $keywords = $this->CategoryTagKeyword->find(
            'all', [
                'conditions' => [
                    'is_stopped' => false
                ],
                'fields' => [
                    'id',
                    'tag_id',
                    'category_id',
                    'keyword_id',
                    'is_active',
                    'AdwordsAdGroup.ad_group_id'
                ]
            ]
        );

        $toEnable = [];
        $toPause = [];

        foreach ($keywords as $i => $key) {
            $productAliasNamesIds = $this->ProductAliasNameTag->find(
                'list', [
                    'conditions' => [
                        'tag_id' => $key['CategoryTagKeyword']['tag_id']
                    ],
                    'fields' => ['product_alias_name_id']
                ]
            );

            $this->ProductAliasName->virtualFields = [
                'stock' => 'get_stock_with_supplier(product_id)'
            ];

            $this->ProductAliasName->contain();

            $stock = $this->ProductAliasName->find(
                'list', [
                    'conditions' => [
                        'ProductAliasName.id' => $productAliasNamesIds,
                        'ProductAliasName.is_active' => 1,
                        'ProductAliasName.category_id'
                        => $key['CategoryTagKeyword']['category_id']
                    ],
                    'fields' => ['stock'],
                    'group' => 'product_id'
                ]
            );

            if (!$key['CategoryTagKeyword']['is_active']
                && array_sum($stock) > 0
            ) {
                $toEnable[$i]['ad_group_id'] = $key['AdwordsAdGroup']['ad_group_id'];
                $toEnable[$i]['keyword_id']
                    = $key['CategoryTagKeyword']['keyword_id'];

                $this->CategoryTagKeyword->id = $key['CategoryTagKeyword']['id'];
                $this->CategoryTagKeyword->saveField('is_active', 1);
            }

            if ($key['CategoryTagKeyword']['is_active']
                && array_sum($stock) == 0
            ) {
                $toPause[$i]['ad_group_id'] = $key['AdwordsAdGroup']['ad_group_id'];
                $toPause[$i]['keyword_id'] = $key['CategoryTagKeyword']['keyword_id'];

                $this->CategoryTagKeyword->id = $key['CategoryTagKeyword']['id'];
                $this->CategoryTagKeyword->saveField('is_active', 0);
            }
        }

        $this->CategoryTagKeyword->initGoogle();

        $user = new AdWordsUser();
        $user->SetClientCustomerId($this->_customer_id);

        $enable = true;
        if (!empty($toEnable)) {
            $enable = $this->_updateKeywordStatus(
                $user,
                $toEnable,
                "ENABLED"
            );
        }

        $pause = true;
        if (!empty($toPause)) {
            $pause = $this->_updateKeywordStatus(
                $user,
                $toPause,
                "PAUSED"
            );
        }

        if ($enable && $pause) {
            $this->respond(
                count($toPause) . __(' au fost oprite si ')
                . count($toEnable) . __(' au fost (re)pornite'),
                'success'
            );
        }

        $this->respond(
            __('A aparut o eroare la update-ul keywordurilor.'),
            'error'
        );
    }

    /**
     * The getTagsWithStockAndNoKeywords method
     *
     * This method displays tags with stock, without keywords
     *
     * @return view
     */
    public function getTagsWithStockAndNoKeywords()
    {
        $tags = $this->ProductAliasNameTag->query(
            'CALL get_products_with_stock_without_keywords()'
        );

        if (isset($this->request->query['export'])) {
            $this->_export($tags);
        }

        $this->set(compact('tags'));

    }

    /**
     * The _export method
     *
     * This method creates a file for downloading
     *
     * @return download
     */
    private function _export($tags)
    {
        $export_file_name = time() . '_'
            . '_model+stock-keywords.csv';
        $export_dir = WWW_ROOT . 'logs' . DS . 'keywords';
        if (!is_dir($export_dir)) {
            mkdir($export_dir, 0755, true);
        }
        $export_handle = fopen($export_dir . DS . $export_file_name, 'w');
        $headers = array(
            __('Tag'),
            __('Categorie'),
            __('Nr Produse cu stoc'),
            __('Stoc total'),
        );
        fputcsv($export_handle, $headers, ';');

        foreach ($tags as $t) {
            $line = array(
                $t[0]['Model'],
                ltrim($t[0]['tag'], $t[0]['Model'] . " "),
                $t[0]['categorie'],
                $t[0]['nr_produse'],
                $t[0]['stoc'],
            );
            fputcsv($export_handle, $line, ';');
        }
        $this->layout = false;
        $this->autoRender = false;
        fclose($export_handle);
        $file_url = $export_dir . DS . $export_file_name;
        header('Content-Type: text/csv');
        header("Content-disposition: attachment; filename=$export_file_name");
        readfile($file_url);
    }

    public function editUrl($keyword_id)
    {
        if (!$this->request->is('ajax')) {
            $this->respond(
                __('A aparut o eroare!'),
                'error'
            );
        }

        $this->layout = false;
        $this->autoRender = false;

        $data = $this->request->data;

        $this->CategoryTagKeyword->bindModel(
            [
                'belongsTo' => [
                    'AdwordsAdGroup' => [
                        'className' => 'Seo.AdwordsAdGroup',
                        'foreignKey' => 'ad_group_id',
                        'fields' => ['ad_group_id']
                    ],
                ]
            ]
        );

        $keyword = $this->CategoryTagKeyword->findByKeywordId($keyword_id);

        $this->CategoryTagKeyword->id = $keyword['CategoryTagKeyword']['id'];
        $this->CategoryTagKeyword->saveField('url', $data['value']);

        $this->CategoryTagKeyword->initGoogle();

        $user = new AdWordsUser();
        $user->SetClientCustomerId($this->_customer_id);

        $result = $this->_updateKeywordUrl(
            $user,
            $keyword_id,
            $keyword['AdwordsAdGroup']['ad_group_id'],
            $data['value']
        );
        if ($result) {
            return true;
        }

        $this->response->body(__('Eroare la salvarea datelor'));
        $this->response->statusCode(403);
        return false;
    }

    private function _updateKeywordUrl(
        AdWordsUser $user,
        $keyword_id,
        $ad_group_id,
        $url
    )
    {
        $adGroupCriterionService =
            $user->GetService('AdGroupCriterionService', ADWORDS_VERSION);

        $adGroupCriterion = new BiddableAdGroupCriterion();
        $adGroupCriterion->adGroupId = $ad_group_id;

        $adGroupCriterion->criterion = new Criterion($keyword_id);
        $adGroupCriterion->finalUrls = array($url);

        $operation = new AdGroupCriterionOperation();
        $operation->operand = $adGroupCriterion;
        $operation->operator = 'SET';
        $operations[] = $operation;

        $result = $adGroupCriterionService->mutate($operations);

        return empty($result->partialFailureErrors) ? true : false;
    }

    private function _getMatchType($string)
    {
        $phrase = preg_match('/^(["\']).*\1$/m', $string);
        if ($phrase) return "PHRASE";

        $first = substr($string, 0, 1);
        $end = substr($string, -1);
        if ($first == "[" && $end == "]") return "EXACT";

        return "BROAD";
    }

    private function _getKeywordText($string)
    {
        $first = substr($string, 0, 1);
        $end = substr($string, -1);

        if (($first == '"' && $end == '"')
            || ($first == '[' && $end == ']')
        ) {
            return substr($string, 1, -1);
        }

        return $string;
    }

    private function _getDatabaseKeywordText($string, $match)
    {
        if (strtoupper($match) == "PHRASE") {
            return '"' . $string . '"';
        }

        if (strtoupper($match) == "EXACT") {
            return '[' . $string . ']';
        }

        return $string;
    }

    public function deleteKeyword($keyword_id) {

        $this->CategoryTagKeyword->bindModel(
            [
                'belongsTo' => [
                    'AdwordsAdGroup' => [
                        'className' => 'Seo.AdwordsAdGroup',
                        'foreignKey' => 'ad_group_id',
                        'fields' => ['ad_group_id']
                    ],
                ]
            ]
        );

        $keyword = $this->CategoryTagKeyword->findById($keyword_id);

        $this->CategoryTagKeyword->delete($keyword_id);

        $this->CategoryTagKeyword->initGoogle();

        $user = new AdWordsUser();
        $user->SetClientCustomerId($this->_customer_id);

        $adGroupCriterionService =
            $user->GetService('AdGroupCriterionService', ADWORDS_VERSION);

        $criterion = new Criterion();
        $criterion->id = $keyword['CategoryTagKeyword']['keyword_id'];

        $adGroupCriterion = new AdGroupCriterion();
        $adGroupCriterion->adGroupId = $keyword['AdwordsAdGroup']['ad_group_id'];
        $adGroupCriterion->criterion = new Criterion(
            $keyword['CategoryTagKeyword']['keyword_id']
        );

        $operation = new AdGroupCriterionOperation();
        $operation->operand = $adGroupCriterion;
        $operation->operator = 'REMOVE';

        $operations = array($operation);


        $result = $adGroupCriterionService->mutate($operations);
        if (empty($result->partialFailureErrors)) {
            $this->respond(
                __('Stergerea a fost efectuata cu succes'),
                'success'
            );
        }

        $this->respond(
            __('A aparut o eroare la stergerea in AdWords'),
            'error'
        );
    }

    public function edit($keyword_id)
    {
        if ($this->request->is(['post', 'put'])) {
            $data = $this->request->data['CategoryTagKeyword'];

            $tag = $this->Tag->findByName($data['tag_id'], ['id']);
            if (empty($tag)) {
                $this->respond(
                    __('Tagul nu a putut fi asociat'),
                    'error'
                );
            }

            $data['tag_id']    = $tag['Tag']['id'];
            $data['is_active'] = 0;

            if (!$this->CategoryTagKeyword->save($data)) {
                $this->respond(
                    __('A intervenit o eroare la salvarea datelor (1)'),
                    'error'
                );
            }

            $this->CategoryTagKeyword->initGoogle();

            try {

                $user = new AdWordsUser();
                $user->SetClientCustomerId($this->_customer_id);

                $user->LogAll();
                $keyword[] = $data;
                if ($this->_addKeywords($user, $keyword)) {
                    $this->respond(
                        __('Keyword-ul a fost editat cu succes!'),
                        'success',
                        '/seo/seo_category_tag_keywords'
                    );
                } else {
                    $this->respond(
                        __('A intervenit o eroare la trimiterea de keywords!'),
                        'success'
                    );
                }
            } catch (Exception $e) {
                $this->respond(
                    __("A aparut o eroare la trimitere: %s\n", $e->getMessage()),
                    'error'
                );
            }

            $this->respond(
                __('Datele au fost salvate cu succes'),
                'success'
            );
        }

        $this->CategoryTagKeyword->bindModel(
            [
                'belongsTo' => [
                    'AdwordsAdGroup' => [
                        'className' => 'Seo.AdwordsAdGroup',
                        'foreignKey' => 'ad_group_id',
                        'fields' => ['ad_group_id']
                    ],
                    'Tag' => [
                        'className' => 'Catalog.Tag',
                        'foreignKey' => 'tag_id',
                        'fields' => ['name']
                    ]
                ]
            ]
        );

        $keyword = $this->CategoryTagKeyword->findById($keyword_id);

        $categories = $this->Category->find(
            'list', array(
                'conditions' => array(
                    '(rght - lft)' => 1
                )
            )
        );

        $this->AdWordsCampaign->bindModel(
            [
                'hasMany' => [
                    'AdwordsAdGroup' => [
                        'className' => 'Seo.AdwordsAdGroup',
                        'foreignKey' => 'campaign_id',
                        'fields' => ['id', 'name']
                    ]
                ]
            ]
        );

        $campaigns = $this->AdWordsCampaign->find('all');
        $adGroups = [];

        if (!empty($campaigns)) {
            foreach ($campaigns as $c) {
                if (!empty($c['AdwordsAdGroup'])) {
                    foreach ($c['AdwordsAdGroup'] as $g) {
                        $adGroups[$c['AdWordsCampaign']['name']][$g['id']]
                            = $g['name'];
                    }
                }
            }
        }

        $tags = $this->Tag->find('list');

        $this->set(compact(
            'categories',
            'tags',
            'adGroups',
            'keyword'
        ));
    }

    function getAdGroupKeywords() {
        if (!$this->request->is('post')) {
            $this->respond(
                __('Date inconsistente'),
                'error'
            );
        }

        $adGroupId = $this->request->data['SeoCategoryTagKeyword']['ad_group_id'];

        $seoAdGroup = $this->AdwordsAdGroup->findByAdGroupId($adGroupId);

        $this->CategoryTagKeyword->initGoogle();

        $user = new AdWordsUser();
        $user->SetClientCustomerId($this->_customer_id);

        // Get the service, which loads the required classes.
        $adGroupCriterionService =
            $user->GetService('AdGroupCriterionService', ADWORDS_VERSION);

        // Create selector.
        $selector = new Selector();
        $selector->fields = array('Id', 'CriteriaType', 'KeywordMatchType', 'Status',
            'KeywordText');
        $selector->ordering[] = new OrderBy('KeywordText', 'ASCENDING');

        // Create predicates.
        $selector->predicates[] = new Predicate('AdGroupId', 'IN', array($adGroupId));
        $selector->predicates[] =
            new Predicate('CriteriaType', 'IN', array('KEYWORD'));

        // Create paging controls.
        $selector->paging = new Paging(0, AdWordsConstants::RECOMMENDED_PAGE_SIZE);

        $transaction = $this->CategoryTagKeyword->getDataSource();
        $transaction->begin();
        do {
            $page = $adGroupCriterionService->get($selector);

            if (isset($page->entries)) {
                foreach ($page->entries as $adGroupCriterion) {
                    $save['keyword_id'] = $adGroupCriterion->criterion->id;

                    $find = $this->CategoryTagKeyword->findByKeywordId($save['keyword_id']);
                    if (!empty($find)) {
                        continue;
                    }

                    $keyword = $adGroupCriterion->criterion->text;
                    if ($adGroupCriterion->criterion->matchType == 'EXACT') {
                        $keyword = '[' . $keyword . ']';
                    }

                    if ($adGroupCriterion->criterion->matchType == 'PHRASE') {
                        $keyword = '"' . $keyword . '"';
                    }

                    $save['is_active']
                        = ($adGroupCriterion->userStatus == "PAUSED") ? 0 : 1;
                    $save['created_at']  = date('Y-m-d H:i:s');
                    $save['keyword']     = $keyword;
                    $save['ad_group_id'] = $seoAdGroup['AdwordsAdGroup']['id'];
                    $save['is_stopped']  = 0;

                    $this->CategoryTagKeyword->create();
                    if (!$this->CategoryTagKeyword->save($save)) {
                        $transaction->rollback();
                        $this->respond(
                            __('Eroare la importul keywords-urilor'),
                            'error'
                        );
                    }
                }
            }

            $selector->paging->startIndex += AdWordsConstants::RECOMMENDED_PAGE_SIZE;
        } while ($page->totalNumEntries > $selector->paging->startIndex);

        $transaction->commit();
        $this->respond(
            __('Importul a fost finalizat cu success'),
            'success'
        );
    }
}
