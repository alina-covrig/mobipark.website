<?php
/**
* SeoManufacturersController
*
* This file is module-wide controller file. You can put all
* module-wide controller-related methods here.
*
* PHP version 5.4
*
* @category  Admin
* @package   SeoManufacturers
* @author    Daniel Ionascu <daniel.ionascu88@gmail.com>
* @copyright 2014 Evercloud
* @license   http://www.php.net/license/3_01.txt  PHP License 3.01
* @version   SVN: $Id$
* @link      /modules/seo/controller/seo/seo_manufacturers
* @since     1.0
*/

App::uses('AppController', 'Controller');

/**
* OrdersAppController Class
*
* Inside this Controller admin logic methods will
* be placed
*
* @category  Admin
* @package   SeoManufacturers
* @author    Daniel Ionascu <daniel.ionascu88@gmail.com>
* @copyright 2014 Evercloud
* @license   http://www.php.net/license/3_01.txt  PHP License 3.01
* @version   Release: 1.0
* @link      /modules/seo/controller/seo/seo_manufacturers
* @since     Class available since Release 1.0
*/
class SeoManufacturersController extends SeoAppController
{
    public $uses = array(
        'Seo.SeoManufacturer',
        'Catalog.Manufacturer'
    );
    
    /**
     * The edit method
     *
     * Manufacturer seo can be edited and modified as wished
     *
     * @param integer $id the Manufacturer id to be edited
     *
     * @return void
     */
    public function edit($id) 
    {
        if (!$id) {
            $this->Session->setFlash(
                __('Trebuie specificat un producator'), 
                'Alerts/warning'
            );
            return $this->redirect($this->request->referer());
        }
        
        $this->Manufacturer->recursive = -1;
        $this->Manufacturer->contain('SeoManufacturer');
        $manufacturer = $this->Manufacturer->findById($id);
        if (!$manufacturer) {
            $this->Session->setFlash(
                __('Producator invalid'), 
                'Alerts/warning'
            );
            return $this->redirect($this->request->referer());
        }
        if ($this->request->is(array('post',  'put'))) {
            
            if (!empty ($manufacturer['SeoManufacturer']['id'])) {
                $seoId = $manufacturer['SeoManufacturer']['id'];
                $this->request->data['SeoManufacturer']['id'] = $seoId;
            }
            if ($this->SeoManufacturer->save($this->request->data)) {
                return $this->respond(
                    __('Actualizat!'),
                    'success'
                );
            }
        }
        
        // set post data
        if (!$this->request->data) {
            $this->request->data = $manufacturer;
        } else {
            $this->request->data['Manufacturer'] = $manufacturer['Manufacturer']; 
        }
    }
}
