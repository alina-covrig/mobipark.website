<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header"><?php echo __("Seo Producator ").$this->request->data['Manufacturer']['name']?></h1>
    </div>
    <!-- /.col-lg-12 -->
</div>
<!-- /.row -->
<div class="row">
    <div class="col-lg-6">
        <?= $this->H->bslk(
            "/catalog/manufacturers/edit/".$this->request->data['Manufacturer']['id'],
            'btn btn-warning btn-block',
            $this->Html->icon('fa fa-arrow-left') . __(' Inapoi la editare producator'),
            __("Inapoi la editare producator")
        )?>
        <div class="mg20"></div>
    </div>
</div>
<!-- /.row -->
<div class="row">
    <div class="col-md-12">
        <?php echo $this->Form->create('SeoManufacturer');?>
            <?php echo $this->Form->input('manufacturer_id', array('type' => 'hidden', 'value' => $this->request->data['Manufacturer']['id']));?>
            <?php echo $this->Form->input('meta_keywords', array(
                'label' => __('Meta keywords'), 
                'value' => isset ($this->request->data['SeoManufacturer']['meta_keywords']) ? $this->request->data['SeoManufacturer']['meta_keywords'] : ''
            ));?>
            <?php echo $this->Form->input('meta_title', array(
                'label' => __('Meta title'),
                'value' => isset ($this->request->data['SeoManufacturer']['meta_title']) ? $this->request->data['SeoManufacturer']['meta_title'] : ''
            ));?>
            <?php echo $this->Form->input('meta_description', array(
                'label' => __('Meta descriere'),
                'value' => isset ($this->request->data['SeoManufacturer']['meta_description']) ? $this->request->data['SeoManufacturer']['meta_description'] : ''
            ));?>
            <?php echo $this->Form->input('hidden_keywords', array(
                'label' => __('Cuvinte cheie ascunse'),
                'value' => isset ($this->request->data['SeoManufacturer']['hidden_keywords']) ? $this->request->data['SeoManufacturer']['hidden_keywords'] : ''
            ));?>
            <?php echo $this->Form->submit(__('Editeaza SEO'), array('class' => "btn btn-success"));?>
        <?php echo $this->Form->end();?>
    </div>
</div>