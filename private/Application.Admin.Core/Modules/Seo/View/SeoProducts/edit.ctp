<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header"><?php echo __("Seo Produs").$this->request->data['ProductAliasName']['name']?></h1>
    </div>
    <!-- /.col-lg-12 -->
</div>
<!-- /.row -->
<div class="row">
    <div class="col-lg-6">
        <?= $this->H->bslk(
            "/catalog/products/edit/".$this->request->data['Product']['id'],
            'btn btn-warning btn-block',
            $this->Html->icon('fa fa-arrow-left') . __(' Inapoi la editare produs'),
            __("Inapoi la editare produs")
        )?>
        <div class="mg20"></div>
    </div>
</div>
<!-- /.row -->
<div class="row">
    <div class="col-md-12">
        <?php echo $this->Form->create('SeoProduct');?>
            <?php echo $this->Form->input('product_id', array('type' => 'hidden', 'value' => $this->request->data['Product']['id']));?>
            <?php echo $this->Form->input('meta_keywords', array(
                'label' => __('Meta keywords'), 
                'value' => isset ($this->request->data['SeoProduct']['meta_keywords']) ? $this->request->data['SeoProduct']['meta_keywords'] : ''
            ));?>
            <?php echo $this->Form->input('meta_title', array(
                'label' => __('Meta title'),
                'value' => isset ($this->request->data['SeoProduct']['meta_title']) ? $this->request->data['SeoProduct']['meta_title'] : ''
            ));?>
            <?php echo $this->Form->input('meta_description', array(
                'label' => __('Meta descriere'),
                'value' => isset ($this->request->data['SeoProduct']['meta_description']) ? $this->request->data['SeoProduct']['meta_description'] : ''
            ));?>
            <?php echo $this->Form->input('hidden_keywords', array(
                'label' => __('Cuvinte cheie ascunse'),
                'value' => isset ($this->request->data['SeoProduct']['hidden_keywords']) ? $this->request->data['SeoProduct']['hidden_keywords'] : ''
            ));?>
            <?php echo $this->Form->submit(__('Editeaza SEO'), array('class' => "btn btn-success"));?>
        <?php echo $this->Form->end();?>
    </div>
</div>