<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header"><?php echo __("Seo Categorie ").$this->request->data['Category']['name']?></h1>
    </div>
    <!-- /.col-lg-12 -->
</div>
<!-- /.row -->
<div class="row">
    <div class="col-lg-6">
        <?= $this->H->bslk(
            "/catalog/categories",
            'btn btn-warning btn-block',
            $this->Html->icon('fa fa-arrow-left') . __('Inapoi la lista categorii'),
            __("Inapoi la lista categorii")
        )?>
        <div class="mg20"></div>
    </div>
</div>
<!-- /.row -->
<div class="row">
    <div class="col-md-12">
        <?php echo $this->Form->create('SeoCategory');?>
            <?php echo $this->Form->input('category_id', array('type' => 'hidden', 'value' => $this->request->data['Category']['id']));?>
            <?php echo $this->Form->input('meta_keywords', array(
                'label' => __('Meta keywords'), 
                'value' => isset ($this->request->data['SeoCategory']['meta_keywords']) ? $this->request->data['SeoCategory']['meta_keywords'] : ''
            ));?>
            <?php echo $this->Form->input('meta_title', array(
                'label' => __('Meta title'),
                'value' => isset ($this->request->data['SeoCategory']['meta_title']) ? $this->request->data['SeoCategory']['meta_title'] : ''
            ));?>
            <?php echo $this->Form->input('meta_description', array(
                'label' => __('Meta descriere'),
                'value' => isset ($this->request->data['SeoCategory']['meta_description']) ? $this->request->data['SeoCategory']['meta_description'] : ''
            ));?>
            <?php echo $this->Form->input('hidden_keywords', array(
                'label' => __('Cuvinte cheie ascunse'),
                'value' => isset ($this->request->data['SeoCategory']['hidden_keywords']) ? $this->request->data['SeoCategory']['hidden_keywords'] : ''
            ));?>
            <?php echo $this->Form->submit(__('Editeaza SEO'), array('class' => "btn btn-success"));?>
        <?php echo $this->Form->end();?>
    </div>
</div>