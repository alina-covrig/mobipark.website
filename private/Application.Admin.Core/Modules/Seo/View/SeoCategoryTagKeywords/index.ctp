<?=$this->start('page-title');?>
    <strong><?php echo __("Keywords")?></strong>
<?=$this->end()?>
    <div class="row" style="padding-top: 20px;">
        <div class="col-lg-3">
            <?= $this->H->bslk(
                "/seo/seo_category_tag_keywords/add",
                'btn btn-warning btn-block',
                $this->Html->icon('fa fa-plus') . __(' Adauga keywords'),
                __("Adauga keywords")
            )?>
            <div class="mg20"></div>
        </div>
        <!-- /.col-lg-3 -->
        <div class="col-lg-3">
            <?= $this->H->bslk(
                "/seo/seo_category_tag_keywords/saveCampaigns",
                'btn btn-success btn-block',
                $this->Html->icon('fa fa-refresh fa-spin') . __(' Sincronizare campanii'),
                __("Sincronizare campanii")
            )?>
            <div class="mg20"></div>
        </div>

        <div class="col-lg-3">
            <?= $this->H->bslk(
                "/seo/seo_category_tag_keywords/saveAdGroups",
                'btn btn-success btn-block',
                $this->Html->icon('fa fa-refresh fa-spin') . __(' Sincronizare Ad Groups'),
                __("Sincronizare Ad Groups")
            )?>
            <div class="mg20"></div>
        </div>
        <!-- /.col-lg-3 -->

        <div class="col-lg-3">
            <?= $this->H->bslk(
                "/seo/seo_category_tag_keywords/updateKeywords",
                'btn btn-danger btn-block',
                $this->Html->icon('fa fa-bug') . __(' Verificare keywords'),
                __("Verificare keywords")
            )?>
            <div class="mg20"></div>
        </div>
        <!-- /.col-lg-3 -->
    </div>
    <!-- /.row -->

    <div class="row">
        <div class="col-lg-6">
            <?= $this->H->bslk(
                "/seo/seo_category_tag_keywords/getTagsWithStockAndNoKeywords",
                'btn btn-warning btn-block',
                $this->Html->icon('fa fa-newspaper-o') . __(' Telefoane, cu stoc, fara cuvinte cheie, pe categorii'),
                __("Telefoane, cu stoc, fara cuvinte cheie, pe categorii")
            )?>
            <div class="mg20"></div>
        </div>
    </div>

    <div class="row">
        <div class="col-lg-3">
            <?php echo $this->Form->create('SeoCategoryTagKeyword', ['url' => 'getAdGroupKeywords']);?>
                <?php echo $this->Form->input(
                    'ad_group_id', [
                        'label' => false,
                        'required' => true,
                        'class' => 'form-control ad_group input-xs',
                        'name' => 'data[SeoCategoryTagKeyword][ad_group_id]',
                        'empty' => __('Import keywords')
                    ]
                );?>
            <?php echo $this->Form->submit(__('Importa'), array('class' => "btn btn-success"));?>
            <?php echo $this->Form->end();?>
            </form>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
            <table class="table table-condensed small">
                <thead>
                <tr>
                    <th><?=__('ID')?></th>
                    <th><?=__('Campanie')?></th>
                    <th><?=__('Ad Group')?></th>
                    <th><?=__('Categorie')?></th>
                    <th><?=__('Tag')?></th>
                    <th><?=__('Keywords')?></th>
                    <th><?=__('URL')?></th>
                    <th><?=__('Nr Produse')?></th>
                    <th><?=__('Stoc')?></th>
                    <th><?=__('Enabled?')?></th>
                    <th><?=__('Stopped?')?></th>
                    <th>&nbsp;</th>
                </tr>
                <tr class="remove_form_group">
                    <?php echo $this->element('Toolkit.filterForm')?>
                </tr>
                </thead>
                <tbody>
                <?php foreach ($keywords as $k):?>
                    <tr>
                        <td><?=$k['CategoryTagKeyword']['id']?></td>
                        <td><?=$k['AdwordsAdGroup']['campaign']?></td>
                        <td><?=$k['AdwordsAdGroup']['name']?></td>
                        <td><?=$k['Category']['name']?></td>
                        <td><?=$k['Tag']['name']?></td>
                        <td><?=$k['CategoryTagKeyword']['keyword']?></td>
                        <td>
                            <a href="<?=$k['CategoryTagKeyword']['url']?>" target="_blank" title="Vezi link">
                                <i class="fa fa-eye"></i>
                            </a>
                            <a
                                href="#"
                                class="edit-url"
                                data-type="text"
                                data-pk="<?=$k['CategoryTagKeyword']['keyword_id']?>"
                                data-name="CategoryTagKeyword[keyword_id]"
                                data-url="/seo/seo_category_tag_keywords/editUrl/<?=$k['CategoryTagKeyword']['keyword_id']?>.json"
                                data-action="/seo/seo_category_tag_keywords/editUrl/<?=$k['CategoryTagKeyword']['keyword_id']?>.json"
                                data-value="<?=$k['CategoryTagKeyword']['url']?>"
                                data-original-title="<?=__('Editare url')?>"
                                ><?=$k['CategoryTagKeyword']['url']?></a>

                        </td>
                        <td><?=$k['CategoryTagKeyword']['products_nr']?></td>
                        <td><?=$k['CategoryTagKeyword']['stock']?></td>
                        <td>
                            <?=($k['CategoryTagKeyword']['is_active'])? 'Enabled' : 'Paused';?>
                        </td>
                        <td>
                            <input type="checkbox" name="is_active" class="is_stopped" data-keyword-id="<?=$k['CategoryTagKeyword']['keyword_id']?>" target="_blank" <?=($k['CategoryTagKeyword']['is_stopped'])? "checked='checked'": '';?> />
                        </td>
                        <td>
                            <a href="/seo/seo_category_tag_keywords/edit/<?=$k['CategoryTagKeyword']['id']?>" class="btn btn-primary btn-xs btn-outline" data-toggle="tooltip" data-placement="top" data-container="body" title="Editare" data-original-title="Editare"><i class="fa fa fa-pencil-square"></i></a>
                            <a href="/seo/seo_category_tag_keywords/deleteKeyword/<?=$k['CategoryTagKeyword']['id']?>" class="btn btn-danger btn-xs btn-outline" data-toggle="tooltip" data-placement="top" data-container="body" title="Sterge" data-original-title="Sterge" onclick="return confirm(&quot;Esti sigur ca vrei sa stergi keyword-ul?&quot;)"><i class="fa fa fa-trash-o"></i></a>
                        </td>
                    </tr>
                <?php endforeach;?>
                </tbody>
            </table>
            <?php echo $this->element('Navigation/pagination'); ?>
        </div>
    </div>
    <div id="dialog" title="Asteptati...">
        <div id="ajax-loader"></div>
        <p id="ajax-message"></p>
    </div>
<?php echo $this->element('Plugins/inline-edit')?>
<?=$this->start('extra-scripts')?>
    <script type="text/javascript" >

        $('.edit-url').editable();

        $(document).ready(function(){
            $(document).on('click', '.is_stopped',  function(){
                var box = $(this);
                var keyword_id = $(this).data('keyword-id');
                var checked = $(this).prop('checked');
                $.ajax({
                    'url': '/seo/seo_category_tag_keywords/changeKeywordStatus',
                    'type': 'post',
                    'dataType': 'json',
                    'data': {
                        'keyword_id': keyword_id,
                        'checked': checked
                    },
                    beforeSend: function() {
                        $( "#dialog" ).dialog({
                            resizable: false,
                            height:120,
                            modal: true,
                        });
                        $( "#ajax-loader" ).show();
                    },
                    success: function(r) {
                        if (r.success == 0) {
                            $( "#ajax-loader" ).hide();
                            $( "#ajax-message" ).html(r.message);
                        } else {
                            $( "#ajax-message" ).html('');
                            $( "#dialog" ).dialog( "close" );
                        }
                    }
                })
            })
        });

    </script>
<?=$this->end()?>