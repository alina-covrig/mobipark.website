<?=$this->start('page-title');?>
    <strong><?php echo __("Telefoane, cu stoc, fara cuvinte cheie, pe categorii")?></strong>
<?=$this->end()?>
    <div class="row" style="padding-top: 20px;">
        <div class="col-lg-3">
            <?= $this->H->bslk(
                "/seo/seo_category_tag_keywords",
                'btn btn-warning btn-block',
                $this->Html->icon('fa fa-arrow-left') . __(' Inapoi la lista de keywords'),
                __("Inapoi la lista de keywords")
            )?>
            <div class="mg20"></div>
        </div>
        <!-- /.col-lg-3 -->

        <div class="col-lg-3">
            <?= $this->H->bslk(
                "/seo/seo_category_tag_keywords/getTagsWithStockAndNoKeywords?export",
                'btn btn-success btn-block',
                $this->Html->icon('fa fa-file-text-o') . __(' Export'),
                __("Export")
            )?>
            <div class="mg20"></div>
        </div>
        <!-- /.col-lg-3 -->
    </div>
    <!-- /.row -->

    <div class="row">
        <div class="col-md-12">
            <table class="table table-condensed small">
                <thead>
                <tr>
                    <th><?=__('Producator')?></th>
                    <th><?=__('Tag')?></th>
                    <th><?=__('Categorie')?></th>
                    <th><?=__('Nr produse cu stoc')?></th>
                    <th><?=__('Stoc')?></th>
                    <th>&nbsp;</th>
                </tr>
                <tr class="remove_form_group">
                    <?php echo $this->element('Toolkit.filterForm')?>
                </tr>
                </thead>
                <tbody>
                <?php foreach($tags as $t): ?>
                    <tr>
                        <td><?=$t[0]['Model']?></td>
                        <td><?=ltrim($t[0]['tag'], $t[0]['Model']. " ");?></td>
                        <td><?=$t[0]['categorie']?></td>
                        <td><?=$t[0]['nr_produse']?></td>
                        <td><?=$t[0]['stoc']?></td>
                    </tr>
                <?php endforeach; ?>
                </tbody>
            </table>
        </div>
    </div>