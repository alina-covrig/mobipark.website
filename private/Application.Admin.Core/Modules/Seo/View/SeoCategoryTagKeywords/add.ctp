<?php echo $this->element('Plugins/typeahead')?>
<?php $this->start('extra-scripts')?>
<script type="text/javascript">
    $(document).ready(function() {
        enableTypeAhead();
        getUrl();
        removeProduct();
        addProduct();
    });

    function validate_form()
    {
        validate = true;
        $('.categories, .tag, .keywords, .url, .ad_group').each(function(){
            if (!$(this).val()) {
                validate = false;
                $(this).css('border', '1px solid #c9302c');
            } else {
                $(this).css('border', '1px solid #ccc');
            }
        })
        return validate;
    }

    function addProduct() {
        $(document).on('click touchstart', '.add_product', function(e) {
            if (validate_form()) {
                content = $(this).parents('.add_line').html();
                content = '<div class="row add_line">' + content + '</div>';

                var insertContent = $(content).insertAfter($('.add_line').last());
                enableTypeAhead();
                $(insertContent).find(".categories").val($(this).data('category-id'));
                $(insertContent).find(".url").val($(this).data('url'));
                $(insertContent).find(".tag").val($(this).data('tag'));
                $(insertContent).find(".ad_group").val($(this).data('ad-group'));

            }
            return false;
        });
    }

    function removeProduct() {
        $(document).on('click touchstart', '.remove_product', function() {
            if ($('.add_line').length > 1) {
                var isGood = confirm('Esti sigur ca vrei sa stergi keyword-ul?');
                if (isGood == true) {
                    $(this).parents('.add_line').remove();
                }
            } else {
                alert('Stergerea se poate efectua doar cand sunt mai multe keyword-uri');
            }
            return false;
        });
    }

    function enableTypeAhead() {
        $('.typeahead').each(function() {
            $(this).typeahead({
                ajax: '/seo/seo_category_tag_keywords/getTags.json',
            });
        });
    }

    function getUrl() {
        $(document).on('change', '.categories, .tag, .ad_group', function(){
            var parent = $(this).parents('.add_line');
            var category = parent.find('.categories').val();
            var ad_group = parent.find('.ad_group').val();
            var tag = parent.find('.tag').val();
            if (category !== '' && tag !== '') {
                $.ajax({
                    'url': '/seo/seo_category_tag_keywords/getLinks',
                    'dataType': 'json',
                    'type': 'post',
                    'data': {
                        category: category,
                        tag: tag
                    },
                    success: function(r) {
                        parent.find('.url').val(r.url);
                        parent.find('.view_link').attr('href', r.url).removeClass('hide');
                        parent.find('.stock').html(r.stock);
                        parent.find('.nr-products').html(r.noOfProducts);

                        parent.find('.add_product')
                            .attr('data-category-id', category)
                            .attr('data-tag', tag)
                            .attr('data-url', r.url)
                            .attr('data-ad-group', ad_group);
                    }
                })
            }
        })

        $(document).on('keyup', '.url', function(){
            var parent = $(this).parents('.add_line');
            parent.find('.add_product')
                .attr('data-url', $(this).val());
        })

    }
</script>
<?php $this->end()?>
<?=$this->start('page-title');?>
<strong><?php echo __("Seo Keywords")?></strong>
<?=$this->end()?>

<div class="row">
    <div class="col-lg-6">
        <?= $this->H->bslk(
            "/seo/seo_category_tag_keywords",
            'btn btn-warning btn-block',
            $this->Html->icon('fa fa-arrow-left') . __(' Inapoi la lista de keywords'),
            __("Inapoi la lista de keywords")
        )?>
        <div class="mg20"></div>
    </div>
</div>
<!-- /.row -->

<div class="row">
    <div class="col-md-12">
        <?php echo $this->Form->create('CategoryTagKeyword');?>
        <div class="row">
            <div class="col-md-2">
                <h4>Categorie</h4>
            </div>
            <div class="col-md-2">
                <h4>Tag</h4>
            </div>
            <div class="col-md-2">
                <h4>Keywords</h4>
            </div>
            <div class="col-md-2">
                <h4>URL</h4>
            </div>
            <div class="col-md-1">
                <h4>Ad Group</h4>
            </div>
            <div class="col-md-1">
                <h4>Nr. Produse</h4>
            </div>
            <div class="col-md-1">
                <h4>Stoc total</h4>
            </div>
        </div>

        <div class="row add_line">
            <div class="col-md-2">
                <?php echo $this->Form->input(
                    'category_id', [
                        'label' => false,
                        'class' => 'form-control categories input-xs',
                        'empty' => 'Alege categorie',
                        'required' => true,
                        'name' => 'data[CategoryTagKeyword][category_id][]'
                    ]
                );?>
            </div>
            <div class="col-md-2">
                <?php echo $this->Form->input(
                    'tag_id', [
                        'label' => false,
                        'type' => 'text',
                        'class' => 'form-control typeahead tag input-xs',
                        'required' => true,
                        'name' => 'data[CategoryTagKeyword][tag_id][]',
                        'autocomplete' => 'off',
                        'maxlength' => false
                    ]
                );?>
            </div>

            <div class="col-md-2">
                <?php echo $this->Form->input(
                    'keyword', [
                        'label' => false,
                        'required' => true,
                        'class' => 'to-delete form-control keywords input-xs',
                        'name' => 'data[CategoryTagKeyword][keyword][]'
                    ]
                );?>
            </div>

            <div class="col-md-2">
                <div class="row">
                    <div class="col-md-9">
                        <?php echo $this->Form->input(
                            'url', [
                                'label' => false,
                                'required' => true,
                                'class' => 'form-control url input-xs',
                                'name' => 'data[CategoryTagKeyword][url][]'
                            ]
                        );?>
                    </div>
                    <div class="col-md-3">
                        <a href="#" title="<?=__('Vezi link')?>" target="_blank" class="view_link hide" >
                            <i class="fa fa-eye"></i>
                        </a>
                    </div>
                </div>
            </div>

            <div class="col-md-1">
                <?php echo $this->Form->input(
                    'ad_group_id', [
                        'label' => false,
                        'required' => true,
                        'class' => 'form-control ad_group input-xs',
                        'name' => 'data[CategoryTagKeyword][ad_group_id][]',
                        'empty' => __('Alege Ad Group')
                    ]
                );?>
            </div>

            <div class="col-md-1">
                <div class="form-group">
                    <div>
                        <p class="nr-products to-delete"></p>
                    </div>
                </div>
            </div>

            <div class="col-md-1">
                <div class="form-group">
                    <div>
                        <p class="stock to-delete"></p>
                    </div>
                </div>
            </div>

            <div class="col-md-1">
                <a class="btn btn-info btn-outline btn-xs add_product" title="Adauga tag"><i class="fa fa fa-plus"></i></a>
                <a class="btn btn-danger btn-outline btn-xs remove_product" title="Sterge tag"><i class="fa fa fa-trash-o"></i></a>
            </div>
        </div>

        <div class="clearfix"></div>
        <?php echo $this->Form->submit(__('Trimite'), array('class' => "btn btn-success"));?>
        <?php echo $this->Form->end();?>
    </div>
</div>