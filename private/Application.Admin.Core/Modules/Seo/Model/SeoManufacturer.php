<?php
/**
* SeoManufacturer.php
*
* This file contains the SeoManufacturer model
*
* PHP version 5.4
*
* @category  Admin
* @package   Seo
* @author    Daniel Ionascu <daniel.ionascu88@gmail.com>
* @copyright 2014 Evercloud
* @license   http://www.php.net/license/3_01.txt  PHP License 3.01
* @version   SVN: $Id$
* @link      /modules/access/models/seo_manufacturer
* @since     1.0
*/

App::uses('SeoAppModel', 'Seo.Model');


/**
* SeoManufacturer Model Class
*
* Inside this Model any application-wide
* data manipulation methods will be placed
*
* @category  Admin
* @package   Seo
* @author    Daniel Ionascu <daniel.ionascu88@gmail.com>
* @copyright 2014 Evercloud
* @license   http://www.php.net/license/3_01.txt  PHP License 3.01
* @version   Release: 1.0
* @link      /modules/seo/models/seo_manufacturer
* @since     Class available since Release 1.0
*/
class SeoManufacturer extends SeoAppModel
{
    /// properties {{{
    
    public $useTable = 'manufacturers';
    
    /**
    * Validation rules
    *
    * @var array
    */
    public $validate = array(
        'manufacturer_id' => array(
            'pos_integer' => array(
                'rule'     => 'naturalNumber',
                'message'  => 'Valoare gresita pentru id categorie',
            ),
            'isUnique' => array(
                'rule'     => 'isUnique',
                'message'  => 'Producator existent',
            ),
        ),
        'meta_description' => array(
            'notBlank' => array(
                'rule'     => 'notBlank',
                'required' => true,
                'message'  => 'Completati metadescrierea',
            ),
        ),
        'meta_keywords' => array(
            'notBlank' => array(
                'rule'     => 'notBlank',
                'required' => true,
                'message'  => 'Completati cuvintele cheie',
            )
        ),
        'meta_title' => array(
            'notBlank' => array(
                'rule'     => 'notBlank',
                'required' => true,
                'message'  => 'Completati meta titlte',
            ),
            'max' => array(
                'rule'     => array('maxLength', 80),
                'message'  => 'Maxim 80 de caractere',
            ),
        ),
    );
    
    /// }}}
    
}