<?php
/**
* SeoAppModel
*
* Inside this Model any module-wide
* data manipulation methods will be placed
*
* PHP version 5.4
*
* @category  Admin
* @package   Seo
* @author    Daniel Ionascu <daniel.ionascu88@gmail.com>
* @copyright 2014 Evercloud
* @license   http://www.php.net/license/3_01.txt  PHP License 3.01
* @version   SVN: $Id$
* @link      /modules/seo/models/seo-app-model
* @since     1.0
*/

App::uses('AppModel', 'Model');


/**
* SeoAppModel Class
*
* Inside this Model any application-wide
* data manipulation methods will be placed
*
* @category  Admin
* @package   Seo
* @author    Daniel Ionascu <daniel.ionascu88@gmail.com>
* @copyright 2014 Evercloud
* @license   http://www.php.net/license/3_01.txt  PHP License 3.01
* @version   Release: 1.0
* @link      /modules/seo/models/seo-app-model
* @since     Class available since Release 1.0
*/
class SeoAppModel extends AppModel
{
    /**
    * The table prefix used by the Orders module
    * 
    * @var 
    */
    public $tablePrefix = 'seo__';
}
