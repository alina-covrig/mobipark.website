<?php
/**
 * AdwordsAdGroup.php
 *
 * This file contains the AdGroup model
 *
 * PHP version 5.4
 *
 * @category  Admin
 * @package   Seo
 * @author    Daniel Ionascu <daniel.ionascu88@gmail.com>
 * @copyright 2016 Evercloud
 * @license   http://www.php.net/license/3_01.txt  PHP License 3.01
 * @version   SVN: $Id$
 * @link      /modules/access/models/adwords_ad_group
 * @since     1.0
 */

App::uses('SeoAppModel', 'Seo.Model');


/**
 * AdwordsAdGroup Model Class
 *
 * Inside this Model any application-wide
 * data manipulation methods will be placed
 *
 * @category  Admin
 * @package   Seo
 * @author    Daniel Ionascu <daniel.ionascu88@gmail.com>
 * @copyright 2016 Evercloud
 * @license   http://www.php.net/license/3_01.txt  PHP License 3.01
 * @version   Release: 1.0
 * @link      /modules/seo/models/adwords_ad_group
 * @since     Class available since Release 1.0
 */
class AdwordsAdGroup extends SeoAppModel
{
    public $actsAs = array('Containable');

    public $useTable = 'ad_groups';

}
