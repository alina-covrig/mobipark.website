<?php
/**
 * CategoryTagKeyword.php
 *
 * This file contains the SeoCategoryTagKeyword model
 *
 * PHP version 5.4
 *
 * @category  Admin
 * @package   Seo
 * @author    Daniel Ionascu <daniel.ionascu88@gmail.com>
 * @copyright 2014 Evercloud
 * @license   http://www.php.net/license/3_01.txt  PHP License 3.01
 * @version   SVN: $Id$
 * @link      /modules/access/models/seo_category_tag_keywords
 * @since     1.0
 */

App::uses('SeoAppModel', 'Seo.Model');


/**
 * CategoryTagKeyword Model Class
 *
 * Inside this Model any application-wide
 * data manipulation methods will be placed
 *
 * @category  Admin
 * @package   Seo
 * @author    Daniel Ionascu <daniel.ionascu88@gmail.com>
 * @copyright 2014 Evercloud
 * @license   http://www.php.net/license/3_01.txt  PHP License 3.01
 * @version   Release: 1.0
 * @link      /modules/seo/models/seo_category_tag_keywords
 * @since     Class available since Release 1.0
 */
class CategoryTagKeyword extends SeoAppModel
{
    public $actsAs = array('Containable');

    public $useTable = 'category_tag_keywords';

    public function initGoogle()
    {
        error_reporting(E_STRICT | E_ALL);
        $depth = '/../../../';
        define('SRC_PATH', WWW_ROOT . 'src/');
        define('LIB_PATH', 'Google/Api/Ads/AdWords/Lib');
        define('UTIL_PATH', 'Google/Api/Ads/Common/Util');
        define('ADWORDS_UTIL_PATH', 'Google/Api/Ads/AdWords/Util');
        define('ADWORDS_UTIL_VERSION_PATH', 'Google/Api/Ads/AdWords/Util/v201509');
        define('ADWORDS_VERSION', 'v201509');
        // Configure include path
        ini_set('include_path', implode(array(
            ini_get('include_path'), PATH_SEPARATOR, SRC_PATH
        )));
        // Include the AdWordsUser
        require_once LIB_PATH . '/AdWordsUser.php';
    }
}
