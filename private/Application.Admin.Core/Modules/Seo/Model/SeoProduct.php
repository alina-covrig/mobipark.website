<?php
/**
* SeoProduct.php
*
* This file contains the SeoProduct model
*
* PHP version 5.4
*
* @category  Admin
* @package   Seo
* @author    Daniel Ionascu <daniel.ionascu88@gmail.com>
* @copyright 2014 Evercloud
* @license   http://www.php.net/license/3_01.txt  PHP License 3.01
* @version   SVN: $Id$
* @link      /modules/access/models/seo-product
* @since     1.0
*/

App::uses('SeoAppModel', 'Seo.Model');


/**
* SeoProduct Model Class
*
* Inside this Model any application-wide
* data manipulation methods will be placed
*
* @category  Admin
* @package   Seo
* @author    Daniel Ionascu <daniel.ionascu88@gmail.com>
* @copyright 2014 Evercloud
* @license   http://www.php.net/license/3_01.txt  PHP License 3.01
* @version   Release: 1.0
* @link      /modules/seo/models/seo-product
* @since     Class available since Release 1.0
*/
class SeoProduct extends SeoAppModel
{
    /// properties {{{
    
    public $useTable = 'products';
    
    /**
    * Validation rules
    *
    * @var array
    */
    public $validate = array(
        'product_id' => array(
            'pos_integer' => array(
                'rule'     => 'naturalNumber',
                'message'  => 'Valoare gresita pentru id produs',
            ),
            'isUnique' => array(
                'rule'     => 'isUnique',
                'message'  => 'Denumirea exista deja',
            ),
        ),
    );
    
    /// }}}
    
}
