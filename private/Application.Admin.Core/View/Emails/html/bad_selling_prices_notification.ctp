
            <table  align="center" width="660" style="color:#626262; margin:0 20px;  font-size: 16px; line-height: 28px; color: #5f5f5f;">
                <tr>
                    <td style="padding-top:25px; font-size:26px; font-weight:bold; color:#333">
                        <?php echo __('Produse cu pretul de vanzare mai mic decat cel de intrare')?>
                    </td>
                </tr>
                <tr>
                    <td style="padding-top: 20px;">
                        <table>
                            <tr>
                                <td width="40%"
                                    style="vertical-align: bottom; padding: 10px 10px 5px 10px; font-size: 12px">NUME PRODUS</td>
                                <td width="20%"
                                    style="vertical-align: bottom; text-align: right; padding: 10px 10px 5px 10px; font-size: 12px">PRET INTRARE</td>
                                <td width="40%"
                                    style="vertical-align: bottom; text-align: right; padding: 10px 10px 5px 10px; font-size: 12px">PRET VANZARE</td>
                            </tr>
            
                            <?php foreach($emailEntryProducts as $emailEntryProduct): ?>
                                <tr>
                                <td width="350"
                                    style="font-weight: bold; color: #555; padding: 8px 10px; margin-bottom: 4px; font-size: 13px; line-height: 17px; background-color: #F4F4F4">
                                        <a style="text-decoration:none; color:#1E90FF"  href="<?php echo $baseUrl.$relUrl.$emailEntryProduct['Product']['id']; ?>">
                                            <?=$emailEntryProduct['Product']['ProductAliasName']['name']?>
                                        </a>
                                </td>
                                <td
                                    style="text-align: right; padding: 8px 10px; margin-bottom: 4px; font-size: 13px; line-height: 17px; background-color: #F4F4F4"><?=$emailEntryProduct['EntryProduct']['price']?></td>
                                <td
                                    style="text-align: right; padding: 8px 10px; margin-bottom: 4px; font-size: 13px; line-height: 17px; background-color: #F4F4F4"><?=$emailEntryProduct['Product']['selling_price']?></td>
                            </tr>
                            <?php endforeach; ?>
                        </table>
                    </td>
                </tr>
            </table>

