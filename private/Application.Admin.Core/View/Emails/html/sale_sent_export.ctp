<table  align="center" width="660" style="color:#626262; margin:0 20px;  font-size: 16px; line-height: 28px; color: #5f5f5f;">
    <tr>
        <td style="padding-top:25px; font-size:26px; font-weight:bold; color:#333">
            Confirmation of order ID <?=$sale['Sale']['id']?> from <?=$storeName?>
        </td>
    </tr>
    <tr>
        <td style="line-height:24px;">
            Your order with id <?=$sale['Sale']['id']?> is ready.<br>
            Attached you will find information concerning your order which we sent out. <br>
            Furthermore we are looking forward for a successful common relationship.<br>
        </td>
    </tr>
</table>
