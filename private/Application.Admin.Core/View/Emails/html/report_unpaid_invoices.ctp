<table  align="center" width="660" style="margin:0 20px;  font-size: 16px; line-height: 28px; color: #5f5f5f;">

    <tr>
        <td style="line-height:24px;">
            <br><br>
            <table style="width: 100%">
                <?php foreach($invoices as $uid => $user_invoices) :?>
                    <tr>
                        <td colspan="5">
                            <h3>
                                <?=__('Facturile depasite ale userului #%s', $uid)?> &mdash;
                                <a href="<?=Configure::read('BACKEND_URL')?>orders/invoices?Invoice[user_id]=<?=$uid?>">
                                    <?=__('Vezi toate facturile')?>
                                </a>
                            </h3>
                        </td>
                    </tr>
                    <tr>
                        <td>ID Comanda</td>
                        <td>ID Factura</td>
                        <td>Valoare</td>
                        <td>Termen de plata</td>
                        <td></td>
                    </tr>
                    <?php foreach ($user_invoices as $i):?>
                        <tr>
                            <td><?=$i['Invoice']['sale_id']?></td>
                            <td><?=$i['Invoice']['id']?></td>
                            <td><?=$i['Invoice']['value']?></td>
                            <td><?=date('d.m.Y', strtotime($i['Invoice']['payment_due_at']))?></td>
                            <td><a href="<?=Configure::read('BACKEND_URL')?>/customers/users/edit/<?=$uid?>"><?=__('Informatii user')?></a></td>
                        </tr>
                    <?php endforeach?>
                <?php endforeach;?>
            </table>
        </td>
    </tr>
</table>