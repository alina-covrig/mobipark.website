<table  align="center" width="660" style="color:#626262; margin:0 20px;  font-size: 16px; line-height: 28px; color: #5f5f5f;">
    <?php if (!isset($message)) : ?>
        <tr>
            <td style="padding-top:25px; font-size:26px; font-weight:bold; color:#333">
                Completare preturi vanzare lista receptie marfa
            </td>
        </tr>
        <tr>
            <td style="line-height:24px;">
                <br>
                Notificare emisa de: <b><?=$person?></b>.<br>
                <br>
                Completeaza preturile de vanzare pentru produsele din lista.
                <br><br>
                <a href="<?=$order_url?>">Comanda  <?=$order_number?> de la <?=$supplier?> </a>
                <br>
                Data livrare estimata: <?=$delivery_date?>
            </td>
        </tr>
    <?php else:?>
        <tr>
            <td style="line-height:24px;">
                <br>
                Notificare emisa de: <b><?=$person?></b>.<br>
                <br>
                <?=$message?>
            </td>
        </tr>
    <?php endif;?>

</table>
