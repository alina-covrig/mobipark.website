<table  align="center" width="660" style="margin:0 20px;  font-size: 16px; line-height: 28px; color: #5f5f5f;">
    <tr>
        <td style="padding-top:25px; font-size:26px; font-weight:bold; color:#333">
           Draga <?= $user_name ?>,
        </td>
    </tr>
    <tr>
        <td style="line-height:24px">
            <br/>
            Iti multumim ca esti client fidel. Pentru a ne arata recunostinta
            fata de parteneriatul nostru, iti oferim acces la cel mai eclusivist
            nivel GSMnet.ro, <b>Clubul VIP</b>.
        </td>
    </tr>
    <tr>
        <td style="line-height:24px;">
            <br>
            Statusul VIP se adauga nivelului curent de Reseller (<?=$reseller_name?>)
            si iti pune la dispozitie cel mai mic pret listat in paginile produselor cu discount-uri de volum,
            indiferent de numarul de bucati comandate.
            <br>
            Iti multumim ca esti clientul nostru! Pentru mai multe detalii despre campanie,
            te invitam sa accesezi pagina VIP.
        </td>
    </tr>
    <tr>
        <td style="line-height:24px;" align="center">
            <a style="display:block; background-color:#2E82C3; padding:1px 0; font-size:16px; width:150px;
                        text-align:center; color:#fff; font-weight:bold; text-decoration:none"
               href="<?= $baseUrl ?>contul-meu/club-vip?email=<?=$user_email?>&hash=<?=$user_hash?>"
               title="Vezi pagina VIP"
            >
               Intra in pagina
            </a>
        </td>
    </tr>
</table>