<table  align="center" width="660" style="color:#626262; margin:0 20px;  font-size: 16px; line-height: 28px; color: #5f5f5f;">
    <tr>
        <td style="padding-top:25px; font-size:26px; font-weight:bold; color:#333">
            S-a modificat tipul contului tau de pe <?=$storeName?>
        </td>
    </tr>
    <tr>
        <td style="line-height:24px;">
            <br>
            Va aducem la cunostinta ca tip contului dumneavoastra de reseller s-a modificat in <br> <b><?=$reseller_name?></b>.<br>
            <br>
            Pentru a afla discountul de care beneficiati accesati pagina http://www.gsmnet.ro/contact/reselleri (trebuie sa fiti autentificati). Mentionam ca
            tipul contului este direct dependent de rulajul dumneavoastra lunar. Pentru mai multe detalii puteti accesa aceste informatii in contul dumneavoastra sau ne
            puteti contacta la <b>office@gsmnet.ro</b>
        </td>
    </tr>
</table>