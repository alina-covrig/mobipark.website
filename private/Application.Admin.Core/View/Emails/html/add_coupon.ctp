<table  align="center" width="660" style="color:#626262; margin:0 20px;  font-size: 16px; line-height: 28px; color: #5f5f5f;">
    <tr>
        <td style="padding-top:25px; font-size:26px; font-weight:bold; color:#333">
            Ati primit un cupon pe <?=$storeName?>
        </td>
    </tr>
    <tr>
        <td style="line-height:24px;">
            <br>
            <?php if($coupon['type'] == 'value' || $coupon['type'] == ''): ?>
                Ati primit un cupon in valoare de <b><?=$coupon['value']?> Lei</b>
            <?php elseif($coupon['type'] == 'percent'): ?>
                Ati primit un cupon ce va ofera o reducere de <b><?=$coupon['value']?>%</b>
            <?php endif; ?>
            cu seria <b><?=$coupon['code']?></b>. <br>
            Cuponul este valabil pana la data <b><?=$coupon['expires_at']?></b> <br>
            <br>
            Aceste detalii le puteti gasi si in pagina de administrare a contului dumneavoastra.
        </td>
    </tr>
</table>