<table  align="center" width="660" style="color:#626262; margin:0 20px;  font-size: 16px; line-height: 28px; color: #5f5f5f;">
    <tr>
        <td style="font-size:24px; padding-top:10px; color:#333; text-align:center;">
            Lasa un Review la produsele comandate de pe GSMnet.ro
        </td>
    </tr>
    <tr>
        <td style="font-size:16px; padding-top:10px; color:#333; text-align:center;">
            Este important sa ii ajutam pe cei apropiati sa faca cele mai bune alegeri!
            Lasa-ne un <b>review</b> la produsele cumparate de pe GSMnet.ro si astfel ii ajuti pe
            ceilalti sa faca o alegere documentata.
        </td>
    </tr>
    <tr>
        <td style="line-height:24px;">
            <br><br>
            <table style="width: 100%">
                <?php
                foreach($pan_list as $pan){
                    echo '<tr>';
                    echo '<td style="padding:10px; border-bottom:1px solid #EEE"><a href="'.$pan['url'].'">'.$this->H->img($pan['image'], $pan['image'], '40x40', 'products/' . $pan['product_id'], true).'</a></td>
                    <td style="padding:10px; border-bottom:1px solid #EEE"><a style="color:#0e53a5; font-weight:bold; text-decoration:none" href="'.$pan['url'].'">'.$pan['name'].'</a></td>
                    <td style="padding:10px; border-bottom:1px solid #EEE"><a style="display:block; background-color: #43a678; padding: 1px 0; font-size:10px; width:150px; text-align:center; color:#FFF; font-weight:bold; text-decoration:none" href="'.$pan['url'].'">SCRIE COMENTARIUL TAU</a></td>';
                    echo '</tr>';
                }
                ?>
            </table>
        </td>
    </tr>
</table>