<?php if($sale['Sale']['delivery_method_id']=='15'){ ?>

            <table  align="center" width="660" style="color:#626262; margin:0 20px;  font-size: 16px; line-height: 28px; color: #5f5f5f;">
                <tr>
                    <td style="padding-top:25px; font-size:26px; font-weight:bold; color:#333">
                        Confirmare comanda ID <?=$sale['Sale']['id']?> de pe <?=$storeName?>
                    </td>
                </tr>
                <tr>
                    <td style="line-height:24px;">
                        Comanda cu id-ul <?=$sale['Sale']['id']?> este pregatita.<br>
                        Va asteptam la sediul nostru aflat in <?=$companyAddress ?> pentru ridicarea acesteia.<br>
                    </td>
                </tr>
            </table>

<?php  } else { ?>

            <table  align="center" width="660" style="color:#626262; margin:0 20px;  font-size: 16px; line-height: 28px; color: #5f5f5f;">
                <tr>
                    <td style="padding-top:25px; font-size:26px; font-weight:bold; color:#333">
                        Confirmare comanda ID <?=$sale['Sale']['id']?> de pe <?=$storeName?>
                    </td>
                </tr>
                <tr>
                    <td style="line-height:24px;">
                        Comanda cu id-ul <?=$sale['Sale']['id']?> a fost preluata de catre Fan Courier si este in curs de livrare. Nr-ul de AWB alocat expeditiei este <?=$sale['SaleFancourier']['awb']?>.<br>
                        <br>
                        <b>Pentru urmarirea coletului puteti apela urmatoarele numere de telefon:</b><br>
                        Numar apelabil din orice retea: <a href="tel:0219336" style="text-decoration:none; color:#1E90FF">021 9336</a><br>
                        Vodafone:  <a href="tel:0723552233" style="text-decoration:none; color:#1E90FF">0723 55 22 33</a> sau *FAN<br>\
                        Orange:  <a href="tel:0742552233" style="text-decoration:none; color:#1E90FF">0742 55 22 33</a> sau *COURIER sau *FAN<br>
                        E-mail: <a href="mailto:fan@fancourier.ro" style="text-decoration:none; color:#1E90FF">fan@fancourier.ro</a><br>
                        <br>
                        Costul transportului pentru aceasta comanda este:
                            <?php
                                if($sale['Sale']['delivery_value']=='0.00'){
                                    echo "Beneficiati de transport Gratuit";
                                }
                                else{
                                    echo price($sale['Sale']['delivery_value']).' '.Configure::read('LOCAL_CURRENCY');
                                }
                            ?>.
                        In momentul receptiei, va rugam sa verificati integritatea exterioara a coletului si apoi sa semnati de primire.
                        Factura produselor este atasata acestui email si poate fi descarcata si din contul dvs. de client la sectiunea <a style="text-decoration:none; color:#1E90FF"  href="<?php echo $baseUrl; ?>contul-meu/comenzi/">Comenzi</a>, iar chitanta va fi eliberata de catre reprezentantul firmei de curierat.<br><br>
                        Coletul dvs. a fost pregatit de "<?=$adminDetails['Person']['fname'].' '.$adminDetails['Person']['lname']?>". Daca coletul dvs. nu ajunge in maxim 48 ore, va rugam sa ne contactati imediat.<br><br>
                        In cazul in care refuzati un colet, ne rezervam dreptul de a nu mai onora comenzile. Pentru a relua colaborarea, se va achita in avans contravaloarea transportului pentru coletul refuzat (tur - retur) si cea a comenzii in curs prin Card Online sau O.P.<br>
                        Va multumim!<br>
                        <br>
                        Deoarece este foarte important pentru noi sa avem o colaborare cat mai buna, va rugam sa ne semnalati orice problema intampinati in procesul de comanda printr-o simpla sesizare prin formularul de <a style="text-decoration:none; color:#1E90FF"  href="<?php echo $baseUrl; ?>contact">Contact</a>.
                    </td>
                </tr>
            </table>
<?php } ?>