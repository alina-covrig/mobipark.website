<!-- /.row -->
<style>
    .navbar-static-top { display: none; }
    .wide #page-wrapper { margin-left: 0; padding: 2px 15px 15px 15px !important; }
    #DateCountdown{
        height: 500px;
        max-width: 1500px;
        margin: 0 auto;
    }
    table {margin-top: 20px;}
    td {border: none !important;}
    td span {font-size: 100px; color: #CCCCCC;}
    td b {font-size: 110px; color: #333;}

    .inf {
        color: #bababa;
        position: absolute;
        bottom: 10px;
        left: 10px;
    }
</style>
<div class="col-lg-12">
    <div class="row">
        <div class="col-lg-12 text-center">
            <div id="DateCountdown" data-date="<?=date('Y-m-d ')?>"></div>
        </div>
    </div>
    <hr>
    <table class="table">
        <tbody>
        <tr>
            <td style="width: 30%"><span>Nepreluate:</span></td>
            <td><b><?=$homeStats['to_process']?></b></td>
            <td style="width: 10%"></td>
            <td style="width: 30%"><span>Preluate:</span></td>
            <td><b><?=$homeStats['processing']?></b></td>
        </tr>
        <tr>
            <td style="width: 30%"><span>Procesate:</span></td>
            <td><b><?=$homeStats['processed']?></b></td>
            <td style="width: 10%"></td>
            <td style="width: 30%"><span>Finalizate:</span></td>
            <td><b><?=$homeStats['finalized']?></b></td>
        </tr>
        </tbody>
    </table>
</div>
<small class="inf">
    <i>&nbsp;Datele afisate apartin comenzilor care:
        au metoda de livrare diferita de "Rdicare de la sediul firmei",
        au sablonul de factura in lei,
        nu sunt generate manual din admin,
        nu apartin adminilor !
        <br>&nbsp;
        *Obs: Nepreluate = 'Plasata' + 'Validata';&nbsp;&nbsp;&nbsp;&nbsp;
        Preluate: 'Asteapta procesare' + 'In procesare' + 'Procesare stand by';&nbsp;&nbsp;&nbsp;&nbsp;
        Procesate: 'Procesata';&nbsp;&nbsp;&nbsp;&nbsp;
        Finalizate:	'Finalizata' azi.
    </i>
</small>
<script>
    setTimeout(function(){
        window.location.reload(1);
    }, 60000);
</script>
<!-- /.row -->

<?php $this->start('extra-scripts')?>
<?php echo $this->Html->css('/admin/css/TimeCircles.css')?>
<?php echo $this->Html->script('/admin/js/TimeCircles.js')?>
<script>
    $("#DateCountdown").TimeCircles({
        "animation": "smooth",
        "bg_width": 1,
        "fg_width": 0.04,
        "circle_bg_color": "#f9f9f9",
        "time": {
            "Days": {
                "text": "Days",
                "color": "#bbb",
                "show": false
            },
            "Hours": {
                "text": "Ora",
                "color": "#CCCCCC",
                "show": true
            },
            "Minutes": {
                "text": "Minute",
                "color": "#CCCCCC",
                "show": true
            },
            "Seconds": {
                "text": "Secunde",
                "color": "#CCCCCC",
                "show": true
            }
        }
    });
</script>
<?php $this->end()?>