<?php
/**
 * H View Helper
 *
 * This file is application-wide helper file. You can put all
 * application-wide helper-related methods here. It's a conveinence
 * name for simple usage
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link      http://cakephp.org CakePHP(tm) Project
 * @package   app.View.Helper
 * @since     CakePHP(tm) v 0.2.9
 * @license   http://www.opensource.org/licenses/mit-license.php MIT License
 */

App::uses('AppHelper', 'View');

/**
 * H helper
 *
 * Add your application-wide methods in the class below
 * Generally bootstrap links, etc
 *
 * @package HViewHelper
 */
class HHelper extends AppHelper
{
    // properties {{{
        private  $_modal = '';
    // }}}
    
    /**
    * Bootrstrap link method
    * 
    * This method intends to help you create links with ease
    * 
    * @param string $path             preceeded by /
    * @param string $class            bootstrap class if needed
    * @param string $text             between the tags
    * @param string $tooltip          text
    * @param string $tooltip_position top, left, bottom or right
    * @param string $return_confirm   text to be asked before
    * 
    * @return string an anchor tag
    */
    public function bslk ($path, $class, $text, $tooltip = '', $tooltip_position = 'top', $return_confirm = null, $options = array()) 
    {
        $ttip = '';
        $confirm = '';
        if($tooltip) {
            $ttip = " data-toggle='tooltip' " . 
                "data-placement='{$tooltip_position}'" . 
                "data-container='body'" . 
                "title='$tooltip' data-original-title='$tooltip' ";
        }
        if(!is_null($return_confirm)) {
            $confirm = " onclick=\"return confirm('$return_confirm')\" ";
        }
        
        $options_list = '';
        foreach ($options as $o_name => $o_value) {
            $options_list .= $o_name.'="'.$o_value.'" ';
        } 
        return "<a href='{$path}' class='{$class}' $ttip $confirm $options_list>{$text}</a>";
    }
    
    public function img($name, $title, $size = "200x200", $folder = '', $noSize = false)
    {
        $sizeex = explode('x', $size);
        if($folder) {
            $folder = trim($folder, '/') . '/';
        }
        $wh = "style='width:100%;max-width:{$sizeex[0]}px'";
        if(!$noSize) {
            $wh = "width='{$sizeex[0]}' height='{$sizeex[1]}'";
        }
        return "<img src='" . Configure::read('THUMBS_PATH') . $size . '/' . $folder . $name . "' $wh title='{$title}' alt='{$title}' />";
    }
    
    /**
    * Modal helper method
    * 
    * This method returns a html element for a modal dialog
    * 
    * @param string $modalId           the id of the modal
    * @param string $modalTitleId      the id of the title
    * @param string $modalTitle        the modal title
    * @param string $modalContent      the content of the modal
    * @param string $modalConfirmLink  the link to go to 
    * @param string $modalConfirmTitle the value of the confirm button
    * 
    * @return string
    */
    public function modal(
        $modalId, 
        $modalTitleId, 
        $modalTitle, 
        $modalContent, 
        $modalConfirmLink = null, 
        $modalConfirmTitle = null
    ) {
        $modal = '<div class="modal fade" id="' 
            . $modalId . '"'
            . 'tabindex="-1" role="dialog" aria-labelledby="' 
            . $modalTitleId 
            . '" aria-hidden="true">';
        $modal .= '<div class="modal-dialog">';
        $modal .= '<div class="modal-content">';
        $modal .= '<div class="modal-header">';
        $modal .= '<button type="button" class="close"'
            . 'data-dismiss="modal" aria-hidden="true">×</button>';
        $modal .= '<h4 class="modal-title" id="' 
            . $modalTitleId 
            . '">' 
            . $modalTitle 
            . '</h4>';
        $modal .= '</div>';
        $modal .= '<div class="modal-body">';
        $modal .= $modalContent;
        $modal .= '</div>';
        $modal .= '<div class="modal-footer">';
        $modal .= '<button type="button" class="btn btn-default"'
            . 'data-dismiss="modal" data-original-title="" title="">'
            . __('Inchide')
            . '</button>';
        if($modalConfirmLink) {
            if(!$modalConfirmTitle) {
                $modalConfirmTitle = 'OK';
            }
            $modal .= '<a class="btn btn-primary" href="'
                . $$modalConfirmLink
                . '">'
                . $modalConfirmTitle
                . '</a>';
        }
        $modal .= '</div>';
        $modal .= '</div>';
        $modal .= '</div>';
        $modal .= '</div>';
        return $modal;
    }

    public function startModalForm($id, $title_id, $title, $action = '', $method = 'post')
    {
        $modal = '<div class="modal fade" id="'
            . $id . '"'
            . 'tabindex="-1" role="dialog" aria-labelledby="'
            . $title_id
            . '" aria-hidden="true">';
        $modal .= '<div class="modal-dialog">';
        $modal .= '<div class="modal-content">';
        $modal .= '<div class="modal-header">';
        $modal .= '<button type="button" class="close"'
            . 'data-dismiss="modal" aria-hidden="true">×</button>';
        $modal .= '<h4 class="modal-title" id="'
            . $title_id
            . '">'
            . $title
            . '</h4>';
        $modal .= '</div>';
        $modal .= '<form action="' . $action . '" method="' . $method . '">';
        $modal .= '<div class="modal-body">';
        return $modal;
    }

    public function endModalForm($text = 'OK', $cancel_text = 'Inchide', $no_buttons = false)
    {
        $modal = '</div>';
        if(!$no_buttons) {
            $modal .= '<div class="modal-footer">';
            $modal .= '<button type="button" class="btn btn-default"'
                . 'data-dismiss="modal" data-original-title="" title="">'
                . $cancel_text
                . '</button>';
            $modal .= '<button type="submit" class="btn btn-success"'
                . '>'
                . $text
                . '</button>';
        }

        $modal .= '</div>';
        $modal .= '</form>';
        $modal .= '</div>';
        $modal .= '</div>';
        $modal .= '</div>';
        return $modal;
    }
    
    /**
    * Displays a file with an icon
    * 
    * @param string $filename
    * @param string $webroot_folder
    * 
    * @return string
    */
    public function file($filename, $webroot_folder = '/')
    {
        $file = Configure::read('WEBSITE_URL')
            . trim($webroot_folder, '/') . '/'
            . $filename;
        $file_headers = @get_headers($file);
        if (!isset($file_headers[0])) {
            return false;
        }
        if ($file_headers[0] == 'HTTP/1.1 404 Not Found'){
            return false;
        } else if (
            $file_headers[0] == 'HTTP/1.1 302 Found'
            && $file_headers[7] == 'HTTP/1.1 404 Not Found'
        ) {
            return false;
        }
        $extension = pathinfo($filename)['extension'];
        $fileanchor = "<a href='$file' target='blank' class='btn btn-social'>";
        $fileanchor .= "<i class='fa fa-file-pdf-o'></i> ";
        $fileanchor .= "{$filename}</a>";
        return $fileanchor;
    }
    
    public function getImportType($type) 
    {
        switch($type){
        case 'Product':
            return __('produse');
                break;
        case 'Characteristic':
            return __('caracteristici');
                break;
        case 'Image':
            return __('imagini');
                break;
        case 'Price':
            return __('preturi');
                break;
        case 'SupplierProduct':
            return __('produse furnizori');
                break;
        case 'SupplierOffer':
            return __('oferte furnizori');
                break;
        case 'ImageFile':
            return __('imagini (zip)');
                break;
        case 'Entry':
            return __('NIR-uri');
                break;
        case 'QuantityPrice':
            return __('pret cantitativ');
            break;

        }
    }
    
    public function getImportController($type) 
    {
        switch($type) {
            case 'Product':
                return 'products_imports';
                    break;
            case 'Image':
                return 'images_imports';
                    break;
            case 'Characteristic':
                return 'characteristics_imports';
                    break;
            case 'Price':
                return 'prices_imports';
                    break;
            case 'SupplierProduct':
                return 'supplier_products_imports';
                    break;
            case 'SupplierOffer':
                return 'supplier_offers_imports';
                    break;
            case 'ImageFile':
                return 'images_imports';
                    break;
            case 'Entry':
                return 'entries_imports';
                    break;
            case 'ProductFlag':
                return 'product_flags_imports';
                    break;
            case 'QuantityPrice':
                return 'quantity_prices_imports';
                break;
        }
    }

    public function storePath($localPath) {
        return Configure::read('WEBSITE_URL')
            . trim($localPath, '/');
    }

    public function getParentCategory($id)
    {
        App::import("Model", "Catalog.Category");
        $model = new Category();
        $model->id = $id;
        return $model->field('parent_id');
    }

    public function iconAvailability($av) {
        $html = '';
        switch($av['id']) {
            case null:
                $html .= "<i class='fa fa-exclamation-triangle' style='color:salmon' title='Disponibilitate nesetata'></i>";
                break;
            case '1':
                $html .= "<i class='fa fa-rotate-270 fa-battery-full' style='color:" . $av['hex_color'] . "' title='" . $av['name'] . "'></i>";
                break;
            case '3':
                $html .= "<i class='fa fa-rotate-270 fa-battery-half' style='color:" . $av['hex_color'] . "' title='" . $av['name'] . "'></i>";
                break;
            case '4':
                $html .= "<i class='fa fa-rotate-270 fa-battery-empty' style='color:" . $av['hex_color'] . "' title='" . $av['name'] . "'></i>";
                break;
        }
        return $html;
    }

}
