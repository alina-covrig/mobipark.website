<?php
/**
 * Application level View Helper
 *
 * This file is application-wide helper file. You can put all
 * application-wide helper-related methods here.
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link      http://cakephp.org CakePHP(tm) Project
 * @package   app.View.Helper
 * @since     CakePHP(tm) v 0.2.9
 * @license   http://www.opensource.org/licenses/mit-license.php MIT License
 */

App::uses('Helper', 'View');

/**
 * Application helper
 *
 * Add your application-wide methods in the class below, your helpers
 * will inherit them.
 *
 * @package app.View.Helper
 */
class AppHelper extends Helper
{
    public function getStatus ($status) {
        $return = '';
        switch($status) {
            case 'unsolved':
                $return = __('Nerezolvat');
                break;
            case 'cancel':
                $return = __('Storno');
                break;
            case 'replace':
                $return = __('Inlocuit');
                break;
            case 'coupon':
                $return = __('Cupon');
                break;
            case 'transport':
                $return = __('Transport');
                break;
            case 'reject':
                $return = __('Respins');
                break;
        }
        return $return;
    }


    public function url($url = null, $full = false)
    {
        if (is_array($url))
        {
            $url = array_merge(
                array(
                    'controller' => $this->request->controller,
                    'plugin' => $this->request->plugin,
                    'action' => $this->action
                ),
                $url);
            ksort($url);
        }
        Cache::config('router', array(
            'engine' => DEF_CACHE, // Or Xcache, or Apc
            'prefix' => 'routes_',
            'duration' => '+5 minutes'
        ));

        $key = md5(serialize($url).$full);
        if (($link = Cache::read($key, 'router')) === false)
        {
            $link = h(Router::url($url, $full));
            Cache::write($key, $link, 'router');
        }
        return $link;
    }

    public function currency($c)
    {
        if (!empty($c['acronym'])) {
            return '<i class="fa fa-' . strtolower($c['acronym']) . '"></i>';
        }
        return 'LEI';
    }

    public function booleanText($bool)
    {
        if ($bool) {
            return 'da';
        }
        return 'nu';
    }

    public function booleanIcon($bool, $tooltip_true = '', $tooltip_false = '')
    {
        if ($bool) {
            return '<i class="fa fa-check text-success" data-toggle="tooltip" title="' . $tooltip_true . '"></i>';
        }
        return '<i class="fa fa-times text-lightgray" data-toggle="tooltip" title="' . $tooltip_false . '"></i>';
    }

    public function unpaidValue($value, $paid_value)
    {
        $text_class = '';
        if ($paid_value == 0) {
            $text_class = 'text-red';
        } elseif ($value == $paid_value) {
            $text_class = 'text-success';
        } else {
            $text_class = 'text-orange';
        }
        return '<span class="'.$text_class.'">'.pretty_price($value - $paid_value).'</span>';
    }

    public function cellInvoiceType($type)
    {
        return '<td class="invoice-type '.alias($type).'">'.$type.'</td>';
    }

    public function articleImage($image = null)
    {
        if (is_null($image)) {
            $image_path = Configure::read('WEBSITE_URL') . 'img' . DS . 'no_image.png';
        } else {
            $image_path = Configure::read('WEBSITE_URL') . $image;
        }
        return '<img src="' . $image_path . '" alt="" height="40" style="max-width: 100%;" />';
    }
}
