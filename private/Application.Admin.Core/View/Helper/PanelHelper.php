<?php
/**
 * Panel View Helper
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link      http://cakephp.org CakePHP(tm) Project
 * @package   app.View.Helper
 * @since     CakePHP(tm) v 0.2.9
 * @license   http://www.opensource.org/licenses/mit-license.php MIT License
 */

App::uses('AppHelper', 'View');

/**
 * Panel helper
 *
 * Add your application-wide methods in the class below
 * Generally bootstrap links, etc
 *
 * @package HViewHelper
 */
class PanelHelper extends AppHelper
{
    // properties {{{
        
    // }}}
    
    public function start(
        $text,
        $class = 'panel-default',
        $minimize = false,
        $is_minimized = false
    )
    {
        $return = '<div class="panel ' . $class . '">';
        $return .= '<div class="panel-heading">';
        $return .= $text;
        if ($minimize) {
            if ($is_minimized) {
                $class = 'fa-toggle-down';
            } else {
                $class = 'fa-toggle-up';
            }
            $return .= '<a href="javascript:void(0)" class="toggle-pane-body pull-right"><i class="fa ' . $class . '"></i></a>';
        }
        $return .= '</div>';
        if ($is_minimized) {
            $return .= '<div class="panel-body" style="display: none">';
        } else {
            $return .= '<div class="panel-body">';
        }
        return $return;
    }
    
    public function end($text = null) 
    {
        $return = '</div>';
        if(!is_null($text)) {
            $return .= '<div class="panel-footer">';
            $return .= $text;
            $return .= '</div>';
        }
        $return .= '</div>';
        return $return;
    }
}
