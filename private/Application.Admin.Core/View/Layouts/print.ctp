<html>
    <head>
        <title><?php echo __("Administrare ")?> | <?php echo Configure::read('STORE_NAME')?></title>
        <?php echo $this->Html->css('/admin/css/print.css')?>
    </head>
    <body>
        <?php echo $this->fetch('content')?>
    </body>
</html>
