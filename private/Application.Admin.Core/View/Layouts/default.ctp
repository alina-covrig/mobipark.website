<!DOCTYPE html>
<html lang="ro">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1, minimum-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title><?php echo __('Administrare ')?> | <?php echo Configure::read('STORE_NAME')?></title>
    <?=$this->Html->css('/admin/css/bootstrap.min.css')?>
    <?=$this->Html->css('/admin/css/jquery-ui.min.css')?>
    <?=$this->Html->css('/admin/css/chosen.min.css')?>
    <?=$this->Html->css('/admin/css/sb-admin-2.css?v1.0.2')?>
    <?=$this->Html->css('/admin/css/update.css?v=1.0.0')?>
    <?=$this->Html->css('/admin/css/lightbox.css')?>
    <?=$this->Html->css('/admin/css/jquery.datetimepicker.css')?>
    <?=$this->Html->css('/admin/css/plugins/metisMenu/metisMenu.min.css', array('media' => 'screen'))?>
    <?=$this->Html->css('/admin/css/print.css', array('media' => 'print'))?>
    <?=$this->Html->css('/admin/css/custom-checks.css')?>
    <?=$this->Html->css('/admin/js/toastr/toastr.min.css')?>
    <?=$this->fetch('css')?>
	<?=$this->Html->css('https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css')?>
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body>
    <?php if(!empty($topAnnouncements)): ?>
    <div class="announcements">
        <ul>
            <?php foreach($topAnnouncements as $a): ?>
            <li>
                <?=$a['Announcement']['message']?>
            </li>
            <?php endforeach; ?>
        </ul>
    </div>
    <?php endif; ?>
    <div id="wrapper" class="wide">
        <?php echo $this->element('Navigation/navigation')?>

        <div id="page-wrapper" style="padding-top: 20px">
            <?php echo $this->Session->flash()?>
            <?php echo $this->fetch('content')?>
        </div>
        <!-- /#page-wrapper -->

    </div>
    <!-- /#wrapper -->

    <?=$this->Html->script('/admin/js/jquery-1.11.0.js')?>
    <?=$this->Html->script('/admin/js/jquery-ui.min.js')?>
    <?=$this->Html->script('/admin/js/bootstrap.min.js')?>
    <?=$this->Html->script('/admin/js/jquery.datetimepicker.js')?>
    <?=$this->Html->script('/admin/js/plugins/metisMenu/metisMenu.min.js')?>
    <?=$this->Html->script('/admin/js/jquery.floatThead.min.js')?>
    <?=$this->Html->script('/admin/js/lightbox.js')?>
    <?=$this->Html->script('/admin/js/toastr/toastr.min.js')?>
    <?=$this->fetch('script')?>
    <?=$this->Html->script('/admin/js/sb-admin-2.js?1.0')?>
    <?=$this->Html->script('/admin/js/chosen.jquery.min.js')?>
    <?=$this->Html->script('/admin/js/clipboard.min.js')?>
    <?=$this->fetch('extra-scripts')?>
    <script>
        $(function () {
            $('[data-toggle="tooltip"]').tooltip()
        })
    </script>
    <?=$this->H->modal(
        'help-generic-search',
        'help-generic-search-title',
        __('Despre cautarea spotlight'),
        __('Cautarea Spotlight permite cautarea in diverse module si pe anumite campuri. Iata lista completa:<br />
                        &bull; <b>pi/22033</b> cauta produsul cu id-ul 22033<br />
                        &bull; <b>p/husa</b> iphone cauta produsul cu numele<br />
                        &bull; <b>oi/107</b> sau <b>107</b> cauta comanda cu id-ul 107
                        ')
    );
    ?>
</body>
</html>