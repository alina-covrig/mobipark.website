<!DOCTYPE html>
<html lang="ro">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title><?php echo __("Administrare ")?> | <?php echo Configure::read('STORE_NAME')?></title>
    <?php echo $this->Html->css('/admin/css/bootstrap.min.css')?>
    <?php echo $this->Html->css('/admin/css/jquery-ui.min.css')?>
    <?php echo $this->Html->css('/admin/css/sb-admin-2.css')?>
    <?php echo $this->Html->css('/admin/css/update.css')?>
    <?php echo $this->Html->css('/admin/css/jquery.datetimepicker.css')?>
    <?php echo $this->Html->css('/admin/css/plugins/metisMenu/metisMenu.min.css', array('media' => 'screen'))?>
    <?php echo $this->Html->css('/admin/css/print.css', array('media' => 'print'))?>
    <?php echo $this->fetch('css')?>
    <?php echo $this->Html->css('//maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css')?>
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>

<body>
<?php if(!empty($topAnnouncements)): ?>
    <div class="announcements">
        <ul>
            <?php foreach($topAnnouncements as $a): ?>
                <li>
                    <?=$a['Announcement']['message']?>
                </li>
            <?php endforeach; ?>
        </ul>
    </div>
<?php endif; ?>

        <?php echo $this->Session->flash()?>
        <?php echo $this->fetch('content')?>
<!-- /#wrapper -->


<?php echo $this->Html->script('/admin/js/jquery-1.11.0.js')?>
<?php echo $this->Html->script('/admin/js/bootstrap.min.js')?>
<?php echo $this->Html->script('/admin/js/jquery-ui.min.js')?>
<?php echo $this->Html->script('/admin/js/jquery.datetimepicker.js')?>
<?php echo $this->Html->script('/admin/js/plugins/metisMenu/metisMenu.min.js')?>
<?php echo $this->Html->script('/admin/js/jquery.floatThead.min.js')?>
<?php echo $this->fetch('script')?>
<?php echo $this->Html->script('/admin/js/sb-admin-2.js')?>
<?php echo $this->fetch('extra-scripts')?>
</body>
</html>