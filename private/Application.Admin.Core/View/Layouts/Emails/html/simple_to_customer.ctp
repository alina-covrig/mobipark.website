<!doctype html>
<html lang="ro">
<head>
    <meta charset="UTF-8">
    <style>
        * {
            font-family: "Helvetica Neue", "Helvetica", "Arial", sans-serif;
            font-size: 12px;
            color: #534e4e;
        }
    </style>
</head>
<body>
    <div style="width: 700px;margin: 10px auto;">
        <hr/>
        <div style="margin: 20px 0px;">
            <?=$this->fetch('content');?>
        </div>
        <div style="text-align: center">
            <small style="font-size: 11px; color: #817a79;">Acest email este trimis de <?=Configure::read('WEBSITE_NAME')?></small>
        </div>
        <div style="">
            <img height="50px" style="display: block;margin: 5px auto;" src="<?=Configure::read('STORE_URL')?>theme/default/assets/images/logo.png" alt=""/>
        </div>
    </div>
</body>
</html>