<?php
/**
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.View.Layouts.Email.html
 * @since         CakePHP(tm) v 0.10.0.1076
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01//EN">
<html>
<head>
	<title><?php echo $title_for_layout; ?></title>
</head>
<body>
<table cellspacing="0" cellpadding="0" width="100%" align="center" style="font-family:Arial; color:#626262; font-size:11px">
	<tr>
		<td align="center">
			<table cellspacing="0" cellpadding="0" width="100%" bgcolor="#f4f4f4" style="color:#626262;">
				<tr>
					<td align="center">
						<table cellspacing="0" cellpadding="0" width="660" style="color:#626262; margin:0 20px; font-size:12px">
							<tr>
								<td width="250" height="110" ><img src="<?php echo $baseUrl; ?>theme/default/assets/images/email/logo.png"></td>
								<td width="50%" height="110" style="text-align:right; font-size:13px; line-height:18px">
									<?php if($userId!=''): ?>
										ID Utilizator: <b><?php echo $userId; ?></b>
									<?php endif; ?><br>
									<a style="text-decoration:none; color:#1E90FF" href="<?php echo $baseUrl; ?>autentificare/">Autentificare</a>
								</td>
							</tr>
						</table>
					</td>
				</tr>
			</table>
			<?php echo $this->fetch('content'); ?>
			<table width="660" cellspacing="0" style="margin:20px 20px 0 20px; table-layout: auto;">
                <tr>
                    <td colspan="7" style="text-align:center; padding-bottom:20px;">
                        <div style="color:#148AD6; font-size:20px;">
                            URMARESTE-NE PE SOCIAL MEDIA
                        </div>
                    </td>
                </tr>
                <tr>
                    <td colspan="1">
                        <a href="https://www.facebook.com/GSMnet.ro/" title="Facebook">
                            <img height="60" alt="GSMNet.ro pe Facebook"
                             src="<?php echo $baseUrl ?>theme/default/assets/images/email/facebook.png">
                        </a>
                    </td>
                    <td colspan="1">
                        <a href="https://www.youtube.com/c/GSMNETMobiparts+" title="Youtube">
                            <img height="60" alt="GSMNet.ro pe Youtube"
                                 src="<?php echo $baseUrl ?>theme/default/assets/images/email/youtube.png">
                        </a>
                    </td>
                    <td colspan="1">
                        <a href="https://www.linkedin.com/company/gsmnet" title="Linkedin">
                            <img height="60" alt="GSMNet.ro pe Linkedin"
                                 src="<?php echo $baseUrl ?>theme/default/assets/images/email/linkedin.png">
                        </a>
                    </td>
                    <td colspan="1">
                        <a href="https://plus.google.com/+gsmnetroMobiparts" title="Google Plus">
                            <img height="60" alt="GSMNet.ro pe Google Plus"
                                 src="<?php echo $baseUrl ?>theme/default/assets/images/email/google_plus.png">
                        </a>
                    </td>
                    <td colspan="1">
                        <a href="https://search.google.com/local/writereview?placeid=ChIJizIAa5nlukARiaVdIlddOLg"
                           title="Google Business">
                            <img height="60" alt="GSMNet.ro pe Google Business"
                                 src="<?php echo $baseUrl ?>theme/default/assets/images/email/google_business.png">
                        </a>
                    </td>
                    <td colspan="2" style="text-align: right;">
                        <a href="<?php echo $baseUrl; ?>">
                            <img height="60" src="<?php echo $baseUrl; ?>theme/default/assets/images/email/logo.png">
                        </a>
                    </td>
                </tr>
                <tr>
                    <td colspan="7" style="padding-top:15px;"></td>
                </tr>
                <tr bgcolor="#f4f4f4" style="text-align:center;">
                    <td colspan="7" style="color:#888; font-size:13px; line-height:18px; padding: 5px 0; vertical-align:top">
                        Copyright 2007-2017 <?= Configure::read('COMPANY_NAME') ?> <?= Configure::read('COMPANY_PHONE') ?> <?= Configure::read('COMPANY_ADDRESS_1') ?>
                    </td>
                </tr>
			</table>
		</td>
	</tr>
</table>
</body>
</html>