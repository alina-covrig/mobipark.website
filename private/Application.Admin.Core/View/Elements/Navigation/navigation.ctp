<!-- Navigation -->
<nav class="navbar navbar-default navbar-static-top" role="navigation" style="margin-bottom: 0">
    <div class="navbar-header">

        <!-- /.navbar-top-links -->
        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
            <span class="sr-only"><?php echo __("Comuta navigarea")?></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
        </button>
        <ul class="nav navbar-top-links navbar-right mobile-navbar">
            <li class="dropdown">
                <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                    <?php
                    if(!empty($User['picture'])) {
                        echo '<img src="'.$User['picture'].'" style="width:30px; height:30px; border-radius:30px">';
                    } else { ?>
                        <span style="display: inline-block;width: 18px; height: 18px; border-radius: 50px;
                                color: <?=t_color(stringToColorCode($Person['fname'].$Person['lname']))?>;
                                background-color: #<?=stringToColorCode($Person['fname'].$Person['lname'])?>;
                                text-transform: uppercase;text-align: center;line-height: 18px;font-size: 9px;">
                        <?=substr($Person['fname'], 0, 1)?><?=substr($Person['lname'], 0, 1)?>
                    </span>
                    <?php } ?>
                    <i class="fa fa-caret-down"></i>
                </a>
                <ul class="dropdown-menu dropdown-user">
                    <li><a href="/customers/users/edit/<?php echo $User['id']?>"><i class="fa fa-user fa-fw"></i> <?php echo __("Profilul tau")?></a>
                    </li>
                    <li class="divider"></li>
                    <li><a href="/logout"><i class="fa fa-sign-out fa-fw"></i> <?php echo __("Deconectare")?></a>
                    </li>
                </ul>
                <!-- /.dropdown-user -->
            </li>
            <!-- /.dropdown -->
        </ul>
        <a class="navbar-brand" href="/dash">
            <span class="bigger">
                <?php echo Configure::read('STORE_NAME')?>
            </span>
            <?php echo __('Administrare')?>
            <?=$this->fetch('page-title')?>
        </a>
    </div>
    <!-- /.navbar-header -->

    <?php echo $this->element('Navigation/navbar-top-right')?>
    <?php echo $this->element('Navigation/navbar-left')?>

</nav>