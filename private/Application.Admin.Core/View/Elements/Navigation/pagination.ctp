<div class="row hidden-print" style="margin-bottom: 10px;">
    <div class="pagination pagination-small small pull-right">
        <?php
        $currentPage = (int) $this->Paginator->counter('{:page}');
        $totalPages = (int) $this->Paginator->counter('{:pages}');
        if(!empty($showCount)) {
            echo '<span class="nr">';
            echo $this->Paginator->counter('Pagina {:page} / {:pages} &bull; Total rezultate: {:count}');
            echo '</span>';
        }
        if($totalPages > 1) {
            if (!empty($showSelect)) {
                ?>
                <select class="trigger_select_page form-control input-xs"
                        data-crt-page="<?= $currentPage ?>">
                    <option value="" disabled>Pag.</option>
                    <?php for ($i = 1; $i <= $totalPages; $i++) : ?>
                        <option value="<?= $i ?>" <?= ($currentPage == $i) ? 'selected' : '' ?>><?= $i ?></option>
                    <?php endfor; ?>
                </select>
                <?php
            }
            ?>
            <ul class="pagination small">
                <?php
                echo $this->Paginator->prev(__('Inapoi'), array('tag' => 'li'), null, array('tag' => 'li', 'class' => 'disabled', 'disabledTag' => 'a'));
                echo $this->Paginator->numbers(array('separator' => '', 'currentTag' => 'a', 'currentClass' => 'active', 'tag' => 'li', 'first' => 1));
                echo $this->Paginator->next(__('Inainte'), array('tag' => 'li', 'currentClass' => 'disabled'), null, array('tag' => 'li', 'class' => 'disabled', 'disabledTag' => 'a'));
                ?>
            </ul>
            <?php
        }
        ?>
    </div>
</div>