<!--Trebuie generat cumva :)-->
<?php $this->start('extra-scripts')?>
    <script type="text/javascript">
        $(document).ready(function() {
            $('.wide-search-trigger').click(function() {
                $('.wide-search-bling-small').stop().fadeToggle(200);
            });
        });
        $(document).on('click', '#switch-menu-visibility', function() {
            $btn = $(this);
            $wrapper = $('#wrapper');
            $tree_lbls = $('.side-menu-tree span');
            $search_trigger = $('.wide-search-trigger');
            $large_search = $('.wide-search-bling-large');
            $visibility_switch = $('#switch-menu-visibility i');

            $visibility_switch
                .fadeToggle("fast", "linear").toggleClass('fa-caret-right')
                .fadeToggle("fast", "linear").toggleClass('fa-caret-left');
            if ($wrapper.hasClass('wide')){
                console.log('opening');
                $search_trigger.hide();
                $wrapper.delay(350).toggleClass('wide');
                $tree_lbls.delay(250).fadeToggle("fast", "linear" );
                $large_search.delay(250).fadeToggle("fast", "linear" );
            } else {
                console.log('closing');
                $tree_lbls.fadeToggle("fast", "linear");
                $large_search.fadeToggle("fast", "linear");
                $wrapper.delay(200).toggleClass('wide',"fast");
                $search_trigger.delay(250).fadeToggle(400, "linear");
            }
        });
    </script>
<?php $this->end()?>

<div class="navbar-default sidebar" role="navigation">
    <div class="sidebar-nav navbar-collapse">
        <ul class="nav" id="side-menu">
            <?php foreach($sidebar_menu as $m) :?>
                <?php if((!empty($m['Action']['path']) && in_array($m['Action']['path'], $AccessList)) || empty($m['Action']['path'])) :?>
                    <li class="side-menu-tree">
                        <a href="<?php echo (empty($m['Menu']['children']))?$m['Action']['path']:'#'?>" title="<?php echo $m['Menu']['name']?>" data-toggle="tooltip" data-placement="right">
                            <i class="fa <?php echo $m['Menu']['class']?> fa-fw"></i> 
                            <span> <?php echo $m['Menu']['name']?> </span>
                            <?php if(!empty($m['Menu']['children'])):?>
                                <i class="fa arrow"></i>
                            <?php endif;?>
                        </a>
                        <?php if(!empty($m['Menu']['children'])) :?>
                            <ul class="nav nav-second-level">
                                <?php foreach($m['Menu']['children'] as $submenu):?>
                                    <?php if((!empty($submenu['Action']['path']) && in_array($submenu['Action']['path'], $AccessList)) || empty($submenu['Action']['path'])) :?>
                                        <li>
                                            <a href="<?php echo (empty($submenu['Menu']['children']))?$submenu['Action']['path']:'#'?>" title="<?php echo $submenu['Menu']['name']?>"  data-toggle="tooltip" data-placement="right">
                                                <i class="fa <?php echo $submenu['Menu']['class']?> fa-fw"></i> 
                                               <span> <?php echo $submenu['Menu']['name']?> </span>
                                                <?php if(!empty($submenu['Menu']['children'])):?>
                                                    <i class="fa arrow"></i>
                                                <?php endif;?>
                                            </a>
                                            <?php if(!empty($submenu['Menu']['children'])) :?>
                                                <ul class="nav nav-third-level">
                                                    <?php foreach($submenu['Menu']['children'] as $subsubmenu):?>
                                                        <?php if((!empty($subsubmenu['Action']['path']) && in_array($subsubmenu['Action']['path'], $AccessList)) || empty($subsubmenu['Action']['path'])) :?>
                                                            <li>
                                                                <a href="<?php echo (empty($subsubmenu['Menu']['children']))?$subsubmenu['Action']['path']:'#'?>" title="<?php echo $subsubmenu['Menu']['name']?>">
                                                                    <i class="fa <?php echo $subsubmenu['Menu']['class']?> fa-fw"></i>
                                                                    <span> <?php echo $subsubmenu['Menu']['name']?> </span>
                                                                </a>
                                                            </li>
                                                        <?php endif;?>
                                                    <?php endforeach;?>
                                                </ul>
                                                <!-- /.nav-third-level -->
                                            <?php endif;?>
                                        </li>
                                    <?php endif;?>
                                <?php endforeach;?>
                            </ul>
                            <!-- /.nav-second-level -->
                        <?php endif;?>
                    </li>
                <?php endif;?>
            <?php endforeach;?>
            <li class="switch-menu-visibility-container">
                <span id="switch-menu-visibility" class="hidden-print"><i class="fa fa-caret-right"></i></span>
            </li>
        </ul>
    </div>
    <!-- /.sidebar-collapse -->
</div>
<!-- /.navbar-static-side -->