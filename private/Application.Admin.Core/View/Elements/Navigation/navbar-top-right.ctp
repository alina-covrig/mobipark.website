<ul class="nav navbar-top-links navbar-right">
    <li class="dropdown">
        <a class="dropdown-toggle" data-toggle="dropdown" href="#">
            <?php
                if(!empty($User['picture'])) {
                    echo '<img src="'.$User['picture'].'" style="width:30px; height:30px; border-radius:30px">';
                } else { ?>
                    <!--
                    <i class="fa fa-user fa-fw"></i> -->
                    <span style="display: inline-block;width: 18px; height: 18px; border-radius: 50px;
                            color: <?=t_color(stringToColorCode($Person['fname'].$Person['lname']))?>;
                            background-color: #<?=stringToColorCode($Person['fname'].$Person['lname'])?>;
                            text-transform: uppercase;text-align: center;line-height: 18px;font-size: 9px;">
                        <?=substr($Person['fname'], 0, 1)?><?=substr($Person['lname'], 0, 1)?>
                    </span>
                <?php } ?>
                <i class="fa fa-caret-down"></i>
        </a>
        <ul class="dropdown-menu dropdown-user">
            <li><a href="/customers/users/edit/<?php echo $User['id']?>"><i class="fa fa-user fa-fw"></i> <?php echo __("Profilul tau")?></a>
            </li>
            <li class="divider"></li>
            <li><a href="/logout"><i class="fa fa-sign-out fa-fw"></i> <?php echo __("Deconectare")?></a>
            </li>
        </ul>
        <!-- /.dropdown-user -->
    </li>
    <!-- /.dropdown -->
</ul>
<!-- /.navbar-top-links -->