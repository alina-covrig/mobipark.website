<div class="row">
    <div class="col-md-10 col-md-offset-1">
        <div class="alert alert-danger alert-dismissable mt20">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
            <?php echo $message?>
        </div>
    </div>
</div>