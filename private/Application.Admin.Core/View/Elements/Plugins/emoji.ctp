<?= $this->Html->css('/admin/css/plugins/emoji/emoji.css');?>
<?= $this->Html->script('/admin/js/plugins/emoji/config.js')?>
<?= $this->Html->script('/admin/js/plugins/emoji/util.js')?>
<?= $this->Html->script('/admin/js/plugins/emoji/jquery.emojiarea.js')?>
<?= $this->Html->script('/admin/js/plugins/emoji/emoji-picker.js')?>
<script>
    $(function() {
        // Initializes and creates emoji set from sprite sheet
        window.emojiPicker = new EmojiPicker({
            emojiable_selector: '[data-emojiable=true]',
            assetsPath: '/admin/img/plugins/emoji',
            popupButtonClasses: 'fa fa-smile-o'
        });
        // Finds all elements with `emojiable_selector` and converts them to rich emoji input fields
        // You may want to delay this step if you have dynamically created input fields that appear later in the loading process
        // It can be called as many times as necessary; previously converted input fields will not be converted again
        window.emojiPicker.discover();
    });
</script>