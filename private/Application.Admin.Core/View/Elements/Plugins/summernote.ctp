<?php $this->start('extra-scripts')?>
    <script type="text/javascript">
        function uploadImgSummernote(files, editor, welEditable, element, url){
            let form_data = new FormData();
            let el = element;

            form_data.append("file", files[0]);

            $.ajax({
                data: form_data,
                type: "POST",
                url: url,
                cache: false,
                contentType: false,
                processData: false,
                success: function (r) {
                    if(r.data.err != ""){
                        alert(r.data.err);
                    } else {
                        $(el).summernote("editor.insertImage", r.data.url);
                    }
                }
            });
        }
    </script>
<?php $this->end()?>