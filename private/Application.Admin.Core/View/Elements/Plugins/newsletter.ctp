<?php $this->Html->script('/admin/js/plugins/newsletter/newsletter-builder.js', array('inline' => false));?>
<?php $this->Html->script('/admin/js/plugins/bootstrap-editable/js/bootstrap-editable.min.js', array('inline' => false));?>
<?php $this->Html->css('/admin/css/plugins/newsletter/Icomoon/style.css', array('inline' => false));?>
<?php $this->Html->css('/admin/css/plugins/newsletter/newsletter-builder.css', array('inline' => false));?>
<?php $this->Html->css('/admin/css/plugins/newsletter/newsletter.css', array('inline' => false));?>