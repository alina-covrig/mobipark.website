<?php
/**
 * The alias function
 *
 * Calls inflector slug with - as a second parmaeter
 * Calls strtolower
 *
 * @param string $who the string to return the alias
 *
 * @return string
 */
function alias ($who)
{
    $who = str_replace('&', ' ', $who);
    $who = str_replace('@', ' at ', $who);
    $who = str_replace('+', ' plus ', $who);
    $who = str_replace(' si ', ' ', $who);
    $who = utf8_encode($who);
    $who = strtolower(trim($who));
    $who = preg_replace("/[^a-z0-9]+/", '-', $who);
    return $who;
    //return Inflector::slug (strtolower ($who), '-');
}

/**
 * The extract_strings function
 *
 * This function takes an associative array and filles the $vector
 * array with every string value the associative has
 * This is a recursive function
 *
 * @param array $array  The array to scan for strings
 * @param array $vector The built vector of strings
 *
 * @return void
 */
function extract_strings ($array, &$vector = array ())
{

    if (is_array ($array)) {
        foreach ($array as $value) {
            if (!is_array ($value)) {
                $vector[] = $value;
            } else {
                extract_strings ($value, $vector);
            }
        }
    } else {
        if (!is_array ($array)) {
            $vector[] = $array;
        }
    }
    if (is_array ($vector)) {
        $vector = array_unique ($vector);
    }
}

function val2mess($array, $glue = '<br />') {
    extract_strings($array, $vector);
    return implode($glue, $vector);
}

function price ($price, $rate = 1, $decimals = 2)
{
    $price = round($price/$rate, $decimals);
    return number_format ($price, $decimals, '.', '');
}

function pretty_price($price) {
    $price = round($price, 2);
    return number_format($price, 2, '.', ',');
}

function p ($price, $dv = 0, $dp = 0)
{
    $dv = floatval($dv);
    $dp = floatval($dp);
    return price(($price - $dv) * (1 - ($dp/100)));
}

function label ($string)
{
    return array ('label' => $string);
}

function humanTiming_ro ($time)
{
    $tokens = array (
        31536000 => 'an',
        2592000 => 'luna',
        604800 => 'saptamana',
        86400 => 'zi',
        3600 => 'ora',
        60 => 'minut',
        1 => 'secunda'
    );

    $tokens_pl = array (
        31536000 => 'ani',
        2592000 => 'luni',
        604800 => 'saptamani',
        86400 => 'zile',
        3600 => 'ore',
        60 => 'minute',
        1 => 'secunde'
    );

    if (time () >= $time) {
        $time = time () - $time;
        foreach ($tokens as $unit => $text) {
            if ($time < $unit) continue;
            $numberOfUnits = floor ($time / $unit);
            if ($numberOfUnits > 1) {
                return 'in urma cu ' . $numberOfUnits . ' ' . $tokens_pl[$unit];
            }
            return 'in urma cu ' . $numberOfUnits . ' ' . $text;
        }
    }
    else {
        $time = $time - time();
        foreach ($tokens as $unit => $text) {
            if ($time < $unit) continue;
            $numberOfUnits = floor ($time / $unit);
            if ($numberOfUnits > 1) {
                return 'peste ' . $numberOfUnits . ' ' . $tokens_pl[$unit];
            }
            return 'peste ' . $numberOfUnits . ' ' . $text;
        }
    }
}

function stringToColorCode ($str)
{
    $code = dechex (crc32 ($str));
    $code = substr ($code, 0, 6);
    return $code;
}

function complementaryHEX ($hexcode)
{
    $redhex = substr ($hexcode, 0, 2);
    $greenhex = substr ($hexcode, 2, 2);
    $bluehex = substr ($hexcode, 4, 2);
    $var_r = (hexdec ($redhex)) / 255;
    $var_g = (hexdec ($greenhex)) / 255;
    $var_b = (hexdec ($bluehex)) / 255;
    $var_min = min ($var_r, $var_g, $var_b);
    $var_max = max ($var_r, $var_g, $var_b);
    $del_max = $var_max - $var_min;
    $l = ($var_max + $var_min) / 2;

    if ($del_max == 0) {
        $h = 0;
        $s = 0;
    } else {
        if ($l < 0.5) {
            $s = $del_max / ($var_max + $var_min);
        } else {
            $s = $del_max / (2 - $var_max - $var_min);
        };

        $del_r = ((($var_max - $var_r) / 6) + ($del_max / 2)) / $del_max;
        $del_g = ((($var_max - $var_g) / 6) + ($del_max / 2)) / $del_max;
        $del_b = ((($var_max - $var_b) / 6) + ($del_max / 2)) / $del_max;

        if ($var_r == $var_max) {
            $h = $del_b - $del_g;
        } elseif ($var_g == $var_max) {
            $h = (1 / 3) + $del_r - $del_b;
        } elseif ($var_b == $var_max) {
            $h = (2 / 3) + $del_g - $del_r;
        };

        if ($h < 0) {
            $h += 1;
        };

        if ($h > 1) {
            $h -= 1;
        };
    };
    $h2 = $h + 0.5;

    if ($h2 > 1) {
        $h2 -= 1;
    };
    if ($s == 0) {
        $r = $l * 255;
        $g = $l * 255;
        $b = $l * 255;
    } else {
        if ($l < 0.5) {
            $var_2 = $l * (1 + $s);
        } else {
            $var_2 = ($l + $s) - ($s * $l);
        };

        $var_1 = 2 * $l - $var_2;
        $r = 255 * hue_2_rgb ($var_1, $var_2, $h2 + (1 / 3));
        $g = 255 * hue_2_rgb ($var_1, $var_2, $h2);
        $b = 255 * hue_2_rgb ($var_1, $var_2, $h2 - (1 / 3));
    };
    $rhex = sprintf ("%02X", round ($r));
    $ghex = sprintf ("%02X", round ($g));
    $bhex = sprintf ("%02X", round ($b));

    $rgbhex = $rhex . $ghex . $bhex;
    return $rgbhex;

}

function hue_2_rgb ($v1, $v2, $vh)
{
    if ($vh < 0) {
        $vh += 1;
    };

    if ($vh > 1) {
        $vh -= 1;
    };

    if ((6 * $vh) < 1) {
        return ($v1 + ($v2 - $v1) * 6 * $vh);
    };

    if ((2 * $vh) < 1) {
        return ($v2);
    };

    if ((3 * $vh) < 2) {
        return ($v1 + ($v2 - $v1) * ((2 / 3 - $vh) * 6));
    };

    return ($v1);
}

function t_color($hexcolor)
{
    return (hexdec($hexcolor) > 0xffffff/2) ? '#000':'#fff';
}

/**
 * @param $cif
 * @return bool
 */
function checkCIF($cif=''){
    $cif = trim(strtolower($cif),'ro');

    if (!is_numeric($cif)) {
        return false;
    }
    if (strlen($cif)>10 ) {
        return false;
    }

    $control_key = substr($cif, -1);
    $cif = substr($cif, 0, -1);
    while (strlen($cif)!=9){
        $cif = '0'.$cif;
    }

    $sum =
        $cif[0] * 7 + $cif[1] * 5 + $cif[2] * 3
        + $cif[3] * 2+ $cif[4] * 1 + $cif[5] * 7
        + $cif[6] * 5 + $cif[7] * 3 + $cif[8] * 2;
    $sum = $sum*10;
    $rest = fmod($sum, 11);
    if ($rest==10 ) {
        $rest=0;
    }
    if ($rest == $control_key) {
        return true;
    } else {
        return false;
    }
}

function generatePassword ($length = 8, $specialChars = 0) {
    $chars = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLM'
        . 'NOPQRSTUVWXYZ0123456789';
    if ($specialChars) {
        $chars .= '!@#$%^&*()_';
    }
    $count = mb_strlen($chars);

    for ($i = 0, $result = ''; $i < $length; $i++) {
        $index = rand(0, $count - 1);
        $result .= mb_substr($chars, $index, 1);
    }

    return $result;
}

function getSaleDocumentType($type) {
    $type_translated = '';

    switch ($type) {
        case 'awb':
            $type_translated = __('awb');
            break;
        case 'invoice':
            $type_translated = __('factura');
            break;
        case 'packing_list':
            $type_translated = __('deviz');
            break;
    }

    return strtoupper($type_translated);
}

function randomNumber($length)
{
    $result = '';

    for($i = 0; $i < $length; $i++) {
        $result .= mt_rand(1, 9);
    }

    return $result;
}

function getWebPage($url, $follow_location=true, $post=false, $post_fields=array(), $referer="http://www.google.com", $cookie="", $headers=array())
{
    if(!function_exists("curl_init"))
    {
        return array("content"=>file_get_contents($url));
    }

    $user_agent="Mozilla/5.0 (Windows; U; Windows NT 6.1; en-US; rv:1.9.2.3) Gecko/20100401 Firefox/3.6.3 GTB7.0";

    $options = array(
        CURLOPT_RETURNTRANSFER => true,        				// return web page
        CURLOPT_SSL_VERIFYPEER => false,					// ssl verify peer
        CURLOPT_SSL_VERIFYHOST => false,					// ssl verify host
        CURLOPT_HEADER         => false,       				// don't return headers
        CURLOPT_FOLLOWLOCATION => $follow_location,        	// follow redirects
        CURLOPT_ENCODING       => "",          				// handle all encodings
        CURLOPT_USERAGENT      => $user_agent, 				// who am i
        CURLOPT_AUTOREFERER    => true,       				// set referer on redirect
        CURLOPT_CONNECTTIMEOUT => 120,         				// timeout on connect
        CURLOPT_TIMEOUT        => 120,         				// timeout on response
        CURLOPT_MAXREDIRS      => 10,         				// stop after 10 redirects
        CURLOPT_REFERER		   => $referer 					// referer
    );

    if($post && !empty($post_fields) && count($post_fields)>0)
    {
        $options[CURLOPT_POST]=$post;
        $options[CURLOPT_POSTFIELDS]=$post_fields;
    }

    if(!empty($cookie))
    {
        //$options[CURLOPT_COOKIEFILE]=DIR_FUNCTII_ABS."data/".$cookie;
        //$options[CURLOPT_COOKIEJAR]=DIR_FUNCTII_ABS."data/".$cookie;
    }

    if(!empty($headers))
    {
        $options[CURLOPT_HTTPHEADER]=$headers;
    }

    $ch      = curl_init($url);
    curl_setopt_array($ch, $options);
    $content = curl_exec($ch);
    $err     = curl_errno($ch);
    $errmsg  = curl_error($ch);
    $header  = curl_getinfo($ch);
    curl_close($ch);

    $return['errno']   = $err;
    $return['errmsg']  = $errmsg;
    $return['content'] = $content;

    return $return;
}

function sanitizeFilename($filename)
{
    $str = strip_tags($filename);
    $str = preg_replace('/[\r\n\t ]+/', ' ', $str);
    $str = preg_replace('/[\"\*\/\:\<\>\?\'\|]+/', ' ', $str);
    $str = strtolower($str);
    $str = html_entity_decode( $str, ENT_QUOTES, "utf-8" );
    $str = htmlentities($str, ENT_QUOTES, "utf-8");
    $str = preg_replace("/(&)([a-z])([a-z]+;)/i", '$2', $str);
    $str = str_replace(' ', '-', $str);
    $str = rawurlencode($str);
    $str = str_replace('%', '-', $str);
    return $str;
}

function compareFloatNumbers($float1, $float2, $operator='=')
{
    // Check numbers to 5 digits of precision
    $epsilon = 0.00001;

    $float1 = (float)$float1;
    $float2 = (float)$float2;

    switch ($operator)
    {
        // equal
        case "=":
        case "eq":
            {
                if (abs($float1 - $float2) < $epsilon) {
                    return true;
                }
                break;
            }
        // less than
        case "<":
        case "lt":
            {
                if (abs($float1 - $float2) < $epsilon) {
                    return false;
                }
                else
                {
                    if ($float1 < $float2) {
                        return true;
                    }
                }
                break;
            }
        // less than or equal
        case "<=":
        case "lte":
            {
                if (compareFloatNumbers($float1, $float2, '<') || compareFloatNumbers($float1, $float2, '=')) {
                    return true;
                }
                break;
            }
        // greater than
        case ">":
        case "gt":
            {
                if (abs($float1 - $float2) < $epsilon) {
                    return false;
                }
                else
                {
                    if ($float1 > $float2) {
                        return true;
                    }
                }
                break;
            }
        // greater than or equal
        case ">=":
        case "gte":
            {
                if (compareFloatNumbers($float1, $float2, '>') || compareFloatNumbers($float1, $float2, '=')) {
                    return true;
                }
                break;
            }
        case "<>":
        case "!=":
        case "ne":
            {
                if (abs($float1 - $float2) > $epsilon) {
                    return true;
                }
                break;
            }
        default:
            {
                die("Unknown operator '".$operator."' in compareFloatNumbers()");
            }
    }

    return false;
}

function arrayHasValues($array)
{
    if (!is_array($array)) {
        return false;
    }

    $has_values = false;
    foreach ($array as $a) {
        if ($a !== '') {
            $has_values = true;
            break;
        }
    }

    return $has_values;
}

function transformDateRomaniaMonths($date, $separator = '-')
{
    $str_to_time = strtotime($date);

    $Y = date('Y', $str_to_time);
    $M = date('m', $str_to_time);
    $M = romaniaMonths($M);
    $D = date('j', $str_to_time);

    return $D . $separator . $M . $separator . $Y;
}

function romaniaMonths($month_number)
{
    switch ($month_number) {
        case '01':
            return 'Ianuarie';
        case '02':
            return 'Februarie';
        case '03':
            return 'Martie';
        case '04':
            return 'Aprilie';
        case '05':
            return 'Mai';
        case '06':
            return 'Iunie';
        case '07':
            return 'Iulie';
        case '08':
            return 'August';
        case '09':
            return 'Septembrie';
        case '10':
            return 'Octombrie';
        case '11':
            return 'Noiembrie';
        case '12':
            return 'Decembrie';
    }
}