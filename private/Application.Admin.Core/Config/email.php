<?php
/**
* EmailConfig file
*
* PHP version 5.4
*
* @category  Frontend
* @package   Generic
* @author    Daniel Ionascu <daniel.ionascu88@gmail.com>
* @copyright 2014 Evercloud
* @license   http://www.php.net/license/3_01.txt  PHP License 3.01
* @version   SVN: $Id$
* @link      /frontend/email.php
* @since     Class available since Release 1.0
*/

/**
* UsersController Class
* 
* Users logic
*
* @category  Frontend
* @package   Generic
* @author    Daniel Ionascu <daniel.ionascu88@gmail.com>
* @copyright 2014 Evercloud
* @license   http://www.php.net/license/3_01.txt  PHP License 3.01
* @version   Release: 1.0
* @link      /frontend/email.php
* @since     Class available since Release 1.0
*/
class EmailConfig
{

   /* public $default = array(
        'transport' => 'Smtp',
        'from' => array('office@parfums-de-provence.ro' => 'Parfums De Provence'),
        'host' => 'smtp.mailgun.org',
        'port' => 2525,
        'timeout' => 30,
        'username' => 'postmaster@mg.gsmnet.ro',
        'password' => 'cdf2d170d1245495fabe8848f44c6f92',
        'emailFormat' => 'html',
        //'charset' => 'utf-8',
        //'headerCharset' => 'utf-8',
    );*/

    public $default = [
        'transport' => 'Smtp',
       'from' => array('office@gsmnet.ro' => 'GSMnet.ro'),
        'host' => 'smtp.eu.mailgun.org',
        'port' => 587 ,
        'timeout' => 30,
        'username' => 'postmaster@mg.gsmnet.ro',
        'password' => 'cdf2d170d1245495fabe8848f44c6f92',
        'emailFormat' => 'html',
    ];
}
