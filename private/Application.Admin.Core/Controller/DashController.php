<?php
/**
 * Dashboard Controller
 *
 * This file is the ground file for showing combined modules pages
 *
 * PHP version 5.4
 *
 * @category  Admin
 * @package   Generic
 * @author    Lucian Vasile <lucian.vasile@live.com>
 * @copyright 2014 Evercloud
 * @license   http://www.php.net/license/3_01.txt  PHP License 3.01
 * @version   SVN: $Id$
 * @link      /admin/core/controller/PagesController.php
 * @since     1.0
 */

App::uses('Controller', 'AppController');

/**
 * DashController Class
 *
 * Inside this Controller pages common for modules will be implemented
 *
 * @category  Admin
 * @package   Generic
 * @author    Lucian Vasile <lucian.vasile@live.com>
 * @copyright 2014 Evercloud
 * @license   http://www.php.net/license/3_01.txt  PHP License 3.01
 * @version   Release: 1.0
 * @link      /admin/core/controller/pagesController.php
 * @since     Class available since Release 1.0
 */
class DashController extends AppController
{


    /**
     * The index method
     *
     * @return void
     */
    public function index()
    {
        $this->render('/Pages/home');
    }
}
