<?php
/**
 * SMSComponent.php file
 *
 * PHP version 5.4
 *
 * @category  Admin
 * @package   None
 * @author    Catalin Prodan <clinpan@gmail.com>
 * @copyright 2014 Evercloud
 * @license   http://www.php.net/license/3_01.txt  PHP License 3.01
 * @version   SVN: $Id$
 * @link      /sms-component
 * @since     1.0
 */

/**
 * SMSComponent Class
 *
 * Inside this Component any SMS communication methods will be placed
 *
 * @category  Admin
 * @package   None
 * @author    Catalin Prodan <clinpan@gmail.com>
 * @copyright 2014 Evercloud
 * @license   http://www.php.net/license/3_01.txt  PHP License 3.01
 * @version   Release: 1.0
 * @link      /sms-component
 * @since     Class available since Release 1.0
 */
class SMSComponent extends Component
{
    
    var $providers = array(
        'web2sms' => array(
            'ws_endpoint' => "https://www.web2sms.ro/wsi/service.php?wsdl",
            'ws_username' => 'alexmamo',
            'ws_authKey' => '4b35b9fc90df91bf9f93664b667afef722d30743',
        )
    );
    
    var $scheduleDate = null;
    var $validity = null;
    var $callbackUrl = null;
    
    const DEFAULT_PROVIDER = 'web2sms';
    
    /**
     * The send method
     * 
     * @param string $message   the SMS message to send
     * @param string $recipient the SMS recipient's phone number
     *  
     * @return boolean
     */
    public function send($message = '', $recipient = '') 
    {
        if (Configure::read('debug') == 2) {
            return true;
        }

        if ($message == '' || $recipient == '') {
            return false;
        }
        
        $url = $this->providers[self::DEFAULT_PROVIDER]['ws_endpoint'];
        $soapClient = new SoapClient($url);
        try {
            $resp = $soapClient->sendSmsAuthKey(
                $this->providers[self::DEFAULT_PROVIDER]['ws_username'],
                $this->providers[self::DEFAULT_PROVIDER]['ws_authKey'],
                Configure::read('STORE_NAME'),
                $recipient,
                $message,
                $this->scheduleDate,
                $this->validity,
                $this->callbackUrl
            );
            $sent = true;
        }  catch (Exception $e) {
            /*
            if ($verbose) {
                echo "Eroare la alocarea resurselor pentru un SMS!\n";
                echo "Cod: " . $e->getMessage() . "\n";
                echo "Mesaj: " . $e->getMessage() . "\n";
            }
            */
            $sent = false;
        }
        return $sent;
    }
}
