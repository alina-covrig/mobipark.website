<?php
/**
* Application level Controller
*
* This file is application-wide controller file. You can put all
* application-wide controller-related methods here.
*
* PHP version 5.4
*
* @category  Admin
* @package   Generic
* @author    Lucian Vasile <lucian.vasile@live.com>
* @copyright 2014 Evercloud
* @license   http://www.php.net/license/3_01.txt  PHP License 3.01
* @version   SVN: $Id$
* @link      /admin/core/controller/AppController.php
* @since     1.0
*/

App::uses('Controller', 'Controller');
App::uses('Folder', 'Utility');
App::uses('File', 'Utility');
App::uses('Model', 'AppModel');

/**
* AppController Class
*
* Inside this Controller application-wide logic methods
* will be placed
*
* @category  Admin
* @package   Generic
* @author    Lucian Vasile <lucian.vasile@live.com>
* @copyright 2014 Evercloud
* @license   http://www.php.net/license/3_01.txt  PHP License 3.01
* @version   Release: 1.0
* @link      /admin/core/controller/AppController.php
* @since     Class available since Release 1.0
 *
 * @property Configuration Configuration
 * @property Menu Menu
 * @property Permission Permission
 * @property User User
 * @property AppModel AppModel
*/
class AppController extends Controller
{
    // {{{ properties

    /**
* The application-wide components
* 
* This property is an array of application-wide 
* loaded components 
* 
* @var $components
*/
    public $components = array(
    'RequestHandler', 
    'Session', 
    'Cookie',
    'Paginator',
    //'DebugKit.Toolbar'
    );

    /**
    * The application-wide models
    * 
    * This property is an array of application-wide 
    * loaded Models 
    * 
    * @var $uses
    */
    public $uses = array(
        'Configuration',
        'Access.Menu',
        'Access.GroupPermission',
        'Access.Permission',
        'Customers.User',
        'Cms.Announcement',
        'Cms.Page',
        'AppModel',

    );

    public $respondWithArray = false;
    
    /**
    * The application-wide helpers
    * 
    * This property is an array of application-wide 
    * loaded Helpers 
    * 
    * @var $helpers
    */
    public $helpers = array(
        'Form' => array('className' => 'Bs3Helpers.Bs3Form'),
        'Html' => array('className' => 'Bs3Helpers.Bs3Html'),
        'H',
        'Panel',
    );
    
    /**
    * Allowed pages by Permissions module
    * 
    * @var $allowedPages
    */
    public $allowedPages = array(
        '/debug_kit/toolbar_access/sql_explain',
        '/dash/search',
        '/dash',
        '/dash/ticker',
        '/orders/sales/_regenerateInvoice',
        '/remoteGet',
    );

    public $superAllowedPages = array(
        '/login',
        '/forbidden',
        '/',
        '/logout',
    );
    
    public $channels = [];



    public $LastEntryDate_sql = 'SELECT LastEntry.created_at '
    . 'FROM merchandise__entries AS LastEntry '
    . 'LEFT JOIN merchandise__entry_products AS ep '
    . 'ON ep.entry_id = LastEntry.id '
    . 'WHERE ep.product_id = Product.id '
    . 'AND LastEntry.invoice_number NOT LIKE "Nir transfer%"'
    . 'AND LastEntry.invoice_number NOT LIKE "RG %"'
    . 'ORDER BY LastEntry.created_at DESC '
    . 'LIMIT 1';

    
    /**
    * The beforeFilter method
    * 
    * This method is called upon every request,
    * before any other actions in the flow.
    * It's a callback method.
    * Be careful not to stuff it with unnecessary code
    * 
    * @return void
    */
    public function beforeFilter() 
    {
        Cache::clear(false, '_cake_core_');
        Cache::clear(false, '_cake_model_');
        Cache::clear(false, 'configurations');
        $this->_setRequestHeaderes();
        if (isset($this->request->query['url'])) {
            unset($this->request->query['url']);
        }
        $this->_getConfiguration();
        // bypass securitate normala prin api key din baza de date
        // versiunea 1 care e nerafinata
        if (!isset($_GET[Configure::read('SECRET_APP_KEY')])) {
            $this->_setCookieData();
            $this->_checkUser();
            $this->_checkAccess();
        }
        $this->_getMenu();
        $this->_getAnnouncements();
        parent::beforeFilter();
    }

    /**
     * The _getChannels method
     *
     * @return view
     */
    private function _getChannels() 
    {
        $channels = Cache::read('channels', 'eighth');
        if (!$channels) {
            $channels = $this->Sale->find(
                'list', [
                'fields' => [
                    'source',
                    'source',
                ],
                'group' => [
                    'source'
                ]
                ]
            );
            Cache::write('channels', $channels, 'eighth');
        }
        $this->channels = $channels;
        $this->set('channels', $channels);
    }

    /**
     * The _setRequestHeaderes private method
     *
     * This method sets custom request headers for custom request types
     *
     * @return void
     */
    private function _setRequestHeaderes()
    {
        if ($this->request->is('ajax')) {
            header('Expires: -1');
        }
    }

    /**
    * The getConfiguration method
    * 
    * This method will get the configurations from 
    * the database or from the cache and write them
    * to the Configure static object. They may be 
    * used application-wide (Model, Controller, 
    * View, Plugins, etc)
    * 
    * @return void
    */
    private function _getConfiguration() 
    {
        $configurations = Cache::read('Application.Configurations');
        $configurations = false;
        if (!$configurations) {
            $configurations = $this->Configuration->find(
                'list', 
                array(
                    'fields' => array(
                        'Configuration.name',
                        'Configuration.value',
                    )
                )
            );
            Cache::write('Application.Configurations', $configurations);
        }
        foreach ($configurations as $cname => $cvalue) {
            Configure::write($cname, $cvalue);
        }
    }
    
    /**
    * The getData method
    * 
    * This method is used by dataTables.js for server-side ajax requests
    * It returns a JSON string containing the information needed
    * by dataTables.js in it's specific format
    * 
    * $conditions_string example:
    * User.is_admin=1|User.email=lucian.vasile@live.com
    * 
    * @param string $model             the datasource model
    * @param string $conditions_string a string separating fields by |
    * @param string $returnAadata      only return $aaData
    * 
    * @return json
    */
    public function getData(
        $model, 
        $conditions_string = null, 
        $returnAadata = false
    ) {
        $filters = $this->_getFiltersForDataTables($_GET);
        if (!is_null($conditions_string)) {
            $raw_1 = explode("|", $conditions_string);
            foreach ($raw_1 as $raw_entry_1) {
                $raw_2 = explode('=', $raw_entry_1);
                $filters['conditions'][$raw_2[0]] = $raw_2[1];
            }
        }
        $this->Paginator->settings = $filters;
        $gridData = $this->Paginator->paginate($model);
        $aaData = array();
        $k = 0;
        foreach ($gridData as $data) {
            $i = 0;
            foreach ($this->displayFields as $modelName => $fields) {
                foreach ($fields as $field => $displayName) {
                    $aaData[$k][$i++] = $data[$modelName][$field];
                }
            }
            $k++;
        }
        if ($returnAadata) {
            return $aaData;
        }
        $returnarray = array(
            'sEcho' => (int) $_GET['sEcho'],
            'iTotalRecords' => $this->request->params['paging'][$model]['count'],
            'iTotalDisplayRecords' => 
                $this->request->params['paging'][$model]['count'],
            'aaData' => $aaData,
        );
        die(json_encode($returnarray));
    }
    
    /**
    * The getFiltersForDataTables method
    * 
    * This method is called withing the getData method and transforms the
    * $_GET array to an options array for Paginator component
    * 
    * @param array $get $_GET array from dataTables.js
    * 
    * @return array
    */
    private function _getFiltersForDataTables($get) 
    {
        
        $options = array();
        $fields = array();
        
        // get the fields 
        foreach ($this->displayFields as $model => $fieldList) {
            foreach ($fieldList as $field => $displayName) {
                $fields[] = "{$model}.{$field}";
            }
        }
        if (isset($get['sSearch']) && !empty($get['sSearch'])) {
            $options['conditions'] = array();
            foreach ($fields as $field) {
                if (strpos($field, 'is_') === false    ) {
                    $options['conditions']['OR'][] 
                        = array("{$field} LIKE" => '%' . $get['sSearch'] . '%');
                }
            }
        }
        for ($i = 0; $i < $get['iColumns']; $i++) {
            if (isset($get['sSearch_' . $i]) && !empty($get['sSearch_' . $i])) {
                $options['conditions']['OR'][] 
                    = array(
                        $fields[$i] . ' LIKE' => '%' . $get['sSearch_' . $i] . '%'
                    );
            }
        }
        if (isset($get['iSortCol_0']) && !empty($get['iSortCol_0'])) {
            $dir = 'asc';
            if (isset($get['sSortDir_0']) && !empty($get['sSortDir_0'])) {
                $dir = $get['sSortDir_0'];
            }
            $options['order'] = array($fields[$get['iSortCol_0']] => $dir);
        }
        if ($get['iDisplayLength'] != -1) {
            $options['maxlimit'] = $get['iDisplayLength'];
            $options['limit'] = $get['iDisplayLength'];
            $options['page'] = floor($get['iDisplayStart'] / $get['iDisplayLength'])
                + 1;
        }
        return $options;
    }
    
    /**
    * The _getMenu method
    * 
    * Gets the menu from the cache or generates it if
    * not cached and sets it to the view
    * 
    * @return void
    */
    private function _getMenu() 
    {
        $menu = Cache::read('Application.Menu');
        //$menu = false;
        if (!$menu) {
            // get only three levels
            $menu = $this->Menu->find(
                'all', 
                array(
                    'conditions' => array(
                        'Menu.parent_id' => null
                    ),
                    'order' => array("Menu.lft")
                )
            );
            foreach ($menu as &$m) {
                $m['Menu']['children'] = $this->Menu->children(
                    $m['Menu']['id'], 
                    true, 
                    null, 
                    "Menu.lft", 
                    null, 
                    1, 
                    1
                );
                foreach ($m['Menu']['children'] as &$sm) {
                    $sm['Menu']['children'] = $this->Menu->children(
                        $sm['Menu']['id'], 
                        true, 
                        null, 
                        "Menu.lft", 
                        null, 
                        1, 
                        1
                    );
                }
            }
            Cache::write('Application.Menu', $menu);
        }
        $this->set('sidebar_menu', $menu);
    }
    
    /**
    * The _setCookieData method
    * 
    * Sets the name of the cookie and other settings
    * Sets auth cookies to the view to be used
    * 
    * @return void
    */
    private function _setCookieData() 
    {
        $this->Cookie->name = Configure::read('COOKIE_NAME');
        $this->Cookie->time = '24 hours';
        $this->Cookie->path = '/';
        $this->Cookie->httpOnly = true;
        $this->Cookie->type('aes');
        if ($this->Cookie->check(sha1('Person'))) {
            $this->set('Person', $this->Cookie->read(sha1('Person')));
        }
        if ($this->Cookie->check(sha1('User'))) {
            $user = $this->Cookie->read(sha1('User'));
            Configure::write('USER_EMAIL', $user['email']);
            Configure::write('USER_HASH', $user['hash']);
            Configure::write('USER_ID', $user['id']);
            $this->set('User', $user);
        }
        if (isset($_GET['email']) && isset($_GET['hash'])) {
            Configure::write('USER_EMAIL', $_GET['email']);
            Configure::write('USER_HASH', $_GET['hash']);
        }
        if ($this->getAccessList()) {
            $this->set('AccessList', $this->getAccessList());
        }
        $this->set('global_search_term', $this->Cookie->read('search_term'));
    }
    
    /**
    * The _checkUser method
    * 
    * This method checks if an user is logged in
    * 
    * @return void or redirect /
    */
    private function _checkUser() 
    {
        if (in_array($this->request->here, $this->superAllowedPages)) {
            return;
        }
        if (isset($_GET['hash']) && isset($_GET['email'])) {
            return;
        }
        if (!$this->Cookie->check(sha1('User'))
            || !$this->Cookie->check(sha1('Person'))
        ) {
            $this->Cookie->delete(sha1('User'));
            $this->Cookie->delete(sha1('Person'));
            $this->redirect('/');
        }

        $id = $this->getCurrentUserId();
        if ($this->User->field('request_login', array('User.id' => $id)) == 1) {
            $this->User->id = $id;
            $this->User->saveField('request_login', 0);
            $this->deleteAccessList();
            $this->redirect('/');
        }
        return;
    }
    
    /**
    * The _checkAccess method
    * 
    * This method checks if there is access to a 
    * certain action
    * 
    * @return void or redirect /forbidden
    */
    private function _checkAccess() 
    {
        if (in_array($this->request->here, $this->superAllowedPages)) {
            return;
        }
        if (!$this->getAccessList()) {
            $this->redirect('/');
        }
        $accessList = $this->getAccessList();

        $module = $this->request->params['plugin'];
        $controller = $this->request->params['controller'];
        $action = '/' . $this->request->params['action'];
        if ($action == '/getData') {
            return;
        }
        if ($action == '/index') {
            $action = '';
        }
        $path = '/' . $module . '/' . $controller . $action;

        if (strpos($this->request->here, '/dash/') !== false) {
            return;
        }
        if (empty($this->request->params['plugin'])
            && $this->request->params['controller'] == 'app'
            && !empty($this->request->params['action'])
            && in_array('/' . $this->request->params['action'], $this->allowedPages)
        ) {
            return;
        }
        if (in_array($this->request->here, $this->allowedPages)) {
            return;
        }
        if (!in_array($path, $accessList)) {
            return $this->redirect('/forbidden');
        }
        return;
    }
    
    /**
    * The getRedirectData method
    * 
    * This method gets the data set by setRedirectData
    * 
    * @param Model $model the model to check for validation errors
    * 
    * @return void
    */
    public function getRedirectData($model) 
    {
        if ($this->Session->check('validationErrors')) {
            $this->$model->validationErrors 
                = $this->Session->read('validationErrors');
            $this->Session->delete('validationErrors');
        }
    }
    
    /**
    * The setRedirectData method
    * 
    * This method is used to set the validation errors
    * from the referer page (if upon save there's a redirect)
    * 
    * @param Model $model the model to check for validation errors
    * 
    * @return void
    */
    public function setRedirectData($model) 
    {
        $key = $model;
        $model = preg_replace('/-[0-9]+/', '', $key);
        if (isset($this->$model->validationErrors) 
            && !empty($this->$model->validationErrors)
        ) {
            if (isset($this->$model->validationErrors['alias'][0])) {
                $this->Session->setFlash(
                    $this->$model->validationErrors['alias'][0], 
                    'Alerts/error'
                );
            }
            $this->Session->write(
                'validationErrors', 
                $this->$model->validationErrors
            );
        }
    }
    
    /**
    * The setValidationFlash method
    * 
    * Convenience method for setting all validation erros in one string
    * 
    * @param Model     $model   the model to check for validation erros
    * @param undefined $key     the place where to display the flash
    * @param undefined $element the element to use to display the message
    * 
    * @return void
    */
    public function setValidationFlash(
        $model, 
        $key = 'flash', 
        $element = 'Alerts/error'
    ) {
        $msg_array = array();
        foreach ($this->$model->validationErrors as $value) {
            $msg_array[] = implode("<br />", $value);
        }
        $msg = implode("<br />", $msg_array);
        $this->Session->setFlash($msg, $element, array(), $key);
    }
    
    /**
    * The cleanRequestDataKeys method
    * 
    * This changes the main key from $this->request->data
    * Eg: if the main key is like ModelName-2 it changes
    * it to ModelName
    * 
    * @return void
    */
    public function cleanRequestDataKeys() 
    {
        $data = $this->request->data;
        $key = key($data);
        $model = preg_replace('/-[0-9]+/', '', $key);
        unset($this->request->data[$key]);
        $this->request->data[$model] = $data[$key];
    } 
    
    /**
    * The getAccessList method
    * 
    * This returns the current user access list
    * 
    * @return boolean or array
    */
    public function getAccessList() 
    {
        $id = $this->getCurrentUserId();
        if (!$id) {
            return false;
        }

        $accessList = Cache::read('AccessList' . $id, 'oney');
        if (!$accessList) {
            $accessList = Cache::read('AccessList' . $id, 'eighth');
        }

        // $id = 256413 (bogdan@gsmnet.ro sa nu fie delogat)
        if (isset($_GET['hash'])
            && isset($_GET['email'])
            && !$accessList
            || $id == 256413
            || $_SERVER['REMOTE_ADDR'] == '5.2.225.206'
            || $_SERVER['REMOTE_ADDR'] == '89.46.130.98'
        ) {

            // single premissions
            $user_permissions_raw = $this->User->query(
                'SELECT access__actions.path from access__actions
                left join access__permissions
                    on access__permissions.action_id = access__actions.id
                left join customers__users
                    on access__permissions.user_id = customers__users.id
                where customers__users.id = ' . $id
            );
            $user_permissions = array();
            foreach ($user_permissions_raw as $up) {
                $user_permissions[] = $up['access__actions']['path'];
            }

            // group premissions
            $this->User->id = $id;
            $group_id = $this->User->field('group_id');
            if ($group_id !='') {
                $this->GroupPermission->bindModel(
                    array(
                        'belongsTo' => array(
                            'Action' => array(
                                'className' => 'Access.Action',
                                'foreignKey' => 'action_id'
                            )
                        )
                    )
                );

                $group_permissions = $this->GroupPermission->find(
                    'all', array(
                    'conditions' => array(
                        'GroupPermission.group_id' => $group_id
                    ),
                    'contain' => array(
                        'Action'
                    ),
                    )
                );

                foreach ($group_permissions as $group_permission) {
                    $user_permissions[] = $group_permission['Action']['path'];
                }
            }

            $user_permissions = array_unique($user_permissions);

            $cache_config = 'oney';

            Cache::write(
                'AccessList' . $id,
                $user_permissions,
                $cache_config
            );

            $accessList = $user_permissions;
        }
        if (!$accessList) {
            return false;
        }
        return $accessList;
    }
    
    /**
    * The delteAccessList method
    * 
    * This method cuts the user's access to any page
    * until the next login
    * 
    * @return void or false
    */
    public function deleteAccessList()
    {
        $id = $this->getCurrentUserId();

        if (!$id) {
            return false;
        }

        Cache::delete('AccessList' . $id, 'oney');
        Cache::delete('AccessList' . $id, 'eighth');

        //fix, because they wanted persistent login and the AccessList deletion
        //is not working anymore (so, i delete all cookies!)
        if (isset($_SERVER['HTTP_COOKIE'])) {
            $cookies = explode(';', $_SERVER['HTTP_COOKIE']);
            foreach ($cookies as $cookie) {
                $parts = explode('=', $cookie);
                $name = trim($parts[0]);
                setcookie($name, '', time() - 1000);
                setcookie($name, '', time() - 1000, '/');
            }
        }
    }
    
    /**
    * The respond method
    * 
    * A convenience method for treating responses of the actions
    * 
    * @param string $message     the message respons
    * @param string $type        type (success, info, error or warning)
    * @param mixed  $redirect    url
    * @param mixed  $append_data data to append to the response
    * 
    * @return mixed
    */
    public function respond(
        $message, 
        $type = 'info', 
        $redirect = null,
        $append_data = null
    ) {
        $allowed_types = array(
            'success',
            'info',
            'error',
            'warning',
        );
        if (!in_array($type, $allowed_types)) {
            $type = 'info';
        }
        if (isset($this->request->params['ext']) 
            && $this->request->params['ext'] == 'json'
        ) {
            header('Content-type: application/json');
            $response = array(
                'status' => $type,
                'message' => $message
            );
            if (!is_null($append_data)) {
                $response['data'] = $append_data;
            }
            die(json_encode($response));
        }

        if ($this->respondWithArray) {
            return array(
                'status' => $type,
                'message' => $message,
                'data' => $append_data
            );
        }

        $this->Session->setFlash(
            $message, 
            "Alerts/$type"
        );
        if (is_null($redirect)) {
            return $this->redirect($this->request->referer());
        } 
        if ($redirect !== false) {
            return $this->redirect($redirect);
        }
        if ($redirect === false) {
            return;
        } 
        return $this->redirect($this->request->referer());
    }
    
    /**
    * The getCurrentUserId method
    * 
    * This method returns the current user id (DOH!)
    * 
    * @return integer
    */
    public function getCurrentUserId() 
    {
        if (isset($_GET['hash']) && isset($_GET['email'])) {
            $id = $this->User->field(
                'id', [
                'User.hash' => $_GET['hash'],
                'User.email' => str_replace(' ', '+', trim($_GET['email'])),
                ]
            );
            if ($id) {
                return $id;
            }
        }
        return $this->Cookie->read(sha1('User') . '.id');
    }

    /**
     * The getAdmins method
     *
     * Gets the admins from the database
     *
     * @return array
     */
    public function getAdmins()
    {
        $this->User->Behaviors->attach('Containable');
        $this->User->bindModel(
            array(
                'hasMany' => array(
                    'Permission' => array(
                        'className' => 'Access.Permission'
                    )
                ),
                'hasOne' => array(
                    'AdminDetail' => array(
                        'className' => 'Customers.AdminDetail'
                    )
                )
            )
        );
        $this->User->Permission->bindModel(
            array(
                'belongsTo' => array(
                    'Action' => array(
                        'className' => 'Access.Action'
                    )
                )
            )
        );
        $this->User->contain(
            array(
                'Person.fname',
                'Person.lname',
                'AdminDetail.picture',
                'AdminDetail.is_picker',
                'AdminDetail.is_home_employee'
            )
        );

        $users = $this->User->find(
            'all',
            array(
                'conditions' => array(
                    'User.is_admin' => true,
                ),
                'fields' => array(
                    'User.id',
                    'User.email',
                    'User.last_login',
                ),
                'order' => array(
                    'AdminDetail.is_picker' => 'DESC',
                    'AdminDetail.is_home_employee' => 'DESC',
                    'Person.fname' => 'ASC',
                    'Person.lname' => 'ASC',
                ),
                'group' => 'User.id'
            )
        );
        return $users;
    }

    /**
     * The getPickerAdmins method
     *
     * Gets the administrators that are also pickers from the database
     *
     * @return array
     */
    public function getPickerAdmins()
    {
        $minimum_date = date('Y-m-d 00:00:00', strtotime('-30 days'));

        $this->Sale->virtualFields['count_sales'] = "COUNT(Sale.id)";
        $this->Sale->contain();
        $admins = $this->Sale->find(
            'list', [
                'fields' => [
                    'Sale.picker_id',
                    'Sale.count_sales'
                ],
                'conditions' => [
                    'Sale.picker_id IS NOT NULL',
                    'Sale.created_at >=' => $minimum_date
                ],
                'group' => ['Sale.picker_id'],
                'order' => [
                    'get_user_full_name(Sale.picker_id)' => 'ASC'
                ]
            ]
        );

        $conditions = [
            'User.is_admin' => true
        ];
        if (!empty($admins)) {
            $conditions['User.id NOT IN '] = array_keys($admins);
        }

        $this->User->virtualFields['user_full_name']
            = 'get_user_full_name(User.id)';
        $this->User->contain();
        $not_pickers = $this->User->find(
            'list',
            array(
                'conditions' => $conditions,
                'fields' => array(
                    'User.id',
                    'User.email',
                ),
                'order' => [
                    "User.user_full_name ASC"
                ],
                'group' => 'User.id'
            )
        );

        $all_admins = array_merge(
            array_keys($admins),
            array_keys($not_pickers)
        );

        $this->User->Behaviors->attach('Containable');
        $this->User->bindModel(
            array(
                'hasMany' => array(
                    'Permission' => array(
                        'className' => 'Access.Permission'
                    )
                ),
                'hasOne' => array(
                    'AdminDetail' => array(
                        'className' => 'Customers.AdminDetail',
                        'foreignKey' => 'user_id',
                        'sort' => ['AdminDetail.id' => 'DESC'],
                        'groupBy' => [
                            'AdminDetail.user_id'
                        ]
                    )
                )
            )
        );
        $this->User->Permission->bindModel(
            array(
                'belongsTo' => array(
                    'Action' => array(
                        'className' => 'Access.Action'
                    )
                )
            )
        );
        $this->User->contain(
            array(
                'Person.fname',
                'Person.lname',
                'AdminDetail.picture'
            )
        );

        $admins_str = implode(",", $all_admins);
        $users = $this->User->find(
            'all',
            array(
                'conditions' => array(
                    'User.is_admin' => true,
                    'User.id IN ' => $all_admins
                ),
                'fields' => array(
                    'User.id',
                    'User.email',
                    'User.last_login',
                ),
                'order' => [
                    "FIND_IN_SET(User.id, '" . $admins_str . "')"
                ],
                'group' => [
                    'User.id'
                ]
            )
        );

        return $users;
    }

    /**
     * The remoteUpload method
     *
     * @param string $file The local file
     * @param string $path The remote path
     * @param string $name The remote name
     *
     * @return bool
     */
    protected function remoteUpload($file, $path, $name = null) 
    {
        return $this->AppModel->remoteUpload($file, $path, $name);
    }

    /**
     * The uploadPicture method
     *
     * @param object from server request
     * @param int                        $number_of_mb number of MB allowed, default 1 MB
     * @param string                     $dest_folder  path wher picture will be saved,
     *
     * @return array with url value and error msg
     */

    protected function uploadPicture(
        $request,
        $dest_folder = 'summernote',
        $number_of_mb = 1
    ) {

        return $this->AppModel->uploadPicture(
            $request,
            $dest_folder,
            $number_of_mb
        );
    }

    /**
     * The remoteDelete
     *
     * @param string $path The path relative to the remote's webroot
     *
     * @return bool
     */
    protected function remoteDelete($path) 
    {
        return $this->AppModel->remoteDelete($path);
    }

    /**
     * The remoteRename
     *
     * @param string $path The path relative to the remote's webroot
     *
     * @return bool
     */
    protected function remoteRename($path)
    {
        return $this->AppModel->remoteRename($path);
    }

    /**
     * The remoteGet
     *
     * @param string $path The path to the file to get
     *
     * @return bool
     */
    public function remoteGet($path = null)
    {
        $this->layout = false;
        $this->autoRender = false;
        if (!is_null($path)) {
            $_GET['path'] = $path;
        }
        if (!isset($_GET['path'])) {
            return $this->respond(
                __('Date inconsistente'),
                'error'
            );
        }
        $info = pathinfo($path);
        if (!isset($info['ext'])) {
            $_GET['path'] .= '.pdf';
        }
        $content = $this->AppModel->remoteGet($_GET['path']);
        header('Content-type: application/pdf');

        echo base64_decode($content->data->file);die();
    }

    /**
     * The _getAnnouncements method
     *
     * Gets the announcements for backend
     *
     * @return void
     */
    private function _getAnnouncements()
    {
        $topAnnouncements = $this->Announcement->find(
            'all', array(
                'conditions' => array(
                    'is_backend' => 1,
                    'from <' => date('Y-m-d H:i:s'),
                    'to >' => date('Y-m-d H:i:s')
                ),
                'order' => 'order ASC'
            )
        );

        $this->set(compact('topAnnouncements'));
    }

    /**
     * The hideDeletedProducts method
     *
     * Hides products marked with `ProductAliasName.is_deleted` = 1
     *
     * @param array $conditions the conditions array is loaded with the new condition
     *
     * @return $conditions
     */
    public function hideDeletedProducts($conditions = [])
    {
        $conditions['ProductAliasName.is_deleted'] = 0;
        return $conditions;
    }

    /**
     *  Method used globally to obtain info from the "logs" table
     *
     * @param null   $user_id -- user responsible for log entry
     *      1 - System user -> gets all logs
     * @param string $model   -- model pertaining to log event
     * @param null   $row_id  -- index of logged model
     * @param int    $level   -- logging level, see php standard
     * @param int    $limit   -- max number of returned items
     *
     * @return array/boolean
     */
    protected function getLog(
        $user_id = null,
        $model = '',
        $row_id = null,
        $level = 5,
        $limit = 10
    ) {
        $conditions = [];
        $conditions['Log.model'] = $model;

        if (!$this->User->find(
            'first',
            ['conditions' => ['User.id' => $user_id]]
        )) {
            return $this->respond(
                __("Nu e niciun user cu acel id!"),
                'error',
                null
            );
        }

        if ($user_id != 1 ) {
            $conditions['Log.user_id'] = $user_id;
        }

        if ($row_id ) {
            $conditions['Log.row_id'] = $row_id;
        }

        if ($level ) {
            $conditions['Log.level'] = $level;
        }

        $logs = $this->Log->find(
            'all',
            [
                'conditions' => $conditions,
                'limit' => $limit
            ]
        );

        return ($logs) ? json_encode($logs)
                        : false;
    }

    /**
     * Get the VAT percent value at a given date
     *
     * @param string $date - given date
     *
     * @return false|string
     */
    public function getVAT($date = null)
    {
        if (is_null($date)) {
            $date = date('Y-m-d');
        }

        $vat = $this->Vat->find(
            'first',
            [
                'conditions' => [
                    'Vat.date <' => date("Y-m-d", strtotime($date))
                ],
                'order' => [
                    'Vat.date' => 'DESC'
                ]
            ]
        );
        $result = (!empty($vat))
            ? (float)$vat['Vat']['VAT']
            : 19;

        return $result;
    }

    /**
     * The isMasterAdmin method
     * Self explanatory
     *
     * @return bool
     */
    public function isMasterAdmin() 
    {
        return ($this->getCurrentUserId() == Configure::read('MASTER_USER_ID'))
            ? true
            : false;
    }


}
